﻿#pragma once

#include <QThread>
#include <QMap>
#include <QString>



class ReadInputThread : public QThread
{
	Q_OBJECT


public:
	void ExitInputThread();
	virtual void run();


public:
	ReadInputThread(QObject *parent);
	~ReadInputThread();
	void InitInputMap(QStringList inputList);
	void SetStartFlag(bool state);
	
private:
	bool m_bExit;
	bool  m_bStart;
	

	QMap<QString, int> m_inputMap;
	QMap<QString, int> m_inputPreMap;
};
