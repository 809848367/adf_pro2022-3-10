#pragma once

#include <QWidget>
#include "ui_CommunicationPage.h"
#include <QtCore>
#include <QStandardItemModel>
#include <QStandardItem>

class CommunicationPage : public QWidget
{
	Q_OBJECT

public:
	CommunicationPage(QWidget *parent = Q_NULLPTR);
	~CommunicationPage();
	void Init();
	void InitWindow();
	void InitConnect();


public slots:
void RefreshSlot();


private:
	Ui::CommunicationPage ui;
	QStandardItemModel* treeModule;

};
