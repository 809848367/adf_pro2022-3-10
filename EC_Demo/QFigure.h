﻿#ifndef QFIGURE_H
#define QFIGURE_H
#include <QtCore>
#include <QLabel>
#include <opencv2/opencv.hpp>

struct AreaWithPos
{
    cv::Mat Image;
    cv::Point Pos;
};
Q_DECLARE_METATYPE(AreaWithPos)

class QFigure : public QLabel
{
    Q_OBJECT
public:
    explicit QFigure(QWidget *parent = nullptr);
    //加载图片
    void SetImage(cv::Mat Image, bool ChangeROI = true);
    cv::Mat ExportImage();
    //控制
    void SetROI(cv::Rect ROI, bool ChangePartner = true);
    void ChangeCallbackMode(int Mode);
    void SetCurcor(int Type,cv::Size s,cv::Scalar Color);
    void SetPartner(QFigure *Partner);

    //画图
    int DrawRect(cv::Point2f Pt1,cv::Point2f Pt2,cv::Scalar Color);
    int DrawRect(cv::Rect2f Region,cv::Scalar Color);
    int DrawCircle(cv::Point2f Center,double Rx,double Ry,cv::Scalar Color);
    int DrawCircle(cv::Point2f Center,double Rx,double Ry, double Angle, cv::Scalar Color);
    int DrawLine(cv::Point2f Pt1,cv::Point2f Pt2,cv::Scalar Color);
    int DrawArea(AreaWithPos Area, cv::Scalar Color);
    void ClearDraw();

    //信息
    QSize ImageSize();
private:
    //基础数据
    QList<QFigure*> Partners;
    QList<cv::Mat> ImageTower;
    cv::Rect ROINow; //原图上的ROI
    cv::Rect ROIShow; //显示的金字塔层的ROI
    int NTower; //当前显示的金字塔层数
    QList<QList<double>> LineData; //额外的划线图形
    QList<QList<double>> TempLineData; //临时的划线图形（用于鼠标回调的显示）
    QList<QPair<AreaWithPos,QVector<double>>> AreaData;  //额外的面区域，需要与Pixel完全重合
    QList<QList<double>> CurcorData;   //临时的鼠标图案（用于鼠标回调的显示）

    //显示
    void ShowImage(int n = 0, bool PartnerROI = true);
    void ShowLine(QList<QList<double>> Data, cv::Mat &Image, cv::Rect tROI, double XRadio, double YRadio);
    void ShowArea(QList<QPair<AreaWithPos,QVector<double>>> Data, cv::Mat &Image, cv::Rect tROI, double XRadio, double YRadio);

    //界面尺寸变换
    void resizeEvent(QResizeEvent *event);

    //鼠标事件
    void mousePressEvent(QMouseEvent *event);//QT鼠标按下事件
    void mouseReleaseEvent(QMouseEvent *event);//QT鼠标松开事件
    void mouseDoubleClickEvent(QMouseEvent *event);//QT鼠标双击事件
    void mouseMoveEvent(QMouseEvent *event);//QT鼠标移动事件
    void wheelEvent(QWheelEvent *event);//QT鼠标滚轮转动事件
    void DealMOuseEvent(int event,int x,int y,Qt::MouseButtons Buttons);//自定义事件处理函数

    //鼠标事件处理函数具体
    //选择（包含区域选择，画方，画圆，划线）
    void SelectCallback(int event,int x,int y,Qt::MouseButtons Buttons);
    int SelectCallbackStep; //选择事件流程标志
public:
    int SelectCallbackType; //选择事件图案标志
    cv::Scalar SelectCallbackColor; //选择事件图案颜色标志

private:
    //移动
    void MoveCallback(int event,int x,int y,Qt::MouseButtons Buttons);
    int MoveCallbackStep;   //移动事件流程标志

    //点击（包含单击，双击）
    void ClickedCallback(int event,int x,int y,Qt::MouseButtons Buttons);

    //光标变化（这个主要用于画笔）
    void CursorCallback(int event,int x,int y,Qt::MouseButtons Buttons);
    cv::Scalar CursorCallbackColor; //光标变化颜色
    int CursorCallbackType;         //光标变化光标类型
    cv::Size CursorCallbackSize;    //光标变化光标尺寸
signals:
    //Mouse Signal
    void SelectCB(cv::Point Pt1,cv::Point Pt2);
    void LBtnClickCB(int x,int y);
    void LBtnDClickCB(int x,int y);
    void RBtnClickCB(int x,int y);
    void RBtnDClickCB(int x,int y);
    void MouseWheel(int x,int y,int value);
    void MoveCB(cv::Point Old,cv::Point New);
    void MousePos(cv::Point Pos,unsigned char GV);

public slots:
    //视野变化的槽函数
    void ZoomInBySelect(cv::Point Pt1,cv::Point Pt2);
    void ZoomInOut(int x,int y,int value);
    void ZoomReset();
    void ZoomMove(cv::Point Old,cv::Point New);
};

#endif // QFIGURE_H
