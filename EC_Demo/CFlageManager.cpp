#include "CFlageManager.h"
CFlageManager theFlageManager;

CFlageManager::CFlageManager(QObject *parent)
	: QObject(parent)
{

}

CFlageManager::~CFlageManager()
{
}

CtrlError CFlageManager::SetFlagValueByName(QString flag_name, int val)
{
	//flag_62; ��������
	QStringList splitList = flag_name.split("_");
	if (splitList.count()<2)
		return CtrlError(ModuleFlage_Not_Found);

	QMutexLocker locker(&m_Mutex[splitList[splitList.count()-1].toInt()-1]);

	foreach(FlagInfo* flag,theDbManager.GetFlagInfo())
	{
		if (flag->name == flag_name)
		{
			flag->value = val;
			break;
		}
	}
	return CtrlError(NoError);
}

CtrlError CFlageManager::GetFlagValueByName(QString flag_name, int& val)
{
	QStringList splitList = flag_name.split("_");
	if (splitList.count()<2)
		return CtrlError(ModuleFlage_Name_Error);
	QMutexLocker locker(&m_Mutex[splitList[splitList.count() - 1].toInt() - 1]);
	foreach(FlagInfo* flag, theDbManager.GetFlagInfo())
	{
		if (flag->name == flag_name)
		{
			val = flag->value;
			break;
		}
	}

	return CtrlError(NoError);
}

void CFlageManager::ClearAllFlag()
{
	foreach(FlagInfo* flag, theDbManager.GetFlagInfo())			
		flag->value = 0;
	return;
}

CtrlError CFlageManager::WaitAndSetFlagValueByName(QString flag_name, int waitVal, int setVal)
{
	QStringList splitList = flag_name.split("_");
	if (splitList.count() < 2)
		return CtrlError(ModuleFlage_Not_Found);

	bool findFlag = false;
	FlagInfo* find_obj = NULL;
	foreach(FlagInfo* flag, theDbManager.GetFlagInfo())
	{
		if (flag->name == flag_name)
		{
			findFlag = true;
			find_obj = flag;
			break;
		}
	}
	if (!findFlag)
		return CtrlError(ModuleFlage_Not_Found);
	while (true)
	{
		QMutexLocker locker(&m_Mutex[splitList[splitList.count() - 1].toInt() - 1]);
		if (find_obj->value == waitVal)
		{
			find_obj->value = setVal;
			break;
		}
		else
		{
			QThread::msleep(1);
			continue;
		}
			
	}
	return CtrlError(NoError);
}

CtrlError CFlageManager::GetTempStringByKey(QString key, QString& val)
{
	//m_TempStringValue.keys().contains(key);
	m_TempStringValue.contains(key);
	if (!m_TempStringValue.contains(key))
		return ModuleFlage_KeyNotFound;
	val = m_TempStringValue[key];
	return NoError;
}

void CFlageManager::SetTempStringByKey(QString key, QString val)
{
	m_TempStringValue[key] = val;
}

CtrlError CFlageManager::InitFlag()
{
	QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
	for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
	{
		CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
	}
	return CtrlError(NoError);
}
