#include "QLoginDlg.h"
#include <QMessageBox>
#include <QString>


#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 
QLoginDlg::QLoginDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->resize(QSize(500, 500 * 0.618));
	this->setWindowTitle("Login page");
}

QLoginDlg::~QLoginDlg()
{
}

void QLoginDlg::on_loginSolt()
{
	QString countStr = ui.countEdit->text();
	QString pwdStr = ui.pwdEdit->text();
	if (theUserManager.GetCurrentUserLevel() != LOGIN_LEVEL_NULL)
	{
		int ret = QMessageBox::information(this, "提示", "已登录是否退出",QMessageBox::Yes,QMessageBox::No);

		//if (ret = QMessageBox::Yes)
		//	theUserManager.LoginOut();
		//else
		//	return;
		switch (ret)
		{
		case QMessageBox::Yes:
			theUserManager.LoginOut();
			break;
		case QMessageBox::No:
			return;
			break;
		default:
			break;
		}
	}

	if (theUserManager.Login(countStr,pwdStr))
	{
		emit loginSuccess(theUserManager.GetCurrentUserLevel(), theUserManager.Get_CurrentlevelName());
		close();
	}
	else
	{
		QMessageBox::information(this, "提示", "账号或密码错误");
	}

	//theUserManager;
	//if (countStr == "1"&&pwdStr=="1" )
	//{
	//	emit loginSuccess();
	//	close();
	//}
	//QMessageBox::information(this, "提示", "登录成功", QMessageBox::Yes, QMessageBox::Yes);
	//QString msg = QString().sprintf("登录信息账号： %1 ；密码：%2 ;").arg(countStr).arg(pwdStr);
	//QMessageBox::information(this, "提示", msg, QMessageBox::Yes, QMessageBox::Yes);
	//emit loginSuccess();
	//close();

}

void QLoginDlg::on_exitSolt()
{
	close();
}
