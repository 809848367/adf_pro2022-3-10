#ifndef TEACHDATA_H
#define TEACHDATA_H
#include <QtCore>

class TeachData: public QObject
{
    Q_OBJECT
public:
    explicit TeachData(QObject *parent = nullptr);
    virtual int FromTable(QVector<QString> Keys,QVector<QVector<QVariant>> Data) = 0;
    virtual void ToTable(QVector<QString> &Keys,QVector<QVector<QVariant>> &Data) = 0;
};

#endif // TEACHDATA_H
