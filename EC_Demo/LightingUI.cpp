﻿#include "LightingUI.h"
#include "ui_LightingUI.h"
#include <QSpinBox>
#include <QSlider>
#include <QMessageBox>
#include <QInputDialog>
LightingUI::LightingUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LightingUI)
{
    ui->setupUi(this);
    InitLightingList();
    HWM = nullptr;
    InitTeachDataList();
    connect(&TeachDataBtns,SIGNAL(buttonClicked(int)),this,SLOT(TeachDataBtns_clicked(int)));
    connect(&LightValueMapper,SIGNAL(mapped(int)),this,SLOT(LightValueChanged(int)));
    connect(&RefreshTimer,SIGNAL(timeout()),this,SLOT(RefreshTimerSlot()));
    RefreshTimer.start(100);
}

LightingUI::~LightingUI()
{
    delete ui;
}

void LightingUI::SetHardwareManager(HardwareManager *HWM, QString Station)
{
    this->HWM = HWM;
    this->Station = Station;
}

void LightingUI::InitLightingList()
{
    ui->Lighting->setColumnCount(3);
    ui->Lighting->setEditTriggers(QTableWidget::NoEditTriggers);
    ui->Lighting->horizontalHeader()->setVisible(true);
    ui->Lighting->horizontalHeader()->setFixedHeight(30);
    QList<QString> HItems;
    HItems<<"Current"<<"SetValue"<<"SetValue";
    ui->Lighting->setHorizontalHeaderLabels(HItems);
    ui->Lighting->verticalHeader()->setVisible(true);
    ui->Lighting->verticalHeader()->setFixedWidth(30);
    ui->Lighting->setColumnWidth(0,100);
    ui->Lighting->setColumnWidth(1,100);
    ui->Lighting->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
    ui->Lighting->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Fixed);
    ui->Lighting->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
}

void LightingUI::ShowData()
{
    CtrlError ret;
    if(HWM == nullptr)
    {
        ui->Lighting->setRowCount(0);
        return;
    }
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetAllLight";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Value") || !Recv.Datas["Value"].canConvert<QMap<int,uchar>>()) return;
    QMap<int,uchar> Value = Recv.Datas["Value"].value<QMap<int,uchar>>();
    if(ret != 0) return;
    QList<int> Index = Value.keys();
    if(ui->Lighting->rowCount() != Value.size())
    {
        ui->Lighting->setRowCount(Value.size());
        for(int i=0;i<Index.size();i++)
        {
            ui->Lighting->setVerticalHeaderItem(i,new QTableWidgetItem(QString::number(Index[i])));
            ui->Lighting->setItem(i,0,new QTableWidgetItem(QString::number(Value[i])));
            QSpinBox *SpinBox = new QSpinBox();
            SpinBox->setMinimum(0);
            SpinBox->setMaximum(255);
            SpinBox->setValue(0);
            SpinBox->setSingleStep(1);
            ui->Lighting->setCellWidget(i,1,SpinBox);
            QSlider *Slider = new QSlider();
            Slider->setOrientation(Qt::Horizontal);
            Slider->setMinimum(0);
            Slider->setMaximum(255);
            Slider->setValue(0);
            Slider->setSingleStep(5);
            ui->Lighting->setCellWidget(i,2,Slider);
            connect(SpinBox,SIGNAL(valueChanged(int)),Slider,SLOT(setValue(int)));
            connect(Slider,SIGNAL(valueChanged(int)),SpinBox,SLOT(setValue(int)));
            connect(SpinBox,SIGNAL(valueChanged(int)),&LightValueMapper,SLOT(map()));
            LightValueMapper.setMapping(SpinBox,i);
        }
    }
    else
    {
        for(int i=0;i<Index.size();i++)
        {
            ui->Lighting->setItem(i,0,new QTableWidgetItem(QString::number(Value[Index[i]])));
        }
    }
}

void LightingUI::InitTeachDataList()
{
    ui->TeachData->setColumnCount(3);
    ui->TeachData->horizontalHeader()->setVisible(true);
    ui->TeachData->horizontalHeader()->setFixedHeight(30);
    QList<QString> HItems;
    HItems<<"Name"<<"Value"<<"";
    ui->TeachData->setHorizontalHeaderLabels(HItems);
    ui->TeachData->verticalHeader()->setVisible(true);
    ui->TeachData->verticalHeader()->setFixedWidth(30);
    ui->TeachData->setColumnWidth(0,100);
    ui->TeachData->setColumnWidth(2,50);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);
}

void LightingUI::ShowTeachData()
{
    ShowTeachDataFlag = true;
    while(TeachDataBtns.buttons().size() > 0) TeachDataBtns.removeButton(TeachDataBtns.buttons()[0]);
    ui->TeachData->clearContents();
    QList<QPair<QString, QMap<int,uchar>>> Lights = TeachData.GetLights();
    ui->TeachData->setRowCount(Lights.size()+1);
    for(int i=0;i<Lights.size();i++)
    {
        ui->TeachData->setVerticalHeaderItem(i,new QTableWidgetItem(QString::number(i)));
        ui->TeachData->setItem(i,0,new QTableWidgetItem(Lights[i].first));
        ui->TeachData->item(i,0)->setFlags(ui->TeachData->item(i,0)->flags() & ~Qt::ItemIsEditable);
        QString tmp;
        foreach(int Key,Lights[i].second.keys())
        {
            tmp.append(QString::number(Key)+","+QString::number(Lights[i].second[Key])+";");
        }
        if(!tmp.isEmpty()) tmp.remove(tmp.size()-1,1);
        ui->TeachData->setItem(i,1,new QTableWidgetItem(tmp));
        QPushButton *TeachDataBtn = new QPushButton();
        TeachDataBtn->setText("Now");
        TeachDataBtns.addButton(TeachDataBtn,i);
        ui->TeachData->setCellWidget(i,2,TeachDataBtn);
    }
    ui->TeachData->setVerticalHeaderItem(Lights.size(),new QTableWidgetItem());
    for(int j=0;j<ui->TeachData->columnCount();j++)
    {
        ui->TeachData->setItem(Lights.size(),j,new QTableWidgetItem());
        ui->TeachData->item(Lights.size(),j)->setBackgroundColor(QColor(230,230,230));
        ui->TeachData->item(Lights.size(),j)->setFlags(ui->TeachData->item(Lights.size(),j)->flags() & ~Qt::ItemIsEditable);
    }
    ShowTeachDataFlag = false;
}

void LightingUI::LightValueChanged(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    if(n<0||n>=ui->Lighting->rowCount()) return;
    QTableWidgetItem *item = ui->Lighting->verticalHeaderItem(n);
    QSpinBox* SpinBox = (QSpinBox*)ui->Lighting->cellWidget(n,1);
    if(SpinBox == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetLight";
    Send.Identfication = 100;
    Send.Datas.insert("Index",QVariant::fromValue(item->text().toInt()));
    Send.Datas.insert("Value",QVariant::fromValue(SpinBox->value()));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
}

void LightingUI::RefreshTimerSlot()
{
    if(!isVisible()) return;
    ShowData();
}

void LightingUI::RefreshTeachData(QString Station, TeachData_Lighting TeachData)
{
    if(this->Station != Station) return;
    this->TeachData = TeachData;
    ShowTeachData();
}

void LightingUI::TeachDataBtns_clicked(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetAllLight";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Value") || !Recv.Datas["Value"].canConvert<QMap<int,uchar>>()) return;
    QMap<int,uchar> Value = Recv.Datas["Value"].value<QMap<int,uchar>>();
    TeachData.SetLight(ui->TeachData->item(n,0)->text(),Value);
    UpDateTeachData(Station,TeachData);
}

void LightingUI::on_Teach_Add_clicked()
{
    QString Name = QInputDialog::getText(this,"Add","Input Add Name");
    if(Name.isEmpty()) return;
    TeachData.SetLight(Name,QMap<int,uchar>());
    UpDateTeachData(Station,TeachData);
}

void LightingUI::on_Teach_Del_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    if(QMessageBox::warning(this,"Warning",QString("Delete %1 ?").arg(Name),QMessageBox::Ok,QMessageBox::Cancel) != QMessageBox::Ok) return;
    TeachData.RemoveLight(Name);
    UpDateTeachData(Station,TeachData);
}

void LightingUI::on_Teach_Set_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    QMap<int,uchar> tLights;
    if(TeachData.GetLight(Name,tLights) != 0) return;

    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetAllLight";
    Send.Identfication = 100;
    Send.Datas.insert("Value",QVariant::fromValue(tLights));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void LightingUI::on_TeachData_cellChanged(int row, int column)
{
    if(ShowTeachDataFlag) return;
    if(column == 0) return;
    if(row<0 || row >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(row,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;

    item = ui->TeachData->item(row,1);
    if(!item) return;
    QList<QString> tmp = item->text().split(";");
    QMap<int,uchar> tLight;
    foreach(QString KeyValue,tmp)
    {
        if(!KeyValue.contains(",")) continue;
        bool isOK;
        int Index = KeyValue.left(KeyValue.indexOf(",")).toInt(&isOK);
        if(!isOK) continue;
        uchar Value = KeyValue.right(KeyValue.size()-KeyValue.lastIndexOf(",")-1).toUInt(&isOK);
        if(!isOK) continue;
        tLight.insert(Index,Value);
    }
    TeachData.SetLight(Name,tLight);
    UpDateTeachData(Station,TeachData);
}
