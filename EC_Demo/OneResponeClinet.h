#pragma once


#include <QtCore>
#include <QTcpSocket>
#include <atomic>
#include "CtrlError.h"
//one cmd for one respond\
//带心跳的 socket 通信类  ADF PLC 使用 
struct PLCData 
{
	qint8 port;
	qint8 value;
	int timeOut;
	QString OperatorType;//read, write,wait
	int indetify;//标识
	QString resInfo;
	PLCData() // 默认构造心跳
	{
		port = 0x0a;
		value = 0x01;
		OperatorType = "write";
		resInfo = "";
		timeOut = 0;
	}
};
Q_DECLARE_METATYPE(PLCData)

class OneResponeClinet : public QThread	
{
	Q_OBJECT
public:
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x00102000,
		ConnectionPLCError = Base + 0x001,
		SocketError = Base + 0x002,
	};
	Q_ENUM(ErrorCode)

public:
	OneResponeClinet(QString IP, quint16 Port);
	~OneResponeClinet();
	void run() override;
	bool IsConnectServer();

public:
	void PushOneCmdDataToList(PLCData data);
	void PushOneRespondDataToList(PLCData data);
	PLCData GetOneRespondDataFromList();
	bool WaitDataByIdnetiy(int indetify,int tOut);
private slots:

private:
	PLCData GetOneCmdDataFromList();



	CtrlError SendAndRecv(QByteArray send, QByteArray& respond);
	QByteArray CreateWriteCmd(qint8 port,qint8 value);
	QByteArray CreateReadCmd(qint8 port);
private:
	QTcpSocket* m_socket;
	QString IP;
	qint16 Port;
	QList<PLCData> cmdList;		//发送 
	QList<PLCData> respondList;	//反馈
	bool m_bRun;
	QMutex m_cmdMutex;//命令锁
	QMutex m_resMutex;//回应锁
};
