#ifndef ALGOMANAGER_H
#define ALGOMANAGER_H

#include <QtCore>
#include "HardwareModule.h"
#include "CtrlError.h"
#include "AlgoClient.h"
struct AlgoResult
{
    AlgoResult() {}
    QString Surface;
    QString Result;
    QString ResultPath;
    QString TargetPoint;
	QStringList FacePath;
	QString msg;
};
Q_DECLARE_METATYPE(AlgoResult)

class AlgoManager : public HardwareModule
{
    Q_OBJECT
public:
    explicit AlgoManager(QObject *parent = nullptr);
    ~AlgoManager();
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);
    CtrlError Init(QString Name,QMap<QString,QPair<QString,quint16>> Client);

    //Send Command
    CtrlError SendCmd(QString Station, QString SN, QString Surface, QMap<QString,QString> Data);
	CtrlError SendCmdFree(QMap<QString, QString> Data);
	CtrlError SendCmdMulityFace(QString Station, QString SN, QStringList Surface, QMap<QString, QString> Data);
    CtrlError LoadGolden(QString Station, QString SN, QString Surface, QString GoldenPath, QMap<QString,QString> Options = QMap<QString,QString>());
    CtrlError SendCmdPOS(QString Station, QString SN, QString Surface, QString ImagePath, QMap<QString,QString> Options = QMap<QString,QString>());
    CtrlError SendCmdSRC(QString Station, QString SN, QString Surface, QString ImagePath, QMap<QString,QString> Options = QMap<QString,QString>());
    CtrlError SendCmdMOON(QString Station, QString SN, QString Surface, QString ImagePath, QMap<QString,QString> Options = QMap<QString,QString>());
    CtrlError SendCmdIMS(QString Station, QString SN, QString Surface, QString ImagePath, QString ConfigPath, QMap<QString,QString> Options = QMap<QString,QString>());
	CtrlError SendCmdIMS_Mulity(QString Station, QString SN, QStringList Surface, QStringList ImagePath, QString ConfigPath, QMap<QString, QString> Options = QMap<QString, QString>());

    //查询控制
    CtrlError SetBarCode(QString SN, QString BarCode);
    CtrlError ChcekFinish(QString SN,int &Send,int &Recv,bool &isFinish);
    CtrlError WaitFinish(QString SN, int TimeOut);
    CtrlError GetResult(QString SN, QString Surface, AlgoResult &Result);
    CtrlError WaitResult(QString SN, QString Surface, int TimeOut, AlgoResult &Result);
    CtrlError GetAllResult(QString SN, QMap<QString,AlgoResult> &Results);
    CtrlError Complete(QString SN, QString NewFolder, int Force = 0);
private:
    QString Name;
    QReadWriteLock Datalock;
    QMap<QString,AlgoClient*> Clients;
    QMap<QString,QSet<QString>> SendSum;
    QMap<QString,QSet<QString>> RecvSum;
    QMap<QString,QMap<QString,AlgoResult>> RecvRes;
    QMap<QString,QSet<QString>> RecvPath;
    QMap<QString,QString> BarCodes;
    QMap<QString,QMap<QString,AlgoCommData>> DataBaseData;
signals:
    CtrlError SetDataInDB(QString SN,QMap<QString,AlgoCommData> Datas, QMap<QString,QString> FilePaths);
private slots:
    void RecvResult(QString Name,QString ResultStr);
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00600000,
        StationNotExist          = Base + 0x001,
        SNNotFinish              = Base + 0x002,
        SNNotExist              = Base + 0x003,
        ExecuteDataNotRight     = Base + 0x004,
        InitDataErr             = Base + 0x005,
        ResultNotExist          = Base + 0x006,
        WaitResultTimeout       = Base + 0x007,
        WaitFinishTimeout       = Base + 0x008,
		IMSMulityMatchError		= Base + 0x009,
    };
    Q_ENUM(ErrorCode)
};

#endif // ALGOMANAGER_H
