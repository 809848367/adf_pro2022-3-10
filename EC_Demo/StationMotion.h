#pragma once
#include <QString>
#include <QList>
#include <QStringList>
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 



//******************** 点位 *****************************
struct PointStation {
	int PointIndex;//对应点位ID
	QString PointName;//对应点位名称 非空则为有用的点位
					  //QString StationName;
	QList<double> pValueList;//根据列名输入轴的点位值
	PointStation()
	{
		PointIndex = 0;
		PointName = "";
		pValueList.clear();
		//StationName = "";
	}
};
//******************** 工站 *****************************
class CStationMotion
{
public:
	CStationMotion();
	CStationMotion(QString stName, int axisCount, QStringList axisNameList);
	~CStationMotion();

	QString m_StatName;//工站名称
	int m_AxisCount;//轴数量
	QStringList m_AxisNameList;//轴名称列表
	QList<PointStation*> m_PonitList;//默认创建100个点位供使用 用作tablistview 使用
	QStringList m_ablePos;//可用点位
	QString m_PosTableName;//点位表名
	int m_StatID;
};


////多个安全点位 进行
//class SafeMove
//{
//public:
//	SafeMove();
//
//public:
//	QString name;
//	//工站 、点位 、优先级
//	QList<SafePos> posList;
//	void SortPosList();
//};
