#include "InfoDlg.h"
#include <QVBoxLayout>
#include "HistoryUI.h"
#include "History.h"
InfoDlg* InfoDlg::info_stance = nullptr;
InfoDlg::InfoDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	Init();
	InitConnection();
	InitWindow();
}

InfoDlg::~InfoDlg()
{
}

void InfoDlg::Init()
{
	//History histroy = History::GetInstance();
}

void InfoDlg::InitConnection()
{

}

void InfoDlg::InitWindow()
{
	Qt::WindowFlags windowFlag = Qt::Dialog;
	windowFlag |= Qt::WindowMinimizeButtonHint;
	windowFlag |= Qt::WindowMaximizeButtonHint;
	windowFlag |= Qt::WindowCloseButtonHint;
	this->setWindowFlags(windowFlag);

	mainWidget = new QTabWidget(ui.CenterWidget);

	QVBoxLayout *verticalLayout;
	verticalLayout = new QVBoxLayout(ui.CenterWidget);
	verticalLayout->addWidget(mainWidget);
	HistoryUI* History_UI = new HistoryUI(mainWidget);
	mainWidget->addTab(History_UI, "History");
}

InfoDlg* InfoDlg::GetInstance()
{
	if (info_stance == nullptr)
	{
		info_stance = new InfoDlg;
	}
	return info_stance;
}

void InfoDlg::DispInfoDlg()
{
	InfoDlg::GetInstance()->show();
}

void InfoDlg::ReleaseInfoDlg()
{
	if (info_stance != nullptr)
	{
		delete info_stance;
		info_stance = nullptr;
	}
}
