#include "DataAnalysis.h"
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8") 
#endif 

CDataAnalysis dataAnaly;

CDataAnalysis::CDataAnalysis()
{
	m_deviceStatus = Device_Status_Wait_Reset;
	m_operType.append(QString("单轴回原"));
	m_operType.append(QString("工站回原"));
	m_operType.append(QString("单轴运动"));
	m_operType.append(QString("工站运动"));
	m_operType.append(QString("OutPutWrite"));
	m_operType.append(QString("InputRead"));
	m_operType.append(QString("标志位设置"));
	m_operType.append(QString("标志位检测"));
	m_operType.append(QString("标志位等待"));
	m_operType.append(QString("延时操作"));
	m_operType.append(QString("内部步骤跳转"));

	m_operType.append(QString("外部流程启动"));
	m_operType.append(QString("单轴偏移运动"));
	m_operType.append(QString("串口信息发送"));
	m_operType.append(QString("串口信息接收"));
	m_operType.append(QString("Tcp信息接收"));
	m_operType.append(QString("Tcp发送"));
	m_operType.append(QString("delay"));
	m_operType.append(QString("startFlow"));
	m_operType.append(QString("jumpIndex"));
}



CDataAnalysis::~CDataAnalysis()
{
}

bool CDataAnalysis::AnalysisProgramXml(QString path)
{
	QFile file(path);
	//file.open(QIODevice::ReadWrite | QIODevice::Text);
	if (!file.open(QFile::ReadOnly))
		return false;
	QDomDocument doc;
	if (!doc.setContent(&file))
	{
		file.close();
		return false;
	}
	file.close();

	//
	QDomElement root = doc.documentElement();
	if (root.nodeName() != "program")
		return false;
	m_CurProgram.labStr = root.nodeName();
	m_CurProgram.Identification = root.attribute("Identification");
	m_CurProgram.aliasName = root.attribute("name");
	//解析文件
	QDomNode node = root.firstChild();
	while (!node.isNull())
	{
		if (node.isElement())
		{
			QDomElement e = node.toElement();
			if (e.nodeName() == "process")
			{
				ProcessInfo* pInfo = new ProcessInfo();
				pInfo->aliasName = e.attribute("Alias");
				m_processNameList.push_back(pInfo->aliasName);
				//pInfo->processNum = e.attribute("processNum");
				pInfo->index = e.attribute("index").toInt();
				pInfo->Identification = e.attribute("Identification");
				pInfo->labStr = e.nodeName();
				pInfo->startInput = e.attribute("StartInput");
				pInfo->pauseInput = e.attribute("PauseInput");
				pInfo->stopInput = e.attribute("StopInput");
				if (e.hasAttribute("bUse"))
					pInfo->bUse = e.attribute("bUse").toInt();
				else
					pInfo->bUse = 1;
				//m_processNameList.push_back(pInfo->aliasName);
				//qDebug() << m_processNameList;

				QDomNode secondNode = e.firstChild();
				while (!secondNode.isNull())
				{
					if (secondNode.isElement())
					{
						QDomElement e2 = secondNode.toElement();
						if (e2.nodeName() == "step")
						{
							StepInfo* stInfo = new StepInfo();
							stInfo->labStr = e2.nodeName();
							stInfo->stepType = e2.attribute("type");
							stInfo->aliasName = e2.attribute("stepName");
							//stInfo->stepNum = e2.attribute("stepIndex");

							stInfo->index = e2.attribute("index").toInt();
							stInfo->Identification = e2.attribute("Identification");
							stInfo->CodSupplement = e2.attribute("CodSupplement").toInt();
							stInfo->belongProc = e2.attribute("belong");

							if (e2.hasAttribute("bUse"))
								stInfo->bUse = e2.attribute("bUse").toInt();
							else
								stInfo->bUse = 1;

							QDomNode thirdNode = e2.firstChild();
							while (!thirdNode.isNull())
							{
								if (thirdNode.isElement())
								{
									QDomElement e3 = thirdNode.toElement();
									if (e3.nodeName() == "operator")
									{
										OperatorInfo* oInfo = new OperatorInfo();
										//QString labStr;		//标签名称
										//QString station;	//工站名称
										//QString aliasName;	//别名
										//QString oprtType;	//操作类型
										//QString posNum;		//点位索引
										//QString param4;//参数4
										//QString param5;//参数5
										//double  param1;
										//double  param2;
										//double  param3;
										//QString opeatorNum;
										oInfo->labStr = e3.nodeName();
										oInfo->station = e3.attribute("station");
										oInfo->aliasName = e3.attribute("Name");
										oInfo->oprtType = e3.attribute("type");
										oInfo->postion = e3.attribute("postion");
										oInfo->param4 = e3.attribute("param4");
										oInfo->param5 = e3.attribute("param5");
										oInfo->param1 = e3.attribute("param1");
										oInfo->param2 = e3.attribute("param2");
										oInfo->param3 = e3.attribute("param3");

										oInfo->index = e3.attribute("index").toInt();
										oInfo->Identification = e3.attribute("Identification");
										oInfo->belongStep = e3.attribute("belong");
										if (e3.hasAttribute("bUse"))
											oInfo->bUse = e3.attribute("bUse").toInt();
										else
											oInfo->bUse = 1;

										stInfo->opeartorInfoList << oInfo;
									}
								}
								thirdNode = thirdNode.nextSibling();
							}
							pInfo->stepInfoList << stInfo;
						}
					}
					secondNode = secondNode.nextSibling();
				}
				m_CurProgram.processInfoList << pInfo;
			}
		}
		node = node.nextSibling();
	}
	qDebug() << root.nodeName();
	return true;
}

void CDataAnalysis::clear()
{
	if (m_CurProgram.processInfoList.count() == 0)
		return;

	foreach(ProcessInfo *pInfo, m_CurProgram.processInfoList)
	{
		foreach(StepInfo  *sInfo, pInfo->stepInfoList)
		{
			sInfo->opeartorInfoList.clear();
		}
		pInfo->stepInfoList.clear();
	}
	m_CurProgram.processInfoList.clear();

}

void CDataAnalysis::SaveProgramInfo(ProgramInfo Program, QString SavePath)
{
	if (SavePath == "")
		return;
	QDomDocument doc;

	QDomProcessingInstruction instruction;
	instruction = doc.createProcessingInstruction("xml", " version = \"1.0\" encoding = \"UTF - 8\"");
	doc.appendChild(instruction);
	QDomElement root = doc.createElement(Program.labStr);
	root.setAttribute("name", Program.aliasName);
	root.setAttribute("Identification", Program.Identification);
	doc.appendChild(root);
	foreach(ProcessInfo * proc, Program.processInfoList)
	{
		QDomElement ProcElement = doc.createElement(proc->labStr);
		//流程添加
		//<Process index = "0" Identification = "00" Alias = "复位" StartInput = "复位" PauseInput = "暂停" 
		//StopInput = "停止">

		ProcElement.setAttribute("index", QString("%1").arg(proc->index));
		//ProcElement.setAttribute("Identification", QString().sprintf("%02d", proc->Identification));
		ProcElement.setAttribute("Alias", QString("%1").arg(proc->aliasName));
		ProcElement.setAttribute("StartInput", QString("%1").arg(proc->startInput));
		ProcElement.setAttribute("PauseInput", QString("%1").arg(proc->pauseInput));
		ProcElement.setAttribute("StopInput", QString("%1").arg(proc->stopInput));
		ProcElement.setAttribute("Identification", QString("%1").arg(proc->Identification));
		foreach(StepInfo *step, proc->stepInfoList)
		{
			QDomElement StepElement = doc.createElement(step->labStr);
			//<step index = "0" stepNum = "00" type = "Normal" stepName = "z轴初始化" stepIndex = "0000" 
			//Identification = "0000" belong = "00" CodSupplement = "1">
			StepElement.setAttribute("index", QString("%1").arg(step->index));
			//StepElement.setAttribute("stepNum", QString("%1").arg(step->stepNum));
			StepElement.setAttribute("type", QString("%1").arg(step->stepType));
			StepElement.setAttribute("stepName", QString("%1").arg(step->aliasName));
			StepElement.setAttribute("Identification", QString("%1").arg(step->Identification));
			//StepElement.setAttribute("Identification", QString().sprintf("%02d%02d", proc->Identification, step->Identification));
			StepElement.setAttribute("belong", QString("%1").arg(step->belongProc));
			StepElement.setAttribute("CodSupplement", QString("%1").arg(step->CodSupplement));
			StepElement.setAttribute("bUse", QString("%1").arg(step->bUse));
			foreach(OperatorInfo * oper, step->opeartorInfoList)
			{
				QDomElement OperatElement = doc.createElement(oper->labStr);
				//<operator index = "0" station = "中转工站" Identification = "010000" Name = "操作1" 
				//	type = "AxisHome" postion = "0" description1 = "1" description2 = "2" param1 = "1" 
				//	param2 = "1" param3 = "1" belong = "0100">< / operator>
				//QString labStr;		//标签名称	nodeName
				//QString station;	//工站名称	attribute::station
				//QString aliasName;	//别名		attribute::Name
				//QString oprtType;	//操作类型	attribute::type
				//QString posNum;		//点位索引	attribute::postion
				//QString descption1;	//描述1		attribute::description1
				//QString descption2;	//描述2		attribute::description2
				//QString  param1;	//参数1 供相应操作处理	attribute::param1
				//QString  param2;	//参数2 供相应操作处理	attribute::param2
				//QString  param3;	//参数2 供相应操作处理	attribute::param3
				//					//QString opeatorNum;
				//int index;				//attribute::index
				//QString Identification;	//唯一标识 号 流程号 + 步骤号 + 操作号 即标识 attribute::Identification
				//QString belongStep;		//attribute::belong

				OperatElement.setAttribute("index", QString("%1").arg(oper->index));
				OperatElement.setAttribute("Name", QString("%1").arg(oper->aliasName));
				OperatElement.setAttribute("station", QString("%1").arg(oper->station));
				OperatElement.setAttribute("type", QString("%1").arg(oper->oprtType));
				OperatElement.setAttribute("postion", QString("%1").arg(oper->postion));	
				OperatElement.setAttribute("param4", QString("%1").arg(oper->param4));
				OperatElement.setAttribute("param5", QString("%1").arg(oper->param5));
				OperatElement.setAttribute("param1", QString("%1").arg(oper->param1));
				OperatElement.setAttribute("param2", QString("%1").arg(oper->param2));
				OperatElement.setAttribute("param3", QString("%1").arg(oper->param3));
				OperatElement.setAttribute("belong", QString("%1").arg(oper->belongStep));
				OperatElement.setAttribute("Identification", QString("%1").arg(oper->Identification));
				OperatElement.setAttribute("bUse", QString("%1").arg(oper->bUse));
				StepElement.appendChild(OperatElement);
			}


			ProcElement.appendChild(StepElement);
		}

		root.appendChild(ProcElement);
		//doc.appendChild(root);
	}
	SaveXMLFile(doc, SavePath);
	//save xml file 
	//QFile file(SavePath);
	//if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
	//	return;
	//QTextStream out(&file);
	//doc.save;
	//file.close();
}

void CDataAnalysis::SaveXMLFile(QDomDocument& doc, QString savePath)
{
	QFile file(savePath);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
		return;
	QTextStream out(&file);
	doc.save(out, 4);
	file.close();
}

void CDataAnalysis::DataSort()
{
	//刷新 index indefiy
	for (int i = 0; i != m_CurProgram.processInfoList.count(); ++i)
	{
		ProcessInfo * proc = m_CurProgram.processInfoList.at(i);
		proc->Identification = QString("%1").arg(i, 2, 10, QLatin1Char('0'));
		proc->index = i;
		for (int j = 0; j != proc->stepInfoList.count(); ++j)
		{
			StepInfo* step = proc->stepInfoList.at(j);
			step->index = j;
			QString identifyPre = QString("%1").arg(step->index, 2, 10, QLatin1Char('0'));
			step->Identification = QString("%1%2").arg(proc->Identification).arg(identifyPre);
			step->belongProc = proc->Identification;
			for (int k = 0; k != step->opeartorInfoList.count(); ++k)
			{
				OperatorInfo *oper = step->opeartorInfoList.at(k);
				oper->index = k;
				QString identifyPre = QString("%1").arg(oper->index, 2, 10, QLatin1Char('0'));
				oper->Identification = QString("%1%2").arg(step->Identification).arg(identifyPre);
				oper->belongStep = step->Identification;
			}
		}
	}
}

QStringList CDataAnalysis::GetOperatorTypeList()
{	
	return m_operType;

}

QStringList CDataAnalysis::GetProcessNameList()
{
	return m_processNameList;
}

