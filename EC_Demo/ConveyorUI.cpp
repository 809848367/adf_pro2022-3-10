﻿#include "ConveyorUI.h"
#include "ui_ConveyorUI.h"
#include <QMessageBox>
ConveyorUI::ConveyorUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConveyorUI)
{
    ui->setupUi(this);
    connect(&RefreshTimer,SIGNAL(timeout()),this,SLOT(RefreshTimerSlot()));
    RefreshTimer.start(200);
}

ConveyorUI::~ConveyorUI()
{
    delete ui;
}

void ConveyorUI::SetHardwareManager(HardwareManager *HWM, QString Station)
{
    this->HWM = HWM;
    this->Station = Station;
}

void ConveyorUI::on_Start_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Start";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void ConveyorUI::on_Stop_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Stop";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void ConveyorUI::on_Enable_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetEnable";
    Send.Identfication = 100;
    Send.Datas.insert("Enable",QVariant::fromValue(true));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void ConveyorUI::on_Disable_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetEnable";
    Send.Identfication = 100;
    Send.Datas.insert("Enable",QVariant::fromValue(false));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void ConveyorUI::on_ClearError_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "ClearError";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void ConveyorUI::RefreshTimerSlot()
{
    if(!isVisible()) return;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<int>()) return;
    int Status = Recv.Datas["Status"].value<int>();


    switch(Status)
    {
    case 0:
        ui->Status->setText("Disable");
        break;
    case 1:
        ui->Status->setText("Enable");
        break;
    case 2:
        ui->Status->setText("Running");
        break;
    case 3:
        ui->Status->setText("Error");
        break;
    default:
        ui->Status->setText("Unknow");
        break;
    }
}
