#pragma once

#include <QObject>
#include <QtCore>
//通用信息映射类
//用作设备整体信息遍历刷新，供UI或者其他模块使用
#define  theMappingManager MappingManager::Instance()
class MappingManager : public QObject
{
	Q_OBJECT

public:
	MappingManager(QObject *parent = Q_NULLPTR);
	~MappingManager();
	QMap<QString, QList<QString>> GetMapInfo();

public slots:
	//<module,Item>
void AddOneClient(QString clientName);//连接服务器
void DeleteOneClient(QString clinetName);//断开服务器

void AddOneServer(QString serverName);
void AddOneSkt(QString serverName,QString clientName);//服务器连接的skt
void DeleteOneSkt(QString serverName, QString clientName);//服务器连接的skt



public:
	QMap<QString, QList<QString>> mdouleInfo;
	static MappingManager* Instance();
};


