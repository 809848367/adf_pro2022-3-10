#ifndef HISTORYDEFINE_H
#define HISTORYDEFINE_H
#include <QtCore>

struct HistorySurface
{
    QString SN;
    QString Surface;
    QMap<QString,QString> Files;
    QMap<QString,QString> Datas;
};
Q_DECLARE_METATYPE(HistorySurface)
struct HistoryResult
{
    QString SN;
    QString BarCode;
    QString Result;
    QDateTime DateTime;
    QMap<QString,HistorySurface> Surfaces;
};
Q_DECLARE_METATYPE(HistoryResult)
Q_DECLARE_METATYPE(QVector<HistoryResult>)
#endif // HISTORYDEFINE_H
