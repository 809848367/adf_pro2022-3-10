#pragma once

#include <QObject>
#include <QtCore>
#include "DatabaseManager.h"
#include "CtrlError.h"
#include <QMetaObject>
//Flag - 临时变量类
class CFlageManager : public QObject
{
	Q_OBJECT
		//Q_GADGET
public:
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x00100000,
		ModuleFlage_Not_Found		=				Base + 0x001,
		ModuleFlage_Change_Default	=				Base + 0x002,
		ModuleFlage_Name_Error	    =				Base + 0x003,
		ModuleFlage_Commond			=				Base + 0x004,
		ModuleFlage_KeyNotFound		=				Base + 0x005,
	};
	Q_ENUM(ErrorCode)

	CFlageManager(QObject *parent= nullptr);
	~CFlageManager();
	CtrlError SetFlagValueByName(QString flag_name, int val);
	CtrlError GetFlagValueByName(QString flag_name, int& val);

	void ClearAllFlag();

	//wait and set
	CtrlError WaitAndSetFlagValueByName(QString flag_name, int waitVal, int setVal);

	CtrlError GetTempStringByKey(QString key,QString& val);
	void SetTempStringByKey(QString key, QString val);
	QMap<QString, QString> GetTempData() { return m_TempStringValue; };


public:
	CtrlError InitFlag();
	//QSemaphore m_semap[100];//最多使用100
	QMutex m_Mutex[100];
	QMap<QString, QString> m_TempStringValue;//临时变量存储
	//QList<QString> m_SN;

};

extern CFlageManager theFlageManager;