#include "TeachData_Robot.h"

TeachData_Robot::TeachData_Robot(QObject *parent) : TeachData(parent)
{
    qRegisterMetaType <TeachData_Robot>("TeachData_Robot");
}

TeachData_Robot::TeachData_Robot(const TeachData_Robot &TeachData)
{
    this->Points = TeachData.Points;
    this->Ways = TeachData.Ways;
}

int TeachData_Robot::FromTable(QVector<QString> Keys, QVector<QVector<QVariant> > Data)
{
    if(Keys.size() != 18) return -1;
    QVector<RobotPoint> tData;
    QString tName;
    for(int i=0;i<Data.size();i++)
    {
        if(Data[i].size() != 18) return -1;
        RobotPoint tPoint;
        QString tPointName;
        for(int j=0;j<Keys.size();j++)
        {
            if(Keys[j] == "Name"     && Data[i][j].typeName() == QString("QString")) tPointName      = Data[i][j].value<QString>();
            if(Keys[j] == "X"        && Data[i][j].typeName() == QString("double"))  tPoint.Pose[0]  = Data[i][j].value<double>();
            if(Keys[j] == "Y"        && Data[i][j].typeName() == QString("double"))  tPoint.Pose[1]  = Data[i][j].value<double>();
            if(Keys[j] == "Z"        && Data[i][j].typeName() == QString("double"))  tPoint.Pose[2]  = Data[i][j].value<double>();
            if(Keys[j] == "RX"       && Data[i][j].typeName() == QString("double"))  tPoint.Pose[3]  = Data[i][j].value<double>();
            if(Keys[j] == "RY"       && Data[i][j].typeName() == QString("double"))  tPoint.Pose[4]  = Data[i][j].value<double>();
            if(Keys[j] == "RZ"       && Data[i][j].typeName() == QString("double"))  tPoint.Pose[5]  = Data[i][j].value<double>();
            if(Keys[j] == "speed"    && Data[i][j].typeName() == QString("double"))  tPoint.speed    = Data[i][j].value<double>();
            if(Keys[j] == "moveType" && Data[i][j].typeName() == QString("double"))  tPoint.moveType = Data[i][j].value<double>();
            if(Keys[j] == "smooth"   && Data[i][j].typeName() == QString("double"))  tPoint.smooth   = Data[i][j].value<double>();
            if(Keys[j] == "Pos1"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[0]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos2"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[1]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos3"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[2]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos4"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[3]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos5"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[4]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos6"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[5]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos7"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[6]  = Data[i][j].value<double>();
            if(Keys[j] == "Pos8"     && Data[i][j].typeName() == QString("double"))  tPoint.Pos[7]  = Data[i][j].value<double>();
        }
        if(!tPointName.contains("-"))
        {
            if(tData.size() == 1) SetPoint(tName,tData[0]);
            else if(tData.size() > 1) SetWay(tName,tData);
            tData.clear();
            tName = tPointName;
        }
        tData.push_back(tPoint);
    }
    if(tData.size() == 1) SetPoint(tName,tData[0]);
    else if(tData.size() > 1) SetWay(tName,tData);
    return 0;
}

void TeachData_Robot::ToTable(QVector<QString> &Keys, QVector<QVector<QVariant> > &Data)
{
    Keys.clear();
    Keys<<"Name"<<"X"<<"Y"<<"Z"<<"RX"<<"RY"<<"RZ"<<"speed"<<"moveType"<<"smooth"<<"Pos1"<<"Pos2"<<"Pos3"<<"Pos4"<<"Pos5"<<"Pos6"<<"Pos7"<<"Pos8";
    Data.clear();
    for(int i=0;i<Points.size();i++)
    {
        QVector<QVariant> tData;
        tData.push_back(QVariant::fromValue(Points[i].first));
        tData.push_back(QVariant::fromValue(Points[i].second.Pose[0]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pose[1]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pose[2]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pose[3]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pose[4]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pose[5]));
        tData.push_back(QVariant::fromValue(Points[i].second.speed));
        tData.push_back(QVariant::fromValue((double)Points[i].second.moveType));
        tData.push_back(QVariant::fromValue((double)Points[i].second.smooth));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[0]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[1]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[2]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[3]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[4]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[5]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[6]));
        tData.push_back(QVariant::fromValue(Points[i].second.Pos[7]));
        Data.push_back(tData);
    }
    for(int i=0;i<Ways.size();i++)
    {
        for(int j=0;j<Ways[i].second.size();j++)
        {
            QVector<QVariant> tData;
            tData.push_back(QVariant::fromValue(j==0?Ways[i].first:Ways[i].first + "-" + QString::number(j)));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pose[0]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pose[1]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pose[2]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pose[3]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pose[4]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pose[5]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].speed));
            tData.push_back(QVariant::fromValue((double)Ways[i].second[j].moveType));
            tData.push_back(QVariant::fromValue((double)Ways[i].second[j].smooth));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[0]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[1]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[2]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[3]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[4]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[5]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[6]));
            tData.push_back(QVariant::fromValue(Ways[i].second[j].Pos[7]));
            Data.push_back(tData);
        }
    }
    return;
}

void TeachData_Robot::SetPoint(QString Name, RobotPoint Point)
{
    int Index = 0;
    for(;Index < Points.size();Index++)
    {
        if(Points[Index].first == Name) break;
    }
    if(Index < Points.size() && Points[Index].first == Name)
    {
        Points[Index].second = Point;
    }
    else
    {
        Points.push_back(QPair<QString,RobotPoint>(Name,Point));
    }
    return;
}

void TeachData_Robot::RemovePoint(QString Name)
{
    int Index = 0;
    for(;Index < Points.size();Index++)
    {
        if(Points[Index].first == Name) break;
    }
    if(Index < Points.size() && Points[Index].first == Name)
    {
        Points.removeAt(Index);
    }
    return;
}

int TeachData_Robot::GetPoint(QString Name, RobotPoint &Point)
{
    int Index = 0;
    for(;Index < Points.size();Index++)
    {
        if(Points[Index].first == Name)
        {
            Point = Points[Index].second;
            return 0;
        }
    }
    return -1;
}

QList<QPair<QString, RobotPoint> > TeachData_Robot::GetPoints()
{
    return Points;
}

void TeachData_Robot::SetWay(QString Name, QVector<RobotPoint> Way)
{
    int Index = 0;
    for(;Index < Ways.size();Index++)
    {
        if(Ways[Index].first == Name) break;
    }
    if(Index < Ways.size() && Ways[Index].first == Name)
    {
        Ways[Index].second = Way;
    }
    else
    {
        Ways.push_back(QPair<QString,QVector<RobotPoint>>(Name,Way));
    }
    return;
}

void TeachData_Robot::RemoveWay(QString Name)
{
    int Index = 0;
    for(;Index < Ways.size();Index++)
    {
        if(Ways[Index].first == Name) break;
    }
    if(Index < Ways.size() && Ways[Index].first == Name)
    {
        Ways.removeAt(Index);
    }
    return;
}

TeachData_Robot &TeachData_Robot::operator =(const TeachData_Robot &TeachData)
{
    this->Points = TeachData.Points;
    this->Ways = TeachData.Ways;
    return *this;
}

int TeachData_Robot::GetWay(QString Name, QVector<RobotPoint> &Way)
{
    int Index = 0;
    for(;Index < Ways.size();Index++)
    {
        if(Ways[Index].first == Name)
        {
            Way = Ways[Index].second;
            return 0;
        }
    }
    return -1;
}

QList<QPair<QString,QVector<RobotPoint>>> TeachData_Robot::GetWays()
{
    return Ways;
}
