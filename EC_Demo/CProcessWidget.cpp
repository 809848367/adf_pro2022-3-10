#include "CProcessWidget.h"
#include "QiniOperator.h"
#include "QStyleFactory"
#include "DataAnalysis.h"
#include <QtXml/QtXml>
#include <QStyleFactory>
#include <QSizePolicy>
#include <QMessageBox>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QMenu>
#include <QFileDialog>
#include "MachineManager.h"
#include "DatabaseManager.h"
#include <QScrollBar>
#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 
CProcessWidget::CProcessWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	Init();
	InitWindow();
	InitConnection();
}

CProcessWidget::~CProcessWidget()
{
}

void CProcessWidget::Init()
{
	m_scrollBar = ui.treeView->verticalScrollBar();
	TreeviewInit();
	TableviewInit();
	InitMenu();
	StationInit();
	m_tempOperator = NULL;
	m_tempStep = NULL;
}

void CProcessWidget::InitWindow()
{
	
}

void CProcessWidget::InitConnection()
{
	connect(ui.Load_Proce_BTN, &QPushButton::clicked, this, &CProcessWidget::LoadProcessBtn);
	connect(ui.Refresh_BTN, &QPushButton::clicked, this, &CProcessWidget::RefreshProcessBtn);
	connect(ui.Save_As_BTN, &QPushButton::clicked, this, &CProcessWidget::SaveAsBtn);
	connect(ui.Save_BTN, &QPushButton::clicked, this, &CProcessWidget::SaveBtn);
	connect(ui.ReSort_BTN, &QPushButton::clicked, this, &CProcessWidget::ResortBtn);
	connect(ui.treeView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &CProcessWidget::selectionChangedTreeSlot);
	//选择tableview时的事件
	connect(ui.tableView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &CProcessWidget::selectionChangedTableSlot);
	connect(ui.tableView, &QTableView::customContextMenuRequested, this, &CProcessWidget::tableviewContextMenuSlot);
	connect(ui.treeView, &QTreeView::customContextMenuRequested, this, &CProcessWidget::treeviewContextMenuSlot);
	//Proc 
	connect(m_actTreeRun, &QAction::triggered, this, &CProcessWidget::on_actTreeRunSlot);
	connect(m_actTreeAdd, &QAction::triggered, this, &CProcessWidget::on_actTreeAddSlot);
	connect(m_actTreeInsert, &QAction::triggered, this, &CProcessWidget::on_actTreeInsertSlot);
	connect(m_actTreeDelete, &QAction::triggered, this, &CProcessWidget::on_actTreeDeleteSlot);
	connect(m_actTreeInvalid, &QAction::triggered, this, &CProcessWidget::on_actTreeInvalidSlot);
	connect(m_actTreeEnable, &QAction::triggered, this, &CProcessWidget::on_actTreeEnableSlot);
	connect(m_actTreeCopy, &QAction::triggered, this, &CProcessWidget::on_actTreeCopySlot);
	connect(m_actTreePaste, &QAction::triggered, this, &CProcessWidget::on_actTreePasteSlot);

	//Operator
	connect(m_actTabRun, &QAction::triggered, this, &CProcessWidget::on_actTabRunSlot);
	connect(m_actTabAdd, &QAction::triggered, this, &CProcessWidget::on_actTabAddSlot);
	connect(m_actTabInsert, &QAction::triggered, this, &CProcessWidget::on_actTabInsertSlot);
	connect(m_actTabDelete, &QAction::triggered, this, &CProcessWidget::on_actTabDeleteSlot);
	connect(m_actTabInvalid, &QAction::triggered, this, &CProcessWidget::on_actTabInvaildSlot);
	connect(m_actTabEnable, &QAction::triggered, this, &CProcessWidget::on_actTabEnableSlot);
	connect(m_actCopy, &QAction::triggered, this, &CProcessWidget::on_actTabCopySlot);
	connect(m_actPaste, &QAction::triggered, this, &CProcessWidget::on_actTabPasteSlot);

	//step connect...
	connect(m_actStepRun, &QAction::triggered, this, &CProcessWidget::on_actStepRunSlot);
	connect(m_actStepAdd, &QAction::triggered, this, &CProcessWidget::on_actStepAddSlot);
	connect(m_actStepInsert, &QAction::triggered, this, &CProcessWidget::on_actStepInsertSlot);
	connect(m_actStepDelete, &QAction::triggered, this, &CProcessWidget::on_actStepDeleteSlot);
	connect(m_actStepInvalid, &QAction::triggered, this, &CProcessWidget::on_actStepInvalidSlot);
	connect(m_actStepEnable, &QAction::triggered, this, &CProcessWidget::on_actStepEnableSlot);

	connect(m_actStepCopy, &QAction::triggered, this, &CProcessWidget::on_actStepCopySlot);
	connect(m_actStepPaste, &QAction::triggered, this, &CProcessWidget::on_actStepPasteSlot);



	connect(ui.Btn_Ensure, &QPushButton::clicked, this, &CProcessWidget::on_ensure_change_slot);
	connect(ui.Btn_Cancle, &QPushButton::clicked, this, &CProcessWidget::on_cancle_slot);

	connect(ui.ReStateProcBtn, &QPushButton::clicked, this, &CProcessWidget::ResetProc);
	//station choose
	connect(ui.Operator_Station_ComboBox, &QComboBox::currentTextChanged, this, &CProcessWidget::ChooseIndexChanged);
	connect(ui.EnsureStepBtn, &QPushButton::clicked, this, &CProcessWidget::on_EnsureStepBtnSlot);

	connect(ui.Operator_Type_ComboBox, &QComboBox::currentTextChanged, this, &CProcessWidget::OperatorTypeTextChanged);

	connect(ui.ProcExitBtn, &QPushButton::clicked, this, &CProcessWidget::ExitRunProc);


	//流程绑定 只需要绑定一个操作就行了
	foreach(CParentProcess * proc, theMachineManager.GetProcList())
	{
		connect(this, &CProcessWidget::SigOperator, proc, &CParentProcess::ExecOperatorSlot);
		connect(this, &CProcessWidget::SigStep, proc, &CParentProcess::ExecStepSlot);
		break;
	}
	//流程运行退出绑定
	foreach(CParentProcess * proc, theMachineManager.GetProcList())
	{
		connect(proc, &CParentProcess::SignalProcStart, this, &CProcessWidget::AddOneRunProc,Qt::UniqueConnection);
		connect(proc, &CParentProcess::SignalProcIdle, this, &CProcessWidget::DeleteOneRunProc, Qt::UniqueConnection);
		connect(proc, &CParentProcess::SignalProCurrentStep, this, &CProcessWidget::Slot_CurrentRunStep, Qt::UniqueConnection);
	}

}

void CProcessWidget::InitMenu()
{
	ui.treeView->setContextMenuPolicy(Qt::CustomContextMenu);
	m_treeMenu = new QMenu(ui.treeView);
	m_actTreeRun = new QAction("执行该流程");
	m_actTreeAdd = new QAction("添加新流程");
	m_actTreeInsert = new QAction("插入新流程");
	m_actTreeDelete = new QAction("删除该流程");
	m_actTreeInvalid = new QAction("禁用该流程");
	m_actTreeEnable = new QAction("启用该流程");
	m_actTreeCopy = new QAction("拷贝该流程");
	m_actTreePaste = new QAction("粘贴该流程");

	m_treeMenu->addAction(m_actTreeRun);
	m_treeMenu->addAction(m_actTreeAdd);
	m_treeMenu->addAction(m_actTreeInsert);
	m_treeMenu->addAction(m_actTreeDelete);
	m_treeMenu->addAction(m_actTreeInvalid);
	m_treeMenu->addAction(m_actTreeEnable);
	m_treeMenu->addAction(m_actTreeCopy);
	m_treeMenu->addAction(m_actTreePaste);

	m_StepMenu = new QMenu(ui.treeView);
	m_actStepRun = new QAction("执行该步骤");
	m_actStepAdd = new QAction("添加步骤");
	m_actStepInsert = new QAction("插入步骤");
	m_actStepDelete = new QAction("删除该步骤");
	m_actStepInvalid = new QAction("禁用该步骤");
	m_actStepEnable = new QAction("启用该步骤");
	m_actStepCopy = new QAction("拷贝该步骤");
	m_actStepPaste = new QAction("粘贴该步骤");



	m_StepMenu->addAction(m_actStepRun);
	m_StepMenu->addAction(m_actStepAdd);
	m_StepMenu->addAction(m_actStepInsert);
	m_StepMenu->addAction(m_actStepDelete);
	m_StepMenu->addAction(m_actStepInvalid);
	m_StepMenu->addAction(m_actStepEnable);
	m_StepMenu->addAction(m_actStepCopy);
	m_StepMenu->addAction(m_actStepPaste);



	ui.tableView->setContextMenuPolicy(Qt::CustomContextMenu);
	m_tableMenu		= new QMenu(ui.tableView);
	m_actTabRun		= new QAction("执行操作");
	m_actTabAdd		= new QAction("添加操作");
	m_actTabInsert	= new QAction("插入操作");
	m_actTabDelete	= new QAction("删除操作");
	m_actTabInvalid = new QAction("禁用操作");
	m_actTabEnable	= new QAction("启用操作");
	m_actCopy		= new QAction("拷贝操作");
	m_actPaste		= new QAction("粘贴操作");

	m_tableMenu->addAction(m_actTabRun);
	m_tableMenu->addAction(m_actTabAdd);
	m_tableMenu->addAction(m_actTabInsert);
	m_tableMenu->addAction(m_actTabDelete);
	m_tableMenu->addAction(m_actTabInvalid);
	m_tableMenu->addAction(m_actTabEnable);
	m_tableMenu->addAction(m_actCopy);
	m_tableMenu->addAction(m_actPaste);

}

//int CProcessWidget::LoadProcessXML(QString procXML)
//{
//
//	dataAnaly.clear();
//	bool bRet = dataAnaly.AnalysisProgramXml(procXML);
//	if (bRet)
//		return 0;
//	else
//		return -1;
//	return 0;
//}

void CProcessWidget::TreeviewInit()
{
	m_treeModel = new QStandardItemModel(ui.treeView);
	ui.treeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_treeModel->setHorizontalHeaderLabels(QStringList() << QString("流程模块"));
	ui.treeView->setModel(m_treeModel);
	ui.treeView->setStyleSheet(
		"QTreeView::item{  background-clip: margin; height: 25px;} \
		QTreeWidget::item:hover{}\
		QTreeView::item{selection-background-color:rgb(100,100,255)}} "
	);
	//ui.treeView->setRootIndex;
	//QHeaderView *verticalHeader = ui.treeView->header();
	//verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
	//verticalHeader->setDefaultSectionSize(24);
	ui.treeView->setStyle(QStyleFactory::create("windows"));

	//
	//QString processPath;
	//iniOperator.readStringValue("filePath", "ProcessPath", processPath);
	//LoadProcessXML(processPath);

	refreshTreeView();
}

void CProcessWidget::TableviewInit()
{
	ui.tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui.tableView->setSelectionMode(QAbstractItemView::SingleSelection);
	ui.tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tableView->setAlternatingRowColors(true);

	//ui.tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	m_tableModel = new QStandardItemModel(ui.tableView);
	//QList<QStandardItem*> rowList;
	//rowList << new QStandardItem("1") << new QStandardItem("1") << new QStandardItem("1")
	//	<< new QStandardItem("1") << new QStandardItem("1");
	//m_tableModel->appendRow(rowList);
	ui.tableView->setModel(m_tableModel);
	m_tableModel->setHorizontalHeaderLabels(QStringList() << "标识号" << "工站" << "名称" << "类型" << "点位" << "参数1" << "参数2" << "参数3" << "参数4" << "参数5");

	ui.tableView->horizontalHeader()->setStyleSheet(
		"QHeaderView::section {" "color: black;padding-left: 8px;border: 1px solid #6c6c6c;}");
	ui.tableView->verticalHeader()->hide();

	//ui.tableView->setColumnWidth(3, 100);
	//ui.tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	
	//ui.tableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Fixed);//对第0列单独设置固定宽度  
	//ui.tableView->setColumnWidth(3,200);//设置固定宽度
	//horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//所有列都扩展自适应宽度，填充充满整个屏幕宽度  
	//															   //horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents );//根据列内容来定列宽  
	//horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);//对第0列单独设置固定宽度  
	//setColumnWidth(0, 45);//设置固定宽度   

}

void CProcessWidget::StationInit()
{
	m_stationModel = new QStandardItemModel;
	ui.Operator_Station_ComboBox->setModel(m_stationModel);
	foreach(S_Total data, theDbManager.GetTotalInfo())
	{
		m_stationModel->appendRow(new QStandardItem(data.Name));
	}
	m_stationModel->appendRow(new QStandardItem("虚拟工站"));
	m_stationModel->appendRow(new QStandardItem("TCP"));
	m_stationModel->appendRow(new QStandardItem("ETHPLC"));

	m_stationModel->appendRow(new QStandardItem("Camera"));
	m_stationModel->appendRow(new QStandardItem("AlgoManager"));
	m_stationModel->appendRow(new QStandardItem("Lighting"));
	m_stationModel->appendRow(new QStandardItem("PLC"));

	m_operaModel = new QStandardItemModel;
	ui.Operator_Type_ComboBox->setModel(m_operaModel);
	ChooseIndexChanged("虚拟工站");
}

void CProcessWidget::refreshTreeView()
{
	if (m_scrollBar)
		m_treeViewScollValue = m_scrollBar->value();
	qDebug() <<"m_treeViewScollValue:"<< m_treeViewScollValue;


#pragma region recordSelectItem
	QModelIndex curIndex = ui.treeView->currentIndex();

	//如何使用 以前保存好的curIndex  刷新页面后再次定位
	int level = 0;
	QModelIndex parentIndex = curIndex.parent();
	//干脆存索引...
	QList<QModelIndex> indexList;
	indexList << curIndex;

	while (parentIndex.isValid())
	{
		indexList << parentIndex;
		parentIndex = parentIndex.parent();
	}

#pragma endregion recordSelectItem

#pragma region refresh_treeview_data

	QModelIndexList selected = ui.treeView->selectionModel()->selectedIndexes();

	m_treeModel->clear();
	dataAnaly.m_CurProgram;
	//m_treeModel->setHorizontalHeaderLabels(QStringList() << QString(dataAnaly.m_CurProgram.aliasName + "(" + dataAnaly.m_CurProgram.Identification + ")"));

	QStringList titleList = QStringList() << QString(dataAnaly.m_CurProgram.aliasName + "(" + dataAnaly.m_CurProgram.Identification + ")");
	m_treeModel->setHorizontalHeaderLabels(titleList);


	//创建item model
	foreach(ProcessInfo* info, dataAnaly.m_CurProgram.processInfoList)
	{
		QStandardItem* item = new QStandardItem(QString().sprintf("%02d", info->index) + "|" + info->aliasName + "|" + info->Identification);
		QList<QStandardItem*> list;
		foreach(StepInfo* sInfo, info->stepInfoList)
		{
			QStandardItem* item2 = new QStandardItem(QString().sprintf("%02d", sInfo->index) + "|" + sInfo->aliasName + "|" + sInfo->Identification);

			QList<QStandardItem*> oList;
			foreach(OperatorInfo * oInfo, sInfo->opeartorInfoList)
			{
				oList << new QStandardItem(QString().sprintf("%02d", oInfo->index) + "|" + oInfo->aliasName + "|" + oInfo->Identification);
			}
			item2->appendRows(oList);
			list << item2;
		}
		item->appendRows(list);
		m_treeModel->appendRow(item);
	}

	//颜色刷新
	for (int i = 0; i != dataAnaly.m_CurProgram.processInfoList.count(); ++i)
	{
		//流程变色
		ProcessInfo* info = dataAnaly.m_CurProgram.processInfoList.at(i);
		for (int j = 0; j != titleList.count(); ++j)
		{
			if (info->bUse == 1)
			{
				m_treeModel->item(i, j)->setBackground(QColor(Qt::white));
			}
			else if (info->bUse == 0)
				m_treeModel->item(i, j)->setBackground(QColor(Qt::darkRed));
			QModelIndex parent = QModelIndex();
			QStandardItem *curItem = m_treeModel->item(i, j);
			//step 
			for (int k = 0; k != dataAnaly.m_CurProgram.processInfoList.at(i)->stepInfoList.count(); ++k)
			{
				if (dataAnaly.m_CurProgram.processInfoList.at(i)->stepInfoList.at(k)->bUse == 1)
				{
					if (k %2 == 0)
						m_treeModel->item(i, j)->child(k, 0)->setBackground(QColor(Qt::white));
					else
						m_treeModel->item(i, j)->child(k, 0)->setBackground(QColor(Qt::lightGray));
				}
				else if (dataAnaly.m_CurProgram.processInfoList.at(i)->stepInfoList.at(k)->bUse == 0)
				{
					m_treeModel->item(i, j)->child(k, 0)->setBackground(QColor(Qt::darkRed));
				}
				StepInfo * stepInfo = dataAnaly.m_CurProgram.processInfoList.at(i)->stepInfoList.at(k);
				//操作表格刷新
				for (int h = 0; h != stepInfo->opeartorInfoList.count(); ++h)
				{
					OperatorInfo *oper = stepInfo->opeartorInfoList.at(h);
					if (oper->bUse == 1)
					{
						if (h%2==0)
							m_treeModel->item(i, j)->child(k, 0)->child(h, 0)->setBackground(QColor(Qt::white));
						else
							m_treeModel->item(i, j)->child(k, 0)->child(h, 0)->setBackground(QColor(Qt::lightGray));
					}
					else if (oper->bUse == 0)
					{
						m_treeModel->item(i, j)->child(k, 0)->child(h, 0)->setBackground(QColor(Qt::darkRed));
					}

				}
			}
		}

		//步骤变色
		//for (int k = 0; k != info->stepInfoList.count(); ++k)
		//{
		//	StepInfo* sInfo = info->stepInfoList.at(k);
		//	if (sInfo->bUse == 1)
		//	{
		//		for (int j = 0; j != titleList.count(); ++j)
		//			m_treeModel->item(k, j)->setBackground(QColor(Qt::white));
		//	}
		//	else if (sInfo->bUse == 0) {
		//		for (int j = 0; j != titleList.count(); ++j)
		//			m_treeModel->item(k, j)->setBackground(QColor(Qt::darkGray));
		//	}
		//	for (int h = 0; h != sInfo->opeartorInfoList.count(); ++h)
		//	{
		//		OperatorInfo * oInfo = sInfo->opeartorInfoList.at(h);
		//		if (oInfo->bUse == 1)
		//		{
		//			for (int j = 0; j != titleList.count(); ++j)
		//				m_treeModel->item(k, j)->setBackground(QColor(Qt::white));
		//		}
		//		else if (oInfo->bUse == 0) {
		//			for (int j = 0; j != titleList.count(); ++j)
		//				m_treeModel->item(h, j)->setBackground(QColor(Qt::darkGray));
		//		}
		//	}
		//}
	}

#pragma endregion refresh_treeview_data
	//没办法只能全部打开
	//ui.treeView->expandAll();

	//if (!selected.isEmpty())
	//{
	//	foreach(QModelIndex index, selected)
	//	{
	//		ui.treeView->selectionModel()->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
	//	}
	//}

#pragma region reposition item
	QStandardItem* findItem = NULL;
	while (indexList.size() > 0)
	{
		//QModelIndex iEnd = indexList.last();
		QModelIndex iEnd = indexList.at(indexList.size() - 1);
		//iEnd.row();
		if (findItem == NULL)
			findItem = m_treeModel->item(iEnd.row());
		else if (findItem->hasChildren())
		{
			//findItem->
			findItem = findItem->child(iEnd.row());
		}
		//qDebug() << "find item text:" << findItem->text();
		indexList.pop_back();

	}
	if (findItem != NULL)
	{
		QItemSelectionModel* m_treeSelectModel = ui.treeView->selectionModel();
		m_treeModel->indexFromItem(findItem);
		m_treeSelectModel->setCurrentIndex(m_treeModel->indexFromItem(findItem), QItemSelectionModel::SelectCurrent);
		//qDebug() << "find item text:" << findItem->text();

		//treeModel;
	}
#pragma endregion reposition item

	if (m_scrollBar)
		m_scrollBar->setValue(m_treeViewScollValue);
}

void CProcessWidget::Slot_CurrentRunStep(QString procName, int step)
{
 	if (procName == ui.RunProcComboBox->currentText())
 	{
		ui.StepLabel->setText(QString::number(step));
 	}
 	 
}

void CProcessWidget::LoadProcessBtn()
{
	QString fileName;
	fileName = QFileDialog::getOpenFileName(this, tr("文件对话框！"),
		"D:", tr("流程文件( *xml);;""*.*"));
	int iRet = theMachineManager.ReloadProcessXML(fileName);
	if (iRet != MachineManager::NoError)
	{
		QMessageBox::warning(this, "提示", "流程加载失败", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	refreshTreeView();
	theMachineManager.MsgRegister();
}

void CProcessWidget::RefreshProcessBtn()
{

}

void CProcessWidget::SaveAsBtn()
{
	QString fileName;
	fileName = QFileDialog::getOpenFileName(this, tr("文件对话框！"),
		"D:", tr("流程文件( *xml);;""*.*"));
	//if (m_recodPath.isEmpty())
	//{
	//	fileName = QFileDialog::getOpenFileName(this, tr("文件对话框！"),
	//		"D:", tr("流程文件( *xml);;""*.*"));
	//}
	//else
	//	fileName = QFileDialog::getOpenFileName(this, tr("文件对话框！"),
	//		m_recodPath, tr("流程文件( *xml);;""*.*"));

	//m_recodPath = fileName;
	//save proc to xml 
	dataAnaly.SaveProgramInfo(dataAnaly.m_CurProgram, fileName);

}

void CProcessWidget::ResortBtn()
{
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::SaveBtn()
{
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	QString processPath;
	iniOperator.readStringValue("filePath", "ProcessPath", processPath);
	dataAnaly.SaveProgramInfo(dataAnaly.m_CurProgram, processPath);
}

void CProcessWidget::selectionChangedTreeSlot(const QItemSelection &selected, const QItemSelection &deselected)
{

	DisableOperatorWidget();
	QItemSelectionModel* selections = ui.treeView->selectionModel();
	m_treeSelectModel = ui.treeView->selectionModel();
	QModelIndexList indexes = selections->selectedIndexes();
	QString treeSelectStr = "";

	foreach(QModelIndex index, indexes)
	{
		QStandardItem* item = m_treeModel->itemFromIndex(index);
		if (item)
		{
			qDebug() << "selectionChangedSlot" << item->text()
				<< "index.row()" << index.row() << "index.column"
				<< index.column();
			//根据treeview 选取的内容查找dataAnalysis 内容
			QString selectContent = item->text();

			m_tableModel->clear(); 			//m_tableModel->setHorizontalHeaderLabels(QStringList() << "工站" << "名称" << "类型"  			//	<< "描述" << "点位");
			m_tableModel->setHorizontalHeaderLabels(QStringList() << "标识号" << "工站" << "名称" << "类型" << "点位" << "参数1" << "参数2" << "参数3" << "参数4" << "参数5");

			if (!selectContent.contains("|"))
				return;
			QStringList strList = selectContent.split("|");
			QString getStr = strList.at(strList.count() - 1);
			treeSelectStr = strList.at(0);
			
			if (2 == getStr.count())
			{
				ui.Process_Name_lineEdit->setText(strList.at(1));
				ui.Process_Num_lineEdit->setText(strList.at(2));
				m_treeRowIndex = index.row();
				m_SelProcIdntify = strList.at(2);
				//m_treeSelectStr = strList.at(0);
				m_treeSelectStr = strList.at(strList.count() - 1);
				//ui.Process_Start_IO_ComboBox->setCurrentText("");
				//ui.Process_Pause_IO_ComboBox->setCurrentText("");
				//ui.Process_Stop_IO_ComboBox->setCurrentText("");
				//foreach(ProcessInfo * proc, dataAnaly.m_CurProgram.processInfoList)
				//{
				//	if (proc->aliasName == strList.at(1))
				//	{
				//		foreach(InputParam input, theDbManager.GetInput())
				//		{
				//			if (input.name == proc->startInput)
				//			{
				//				ui.Process_Start_IO_ComboBox->setCurrentText(proc->startInput);
				//			}
				//			if (input.name == proc->pauseInput)
				//			{
				//				ui.Process_Pause_IO_ComboBox->setCurrentText(proc->pauseInput);
				//			}
				//			if (input.name == proc->stopInput)
				//			{
				//				ui.Process_Stop_IO_ComboBox->setCurrentText(proc->stopInput);
				//			}
				//		}
				//	}
				//}

			}

			// fresh step info groubox
			else if (4 == getStr.count())
			{
				m_SelStepIdntify = getStr;
				m_treeRowIndex = index.row();

				ui.Step_Identify_lineEdit->setText(strList.at(2));
				ui.Step_Name_lineEdit->setText(strList.at(1));
				foreach(ProcessInfo * proc, dataAnaly.m_CurProgram.processInfoList)
				{
					foreach(StepInfo * step, proc->stepInfoList)
					{
						if (step->Identification == strList.at(2))
						{
							//ui.Step_Type_ComboBox->setCurrentText(step->stepType);
							m_SelProcIdntify = step->belongProc;
						}
					}
				}
			}


			if (getStr.count() != 4)
				return;


			//根据步骤号获取step 下 operator list 
			QList<OperatorInfo *> showData;
			foreach(ProcessInfo* pInfo, dataAnaly.m_CurProgram.processInfoList)
			{
				foreach(StepInfo * sInfo, pInfo->stepInfoList)
				{
					if (sInfo->Identification == getStr)
					//if (sInfo->aliasName == getStr)
					{
						showData = QList<OperatorInfo *>(sInfo->opeartorInfoList);
						if (sInfo->CodSupplement == 1)
							ui.Step_End_CodeSuport_CheckBox->setCheckState(Qt::CheckState::Checked);
						else
							ui.Step_End_CodeSuport_CheckBox->setCheckState(Qt::CheckState::Unchecked);

						m_tableModel->clear();
						refreshTableView(sInfo);
						return;
					}
				}
			}
		}
	}
}

void CProcessWidget::selectionChangedTableSlot(const QItemSelection &selected, const QItemSelection &deselected)
{
	QItemSelectionModel* selections = ui.tableView->selectionModel();
	m_tableSelectModel = ui.tableView->selectionModel();
	//m_tableSelectModel = ui.tableView->selectionModel();
	QModelIndexList indexes = selections->selectedIndexes();
	//QStandardItem* identifyItem = m_tableModel->itemFromIndex(indexes.at(0));
	//qDebug() <<"identifyItem"<< identifyItem->text();
	//m_tabSelectStr = identifyItem->text();


	m_tabSelectStr = m_tableModel->itemFromIndex(indexes.at(0))->text();
	m_tabSelectRowIndex =  ui.tableView->currentIndex().row();


	ui.Operator_Num_lineEdit->setText(m_tableModel->itemFromIndex(indexes.at(0))->text());
	ui.Operator_Name_lineEdit->setText(m_tableModel->itemFromIndex(indexes.at(2))->text());
	ui.Operator_Station_ComboBox->setCurrentText(m_tableModel->itemFromIndex(indexes.at(2))->text());
	ui.Operator_Pos_lineEdit->setText(m_tableModel->itemFromIndex(indexes.at(4))->text());

	AbleOperatorWidget();
	qDebug() << "identifyItem:" << m_tableModel->itemFromIndex(indexes.at(0))->text() << " ;"
		<< "stationName:" << m_tableModel->itemFromIndex(indexes.at(1))->text() << " ;"
		<< "stepName:" << m_tableModel->itemFromIndex(indexes.at(2))->text() << " ;"
		<< "类型:" << m_tableModel->itemFromIndex(indexes.at(3))->text() << " ;"
		<< "点位:" << m_tableModel->itemFromIndex(indexes.at(4))->text() << " ;"
		<< "参数1:" << m_tableModel->itemFromIndex(indexes.at(5))->text() << " ;"
		<< "参数2:" << m_tableModel->itemFromIndex(indexes.at(6))->text() << " ;"
		<< "参数3:" << m_tableModel->itemFromIndex(indexes.at(7))->text() << " ;"
		<< "参数4:" << m_tableModel->itemFromIndex(indexes.at(8))->text() << " ;"
		<< "参数5:" << m_tableModel->itemFromIndex(indexes.at(8))->text() << " ;";
	//
	ui.Operator_Station_ComboBox->setCurrentText(m_tableModel->itemFromIndex(indexes.at(1))->text());
	ui.Operator_Param_ineEdit_1->setText(m_tableModel->itemFromIndex(indexes.at(5))->text());
	ui.Operator_Param_ineEdit_2->setText(m_tableModel->itemFromIndex(indexes.at(6))->text());
	ui.Operator_Param_ineEdit_3->setText(m_tableModel->itemFromIndex(indexes.at(7))->text());
	ui.Operator_Param_ineEdit_4->setText(m_tableModel->itemFromIndex(indexes.at(8))->text());
	ui.Operator_Param_ineEdit_5->setText(m_tableModel->itemFromIndex(indexes.at(9))->text());

	ui.Operator_Type_ComboBox->setCurrentText(m_tableModel->itemFromIndex(indexes.at(3))->text());

	//ui.Operator_Station_ComboBox->setCurrentText()

	//foreach(QModelIndex index, indexes)
	//{
	//	QStandardItem* item = m_tableModel->itemFromIndex(index);
	//	if (item)
	//	{
	//		qDebug() << "selectionChangedSlot;" << item->text() << "index.row():"
	//			<< index.row() << "index.column:" << index.column();
	//	}
	//}

	//ui.Operator_Point_ComboBox->setCurrentText(m_tableModel->itemFromIndex(indexes.at(4))->text());
}

void CProcessWidget::tableviewContextMenuSlot(QPoint pos)
{
	m_tableMenu->exec(QCursor::pos());
}

void CProcessWidget::treeviewContextMenuSlot(QPoint pos)
{
	QModelIndex index = ui.treeView->currentIndex();
	int row = index.row();
	if (index.row() < 0)
	{
		m_treeMenu->exec(QCursor::pos());
		return;
	}

	QStandardItem* item = m_treeModel->itemFromIndex(ui.treeView->currentIndex());
	QString itemContentStr = item->text();
	m_SelTreeContent = itemContentStr;

	QStringList strList = itemContentStr.split("|");

	QString Identify = strList.at(strList.count() - 1);
	if (Identify.count() == 2)//proc click
	{
		//流程操作菜单
		m_treeMenu->exec(QCursor::pos());
	}
	else if (Identify.count() == 4)
	{
		//步骤操作菜单
		m_StepMenu->exec(QCursor::pos());
	}
	else if (Identify.count() == 6)
	{
		//具体操作菜单
	}



	qDebug() << itemContentStr;
}

void CProcessWidget::ResetProc()
{
	qDebug() << __FUNCTION__;
	theMachineManager.ResetCurrentState();
	theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Idle);
}

void CProcessWidget::AddOneRunProc(QString procName)
{
	//ui.RunProcComboBox->addItem(procName);
	m_runProcList.push_back(procName);
	RefreshRunProcCombox();
}

void CProcessWidget::DeleteOneRunProc(QString procName)
{
	if (m_runProcList.contains(procName))
	{
		//ui.RunProcComboBox->removeItem(procName);
		m_runProcList.removeOne(procName);
		RefreshRunProcCombox();
	}
}

void CProcessWidget::on_actTreeRunSlot()
{
	if (theMachineManager.isAtuo()==true)
	{
		QMessageBox::information(this, "警告", "请在非自动模式下启动流程");
		return;
	}

	qDebug() << "on_actTreeRunSlot";
	if (m_treeSelectStr.isEmpty())
		return;
	ProcessInfo* runPInfo;
	foreach(ProcessInfo* pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		if (pInfo->Identification == m_treeSelectStr)
		{
			runPInfo = pInfo;
			break;
		}
	}
	qDebug() << "执行流程名称：" << runPInfo->aliasName << ";"<< "执行流程编号：" << runPInfo->Identification;
	theMachineManager.StartProcByName(runPInfo->aliasName);
}

void CProcessWidget::on_actTreeAddSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2);
	//创建新的Proc
	QString procIdentify = dataAnaly.m_CurProgram.processInfoList.count();
	ProcessInfo* pInfo = new ProcessInfo();
	pInfo->aliasName = QString("%1").arg("newProc");
	//pInfo->processNum = e.attribute("processNum");
	pInfo->index = QString("%1").arg(idenx.toInt() + 1).toInt();
	//pInfo->Identification = QString("%1").arg(idntifyStr.toInt()+1);
	pInfo->Identification = QString().sprintf("%02d", idntifyStr.toInt() + 1);
	//添加一个步骤
	StepInfo * emptyStep = new StepInfo();
	emptyStep->aliasName = "emptyStep";
	emptyStep->belongProc = QString(pInfo->Identification);
	emptyStep->Identification = pInfo->Identification + "00";


	OperatorInfo* emptyOper = new OperatorInfo;
	emptyOper->aliasName = "new opera";
	emptyOper->belongStep = QString(emptyStep->Identification);
	emptyOper->Identification = emptyStep->Identification + "00";


	//emptyStep->
	emptyStep->opeartorInfoList.push_back(emptyOper);
	pInfo->stepInfoList.push_back(emptyStep);

	//插入
	dataAnaly.m_CurProgram.processInfoList.insert(idntifyStr.toInt() + 1, pInfo);
	dataAnaly.DataSort();

	m_treeModel->clear();
	refreshTreeView();
	//ui.treeView->expand(m_treeSelectModel->currentIndex());
	return;
}

void CProcessWidget::on_actTreeInsertSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2);
	//创建新的Proc
	QString procIdentify = dataAnaly.m_CurProgram.processInfoList.count();
	ProcessInfo* pInfo = new ProcessInfo();
	pInfo->aliasName = QString("%1").arg("newProc");
	//pInfo->processNum = e.attribute("processNum");
	pInfo->index = QString("%1").arg(idenx.toInt()).toInt();
	//pInfo->Identification = QString("%1").arg(idntifyStr.toInt()+1);
	pInfo->Identification = QString().sprintf("%02d", idntifyStr.toInt() + 1);
	//添加一个步骤
	StepInfo * emptyStep = new StepInfo();
	emptyStep->aliasName = "emptyStep";
	emptyStep->belongProc = QString(pInfo->Identification);
	emptyStep->Identification = pInfo->Identification + "00";


	OperatorInfo* emptyOper = new OperatorInfo;
	emptyOper->aliasName = "newOper";
	emptyOper->belongStep = QString(emptyStep->Identification);
	emptyOper->Identification = emptyStep->Identification + "00";


	//emptyStep->
	emptyStep->opeartorInfoList.push_back(emptyOper);
	pInfo->stepInfoList.push_back(emptyStep);

	//插入
	dataAnaly.m_CurProgram.processInfoList.insert(idntifyStr.toInt(), pInfo);
	//流程列表排序
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actTreeDeleteSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2);
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		if (idntifyStr == proc->Identification)
		{
			dataAnaly.m_CurProgram.processInfoList.removeAt(proc->index);
			break;
		}
	}
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actTreeInvalidSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2);
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		if (idntifyStr == proc->Identification)
		{
			proc->bUse = 0;
			break;
		}
	}
	//m_treeModel->clear();
	refreshTreeView();	
}

void CProcessWidget::on_actTreeEnableSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2);
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		if (idntifyStr == proc->Identification)
		{
			proc->bUse = 1;
			break;
		}
	}
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actTreeCopySlot()
{
	qDebug() << __FUNCTION__;
	//get opreator
	ProcessInfo* proInfo = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		if (pInfo->Identification == m_treeSelectStr)
		{
			proInfo = pInfo;
			findFlags = true;
			break;
		}
		if (findFlags)
			break;
	}
	if (!findFlags)
	{
		QMessageBox::information(this, "提示", "拷贝失败", QMessageBox::Yes, QMessageBox::No);
		return;
	}
	m_tempProc = proInfo;
}

void CProcessWidget::on_actTreePasteSlot()
{
	qDebug() << __FUNCTION__;
	if (m_tempProc == NULL)
		return;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		if (m_SelProcIdntify == pInfo->Identification)
		{
			findFlags = true;
			break;
		}
	}
	if (!findFlags)
		return;
	StepInfo* tempStep = new StepInfo;
	ProcessInfo* tempProc = new ProcessInfo;

	tempProc->labStr = m_tempProc->labStr;
	tempProc->aliasName = m_tempProc->aliasName;
	tempProc->index = m_tempProc->index;
	tempProc->Identification = m_tempProc->Identification;
	tempProc->bUse = m_tempProc->bUse;
	tempProc->startInput = m_tempProc->startInput;
	tempProc->pauseInput = m_tempProc->pauseInput;
	tempProc->stopInput = m_tempProc->stopInput;

	for (int i = 0; i < m_tempProc->stepInfoList.count();i++)
	{
		StepInfo* step = new StepInfo;
		step->labStr = m_tempProc->stepInfoList[i]->labStr;
		step->stepType = m_tempProc->stepInfoList[i]->stepType;
		step->aliasName = m_tempProc->stepInfoList[i]->aliasName;
		step->index = m_tempProc->stepInfoList[i]->index;
		step->Identification = m_tempProc->stepInfoList[i]->Identification;

		step->CodSupplement = m_tempProc->stepInfoList[i]->CodSupplement;
		step->belongProc = m_tempProc->stepInfoList[i]->belongProc;
		step->bUse = m_tempProc->stepInfoList[i]->bUse;
		step->opeartorInfoList = m_tempProc->stepInfoList[i]->opeartorInfoList;

		tempProc->stepInfoList.push_back(step);
	}

	if (m_treeRowIndex < dataAnaly.m_CurProgram.processInfoList.count())
		dataAnaly.m_CurProgram.processInfoList.insert(m_treeRowIndex+1, tempProc);
	else  dataAnaly.m_CurProgram.processInfoList.append(tempProc);

	dataAnaly.DataSort();
	refreshTreeView();
}

void CProcessWidget::on_actTabRunSlot()
{
	qDebug() << "on_actTabRunSlot" << "; " << m_tabSelectStr;
	//根据唯一标识获得操作对象

	OperatorInfo* execInfo = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo* sInfo, pInfo->stepInfoList)
		{
			foreach(OperatorInfo* oInfo, sInfo->opeartorInfoList)
			{
				if (m_tabSelectStr == oInfo->Identification)
				{
					execInfo = oInfo;
					findFlags = true;
					break;
				}
				if (findFlags)
					break;
			}
			if (findFlags)
				break;
		}
		if (findFlags)
			break;
	}
	//执行该步骤

	//connect(this, &MainWindow::SigOperator, &p,&ProcessRun::ExecOperator);
	emit SigOperator(execInfo);

}

void CProcessWidget::on_actTabAddSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__;
	m_tableSelectModel = ui.tableView->selectionModel();
	QModelIndexList indexes = m_tableSelectModel->selectedIndexes();
	if (indexes.count() == 0)
	{
		QModelIndex StepIndex = m_treeSelectModel->currentIndex();
		QStandardItem* item = m_treeModel->itemFromIndex(StepIndex);
		QString  stepText = item->text();
		QStringList strList = stepText.split("|");
		if (strList.size() < 3)//选择的操作名非法
			return;
		if (strList[2].length() != 4)//选择的并非步骤号
			return;
		OperatorInfo* newOper = new OperatorInfo;
		QString StepIdentify = strList[2];
		newOper->Identification = StepIdentify + "00";
		newOper->aliasName = "new opera";

		foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
		{
			foreach(StepInfo *step, proc->stepInfoList)
			{
				if (step->Identification == StepIdentify)
					step->opeartorInfoList.push_back(newOper);
				dataAnaly.DataSort();
				m_tableModel->clear();
				refreshTableView(step);
				refreshTreeView();
			}
		}

		return;
	}
		
	int row = indexes.at(0).row();
	QString name = m_tableModel->itemFromIndex(indexes.at(4))->text();
	QString identifyStr = m_tableModel->itemFromIndex(indexes.at(0))->text(); // 步骤标识

	OperatorInfo* newOper = new OperatorInfo;
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			foreach(OperatorInfo *oper, step->opeartorInfoList)
			{
				if (oper->Identification == identifyStr)
				{
					newOper->aliasName = "new opera";
					newOper->belongStep = oper->belongStep;
					step->opeartorInfoList.insert(row + 1, newOper);
					dataAnaly.DataSort();
					m_tableModel->clear();
					refreshTableView(step);
					break;
				}
			}
		}
	}
}

void CProcessWidget::on_actTabInsertSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__;
	m_tableSelectModel = ui.tableView->selectionModel();
	QModelIndexList indexes = m_tableSelectModel->selectedIndexes();
	if (indexes.count() <= 0)
		return;
	int row = indexes.at(0).row();
	QString name = m_tableModel->itemFromIndex(indexes.at(4))->text();
	QString identifyStr = m_tableModel->itemFromIndex(indexes.at(0))->text(); // 步骤标识

	OperatorInfo* newOper = new OperatorInfo;


	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			foreach(OperatorInfo *oper, step->opeartorInfoList)
			{
				if (oper->Identification == identifyStr)
				{
					newOper->aliasName = "new opera";
					newOper->belongStep = oper->belongStep;
					step->opeartorInfoList.insert(row, newOper);
					dataAnaly.DataSort();
					m_tableModel->clear();
					refreshTableView(step);
					break;
				}
			}
		}
	}
}

void CProcessWidget::on_actTabDeleteSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__;

	m_tableSelectModel = ui.tableView->selectionModel();
	QModelIndexList indexes = m_tableSelectModel->selectedIndexes();
	if (indexes.count() <= 0)
		return;
	int row = indexes.at(0).row();
	QString name = m_tableModel->itemFromIndex(indexes.at(4))->text();
	QString identifyStr = m_tableModel->itemFromIndex(indexes.at(0))->text(); // 步骤标识

	OperatorInfo* newOper = new OperatorInfo;


	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			foreach(OperatorInfo *oper, step->opeartorInfoList)
			{
				if (oper->Identification == identifyStr)
				{
					step->opeartorInfoList.removeAt(row);
					dataAnaly.DataSort();
					m_tableModel->clear();
					refreshTableView(step);
					break;
				}
			}
		}
	}
}

void CProcessWidget::on_actTabInvaildSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__;
	m_tableSelectModel = ui.tableView->selectionModel();
	QModelIndexList indexes = m_tableSelectModel->selectedIndexes();
	if (indexes.count() <= 0)
		return;
	int row = indexes.at(0).row();
	QString name = m_tableModel->itemFromIndex(indexes.at(4))->text();
	QString identifyStr = m_tableModel->itemFromIndex(indexes.at(0))->text(); // 步骤标识

	OperatorInfo* newOper = new OperatorInfo;


	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			foreach(OperatorInfo *oper, step->opeartorInfoList)
			{
				if (oper->Identification == identifyStr)
				{
					oper->bUse = 0;
					dataAnaly.DataSort();
					m_tableModel->clear();
					refreshTableView(step);
					for (int i = 0; i != indexes.count(); ++i)
						m_tableModel->item(row, i)->setBackground(QColor(Qt::darkGray));
					//m_tableModel->item(row, 0)->setBackground(QColor(Qt::green));
					break;
				}
			}
		}
	}
}

void CProcessWidget::on_actTabEnableSlot()
{
	m_tableSelectModel = ui.tableView->selectionModel();
	QModelIndexList indexes = m_tableSelectModel->selectedIndexes();
	if (indexes.count() <= 0)
		return;
	int row = indexes.at(0).row();
	QString name = m_tableModel->itemFromIndex(indexes.at(4))->text();
	QString identifyStr = m_tableModel->itemFromIndex(indexes.at(0))->text(); // 步骤标识

	OperatorInfo* newOper = new OperatorInfo;


	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			foreach(OperatorInfo *oper, step->opeartorInfoList)
			{
				if (oper->Identification == identifyStr)
				{
					oper->bUse = 1;
					dataAnaly.DataSort();
					m_tableModel->clear();
					refreshTableView(step);
					break;
				}
			}
		}
	}
}

void CProcessWidget::on_actTabCopySlot()
{
	//get opreator
	OperatorInfo* execInfo = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo* sInfo, pInfo->stepInfoList)
		{
			foreach(OperatorInfo* oInfo, sInfo->opeartorInfoList)
			{
				if (m_tabSelectStr == oInfo->Identification)
				{
					execInfo = oInfo;
					findFlags = true;
					break;
				}
			}
			if (findFlags)
				break;
		}
		if (findFlags)
			break;
	}
	if (!findFlags)
	{
		QMessageBox::information(this, "提示", "拷贝失败", QMessageBox::Yes, QMessageBox::No);
		return;
	}
	m_tempOperator = execInfo;

	//执行该步骤
	//connect(this, &MainWindow::SigOperator, &p,&ProcessRun::ExecOperator);
	//emit SigOperator(execInfo);
}

void CProcessWidget::on_actTabPasteSlot()
{
	if (m_tempOperator == NULL)
		return;
	//
	StepInfo* execStep = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo* sInfo, pInfo->stepInfoList)
		{
			if (m_SelStepIdntify == sInfo->Identification)
			{
				execStep = sInfo;
				findFlags = true;
				break;
			}
		}
		if (findFlags)
			break;
	}
	if (!findFlags)
		return;
	//
	//m_tabSelectStr;
	int index = m_tabSelectStr.right(2).toInt();
	OperatorInfo* opera = new OperatorInfo;
	opera->labStr = m_tempOperator->labStr;
	opera->station = m_tempOperator->station;
	opera->aliasName = m_tempOperator->aliasName;
	opera->oprtType = m_tempOperator->oprtType;
	opera->postion = m_tempOperator->postion;
	opera->param4 = m_tempOperator->param4;
	opera->param5 = m_tempOperator->param5;
	opera->param1 = m_tempOperator->param1;
	opera->param2 = m_tempOperator->param2;
	opera->param3 = m_tempOperator->param3;
	opera->index = m_tempOperator->index;
	opera->Identification = m_tempOperator->Identification;
	opera->Identification = m_tempOperator->Identification;
	opera->belongStep = m_tempOperator->belongStep;
	opera->bUse = m_tempOperator->bUse;

	execStep->opeartorInfoList.insert(m_tabSelectRowIndex, opera);
	m_tableModel->clear();
	refreshTableView(execStep);
}

void CProcessWidget::on_actStepRunSlot()
{
	//qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_treeSelectStr;

	qDebug() << "on_actStepRunSlot" << "; " << m_SelStepIdntify;
	//根据唯一标识获得操作对象

	StepInfo* execStep = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo* sInfo, pInfo->stepInfoList)
		{
			if (m_SelStepIdntify == sInfo->Identification)
			{
				execStep = sInfo;
				findFlags = true;
				break;
			}
		}
		if (findFlags)
			break;
	}
	if (!findFlags)
		return;
	emit SigStep(execStep);
}

void CProcessWidget::on_actStepAddSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2); // 步骤标识
	StepInfo *newStep = new StepInfo;
	//查找到当前的step 
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			if (step->Identification == idntifyStr)
			{
				newStep->aliasName = QString("%1").arg("newStep");
				newStep->belongProc = step->belongProc;
				//newStep->Identification = step->Identification;
				//newStep->Identification = QString("%1").arg(0,4, step->Identification.toInt() + 1) ;
				OperatorInfo* emptyOper = new OperatorInfo;
				emptyOper->aliasName = "newOper";
				emptyOper->belongStep = QString(newStep->Identification);
				emptyOper->Identification = newStep->Identification + "00";

				newStep->opeartorInfoList.push_back(emptyOper);
				//proc->stepInfoList.push_back(newStep);
				proc->stepInfoList.insert(step->index + 1, newStep);
				break;
			}
		}
	}
	QModelIndex curIndex = m_treeSelectModel->currentIndex();
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actStepInsertSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2); // 步骤标识
	StepInfo *newStep = new StepInfo;
	//查找到当前的step 
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			if (step->Identification == idntifyStr)
			{
				newStep->aliasName = QString("%1").arg("newStep");
				newStep->belongProc = step->belongProc;
				//newStep->Identification = step->Identification;
				//newStep->Identification = QString("%1").arg(0,4, step->Identification.toInt() + 1) ;
				OperatorInfo* emptyOper = new OperatorInfo;
				emptyOper->aliasName = "newOper";
				emptyOper->belongStep = QString(newStep->Identification);
				emptyOper->Identification = newStep->Identification + "00";

				newStep->opeartorInfoList.push_back(emptyOper);
				//proc->stepInfoList.push_back(newStep);
				proc->stepInfoList.insert(step->index, newStep);
				break;
			}
		}
	}
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actStepDeleteSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2); // 步骤标识
										//查找到当前的step 
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			if (step->Identification == idntifyStr)
			{
				proc->stepInfoList.removeAt(step->index);
				break;
			}
		}
	}
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actStepInvalidSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__ << " ;select item : " << m_SelTreeContent;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2); // 步骤标识
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			if (step->Identification == idntifyStr)
			{
				step->bUse = 0;
				break;
			}
		}
	}
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actStepEnableSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__;
	QStringList strList = m_SelTreeContent.split("|");
	QString idenx = strList.at(0);
	QString name = strList.at(1);
	QString idntifyStr = strList.at(2); // 步骤标识
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			if (step->Identification == idntifyStr)
			{
				step->bUse = 1;

				break;
			}
		}
	}
	dataAnaly.DataSort();
	//m_treeModel->clear();
	refreshTreeView();
}

void CProcessWidget::on_actStepCopySlot()
{

	StepInfo* copyStep = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo* sInfo, pInfo->stepInfoList)
		{
			if (sInfo->Identification == m_SelStepIdntify)
			{
				copyStep = sInfo;
				findFlags = true;
				break;
			}
		}
		if (findFlags)
			break;
	}
	if (!findFlags)
	{
		QMessageBox::information(this, "提示", "拷贝失败", QMessageBox::Yes, QMessageBox::No);
		return;
	}
	m_tempStep = copyStep;
}

void CProcessWidget::on_actStepPasteSlot()
{
	if (m_tempStep == NULL)
		return;

	ProcessInfo* TargetProcess = NULL;
	bool findFlags = false;
	foreach(ProcessInfo * pInfo, dataAnaly.m_CurProgram.processInfoList)
	{
		if (m_SelProcIdntify == pInfo->Identification)
		{
			TargetProcess = pInfo;
			findFlags = true;
			break;
		}
	}
	if (!findFlags)
		return;
	StepInfo* tempStep = new StepInfo;

	tempStep->labStr = m_tempStep->labStr;
	tempStep->stepType = m_tempStep->stepType;
	tempStep->aliasName = m_tempStep->aliasName;
	tempStep->index = m_tempStep->index;
	tempStep->Identification = m_tempStep->Identification;
	tempStep->CodSupplement = m_tempStep->CodSupplement;
	tempStep->belongProc = m_tempStep->belongProc;

	for (int i = 0; i < m_tempStep->opeartorInfoList.count();i++)
	{
		OperatorInfo* opera = new OperatorInfo;
		opera->labStr = m_tempStep->opeartorInfoList[i]->labStr;
		opera->station = m_tempStep->opeartorInfoList[i]->station;
		opera->aliasName = m_tempStep->opeartorInfoList[i]->aliasName;
		opera->oprtType = m_tempStep->opeartorInfoList[i]->oprtType;
		opera->postion = m_tempStep->opeartorInfoList[i]->postion;
		opera->param4 = m_tempStep->opeartorInfoList[i]->param4;
		opera->param5 = m_tempStep->opeartorInfoList[i]->param5;
		opera->param1 = m_tempStep->opeartorInfoList[i]->param1;
		opera->param2 = m_tempStep->opeartorInfoList[i]->param2;
		opera->param3 = m_tempStep->opeartorInfoList[i]->param3;
		opera->index = m_tempStep->opeartorInfoList[i]->index;
		opera->Identification = m_tempStep->opeartorInfoList[i]->Identification;
		opera->Identification = m_tempStep->opeartorInfoList[i]->Identification;
		opera->belongStep = m_tempStep->opeartorInfoList[i]->belongStep;
		opera->bUse = m_tempStep->opeartorInfoList[i]->bUse;

		tempStep->opeartorInfoList.push_back(opera);
	}
	qDebug() << "Past Step Target Proc:" << TargetProcess->Identification << endl;
	TargetProcess->stepInfoList.insert(m_treeRowIndex,tempStep);
	dataAnaly.DataSort();
	refreshTreeView();
}

void CProcessWidget::on_ensure_change_slot()
{
	QString op_identify = ui.Operator_Num_lineEdit->text();
	QString op_name = ui.Operator_Name_lineEdit->text();
	QString op_station = ui.Operator_Station_ComboBox->currentText();
	QString op_type = ui.Operator_Station_ComboBox->currentText();
	//QString op_name = ui.Operator_Station_ComboBox->currentText();
	//QString op_name = ui.Operator_Num_lineEdit->text();
	//find Operator
	foreach(ProcessInfo * proc, dataAnaly.m_CurProgram.processInfoList)
	{
		foreach(StepInfo *step, proc->stepInfoList)
		{
			foreach(OperatorInfo *oper, step->opeartorInfoList)
			{
				if (oper->Identification == ui.Operator_Num_lineEdit->text())
				{
					oper->aliasName = ui.Operator_Name_lineEdit->text();
					oper->station = ui.Operator_Station_ComboBox->currentText();
					oper->postion = ui.Operator_Pos_lineEdit->text();
					oper->oprtType = ui.Operator_Type_ComboBox->currentText();


					oper->param1 = ui.Operator_Param_ineEdit_1->text();
					oper->param2 = ui.Operator_Param_ineEdit_2->text();
					oper->param3 = ui.Operator_Param_ineEdit_3->text();
					oper->param4 = ui.Operator_Param_ineEdit_4->text();
					oper->param5 = ui.Operator_Param_ineEdit_5->text();
					break;
				}
			}
		}
	}
	refreshTreeView();
}

void CProcessWidget::on_cancle_slot()
{

}

void CProcessWidget::ChooseIndexChanged(QString content)
{
	qDebug() << content;
	//刷新点位列表
	m_operaModel->clear();
	if (content == "虚拟工站")
	{
		m_operaModel->clear();
		ui.Operator_Station_ComboBox->setCurrentText("虚拟工站");
		m_operaModel->appendRow(new QStandardItem("delay"));
		m_operaModel->appendRow(new QStandardItem("jumpIndex"));
		m_operaModel->appendRow(new QStandardItem("writeFlag"));
		m_operaModel->appendRow(new QStandardItem("WriteTempFlag"));
		m_operaModel->appendRow(new QStandardItem("waitFlag"));
		m_operaModel->appendRow(new QStandardItem("SelfAddFlag"));
		m_operaModel->appendRow(new QStandardItem("SelfMinuFlag"));
		m_operaModel->appendRow(new QStandardItem("FlagJump"));
		m_operaModel->appendRow(new QStandardItem("SwitchFlagJump"));
		m_operaModel->appendRow(new QStandardItem("MulityFlagLogicalAndJump"));
		m_operaModel->appendRow(new QStandardItem("MulityFlagLogicalORJump"));
		m_operaModel->appendRow(new QStandardItem("WaitAndSetFlag"));
		m_operaModel->appendRow(new QStandardItem("ClearAllFlag"));
		m_operaModel->appendRow(new QStandardItem("WriteTempString"));
		m_operaModel->appendRow(new QStandardItem("ReadTempString"));
		m_operaModel->appendRow(new QStandardItem("CreateSN"));
		m_operaModel->appendRow(new QStandardItem("GetSN"));
		m_operaModel->appendRow(new QStandardItem("startFlow"));
		m_operaModel->appendRow(new QStandardItem("waitFlowStart"));
		m_operaModel->appendRow(new QStandardItem("waitFlowEnd"));
		m_operaModel->appendRow(new QStandardItem("FlowEnd"));
		m_operaModel->appendRow(new QStandardItem("ShowInfo"));
		m_operaModel->appendRow(new QStandardItem("DeleteDir"));
		m_operaModel->appendRow(new QStandardItem("AddOneProduct"));
		m_operaModel->appendRow(new QStandardItem("AddOneOKProduct"));
		m_operaModel->appendRow(new QStandardItem("AddOneNGProduct"));
		m_operaModel->appendRow(new QStandardItem("ClearCount"));
		
		
		m_operaModel->appendRow(new QStandardItem("RefreshCTWithTowTimePoint"));
		m_operaModel->appendRow(new QStandardItem("CreateStartTime"));
		m_operaModel->appendRow(new QStandardItem("ElapsedFromTime"));
		m_operaModel->appendRow(new QStandardItem("ElapsedFromTwoTime"));
		
		m_operaModel->appendRow(new QStandardItem("msgBox"));
		m_operaModel->appendRow(new QStandardItem("TestDebug"));
		m_operaModel->appendRow(new QStandardItem("ErrorTestDebug"));
		m_operaModel->appendRow(new QStandardItem("ShowMulityTempImg"));
		m_operaModel->appendRow(new QStandardItem("ShowMulityTempImgWithResult"));
		return;
	}else
	
	if (content == "Robot2"|| content == "Robot1")
	{
		m_operaModel->appendRow(new QStandardItem("Robot_GetStatus"));
		m_operaModel->appendRow(new QStandardItem("Robot_MovePositionWithIMS"));
		m_operaModel->appendRow(new QStandardItem("Robot_MovePositionWithIMS6"));
		m_operaModel->appendRow(new QStandardItem("Robot_MovePosition"));
		m_operaModel->appendRow(new QStandardItem("Robot_Reset"));
		m_operaModel->appendRow(new QStandardItem("Robot_SetEnable"));
		m_operaModel->appendRow(new QStandardItem("Robot_WaitArrive"));
	}
	else if (content == "SerialPortManager")
	{
		m_operaModel->appendRow(new QStandardItem("SerialPortManager operator1"));
		m_operaModel->appendRow(new QStandardItem("SerialPortManager operator2"));
		m_operaModel->appendRow(new QStandardItem("SerialPortManager operator3"));
	}
	//else if (content == "LightingNG" || content == "LightingDown" || content == "LightingUp" 
	//	|| content == "Lighting2" || content == "Lighting1" )
	else if (content.contains("Lighting"))
	{
		m_operaModel->appendRow(new QStandardItem("Lighting_SetLight"));
		m_operaModel->appendRow(new QStandardItem("Lighting_SetAllLight"));
		m_operaModel->appendRow(new QStandardItem("Lighting_GetLight"));
		m_operaModel->appendRow(new QStandardItem("Lighting_GetAllLight"));
		m_operaModel->appendRow(new QStandardItem("Lighting_WaitLight"));
		m_operaModel->appendRow(new QStandardItem("Lighting_WaitAllLight"));
		m_operaModel->appendRow(new QStandardItem("Lighting_SetLightAndWait"));
		m_operaModel->appendRow(new QStandardItem("Lighting_SetAllLightAndWait"));
	}
	//else if (content == "CameraDown"|| content == "CameraUp" || content == "CameraNG"
	//	|| content == "Camera2" || content == "Camera1")
	else if (content.contains("Camera"))
	{
		m_operaModel->appendRow(new QStandardItem("Camera_Open"));
		m_operaModel->appendRow(new QStandardItem("Camera_Close"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetStatus"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImage"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImageAndSave"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImageSaveAndDisp"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImage2"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImageAndSave2"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImageAndSaveRecord"));
		m_operaModel->appendRow(new QStandardItem("Camera_GetImageAndSaveRecord2"));
	}
	//else if (content == "Conveyor2"|| content == "Conveyor1")
	else if (content.contains("Conveyor"))
	{
		m_operaModel->appendRow(new QStandardItem("Conveyor_Start"));
		m_operaModel->appendRow(new QStandardItem("Conveyor_Stop"));
		m_operaModel->appendRow(new QStandardItem("Conveyor_GetStatus"));
		m_operaModel->appendRow(new QStandardItem("Conveyor_SetEnable"));
		m_operaModel->appendRow(new QStandardItem("Conveyor_SetVel"));
		m_operaModel->appendRow(new QStandardItem("Conveyor_ClearErr"));
	}
	//else if (content == "PanaMotor2"|| content == "PanaMotor1")
	else if (content.contains("PanaMotor"))
	{
		m_operaModel->appendRow(new QStandardItem("PanaMotor_MovePos"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_Stop"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_WaitStop"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_GetPos"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_GetStatus"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_SetEnable"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_SetVel"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_WaitArrive"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_Reset"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_MoveOffset"));
		m_operaModel->appendRow(new QStandardItem("PanaMotor_ComparePos"));
		
	}
	else if (content == "IOControl")
	{
		m_operaModel->appendRow(new QStandardItem("IOControl_SetOutPut"));
		m_operaModel->appendRow(new QStandardItem("IOControl_SetOutPuts"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetInPut"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetInPuts"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetOutPut"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetOutPuts"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetIONames"));
		m_operaModel->appendRow(new QStandardItem("IOControl_WatiInput"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetInPutJump"));
		m_operaModel->appendRow(new QStandardItem("IOControl_GetInPutAndSetFlag"));
		m_operaModel->appendRow(new QStandardItem("IOControl_ContinueCheck"));

	}
	else if (content == "AlgoManager")
	{
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmd"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmdPOS"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmdSRC"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmdMOON"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmdIMS"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmdIMSMulity"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SendCmdIMSMulityADFOnePic"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_SetBarCode"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_ChcekFinish"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_WaitFinish"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_WaitResult"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_WaitResultSetFlag"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_WaitResultSetFlagADF"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_WaitResultSetFlagByResultInfoMulity"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_GetResult"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_GetAllResult"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_GetAllResultSetFlag"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_Complete"));
		m_operaModel->appendRow(new QStandardItem("AlgoManager_LoadGolden"));
	}
	else if(content.contains("EPSONRobot"))
	{
		//Operator_Type_EPSON_Robot_SendCmd,//发送命令
		//Operator_Type_EPSON_Robot_SendCmdAndRecv,
		//m_operaModel->appendRow(new QStandardItem("EPSONRobot_SendCmd"));
		m_operaModel->appendRow(new QStandardItem("EPSONRobot_SendAndRecv"));
		m_operaModel->appendRow(new QStandardItem("EPSONRobot_WaitRobotCmdSetFlag"));
	}
	else if (content == "PLC")
	{
		m_operaModel->appendRow(new QStandardItem("PLC_ReadPort"));
		m_operaModel->appendRow(new QStandardItem("PLC_WritePort"));
		m_operaModel->appendRow(new QStandardItem("PLC_WaitPort"));
	}
	else if (content == "ETHPLC")
	{
		m_operaModel->appendRow(new QStandardItem("ETHPLC_Connect"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_Disconnect"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_ReadPortInt"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WritePortInt"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_ReadPortFloat"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WritePortFloat"));

		m_operaModel->appendRow(new QStandardItem("ETHPLC_WriteDataCheck"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WaitPort"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WritePortAndWaitTimesInt"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WritePortAndWaitTimesFloat"));

		m_operaModel->appendRow(new QStandardItem("ETHPLC_CheckPortSetFlag"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_CheckPortIntSetFlag"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_CheckPortFloatSetFlag"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WriteTempDataMulityInt"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WriteTempDataMulityFloat"));
		m_operaModel->appendRow(new QStandardItem("ETHPLC_WriteTempPosToPLC"));
		m_operaModel->appendRow(new QStandardItem("ETH_PLC_ReadPortIntWriteToTempDataMulity"));
		m_operaModel->appendRow(new QStandardItem("ETH_PLC_ReadPortFloatWriteToTempDataMulity"));

		
	}
	else if (content.contains("TCP"))
	{
		m_operaModel->appendRow(new QStandardItem("TCP_ClientCreateTcpClientConnectServer"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientOperator_Type_WaitClientReConnect"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientSendDataToServer"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientSendTempDataToServer"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientSendTempDataToServerMulity"));
		m_operaModel->appendRow(new QStandardItem("TCP_ADFClientSendTempDataToServerMulity"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientSendAndRecv"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientWaitOneMsgFromSever"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientWaitOneMsgFromSeverEqualParam1"));
		m_operaModel->appendRow(new QStandardItem("TCP_ADFClientWaitOneMsgFromSeverShockPlate"));
		
		m_operaModel->appendRow(new QStandardItem("TCP_ClientDeleteOnClient"));
		m_operaModel->appendRow(new QStandardItem("TCP_ClientDataClear"));
		//server
		m_operaModel->appendRow(new QStandardItem("TCP_ServerCreateTcpServer"));
		m_operaModel->appendRow(new QStandardItem("TCP_ServerSendMsgToSkt"));
		m_operaModel->appendRow(new QStandardItem("TCP_ServerRecvMsgFromSkt"));
	}
}

void CProcessWidget::OperatorTypeTextChanged(QString curStr)
{
	qDebug() << curStr;
	RefreshHelpContent(curStr);


}

void CProcessWidget::AbleOperatorWidget()
{
	ui.Operator_Num_lineEdit->setEnabled(true);
	ui.Operator_Name_lineEdit->setEnabled(true);
	ui.Operator_Station_ComboBox->setEnabled(true);
	ui.Operator_Type_ComboBox->setEnabled(true);
	ui.Operator_Pos_lineEdit->setEnabled(true);
	ui.Operator_Param_ineEdit_1->setEnabled(true);
	ui.Operator_Param_ineEdit_2->setEnabled(true);
	ui.Operator_Param_ineEdit_3->setEnabled(true);
	ui.Operator_Param_ineEdit_4->setEnabled(true);
	ui.Operator_Param_ineEdit_5->setEnabled(true);
	ui.Btn_Ensure->setEnabled(true);
	ui.Btn_Cancle->setEnabled(true);
}

void CProcessWidget::DisableOperatorWidget()
{
	ui.Operator_Num_lineEdit->setEnabled(false);
	ui.Operator_Name_lineEdit->setEnabled(false);
	ui.Operator_Station_ComboBox->setEnabled(false);
	ui.Operator_Type_ComboBox->setEnabled(false);
	ui.Operator_Pos_lineEdit->setEnabled(false);
	ui.Operator_Param_ineEdit_1->setEnabled(false);
	ui.Operator_Param_ineEdit_2->setEnabled(false);
	ui.Operator_Param_ineEdit_3->setEnabled(false);
	ui.Operator_Param_ineEdit_4->setEnabled(false);
	ui.Operator_Param_ineEdit_5->setEnabled(false);
	ui.Btn_Ensure->setEnabled(false);
	ui.Btn_Cancle->setEnabled(false);
}

void CProcessWidget::ExitRunProc()
{
	QString procName =  ui.RunProcComboBox->currentText();
	theMachineManager.ResetProcByName(procName);
}

void CProcessWidget::RefreshRunProcCombox()
{
	ui.RunProcComboBox->clear();
	ui.RunProcComboBox->addItems(m_runProcList);
}

void CProcessWidget::refreshTableView(StepInfo * sInfo)
{
	QStringList titleList = QStringList() << "标识号" << "工站" << "名称" << "类型" << "点位" << "参数1" << "参数2" << "参数3" << "参数4" << "参数5";

	m_tableModel->setHorizontalHeaderLabels(titleList);
	for (int i = 0; i != sInfo->opeartorInfoList.count(); ++i)
	{
		QList<QStandardItem*> itemList;
		itemList << new QStandardItem(sInfo->opeartorInfoList.at(i)->Identification)
			<< new QStandardItem(sInfo->opeartorInfoList.at(i)->station) << new QStandardItem(sInfo->opeartorInfoList.at(i)->aliasName)
			<< new QStandardItem(sInfo->opeartorInfoList.at(i)->oprtType) << new QStandardItem(sInfo->opeartorInfoList.at(i)->postion)
			<< new QStandardItem(sInfo->opeartorInfoList.at(i)->param1) << new QStandardItem(sInfo->opeartorInfoList.at(i)->param2)
			<< new QStandardItem(sInfo->opeartorInfoList.at(i)->param3) << new QStandardItem(sInfo->opeartorInfoList.at(i)->param4) 
			<< new QStandardItem(sInfo->opeartorInfoList.at(i)->param5);


		m_tableModel->appendRow(itemList);
		if (sInfo->opeartorInfoList.at(i)->bUse)
		{
			for (int j = 0; j != itemList.count(); ++j)
				m_tableModel->item(i, j)->setBackground(QColor(Qt::white));
		}
		else
		{
			for (int j = 0; j != itemList.count(); ++j)
				m_tableModel->item(i, j)->setBackground(QColor(Qt::darkGray));
		}
	}
	ui.tableView->resizeColumnsToContents();
	//刷新 treeview 
	//dataAnaly.DataSort();
	//m_treeModel->clear();
	//refreshTreeView();
	//foreach(OperatorInfo * oInfo, sInfo->opeartorInfoList)
	//{
	//	QList<QStandardItem*> itemList;
	//	itemList << new QStandardItem(oInfo->Identification) << new QStandardItem(oInfo->station) << new QStandardItem(oInfo->aliasName)
	//		<< new QStandardItem(oInfo->oprtType) << new QStandardItem(oInfo->posNum)
	//		<< new QStandardItem(oInfo->descption1) << new QStandardItem(oInfo->descption2)
	//		<< new QStandardItem(oInfo->param1) << new QStandardItem(oInfo->param2) << new QStandardItem(oInfo->param3);
	//	m_tableModel->appendRow(itemList);
	//}
}


void CProcessWidget::RefreshHelpContent(QString op_type)
{
	if (op_type == "")
		return;
	ui.textEdit->setText(op_type);
	QString descriptionStr;
	if (op_type == "delay") { descriptionStr = "延时操作，param5 延时 ms"; }
	else if (op_type == "jumpIndex") { descriptionStr = "步骤跳转\n跳转到参数1"; }
	else if (op_type == "writeFlag") { descriptionStr = "写入标志位\n点位列表:标志位名称\n参数1:值"; }
	else if (op_type == "WriteTempFlag") { descriptionStr = "将存储整数型的临时变量写入flag,\n点位列表:flag名称\n参数1:临时变量名称"; }
	else if (op_type == "waitFlag") { descriptionStr = "等待标志位 \n点位列表:标志位名称\n参数1:值 \n参数5:超时"; }
	else if (op_type == "SelfAddFlag") { descriptionStr = "标志位自加1"; }
	else if (op_type == "SelfMinuFlag") { descriptionStr = "标志位自减1"; }
	
	else if (op_type == "FlagJump") { descriptionStr = "根据标志位跳转,\n标志位为1跳转参数1\n标志位为0跳转参数2\n"; }
	else if (op_type == "SwitchFlagJump") { descriptionStr = "根据标志位数值跳转\n点位列表:标志位名称\n参数1：标志位的值\n参数2：标志位对应值跳转步骤\n"; }

	else if (op_type == "MulityFlagLogicalAndJump") { descriptionStr = "多个标志位逻辑与运算根据结果跳转步骤\n参数1：标志位列表,分割\n参数2：结果为1跳转步骤\n参数3:结果为0跳转步骤\n"; }
	else if (op_type == "MulityFlagLogicalORJump") { descriptionStr = "多个标志位逻辑或运算根据结果跳转步骤\n参数1：标志位列表,分割\n参数2：结果为1跳转步骤\n参数3:结果为0跳转步骤\n"; }
	else if (op_type == "WaitAndSetFlag") { descriptionStr = "等待一个标志并且对其进行赋值"; }
	else if (op_type == "ClearAllFlag") { descriptionStr = "重置所有标志位"; }
	else if (op_type == "WriteTempString") { descriptionStr = "写入一个临时数据\n 点位列表:变量名称 \n参数1:变量值"; }
	else if (op_type == "ReadTempString") { descriptionStr = "转移一个临时变量到另外一个临时变量里面，\n参数2:原变量Key\n参数1:新变量key"; }
	else if (op_type == "CreateSN") { descriptionStr = "根据当前时间，创建一个SN"; }
	else if (op_type == "GetSN") { descriptionStr = "获得一个SN"; }
	else if (op_type == "startFlow") { descriptionStr = "流程开启 点位列表:流程名称"; }
	else if (op_type == "waitFlowStart") { descriptionStr = "等待流程开启 点位列表:流程名称"; }
	else if (op_type == "waitFlowEnd") { descriptionStr = "等待流程结束 点位列表:流程名称"; }
	else if (op_type == "FlowEnd") { descriptionStr = "流程主动结束 点位列表:流程名称"; }
	if (op_type == "AddOneProduct") { descriptionStr = "增加一个物料"; }
	else if (op_type == "AddOneOKProduct") { descriptionStr = "增加一个OK物料"; }
	else if (op_type == "AddOneNGProduct") { descriptionStr = "增加一个NG物料"; }
	else if (op_type == "ClearCount") { descriptionStr = "计数清零"; }
	else if (op_type == "ShowInfo") { descriptionStr = "消息显示到主页面消息栏\n参数1:显示消息内容 \n参数2:消息类型 \n0:正常消息 黑色 \n1:警告深蓝色\n2:错误深红色"; }
	else if (op_type == "DeleteDir") { descriptionStr = "删除文件夹保留距离现在一定小时的文件\n参数1:文件夹路径 \n参数2:小时数 ，0全部删除"; }
	else if (op_type == "RefreshCTWithTowTimePoint") { descriptionStr = "刷新物料CT,并且存储在临时变量中\n参数1:开始时间\n参数2:结束时间\n参数3:临时变量记录key值"; }

	else if (op_type == "msgBox") { descriptionStr = "弹框报警，流程卡顿 \n参数1:报警内容"; }
	else if (op_type == "CreateStartTime") { descriptionStr = "创建一个时间节点，\n 参数1：节点的key值 "; }
	else if (op_type == "ElapsedFromTime") { descriptionStr = "计算从某个时间到现在过去时间 \n参数1：存储时间key值，\n参数2:上一个时间节点"; }
	else if (op_type == "ElapsedFromTwoTime") { descriptionStr = "计算两个时间的时间差 \n参数1：存储时间key值，\n参数2:上一个时间节点\n参数3:上一个时间节点"; }
	else if (op_type == "TestDebug") { descriptionStr = "测试输出一段内容"; }
	else if (op_type == "ErrorTestDebug") { descriptionStr = "测试输出一段内容\n 触发异常"; }
	else if (op_type == "ShowMulityTempImg") { descriptionStr = "显示临时变量里面的多张图片参数1：以$符号分割"; }
	else if (op_type == "ShowMulityTempImgWithResult") { descriptionStr = "显示临时变量里面的多张图片且带结果\n参数1：图片路径，后缀自动携带 _ResultPath,\n参数2:结果 后缀自动携带_Result"; }

	else if (op_type == "Robot_GetStatus") { descriptionStr = "或者机械手状态"; }
	else if (op_type == "Robot_MovePositionWithIMS") { descriptionStr = ""; }
	else if (op_type == "Robot_MovePositionWithIMS6") { descriptionStr = ""; }
	
	if (op_type == "Robot_MovePosition") { descriptionStr = ""; }
	else if (op_type == "Robot_Reset") { descriptionStr = ""; }
	else if (op_type == "Robot_SetEnable") { descriptionStr = ""; }
	else if (op_type == "Robot_WaitArrive") { descriptionStr = ""; }
	else if (op_type == "SerialPortManager operator1") { descriptionStr = ""; }
	else if (op_type == "SerialPortManager operator2") { descriptionStr = ""; }
	else if (op_type == "SerialPortManager operator3") { descriptionStr = ""; }
	else if (op_type == "Lighting_SetLight") { descriptionStr = "设置灯条"; }
	else if (op_type == "Lighting_SetAllLight") { descriptionStr = ""; }
	else if (op_type == "Lighting_GetLight") { descriptionStr = ""; }
	else if (op_type == "Lighting_GetAllLight") { descriptionStr = ""; }
	else if (op_type == "Lighting_WaitLight") { descriptionStr = ""; }
	else if (op_type == "Lighting_WaitAllLight") { descriptionStr = ""; }
	else if (op_type == "Lighting_SetLightAndWait") { descriptionStr = ""; }
	else if (op_type == "Lighting_SetAllLightAndWait") { descriptionStr = ""; }
	else if (op_type == "Camera_Open") { descriptionStr = ""; }
	else if (op_type == "Camera_Close") { descriptionStr = ""; }
	else if (op_type == "Camera_GetStatus") { descriptionStr = ""; }
	else if (op_type == "Camera_GetImage") { descriptionStr = "拍照"; }
	else if (op_type == "Camera_GetImageAndSave") { descriptionStr = "拍照存图"; }
	else if (op_type == "Camera_GetImageSaveAndDisp") { descriptionStr = "拍照存图显示"; }
	else if (op_type == "Camera_GetImage2") { descriptionStr = ""; }
	else if (op_type == "Camera_GetImageAndSave2") { descriptionStr = ""; }
	else if (op_type == "Camera_GetImageAndSaveRecord") { descriptionStr = "拍照存图，路径保存在临时变量，\n标签为参数4。"; }
	else if (op_type == "Camera_GetImageAndSaveRecord2") { descriptionStr = ""; }
	else if (op_type == "Conveyor_Start") { descriptionStr = "皮带启动"; }
	else if (op_type == "Conveyor_Stop") { descriptionStr = "皮带停止"; }
	else if (op_type == "Conveyor_GetStatus") { descriptionStr = ""; }
	else if (op_type == "Conveyor_SetEnable") { descriptionStr = ""; }
	else if (op_type == "Conveyor_SetVel") { descriptionStr = ""; }
	else if (op_type == "Conveyor_ClearErr") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_MovePos") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_Stop") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_WaitStop") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_GetPos") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_GetStatus") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_SetEnable") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_SetVel") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_WaitArrive") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_Reset") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_MoveOffset") { descriptionStr = ""; }
	else if (op_type == "PanaMotor_ComparePos") { descriptionStr = ""; }
	else if (op_type == "IOControl_SetOutPut") { descriptionStr = "设置输出"; }
	else if (op_type == "IOControl_SetOutPuts") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetInPut") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetInPuts") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetOutPut") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetOutPuts") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetIONames") { descriptionStr = ""; }
	else if (op_type == "IOControl_WatiInput") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetInPutJump") { descriptionStr = ""; }
	else if (op_type == "IOControl_GetInPutAndSetFlag") { descriptionStr = ""; }
	else if (op_type == "IOControl_ContinueCheck") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_SendCmd") { descriptionStr = "发送命令"; }
	else if (op_type == "AlgoManager_SendCmdPOS") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_SendCmdSRC") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_SendCmdMOON") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_SendCmdIMS") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_SendCmdIMSMulity") { descriptionStr = "发送多张图到IMS，参数4：面号，参数5：图片路径"; }
	else if (op_type == "AlgoManager_SendCmdIMSMulityADFOnePic") {
		descriptionStr = "发送单张图片和点位给到IMS，分割符号 $,必须包含 Station,SN,Surface,三个key \n参数1:固定key名称；\n参数2:固定value值 \
	\n参数3:临时变量列表，名称做key，内容做值;必须有名称携带SN的临时变量 \n参数4:标志位值做value，以index做key名 \n参数5:图片路径 以参数名称做key ";
	}

	else if (op_type == "AlgoManager_SetBarCode") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_ChcekFinish") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_WaitFinish") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_WaitResult") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_WaitResultSetFlag") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_WaitResultSetFlagADF") { descriptionStr = "ADF使用，根据返回结果设置Flag,\n 临时变量 参数4_Point \
		 \n 临时变量 参数4_Result \n 临时变量 参数4_ResultPath  \n参数5:超时"; }
	else if (op_type == "AlgoManager_WaitResultSetFlagByResultInfoMulity") 
	{
		descriptionStr = "根据返回结果设置Flag值；\n点位列表：标志位名称\
			\n参数1：返回值 例如 PASS,ERROR,FAIL... \n参数2：对应标志位的值 1,2,3...\n参数3：SN \
			\n参数4:面号 \n生成临时变量，如果没有临时变量值为空或者0\n 临时变量 参数4_Point \n 临时变量 参数4_Result\n 临时变量 参数4_ResultPath  \
 			\n参数5:超时,0无限等待";
	}
	else if (op_type == "AlgoManager_GetResult") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_GetAllResult") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_GetAllResultSetFlag") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_Complete") { descriptionStr = ""; }
	else if (op_type == "AlgoManager_LoadGolden") { descriptionStr = ""; }
	else if (op_type == "EPSONRobot_SendAndRecv") { descriptionStr = ""; }
	else if (op_type == "EPSONRobot_WaitRobotCmdSetFlag") { descriptionStr = ""; }
	else if (op_type == "PLC_ReadPort") { descriptionStr = "读取PLC端口值\n参数3:端口号 \n参数4:写入的值"; }
	else if (op_type == "PLC_WritePort") { descriptionStr = "写入PLC端口值\n参数3:端口号 \n参数4:写入的值"; }
	else if (op_type == "PLC_WaitPort") { descriptionStr = "等待PLC端口值\n参数3:端口号 \n参数4:写入的值\n参数5:等待时间"; }

	else if (op_type == "ETHPLC_Connect") { descriptionStr = "链接PLC\n参数1:IP \n参数2:端口号\n 参数3:链接ID"; }
	else if (op_type == "ETHPLC_Disconnect") { descriptionStr = "断开PLC链接\n参数1:链接ID"; }
	else if (op_type == "ETHPLC_ReadPortInt") { descriptionStr = "读取PLC端口值\n参数3:端口号 \n参数4:临时变量key"; }
	else if (op_type == "ETHPLC_WritePortInt") { descriptionStr = "写入PLC端口值\n参数3:端口号 \n参数4:写入的值"; }

	else if (op_type == "ETHPLC_ReadPortFloat") { descriptionStr = "读取PLC端口值\n参数3:端口号 \n参数4:临时变量key"; }
	else if (op_type == "ETHPLC_WritePortFloat") { descriptionStr = "写入PLC端口值\n参数3:端口号 \n参数4:写入的值"; }

	else if (op_type == "ETHPLC_WriteDataCheck") { descriptionStr = "写入PLC端口值\n参数3:端口号 \n参数4:写入的值"; }

	else if (op_type == "ETHPLC_WaitPort") { descriptionStr = "等待PLC端口值为期待值\n参数3:端口号 \n参数4:值\n参数5:等待时间，0无线等待"; }
	else if (op_type == "ETHPLC_WritePortAndWaitTimesInt") { descriptionStr = "向PLC端口写入整数值并且读取验证\n参数1:端口号 \n参数2:值\n参数3:允许最大失败次数\n参数5:单次检测时间，0无线等待"; }
	else if (op_type == "ETHPLC_WritePortAndWaitTimesFloat") { descriptionStr = "向PLC端口写入浮点值并且读取验证\n参数1:端口号 \n参数2:值\n参数3:允许最大失败次数\n参数5:单次检测时间，0无线等待"; }


	else if (op_type == "ETHPLC_CheckPortSetFlag") { descriptionStr = "等待PLC端口值一定时间为期待值设置标志位\n点位列表:标志位名称;\n参数1:端口号 \n参数2:值\n参数3:时间内检测到信号，标志位的值\n参数4:时间内没检测到信号，标志位的值 \n参数5:等待时间，0无线等待"; }
	else if (op_type == "ETHPLC_CheckPortIntSetFlag") { descriptionStr = "等待PLC端口值一定时间为期待值设置标志位\n点位列表:标志位名称;\n参数1:端口号 \n参数2:值\n参数3:时间内检测到信号，标志位的值\n参数4:时间内没检测到信号，标志位的值 \n参数5:等待时间，0无线等待"; }
	else if (op_type == "ETHPLC_CheckPortFloatSetFlag") { descriptionStr = "等待PLC端口值一定时间为期待值设置标志位\n点位列表:标志位名称;\n参数1:端口号 \n参数2:值\n参数3:时间内检测到信号，标志位的值\n参数4:时间内没检测到信号，标志位的值 \n参数5:等待时间，0无线等待"; }
	else if (op_type == "ETHPLC_WriteTempDataMulityInt") { descriptionStr = "将整数型临时数据数据写入PLC \n参数1:临时变量,\n参数2:PLC端口号,多个端口号以英文逗号分割“,”"; }
	else if (op_type == "ETHPLC_WriteTempDataMulityFloat") { descriptionStr = "将小数型临时数据数据写入PLC \n参数1:临时变量,\n参数2:PLC端口号,多个端口号以英文逗号分割“,”"; }

	else if (op_type == "ETHPLC_WriteTempPosToPLC") { descriptionStr = "将临时数据将X,Y,U浮点数据写入PLC端口,端口以英文逗号分割\n点位列表:标志位名称 \
	\n参数1:临时变量,\n参数2:PLC端口号,多个端口号以英文逗号分割“,”\n参数3:成功标志位的值 \n参数4:失败标志位的值"; }
	else if (op_type == "ETH_PLC_ReadPortIntWriteToTempDataMulity") {
		descriptionStr = "将端口Int类型数据写入临时变量,端口以英文逗号分割\n参数1:临时变量,\n参数2:PLC端口号,多个端口号以英文逗号分割“,”";}
	else if (op_type == "ETH_PLC_ReadPortFloatWriteToTempDataMulity") {
		descriptionStr = "将端口Float类型数据写入临时变量,端口以英文逗号分割\n参数1:临时变量,\n参数2:PLC端口号,多个端口号以英文逗号分割“,”";
	}



	else if (op_type == "TCP_ClientCreateTcpClientConnectServer") { descriptionStr = "创建一个客户端连接服务器\n 点位列表:链接名称\n参数1:IP地址\n参数2:端口 "; }
	else if (op_type == "TCP_ClientOperator_Type_WaitClientReConnect") { descriptionStr = "等待客户端连接到服务器，否则退出 \n 点位列表:链接名称 "; }
	else if (op_type == "TCP_ClientSendDataToServer") { descriptionStr = "发送一段命令到服务器 \n 点位列表:链接名称 \n参数1:发送数据"; }
	else if (op_type == "TCP_ClientSendTempDataToServer") { descriptionStr = "发送临时变量到服务器\n 点位列表:链接名称 \n参数1:临时变量key"; }
	else if (op_type == "TCP_ClientSendTempDataToServerMulity") { descriptionStr = "发送多个临时变量到服务器 参数1：临时变量1$临时变量2$临时变量3"; }
	else if (op_type == "TCP_ADFClientSendTempDataToServerMulity") { descriptionStr = "ADF 专用发送多个临时变量到服务器\n 参数1：前面步骤创建的 临时变量1$临时变量2$临时变量3，变量后缀_Point、 _ResultPath、 _Result，\
	通过这些临时变量的值发送给机械手指令，成功发送结果，失败发送错误信息	"; }

	else if (op_type == "TCP_ClientSendAndRecv") { descriptionStr = "发送命令接收返回\n 点位列表:链接名称  \n参数1:发送指令 \n参数2:接收数据存储key"; }
	else if (op_type == "TCP_ClientWaitOneMsgFromSever") { descriptionStr = "等待服务器一条命令\n 点位列表:链接名称"; }
	else if (op_type == "TCP_ClientWaitOneMsgFromSeverEqualParam1") { descriptionStr = "等待服务器命令\n，点位列表:链接名称 \n 参数1:返回数据等于参数1\n参数2:\n参数3:\n参数4:\n参数5:超时检测"; }
	else if (op_type == "TCP_ClientDeleteOnClient") { descriptionStr = "删除客户端\n 点位列表:链接名称"; }
	else if (op_type == "TCP_ClientDataClear") { descriptionStr = "删除一个Tcp链接的数据\n 点位列表:链接名称"; }

	else if (op_type == "TCP_ADFClientWaitOneMsgFromSeverShockPlate") { descriptionStr = "获取指定客户端返回数据 \n 点位列表:链接名称 \n参数1:标志位名称\n参数2:返回数据 比较内容 一般设置为OK,NG1,NG2...\n参数3:对应标志位值\n参数4:成功后数据存放key名称\n参数5:超时时间"; }

	else if (op_type == "TCP_ServerCreateTcpServer") { descriptionStr = "创建一个服务器 \n 点位列表:服务器名称 \n参数1:端口\n参数2:最大链接数"; }
	else if (op_type == "TCP_ServerSendMsgToSkt") { descriptionStr = "发送数据到指定客户端 \n 点位列表:链接名称\n参数1:发送内容"; }
	else if (op_type == "TCP_ServerRecvMsgFromSkt") { descriptionStr = "获取指定客户端返回数据 \n 点位列表:链接名称 "; }
	
	ui.textEdit->setText(op_type + ":" + descriptionStr);
}

void CProcessWidget::on_EnsureStepBtnSlot()
{
	qDebug() << "当前函数名		__func__		:" << __func__;
	QString procIdentify = ui.Process_Num_lineEdit->text();


	foreach(ProcessInfo * proc, dataAnaly.m_CurProgram.processInfoList)
	{
		if (procIdentify == proc->Identification)
		{
			proc->aliasName = ui.Process_Name_lineEdit->text();
		}

		foreach(StepInfo *step, proc->stepInfoList)
		{
			if (step->Identification == ui.Step_Identify_lineEdit->text())
			{
				step->aliasName = ui.Step_Name_lineEdit->text();
				//step->stepType = ui.Step_Type_ComboBox->currentText();
			}
		}
	}
	//m_treeModel->clear();
	refreshTreeView();
}