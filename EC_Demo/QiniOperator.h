#pragma once

#include <QString>
#include<QSettings>
#define FILE_NAEE_PATHMANAGER	(QCoreApplication::applicationDirPath()+"/"+"pathManager.ini")
//#define FILE_NAEE_PATHMANAGER	(QCoreApplication::applicationDirPath()+"/"+"APP.ini")
#define LOCAL_INI_PATH "./../Debug/local.ini"


class QiniOperator
{
public:
	QiniOperator();
	~QiniOperator();
	bool isLocalIniExist(QString filePath);
	//下面两函数搭配使用，判断当前路径下是否有文件存在。
	void setPath(QString curPath);
	bool isFileExist();

	void readIntValue(QString section, QString key, int & value);
	void readStringValue(QString section, QString key, QString & value);
	void readDoubleValue(QString section, QString key, double & value);
	void readBoolValue(QString section, QString key, bool & value);
	void readData(QString section,QString key,QVariant& value);


	void setValue(QString section, QString key, QVariant value);
	//QiniOperator* getInstance();
	//void DestoryInstance(QObject *parent);

private:
	QString m_curPath;
};
extern QiniOperator iniOperator;
