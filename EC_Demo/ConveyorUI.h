﻿#ifndef ConveyorUI_H
#define ConveyorUI_H

#include <QWidget>
#include <QtCore>
#include "HardwareManager.h"


namespace Ui {
class ConveyorUI;
}

class ConveyorUI : public QWidget
{
    Q_OBJECT

public:
    explicit ConveyorUI(QWidget *parent = nullptr);
    ~ConveyorUI();
    void SetHardwareManager(HardwareManager* HWM,QString Station);
private:
    Ui::ConveyorUI *ui;
    HardwareManager* HWM;
    QString Station;
    QTimer RefreshTimer;
private slots:
    void on_Start_clicked();
    void on_Stop_clicked();
    void on_Enable_clicked();
    void on_Disable_clicked();
    void on_ClearError_clicked();
    void RefreshTimerSlot();
};

#endif // ConveyorUI_H
