﻿#ifndef SERIALPORTMANAGER_H
#define SERIALPORTMANAGER_H

#include <QtCore>
#include "HardwareModule.h"
#include "SerialPort.h"

class SerialPortManager : public HardwareModule
{
    Q_OBJECT
public:
    explicit SerialPortManager(QObject *parent = nullptr);
    ~SerialPortManager();
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    //初始化
    CtrlError Init(QString Name, QList<SerialPort::SerialPortConfig> Configs);
    //同步模式
    CtrlError SendAndRecv(QString Com, QByteArray Cmd, QByteArray &Res, int Priority = 1);
    //异步模式
    CtrlError Send(QString Com, QByteArray Cmd, QString &ID, int Priority = 1);
    CtrlError Recv(QString ID,QByteArray &Res);
    CtrlError WaitforRecv(QString ID, QByteArray &Res);
private:
    QString Name;
    QMap<QString,SerialPort*> SerialList;
    QMap<QString,QByteArray> RecvData;
    QMutex RecvDataLock;
signals:
public slots:
    void RecvMsg(QString ID, QByteArray Msg);
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00300000,
        ExecuteNotAllowed   = Base + 0x001,
        SerialNotExist      = Base + 0x002,
        RecvDataNotExist    = Base + 0x003,
        InitDataErr         = Base + 0x004,

    };
    Q_ENUM(ErrorCode)
};

#endif // SERIALPORTMANAGER_H
