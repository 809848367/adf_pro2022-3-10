﻿#include "SerialPort.h"
#include <thread>
SerialPort::SerialPort(QObject *parent) : QObject(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    connect(this,SIGNAL(SendAndRecvMsg(QByteArray,int)),this,SLOT(SendAndRecvMsgSlot(QByteArray,int)),Qt::BlockingQueuedConnection);
    Serial = new QSerialPort();
    threadFlag = 0;
    SendList1.clear();
    SendList2.clear();
    std::thread t(&SerialPort::thread_Send,this);
    t.detach();
}

SerialPort::~SerialPort()
{
    threadFlag = 3;
    while(threadFlag == 3) QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

CtrlError SerialPort::Open(SerialPort::SerialPortConfig Config)
{
    this->Config = Config;
    //重复打开
    if(Serial->isOpen()) return CtrlError(SerialPortHasOpen,Config.COM);

    Serial->setPortName(Config.COM);
    //打开串口
    if(!Serial->open(QSerialPort::ReadWrite)) return CtrlError(SerialPortOpenFail,Config.COM);

    //设置参数
    if(!Serial->setBaudRate((QSerialPort::BaudRate)Config.BaudRate,QSerialPort::AllDirections) ||
       !Serial->setDataBits((QSerialPort::DataBits)Config.DataBits) ||
       !Serial->setFlowControl((QSerialPort::FlowControl)Config.FlowControl)||
       !Serial->setParity((QSerialPort::Parity)Config.Parity) ||
       !Serial->setStopBits((QSerialPort::StopBits)Config.StopBits))
    {
        Serial->clear();
        Serial->close();
        return CtrlError(SerialPortSetParaErr,Config.COM);
    }
    threadFlag = 1;
    return CtrlError(NoError);
}

CtrlError SerialPort::Stop()
{
    threadFlag = 2;
    while(threadFlag == 2) QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    if(!Serial->isOpen()) return CtrlError(SerialPortNotOpen,Config.COM);
    Serial->clear();
    Serial->close();
    return CtrlError(NoError);
}

CtrlError SerialPort::SendMsg(QByteArray Msg, QString &ID, int Priority)
{
    tIDLock.lock();
    int tID = this->tID;
    if(++this->tID > 1000000) this->tID = 0;
    tIDLock.unlock();
    if(!Serial->isOpen()) return CtrlError(SerialPortNotOpen,Config.COM);
    if(SendList1.size() + SendList2.size() > 1000000) return CtrlError(TempIsTooBig);
    ID = Config.COM+QString::number(tID);
    if(Priority == 1)
    {
        Send1.lock();
        SendList1.push_back(QPair<int,QByteArray>(tID,Msg));
        Send1.unlock();
    }
    else if(Priority == 2)
    {
        Send2.lock();
        SendList2.push_back(QPair<int,QByteArray>(tID,Msg));
        Send2.unlock();
    }
    else return CtrlError(PriorityError);
    return CtrlError(NoError);
}

void SerialPort::thread_Send()
{
    int ID;
    QByteArray Data;
    while(1)
    {
        //无数据发送时，10ms一次，防止卡机
        if(SendList1.empty() && SendList2.empty()) std::this_thread::sleep_for(std::chrono::milliseconds(10));
        //关闭模式
        if(threadFlag == 3)
        {
            threadFlag = 4;
            return;
        }
        if(threadFlag == 2) threadFlag = 0;
        if(threadFlag == 0) continue;
        //发送数据
        if(threadFlag == 1)
        {
            //检查高优先级是否有数据发送
            if(!SendList1.isEmpty())
            {
                Send1.lock();
                ID = SendList1.first().first;
                Data = SendList1.first().second;
                SendList1.removeFirst();
                Send1.unlock();
            }
            //检查低优先级是否有数据发送
            else if(!SendList2.isEmpty())
            {
                Send2.lock();
                ID = SendList2.first().first;
                Data = SendList2.first().second;
                SendList2.removeFirst();
                Send2.unlock();
            }
            //无数据要发送，直接下一轮
            else continue;
            //发送数据
            emit SendAndRecvMsg(Data,ID);
        }
    }
}

void SerialPort::SendAndRecvMsgSlot(QByteArray Msg, int ID)
{
    //LogManager::GetInstance().WriteLog("ModuleBus",Config.COM,"Send:"+Msg.toHex());
    Serial->write(Msg);
	if (!Serial->waitForBytesWritten(timeout))
		return;
    QByteArray Recv;
    while(true)
    {
        Serial->waitForReadyRead(timeout);
        if(Serial->bytesAvailable() > 0)
        {
            Recv.append(Serial->readAll());
            if(Config.Type == "Modbus")
            {
                if(Recv.size() < 2) continue;
                if(int(Recv[1]) == Msg[1]+0x80 && Recv.size() < 5) continue;
                if(Recv[1] == Msg[1])
                {
                    if(int(Recv[1]) == 0x01 && Recv.size() < 6) continue;
                    if(int(Recv[1]) == 0x03 && Recv.size() < 3) continue;
                    if(int(Recv[1]) == 0x03 && Recv.size() < 5 + Recv[2]) continue;
                    if(int(Recv[1]) == 0x05 && Recv.size() < 8) continue;
                    if(int(Recv[1]) == 0x06 && Recv.size() < 8) continue;
                    if(int(Recv[1]) == 0x10 && Recv.size() < 8) continue;
                }
            }
            else if(Config.Type == "Lighting")
            {
                if(Msg.size() == 7 && Recv.size() < 1) continue;
                if(Msg.size() == 3 && Recv.size() < 5) continue;
            }
        }
        emit RecvMsg(Config.COM+QString::number(ID),Recv);
        //LogManager::GetInstance().WriteLog("ModuleBus",Config.COM,"Recv:"+Recv.toHex());
        break;
    }
}






















