#include "TestBox.h"
msgBox::msgBox(QObject *parent) :QObject(parent)
{

}

void msgBox::readyShow()
{
	this->moveToThread(qApp->thread());
	QTimer::singleShot(0, this, SLOT(onShow()));
}

msgBox::msgBox(const QString &title, const QString &msg, const int type) :title(title), msg(msg), type(type)
{

}

void msgBox::show(const QString &title, const QString &msg, const int type)
{
	QEventLoop eventLoop;
	auto messageBox = new msgBox(title, msg, type);
	connect(messageBox, SIGNAL(destroyed()), &eventLoop, SLOT(quit()));
	messageBox->readyShow();
	eventLoop.exec();
}

void msgBox::onShow()
{
	QMessageBox::StandardButton result;
	switch (type)
	{
	case 1:
		result = QMessageBox::information(NULL, title, msg, QMessageBox::Yes | QMessageBox::No);
		break;
	case 2:
		result = QMessageBox::critical(NULL, title, msg, QMessageBox::Yes | QMessageBox::No);
		break;
	case 3:
		//QMessageBox::question(NULL, title, msg, QString::fromLocal8Bit("ok"));
		result = QMessageBox::question(NULL, title, msg, QMessageBox::Yes | QMessageBox::No);
		break;
	}
	//QMessageBox::StandardButton result = QMessageBox::information(nullptr,"��ʾ",op->param1,QMessageBox::Yes| QMessageBox::No);
	this->deleteLater();
}

