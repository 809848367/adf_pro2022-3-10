﻿#include "RobotUI.h"
#include "ui_RobotUI.h"
#include <QMessageBox>
#include <QInputDialog>
RobotUI::RobotUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RobotUI)
{
    ui->setupUi(this);
    HWM = nullptr;
    isReseting = false;
    InitTeachDataList();
    ShowTeachDataFlag = false;
    Pos_Add_Group.addButton(ui->X_Add,0);
    Pos_Add_Group.addButton(ui->Y_Add,1);
    Pos_Add_Group.addButton(ui->Z_Add,2);
    Pos_Add_Group.addButton(ui->RX_Add,3);
    Pos_Add_Group.addButton(ui->RY_Add,4);
    Pos_Add_Group.addButton(ui->RZ_Add,5);
    Pos_Minus_Group.addButton(ui->X_Minus,0);
    Pos_Minus_Group.addButton(ui->Y_Minus,1);
    Pos_Minus_Group.addButton(ui->Z_Minus,2);
    Pos_Minus_Group.addButton(ui->RX_Minus,3);
    Pos_Minus_Group.addButton(ui->RY_Minus,4);
    Pos_Minus_Group.addButton(ui->RZ_Minus,5);
    MoveBtn_Group.addButton(ui->X_MoveBtn,0);
    MoveBtn_Group.addButton(ui->Y_MoveBtn,1);
    MoveBtn_Group.addButton(ui->Z_MoveBtn,2);
    MoveBtn_Group.addButton(ui->RX_MoveBtn,3);
    MoveBtn_Group.addButton(ui->RY_MoveBtn,4);
    MoveBtn_Group.addButton(ui->RZ_MoveBtn,5);
    TeachDataRadio.addButton(ui->TeachPointBtn,0);
    TeachDataRadio.addButton(ui->TeachWayBtn,1);
    connect(&Pos_Add_Group,SIGNAL(buttonClicked(int)),this,SLOT(Pos_Add_clicked(int)));
    connect(&Pos_Minus_Group,SIGNAL(buttonClicked(int)),this,SLOT(Pos_Minus_clicked(int)));
    connect(&MoveBtn_Group,SIGNAL(buttonClicked(int)),this,SLOT(MoveBtn_clicked(int)));
    connect(&RefreshTimer,SIGNAL(timeout()),this,SLOT(RefreshSlot()));
    connect(&TeachDataBtns,SIGNAL(buttonClicked(int)),this,SLOT(TeachDataBtns_clicked(int)));
    connect(&TeachDataRadio,SIGNAL(buttonClicked(int)),this,SLOT(TeachDataRadio_clicked(int)));
    RefreshTimer.start(100);
    ui->TeachPointBtn->setChecked(true);
    TeachDataRadio_clicked(0);
}

RobotUI::~RobotUI()
{
    delete ui;
}

void RobotUI::SetHardwareManager(HardwareManager *HWM, QString Station)
{
    this->HWM = HWM;
    this->Station = Station;
}

void RobotUI::InitTeachDataList()
{
    ui->TeachData->setColumnCount(10);
    ui->TeachData->horizontalHeader()->setVisible(true);
    ui->TeachData->horizontalHeader()->setFixedHeight(30);
    QList<QString> HItems;
    HItems<<"X"<<"Y"<<"Z"<<"RX"<<"RY"<<"RZ"<<"speed"<<"moveType"<<"smooth"<<"";
    ui->TeachData->setHorizontalHeaderLabels(HItems);
    ui->TeachData->verticalHeader()->setVisible(true);
    ui->TeachData->verticalHeader()->setFixedWidth(30);
    for(int i=0;i<6;i++) ui->TeachData->setColumnWidth(i,100);
    for(int i=6;i<9;i++) ui->TeachData->setColumnWidth(i,100);
    for(int i=9;i<10;i++) ui->TeachData->setColumnWidth(i,50);
    for(int i=0;i<6;i++) ui->TeachData->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Stretch);
    for(int i=6;i<10;i++)ui->TeachData->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
}

void RobotUI::ShowTeachData(QString Name)
{
    ShowTeachDataFlag = true;
    QVector<RobotPoint> Data;
    while(TeachDataBtns.buttons().size() > 0) TeachDataBtns.removeButton(TeachDataBtns.buttons()[0]);
    ui->TeachData->clearContents();
    if(ui->TeachPointBtn->isChecked())
    {
        QList<QPair<QString, RobotPoint>> Points = TeachData.GetPoints();
        QList<QString> Names;
        for(int i=0;i<Points.size();i++)
        {
            Names.push_back(Points[i].first);
        }
        ui->TeachList->clear();
        ui->TeachList->addItems(Names);
        if(!Names.isEmpty())
        {
            if(Names.contains(Name)) ui->TeachList->setCurrentText(Name);
            else ui->TeachList->setCurrentText(Names[0]);
            RobotPoint Point;
            TeachData.GetPoint(ui->TeachList->currentText(),Point);
            Data.clear();
            Data.push_back(Point);
        }
        ui->TeachAddPoint->setEnabled(false);
        ui->TeachDelPoint->setEnabled(false);
    }
    else if(ui->TeachWayBtn->isChecked())
    {
        QList<QPair<QString,QVector<RobotPoint>>> Ways = TeachData.GetWays();
        QList<QString> Names;
        for(int i=0;i<Ways.size();i++)
        {
            Names.push_back(Ways[i].first);
        }
        ui->TeachList->clear();
        ui->TeachList->addItems(Names);
        if(!Names.isEmpty())
        {
            if(Names.contains(Name)) ui->TeachList->setCurrentText(Name);
            else ui->TeachList->setCurrentText(Names[0]);
            QVector<RobotPoint> Way;
            TeachData.GetWay(ui->TeachList->currentText(),Way);
            Data = Way;
        }
        ui->TeachAddPoint->setEnabled(true);
        ui->TeachDelPoint->setEnabled(true);
    }
    ui->TeachData->setRowCount(Data.size()+1);
    for(int i=0;i<Data.size();i++)
    {
        ui->TeachData->setVerticalHeaderItem(i,new QTableWidgetItem(QString::number(i)));
        for(int j=0;j<6;j++)
        {
            ui->TeachData->setItem(i,j,new QTableWidgetItem(QString::number(Data[i].Pose[j],'f',3)));
            ui->TeachData->item(i,j)->setFlags(ui->TeachData->item(i,j)->flags() & ~Qt::ItemIsEditable);
        }
        ui->TeachData->setItem(i,6,new QTableWidgetItem(QString::number(Data[i].speed)));
        ui->TeachData->setItem(i,7,new QTableWidgetItem(QString::number(Data[i].moveType)));
        ui->TeachData->setItem(i,8,new QTableWidgetItem(QString::number(Data[i].smooth)));
        QPushButton *TeachDataBtn = new QPushButton();
        TeachDataBtn->setText("Now");
        TeachDataBtns.addButton(TeachDataBtn,i);
        ui->TeachData->setCellWidget(i,9,TeachDataBtn);
    }
    ui->TeachData->setVerticalHeaderItem(Data.size(),new QTableWidgetItem());
    for(int j=0;j<ui->TeachData->columnCount();j++)
    {
        ui->TeachData->setItem(Data.size(),j,new QTableWidgetItem());
        ui->TeachData->item(Data.size(),j)->setBackgroundColor(QColor(230,230,230));
        ui->TeachData->item(Data.size(),j)->setFlags(ui->TeachData->item(Data.size(),j)->flags() & ~Qt::ItemIsEditable);
    }
    ShowTeachDataFlag = false;
}

void RobotUI::RefreshSlot()
{
    if(!isVisible()) return;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<RobotStatus>()) return;
    RobotStatus status = Recv.Datas["Status"].value<RobotStatus>();
    //RobotMode
    QList<QString> RobotMode;
    RobotMode<<"TEACH"<<"PLAY"<<"REMOTE";
    ui->robotMode->setText((status.robotMode>=0 && status.robotMode<RobotMode.size())?RobotMode[status.robotMode]:"UnKnow");
    //robotState
    QList<QString> robotState;
    robotState<<"Stop"<<"Pause"<<"E-Stop"<<"Running"<<"Alarm";
    ui->robotState->setText((status.robotState>=0 && status.robotState<robotState.size())?robotState[status.robotState]:"UnKnow");
    //servoReady
    QList<QString> servoReady;
    servoReady<<"Servo off"<<"Servo on";
    ui->servoReady->setText((status.servoReady>=0 && status.servoReady<servoReady.size())?servoReady[status.servoReady]:"UnKnow");
    //can_motor_run
    QList<QString> can_motor_run;
    can_motor_run<<"Sync off"<<"Sync on";
    ui->can_motor_run->setText((status.can_motor_run>=0 && status.can_motor_run<can_motor_run.size())?can_motor_run[status.can_motor_run]:"UnKnow");
    //Pose
    ui->X_Pos->setText(QString::number(status.machinePose[0],'f',3));
    ui->Y_Pos->setText(QString::number(status.machinePose[1],'f',3));
    ui->Z_Pos->setText(QString::number(status.machinePose[2],'f',3));
    ui->RX_Pos->setText(QString::number(status.machinePose[3],'f',3));
    ui->RY_Pos->setText(QString::number(status.machinePose[4],'f',3));
    ui->RZ_Pos->setText(QString::number(status.machinePose[5],'f',3));
    //Enable
    ui->ResetBtn->setEnabled(status.robotMode == 2 && !isReseting);
    ui->EnableBtn->setEnabled(status.robotMode == 2 && status.robotState == 0 && status.can_motor_run == 1);
    ui->DisableBtn->setEnabled(status.robotMode == 2 && status.robotState == 0 && status.can_motor_run == 1);
    foreach(auto Btn,Pos_Add_Group.buttons())
    {
        Btn->setEnabled(status.robotMode == 2 && status.robotState == 0 && status.can_motor_run == 1 && status.servoReady == 1);
    }
    foreach(auto Btn,Pos_Minus_Group.buttons())
    {
        Btn->setEnabled(status.robotMode == 2 && status.robotState == 0 && status.can_motor_run == 1 && status.servoReady == 1);
    }
    foreach(auto Btn,MoveBtn_Group.buttons())
    {
        Btn->setEnabled(status.robotMode == 2 && status.robotState == 0 && status.can_motor_run == 1 && status.servoReady == 1);
    }
    ui->MovewayPoint->setEnabled(status.robotMode == 2 && status.robotState == 0 && status.can_motor_run == 1 && status.servoReady == 1);
}

void RobotUI::on_ResetBtn_clicked()
{
    CtrlError ret;
    isReseting = true;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Reset";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    isReseting = false;
    return;
}

void RobotUI::on_EnableBtn_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetEnable";
    Send.Identfication = 100;
    Send.Datas.insert("Enable",QVariant::fromValue(true));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void RobotUI::on_DisableBtn_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetEnable";
    Send.Identfication = 100;
    Send.Datas.insert("Enable",QVariant::fromValue(false));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void RobotUI::Pos_Add_clicked(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0 || !Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<RobotStatus>())
    {
        QMessageBox::critical(this,"Error",ret);
        return;
    }
    RobotStatus status = Recv.Datas["Status"].value<RobotStatus>();
    QVector<double> Target;
    for(int i=0;i<6;i++) Target.push_back(status.machinePose[i]);
    Target[n] +=  ui->Step->value();
    if(n >= 3 && Target[n] > 180) Target[n]-=360;
    RobotPoint tPoint;
    tPoint.Pose = Target;
    tPoint.moveType = 0;
    tPoint.speed = ui->Speed->value();
    Send = HardwareCommData();
    Send.Station = Station;
    Send.OperatorName = "MovePositionByPose";
    Send.Identfication = 100;
    Send.Datas.insert("Point",QVariant::fromValue(tPoint));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void RobotUI::Pos_Minus_clicked(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0 || !Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<RobotStatus>())
    {
        QMessageBox::critical(this,"Error",ret);
        return;
    }
    RobotStatus status = Recv.Datas["Status"].value<RobotStatus>();
    QVector<double> Target;
    for(int i=0;i<6;i++) Target.push_back(status.machinePose[i]);
    Target[n] -=  ui->Step->value();
    if(n >= 3 && Target[n] < -180) Target[n]+=360;
    RobotPoint tPoint;
    tPoint.Pose = Target;
    tPoint.moveType = 0;
    tPoint.speed = ui->Speed->value();
    Send = HardwareCommData();
    Send.Station = Station;
    Send.OperatorName = "MovePositionByPose";
    Send.Identfication = 100;
    Send.Datas.insert("Point",QVariant::fromValue(tPoint));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void RobotUI::MoveBtn_clicked(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0 || !Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<RobotStatus>())
    {
        QMessageBox::critical(this,"Error",ret);
        return;
    }
    RobotStatus status = Recv.Datas["Status"].value<RobotStatus>();
    QVector<double> Target;
    for(int i=0;i<6;i++) Target.push_back(status.machinePose[i]);
    bool isOK = false;
    switch(n)
    {
    case 0:
        Target[n] =  ui->X_Move->text().toDouble(&isOK);
        break;
    case 1:
        Target[n] =  ui->Y_Move->text().toDouble(&isOK);
        break;
    case 2:
        Target[n] =  ui->Z_Move->text().toDouble(&isOK);
        break;
    case 3:
        Target[n] =  ui->RX_Move->text().toDouble(&isOK);
        break;
    case 4:
        Target[n] =  ui->RY_Move->text().toDouble(&isOK);
        break;
    case 5:
        Target[n] =  ui->RZ_Move->text().toDouble(&isOK);
        break;
    }
    if(!isOK)  QMessageBox::critical(this,"Error","123");
    if(n >= 3 && Target[n] > 180)   Target[n]-=360;
    if(n >= 3 && Target[n] < -180)  Target[n]+=360;
    RobotPoint tPoint;
    tPoint.Pose = Target;
    tPoint.moveType = 0;
    tPoint.speed = ui->Speed->value();
    Send = HardwareCommData();
    Send.Station = Station;
    Send.OperatorName = "MovePositionByPose";
    Send.Identfication = 100;
    Send.Datas.insert("Point",QVariant::fromValue(tPoint));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void RobotUI::TeachDataBtns_clicked(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0 || !Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<RobotStatus>())
    {
        QMessageBox::critical(this,"Error",ret);
        return;
    }
    RobotStatus status = Recv.Datas["Status"].value<RobotStatus>();
    if(ui->TeachPointBtn->isChecked())
    {
        RobotPoint tPoint;
        if(TeachData.GetPoint(ui->TeachList->currentText(),tPoint) != 0) return;
        tPoint.Pose.clear();
        for(int i=0;i<6;i++) tPoint.Pose.push_back(status.machinePose[i]);
        tPoint.Pos.clear();
        for(int i=0;i<8;i++) tPoint.Pos.push_back(status.machinePos[i]);
        TeachData.SetPoint(ui->TeachList->currentText(),tPoint);
    }
    else if(ui->TeachWayBtn->isChecked())
    {
        QVector<RobotPoint> tWay;
        if(TeachData.GetWay(ui->TeachList->currentText(),tWay) != 0) return;
        if(tWay.size()<=n) return;
        tWay[n].Pose.clear();
        for(int i=0;i<6;i++) tWay[n].Pose.push_back(status.machinePose[i]);
        tWay[n].Pos.clear();
        for(int i=0;i<8;i++) tWay[n].Pos.push_back(status.machinePos[i]);
        TeachData.SetWay(ui->TeachList->currentText(),tWay);
    }
    else return;
    UpDateTeachData(Station,TeachData);
}

void RobotUI::TeachDataRadio_clicked(int n)
{
    ShowTeachData();
}

void RobotUI::on_TeachAdd_clicked()
{
    QString Name = QInputDialog::getText(this,"Add","Input Add Name");
    if(Name.isEmpty()) return;
    if(Name.contains("-"))
    {
        QMessageBox::critical(this,"Error","Name exist '-'");
        return;
    }
    if(ui->TeachPointBtn->isChecked())
    {
        QList<QPair<QString,QVector<RobotPoint>>> Ways = TeachData.GetWays();
        for(int i=0;i<Ways.size();i++)
        {
            if(Ways[i].first == Name)
            {
                QMessageBox::critical(this,"Error","Name is exist at way!");
                return;
            }
        }
        TeachData.SetPoint(Name,RobotPoint());
    }
    else if(ui->TeachWayBtn->isChecked())
    {
        QList<QPair<QString,RobotPoint>> Points = TeachData.GetPoints();
        for(int i=0;i<Points.size();i++)
        {
            if(Points[i].first == Name)
            {
                QMessageBox::critical(this,"Error","Name is exist at Point!");
                return;
            }
        }
        TeachData.SetWay(Name,QVector<RobotPoint>(2));
    }
    ui->TeachList->addItem(Name);
    ui->TeachList->setCurrentText(Name);
    UpDateTeachData(Station,TeachData);
}

void RobotUI::on_TeachDel_clicked()
{
    QString Name = ui->TeachList->currentText();
    if(Name.isEmpty()) return;
    if(QMessageBox::warning(this,"Warning",QString("Delete %1 ?").arg(Name),QMessageBox::Ok,QMessageBox::Cancel) != QMessageBox::Ok) return;
    if(ui->TeachPointBtn) TeachData.RemovePoint(Name);
    else TeachData.RemoveWay(Name);
    UpDateTeachData(Station,TeachData);
}

void RobotUI::on_TeachAddPoint_clicked()
{
    QVector<RobotPoint> Data;
    if(TeachData.GetWay(ui->TeachList->currentText(),Data) != 0) return;
    int row = ui->TeachData->currentRow();
    if(row < 0 || row > Data.size()) row = Data.size();
    Data.insert(row,RobotPoint());
    TeachData.SetWay(ui->TeachList->currentText(),Data);
    UpDateTeachData(Station,TeachData);
}

void RobotUI::on_TeachDelPoint_clicked()
{
    QVector<RobotPoint> Data;
    if(TeachData.GetWay(ui->TeachList->currentText(),Data) != 0) return;
    int row = ui->TeachData->currentRow();
    if(row < 0 || row >= Data.size() || Data.size() <= 2) return;
    Data.removeAt(row);
    TeachData.SetWay(ui->TeachList->currentText(),Data);
    UpDateTeachData(Station,TeachData);
}

void RobotUI::on_MovewayPoint_clicked()
{
    CtrlError ret;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.Identfication = 100;
    if(ui->TeachPointBtn->isChecked())
    {
        RobotPoint Data;
        if(TeachData.GetPoint(ui->TeachList->currentText(),Data) != 0) return;
        Send.OperatorName = "MovePosition";
        Send.Datas.insert("Point",QVariant::fromValue(Data));
    }
    else if(ui->TeachWayBtn->isChecked())
    {
        QVector<RobotPoint> Data;
        if(TeachData.GetWay(ui->TeachList->currentText(),Data) != 0) return;
        Send.OperatorName = "MoveWay";
        Send.Datas.insert("Points",QVariant::fromValue(Data));
    }
    else return;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void RobotUI::on_TeachData_cellChanged(int row, int column)
{
    if(ShowTeachDataFlag) return;
    QVector<RobotPoint> Data;
    if(ui->TeachPointBtn->isChecked())
    {
        RobotPoint tData;
        if(TeachData.GetPoint(ui->TeachList->currentText(),tData) != 0) return;
        Data.push_back(tData);
    }
    else if(ui->TeachWayBtn->isChecked())
    {
        if(TeachData.GetWay(ui->TeachList->currentText(),Data) != 0) return;
    }
    else return;

    if(row < 0 || row >= Data.size()) return;
    bool isOK;
    if(column >=6 && column < 9)
    {
        double tValue = ui->TeachData->item(row,column)->text().toInt(&isOK);
        if(isOK)
        {
            if(column == 6) Data[row].speed =tValue;
            if(column == 7) Data[row].moveType =tValue;
            if(column == 8) Data[row].smooth =tValue;
        }
    }
    if(ui->TeachPointBtn->isChecked())
    {
        TeachData.SetPoint(ui->TeachList->currentText(),Data[0]);
    }
    else if(ui->TeachWayBtn->isChecked())
    {
        TeachData.SetWay(ui->TeachList->currentText(),Data);
    }
    else return;
    UpDateTeachData(Station,TeachData);
}

void RobotUI::on_TeachList_currentTextChanged(const QString &arg1)
{
    if(ShowTeachDataFlag) return;
    ShowTeachData(arg1);
}

void RobotUI::RefreshTeachData(QString Station, TeachData_Robot TeachData)
{
    if(this->Station != Station) return;
    this->TeachData = TeachData;
    ShowTeachData(ui->TeachList->currentText());
}
