#pragma once

#include <QObject>
#include <QtCore>
#include "opencv2/opencv.hpp"

class Public_Test_Func : public QObject
{
	Q_OBJECT

public:
	Public_Test_Func(QObject *parent);
	~Public_Test_Func();

	static int Test01(cv::Mat Image, cv::Mat &ImageOut, double Thresd);
	static int FindCenter(cv::Mat SrcImage, cv::Point2f &Center, double Thresd);
	static int Type1(cv::Mat SrcImage, cv::Point2f Center, double &Angle);
	static int MoveImage(cv::Mat SrcImage, cv::Point2f OldCenter, cv::Point2f NewCenter, double OldAngle, double NewAngle, cv::Mat &DstImage);

};
