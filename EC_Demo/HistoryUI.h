#ifndef HISTORYUI_H
#define HISTORYUI_H
#include <QtCore>
#include <QWidget>
#include "History.h"
#include <QTreeWidgetItem>
namespace Ui {
class HistoryUI;
}

class HistoryUI : public QWidget
{
    Q_OBJECT

public:
    explicit HistoryUI(QWidget *parent = nullptr);
    ~HistoryUI();

private slots:
    void on_SNSearch_clicked();

    void on_BarCodeSearch_clicked();

    void on_DateTimeSearch_clicked();

    void on_DataList_itemClicked(QTreeWidgetItem *item, int column);

private:
    Ui::HistoryUI *ui;
    void ShowData(QVector<HistoryResult> Results);
    bool ShowDataFlag;
    void InitDataList();
};

#endif // HISTORYUI_H
