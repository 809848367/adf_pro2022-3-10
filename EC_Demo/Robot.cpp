﻿#include "Robot.h"

Robot::Robot(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
}

CtrlError Robot::Init(HardwareInitData InitData)
{
    if(InitData.Keys.size() != 3) return CtrlError(InitDataErr,InitData.Name);
    if(InitData.InitData.size() != 1) return CtrlError(InitDataErr,InitData.Name);
    if(InitData.InitData[0].size() != 3) return CtrlError(InitDataErr,InitData.Name);
    QString IP;
    quint16 CmdPort;
    quint16 StatusPort;
    for(int i=0;i<3;i++)
    {
        if(InitData.Keys[i] == "IP" && InitData.InitData[0][i].canConvert<QString>())
            IP = InitData.InitData[0][i].value<QString>();
        else if(InitData.Keys[i] == "CmdPort" && InitData.InitData[0][i].canConvert<quint16>())
            CmdPort = InitData.InitData[0][i].value<quint16>();
        else if(InitData.Keys[i] == "StatusPort" && InitData.InitData[0][i].canConvert<quint16>())
            StatusPort = InitData.InitData[0][i].value<quint16>();
        else
            return CtrlError(InitDataErr,InitData.Name);
    }
    return Init(InitData.Name,IP,CmdPort,StatusPort);
}

CtrlError Robot::Init(QString Name, QString IP, quint16 CmdPort, quint16 StatusPort)
{
    CtrlError ret;
    this->Name = Name;
    ret = CmdClient.Open(Name+"Command",IP,CmdPort);
    if(ret != NoError) return ret;
    ret = StatusClient.Open(Name+"Status",IP,StatusPort);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Robot::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    CtrlError ret;
    ResultData.Station = ExecuteData.Station;
    ResultData.Identfication = ExecuteData.Identfication;
    ResultData.OperatorName = ExecuteData.OperatorName;
    if(ExecuteData.OperatorName == "GetStatus")
    {
        ResultData.Datas.insert("Status",QVariant::fromValue(GetStatus()));
    }
    if(ExecuteData.OperatorName == "MovePosition")
    {
        if(!ExecuteData.Datas["Point"].canConvert<RobotPoint>()) return CtrlError(ExecuteDataNotRight,Name);
        RobotPoint Point = ExecuteData.Datas["Point"].value<RobotPoint>();
        ret =  MovePosition(Point);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "MovePositionByPose")
    {
        if(!ExecuteData.Datas["Point"].canConvert<RobotPoint>()) return CtrlError(ExecuteDataNotRight,Name);
        RobotPoint Point = ExecuteData.Datas["Point"].value<RobotPoint>();
        ret =  MovePositionByPose(Point);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "MoveWay")
    {
        if(!ExecuteData.Datas["Points"].canConvert<QVector<RobotPoint>>()) return CtrlError(ExecuteDataNotRight,Name);
        QVector<RobotPoint> Points = ExecuteData.Datas["Points"].value<QVector<RobotPoint>>();
        ret =  MoveWay(Points);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "Reset")
    {
        ret =  Reset();
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "SetEnable")
    {
        if(!ExecuteData.Datas["Enable"].canConvert<bool>()) return CtrlError(ExecuteDataNotRight,Name);
        bool Enable = ExecuteData.Datas["Enable"].value<bool>();
        ret =  SetEnable(Enable);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "WaitArrive")
    {
        if(!ExecuteData.Datas["Point"].canConvert<RobotPoint>()) return CtrlError(ExecuteDataNotRight,Name);
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        RobotPoint Point = ExecuteData.Datas["Point"].value<RobotPoint>();
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret =  WaitArrive(Point,TimeOut);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "WaitArriveByPose")
    {
        if(!ExecuteData.Datas["Point"].canConvert<RobotPoint>()) return CtrlError(ExecuteDataNotRight,Name);
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        RobotPoint Point = ExecuteData.Datas["Point"].value<RobotPoint>();
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret =  WaitArriveByPose(Point,TimeOut);
        if(ret != NoError) return ret;
    }
    return CtrlError(NoError);
}

RobotStatus Robot::GetStatus()
{
    return StatusClient.GetStatus();
}

CtrlError Robot::MovePosition(RobotPoint Point)
{
    CtrlError ret;
    RobotStatus status = GetStatus();
    if(status.robotMode != 2) return CtrlError(NeedRemoteModeFirst,Name);
    if(status.robotState == 4)  CtrlError(RobotHasAlarm,Name);
//    QVector<double> Reference,Pos;
//    for(int i=0;i<8;i++) Reference.push_back(status.machinePos[i]);
//    ret = inverseKinematic(Point.Pose,Reference,Pos);
//    if(ret != NoError) return ret;
    if(Point.moveType == 0) ret = moveByJoint(Point.Pos,Point.speed);
    else if(Point.moveType == 1) ret = moveByLine(Point.Pos,Point.speed);
    else return CtrlError(moveTypeNotExist,Name);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Robot::MovePositionByPose(RobotPoint Point)
{
    CtrlError ret;
    RobotStatus status = GetStatus();
    if(status.robotMode != 2) return CtrlError(NeedRemoteModeFirst,Name);
    if(status.robotState == 4)  CtrlError(RobotHasAlarm,Name);
    QVector<double> Reference,Pos;
    for(int i=0;i<8;i++) Reference.push_back(status.machinePos[i]);
    ret = inverseKinematic(Point.Pose,Reference,Pos);
    if(ret != NoError) return ret;
    if(Point.moveType == 0) ret = moveByJoint(Pos,Point.speed);
    else if(Point.moveType == 1) ret = moveByLine(Pos,Point.speed);
    else return CtrlError(moveTypeNotExist,Name);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Robot::MoveWay(QVector<RobotPoint> Points)
{
    CtrlError ret;
    RobotStatus status = GetStatus();
//    QVector<double> Reference,Pos;
//    for(int i=0;i<8;i++) Reference.push_back(status.machinePos[i]);

    ret = clearPathPoint();
    if(ret != NoError) return ret;
    foreach(RobotPoint Point,Points)
    {
//        ret = inverseKinematic(Point.Pose,Reference,Pos);
//        if(ret != NoError) return ret;
        ret = addPathPoint(Point.Pos,Point.speed,Point.moveType,Point.smooth);
        if(ret != NoError) return ret;
//        Reference = Pos;
    }
    ret = moveByPath();
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Robot::WaitArrive(RobotPoint Point,int TimeOut)
{
    if(Point.Pose.size() != 6) return CtrlError(PoseSizeNotRight,Name);
    RobotStatus status;
    QTime Timer;
    Timer.start();
    while(1)
    {
        status = GetStatus();
        if(status.robotState == 1 || status.robotState == 2 || status.robotState == 4) return CtrlError(RobotHasAlarm,Name);
        bool isArrive = (status.robotState == 0);
//        for(int i=0;i<6;i++)
        for(int i=0;i<8;i++)
        {
            if(qAbs(status.machinePos[i]-Point.Pos[i]) > 0.05) isArrive = false;
        }
        if(isArrive) break;
        if(TimeOut > 0 && Timer.elapsed() > TimeOut) return CtrlError(WaitArriveTimeOut,Name);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
    return CtrlError(NoError);
}

CtrlError Robot::WaitArriveByPose(RobotPoint Point, int TimeOut)
{
    if(Point.Pose.size() != 6) return CtrlError(PoseSizeNotRight,Name);
    RobotStatus status;
    QTime Timer;
    Timer.start();
    while(1)
    {
        status = GetStatus();
        if(status.robotState == 1 || status.robotState == 2 || status.robotState == 4) return CtrlError(RobotHasAlarm,Name);
        bool isArrive = (status.robotState == 0);
        for(int i=0;i<6;i++)
        {
            if(qAbs(status.machinePose[i]-Point.Pose[i]) > 0.01) isArrive = false;
        }
        if(isArrive) break;
        if(TimeOut > 0 && Timer.elapsed() > TimeOut) return CtrlError(WaitArriveTimeOut,Name);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
    return CtrlError(NoError);
}

CtrlError Robot::Reset()
{
    CtrlError ret;
    RobotStatus status = GetStatus();
    if(status.robotMode != 2) return CtrlError(NeedRemoteModeFirst,Name);
    if(status.robotState == 4)
    {
        ret = clearAlarm();
        if(ret != NoError) return ret;
        QTime tTime;
        tTime.start();
        while(tTime.elapsed() < 3500) QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
    status = GetStatus();
    if(status.robotState == 4)  CtrlError(RobotHasAlarm,Name);
    if(status.can_motor_run == 0)
    {
        ret = syncMotorStatus();
        if(ret != NoError) return ret;
    }
    if(status.robotState != 0)
    {
        ret = stop();
        if(ret != NoError) return ret;
    }
    return CtrlError(NoError);
}

CtrlError Robot::SetEnable(bool Enable)
{
    CtrlError ret;
    RobotStatus status = GetStatus();
    if(status.robotMode != 2) return CtrlError(NeedRemoteModeFirst,Name);
    if(status.robotState == 4)  CtrlError(RobotHasAlarm,Name);
    ret = set_servo_status(Enable);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Robot::SetOutput(int Index, bool out)
{
    CtrlError ret;
    RobotStatus status = GetStatus();
    if(status.robotMode != 2) return CtrlError(NeedRemoteModeFirst,Name);
    ret = setOutput(Index,out);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Robot::inverseKinematic(QVector<double> Pose, QVector<double> Reference, QVector<double> &Pos)
{
    if(Pose.size() != 6) return CtrlError(PoseSizeNotRight,Name);
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","inverseKinematic");
    QJsonObject params;
    QJsonArray targetPose;
    for(int i=0;i<3;i++) targetPose.append(Pose[i]);
    for(int i=3;i<6;i++) targetPose.append(Pose[i]*M_PI/180);
    params.insert("targetPose",targetPose);
    QJsonArray referencePos;
    for(int i=0;i<8;i++) referencePos.append(Reference[i]);
    params.insert("referencePos",referencePos);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result")) return CtrlError(RecvErrorData,Name);
    QString PosStr = res.value("result").toString();
    QList<QString> PosList = PosStr.mid(1,PosStr.size() - 2).split(",");
    if(PosList.size() != 8) return CtrlError(RecvErrorData,Name);
    Pos.clear();
    for(int i=0;i<8;i++) Pos.push_back(PosList[i].toDouble());
    return CtrlError(NoError);
}

CtrlError Robot::moveByJoint(QVector<double> Pos, double Speed)
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","moveByJoint");
    QJsonObject params;
    QJsonArray targetPos;
    for(int i=0;i<8;i++) targetPos.append(Pos[i]);
    params.insert("targetPos",targetPos);
    params.insert("speed",Speed);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(MoveByJointErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::moveByLine(QVector<double> Pos, double Speed)
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","moveByLine");
    QJsonObject params;
    QJsonArray targetPos;
    for(int i=0;i<8;i++) targetPos.append(Pos[i]);
    params.insert("targetPos",targetPos);
    params.insert("speed",Speed);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(MoveByLineErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::clearAlarm()
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","clearAlarm");
    QJsonObject params;
    params.insert("force",1);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(clearAlarmErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::stop()
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","stop");
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(stopErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::syncMotorStatus()
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","syncMotorStatus");
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(syncMotorStatusErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::set_servo_status(bool Enable)
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","set_servo_status");
    QJsonObject params;
    params.insert("status",Enable?1:0);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(set_servo_statusErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::clearPathPoint()
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","clearPathPoint");
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(clearPathPointErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::addPathPoint(QVector<double> Pos, double speed, int moveType, int smooth)
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","addPathPoint");
    QJsonObject params;
    QJsonArray wayPoint;
    for(int i=0;i<8;i++) wayPoint.append(Pos[i]);
    params.insert("wayPoint",wayPoint);
    if((moveType < 0) || (moveType > 1)) return CtrlError(ParamNotRight,Name);
    params.insert("moveType",moveType);
    if((speed < 1) || (speed > (moveType == 0?100:3000))) return CtrlError(ParamNotRight,Name);
    params.insert("speed",speed);
    if((smooth < 0) || (smooth > 7)) return CtrlError(ParamNotRight,Name);
    params.insert("smooth",smooth);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(addPathPointErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::moveByPath()
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","moveByPath");
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result")) return CtrlError(moveByPathErr,Name);
    return CtrlError(NoError);
}

CtrlError Robot::setOutput(int Index,bool out)
{
    QByteArray SendStr,RecvStr;
    QJsonObject Cmd;
    Cmd.insert("jsonrpc","2.0");
    Cmd.insert("method","setOutput");
    QJsonObject params;
    params.insert("addr",Index);
    params.insert("status",out?1:0);
    Cmd.insert("params",params);
    Cmd.insert("id",100);
    SendStr = QJsonDocument(Cmd).toJson(QJsonDocument::Compact) + "\n";
    CmdClient.SendAndRecv(SendStr,RecvStr);
    if(RecvStr.isEmpty()) return CtrlError(RecvErrorData,Name);
    QJsonObject res;
    QJsonParseError err;
    res = QJsonDocument::fromJson(RecvStr,&err).object();
    if(err.error != QJsonParseError::NoError) return CtrlError(RecvErrorData,Name);
    if(!res.contains("id") || res.value("id").toInt() != 100) return CtrlError(RecvErrorData,Name);
    if(!res.contains("result") || res.value("result").toString() != "true") return CtrlError(set_servo_statusErr,Name);
    return CtrlError(NoError);
}
