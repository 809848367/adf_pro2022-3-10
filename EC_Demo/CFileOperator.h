#pragma once

#include <QObject>
#include<QMutex>
#include<QMutexLocker>
#include <QFile>
#include <QApplication>
#include<QDateTime>
#include<QDebug>
#include<QDir>
#include<QFileInfoList> 
#include <QFileInfo>
#include <QFileDialog>
#include <windows.h>
#include <QDateTime>
#include "CtrlError.h"
#if _MSC_VER >= 1600  
#pragma execution_character_set("utf-8")  
#endif  

class CFileOperator : public QObject
{
	Q_OBJECT
public:
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x01100000,
		DirNotFind = Base + 0x001,
		FileNotFind = Base + 0x002,
		DeleteDirError = Base + 0x003,
		DeleteFileError = Base + 0x004,
		PathError = Base + 0x005,

	};
	Q_ENUM(ErrorCode)
public:
	CFileOperator(QObject *parent = NULL);
	~CFileOperator();
	bool Init();
	bool close();

	//bool WriteFile_CSV(QString msg, QString path);
	//bool WriteFileWithTitle_CSV(QString dataStr, QString title, QString path);
	bool WriteTodayFile_CSV(QString msg);

	//bool WriteFile_TXT(QString msg, QString path);
	bool WriteTodayFile_TXT(QString msg);
	bool WriteTodayFile_TxtWithModule(QStringList ModuleName,QString msg);

	//删除 文件夹所有文件 包含时间  天数 ；
	//0的时候删除所有文件夹内部文件
	bool DeleteDirectoryFileWithTime(QString dirPath, int time = 0);

	bool SetTxtDirPath(QString dirPath);
	bool SetCsvDirPath(QString dirPath);

	QFileInfoList GetFileList(QString& path);
	//删除文件夹下所有内容
	CtrlError DeleteDir(QString dirPath);
	//保留文件夹下距离当前时间多少小时内的文件
	//dirPath 文件夹路径
	//hours 小时数
	CtrlError DeleteDirectoryFileByRecentHour(QString dirPath,int hours=0);


private:
	//锁
	QMutex m_txtMutex;
	QMutex m_csvMutex;
	QString m_txtPath;
	QString	m_csvPath;
	QFile m_txtfile;
	QFile m_csvFile;
};
extern CFileOperator theFileManager;