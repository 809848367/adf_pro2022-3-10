﻿#include "AlgoCommData.h"

AlgoCommData::AlgoCommData()
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    Data.clear();
}

AlgoCommData::~AlgoCommData()
{
    Data.clear();
}

CtrlError AlgoCommData::SetData(QString tData)
{
    QJsonParseError tErr;
    QJsonObject t = QJsonDocument::fromJson(tData.toLocal8Bit(),&tErr).object();
    if(tErr.error != QJsonParseError::NoError) return CtrlError(CommandTransError);
    Data.clear();
    foreach(QString Key,t.keys())
    {
        Data.insert(Key,t.value(Key).toString());
    }
    return CtrlError(NoError);
}

QString AlgoCommData::GetData()
{
    QJsonObject t;
    foreach(QString Key,Data.keys())
    {
        t.insert(Key,Data[Key]);
    }
    return QJsonDocument(t).toJson(QJsonDocument::Compact); //Compact: No Space and Enter
}
