#include "QiniOperator.h"
#include <QFile>
#include <QMessageBox>
#include <QDebug>
#include<QTextCodec>
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 

QiniOperator::QiniOperator()
{

}

QiniOperator::~QiniOperator()
{
}
//QiniOperator* instance = NULL;
QiniOperator iniOperator;

bool QiniOperator::isLocalIniExist(QString filePath)
{
	//Debug 下目录进行创建文件
	QFile file(LOCAL_INI_PATH);
	if (!file.exists())
	{
		int rtn = QMessageBox::warning(NULL,"提示", "本地配置文件不存在是否重新创建", QMessageBox::Yes|QMessageBox::No, QMessageBox::Yes);
		if (rtn == QMessageBox::Yes)
		{
			file.open(QIODevice::ReadWrite | QIODevice::Text);
			file.close();
			qDebug() << " isLocalIniExist 创建一个新的本地文件";
			return true;
		}
		else
		{
			qDebug() << " isLocalIniExist 未发现本地文件";
			file.close();
			return false;
		}
	}
	qDebug() << " isLocalIniExist 文件已存在";
	file.close();
	return true;
}

void QiniOperator::setPath(QString curPath)
{
	m_curPath = curPath;
	//this->m_curPath = curPath;
	//m_settings.setPath(QSettings::IniFormat, QSettings::UserScope,curPath);
}

bool QiniOperator::isFileExist()
{
	QFile file(m_curPath);
	if (!file.exists())
		return false;
	return true;
}

void QiniOperator::readIntValue(QString section, QString key, int& value)
{
	QString keyStr = section + "/" + key;
	QSettings* settingg =new  QSettings(m_curPath, QSettings::IniFormat);
	settingg->setIniCodec(QTextCodec::codecForName("UTF-8"));
	value = settingg->value(keyStr).toInt();
	delete settingg;
}

void QiniOperator::readStringValue(QString section, QString key, QString & value)
{
	QString keyStr = section + "/" + key;
	QSettings* settingg = new  QSettings(m_curPath, QSettings::IniFormat);
	settingg->setIniCodec(QTextCodec::codecForName("UTF-8"));
	value = settingg->value(keyStr).toString();
	//qDebug() << value;
	delete settingg;
}

void QiniOperator::readDoubleValue(QString section, QString key, double & value)
{
	QString keyStr = section + "/" + key;
	QSettings* settingg = new  QSettings(m_curPath, QSettings::IniFormat);
	settingg->setIniCodec(QTextCodec::codecForName("UTF-8"));
	value = settingg->value(keyStr).toDouble();
	delete settingg;
}

void QiniOperator::readBoolValue(QString section, QString key, bool & value)
{
	QString keyStr = section + "/" + key;
	QSettings* settingg = new  QSettings(m_curPath, QSettings::IniFormat);
	settingg->setIniCodec(QTextCodec::codecForName("UTF-8"));
	value = settingg->value(keyStr).toBool();
	delete settingg;
}

void QiniOperator::readData(QString section, QString key, QVariant& value)
{
	QString keyStr = section + "/" + key;
	QSettings* settingg = new  QSettings(m_curPath, QSettings::IniFormat);
	settingg->setIniCodec(QTextCodec::codecForName("UTF-8"));
	value = settingg->value(keyStr);
	delete settingg;
}

void QiniOperator::setValue(QString section, QString key, QVariant value)
{
	QString keyStr = section + "/" + key;
	QSettings* settingg = new  QSettings(m_curPath, QSettings::IniFormat);
	settingg->setIniCodec(QTextCodec::codecForName("UTF-8"));
	settingg->setValue(keyStr, value);
	delete settingg;
}

//void QiniOperator::setIntValue(QString section, QString key, int value)
//{
//	QString keyStr = section + "/" + key;
//	QSettings* settingg = new  QSettings(m_curPath, QSettings::IniFormat);
//	settingg->setValue(keyStr, value);
//	delete settingg;
//}

//void QiniOperator::readValue()
//{
//	m_settings.value("conn/ip").toString();
//	m_settings.setValue("conn/ip",111);
//}

//QiniOperator* QiniOperator::getInstance()
//{
//	if (NULL == instance)
//	{
//		instance = new QiniOperator();
//	}
//	else
//		return instance;
//}



















