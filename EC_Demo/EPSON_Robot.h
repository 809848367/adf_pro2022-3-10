#pragma once

#include <QObject>
#include <QObject>
#include "HardwareModule.h"
#include <QTcpSocket>
#include <QtCore>
#include "ClientThread.h"


class EPSON_Robot : public HardwareModule
{
	Q_OBJECT
public:
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x00200000,
		ConnectionEPSONError = Base + 0x001,
		SendCmdDataError = Base + 0x002,
		ResError = Base + 0x003,
		ResTimeOut = Base + 0x004,
		ModuleParaKeyNotContain = Base + 0x005,
		ModuleParaCmdError = Base + 0x006,
		ModuleParaFuncNotContain = Base + 0x007,
		ExecCmdError =			Base + 0x008,
		InitDataErr =			Base + 0x009,
		ExecuteDataNotRight =	Base + 0x00a,
		ClientHasOpen =		Base + 0x00b,
		ClientOpenFail =	Base + 0x00c,
		ClientWriteError =  Base + 0x00d,
		ClientReadTimeOut = Base + 0x00e,
		ModuleNameError =	Base + 0x00f,
	};
	Q_ENUM(ErrorCode)
public:
	EPSON_Robot(QObject *parent = nullptr);
	~EPSON_Robot();
	CtrlError Init(HardwareInitData InitData) Q_DECL_OVERRIDE;
	CtrlError Execute(HardwareCommData ExecuteData, HardwareCommData& ResultData)Q_DECL_OVERRIDE;
private:
	CtrlError Init(QString name,QString ip,int port);
	CtrlError SendAndRecv(CmdData cmd, CmdData& respond);
	CtrlError WaitRobotCmd(CmdData& cmd,int tOut);
private:
	//QTcpSocket* m_socket;
	//QMutex m_socketMutex;
	ClientThread* m_client;
};
