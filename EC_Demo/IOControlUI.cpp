﻿#include "IOControlUI.h"
#include "ui_IOControlUI.h"
#include <QToolButton>
#include <QMessageBox>
IOControlUI::IOControlUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IOControlUI)
{
    ui->setupUi(this);
    InitBtnsList(ui->InPutList);
    InitBtnsList(ui->OutPutList);
    HWM = nullptr;
    connect(&OutPutBtns,SIGNAL(buttonClicked(int)),this,SLOT(OutPutBtnsClicker(int)));
    connect(&RefreshTimer,SIGNAL(timeout()),this,SLOT(RefreshTimerSlot()));
    RefreshTimer.start(100);
}

IOControlUI::~IOControlUI()
{
    delete ui;
}

void IOControlUI::SetHardwareManager(HardwareManager *HWM, QString Station)
{
    this->HWM = HWM;
    this->Station = Station;
}

void IOControlUI::InitBtnsList(QTableWidget *BtnsList)
{
    BtnsList->setRowCount(0);
    BtnsList->setColumnCount(2);
    BtnsList->setEditTriggers(QTableWidget::NoEditTriggers);
    BtnsList->horizontalHeader()->setVisible(true);
    BtnsList->horizontalHeader()->setFixedHeight(30);
    QList<QString> HItems;
    HItems<<"Name"<<"Status";
    BtnsList->setHorizontalHeaderLabels(HItems);
    BtnsList->verticalHeader()->setVisible(true);
    BtnsList->verticalHeader()->setFixedWidth(30);
    BtnsList->setColumnWidth(1,80);
    BtnsList->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    BtnsList->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Fixed);
}

void IOControlUI::ShowData(QTableWidget *BtnsList, QMap<int, QString> IOs, QMap<int, bool> IOValue, QButtonGroup &Btns)
{
    QList<int> Keys = IOs.keys();
    if(BtnsList->rowCount() != Keys.size())
    {
        while(Btns.buttons().size() > 0) Btns.removeButton(Btns.buttons()[0]);
        BtnsList->setRowCount(IOs.size());
        for(int i=0;i<Keys.size();i++)
        {
            BtnsList->setVerticalHeaderItem(i,new QTableWidgetItem(QString::number(Keys[i])));
            BtnsList->setItem(i,0,new QTableWidgetItem(IOs[Keys[i]]));
            QToolButton *Btn = new QToolButton();
            Btns.addButton(Btn,Keys[i]);
            BtnsList->setCellWidget(i,1,Btn);
        }
    }
    for(int i=0;i<Keys.size();i++)
    {
        QToolButton *Btn = (QToolButton*)BtnsList->cellWidget(i,1);
        if(IOValue[Keys[i]] == false)
            Btn->setIcon(QIcon(":/Img/invalid.ico"));
        else
            Btn->setIcon(QIcon(":/Img/valid.ico"));
    }
}

void IOControlUI::OutPutBtnsClicker(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetOutPut";
    Send.Identfication = 100;
    Send.Datas.insert("Index",QVariant::fromValue(n));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0)
    {
        QMessageBox::critical(this,"Error",ret);
        return;
    }
    if(!Recv.Datas.contains("Value") || !Recv.Datas["Value"].canConvert<bool>()) return;
    bool Value = Recv.Datas["Value"].value<bool>();
    Send.OperatorName = "SetOutPut";
    Send.Datas.insert("Value",QVariant::fromValue(!Value));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0)
    {
        QMessageBox::critical(this,"Error",ret);
        return;
    }
}

void IOControlUI::RefreshTimerSlot()
{
    if(!isVisible()) return;
    CtrlError ret;
    if(HWM == nullptr)
    {
        ui->InPutList->setRowCount(0);
        ui->OutPutList->setRowCount(0);
        return;
    }
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetIONames";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("InPuts") || !Recv.Datas["InPuts"].canConvert<QMap<int, QString>>()) return;
    QMap<int, QString> InPuts = Recv.Datas["InPuts"].value<QMap<int, QString>>();
    if(!Recv.Datas.contains("OutPuts") || !Recv.Datas["OutPuts"].canConvert<QMap<int, QString>>()) return;
    QMap<int, QString> OutPuts = Recv.Datas["OutPuts"].value<QMap<int, QString>>();

    Send.OperatorName = "GetInPuts";
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Value") || !Recv.Datas["Value"].canConvert<QMap<int, bool>>()) return;
    QMap<int, bool> InPutValue = Recv.Datas["Value"].value<QMap<int, bool>>();

    Send.OperatorName = "GetOutPuts";
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Value") || !Recv.Datas["Value"].canConvert<QMap<int, bool>>()) return;
    QMap<int, bool> OutPutValue = Recv.Datas["Value"].value<QMap<int, bool>>();

    ShowData(ui->InPutList,InPuts,InPutValue,InputBtns);
    ShowData(ui->OutPutList,OutPuts,OutPutValue,OutPutBtns);
}
