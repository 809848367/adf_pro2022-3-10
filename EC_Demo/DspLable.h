#pragma once

#include <QLabel>
#include "ui_DspLable.h"
#include <QString> 
#include <QMouseEvent>


class DspLable : public QLabel
{
	Q_OBJECT
public:
	DspLable(QWidget *parent = Q_NULLPTR);
	~DspLable();
	virtual void resizeEvent(QResizeEvent *event);
	QString labelName();
	void mouseDoubleClickEvent(QMouseEvent *event);
	void setPixmap(const QPixmap & pixmap);
	void setImg(QImage *img);
	void setPicPath(QString picPath);
	void setDispInfo(QString info);
	QLabel* getInfoLab();//��ϢLab ��ʾ��
private:
	void systemToolDispImage(QString picPath);//��ʾͼƬ
private:
	Ui::DspLable ui;
	QString  m_name;
	QString m_filePath;
	QLabel* m_infoLab;
	QString m_info;
};
