#pragma once

#include <QDialog>
#include <QtCore>
#include "ui_TempDataShowDlg.h"

#define  theTempDlg TempDataShowDlg::GetInstance()
class TempDataShowDlg : public QDialog
{
	Q_OBJECT

public:
	TempDataShowDlg(QWidget *parent = Q_NULLPTR);
	~TempDataShowDlg();
	static TempDataShowDlg* GetInstance();
	static void DispTempDlg();
	static void ReleaseTempDlg();

	void InitTableWidget();


public slots:
void on_RefreshSlot();
void on_CleaerDataSlot();


private:
	Ui::TempDataShowDlg ui;
	static TempDataShowDlg* instance;
};
