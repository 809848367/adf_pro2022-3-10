#pragma once

#include <QObject>
#include <QMutex>
#include "CParentProcess.h"
#include <QtCore>
#include <QMap>

class MachineManager : public QObject
{
	Q_OBJECT
	Q_PROPERTY(bool m_bAtuo READ isAtuo WRITE SetAtuo NOTIFY bAutoChange);
public:
	enum ErrorCode
	{
		NoError = 0x00000000,//成功
		Base = 0x10000000,
		Error_Code_Start		= 0x10000000,
		EmptyProc	= Base + 0x001,//流程为空
		NotFindProc = Base + 0x002,//没有找到该流程
		PorcNotInIdle		= Base + 0x003,//该流程不在就绪状态
		CommondFailed = Base + 0x004,
		LoadProcessXMLError = Base + 0x005,
		ProcExceptionExit = Base + 0x006,//流程异常退出
		CheckProcStateError = Base + 0x007,//流程状态检测错误
		CurrentStateNotAllowExecute = Base + 0x008,//当前状态不允许执行
		NoProcWork = Base + 0x009,//没有流程在工作
	};
	Q_ENUM(ErrorCode)


	enum MachineStatus {
		MachineStatus_Idle = 0,		//待机状态
		MachineStatus_Reset,		//复位状态-》Machine_Idle
		MachineStatus_Work,			//工作状态
		MachineStatus_Work_End,		//流程正常结束 
		MachineStatus_Pause,		//暂停状态 需要再次点击 暂停按钮或者页面上的继续按钮-》工作状态
		MachineStatus_Continue,		//继续
		MachineStatus_Stop_Going,	//停止状态一般来说需要进行复位才能工作,类似于紧急退出-》复位
		MachineStatus_Stop,			//停止状态一般来说需要进行复位才能工作,类似于紧急退出-》复位
		MachineStatus_Error_Going,	//故障停止
		MachineStatus_Error,		//故障退出了流程-》必须复位
		MachineStatus_WaitReset		//待复位-》必须复位
	};
	Q_ENUM(MachineStatus)


public:
	MachineManager(QObject *parent = nullptr);
	~MachineManager();
	int  LoadProcessXML();
	int  ReloadProcessXML(QString ProFile);
	bool Init();
	void DeInit();
	void MsgRegister();
	bool isAtuo();
	QString GetStartProcName() { return  m_MainProc; };
	QString GetResetProName() { return m_ResetProc; };
	
	MachineStatus GetMachineStatus();
	QString GetResetInput();
	QString GetStartInput();
	QString GetPauseInput();
	QString GetStopInput();
	QList<CParentProcess*> GetProcList();
	
	void ResetCurrentState();//退出所有流程 将状态复位
	CtrlError ResetProcByName(QString procName); //将某个流程复位 状态Idle
	
signals:
	void bAutoChange(bool bAuto);
	void SignalStatus_Check();
	void Signal_Machine_Statu(QString status);
	void SignalMachineMsg(QString msg, int type);
	//SetProcList(QList<CParentProcess*> procList);
	void Signal_Send_Proc_List(QList<CParentProcess *> procList);
public slots:
	void SetAtuo(bool bAuto);
	void SetResetInput(QString input);
	void SetStartInput(QString input);
	void SetPauseInput(QString input);
	void SetStopInput(QString input);
	void SetMachineStatus(MachineStatus Status);

	void SetResetProcName(QString procName);//设置复位流程
	void SetStartProcName(QString procName);//设置启动流程
	CtrlError  StartProcByName(QString procName);
	void SetProcList(QList<CParentProcess*> procList);
	int GetProcStatusByName(QString procName);
	int GetProcExceptionByName(QString procName);

	CtrlError MachineReset();
	CtrlError MachineStop();
	CtrlError MachineErrorStop();
	CtrlError MachinePause();
	CtrlError MachineContinue();
	CtrlError MachineStart();
	//由流程触发
	int MachineError(QString msg, int ErrorType);
	void SetExit(bool exitFlag);
	bool GetExit();//退出信号
	int MachineExit();
	//检测流程结束用作流程状态置位操作
	void tRun_Check_Status();
	int ProcessPause();
	int ProcessStop();
	int ProcessContinue();
	//count slot 
	void ClearCTData();
	void AddOneProduct();
	void AddOneOKProduct();
	void AddOneNGProduct();
	void RefrshCTProduct(QString ctStr);
public:
	int TotalCount;//总数
	int OKCount;//OK 品
	int NGCount;//NG 品
	double PASSPercent;// 良率
	double FailurePrecent;//不良率
	double CycleTime;//循环耗时 s
	bool RefreshCT;//
	QMap<int, QTime> m_Map_StartTime;
	QMap<int, QTime> m_Map_EndTime;
	int totalTime;
	
protected:
	CtrlError CheckAllProcessIsIdle(QMap<QString,QString>& procState);
	CtrlError CheckAllProcessIsWork(QMap<QString, QString>& procState);
public:
	QString m_MainProc;//启动流程
	QString m_ResetProc;//复位流程
	QString m_RestInput;//复位IO	
	QString	m_StartInput;//启动IO
	QString	m_PauseInput;//暂停IO
	QString	m_StopInput;//停止IO
	bool m_bAtuo;//自动模式标志
	MachineStatus m_machineStatus;
	//计数区
	QMutex m_stateMutex;//状态锁
	QMutex m_autoMutex;//自动状态锁
	QList<CParentProcess*> m_procList;//流程容器
	QThread* m_status_thread;
	bool m_bExit;
	QMutex m_mutexCount;//计数锁
};
extern MachineManager theMachineManager;
