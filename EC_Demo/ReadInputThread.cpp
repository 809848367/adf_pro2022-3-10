﻿#include "ReadInputThread.h"
#include <QString>
#include <QDebug>
#include "DataAnalysis.h"
#include "DatabaseManager.h"
#include "MachineManager.h"

void ReadInputThread::ExitInputThread()
{
	if (this->isRunning())
	{
		m_bExit = true;
		m_bStart = false;
		this->wait();
		//qDebug() << "ExitInputThread ";
	}
}

void ReadInputThread::run()
{
	do {
		if (m_bExit)
			break;
		QList<uint8_t> InputList;
		//刷新IO状态

		if (!m_bStart)
		{
			msleep(10);
			continue;
		}
		else {
			QString Reset =  theMachineManager.GetResetInput();
			QString Start =  theMachineManager.GetStartInput();
			QString Pause =  theMachineManager.GetPauseInput();
			QString Stop =  theMachineManager.GetStopInput();

			if (m_inputPreMap[Reset]!= m_inputMap[Reset]&& m_inputMap[Reset]== 1)
			{
				//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Reset);
				theMachineManager.MachineReset();
			}
			if (m_inputPreMap[Start] != m_inputMap[Start] && m_inputMap[Start] == 1)
			{
				//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Work);
				theMachineManager.MachineStart();
			}
			if (m_inputPreMap[Pause] != m_inputMap[Pause] && m_inputMap[Pause] == 1)
			{
				//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Pause);
				theMachineManager.MachinePause();
			}
			if (m_inputPreMap[Stop] != m_inputMap[Stop] && m_inputMap[Stop] == 1)
			{
				//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Stop);
				theMachineManager.MachineStop();
			}

		}
		foreach(QString inputName, m_inputMap.keys())
			m_inputPreMap[inputName] = m_inputMap[inputName];
	} while (true);
	qDebug() << "thread work fun jump loop";
}

ReadInputThread::ReadInputThread(QObject *parent)
	: QThread(parent)
{
	m_bExit = false;
	m_bStart = false;
}

ReadInputThread::~ReadInputThread()
{

}

void ReadInputThread::InitInputMap(QStringList inputList)
{
	foreach(QString input, inputList)
	{
		m_inputMap[input] = 0;
		m_inputPreMap[input] = 0;
	}
}

void ReadInputThread::SetStartFlag(bool state)
{
	m_bStart = state;
}
