#pragma once

#include <QDialog>
#include "ui_DevelopmentDlg.h"
#include <QtCore>

class DevelopmentDlg : public QDialog
{
	Q_OBJECT

public:
	DevelopmentDlg(QWidget *parent = Q_NULLPTR);
	~DevelopmentDlg();
	void RecordDevelopment();

private:
	Ui::DevelopmentDlg ui;
};
