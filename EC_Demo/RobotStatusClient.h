﻿#ifndef ROBOTSTATUSCLIENT_H
#define ROBOTSTATUSCLIENT_H

#include <QtCore>
#include <QTcpSocket>
#include <atomic>
#include "RobotDefine.h"
#include "CtrlError.h"

class RobotStatusClient : public QObject
{
    Q_OBJECT
public:
    explicit RobotStatusClient(QObject *parent = nullptr);
    ~RobotStatusClient();
    CtrlError Open(QString Name,QString IP,quint16 Port);
    CtrlError Stop();
    RobotStatus GetStatus();
private:
    QString Name;
    QString IP;
    quint16 Port;
    QTcpSocket *Socket;
    RobotStatus Status;

    template<typename T>
    void GetParam(QByteArray &Data,T &Param);
signals:
private slots:
    void RecvStatus();
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00202000,
        ClientHasOpen           = Base + 0x001,
        ClientOpenFail          = Base + 0x002,
        ClientSetParaErr        = Base + 0x003,
        ClientNotOpen           = Base + 0x004,
    };
    Q_ENUM(ErrorCode)
};

#endif // ROBOTSTATUSCLIENT_H
