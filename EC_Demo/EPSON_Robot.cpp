#include "EPSON_Robot.h"
#include "CFileOperator.h"
#include <QHostAddress>

EPSON_Robot::EPSON_Robot(QObject *parent)
	: HardwareModule(parent)
{
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}
	m_client = Q_NULLPTR;
}

EPSON_Robot::~EPSON_Robot()
{
}

CtrlError EPSON_Robot::Init(HardwareInitData InitData)
{
	if (InitData.Keys.size() != 2) return CtrlError(InitDataErr, InitData.Name);
	if (InitData.InitData.size() != 1) return CtrlError(InitDataErr, InitData.Name);
	if (InitData.InitData[0].size() != 2) return CtrlError(InitDataErr, InitData.Name);

	for (int i = 0; i < InitData.InitData.size(); i++)
	{
		if (CtrlError(NoError) != Init(InitData.Name ,InitData.InitData[i][0].toString(), InitData.InitData[i][1].toInt()))
			return CtrlError(InitDataErr, InitData.Name);
	}

	return CtrlError(NoError);
}

CtrlError EPSON_Robot::Init(QString name, QString ip, int port)
{
	if (m_client == Q_NULLPTR)
		m_client = new ClientThread(ip, port);
	qDebug() << "ip" << ip << "port" << port;
	if (m_client->IsConnectServer())
		return CtrlError(ClientHasOpen);
	m_client->start();
	QTime t;
	t.start();
	while (1)
	{
		if (t.elapsed()>3000)
			return CtrlError(ConnectionEPSONError, name);
		if (m_client->IsConnectServer())
			break;
	}
	return CtrlError(NoError);
}

CtrlError EPSON_Robot::SendAndRecv(CmdData cmd, CmdData& respond)
{
	return m_client->SendCmdWaitRespond(cmd, respond);
}

CtrlError EPSON_Robot::WaitRobotCmd(CmdData& cmd, int tOut)
{
	QTime t;
	t.start();
	while (true)
	{
		if (t.elapsed() > tOut &&tOut != 0)	break;
		if (0 == m_client->CatchOneServerCmd(cmd)) return CtrlError(NoError);
		QThread::msleep(10);
	}
	return CtrlError(ResTimeOut);
}

CtrlError EPSON_Robot::Execute(HardwareCommData ExecuteData, HardwareCommData& ResultData)
{
	ResultData.Station = ExecuteData.Station;
	ResultData.Identfication = ExecuteData.Identfication;
	ResultData.OperatorName = ExecuteData.OperatorName;
	qDebug() << ExecuteData.Station;
	if (!ExecuteData.Station.contains("EPSONRobot"))
		return CtrlError(ModuleNameError);

	CmdData cmdData;
	cmdData.indetify = ExecuteData.Identfication;
	cmdData.OperatorType = ExecuteData.OperatorName;

	if (!ExecuteData.Datas.contains("TimeOut")) return CtrlError(SendCmdDataError);
	if (!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(SendCmdDataError);
	cmdData.timeOut = ExecuteData.Datas["TimeOut"].toInt();
	cmdData.msgType = ExecuteData.Datas["MsgType"].toString();

	if (ExecuteData.OperatorName == "SendAndRecv") 
	{
		if (!ExecuteData.Datas.contains("Cmd")) return CtrlError(SendCmdDataError);
		cmdData.sData = ExecuteData.Datas["Cmd"];
		CmdData respond;
		CtrlError eRet = SendAndRecv(cmdData, respond);
		
		if (0 != eRet)
			return eRet;
		qDebug() << respond.sData.toString() << endl;
		ResultData.Datas.insert("Respond", QVariant::fromValue<CmdData>(respond));
	}
	else if (ExecuteData.OperatorName == "WaitRobotCmd")
	{
		CmdData servCmd;
		CtrlError eRet = WaitRobotCmd(servCmd, cmdData.timeOut);
		ResultData.Datas.insert("ServerCmd", QVariant::fromValue<CmdData>(servCmd));

	}
	else
		return CtrlError(ModuleParaFuncNotContain);
	return CtrlError(NoError);
}

