﻿#ifndef ROBOTCOMMANDCLIENT_H
#define ROBOTCOMMANDCLIENT_H

#include <QtCore>
#include <QTcpSocket>
#include <atomic>
#include "CtrlError.h"

class RobotCommandClient : public QObject
{
    Q_OBJECT
public:
    explicit RobotCommandClient(QObject *parent = nullptr);
    CtrlError Open(QString Name,QString IP,quint16 Port);
    CtrlError Stop();
    CtrlError SendAndRecv(QByteArray Cmd,QByteArray &Res);
    CtrlError Send(QByteArray Cmd,int &ID);
    CtrlError Recv(int ID,QByteArray &Res);
    CtrlError WaitForRecv(int ID,QByteArray &Res);
private:
    void thread_Send();
    std::atomic<int> threadFlag;                //0未运行，1运行中，2准备停止，3准备关闭线程，4已关闭线程（关闭线程后不可开启）
    QTcpSocket *Socket;
    QList<QPair<int,QByteArray>> SendList;      //发送清单，这里不需要查询，所以只有一个
    QMap<int,QByteArray> RecvMap;               //接收清单
    QMutex SendMutex;                           //发送清单的线程锁
    QMutex RecvMutex;                           //接收数据的线程锁
    QString Name;
    QString IP;
    quint16 Port;
    int ID;
    QMutex IDLock;
    std::atomic<int> IDNow;                     //当前发送信息的ID
    const int timeout = 2000;                   //发送后接受超时时间
signals:
    void SendAndRecvMsg(QByteArray Msg);
private slots:
    void SendAndRecvMsgSlot(QByteArray Msg);
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00201000,
        ClientHasOpen           = Base + 0x001,
        ClientOpenFail          = Base + 0x002,
        ClientSetParaErr        = Base + 0x003,
        ClientNotOpen           = Base + 0x004,
        RecvDataNotExist        = Base + 0x005,
    };
    Q_ENUM(ErrorCode)
};

#endif // ROBOTCOMMANDCLIENT_H
