#pragma once

#include <QWidget>
#include "ui_CProcessWidget.h"
#include <QString>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTreeView>
#include <QItemSelectionModel>
#include <QPoint>
#include <QList>
#include "DataAnalysis.h"

class CProcessWidget : public QWidget
{
	Q_OBJECT

public:
	CProcessWidget(QWidget *parent = Q_NULLPTR);
	~CProcessWidget();
	void Init();
	void InitWindow();
	void InitConnection();
	void InitMenu();

	//int LoadProcessXML(QString procXML);//加载流程配置文件

	void TreeviewInit();
	void TableviewInit();

	void StationInit();

	void refreshTreeView();
	void refreshTableView(StepInfo * sInfo);
	//操作帮助
	void RefreshHelpContent(QString op_type);

signals:
	void SigOperator(OperatorInfo* execOpera);	//单步执行操作
	void SigStep(StepInfo* step);		//单步执行步骤


public slots:
	void Slot_CurrentRunStep(QString procName,int step);

	void LoadProcessBtn();
	void RefreshProcessBtn();
	void SaveAsBtn();
	void ResortBtn();
	void SaveBtn();
//treeview tableview 
	void selectionChangedTreeSlot(const QItemSelection &selected, const QItemSelection &deselected);
	void selectionChangedTableSlot(const QItemSelection &selected, const QItemSelection &deselected);
	void tableviewContextMenuSlot(QPoint pos);
	void treeviewContextMenuSlot(QPoint pos);

	void ResetProc();

	void AddOneRunProc(QString procName);
	void DeleteOneRunProc(QString procName);


	//menu slot
	void on_actTreeRunSlot();		// proc run 
	void on_actTreeAddSlot();		// add proc 
	void on_actTreeInsertSlot();	// insert proc
	void on_actTreeDeleteSlot();	// delete proc
	void on_actTreeInvalidSlot();	// invalid proc
	void on_actTreeEnableSlot();	// invalid proc
	void on_actTreeCopySlot();	
	void on_actTreePasteSlot();	

									
	//act slot table 
	void on_actTabRunSlot();
	void on_actTabAddSlot();
	void on_actTabInsertSlot();
	void on_actTabDeleteSlot();
	void on_actTabInvaildSlot();
	void on_actTabEnableSlot();
	void on_actTabCopySlot();
	void on_actTabPasteSlot();

	// step  operator 
	void on_actStepRunSlot();		// run step 
	void on_actStepAddSlot();		// add step
	void on_actStepInsertSlot();	// insert step
	void on_actStepDeleteSlot();	// delete step
	void on_actStepInvalidSlot();	// invalid step
	void on_actStepEnableSlot();	// invalid step
	void on_actStepCopySlot();
	void on_actStepPasteSlot();

	//change operator
	void on_ensure_change_slot();
	void on_cancle_slot();


	void on_EnsureStepBtnSlot();
	//station
	void ChooseIndexChanged(QString content);
	void OperatorTypeTextChanged(QString curStr);


	void AbleOperatorWidget();
	void DisableOperatorWidget();

	void ExitRunProc();

private:
	void RefreshRunProcCombox();

private:
	Ui::CProcessWidget ui;
	QStandardItemModel* m_treeModel;
	QStandardItemModel* m_tableModel;
	QItemSelectionModel* m_treeSelectModel;
	QItemSelectionModel* m_tableSelectModel;


	QMenu*		m_treeMenu;
	QAction*	m_actTreeRun;
	QAction*	m_actTreeAdd;
	QAction*	m_actTreeInsert;
	QAction*	m_actTreeDelete;
	QAction*	m_actTreeInvalid;
	QAction*	m_actTreeEnable;
	QAction*	m_actTreeCopy;
	QAction*	m_actTreePaste;
	//step Menu 
	QMenu*		m_StepMenu;
	QAction*	m_actStepRun;
	QAction*	m_actStepAdd;
	QAction*	m_actStepInsert;
	QAction*	m_actStepDelete;
	QAction*	m_actStepInvalid;
	QAction*	m_actStepEnable;
	QAction*	m_actStepCopy;
	QAction*	m_actStepPaste;
	//operator Menu
	QMenu*		m_tableMenu;
	QAction*	m_actTabRun;
	QAction*	m_actTabAdd;
	QAction*	m_actTabInsert;
	QAction*	m_actTabDelete;
	QAction*	m_actTabInvalid;
	QAction*	m_actTabEnable;
	QAction*	m_actCopy;//拷贝
	QAction*	m_actPaste;//粘贴
	//当前选择的
	QString m_treeSelectStr;//treeview 选取的流程标识
	QString m_tabSelectStr;//选取操作标识号
	QString m_SelTreeContent;
	QString m_SelStepIdntify;//步骤标识
	QString m_SelProcIdntify;//流程标识


	int m_tabSelectRowIndex;//选取操作标识Tab中的行号
	int m_treeRowIndex;//选取步骤的行号

	QStandardItemModel* m_stationModel;
	QStandardItemModel* m_operaModel;

	ProcessInfo*	m_tempProc;
	OperatorInfo*	m_tempOperator;
	StepInfo*		m_tempStep;

	int m_treeViewScollValue;
	QScrollBar * m_scrollBar;

	QStringList m_runProcList;
};
