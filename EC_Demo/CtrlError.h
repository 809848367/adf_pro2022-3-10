﻿#ifndef CTRLERROR_H
#define CTRLERROR_H

#include <QtCore>

class CtrlError
{
    Q_GADGET
public:
    CtrlError();
    CtrlError(int ErrCode);
    CtrlError(int ErrCode,QString Key);
    CtrlError &SetKey(QString Key);
    operator int();
    operator QString();
    static void RegisterErr(int ErrCode, QString ErrStr);
    static QString GetErrStr(int ErrCode);
    static QMap<int,QString> ErrorList;
private:
    int ErrCode;
    QString Key;
};

#endif // CTRLERROR_H
