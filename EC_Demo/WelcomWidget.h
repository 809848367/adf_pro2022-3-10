#pragma once

#include <QWidget>
#include "ui_WelcomWidget.h"
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTreeView>
#include <QItemSelectionModel>
#include "DspLable.h"
#include "CParentProcess.h"

class WelcomWidget : public QWidget
{
	Q_OBJECT

public:
	WelcomWidget(QWidget *parent = Q_NULLPTR);
	~WelcomWidget();
	void Init();
	void InitWindow();
	void InitConnection();
	void InitOther();
	virtual void timerEvent(QTimerEvent *event);

public slots:
	void RegisterProcMsg();
	void DispMsg(QString msg, int type=0);//添加一条消息
	void ShowProInfo(QString msg, int type = 0);//添加一条消息
	void DispMsgWithModule(QStringList prefixList, QString msg, int type = 0);
	void StateRefresh(QString procName, CParentProcess::ProcessState state);
	//void SignalSendImg(QMap<QString, QVariant> Datas);
	void RecvImgInfo(QMap<QString, QVariant> Datas);
	
	void RefreshMachineInfo(QString result);
	void RefreshDsipImage(QList<DspLable*> dispLabList);//刷新图片显示
	void MachineStatus(QString status);
	//btn Slot
	void on_StartBtnSlot();
	void on_PauseBtnSlot();
	void on_StopBtnSlot();
	void on_ResetBtnSlot();
	void on_LoginBtnSlot();
	void Set_ProcListSlot(QList<CParentProcess*> procList);
//主流程结束发送状态用以确认正常或者异常结束
	void SetMachineStatusSlot(bool bException);
	void Slot_AutoCheckSlot(int state);

	void ClearMsgBtn();
	void ResetBtn();
	void ClearCount();

	void on_SelectDispNums(QString selStr);
	//void Set
signals:
	void SignalMsg(QString msgint, int type = 0);
	void SignalLogin(QString str);
	void SignalMachine(QString str);
public:
	
	Ui::WelcomWidget ui;
private:
	
	QStandardItemModel* m_tableModel;
	QItemSelectionModel* m_tableSelectModel;
	QList<CParentProcess*> m_procList;//流程容器

	QList<DspLable*> old_dispLabList;
	int m_countFreshID;
};
