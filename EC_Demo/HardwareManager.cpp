﻿#include "HardwareManager.h"
#include "SerialPortManager.h"
#include "Lighting.h"
#include "PanaMotor.h"
#include "Conveyor.h"
#include "Robot.h"
#include "Camera.h"
#include "IOControl.h"
#include "AlgoManager.h"
#include "EPSON_Robot.h"
#include "PLC.h"

HardwareManager::HardwareManager(QObject *parent) : QObject(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
}

HardwareManager *HardwareManager::GetInstance()
{
    static HardwareManager Instance;
    return &Instance;
}

HardwareManager::~HardwareManager()
{
    for(int i=InitOrder.size();i>0;i--)
    {
        delete Modules[InitOrder[i-1]];
    }
}

CtrlError HardwareManager::InitModule(HardwareInitData InitData)
{
    HardwareModule *tModule;
    if(Modules.contains(InitData.Name)) return CtrlError(ModuleHasInit,Name);
    if(InitData.Type == "Robot")
    {
        tModule = new Robot();
    }
    else if(InitData.Type == "SerialPortManager")
    {
        tModule = new SerialPortManager();
    }
    else if(InitData.Type == "Lighting")
    {
        tModule = new Lighting();
    }
    else if(InitData.Type == "PanaMotor")
    {
        tModule = new PanaMotor();
    }
    else if(InitData.Type == "Conveyor")
    {
        tModule = new Conveyor();
    }
    else if(InitData.Type == "Camera")
    {
        tModule = new Camera();
    }
    else if(InitData.Type == "IOControl")
    {
        tModule = new IOControl();
    }
    else if(InitData.Type == "AlgoManager")
    {
        tModule = new AlgoManager();
    }
	else if (InitData.Type == "EPSON_Robot")
	{
		tModule = new EPSON_Robot();
	}
	else if (InitData.Type == "PLC")
	{
		tModule = new PLC();
	}
	else if (InitData.Type == "EPSONRobot")
	{
		tModule = new EPSON_Robot();
	}
    else return CtrlError(ModuleTypeNotExist,Name);
    Modules.insert(InitData.Name,tModule);
    InitOrder.push_back(InitData.Name);
    return Modules[InitData.Name]->Init(InitData);
}

CtrlError HardwareManager::InitModules(QVector<HardwareInitData> InitDatas)
{
    CtrlError ret;
    for(int i=0;i<InitDatas.size();i++)
    {
        ret = InitModule(InitDatas[i]);
        if(ret != 0) return ret;
    }
    return CtrlError(NoError);
}

CtrlError HardwareManager::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    if(!Modules.contains(ExecuteData.Station)) return CtrlError(ModuleNotExist,Name);
    //LogManager::GetInstance().WriteLog("Execute",ExecuteData.Station,QString::number((int)QThread::currentThreadId(),16) + " " +  ExecuteData.OperatorName);
    CtrlError ret = Modules[ExecuteData.Station]->Execute(ExecuteData,ResultData);
    //LogManager::GetInstance().WriteLog("Execute",ExecuteData.Station, QString::number((int)QThread::currentThreadId(), 16) + " " + ExecuteData.OperatorName + " " + ret);
    return ret;
}

HardwareModule *HardwareManager::GetModulePtr(QString Name)
{
    return Modules[Name];
}
