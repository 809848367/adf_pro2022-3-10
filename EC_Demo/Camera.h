﻿#ifndef CAMERA_H
#define CAMERA_H

#include <QtCore>
#include "HardwareModule.h"
#include "CtrlError.h"
#include "opencv2/opencv.hpp"
#include "CameraDefine.h"
Q_DECLARE_METATYPE(cv::Mat)
class Camera : public HardwareModule
{
    Q_OBJECT
public:
    explicit Camera(QObject *parent = nullptr);
    ~Camera();
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    CtrlError Init(QString Name,QString SN);
    CtrlError Open();
    CtrlError Close();
    CtrlError GetStatus(int &Status);
    CtrlError GetImageAndSave(QString Path, double Exposure, double Gamma, double Gain);
    CtrlError GetImage(cv::Mat &Image, double Exposure, double Gamma, double Gain);
	CtrlError COGNEX_CAM_GetImage(cv::Mat &Image, double Exposure, double Gamma, int Gain);
	CtrlError COGNEX_CAM_Open();
private:
    void* CameraHandle;
    QString SN;
    QString Name;
	QMutex m_catchImgMutex;
signals:

public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00400000,
        EnumDevicesErr          = Base + 0x001,
        CameraSNNotExist        = Base + 0x002,
        CreateHandleErr         = Base + 0x003,
        CameraOpenErr           = Base + 0x004,
        StartGrabbingErr        = Base + 0x005,
        ParaSetErr              = Base + 0x006,
        ParaGetErr              = Base + 0x007,
        GetImageErr             = Base + 0x008,
        InitDataErr             = Base + 0x009,
        ExecuteDataNotRight     = Base + 0x00a,
		SaveImageError			= Base + 0x00b,
    };
    Q_ENUM(ErrorCode)
};

#endif // CAMERA_H
