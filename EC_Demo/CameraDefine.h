#ifndef CAMERADEFINE_H
#define CAMERADEFINE_H
#include <QtCore>

struct CameraParam
{
    double Exposure;
    double Gamma;
    double Gain;
};
Q_DECLARE_METATYPE(CameraParam)
#endif // CAMERADEFINE_H
