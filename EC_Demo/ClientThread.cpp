#include "ClientThread.h"
#include <QHostAddress>

ClientThread::ClientThread(QString ip, qint16 port, QObject *parent)
	:IP(ip), Port(port), QThread(parent)
{
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}
	m_bRun = false;
}

ClientThread::~ClientThread()
{
}	

void ClientThread::run()
{
	m_socket = new QTcpSocket;
	if (m_socket->isOpen()) return;
	m_socket->connectToHost(QHostAddress(IP), Port);
	if (!m_socket->waitForConnected())
		return;
	m_bRun = true;
	while (true)
	{
		QThread::msleep(10);
#pragma region sendAndRecv 
		if (cmdList.size() > 0)
		{
			qDebug() << "cmdList.size��" << cmdList.size();
			CmdData cmd = GetOneCmdDataFromList();
			qDebug() << "cmdList.size��" << cmdList.size();
			QByteArray resData;
			//func name
			QTime t;
			t.start();
			if (CtrlError(NoError) != SendAndRecv(cmd.sData.toString().toLocal8Bit(), resData,cmd.timeOut))
			{
				if (t.elapsed() > cmd.timeOut)//��ʱ���ݶ���
				{
					cmd.resInfo = "TimeOut";
					continue;
				}
				cmd.msgType = "serverRespond";
				cmd.resInfo = "error";
				PushOneRespondDataToList(cmd);
			}
			if (t.elapsed() > cmd.timeOut&& cmd.timeOut!=0)
			{
				cmd.resInfo = "TimeOut";
				cmd.msgType = "serverRespond";
				PushOneRespondDataToList(cmd);
				continue;
			}
			cmd.resInfo = "success";
			cmd.msgType = "serverRespond";
			cmd.sData = QVariant(QString(resData));
			PushOneRespondDataToList(cmd);
		}
#pragma endregion
#pragma region recvData
		QByteArray serverData;
		if (0 ==Recv(serverData))
		{
			if (serverData.length() == 0) continue;
			CmdData serverCmd;
			serverCmd.indetify = 0;
			serverCmd.resInfo = "success";
			serverCmd.msgType = "serverCmd";
			serverCmd.sData = QVariant(QString(serverData));
			qDebug() << QString(serverData);
			//PushOneRespondDataToList(serverCmd);
			PushOneServerCmd(serverCmd);
		}
#pragma endregion 

	}
	qDebug() << "PLC Thread exit";
}

CtrlError ClientThread::SendCmdWaitRespond(CmdData send, CmdData& respond)
{
	PushOneCmdDataToList(send);
	return WaitDataByIdnetiy(send.indetify,send.timeOut,respond);
}

bool ClientThread::IsConnectServer()
{
	return m_bRun;
}

void ClientThread::PushOneCmdDataToList(CmdData data)
{
	m_cmdMutex.lock();//������
	cmdList.push_back(data);
	m_cmdMutex.unlock();//������
}

void ClientThread::PushOneRespondDataToList(CmdData data)
{
	m_resMutex.lock();//��Ӧ��
	respondList.push_back(data);
	m_resMutex.unlock();//��Ӧ��
}

CmdData ClientThread::GetOneRespondDataFromList()
{
	CmdData respond;
	if (respondList.size() == 0)
		return respond;
	m_resMutex.lock();//������
	respond = respondList.front();
	respondList.pop_front();
	m_resMutex.unlock();//������
	return respond;
}

CmdData ClientThread::GetOneCmdDataFromList()
{
	m_cmdMutex.lock();//������
	CmdData cmd = cmdList.front();
	cmdList.pop_front();
	m_cmdMutex.unlock();//������
	return cmd;
}

CtrlError ClientThread::WaitDataByIdnetiy(int indetify, int tOut,CmdData& respond)
{
	QTime t;
	t.start();
	while (1)
	{
		QThread::msleep(10);
		foreach(CmdData data, respondList)
		{
			if (data.indetify == indetify)
			{
				respond = GetOneRespondDataFromList();
				return ErrorCode(NoError);
			}
		}
		if (t.elapsed() > tOut&& tOut != 0)
		{
			respond.resInfo = "TimeOut";
			return ErrorCode(TimeOutError);
		}
			
	}
	return true;
}

CtrlError ClientThread::SendAndRecv(QByteArray cmd, QByteArray& respond,int timeOut)
{
	if (!m_socket->isOpen())
		return CtrlError(ConnectionPLCError);
	m_socket->write(cmd);
	if (!m_socket->waitForBytesWritten())
		return CtrlError(SocketError);
	if (timeOut>0)
	{
		if (!m_socket->waitForReadyRead(timeOut))
			return CtrlError(SocketError);
	}
	else
	{
		if (!m_socket->waitForReadyRead(0xFFFFFFFF))
			return CtrlError(SocketError);
	}

	respond.clear();
	while (m_socket->bytesAvailable() > 0)
	{
		respond.append(m_socket->readAll());
		if (respond[respond.size() - 1] != '\n')
			continue;
		else break;
	}
	return CtrlError(NoError);
}

CtrlError ClientThread::Recv(QByteArray& recvData)
{
	if (!m_socket->waitForReadyRead(10))
		return CtrlError(TimeOutError);
	while (m_socket->bytesAvailable() > 0)
	{
		recvData.append(m_socket->readAll());
		if (recvData[recvData.size() - 1] != '\n')
			continue;
		else break;
	}
	return CtrlError(NoError);
}

CtrlError ClientThread::CatchOneServerCmd(CmdData& serverCmd)
{
	QMutexLocker locker(&m_serverCmdMutex);
	if (serverCmdList.size() == 0)
		return CtrlError(NoSeverCmd);
	serverCmd = serverCmdList.first();
	serverCmdList.removeFirst();
	return CtrlError(NoError);
}

void ClientThread::PushOneServerCmd(CmdData& serverCmd)
{
	QMutexLocker locker(&m_serverCmdMutex);
	serverCmdList.push_back(serverCmd);
}
