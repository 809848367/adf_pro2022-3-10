#pragma once
#include <QObject>
#include <QString>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtDebug>
#include <QSqlError>
#include "StationMotion.h"
#include <QMutex>
#include "ZTH_TAB_STRUCT.h"
#include "CtrlError.h"



//#include <QtCore>
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")
#endif

//database table struct
//card struct 
struct CardS
{
	QString cardType;
	int cardIndex;
	int inputCount;
	int outputCount;
	int externIoCard;
	int BelongCard;
	int axisCount;
	QString cfgPath;
	QString cardName;
};

//Motor
struct MotorParam
{
	QString axisName;
	int axisType;
	int cardIndex;
	int axisIndex;
	//PHAND pHand;		
	//UINT_PTR AxisHandle;
	bool bInit;
	bool bHome;
	bool bSevOn;
	bool bRotating;
	double dbStoke;
	long dbStepRound;
	double dbAcc;		//mm/s^2
	double dbDec;		//mm/s^2
	double dbVelLow;	//mm/s
	double dbVelHigh;	//mm/s

	double posLmt;//+ mm
	double negLmt;//-
	bool bUseLmt;

	int stationID;
	double workSpeed;
	double limtSpeed;
	double homeSpeed;
	//HomePara homePara;	
	MotorParam() {
		axisName = "";
		cardIndex = 0;
		axisIndex = 0;
		//pHand = NULL;
		bInit = false;
		bHome = false;
		bSevOn = false;
		posLmt = 9999.0;
		negLmt = 9999.0;
		bUseLmt = false;
		stationID = 0;
		workSpeed = 1;
		limtSpeed = 50;
		homeSpeed = 2;
	};
};

//input 
struct InputParam
{
	QString name;
	int cardIndex;
	int portIndex;
	int negate;
	int ID;
	int value;//2U 机箱输出数值
	InputParam()
	{
		name = "";
		cardIndex = 0;
		portIndex = 0;
		negate = 0;
		ID = 0;
		value = 0;
	}
};

//output
struct OutputParam
{
	QString name;
	int cardIndex;
	int portIndex;
	int negate;
	int ID;
	int value;//2U 机箱输出数值
	OutputParam()
	{
		name = "";
		cardIndex = 0;
		portIndex = 0;
		negate = 0;
		ID = 0;
		value = 0;
	}
};

//product 
struct Product
{
	int ProductID;
	QString productName;
	Product()
	{
		ProductID = 1;
		productName = "";
	}
};

//Ctrl Box
struct CtrlBox
{
	int ID;
	QString CardName;
	QString COMName;
	int CardCount;
	CtrlBox()
	{
		ID = 1;
		CardName = "";
		COMName = "COM9";
		CardCount = 2;
	}
};
//light
struct LigthParam
{
	QString name;
	int cardIndex;
	int portIndex;
	int ID;
	int value;//灯箱当前值
	LigthParam()
	{
		name = "";
		cardIndex = 0;
		portIndex = 0;
		ID = 0;
		value = 0;
	}
};

//UserInfo
struct UserInfo
{

	int ID;
	QString user_Name;
	QString psw;
	int level;//用户等级 0:未设置 1:超级用户 2:技术员

	UserInfo()
	{
		ID = 0;
		user_Name = "";
		psw = "";
		level = 0;
	}
};


//SafeMovePos
//2021-1-8  添加safemove 功能
struct SafePos
{
	QString posName;//点位名称
	QString stationName;//工站名称
	int axisIndex;//如果轴号为 0 ;那就整体运动 否则单轴运动

	int priority;//优先级
	int belong;//归属 于 safemove
	int ID;
	SafePos()
	{
		stationName = "";
		posName = "";
		priority = 0;
		belong = 1;
		ID = 0;
	}
};
//safeMove

//多个安全点位 进行
class SafeMove
{
public:
	SafeMove();

public:
	QString name;
	int ID;
	//工站 、点位 、优先级
	QList<SafePos> posList;
	void SortPosList();
	void quickSort();
};

struct comm_struct
{
	QString name;//biao
	QString modouleType;

	QVector<QString> keys;
	QVector<QVector<QVariant>> data;


	//data[i].size() == keys.size()

};

struct FlagInfo {
	int ID;
	QString name;
	int value;
	QString description;
};



#define DB_PATH "./../config/MotionData_CtrlBox.db"//数据库文件
class CDatabaseManager : public QObject
{
	Q_OBJECT
public:		
	
	CDatabaseManager(QObject *parent = NULL);
	~CDatabaseManager();
	bool InitDataBase();
	//读表操作
	bool ReadUserInfo(QList<UserInfo>&  UserInfoList, QVector<QString>& fliedNames);
	bool ReadFlagInfo(QList<FlagInfo*>&  FlagInfoList, QVector<QString>& fliedNames);


	bool ReadTotalTable(QList< S_Total >& totalTable, QVector<QString>& fliedNames);
	bool ReadRobot1(QList< S_Robot >& Robot1, QVector<QString>& fliedNames);
	bool ReadRobot2(QList< S_Robot >& Robot2, QVector<QString>& fliedNames);
	bool ReadSerialPortManager(QList< S_SerialPortManager >& SerialPortManager, QVector<QString>& fliedNames);
	bool ReadLighting1(QList< S_Lighting > &Lighting1, QVector<QString>& fliedNames);
	bool ReadCamera1(QList< S_Camera > &Camera1, QVector<QString>& fliedNames);
	bool ReadLighting2(QList< S_Lighting >& Lighting2, QVector<QString>& fliedNames);
	bool ReadCamera2(QList< S_Camera >& Camera2, QVector<QString>& fliedNames);
	bool ReadLightingUp(QList< S_Lighting > &LightingUp, QVector<QString>& fliedNames);
	bool ReadCameraUp(QList< S_Camera > &CameraUp, QVector<QString>& fliedNames);
	bool ReadCameraDown(QList< S_Camera > &CameraDown, QVector<QString>& fliedNames);
	bool ReadLightingDown(QList< S_Lighting >& LightingDown, QVector<QString>& fliedNames);
	bool ReadLightingNG(QList< S_Lighting >& LightingNG, QVector<QString>& fliedNames);
	bool ReadCameraNG(QList< S_Camera >& CameraNG, QVector<QString>& fliedNames);
	bool ReadConveyor1(QList< S_Conveyor >& Conveyor1, QVector<QString>& fliedNames);
	bool ReadConveyor2(QList< S_Conveyor > &Conveyor2, QVector<QString>& fliedNames);
	bool ReadPanaMotor1(QList< S_PanaMotor >& PanaMotor1, QVector<QString>& fliedNames);
	bool ReadPanaMotor2(QList< S_PanaMotor >& PanaMotor2, QVector<QString>& fliedNames);
	bool ReadIOControl(QList< S_IOControl >& IOControl, QVector<QString>& fliedNames);


	bool ReadAllTables();

	QList<S_Total>				GetTotalInfo();
	QList<UserInfo>				GetUserInfo();
	QList<FlagInfo*>			GetFlagInfo();
	QList<S_Robot>				GetRobot1();
	QList<S_Robot>				GetRobot2();
	QList<S_SerialPortManager>	GetSerialPortManager();
	QList<S_Lighting>			GetLighting1();
	QList<S_Camera>				GetCamera1();
	QList<S_Lighting>			GetLighting2();
	QList<S_Camera>				GetCamera2();
	QList<S_Lighting>			GetLightingUp();
	QList<S_Camera>				GetCameraUp();
	QList<S_Lighting>			GetLightingDown();
	QList<S_Lighting>			GetLightingNG();
	QList<S_Camera>				GetCameraNG();
	QList<S_Conveyor>			GetConveyor1();
	QList<S_Conveyor>			GetConveyor2();
	QList<S_PanaMotor>			GetPanaMotor1();
	QList<S_PanaMotor>			GetPanaMotor2();
	QList<S_IOControl>			GetIOControl();




	//get table fileNames 
	//QVector<QString> 
	int GetTableFiledNamesByTableName(QString tableName, QVector<QString>& fliedNames);

	//点位更新 
	bool UpdatePosTab(CStationMotion station, PointStation * Point);
	bool ResetPosTab(CStationMotion station, PointStation * Point);
	
	//更新flag 说明
	bool UpdateFlagTableDescription(QString flagName,QString description);
	

	//增加
	bool AddValueByTable(TeachTable teachData);
	//删除
	bool DeleteValueByTableAndFiled(TeachTable teachData);
	//修改
	bool UpdateValueByTableAndFiled(TeachTable teachData);

	//查询表所有数据
	bool SearchValueByTableString(QString tabName, QVector<QString>&fileds  , QVector< QVector<QString>>& values);
	bool SearchValueByTableQVariant(QString tabName, QVector<QString>&fileds, QVector< QVector<QVariant>>& values);


	bool RefreshTeachDataByTabName(QString tableName,QString modelName,QString modelType);

signals:
	CtrlError SendTableDataSignal(TeachTable Table);

public slots:
//数据更新
//void RefreshTeachData(QString Station, TeachData_Robot		tTeachData);
//void RefreshTeachData(QString Station, TeachData_Lighting		tTeachData);
//void RefreshTeachData(QString Station, TeachData_PanaMotor	tTeachData);

void RefreshTeachData(TeachTable tTeachData);

protected:
	//快捷调试 //填充safeMove里面点位
	void FillSafeMove(QList<SafeMove>&  m_SafeMoveList, QList<SafePos>  m_SafePosList);

public:
	QSqlDatabase db;
	QMutex m_mutex;

	QList<S_Total>				m_Total;
	QList<UserInfo>				m_UserInfoList;
	QList<FlagInfo*>			m_FlagInfoList;
	QList<S_Robot>				m_Robot1;
	QList<S_Robot>				m_Robot2;
	QList<S_SerialPortManager>	m_SerialPortManager;
	QList<S_Lighting>			m_Lighting1;
	QList<S_Camera>				m_Camera1;
	QList<S_Lighting>			m_Lighting2;
	QList<S_Camera>				m_Camera2;
	QList<S_Lighting>			m_LightingUp;
	QList<S_Camera>				m_CameraUp;
	QList<S_Camera>				m_CameraDown;
	QList<S_Lighting>			m_LightingDown;
	QList<S_Lighting>			m_LightingNG;
	QList<S_Camera>				m_CameraNG;
	QList<S_Conveyor>			m_Conveyor1;
	QList<S_Conveyor>			m_Conveyor2;
	QList<S_PanaMotor>			m_PanaMotor1;
	QList<S_PanaMotor>			m_PanaMotor2;
	QList<S_IOControl>			m_IOControl;
	//title
	QVector<QString>			m_Title_Total;
	QVector<QString>			m_Title_UserInfoList;
	QVector<QString>			m_Title_FlagInfoList;
	QVector<QString>			m_Title_Robot1;
	QVector<QString>			m_Title_Robot2;
	QVector<QString>			m_Title_SerialPortManager;
	QVector<QString>			m_Title_Lighting1;
	QVector<QString>			m_Title_Camera1;
	QVector<QString>			m_Title_Lighting2;
	QVector<QString>			m_Title_Camera2;
	QVector<QString>			m_Title_LightingUp;
	QVector<QString>			m_Title_CameraUp;
	QVector<QString>			m_Title_CameraDown;
	QVector<QString>			m_Title_LightingDown;
	QVector<QString>			m_Title_LightingNG;
	QVector<QString>			m_Title_CameraNG;
	QVector<QString>			m_Title_Conveyor1;
	QVector<QString>			m_Title_Conveyor2;
	QVector<QString>			m_Title_PanaMotor1;
	QVector<QString>			m_Title_PanaMotor2;
	QVector<QString>			m_Title_IOControl;
};
extern CDatabaseManager theDbManager;

