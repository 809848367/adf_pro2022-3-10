#include "ImageDispWidget.h"
#include "CParentProcess.h"
#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 
ImageDispWidget::ImageDispWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidgetResizable(false);
	
	//for (int i = 0; i != 10; ++i)
	//{
	//	DspLable* lab = new DspLable(ui.sclWdgtContent);
	//	lab->setText(QString("%1").arg(i));
	//	QImage *img = new QImage; //新建一个image对象
	//	img->load("C://Users//sfai//Desktop//measure.jpg");
	//	lab->setPixmap(QPixmap::fromImage(*img));
	//	lab->setPicPath("C://Users//sfai//Desktop//measure.jpg");
	//	lab->setDispInfo(QString("NG%1KKKKKKKKKKKK").arg(i));
	//	m_ListControl << lab;
	//}
	autoRedraw();
    m_LinNums = 5;
}

ImageDispWidget::~ImageDispWidget()
{
}

void ImageDispWidget::autoRedraw()
{
	static bool refresh = true;
	if (refresh == false)
		return;
	if (m_ListControl.count() <= 0)
		return;
	refresh = false;
	int lineNums = m_LinNums;
	//int lineNums = 1;
	int squareSide = (ui.scrollArea->width()-30) / lineNums;//-30 保留右侧拉伸

	for (int i = 0; i != m_ListControl.count(); ++i)
	{
		DspLable * btn = m_ListControl.at(i);
		btn->move(10 + i % lineNums * (squareSide), 10 + i / lineNums * (squareSide));
		btn->resize(squareSide-20, squareSide-20);
		//信息显示lab
		QLabel* lab = btn->getInfoLab();
		lab->setParent(ui.sclWdgtContent);
		//lab->show();
		lab->resize(squareSide - 20, 20);
		lab->move(10 + i % lineNums * (squareSide), 10 + i / lineNums * (squareSide)+squareSide-20);
		//QCoreApplication::processEvents();
	}
	ui.sclWdgtContent->resize((squareSide)*lineNums, (m_ListControl.count() / lineNums +1)* squareSide + 10);
	refresh = true;
}

void ImageDispWidget::resizeEvent(QResizeEvent *event)
{
	autoRedraw();
}

void ImageDispWidget::setLineDispNums(int lineNums)
{
	if (lineNums <= 0)
		return;
	m_LinNums = lineNums;
}

void ImageDispWidget::setDispList(QList<DspLable*> ListControl)
{
	if (ListControl.count() <= 0)
		return;
	m_ListControl = ListControl;
	foreach(DspLable* dspLab,ListControl)
	{
		dspLab->setParent(ui.sclWdgtContent);
		//QLabel* lab = dspLab->getInfoLab();
		//lab->setParent(ui.sclWdgtContent);
	}
    autoRedraw();
}


void ImageDispWidget::RefreshImagDisp(QMap<QString, QVariant> Datas)
{
	//if (m_ListControl.size() > 0)
	//{
	//	for (int i = 0; i < Datas.keys().size(); i++)
	//	{
	//		Result_Data data = Datas[Datas.keys()[i]].value<Result_Data>();
	//		DspLable* lab = m_ListControl[i];
	//		lab->setParent(NULL);
	//		QLabel*infoLab = lab->getInfoLab();
	//		infoLab->setParent(NULL);
	//		lab->deleteLater();
	//		infoLab->deleteLater();
	//	}
	//	m_ListControl.clear();
	//}
	if (Datas.count() == 0) return;
	//清除页面
	int count = m_ListControl.count();
	for (int i = 0; i < count; i++)
	{
		m_ListControl[0]->setParent(NULL);
		QLabel*infoLab = m_ListControl[0]->getInfoLab();
		m_ListControl[0]->deleteLater();
		infoLab->deleteLater();
		m_ListControl.removeFirst();
	}


	//重新创建加载



	foreach(QString face, Datas.keys())
	{
		Result_Data data = Datas[face].value<Result_Data>();
		DspLable* lab = new DspLable(ui.sclWdgtContent);
		lab->setPicPath(data.reslutImgPath);
		lab->setDispInfo(data.result);
		QFileInfo fileInfo(data.reslutImgPath);
		if (!fileInfo.exists())
			lab->setPixmap(QPixmap::fromImage(QImage()));
		else
			lab->setPixmap(QPixmap::fromImage(QImage(data.reslutImgPath)));
		lab->setParent(ui.sclWdgtContent);
		lab->show();
		QLabel*infoLab = lab->getInfoLab();
		infoLab->setParent(ui.sclWdgtContent);
		infoLab->show();
		m_ListControl.push_back(lab);
	}
	autoRedraw();
}



//void ImageDispWidget::RefreshImagDisp(QMap<QString, QVariant> Datas)
//{
//	if (m_ListControl.size()>0)
//	{
//		for (int i = 0; i < Datas.keys().size();i++)
//		{
//			Result_Data data = Datas[Datas.keys()[i]].value<Result_Data>();
//			DspLable* lab = m_ListControl[i];
//			lab->setParent(ui.sclWdgtContent);
//			lab->setPicPath(data.reslutImgPath);
//			lab->setDispInfo(data.result);
//			QFileInfo fileInfo(data.reslutImgPath);
//			if (!fileInfo.exists())
//				lab->setPixmap(QPixmap::fromImage(QImage()));
//			else
//				lab->setPixmap(QPixmap::fromImage(QImage(data.reslutImgPath)));
//		}
//		autoRedraw();
//		return;
//	}
//
//	foreach(QString face, Datas.keys())
//	{
//		Result_Data data = Datas[face].value<Result_Data>();
//		DspLable* lab = new DspLable(ui.sclWdgtContent);
//		lab->setPicPath(data.reslutImgPath);
//		lab->setDispInfo(data.result);
//		QFileInfo fileInfo(data.reslutImgPath);
//		if (!fileInfo.exists())
//			lab->setPixmap(QPixmap::fromImage(QImage()));
//		else
//			lab->setPixmap(QPixmap::fromImage(QImage(data.reslutImgPath)));
//		lab->setParent(ui.sclWdgtContent);
//		lab->show();
//		m_ListControl.push_back(lab);
//	}
//	autoRedraw();
//}
