#include "WelcomWidget.h"
#include <QMessageBox>
#include <QTime>
#include "QLoginDlg.h"
#include <QDebug>
#include "QiniOperator.h"
#include "CParentProcess.h"
//#include "MachineControl.h"
#include "CFileOperator.h"
#include "MachineManager.h"


#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 
WelcomWidget::WelcomWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	Init();
	InitWindow();
	InitConnection();
	InitOther();
	ui.DsipNumsComBox->setCurrentText("4");
}

WelcomWidget::~WelcomWidget()
{
	killTimer(m_countFreshID);
}

void WelcomWidget::Init()
{

}

void WelcomWidget::InitWindow()
{
	m_tableModel = new QStandardItemModel(ui.tableView);
	m_tableModel->setHorizontalHeaderLabels(QStringList() << "time" << "type" << "msg");
	ui.tableView->setModel(m_tableModel);
	ui.tableView->horizontalHeader()->setStyleSheet(
		"QHeaderView::section {" "color: black;padding-left: 4px;border: 1px solid #6c6c6c;}");
	ui.tableView->verticalHeader()->hide();
	ui.tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	ui.tableView->setColumnWidth(0, 200);
	RefreshMachineInfo("");
	QString contentstr = "1,2,3,4,5,6,7,8,9,10";
	ui.DsipNumsComBox->addItems(contentstr.split(","));

	QPalette pe;
	pe.setColor(QPalette::WindowText, Qt::darkGreen);
	ui.ResultLab->setPalette(pe);
	ui.ResultLab->setText("OK");
}

void WelcomWidget::InitConnection()
{
	connect(ui.StartBtn, &QPushButton::clicked, this, &WelcomWidget::on_StartBtnSlot);
	connect(ui.PauseBtn, &QPushButton::clicked, this, &WelcomWidget::on_PauseBtnSlot);
	connect(ui.StopBtn, &QPushButton::clicked, this, &WelcomWidget::on_StopBtnSlot);
	connect(ui.ResetBtn, &QPushButton::clicked, this, &WelcomWidget::on_ResetBtnSlot);
	connect(this, &WelcomWidget::SignalMsg, this, &WelcomWidget::DispMsg);
	connect(ui.clearMsgBtn, &QPushButton::clicked, this, &WelcomWidget::ClearMsgBtn);
	connect(ui.ClearCountBtn, &QPushButton::clicked, this, &WelcomWidget::ClearCount);
	//connect(ui.ResetBtn_Btn, &QPushButton::clicked, this, &WelcomWidget::ResetBtn);
	connect(ui.LoginBtn, &QPushButton::clicked, this, &WelcomWidget::on_LoginBtnSlot);

	foreach(CParentProcess* proc, theMachineManager.GetProcList())
	{
		connect(proc, &CParentProcess::SignalSendImg, this, &WelcomWidget::RecvImgInfo);
		connect(proc, &CParentProcess::SignalMachineRefresh, this, &WelcomWidget::RefreshMachineInfo);
		connect(proc, &CParentProcess::SignalProcState, this, &WelcomWidget::StateRefresh,Qt::BlockingQueuedConnection);

		connect(proc, &CParentProcess::SignalShowInfo, this, &WelcomWidget::ShowProInfo);
	}
	connect(&theMachineManager, &MachineManager::Signal_Machine_Statu, this, &WelcomWidget::MachineStatus);
	connect(&theMachineManager, &MachineManager::SignalMachineMsg, this, &WelcomWidget::DispMsg);
	connect(ui.checkBox_Auto, &QCheckBox::stateChanged, this, &WelcomWidget::Slot_AutoCheckSlot);
	connect(ui.DsipNumsComBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(on_SelectDispNums(QString)));
}

void WelcomWidget::InitOther()
{
	m_countFreshID = startTimer(100);
}

void WelcomWidget::timerEvent(QTimerEvent *event)
{
	if (event->timerId() == m_countFreshID)
	{
		//ui.Total_lineEdit->setText(QString("%1").arg(theMachineManager.TotalCount));
		//ui.OK_lineEdit->setText(QString("%1").arg(theMachineManager.OKCount));
		//ui.NG_lineEdit->setText(QString("%1").arg(theMachineManager.NGCount));
		//if (theMachineManager.TotalCount == 0)
		//{
		//	ui.PassPrcent_lineEdit->setText(QString("%1").arg(0) + "%");
		//}
		//else
		//{
		//	ui.PassPrcent_lineEdit->setText(QString("%1").arg(theMachineManager.PASSPercent * 100) + "%");
		//}
		//ui.CycleTime_lineEdit->setText(QString("%1").arg(theMachineManager.CycleTime));
	}
}

void WelcomWidget::RegisterProcMsg()
{

}

void WelcomWidget::DispMsg(QString msg, int type)
{
	static int msgCount = 0;
	msgCount++;
	if (msgCount>100)
	{
		//m_tableModel->clear();
		m_tableModel->removeRows(0, 999);
		msgCount = 1;
		ui.tableView->setColumnWidth(0, 200);
		ui.tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
		//ui.tableView->setColumnWidth(0, 200);
	}
	QDateTime dateTime;
	QString curTimeStr = dateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
	qDebug() << curTimeStr;
	QList<QStandardItem*> dataRowList;
	QString typeStr;
	switch (type)
	{
	case 0:
		typeStr = "Noraml";
		break;
	case 1:
		typeStr = "Warnning";
		break;
	case -1:
		typeStr = "Error";
			break;
	default:
		typeStr = "UNKNOW";
	}

	dataRowList << new QStandardItem(curTimeStr) << new QStandardItem(typeStr) << new QStandardItem(msg);
	if (m_tableModel->rowCount()>0)
		m_tableModel->insertRow(0, dataRowList);
	else
		m_tableModel->appendRow(dataRowList);
	//m_tableModel->appendRow(dataRowList);
	theFileManager.WriteTodayFile_TXT(typeStr+"->"+ msg);

}

void WelcomWidget::ShowProInfo(QString msg, int type /*= 0*/)
{
	QString typeStr;
	QPalette pe;
	pe.setColor(QPalette::WindowText, Qt::white);
	
	switch (type)
	{
	case 0:
		//typeStr = "Noraml";
		pe.setColor(QPalette::WindowText, Qt::black);
		break;
	case 1:
		//typeStr = "Warnning";
		pe.setColor(QPalette::WindowText, Qt::darkBlue);
		break;
	case 2:
		//typeStr = "Error";
		pe.setColor(QPalette::WindowText, Qt::darkRed);
		break;
	default:
		//typeStr = "UNKNOW";
		pe.setColor(QPalette::WindowText, Qt::black);
	}
	ui.ShowInfoLabel->setPalette(pe);
	if (msg.isEmpty()||msg=="0")
		ui.ShowInfoLabel->setText("");
	else
		ui.ShowInfoLabel->setText(QString("%1").arg(msg));
	theFileManager.WriteTodayFile_TXT(msg);
}

void WelcomWidget::DispMsgWithModule(QStringList prefixList, QString msg, int type /*= 0*/)
{
	static int msgCount = 0;
	msgCount++;
	if (msgCount > 100)
	{
		m_tableModel->removeRows(0, 999);
		msgCount = 1;
		ui.tableView->setColumnWidth(0, 200);
		ui.tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	}
	QDateTime dateTime;
	QString curTimeStr = dateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
	//qDebug() << curTimeStr;
	QList<QStandardItem*> dataRowList;
	QString typeStr;

	QString showMsg;
	foreach(QString str, prefixList) 
		showMsg += QString("[%1]").arg(str);
	showMsg += msg;
	

	switch (type)
	{
	case 0:
		typeStr = "Noraml";
		break;
	case 1:
		typeStr = "Warnning";
		break;
	case -1:
		typeStr = "Error";
		ui.plainTextEdit->appendPlainText(curTimeStr + "-" + typeStr + ":" + showMsg);
		break;
	default:
		typeStr = "UNKNOW";
	}
	dataRowList << new QStandardItem(curTimeStr) << new QStandardItem(typeStr) << new QStandardItem(showMsg);

	if (m_tableModel->rowCount() > 0)
	{
		m_tableModel->insertRow(0, dataRowList);
		if (showMsg.contains("[STEP]"))
		{
			m_tableModel->item(0, 2)->setBackground(QColor(Qt::gray));
			m_tableModel->item(0, 1)->setBackground(QColor(Qt::gray));
		}
		if (typeStr == "Error")
		{
			m_tableModel->item(0, 1)->setBackground(QColor(255, 0, 128));;
		}
		else if (typeStr == "Warnning")
		{
			m_tableModel->item(0, 1)->setBackground(QColor(Qt::yellow));;
		}
	}
	else
	{
		m_tableModel->appendRow(dataRowList);
	}
	theFileManager.WriteTodayFile_TxtWithModule(prefixList, msg);
}

void WelcomWidget::StateRefresh(QString procName, CParentProcess::ProcessState state)
{
	if ( ui.checkBox_Auto->checkState() != Qt::Checked)
		return;
	QString resetProc = theMachineManager.GetResetProName();
	QString startProc = theMachineManager.GetStartProcName();
	if (resetProc == procName)
	{
		switch (state)
		{
		//case CParentProcess::Process_Status_Unknow:
		//	break;
		case CParentProcess::Process_Status_Idle:
			break;
		case CParentProcess::Process_Status_Start:
			ui.MachineStatusLab->setText("触发复位");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Reset);
			break;
		case CParentProcess::Process_Status_Work:
			ui.MachineStatusLab->setText("正在复位");
			break;
		case CParentProcess::Process_Status_Pause:
			ui.MachineStatusLab->setText("复位暂停");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Pause);
			break;
		case CParentProcess::Process_Status_Continue:
			ui.MachineStatusLab->setText("正在复位");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Reset);
			break;
		case CParentProcess::Process_Status_Stop:
			ui.MachineStatusLab->setText("复位停止");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_End:
			ui.MachineStatusLab->setText("复位结束");
			QThread::msleep(200);
			ui.MachineStatusLab->setText("空闲");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Idle);
			break;
		case CParentProcess::Process_Status_ErrorMoveEnd:
			ui.MachineStatusLab->setText("运动异常");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_ErrorInfoEnd:
			ui.MachineStatusLab->setText("消息异常");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_ErrorEnd:
			ui.MachineStatusLab->setText("异常结束");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_Test:
			break;
		default:
			break;
		}
	}
	else if (startProc == procName)
	{
		switch (state)
		{
		//case CParentProcess::Process_Status_Unknow:
		//	break;
		case CParentProcess::Process_Status_Idle:
			ui.MachineStatusLab->setText("空闲");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Idle);
			break;
		case CParentProcess::Process_Status_Start:
			ui.MachineStatusLab->setText("主流程启动");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Work);
			break;
		case CParentProcess::Process_Status_Work:
			ui.MachineStatusLab->setText("正在工作");
			break;
		case CParentProcess::Process_Status_Pause:
			ui.MachineStatusLab->setText("暂停");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Pause);
			break;
		case CParentProcess::Process_Status_Continue:
			ui.MachineStatusLab->setText("继续运行");
			QThread::msleep(200);
			ui.MachineStatusLab->setText("正在工作");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Work);
			break;
		case CParentProcess::Process_Status_Stop:
			ui.MachineStatusLab->setText("停止");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			QThread::msleep(500);
			ui.MachineStatusLab->setText("待复位");
			break;
		case CParentProcess::Process_Status_End:
			ui.MachineStatusLab->setText("结束");
			QThread::msleep(200);
			ui.MachineStatusLab->setText("空闲");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Idle);
			break;
		case CParentProcess::Process_Status_ErrorMoveEnd:
			ui.MachineStatusLab->setText("运动异常");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_ErrorInfoEnd:
			ui.MachineStatusLab->setText("消息异常");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_ErrorEnd:
			ui.MachineStatusLab->setText("异常退出");
			theMachineManager.SetMachineStatus(MachineManager::MachineStatus_WaitReset);
			break;
		case CParentProcess::Process_Status_Test:
			break;
		default:
			break;
		}
	}
}

void WelcomWidget::RecvImgInfo(QMap<QString, QVariant> Datas)
{
	ui.ImagDspWidget->RefreshImagDisp(Datas);
	//QList<DspLable*> dispLabList;
	//foreach(QString face,Datas.keys())
	//{
	//	Result_Data data = Datas[face].value<Result_Data>();
	//	QImage *img = new QImage;
	//	img->load(data.reslutImgPath);
	//	DspLable* lab = new DspLable();
	//	lab->setPicPath(data.reslutImgPath);
	//	lab->setDispInfo(data.result);
	//	lab->setPixmap(QPixmap::fromImage(*img));
	//	dispLabList.push_back(lab);
	//}
	//RefreshDsipImage(dispLabList);//刷新图片显示
	//for (int i = 0; i < old_dispLabList.count();i++)
	//{
	//	DspLable* lab = old_dispLabList.at(i);
	//	delete lab;
	//}
	//old_dispLabList.clear();
	//old_dispLabList = dispLabList;
}

void WelcomWidget::RefreshMachineInfo(QString result)
{
	QPalette pe;
	if (result == "OK")
	{
		pe.setColor(QPalette::WindowText, Qt::darkGreen);
		
	}
	else if (result == "NG")
	{
		pe.setColor(QPalette::WindowText, Qt::darkRed);
	}
	ui.ResultLab->setPalette(pe);
	ui.ResultLab->setText(result);

	ui.Total_lineEdit->setText(QString("%1").arg(theMachineManager.TotalCount));
	ui.OK_lineEdit->setText(QString("%1").arg(theMachineManager.OKCount));
	ui.NG_lineEdit->setText(QString("%1").arg(theMachineManager.NGCount));
	ui.PassPrcent_lineEdit->setText(QString("%1%").arg(theMachineManager.PASSPercent*100));
	ui.CycleTime_lineEdit->setText(QString("%1 S").arg(theMachineManager.CycleTime/1000));
	theMachineManager.m_Map_EndTime.remove(theMachineManager.TotalCount);
	theMachineManager.m_Map_StartTime.remove(theMachineManager.TotalCount);
}
void WelcomWidget::RefreshDsipImage(QList<DspLable*> dispLabList)
{
	if (dispLabList.count() <= 0)
		return;
	ui.ImagDspWidget->setDispList(dispLabList);
}

void WelcomWidget::MachineStatus(QString status)
{
	ui.MachineStatusLab->setText(status);
}

void WelcomWidget::on_StartBtnSlot()
{
	if (ui.checkBox_Auto->checkState() != Qt::Checked)
	{
		QMessageBox::warning(this, "提示", "非自动模式不允许使用该功能", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	
	MachineManager::MachineStatus status = theMachineManager.GetMachineStatus();
	if (status == MachineManager::MachineStatus_Idle)
	{
		//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Work);
		theMachineManager.MachineStart();
	}
	else
	{

		QMessageBox::warning(this, "提示", "设备不是空闲状态", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
	}
}
void WelcomWidget::on_PauseBtnSlot()
{
	if (ui.checkBox_Auto->checkState() != Qt::Checked)
	{
		QMessageBox::warning(this, "提示", "非自动模式不允许使用该功能", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	MachineManager::MachineStatus status = theMachineManager.GetMachineStatus();
	if (status != MachineManager::MachineStatus_Work && status != MachineManager::MachineStatus_Pause)
	{
		QMessageBox::warning(this, "提示", "设备不处于工作和暂停状态", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	else if (status == MachineManager::MachineStatus_Work)
	{
		//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Pause);
		theMachineManager.MachinePause();
	}
	else if (status == MachineManager::MachineStatus_Pause)
	{
		//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Continue);
		theMachineManager.MachineContinue();
	}
}
void WelcomWidget::on_StopBtnSlot()
{
	if (ui.checkBox_Auto->checkState() != Qt::Checked)
	{
		QMessageBox::warning(this, "提示", "非自动模式不允许使用该功能", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	MachineManager::MachineStatus status = theMachineManager.GetMachineStatus();
	if (status != MachineManager::MachineStatus_Work && status != MachineManager::MachineStatus_Pause&&status != MachineManager::MachineStatus_Reset)
	{
		QMessageBox::warning(this, "提示", "设备不处于工作、暂停和复位中状态无需停止", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}

	theMachineManager.MachineExit();
}
void WelcomWidget::on_ResetBtnSlot()
{
	if (ui.checkBox_Auto->checkState() != Qt::Checked)
	{
		QMessageBox::warning(this, "提示", "非自动模式不允许使用该功能", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	MachineManager::MachineStatus status = theMachineManager.GetMachineStatus();
	if (status == MachineManager::MachineStatus_Work)
	{
		QMessageBox::warning(this, "提示", "设备处于工作状态，等待设备停止再进行复位", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	else if (status == MachineManager::MachineStatus_Pause)
	{
		QMessageBox::warning(this, "提示", "设备处于暂停状态，请退出流程，等待设备停止再进行复位", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		return;
	}
	//theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Reset);
	theMachineManager.MachineReset();
}
void WelcomWidget::on_LoginBtnSlot()
{
	LOGIN_LEVEL level = theUserManager.GetCurrentUserLevel();
	//登录页面
	if (level != LOGIN_LEVEL_NULL)
	{
		int ret;
		switch (level)
		{
		case LOGIN_LEVEL_MANAGER:
			ret = QMessageBox::information(this, "提示", "已登录管理员账户是否登出", QMessageBox::Yes, QMessageBox::No);
			break;
		case LOGIN_LEVEL_TEC:
			ret = QMessageBox::information(this, "提示", "已登录技术员员账户是否登出", QMessageBox::Yes, QMessageBox::No);
			break;

		}

		switch (ret)
		{
		case QMessageBox::Yes:
			theUserManager.LoginOut();
			break;
		case QMessageBox::No:
			return;
			break;
		default:
			break;
		}

		level = theUserManager.GetCurrentUserLevel();
		switch (level)
		{
		case LOGIN_LEVEL_NULL:
			SignalLogin("未登陆");
			break;
		case LOGIN_LEVEL_MANAGER:
			SignalLogin("管理员");
			break;
		case LOGIN_LEVEL_TEC:
			SignalLogin("技术员");
			break;
		default:
			break;
		}


		return;
	}
		

	QLoginDlg dlg;
	dlg.exec();
	//刷新登录状态
	level = theUserManager.GetCurrentUserLevel();
	switch (level)
	{
	case LOGIN_LEVEL_NULL:
		SignalLogin("未登陆");
		break;
	case LOGIN_LEVEL_MANAGER:
		SignalLogin("管理员");
		break;
	case LOGIN_LEVEL_TEC:
		SignalLogin("技术员");
		break;
	default:
		break;
	}

}

void WelcomWidget::Set_ProcListSlot(QList<CParentProcess*> procList)
{
	m_procList = procList;
	foreach(CParentProcess* proc, theMachineManager.GetProcList())
	{
		connect(proc, &CParentProcess::SendProcMsg, this, &WelcomWidget::DispMsg);
		connect(proc, &CParentProcess::SendProMsgWithProfix, this, &WelcomWidget::DispMsgWithModule);
	}
		
}

void WelcomWidget::SetMachineStatusSlot(bool bException)
{
	//正常结束
	if (!bException)
	{
		theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Idle);
		ui.MachineStatusLab->setText("空闲");
	}
	//异常结束
	else
	{
		theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Error);
		ui.MachineStatusLab->setText("Error");
	}
}

void WelcomWidget::Slot_AutoCheckSlot(int state)
{
	if (Qt::Unchecked == state)
		theMachineManager.SetAtuo(false);
	else if (Qt::Checked == state)
		theMachineManager.SetAtuo(true);

}

void WelcomWidget::ClearMsgBtn()
{
	m_tableModel->clear();
	m_tableModel->setHorizontalHeaderLabels(QStringList() << "time" << "type" << "msg");
	ui.tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	ui.tableView->setColumnWidth(0, 200);
	ui.plainTextEdit->clear();
}

void WelcomWidget::ResetBtn()
{
	theMachineManager.SetExit(false);
	theMachineManager.SetMachineStatus(MachineManager::MachineStatus_Idle);
}

void WelcomWidget::ClearCount()
{
	theMachineManager.ClearCTData();
	RefreshMachineInfo("");
}

void WelcomWidget::on_SelectDispNums(QString selStr)
{
	int num = selStr.toInt();
	ui.ImagDspWidget->setLineDispNums(num);
}
