#include "FilePathManager.h"
#include <QCoreApplication>
#include "QiniOperator.h"
#if _MSC_VER >= 1600  
#pragma execution_character_set("utf-8")  
#endif  


FilePathManager::FilePathManager(QObject *parent)
	: QObject(parent)
{
}

FilePathManager::~FilePathManager()
{
}

FilePathManager* FilePathManager::Instance()
{
	static FilePathManager instance;
	return &instance;
}

void FilePathManager::ReadInitData()
{
	QString paramPath = QCoreApplication::applicationDirPath() + "/" + "pathManager.ini";
	iniOperator.setPath(paramPath);
	//filePath
	QString ProcPath, DataBasePath, teachDB, histroyDB, LogDir;
	iniOperator.readStringValue("filePath", "ProcessPath", ProcPath);
	iniOperator.readStringValue("filePath", "DataBasePath", DataBasePath);
	iniOperator.readStringValue("filePath", "teachDB", teachDB);
	iniOperator.readStringValue("filePath", "histroyDB", histroyDB);
	iniOperator.readStringValue("filePath", "LogDir", LogDir);
	m_pathMap.insert("ProcessPath", ProcPath);
	m_pathMap.insert("DataBasePath", DataBasePath);
	m_pathMap.insert("teachDB", teachDB);
	m_pathMap.insert("histroyDB", histroyDB);
	m_pathMap.insert("LogDir", LogDir);
	//MachineCfg
	QString StartProcess, RestProcess, RestInput, StartInput, PauseInput, StopInput;
	iniOperator.readStringValue("MachineCfg", "StartProcess", StartProcess);
	iniOperator.readStringValue("MachineCfg", "RestProcess", RestProcess);
	iniOperator.readStringValue("MachineCfg", "RestInput", StartProcess);
	iniOperator.readStringValue("MachineCfg", "StartInput", RestProcess);
	iniOperator.readStringValue("MachineCfg", "PauseInput", PauseInput);
	iniOperator.readStringValue("MachineCfg", "StopInput", StopInput);

	m_MachineCfgMap.insert("StartProcess", StartProcess);
	m_MachineCfgMap.insert("RestProcess", RestProcess);
	m_MachineCfgMap.insert("RestInput", StartProcess);
	m_MachineCfgMap.insert("StartInput", RestProcess);
	m_MachineCfgMap.insert("PauseInput", PauseInput);
	m_MachineCfgMap.insert("StopInput", StopInput);
	//count
	double PASSPercent, FailurePrecent, CycleTime, TotalCount, OKCount, NGCount;
	iniOperator.readDoubleValue("count",    "TotalCount", TotalCount);
	iniOperator.readDoubleValue("count",    "OKCount", OKCount);
	iniOperator.readDoubleValue("count",    "NGCount", NGCount);
	iniOperator.readDoubleValue("count", "PASSPercent", PASSPercent);
	iniOperator.readDoubleValue("count", "FailurePrecent", FailurePrecent);
	iniOperator.readDoubleValue("count", "CycleTime", CycleTime);

	m_countMap.insert("TotalCount", TotalCount);
	m_countMap.insert("OKCount", OKCount);
	m_countMap.insert("NGCount", NGCount);
	m_countMap.insert("PASSPercent", PASSPercent);
	m_countMap.insert("FailurePrecent", FailurePrecent);
	m_countMap.insert("CycleTime", CycleTime);
}

int FilePathManager::WriteDataToParamIni(QString section,QString key, QVariant value)
{
	if (section == "filePath")
	{
		m_pathMap.insert(key, value.toString());
	}
	else if (section == "MachineCfg")
	{
		m_MachineCfgMap.insert(key, value.toString());
	}
	else if (section == "count")
	{
		m_countMap.insert(key, value.toDouble());
	}
	else
		return NoSection;
	return  NoError;
}

void FilePathManager::WriteToFile()
{
	foreach(QString key, m_pathMap.keys())
		iniOperator.setValue("filePath", key, m_pathMap[key]);
	foreach(QString key, m_MachineCfgMap.keys())
		iniOperator.setValue("MachineCfg", key, m_MachineCfgMap[key]);
	foreach(QString key, m_countMap.keys())
		iniOperator.setValue("count", key, m_countMap[key]);
}

int FilePathManager::ReadFilePath(QString& value, QString key)
{
	if (!m_pathMap.contains(key))
		return KeyNotFind;
	value = m_pathMap[key];
	return NoError;
}

int FilePathManager::ReadMachineCfg(QString& value, QString key)
{
	if (!m_MachineCfgMap.contains(key))
		return KeyNotFind;
	value = m_MachineCfgMap[key];
	return NoError;
}

int FilePathManager::ReadCount(double& value, QString key)
{
	if (!m_countMap.contains(key))
		return KeyNotFind;
	value = m_countMap[key];
	return NoError;
}

