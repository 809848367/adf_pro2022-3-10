﻿#ifndef HARDWAREMANAGER_H
#define HARDWAREMANAGER_H

#include <QtCore>
#include "LogManager.h"
#include "HardwareModule.h"
#include "RobotDefine.h"
class HardwareManager : public QObject
{
    Q_OBJECT
    explicit HardwareManager(QObject *parent = nullptr);
public:
    static HardwareManager* GetInstance();
    ~HardwareManager();
    CtrlError InitModule(HardwareInitData InitData);
    CtrlError InitModules(QVector<HardwareInitData> InitDatas);
    CtrlError Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData);
    HardwareModule *GetModulePtr(QString Name);
private:
    const QString Name = "HardwareManager";
    QMap<QString,HardwareModule*> Modules;
    QVector<QString> InitOrder;
signals:
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00100000,
        ModuleHasInit       = Base + 0x001,
        ModuleNotExist      = Base + 0x002,
        ModuleTypeNotExist  = Base + 0x003,
    };
    Q_ENUM(ErrorCode)

};

#endif // HARDWAREMANAGER_H
