#include "Public_Test_Func.h"

Public_Test_Func::Public_Test_Func(QObject *parent)
	: QObject(parent)
{
}

Public_Test_Func::~Public_Test_Func()
{
}

int Public_Test_Func::Test01(cv::Mat Image, cv::Mat &ImageOut, double Thresd)
{
	cv::Mat Image2;
	cv::cvtColor(Image, Image2, cv::COLOR_BGR2GRAY);
	cv::Point2f Center;
	FindCenter(Image2, Center, Thresd);
	double Angle;
	Type1(Image2, Center, Angle);
	MoveImage(Image, Center, cv::Point2f(2000, 1500), Angle, 90, ImageOut);
	return 0;
}

int Public_Test_Func::FindCenter(cv::Mat SrcImage, cv::Point2f &Center, double Thresd)
{
	QVector<cv::Rect> Areas;

	Areas.push_back(cv::Rect(1602, 384, 578, 469 ));
	Areas.push_back(cv::Rect(927, 1249, 392, 482 ));
	Areas.push_back(cv::Rect(1777, 2107, 376, 287));
	Areas.push_back(cv::Rect(2530, 1145, 361, 492));
	Areas.push_back(cv::Rect(1129, 756, 299, 346 ));
	Areas.push_back(cv::Rect(2382, 745, 330, 349 ));
	Areas.push_back(cv::Rect(1191, 1800, 295, 311));
	Areas.push_back(cv::Rect(2386, 1758, 283, 345));
	cv::Mat DstImage = cv::Mat(SrcImage.rows, SrcImage.cols, CV_8UC1, cv::Scalar(0));
	for (int i = 0; i < Areas.size(); i++)
	{
		cv::Mat tImage = SrcImage(Areas[i]).clone();
		cv::GaussianBlur(tImage, tImage, cv::Size(5, 5), 0, 0);
		cv::Canny(tImage, tImage, Thresd, Thresd);
		cv::rectangle(tImage, Areas[i], cv::Scalar(0), 20);
		DstImage(Areas[i]).setTo(255, tImage);
	}
	std::vector<cv::Point> idx;
	cv::findNonZero(DstImage, idx);
	//    qDebug()<<idx.size();
	float R;
	cv::minEnclosingCircle(idx, Center, R);

	    //cv::circle(DstImage,Center,R,cv::Scalar(128),5);
	    //cv::namedWindow("AAA",0);
	    //cv::imshow("AAA",DstImage);
	    //cv::waitKey(0);
	return 0;
}

int Public_Test_Func::Type1(cv::Mat SrcImage, cv::Point2f Center, double &Angle)
{
	double d_Theta = M_PI / 180 / 5, s_Theta = 4 * M_PI;
	QVector<double> Poly;
	for (double i = 0; i < s_Theta; i += d_Theta)
	{
		double tValue = 0;
		for (int j = 0; j < 5; j++)
			tValue += SrcImage.at<uchar>((380 + 2 * j)*qSin(i) + Center.y, (380 + 2 * j)*qCos(i) + Center.x);
		Poly.push_back(tValue);
	}
	int N = 0;
	int E = -1;
	int S = -1;
	int Conti = 20;
	int Spec = 170;
	double MaxV = -1, MaxP = 0;
	for (int i = 0; i < Poly.size(); i++)
	{
		if (Poly[i] < Spec) N++;
		else
		{
			if (N > Conti) E = i;
			N = 0;
		}
		if (N == Conti)
		{
			S = i - Conti + 1;
			if (E != -1)
			{
				if (S - E > MaxV)
				{
					MaxV = S - E;
					MaxP = (S + E) / 2.0 / 5.0;
				}
			}
		}
	}
	Angle = MaxP;
	//    qDebug()<<Angle;
	//    qDebug()<<Poly;
	return 0;
}

int Public_Test_Func::MoveImage(cv::Mat SrcImage, cv::Point2f OldCenter, cv::Point2f NewCenter, double OldAngle, double NewAngle, cv::Mat &DstImage)
{
	DstImage = cv::Mat(SrcImage.rows, SrcImage.cols, SrcImage.type(), cv::Scalar(0, 0, 0));
	double t = NewAngle - OldAngle;
	double cost = qCos(t*M_PI / 180);
	double sint = qSin(t*M_PI / 180);
	double OffsetX = -NewCenter.x*cost - NewCenter.y*sint + OldCenter.x;
	double OffsetY = NewCenter.x*sint - NewCenter.y*cost + OldCenter.y;

	for (int ny = 0; ny < SrcImage.rows; ny++)
	{
		for (int nx = 0; nx < SrcImage.cols; nx++)
		{
			double ox, oy;
			ox = nx*cost + ny*sint + OffsetX;
			oy = -nx*sint + ny*cost + OffsetY;
			if (ox < 0 || ox >(SrcImage.cols - 1) || oy < 0 || oy >(SrcImage.rows - 1)) continue;
			double nColor1 = 0;
			cv::Vec3b nColor3 = cv::Vec3b(0, 0, 0);
			if (SrcImage.channels() == 1)
			{
				nColor1 = SrcImage.at<uchar>(qRound64(oy), qRound64(ox));
			}
			else
			{
				nColor3 = SrcImage.at<cv::Vec3b>(qRound64(oy), qRound64(ox));
			}
			if (DstImage.channels() == 1)
			{
				DstImage.at<uchar>(ny, nx) = nColor1;
			}
			else
			{
				DstImage.at<cv::Vec3b>(ny, nx) = nColor3;
			}
		}
	}
	return 0;
}
