#ifndef PANAMOTORUI_H
#define PANAMOTORUI_H

#include <QtCore>
#include <QWidget>
#include <QButtonGroup>
#include "HardwareManager.h"
#include "TeachData_PanaMotor.h"
namespace Ui {
class PanaMotorUI;
}

class PanaMotorUI : public QWidget
{
    Q_OBJECT

public:
    explicit PanaMotorUI(QWidget *parent = 0);
    ~PanaMotorUI();
    void SetHardwareManager(HardwareManager* HWM,QString Station);
private:
    Ui::PanaMotorUI *ui;
    HardwareManager* HWM;
    QString Station;
    QTimer RefreshTimer;
    bool isReseting;

    QButtonGroup TeachDataBtns;
    void InitTeachDataList();
    TeachData_PanaMotor TeachData;
    void ShowTeachData();
    bool ShowTeachDataFlag;
signals:
    void UpDateTeachData(QString Station,TeachData_PanaMotor TeachData);
private slots:
    void RefreshTimerSlot();
    void on_Enable_clicked();
    void on_Disable_clicked();
    void on_MovePos_clicked();
    void on_Reset_clicked();
    void on_Stop_clicked();
    void on_Pos_Del_clicked();
    void on_Pos_Add_clicked();

    void RefreshTeachData(QString Station,TeachData_PanaMotor TeachData);
    void TeachDataBtns_clicked(int n);
    void on_TeachAdd_clicked();
    void on_TeachDel_clicked();
    void on_TeachMove_clicked();
    void on_TeachData_cellChanged(int row, int column);
};

#endif // PANAMOTORUI_H
