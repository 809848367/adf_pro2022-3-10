#include "AlgoClient.h"
#include <QTcpServer>
#include "CFileOperator.h"
AlgoClient::AlgoClient(QObject *parent) : QObject(parent)
{
    Socket = nullptr;
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    HeartBeat.start(360000);
    connect(&HeartBeat,SIGNAL(timeout()),this,SLOT(HeartBeatSlot()));
}

AlgoClient::~AlgoClient()
{
    Socket->deleteLater();
}

CtrlError AlgoClient::Open(QString Name, QString IP, quint16 Port)
{
    this->Name = Name;
    this->IP = IP;
    this->Port = Port;
    if(Socket == nullptr) Socket = new QTcpSocket();
    if(Socket->isOpen()) return CtrlError(ClientHasOpen,Name);
    connect(Socket,SIGNAL(readyRead()),this,SLOT(RecvResult()),Qt::UniqueConnection);
    Socket->connectToHost(QHostAddress(IP),Port);
    if(!Socket->waitForConnected(3000)) return CtrlError(ClientOpenFail,Name);
    return CtrlError(NoError);
}

CtrlError AlgoClient::Close()
{
    if(!Socket->isOpen()) return CtrlError(ClientNotOpen,Name);
    Socket->disconnectFromHost();
    Socket->close();
    return CtrlError(NoError);
}

CtrlError AlgoClient::Send(QString Data)
{
    if(!Socket || !Socket->isOpen()) return CtrlError(ClientNotOpen,Name);
    Socket->write((Data+"#").toLocal8Bit());
    Socket->waitForBytesWritten(3000);
	theFileManager.WriteTodayFile_TXT(QString("AlgoSend->%1").arg(Data) );
    return CtrlError(NoError);
}

void AlgoClient::RecvResult()
{
    QList<QString> Data = QString::fromLocal8Bit(Socket->readAll()).split("#");
    for(int i=0;i<Data.size();i++)
    {
		if (!Data[i].isEmpty())
		{
			theFileManager.WriteTodayFile_TXT(QString("AlgoRecv->%1").arg(Data[i]));
			emit RecvResult(Name, Data[i]);
		}
    }
	
}

void AlgoClient::HeartBeatSlot()
{
    Send("{\"Module\":\"Main\",\"Command\":\"isConnected\"}");
}
