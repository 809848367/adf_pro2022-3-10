﻿#ifndef PANAMOTOR_H
#define PANAMOTOR_H

#include <QtCore>
#include "HardwareModule.h"
#include "CtrlError.h"
#include "SerialPortManager.h"
class PanaMotor : public HardwareModule
{
    Q_OBJECT
public:
    explicit PanaMotor(QObject *parent = nullptr);
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    CtrlError Init(QString Name,QString Com,int ID, int Vel, int Acc, int Dec, int Org, int PursePerRound, int EncoderPerRound, SerialPortManager *SPM);
    CtrlError Reset();
    CtrlError MovePos(int Pos);
    CtrlError Stop();
    CtrlError GetPos(int &Pos);
    CtrlError GetStatus(int &Status); //0:失能,1:使能,2:运行,3:报错
    CtrlError SetEnable(bool Enable);
    CtrlError WaitArrive(int Pos, int TimeOut);
    CtrlError WaitStop(int TimeOut);
    CtrlError ClearErr();

    CtrlError GetPosRounds(int &Round);
    CtrlError SetBlockID(int BlockID);
    CtrlError SetBlockVel(int Vel,int Acc,int Dec);

    CtrlError GetIsRunning(bool &isRunning);
    CtrlError GetEnable(bool &Enable);
    CtrlError GetIsErr(bool &isErr);
    CtrlError SetBlockPos(quint32 Pos);
    CtrlError MoveBlock();

private:
    SerialPortManager *SPM;
    QString Name;
    QString Com;
    int ID;
    int Vel,Acc,Dec;
    int Org;
    int PursePerRound,EncoderPerRound;

    QTime GetStatustimer;
    int GetStatustStatus;
    bool GetStatusFirst;

    QTime GetPostimer;
    int GetPostPos;
    bool GetPosFirst;

    void SetCRC16Modbus(QByteArray &Data);
signals:

public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00304000,
        ClearErrFail    = Base + 0x001,
        StartCmdErr     = Base + 0x002,
        StopCmdErr      = Base + 0x003,
        GetStatusCmdErr = Base + 0x004,
        SetEnableCmdErr = Base + 0x005,
        SetVelCmdErr    = Base + 0x006,
        ClearErrCmdErr  = Base + 0x007,
        WaitArrTimeout  = Base + 0x008,
        GetPosCmdErr    = Base + 0x009,
        SetBlockCmdErr  = Base + 0x00a,
        GetEnableCmdErr = Base + 0x00b,
        GetIsErrCmdErr  = Base + 0x00c,
        SetPosCmdErr    = Base + 0x00d,
        MoveBlockCmdErr = Base + 0x00e,
        GetIsRunningCmdErr = Base + 0x010,
        InitDataErr = Base + 0x011,
        ExecuteDataNotRight = Base + 0x012,
        ResetErr = Base + 0x013,
        AxisIsErr = Base + 0x014,
    };
    Q_ENUM(ErrorCode)

};

#endif // PANAMOTOR_H
