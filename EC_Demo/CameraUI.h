#ifndef CAMERAUI_H
#define CAMERAUI_H

#include <QtCore>
#include <QWidget>
#include "HardwareManager.h"
#include "opencv2/opencv.hpp"
#include "TeachData_Camera.h"
#include <QButtonGroup>
Q_DECLARE_METATYPE(cv::Mat)
namespace Ui {
class CameraUI;
}

class CameraUI : public QWidget
{
    Q_OBJECT

public:
    explicit CameraUI(QWidget *parent = 0);
    ~CameraUI();
    void SetHardwareManager(HardwareManager* HWM,QString Station);

private:
    Ui::CameraUI *ui;
    QString Station;
    HardwareManager* HWM;
    QTimer RefreshTimer;
    QButtonGroup TeachDataBtns;
    void InitTeachDataList();
    TeachData_Camera TeachData;
    void ShowTeachData();
    bool ShowTeachDataFlag;
signals:
    void UpDateTeachData(QString Station,TeachData_Camera TeachData);
private slots:
    void on_Open_clicked();
    void on_Close_clicked();
    void on_GetImage_clicked();
    void on_SaveImage_clicked();
    void RefreshTimerSlot();

    void RefreshTeachData(QString Station,TeachData_Camera TeachData);
    void TeachDataBtns_clicked(int n);
    void on_Teach_Add_clicked();
    void on_Teach_Del_clicked();
    void on_Teach_Get_clicked();
    void on_Teach_Save_clicked();
    void on_TeachData_cellChanged(int row, int column);
};

#endif // CAMERAUI_H
