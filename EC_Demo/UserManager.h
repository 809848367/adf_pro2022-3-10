#pragma once
#include <QString>
//用户权限管理模块
enum LOGIN_LEVEL
{
	LOGIN_LEVEL_NULL = 0,
	LOGIN_LEVEL_MANAGER,//管理员登录
	LOGIN_LEVEL_TEC,//技术员登录
};
class UserManager
{
public:
	UserManager();
	~UserManager();
	LOGIN_LEVEL GetCurrentUserLevel();
	QString Get_CurrentlevelName();
	bool Login(QString ,QString );
	void LoginOut();
private:
	LOGIN_LEVEL m_level;//用户等级
	QString m_userName;
};
extern UserManager theUserManager;
