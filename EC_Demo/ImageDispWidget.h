#pragma once

#include <QWidget>
#include "ui_ImageDispWidget.h"
#include <QList>
#include <QPushButton>
#include "DspLable.h"
class ImageDispWidget : public QWidget
{
	Q_OBJECT

public:
	ImageDispWidget(QWidget *parent = Q_NULLPTR);
	~ImageDispWidget();
	void autoRedraw();//自适应布局
	virtual void resizeEvent(QResizeEvent *event);

	void setLineDispNums(int lineNums);//每行显示控件个数
	void setDispList(QList<DspLable*> ListControl);//将要显示的内容 拷贝到 数据中使用 控件还是数据？ 控件对象较易使用

	void RefreshImagDisp(QMap<QString, QVariant> Datas);


private:
	Ui::ImageDispWidget ui;
	QList<DspLable*> m_ListControl;
	int m_LinNums;//默认设置成为两行 
};
