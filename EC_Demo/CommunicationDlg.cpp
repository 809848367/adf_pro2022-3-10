#include "CommunicationDlg.h"
CommunicationDlg* CommunicationDlg::Instance = Q_NULLPTR;


CommunicationDlg::CommunicationDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	Init();
	InitWindow();
	InitConnection();
}

CommunicationDlg::~CommunicationDlg()
{
}

bool CommunicationDlg::Init()
{
	return true;
}
void CommunicationDlg::InitWindow()
{
	Qt::WindowFlags windowFlag = Qt::Dialog;
	windowFlag |= Qt::WindowMinimizeButtonHint;
	windowFlag |= Qt::WindowMaximizeButtonHint;
	windowFlag |= Qt::WindowCloseButtonHint;
	this->setWindowFlags(windowFlag);

	CPage = new CommunicationPage;
	QVBoxLayout *verticalLayout;
	verticalLayout = new QVBoxLayout(ui.CenterWidget);
	CPage = new CommunicationPage(ui.CenterWidget);
	verticalLayout->addWidget(CPage);
}
void CommunicationDlg::InitConnection()
{

}
CommunicationDlg* CommunicationDlg::GetInstance()
{
	if (Instance == Q_NULLPTR)
		Instance = new CommunicationDlg;
	return Instance;
}
void CommunicationDlg::DispCtrlDlg()
{
	theCommunicationDlg->show();
}
void CommunicationDlg::ReleaseCtrlDlg()
{
	if (Instance != Q_NULLPTR)
		delete Instance;
	Instance = Q_NULLPTR;
}


