#include "DevelopmentDlg.h"
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 
DevelopmentDlg::DevelopmentDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	RecordDevelopment();
}

DevelopmentDlg::~DevelopmentDlg()
{
}

void DevelopmentDlg::RecordDevelopment()
{
	//QPlainTextEdit;
	QString developlog1 = "2021-11-19 14:48:58：开发完主页面控制单流程模式\n 版本号1.1\n";
	QString developlog2 = "2021-11-22 08:58:23：完善主页面暂停继续功能,添加流程强制退出等功能\n 版本号1.2\n";
	QString developlog3 = "2021-11-29 13:58:33：添加时间，优化了主页面log显示功能\n 版本号1.3\n";
	QString developlog4 = "2021-12-16 10:00:33：添加汇川ModuleBusRTU库，修改流程页面UI布局\n 版本号1.4\n";
	QString developlog5 = "2022-1-16 13:33:48：完善了汇川ModuleBusRTU库使用,添加了计数功能，修改了页面布局与图标\n 版本号1.4.1\n";
	QString developlog6 = "2022-2-13 14:29:38：完善了汇川ModuleBusRTU库使用，添加写入失败次数检测功能,\n 版本号1.4.2\n";
	ui.plainTextEdit->appendPlainText(developlog1+ developlog2+ developlog3 + developlog4 + developlog5 + developlog6);
}
