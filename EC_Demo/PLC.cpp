#include "CFileOperator.h"
#include "PLC.h"
#include <QHostAddress>
#include <QMetaObject>

PLC::PLC(QObject *parent)
	: HardwareModule(parent)
{
	m_client = nullptr;
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}

}

PLC::~PLC()
{

}

void PLC::HeartBeatSlot()
{
	//写 心跳
	//00 00 00 00 00 06 01 06 00 0a 00 01
	//QByteArray sendData;
	//char head[5] = { 0x00 };
	//sendData.append(char(0x00));
	//sendData.append(char(0x00));
	//sendData.append(char(0x00));
	//sendData.append(char(0x00));
	//sendData.append(char(0x00));
	//sendData.append(char(0x06));
	//sendData.append(char(0x01));
	//sendData.append(char(0x06));
	//sendData.append(char(0x00));
	//sendData.append(char(0x0A));
	//sendData.append(char(0x00));
	//sendData.append(char(0x01));
	//QByteArray recvData;
	//SendAndRecv(sendData, recvData);//通用发接指令
	//WritePort(PLC_Signal_Soft_Running,1);
	//if (int(recvData[8 - 1]) == 0x80 + 0x06 )
	//{
	//	HeartBeat.stop();
	//	theFileManager.WriteTodayFile_TXT1("write PLC heartBeat failed", CFileOperator::Error);
	//}
}

void PLC::stateChanged(QAbstractSocket::SocketState state)
{
	switch (state)
	{
	case QAbstractSocket::UnconnectedState:
		if (HeartBeat.isActive())
			HeartBeat.stop();
		break;
	}
}

CtrlError PLC::Init(HardwareInitData InitData)
{
	if (InitData.Keys.size() != 2) return CtrlError(InitDataErr, InitData.Name);
	if (InitData.InitData.size() != 1) return CtrlError(InitDataErr, InitData.Name);
	if (InitData.InitData[0].size() != 2) return CtrlError(InitDataErr, InitData.Name);

	for (int i = 0;i<InitData.InitData.size();i++)
	{
		if (CtrlError(NoError) != Init(InitData.Name, InitData.InitData[i][0].toString(), InitData.InitData[i][1].toInt()))
			return CtrlError(InitDataErr, InitData.Name);
	}
	//connect(&HeartBeat, SIGNAL(timeout()), this, SLOT(HeartBeatSlot()));//主线程中执行心跳操作
	//HeartBeat.start(50);
	//connect(&HeartBeat, SIGNAL(timeout()), this, SLOT(HeartBeatSlot()));//主线程中执行心跳操作
	return CtrlError(NoError);
}

CtrlError PLC::Init(QString name,QString ip, int port)
{
	m_client = new OneResponeClinet(ip,port);
	m_client->start();
	QThread::msleep(100);
	if (m_client->IsConnectServer())
		return CtrlError(NoError);
	else
		return CtrlError(ConnectionPLCError);
}

CtrlError PLC::Execute(HardwareCommData ExecuteData, HardwareCommData& ResultData)
{
	ResultData.Station = ExecuteData.Station;
	ResultData.Identfication = ExecuteData.Identfication;
	ResultData.OperatorName = ExecuteData.OperatorName;
	qDebug() << ExecuteData.Station;
	if (ExecuteData.Station != "PLC")
		return CtrlError(ModuleNameError);

	PLCData cmdData;
	cmdData.indetify = ExecuteData.Identfication;
	if (!ExecuteData.Datas["Port"].canConvert<int>())
		return CtrlError(ModuleParaCmdError);
	int port = ExecuteData.Datas["Port"].toInt();
	cmdData.port = port;
	cmdData.OperatorType = ExecuteData.OperatorName;

	if (ExecuteData.OperatorName == "ReadPort") //只要端口号 
	{
		int value;
		if (0 != ExecutePLC(cmdData))
			return CtrlError(ExecCmdError);
		ResultData.Datas.insert("Value", QVariant(cmdData.value));
	}
	else if (ExecuteData.OperatorName == "WritePort")
	{
		if (!ExecuteData.Datas.contains("Port"))
			return CtrlError(ModuleParaKeyNotContain);
		if (!ExecuteData.Datas["Port"].canConvert<int>())
			return CtrlError(ModuleParaCmdError);
		int port = ExecuteData.Datas["Port"].toInt();
		int value = ExecuteData.Datas["Value"].toInt();
		cmdData.value = value;
		if (0 != ExecutePLC(cmdData))
			return CtrlError(ExecCmdError);
	}
	else if (ExecuteData.OperatorName == "WaitPort")
	{
		if (!ExecuteData.Datas.contains("Port"))
			return CtrlError(ModuleParaKeyNotContain);
		if (!ExecuteData.Datas["Port"].canConvert<int>())
			return CtrlError(ModuleParaCmdError);
		int port = ExecuteData.Datas["Port"].toInt();
		int value = ExecuteData.Datas["Value"].toInt();
		int tOut = ExecuteData.Datas["TimeOut"].toInt();
		cmdData.value = value;
		cmdData.timeOut = tOut;
		if (0 != ExecutePLC(cmdData))
			return CtrlError(ExecCmdError);
	}
	else 
		return CtrlError(ModuleParaFuncNotContain);
	return CtrlError(NoError);
}

CtrlError PLC::ExecutePLC(PLCData& cmd)
{
	m_client->PushOneCmdDataToList(cmd);
	if (!m_client->WaitDataByIdnetiy(cmd.indetify, cmd.timeOut))
		return  CtrlError(ClientReadTimeOut);
	return CtrlError(NoError);
}
//
//CtrlError PLC::ReadPort(PLCData& cmd)
//{
//	m_client->PushOneRespondDataToList(cmd);
//	if (!m_client->WaitDataByIdnetiy(cmd.indetify, timeOut))
//		return  CtrlError(ClientReadTimeOut);
//	return CtrlError(NoError);
//}
//
//CtrlError PLC::WritePort(PLCData& cmd)
//{
//	m_client->PushOneRespondDataToList(cmd);
//	if (!m_client->WaitDataByIdnetiy(cmd.indetify, timeOut))
//		return  CtrlError(ClientReadTimeOut);
//
//	return CtrlError(NoError);
//}
//
//CtrlError PLC::WaitPort(PLCData& cmd)
//{
//	m_client->PushOneRespondDataToList(cmd);
//	if (!m_client->WaitDataByIdnetiy(cmd.indetify, cmd.timeOut))
//		return  CtrlError(ClientReadTimeOut);
//	return CtrlError(NoError);
//}

