﻿#include "RobotCommandClient.h"
#include <QTcpServer>
#include <thread>
#include "LogManager.h"
RobotCommandClient::RobotCommandClient(QObject *parent) : QObject(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    Socket = new QTcpSocket();
    ID = 0;
    connect(this,SIGNAL(SendAndRecvMsg(QByteArray)),this,SLOT(SendAndRecvMsgSlot(QByteArray)),Qt::UniqueConnection);
    threadFlag = 0;
    IDNow = -1;
    SendList.clear();
    std::thread t(&RobotCommandClient::thread_Send,this);
    t.detach();
}

CtrlError RobotCommandClient::Open(QString Name, QString IP, quint16 Port)
{
    this->Name = Name;
    this->IP = IP;
    this->Port = Port;
    if(Socket->isOpen()) return CtrlError(ClientHasOpen,Name);
    Socket->setReadBufferSize(366*1250);
    Socket->connectToHost(QHostAddress(IP),Port);
    if(!Socket->waitForConnected(3000)) return CtrlError(ClientOpenFail,Name);
    threadFlag = 1;
    return CtrlError(NoError);
}

CtrlError RobotCommandClient::Stop()
{
    threadFlag = 2;
    while(threadFlag == 2) QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    if(!Socket->isOpen()) return CtrlError(ClientNotOpen,Name);
    Socket->disconnectFromHost();
    Socket->waitForDisconnected(3000);
    Socket->close();
    return CtrlError(NoError);
}

CtrlError RobotCommandClient::SendAndRecv(QByteArray Cmd, QByteArray &Res)
{
    CtrlError ret;
    int ID;
    ret = Send(Cmd,ID);
    if(ret != NoError) return ret;
    ret = WaitForRecv(ID,Res);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError RobotCommandClient::Send(QByteArray Cmd, int &ID)
{
    CtrlError ret;
    IDLock.lock();
    ID = this->ID;
    if(this->ID++ >= 1000000) this->ID = 0;
    IDLock.unlock();
	SendMutex.lock();
    SendList.push_back(QPair<int,QByteArray>(ID,Cmd));
	SendMutex.unlock();
    return CtrlError(NoError);
}

CtrlError RobotCommandClient::Recv(int ID, QByteArray &Res)
{
    if(!RecvMap.contains(ID)) return CtrlError(RecvDataNotExist,Name);
    Res = RecvMap[ID];
    RecvMutex.lock();
    RecvMap.remove(ID);
    RecvMutex.unlock();
    return CtrlError(NoError);
}

CtrlError RobotCommandClient::WaitForRecv(int ID, QByteArray &Res)
{
    Res = "";
    while(1)
    {
        RecvMutex.lock();
        if(RecvMap.contains(ID))
        {
            Res = RecvMap[ID];
            RecvMap.remove(ID);
            RecvMutex.unlock();
            return CtrlError(NoError);
        }
        RecvMutex.unlock();
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

void RobotCommandClient::thread_Send()
{
    int ID;
    QByteArray Data;
    while(1)
    {
        //无数据发送时，10ms一次，防止卡机
        if(SendList.empty()) std::this_thread::sleep_for(std::chrono::milliseconds(10));
        //关闭模式
        if(threadFlag == 3)
        {
            threadFlag = 4;
            return;
        }
        if(threadFlag == 2) threadFlag = 0;
        if(threadFlag == 0) continue;
        //发送数据
        if(threadFlag == 1)
        {
            //存在正在发送的数据
            if(IDNow != -1) continue;
            //检查是否有数据发送
            if(!SendList.isEmpty())
            {
                SendMutex.lock();
                ID = SendList.first().first;
                Data = SendList.first().second;
                SendList.removeFirst();
                SendMutex.unlock();
            }
            else continue;
            //记录正在发送的ID号
            IDNow = ID;
            SendAndRecvMsg(Data);
        }
    }
}

void RobotCommandClient::SendAndRecvMsgSlot(QByteArray Msg)
{
    if(Socket->isOpen())
    {
		//LogManager::GetInstance().WriteLog("ModuleBus", Name, "Send:" + Msg);
        Socket->write(Msg);
        Socket->waitForBytesWritten(timeout);
        QByteArray Recv;
        while(true)
        {
            Socket->waitForReadyRead(timeout);
            if(Socket->bytesAvailable() > 0)
            {
                Recv.append(Socket->readAll());
                if(Recv[Recv.size()-1] != '\n') continue;
            }
            //LogManager::GetInstance().WriteLog("ModuleBus", Name, "Recv:" + Recv);
            RecvMutex.lock();
            RecvMap.insert(IDNow,Recv);
            RecvMutex.unlock();
            IDNow = -1;
            break;
        }
    }
}
