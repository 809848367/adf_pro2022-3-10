#include "PanaMotorUI.h"
#include "ui_PanaMotorUI.h"
#include <QMessageBox>
#include <QInputDialog>
PanaMotorUI::PanaMotorUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PanaMotorUI)
{
    ui->setupUi(this);
    isReseting = false;
    InitTeachDataList();
    connect(&TeachDataBtns,SIGNAL(buttonClicked(int)),this,SLOT(TeachDataBtns_clicked(int)));
    connect(&RefreshTimer,SIGNAL(timeout()),this,SLOT(RefreshTimerSlot()));
    RefreshTimer.start(200);
    RefreshTeachData(Station,TeachData_PanaMotor());
}

PanaMotorUI::~PanaMotorUI()
{
    delete ui;
}

void PanaMotorUI::SetHardwareManager(HardwareManager *HWM, QString Station)
{
    this->HWM = HWM;
    this->Station = Station;
}

void PanaMotorUI::InitTeachDataList()
{
    ui->TeachData->setColumnCount(3);
    ui->TeachData->horizontalHeader()->setVisible(true);
    ui->TeachData->horizontalHeader()->setFixedHeight(30);
    QList<QString> HItems;
    HItems<<"Name"<<"Value"<<"";
    ui->TeachData->setHorizontalHeaderLabels(HItems);
    ui->TeachData->verticalHeader()->setVisible(true);
    ui->TeachData->verticalHeader()->setFixedWidth(30);
    ui->TeachData->setColumnWidth(0,100);
    ui->TeachData->setColumnWidth(2,50);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);
}

void PanaMotorUI::ShowTeachData()
{
    ShowTeachDataFlag = true;
    while(TeachDataBtns.buttons().size() > 0) TeachDataBtns.removeButton(TeachDataBtns.buttons()[0]);
    ui->TeachData->clearContents();
    QList<QPair<QString, int>> Points = TeachData.GetPoints();
    ui->TeachData->setRowCount(Points.size()+1);
    for(int i=0;i<Points.size();i++)
    {
        ui->TeachData->setVerticalHeaderItem(i,new QTableWidgetItem(QString::number(i)));
        ui->TeachData->setItem(i,0,new QTableWidgetItem(Points[i].first));
        ui->TeachData->item(i,0)->setFlags(ui->TeachData->item(i,0)->flags() & ~Qt::ItemIsEditable);
        ui->TeachData->setItem(i,1,new QTableWidgetItem(QString::number(Points[i].second)));
        QPushButton *TeachDataBtn = new QPushButton();
        TeachDataBtn->setText("Now");
        TeachDataBtns.addButton(TeachDataBtn,i);
        ui->TeachData->setCellWidget(i,2,TeachDataBtn);
    }
    ui->TeachData->setVerticalHeaderItem(Points.size(),new QTableWidgetItem());
    for(int j=0;j<ui->TeachData->columnCount();j++)
    {
        ui->TeachData->setItem(Points.size(),j,new QTableWidgetItem());
        ui->TeachData->item(Points.size(),j)->setBackgroundColor(QColor(230,230,230));
        ui->TeachData->item(Points.size(),j)->setFlags(ui->TeachData->item(Points.size(),j)->flags() & ~Qt::ItemIsEditable);
    }
    ShowTeachDataFlag = false;
}

void PanaMotorUI::RefreshTimerSlot()
{
    if(!isVisible()) return;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<int>()) return;
    int Status = Recv.Datas["Status"].value<int>();
    Send = HardwareCommData();
    Recv = HardwareCommData();
    Send.Station = Station;
    Send.OperatorName = "GetPos";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) return;
    if(!Recv.Datas.contains("Pos") || !Recv.Datas["Pos"].canConvert<int>()) return;
    int Pos = Recv.Datas["Pos"].value<int>();

    switch(Status)
    {
    case 0:
        ui->Status->setText("Disable");
        break;
    case 1:
        ui->Status->setText("Enable");
        break;
    case 2:
        ui->Status->setText("Running");
        break;
    case 3:
        ui->Status->setText("Error");
        break;
    default:
        ui->Status->setText("Unknow");
        break;
    }

    ui->MovePos->setEnabled(Status == 1);
    ui->Reset->setEnabled(!isReseting);
    ui->Position->setText(QString::number(Pos));
}

void PanaMotorUI::on_Enable_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetEnable";
    Send.Identfication = 100;
    Send.Datas.insert("Enable",QVariant::fromValue(true));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_Disable_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "SetEnable";
    Send.Identfication = 100;
    Send.Datas.insert("Enable",QVariant::fromValue(false));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_MovePos_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "MovePos";
    Send.Identfication = 100;
    Send.Datas.insert("Pos",QVariant::fromValue(ui->MovePosTar->text().toInt()));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_Reset_clicked()
{
    isReseting = true;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Reset";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    isReseting = false;
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_Stop_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Stop";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_Pos_Del_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetPos";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Pos") || !Recv.Datas["Pos"].canConvert<int>()) return;
    int Pos = Recv.Datas["Pos"].value<int>();
    Send = HardwareCommData();
    Recv = HardwareCommData();
    Send.Station = Station;
    Send.OperatorName = "MovePos";
    Send.Identfication = 100;
    Send.Datas.insert("Pos",QVariant::fromValue(Pos-ui->PosStep->value()));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_Pos_Add_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetPos";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Pos") || !Recv.Datas["Pos"].canConvert<int>()) return;
    int Pos = Recv.Datas["Pos"].value<int>();
    Send = HardwareCommData();
    Recv = HardwareCommData();
    Send.Station = Station;
    Send.OperatorName = "MovePos";
    Send.Identfication = 100;
    Send.Datas.insert("Pos",QVariant::fromValue(Pos+ui->PosStep->value()));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::RefreshTeachData(QString Station, TeachData_PanaMotor TeachData)
{
    if(this->Station != Station) return;
    this->TeachData = TeachData;
    ShowTeachData();
}

void PanaMotorUI::TeachDataBtns_clicked(int n)
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetPos";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Pos") || !Recv.Datas["Pos"].canConvert<int>()) return;

    int Pos = Recv.Datas["Pos"].value<int>();
    TeachData.SetPoint(ui->TeachData->item(n,0)->text(),Pos);
    UpDateTeachData(Station,TeachData);
}

void PanaMotorUI::on_TeachAdd_clicked()
{
    QString Name = QInputDialog::getText(this,"Add","Input Add Name");
    if(Name.isEmpty()) return;
    TeachData.SetPoint(Name,0);
    UpDateTeachData(Station,TeachData);
}

void PanaMotorUI::on_TeachDel_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    if(QMessageBox::warning(this,"Warning",QString("Delete %1 ?").arg(Name),QMessageBox::Ok,QMessageBox::Cancel) != QMessageBox::Ok) return;
    TeachData.RemovePoint(Name);
    UpDateTeachData(Station,TeachData);
}

void PanaMotorUI::on_TeachMove_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    int tPoint;
    if(TeachData.GetPoint(Name,tPoint) != 0) return;

    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "MovePos";
    Send.Identfication = 100;
    Send.Datas.insert("Pos",QVariant::fromValue(tPoint));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void PanaMotorUI::on_TeachData_cellChanged(int row, int column)
{
    if(ShowTeachDataFlag) return;
    if(column == 0) return;
    if(row<0 || row >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(row,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;

    item = ui->TeachData->item(row,1);
    if(!item) return;
    int tPoint = item->text().toInt();
    TeachData.SetPoint(Name,tPoint);
    UpDateTeachData(Station,TeachData);
}
