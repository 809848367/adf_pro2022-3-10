#include "CtrlDebugDlg.h"
#include "HardwareManager.h"
#include "RobotUI.h"
#include "LightingUI.h"
#include "PanaMotorUI.h"
#include "ConveyorUI.h"
#include "CameraUI.h"
#include "IOControlUI.h"
#include "TeachManager.h"

#include "MachineManager.h"
#include "DatabaseManager.h"
#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 

CtrlDebugDlg * CtrlDebugDlg::m_pInstance = nullptr;

CtrlDebugDlg::CtrlDebugDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	Init();
	InitWindow();
	InitConnection();
}

CtrlDebugDlg::~CtrlDebugDlg()
{
}

bool CtrlDebugDlg::Init()
{

	return true;
}

void CtrlDebugDlg::InitWindow()
{
	Qt::WindowFlags windowFlag = Qt::Dialog;
	windowFlag |= Qt::WindowMinimizeButtonHint;
	windowFlag |= Qt::WindowMaximizeButtonHint;
	windowFlag |= Qt::WindowCloseButtonHint;
	this->setWindowFlags(windowFlag);

	QVBoxLayout *verticalLayout;
	verticalLayout = new QVBoxLayout(ui.CenterWidget);

	mainWidget = new QTabWidget(this);

	RobotUI* Robot1_UI = new RobotUI(mainWidget);
	RobotUI* Robot2_UI = new RobotUI(mainWidget);
	Robot1_UI->SetHardwareManager(HardwareManager::GetInstance(), "Robot1");
	Robot2_UI->SetHardwareManager(HardwareManager::GetInstance(), "Robot2");
	mainWidget->addTab(Robot1_UI, "Robot1");
	mainWidget->addTab(Robot2_UI, "Robot2");
	connect(Robot1_UI, SIGNAL(UpDateTeachData(QString, TeachData_Robot)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Robot)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Robot)), Robot1_UI, SLOT(RefreshTeachData(QString, TeachData_Robot)));

	connect(Robot2_UI, SIGNAL(UpDateTeachData(QString, TeachData_Robot)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Robot)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Robot)), Robot2_UI, SLOT(RefreshTeachData(QString, TeachData_Robot)));



	IOControlUI* IO_Control_UI = new IOControlUI(mainWidget);
	IO_Control_UI->SetHardwareManager(HardwareManager::GetInstance(), "IOControl");
	mainWidget->addTab(IO_Control_UI, "IOControl");

	LightingUI* Lighting1_UI = new LightingUI(mainWidget);
	LightingUI* Lighting2_UI = new LightingUI(mainWidget);
	LightingUI* LightingUp_UI = new LightingUI(mainWidget);
	LightingUI* LightingDown_UI = new LightingUI(mainWidget);
	LightingUI* LightingNG_UI = new LightingUI(mainWidget);
	Lighting1_UI->SetHardwareManager(HardwareManager::GetInstance(), "Lighting1");
	Lighting2_UI->SetHardwareManager(HardwareManager::GetInstance(), "Lighting2");
	LightingUp_UI->SetHardwareManager(HardwareManager::GetInstance(), "LightingUp");
	LightingDown_UI->SetHardwareManager(HardwareManager::GetInstance(), "LightingDown");
	LightingNG_UI->SetHardwareManager(HardwareManager::GetInstance(), "LightingNG");

	connect(Lighting1_UI, SIGNAL(UpDateTeachData(QString, TeachData_Lighting)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Lighting)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Lighting)), Lighting1_UI, SLOT(RefreshTeachData(QString, TeachData_Lighting)));
	
	connect(Lighting2_UI, SIGNAL(UpDateTeachData(QString, TeachData_Lighting)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Lighting)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Lighting)), Lighting2_UI, SLOT(RefreshTeachData(QString, TeachData_Lighting)));
	
	connect(LightingUp_UI, SIGNAL(UpDateTeachData(QString, TeachData_Lighting)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Lighting)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Lighting)), LightingUp_UI, SLOT(RefreshTeachData(QString, TeachData_Lighting)));
	
	connect(LightingDown_UI, SIGNAL(UpDateTeachData(QString, TeachData_Lighting)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Lighting)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Lighting)), LightingDown_UI, SLOT(RefreshTeachData(QString, TeachData_Lighting)));

	connect(LightingNG_UI, SIGNAL(UpDateTeachData(QString, TeachData_Lighting)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_Lighting)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_Lighting)), LightingNG_UI, SLOT(RefreshTeachData(QString, TeachData_Lighting)));


	mainWidget->addTab(Lighting1_UI, "Lighting1");
	mainWidget->addTab(Lighting2_UI, "Lighting2");
	mainWidget->addTab(LightingUp_UI, "LightingUp");
	mainWidget->addTab(LightingDown_UI, "LightingDown");
	mainWidget->addTab(LightingNG_UI, "LightingNG");


	CameraUI* Camera1_UI = new CameraUI(mainWidget);
	CameraUI* Camera2_UI = new CameraUI(mainWidget);
	CameraUI* CameraUp_UI = new CameraUI(mainWidget);
	CameraUI* CameraDown_UI = new CameraUI(mainWidget);
	CameraUI* CameraNG_UI = new CameraUI(mainWidget);
	
	Camera1_UI->SetHardwareManager(HardwareManager::GetInstance(), "Camera1");
	Camera2_UI->SetHardwareManager(HardwareManager::GetInstance(), "Camera2");
	CameraUp_UI->SetHardwareManager(HardwareManager::GetInstance(), "CameraUp");
	CameraDown_UI->SetHardwareManager(HardwareManager::GetInstance(), "CameraDown");
	CameraNG_UI->SetHardwareManager(HardwareManager::GetInstance(), "CameraNG");

	mainWidget->addTab(Camera1_UI, "Camera1");
	mainWidget->addTab(Camera2_UI, "Camera2");
	mainWidget->addTab(CameraUp_UI, "CameraUp");
	mainWidget->addTab(CameraDown_UI, "CameraDown");
	mainWidget->addTab(CameraNG_UI, "CameraNG");


	ConveyorUI* Conveyor1_UI = new ConveyorUI(mainWidget);
	ConveyorUI* Conveyor2_UI = new ConveyorUI(mainWidget);
	Conveyor1_UI->SetHardwareManager(HardwareManager::GetInstance(), "Conveyor1");
	Conveyor2_UI->SetHardwareManager(HardwareManager::GetInstance(), "Conveyor2");
	mainWidget->addTab(Conveyor1_UI, "Conveyor1");
	mainWidget->addTab(Conveyor2_UI, "Conveyor2");

	PanaMotorUI* PanaMotor1_UI = new PanaMotorUI(mainWidget);
	PanaMotorUI* PanaMotor2_UI = new PanaMotorUI(mainWidget);
	PanaMotor1_UI->SetHardwareManager(HardwareManager::GetInstance(), "PanaMotor1");
	PanaMotor2_UI->SetHardwareManager(HardwareManager::GetInstance(), "PanaMotor2");
	
	connect(PanaMotor1_UI, SIGNAL(UpDateTeachData(QString, TeachData_PanaMotor)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_PanaMotor)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_PanaMotor)), PanaMotor1_UI, SLOT(RefreshTeachData(QString, TeachData_PanaMotor)));

	connect(PanaMotor2_UI, SIGNAL(UpDateTeachData(QString, TeachData_PanaMotor)), &TeachManager::GetInstance(), SLOT(UpDateTeachData(QString, TeachData_PanaMotor)));
	connect(&TeachManager::GetInstance(), SIGNAL(RefreshTeachData(QString, TeachData_PanaMotor)), PanaMotor2_UI, SLOT(RefreshTeachData(QString, TeachData_PanaMotor)));

	mainWidget->addTab(PanaMotor1_UI, "PanaMotor1");
	mainWidget->addTab(PanaMotor2_UI, "PanaMotor2");



	verticalLayout->addWidget(mainWidget);
}

void CtrlDebugDlg::InitConnection()
{

}

CtrlDebugDlg* CtrlDebugDlg::GetInstance()
{
	if (m_pInstance == nullptr)
		m_pInstance = new CtrlDebugDlg();
	return m_pInstance;
}

void CtrlDebugDlg::DispCtrlDlg()
{
	CtrlDebugDlg::GetInstance()->show();
}

void CtrlDebugDlg::ReleaseCtrlDlg()
{
	if (m_pInstance != nullptr)
	{
		delete m_pInstance;
		m_pInstance = nullptr;
	}
}

