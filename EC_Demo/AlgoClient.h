#ifndef ALGOCLIENT_H
#define ALGOCLIENT_H

#include <QtCore>
#include "CtrlError.h"
#include <QTcpSocket>
#include "AlgoCommData.h"

class AlgoClient : public QObject
{
    Q_OBJECT
public:
    explicit AlgoClient(QObject *parent = nullptr);
    ~AlgoClient();
    CtrlError Open(QString Name, QString IP, quint16 Port);
    CtrlError Close();
    CtrlError Send(QString Data);
private:
    QTcpSocket *Socket;
    QString Name;
    QString IP;
    quint16 Port;
    QTimer HeartBeat;
private slots:
    void RecvResult();
    void HeartBeatSlot();
signals:
    void RecvResult(QString Name,QString Data);
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00601000,
        ClientHasOpen           = Base + 0x001,
        ClientOpenFail          = Base + 0x002,
        ClientSetParaErr        = Base + 0x003,
        ClientNotOpen           = Base + 0x004,
        RecvDataNotExist        = Base + 0x005,
    };
    Q_ENUM(ErrorCode)
};

#endif // ALGOCLIENT_H
