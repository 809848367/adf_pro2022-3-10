﻿#include "QFigure.h"
#include <QImage>
#include <QResizeEvent>

enum QOpencvMouseEvent
{
    QOpencvMouseMOVE    = 0,
    QOpencvMouseLDOWN   = 1,
    QOpencvMouseLUP     = 2,
    QOpencvMouseLDCLK   = 3,
    QOpencvMouseRDOWN   = 4,
    QOpencvMouseRUP     = 5,
    QOpencvMouseRDCLK   = 6,
    QOpencvMouseMDOWN   = 7,
    QOpencvMouseMUP     = 8,
    QOpencvMouseMDCLK   = 9,
    QOpencvMouseWUP     = 10,
    QOpencvMouseWDOWN   = 11,
};

QFigure::QFigure(QWidget *parent) : QLabel(parent)
{
    qRegisterMetaType<cv::Rect>("cv::Rect");
    qRegisterMetaType<cv::Point>("cv::Point");
    setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    setMouseTracking(true);
    setCursor(QCursor(Qt::CrossCursor));
    ChangeCallbackMode(0);
}

void QFigure::SetImage(cv::Mat Image, bool ChangeROI)
{
    //以下几种情况直接更新ROI范围：ChangeROI置true，原来不存在图片，原图和现有图尺寸不一致
    if(ChangeROI || ImageTower.empty()  || Image.rows != ImageTower[0].rows || Image.cols != ImageTower[0].cols)
    {
        ROINow = cv::Rect(0,0,Image.cols,Image.rows);
    }
    ImageTower.clear();
    ImageTower.push_back(Image);
    LineData.clear();
    AreaData.clear();
    ShowImage();
}

cv::Mat QFigure::ExportImage()
{
    //如果没有图片，输出孔
    if(ImageTower.empty() || ImageTower[0].empty()) return cv::Mat();
    //复制原图并转换为RGB，注意划线是用的RGB的结构，这里不能转换成BGR
    cv::Mat tImage = ImageTower[0].clone();
    if(tImage.channels() == 1) cv::cvtColor(tImage,tImage,cv::COLOR_GRAY2RGB);
    else if(tImage.channels() == 3) cv::cvtColor(tImage,tImage,cv::COLOR_BGR2RGB);
    //划线
    ShowLine(LineData,tImage,cv::Rect(0,0,tImage.cols,tImage.rows),1,1);

    //转换回BGR，用于显示或导出
    cv::cvtColor(tImage,tImage,cv::COLOR_RGB2BGR);
    return tImage;
}

void QFigure::SetROI(cv::Rect ROI,bool ChangePartner)
{
    if(ImageTower.empty() || ImageTower[0].empty()) return;
    ROINow = ROI;
    ShowImage(0,ChangePartner);
}

void QFigure::ChangeCallbackMode(int Mode)
{
    //初始化步骤参数
    SelectCallbackStep = 0;
    MoveCallbackStep = 0;
    disconnect(this,SIGNAL(SelectCB(cv::Point,cv::Point)),this,SLOT(ZoomInBySelect(cv::Point,cv::Point)));
    disconnect(this,SIGNAL(MouseWheel(int,int,int)),this,SLOT(ZoomInOut(int,int,int)));
    disconnect(this,SIGNAL(RBtnClickCB(int,int)),this,SLOT(ZoomReset()));
    disconnect(this,SIGNAL(MoveCB(cv::Point,cv::Point)),this,SLOT(ZoomMove(cv::Point,cv::Point)));
    if(Mode == 0)
    {
        //设置默认颜色
        SelectCallbackColor = cv::Scalar(255,255,128);
        SelectCallbackType = 0;
        CursorCallbackType = 0;
        //连接信号槽
        connect(this,SIGNAL(SelectCB(cv::Point,cv::Point)),this,SLOT(ZoomInBySelect(cv::Point,cv::Point)),Qt::UniqueConnection);
        connect(this,SIGNAL(MouseWheel(int,int,int)),this,SLOT(ZoomInOut(int,int,int)),Qt::UniqueConnection);
        connect(this,SIGNAL(RBtnClickCB(int,int)),this,SLOT(ZoomReset()),Qt::UniqueConnection);
        connect(this,SIGNAL(MoveCB(cv::Point,cv::Point)),this,SLOT(ZoomMove(cv::Point,cv::Point)),Qt::UniqueConnection);
    }
    if(Mode == 1)
    {
        connect(this,SIGNAL(MouseWheel(int,int,int)),this,SLOT(ZoomInOut(int,int,int)),Qt::UniqueConnection);
    }
}

void QFigure::SetCurcor(int Type, cv::Size s, cv::Scalar Color)
{
    CursorCallbackColor = Color;
    CursorCallbackSize = s;
    CursorCallbackType = Type;
}

void QFigure::SetPartner(QFigure *Partner)
{
    Partners.push_back(Partner);
}

int QFigure::DrawRect(cv::Point2f Pt1, cv::Point2f Pt2, cv::Scalar Color)
{
    return DrawRect(cv::Rect2f(Pt1,Pt2),Color);
}

int QFigure::DrawRect(cv::Rect2f Region, cv::Scalar Color)
{
    QList<double> tData;
    tData.push_back(0);
    tData.push_back(Region.x);
    tData.push_back(Region.y);
    tData.push_back(Region.width);
    tData.push_back(Region.height);
    tData.push_back(Color[0]);
    tData.push_back(Color[1]);
    tData.push_back(Color[2]);
    LineData.push_back(tData);
    ShowImage(1);
    return LineData.size()-1;
}

int QFigure::DrawCircle(cv::Point2f Center, double Rx, double Ry, cv::Scalar Color)
{
    return DrawCircle(Center, Rx, Ry, 0, Color);
}
int QFigure::DrawCircle(cv::Point2f Center, double Rx, double Ry, double Angle, cv::Scalar Color)
{
    QList<double> tData;
    tData.push_back(1);
    tData.push_back(Center.x);
    tData.push_back(Center.y);
    tData.push_back(Rx);
    tData.push_back(Ry);
    tData.push_back(Angle);
    tData.push_back(Color[0]);
    tData.push_back(Color[1]);
    tData.push_back(Color[2]);
    LineData.push_back(tData);
    ShowImage(1);
    return LineData.size()-1;
}

int QFigure::DrawLine(cv::Point2f Pt1, cv::Point2f Pt2, cv::Scalar Color)
{
    QList<double> tData;
    tData.push_back(2);
    tData.push_back(Pt1.x);
    tData.push_back(Pt1.y);
    tData.push_back(Pt2.x);
    tData.push_back(Pt2.y);
    tData.push_back(Color[0]);
    tData.push_back(Color[1]);
    tData.push_back(Color[2]);
    LineData.push_back(tData);
    ShowImage(1);
    return LineData.size()-1;
}

int QFigure::DrawArea(AreaWithPos Area, cv::Scalar Color)
{
    QVector<double> tData;
    tData.push_back(Color[0]);
    tData.push_back(Color[1]);
    tData.push_back(Color[2]);
    AreaData.push_back(QPair<AreaWithPos,QVector<double>>(Area,tData));
    ShowImage(1);
    return AreaData.size()-1;
}

void QFigure::ClearDraw()
{
    LineData.clear();
    AreaData.clear();
    ShowImage(1);
    return;
}

QSize QFigure::ImageSize()
{
    if(ImageTower.empty() || ImageTower[0].empty()) return QSize(0,0);
    else return QSize(ImageTower[0].cols,ImageTower[0].rows);
}

void QFigure::ShowImage(int n, bool PartnerROI)
{
    cv::Mat ImageShow; //正在显示的图
    //如果图片不为空，进入显示流程
    if(ImageTower.empty() || ImageTower[0].empty())
    {
        ImageShow = cv::Mat(height(),width(),CV_8UC3,cv::Scalar(0,0,0));
    }
    else
    {
        //图片变更
        if(n <= 0)
        {
            //更新ROI为真正范围
            //Step1:ROI最小设置成1Pixel
            ROINow.width = qMax(ROINow.width,1);
            ROINow.height = qMax(ROINow.height,1);
            //Step2:长宽比，这里的HxW要小于最大的HxW，防止长宽均大于原图
            int HxW = qMin(qMax(ROINow.width*height(),ROINow.height*width()),qMax(ImageTower[0].cols*height(),ImageTower[0].rows*width()));
            ROINow.width = HxW / height();
            ROINow.height = HxW / width();
            //Step3:左上角位置
            ROINow.x = qMax(ROINow.x,0);
            ROINow.y = qMax(ROINow.y,0);
            //Step4:右下角位置（这里注意不能让左上角为负数）
            ROINow.x = qMin(ROINow.x,qMax(ImageTower[0].cols-ROINow.width,0));
            ROINow.y = qMin(ROINow.y,qMax(ImageTower[0].rows-ROINow.height,0));

            //更新其他显示窗口
            if(PartnerROI)
            {
                for(int i=0;i<Partners.size();i++)
                {
                    if(Partners[i]->ImageSize() == ImageSize()) Partners[i]->SetROI(ROINow,false);
                }
            }

            //计算应该取图像金字塔哪一层，及对应的ROI区域
            NTower = 0;
            ROIShow = ROINow;
            while(ROIShow.width >= width()*2 && ROIShow.height >= height()*2)
            {
                NTower++;
                ROIShow.x /= 2;
                ROIShow.y /= 2;
                ROIShow.width /= 2;
                ROIShow.height /= 2;
            }
            //生成层数对应的图片
            while(ImageTower.size() < NTower+1) ImageTower.push_back(cv::Mat());
            if(ImageTower[NTower].empty()) cv::resize(ImageTower[0],ImageTower[NTower],cv::Size(),qPow(0.5,NTower),qPow(0.5,NTower),cv::INTER_NEAREST);
        }

        //线条变更
        if(n <= 1)
        {
            cv::Mat tImage(ROIShow.height,ROIShow.width,ImageTower[NTower].type(),cv::Scalar(230,230,230));
            //将原图Copy上去
            cv::Mat I1 = ImageTower[NTower](cv::Rect(ROIShow.x,ROIShow.y,qMin(ROIShow.width,ImageTower[NTower].cols-ROIShow.x),qMin(ROIShow.height,ImageTower[NTower].rows-ROIShow.y)));
            cv::Mat I2 = tImage(cv::Rect(0,0,qMin(ROIShow.width,ImageTower[NTower].cols-ROIShow.x),qMin(ROIShow.height,ImageTower[NTower].rows-ROIShow.y)));
            I1.copyTo(I2);
            if(tImage.channels() == 1) cv::cvtColor(tImage,tImage,cv::COLOR_GRAY2RGB);
            else if(tImage.channels() == 3) cv::cvtColor(tImage,tImage,cv::COLOR_BGR2RGB);

            tImage.copyTo(ImageShow);
            ShowLine(CurcorData,ImageShow,ROINow,qPow(0.5,NTower),qPow(0.5,NTower));
            ShowArea(AreaData,ImageShow,ROINow,qPow(0.5,NTower),qPow(0.5,NTower));

            cv::resize(ImageShow,ImageShow,cv::Size(width(),height()),0,0,cv::INTER_NEAREST);

            //生成固定线条
            double XRadio = width()*1.0/ROINow.width;
            double YRadio = height()*1.0/ROINow.height;
            ShowLine(LineData,ImageShow,ROINow,XRadio,YRadio);
            ShowLine(TempLineData,ImageShow,ROINow,XRadio,YRadio);
        }
    }
    //显示图片到UI上
    QImage Image(ImageShow.data,ImageShow.cols,ImageShow.rows,static_cast<int>(ImageShow.step),QImage::Format_RGB888);
    setPixmap(QPixmap::fromImage(Image));
}

void QFigure::ShowLine(QList<QList<double>> Data, cv::Mat &Image, cv::Rect tROI, double XRadio, double YRadio)
{
    //注：所有点位为了让线显示在中间，加了0.49，所以要用Floor，所有尺寸不受干扰，所以用Round

    for(int i=0;i<Data.size();i++)
    {
        if(Data[i].empty()) continue;

        //画方
        if(Data[i][0] == 0)
        {
            //方要有8个参数
            if(Data[i].size() < 8) continue;
            cv::Rect Area = cv::Rect(qFloor((Data[i][1]-tROI.x+0.49) * XRadio),qFloor((Data[i][2]-tROI.y+0.49) * YRadio),qRound(Data[i][3] * XRadio),qRound(Data[i][4] * YRadio));
            cv::Scalar Color = cv::Scalar(Data[i][5],Data[i][6],Data[i][7]);
            cv::rectangle(Image,Area,Color);
        }
        //画圆
        if(Data[i][0] == 1)
        {
            //圆要有9个参数
            if(Data[i].size() < 9) continue;
            cv::Point2f Center = cv::Point2f(qFloor((Data[i][1]-tROI.x+0.49) * XRadio),qFloor((Data[i][2]-tROI.y+0.49) * YRadio));
            double Rx = Data[i][3] * XRadio;
            double Ry = Data[i][4] * YRadio;
            double Angle = Data[i][5];
            cv::Scalar Color = cv::Scalar(Data[i][6],Data[i][7],Data[i][8]);
            cv::ellipse(Image,Center,cv::Size(Rx,Ry),Angle,0,360,Color);
        }

        //画线
        if(Data[i][0] == 2)
        {
            //线要有8个参数
            if(Data[i].size() < 8) continue;
            cv::Point2f Pt1 = cv::Point2f(qFloor((Data[i][1]-tROI.x+0.49) * XRadio),qFloor((Data[i][2]-tROI.y+0.49) * YRadio));
            cv::Point2f Pt2 = cv::Point2f(qFloor((Data[i][3]-tROI.x+0.49) * XRadio),qFloor((Data[i][4]-tROI.y+0.49) * YRadio));
            cv::Scalar Color = cv::Scalar(Data[i][5],Data[i][6],Data[i][7]);
            cv::line(Image,Pt1,Pt2,Color);
        }

        //画Curcor
        if(Data[i][0] == 10 || Data[i][0] == 11)
        {
            //Curcor要有8个参数
            if(Data[i].size() < 8) continue;
            //如果尺寸过小，跳过
            if(Data[i][3]*XRadio < 1 || Data[i][4]*YRadio < 1) continue;
            cv::Scalar Color = cv::Scalar(Data[i][5],Data[i][6],Data[i][7]);
            cv::Mat tImage = cv::Mat(Data[i][3],Data[i][4],CV_8UC3,cv::Scalar(0));
            cv::Mat tMask = cv::Mat(Data[i][3],Data[i][4],CV_8UC1,cv::Scalar(0));
            if(Data[i][0] == 10) cv::rectangle(tImage,cv::Rect(0,0,Data[i][3],Data[i][4]),Color,-1);
            if(Data[i][0] == 11) cv::ellipse(tImage,cv::Point((Data[i][3])/2,(Data[i][4])/2),cv::Size(Data[i][3]/2,Data[i][4]/2),0,0,360,cv::Scalar(255),-1);
            if(Data[i][0] == 10) cv::rectangle(tMask,cv::Rect(0,0,Data[i][3],Data[i][4]),Color,-1);
            if(Data[i][0] == 11) cv::ellipse(tMask,cv::Point((Data[i][3])/2,(Data[i][4])/2),cv::Size(Data[i][3]/2,Data[i][4]/2),0,0,360,cv::Scalar(255),-1);
            cv::resize(tImage,tImage,cv::Size(),XRadio,YRadio,cv::INTER_NEAREST);
            cv::Point2f Pos = cv::Point2f(qRound((Data[i][1]-tROI.x-(Data[i][3]-1)/2) * XRadio),qRound((Data[i][2]-tROI.y-(Data[i][4]-1)/2) * YRadio));

            cv::Rect R1;
            R1.x = Pos.x;
            if(Pos.x < 0) R1.x = 0;
            R1.y = Pos.y;
            if(Pos.y < 0) R1.y = 0;
            R1.width = tImage.cols;
            if(Pos.x < 0) R1.width += Pos.x;
            if(Pos.x + tImage.cols > Image.cols) R1.width += Image.cols - (Pos.x + tImage.cols);
            R1.height = tImage.rows;
            if(Pos.y < 0) R1.height += Pos.y;
            if(Pos.y + tImage.rows > Image.rows) R1.height += Image.rows - (Pos.y + tImage.rows);

            cv::Rect R2 = R1;
            R2.x = 0;
            if(Pos.x < 0) R2.x -= Pos.x;
            R2.y = 0;
            if(Pos.y < 0) R2.y -= Pos.y;

            tImage(R2).copyTo(Image(R1),tMask(R2));
        }
    }
}

void QFigure::ShowArea(QList<QPair<AreaWithPos,QVector<double>>> Data, cv::Mat &Image, cv::Rect tROI, double XRadio, double YRadio)
{
    //注：所有点位为了让线显示在中间，加了0.49，所以要用Floor，所有尺寸不受干扰，所以用Round

    for(int i=0;i<Data.size();i++)
    {
        //如果尺寸过小，跳过
        if(Data[i].first.Image.rows*XRadio < 1 || Data[i].first.Image.cols*YRadio < 1) continue;
        cv::Scalar Color = cv::Scalar(Data[i].second[0],Data[i].second[1],Data[i].second[2]);
        cv::Mat tImage = cv::Mat(Data[i].first.Image.rows,Data[i].first.Image.cols,CV_8UC3,cv::Scalar(0));
        cv::Mat tMask = cv::Mat(Data[i].first.Image.rows,Data[i].first.Image.cols,CV_8UC1,cv::Scalar(0));
        cv::Rect Area = cv::Rect(Data[i].first.Pos.x,Data[i].first.Pos.y,Data[i].first.Image.cols,Data[i].first.Image.rows);
        tImage.setTo(Color,Data[i].first.Image);
        tMask.setTo(cv::Scalar(255),Data[i].first.Image);
        cv::resize(tImage,tImage,cv::Size(),XRadio,YRadio,cv::INTER_NEAREST);
        cv::resize(tMask,tMask,cv::Size(),XRadio,YRadio,cv::INTER_NEAREST);
        cv::Point2f Pos = cv::Point2f(qRound((Data[i].first.Pos.x-tROI.x) * XRadio),qRound((Data[i].first.Pos.y-tROI.y) * YRadio));

        cv::Rect R1;
        R1.x = Pos.x;
        if(Pos.x < 0)  R1.x = 0;
        R1.y = Pos.y;
        if(Pos.y < 0) R1.y = 0;
        R1.width = tImage.cols;
        if(Pos.x < 0) R1.width += Pos.x;
        if(Pos.x + tImage.cols > Image.cols) R1.width += Image.cols - (Pos.x + tImage.cols);
        R1.height = tImage.rows;
        if(Pos.y < 0) R1.height += Pos.y;
        if(Pos.y + tImage.rows > Image.rows) R1.height += Image.rows - (Pos.y + tImage.rows);

        cv::Rect R2 = R1;
        R2.x = 0;
        if(Pos.x < 0) R2.x -= Pos.x;
        R2.y = 0;
        if(Pos.y < 0) R2.y -= Pos.y;

        if(R1.width > 0 && R1.height > 0) tImage(R2).copyTo(Image(R1),tMask(R2));
    }
}

void QFigure::resizeEvent(QResizeEvent *event)
{
    if(!ImageTower.empty() && !ImageTower[0].empty()) ROINow = cv::Rect(0,0,ImageTower[0].cols,ImageTower[0].rows);
    ShowImage(0,false);
}

void QFigure::mousePressEvent(QMouseEvent *event)
{
    //Mouse Click Event
    if(event->button() == Qt::LeftButton)
    {
        DealMOuseEvent(QOpencvMouseLDOWN,event->x(),event->y(),event->buttons());
    }
    if(event->button() == Qt::RightButton)
    {
        DealMOuseEvent(QOpencvMouseRDOWN,event->x(),event->y(),event->buttons());
    }
    if(event->button() == Qt::MiddleButton)
    {
        DealMOuseEvent(QOpencvMouseMDOWN,event->x(),event->y(),event->buttons());
    }
}

void QFigure::mouseDoubleClickEvent(QMouseEvent *event)
{
    //Mouse Double Click Event
    if(event->button() == Qt::LeftButton)
    {
        DealMOuseEvent(QOpencvMouseLDCLK,event->x(),event->y(),event->buttons());
    }
    if(event->button() == Qt::RightButton)
    {
        DealMOuseEvent(QOpencvMouseRDCLK,event->x(),event->y(),event->buttons());
    }
    if(event->button() == Qt::MiddleButton)
    {
        DealMOuseEvent(QOpencvMouseMDCLK,event->x(),event->y(),event->buttons());
    }
}

void QFigure::mouseReleaseEvent(QMouseEvent *event)
{
    //Mouse Release Event
    if(event->button() == Qt::LeftButton)
    {
        DealMOuseEvent(QOpencvMouseLUP,event->x(),event->y(),event->buttons());
    }
    if(event->button() == Qt::RightButton)
    {
        DealMOuseEvent(QOpencvMouseRUP,event->x(),event->y(),event->buttons());
    }
    if(event->button() == Qt::MiddleButton)
    {
        DealMOuseEvent(QOpencvMouseMUP,event->x(),event->y(),event->buttons());
    }
}

void QFigure::mouseMoveEvent(QMouseEvent *event)
{
    //Mouse Move Event
    DealMOuseEvent(QOpencvMouseMOVE,event->x(),event->y(),event->buttons());
}

void QFigure::wheelEvent(QWheelEvent *event)
{
    //Mouse Wheel Event
    if(event->delta() > 0)//Wheel Away to User
    {
        DealMOuseEvent(QOpencvMouseWUP,event->x(),event->y(),event->buttons());
    }
    else//Wheel Come to User
    {
        DealMOuseEvent(QOpencvMouseWDOWN,event->x(),event->y(),event->buttons());
    }
}

void QFigure::DealMOuseEvent(int event,int x,int y,Qt::MouseButtons Buttons)
{
    //没显示图片，不进入流程
    if(ImageTower.empty() || ImageTower[0].empty()) return;

    //清除临时线
    int N = TempLineData.size();
    int M = CurcorData.size();
    TempLineData.clear();
    CurcorData.clear();
    SelectCallback(event,x,y,Buttons);
    MoveCallback(event,x,y,Buttons);
    ClickedCallback(event,x,y,Buttons);
    CursorCallback(event,x,y,Buttons);
    if(N > 0 || M> 0 || TempLineData.size() > 0 || CurcorData.size() > 0) ShowImage(1);
}

void QFigure::SelectCallback(int event, int x, int y, Qt::MouseButtons Buttons)
{
    double XRadio = width()*1.0/ROINow.width;
    double YRadio = height()*1.0/ROINow.height;
    x = x/XRadio + ROINow.x;
    y = y/YRadio + ROINow.y;
    static cv::Point OldPt;

    if(SelectCallbackStep == 0 && event == QOpencvMouseLDOWN)
    {
        //第一次按下
        OldPt = cv::Point(x,y);
        SelectCallbackStep = 1; //标志位变为1，表示该进行下一步了
    }

    else if(SelectCallbackStep == 1 && event == QOpencvMouseLDOWN)
    {
        //第二次按下
        if(qAbs(x-OldPt.x)*XRadio >= 5 && qAbs(y-OldPt.y)*YRadio >= 5) //两次间隔要大于显示的5个Pixel，主要是防止误操作
        {
            SelectCB(OldPt,cv::Point(x,y)); //按点发送信号
            SelectCallbackStep = -1; //标志位变为0，表示结束
            MoveCallbackStep = -1; //这里是防止第二次按下时拖动导致Move信号触发
        }
    }
    else if(SelectCallbackStep == -1 && event == QOpencvMouseLUP)
    {
        SelectCallbackStep = 0; //松开复位
    }

    //显示
    if(SelectCallbackStep == 1)
    {
        QList<double> Data;
        Data.push_back(SelectCallbackType); //图案
        if(SelectCallbackType == 0) //画方
        {
            Data.push_back(qMin(OldPt.x,x)); //X
            Data.push_back(qMin(OldPt.y,y)); //Y
            Data.push_back(qAbs(OldPt.x-x)); //Width
            Data.push_back(qAbs(OldPt.y-y)); //Height
        }
        if(SelectCallbackType == 1) //画圆
        {
            Data.push_back((OldPt.x+x)/2.0); //X
            Data.push_back((OldPt.y+y)/2.0); //Y
            Data.push_back(qAbs(OldPt.x-x)/2.0); //Rx
            Data.push_back(qAbs(OldPt.y-y)/2.0); //Ry
            Data.push_back(0);                   //Angle
        }
        if(SelectCallbackType == 2) //画线
        {
            Data.push_back(OldPt.x); //X1
            Data.push_back(OldPt.y); //Y1
            Data.push_back(x); //X2
            Data.push_back(y); //Y2
        }
        Data.push_back(SelectCallbackColor[0]); //R
        Data.push_back(SelectCallbackColor[1]); //G
        Data.push_back(SelectCallbackColor[2]); //B
        TempLineData.push_back(Data);
    }
}

void QFigure::MoveCallback(int event, int x, int y, Qt::MouseButtons Buttons)
{
    //注意，移动需要的是界面上的坐标，不是图片上的真实坐标
    double XRadio = width()*1.0/ROINow.width;
    double YRadio = height()*1.0/ROINow.height;
    double tx = x/XRadio + ROINow.x;
    double ty = y/YRadio + ROINow.y;
    static cv::Point OldPt;
    if(tx>=0 && tx<ImageTower[0].cols && ty >= 0 && ty <ImageTower[0].rows)
    {
        MousePos(cv::Point(tx,ty), ImageTower[0].at<unsigned char>(ty,tx));
    }
    else
    {
        MousePos(cv::Point(tx,ty), 0);
    }

    if(MoveCallbackStep == 0 && event == QOpencvMouseLDOWN)
    {
        //按下
        OldPt = cv::Point(x,y); //记录点位
        MoveCallbackStep = 1; //标志位变为1，表示该进行下一步了
    }
    else if(MoveCallbackStep == 1 && event == QOpencvMouseLUP)
    {
        //送开
        MoveCallbackStep = 0;
    }
    else if(MoveCallbackStep == 1 && Buttons & Qt::LeftButton) //按下并移动
    {
        if(qAbs(x-OldPt.x)*XRadio >= 2 || qAbs(y-OldPt.y)*YRadio >= 2) //移动了超过5个Pixel
        {
            //记得信号要发送真实坐标
            MoveCB(cv::Point(OldPt.x/XRadio+ROINow.x,OldPt.y/YRadio+ROINow.y),cv::Point(x/XRadio+ROINow.x,y/YRadio+ROINow.y));
            SelectCallbackStep = -1; //如果按下并移动，则关闭选取功能
            OldPt = cv::Point(x,y); //更新点位
        }
    }
    else if(MoveCallbackStep == -1 && event == QOpencvMouseLUP)
    {
        MoveCallbackStep = 0; //松开复位
    }
}

void QFigure::ClickedCallback(int event, int x, int y, Qt::MouseButtons Buttons)
{
    double XRadio = width()*1.0/ROINow.width;
    double YRadio = height()*1.0/ROINow.height;
    x = x/XRadio + ROINow.x;
    y = y/YRadio + ROINow.y;
    if(event == QOpencvMouseLDOWN)
    {
        LBtnClickCB(x,y);
    }
    if(event == QOpencvMouseRDOWN)
    {
        RBtnClickCB(x,y);
    }
    if(event == QOpencvMouseLDCLK)
    {
        //由于双击会触发单击，所以要关掉可能被触发的选择和移动
        SelectCallbackStep = 0;
        MoveCallbackStep = 0;
        LBtnDClickCB(x,y);
    }
    if(event == QOpencvMouseRDCLK)
    {
        RBtnDClickCB(x,y);
    }
    if(event == QOpencvMouseWUP)
    {
        //由于滚轮会触发缩放，所以要关掉会导致异常
        SelectCallbackStep = 0;
        MoveCallbackStep = 0;
        MouseWheel(x,y,-1);
    }
    if(event == QOpencvMouseWDOWN)
    {
        //由于滚轮会触发缩放，所以要关掉会导致异常
        SelectCallbackStep = 0;
        MoveCallbackStep = 0;
        MouseWheel(x,y,1);
    }
}

void QFigure::CursorCallback(int event, int x, int y, Qt::MouseButtons Buttons)
{
    //鼠标不在图框里面，不显示
    if(x < 0 || x >= width() || y<0 || y >= height()) return;
    double XRadio = width()*1.0/ROINow.width;
    double YRadio = height()*1.0/ROINow.height;
    x = x/XRadio + ROINow.x;
    y = y/YRadio + ROINow.y;

    if(CursorCallbackType > 0)
    {
        QList<double> Data;
        Data.push_back(CursorCallbackType); //图案

        Data.push_back(x); //中心X
        Data.push_back(y); //中心Y
        Data.push_back(CursorCallbackSize.width); //尺寸
        Data.push_back(CursorCallbackSize.height); //尺寸
        Data.push_back(0); //角度

        Data.push_back(CursorCallbackColor[0]); //R
        Data.push_back(CursorCallbackColor[1]); //G
        Data.push_back(CursorCallbackColor[2]); //B

        CurcorData.push_back(Data);
    }
}

void QFigure::ZoomInBySelect(cv::Point Pt1,cv::Point Pt2)
{
    if(ImageTower.empty() || ImageTower[0].empty()) return;
    ROINow = cv::Rect(Pt1,Pt2);
    ShowImage();
}

void QFigure::ZoomInOut(int x,int y,int value)
{
    if(ImageTower.empty() || ImageTower[0].empty()) return;
    int Scale = 100;
    if(value > 0) Scale = 110;
    if(value < 0)
    {
        if(ROINow.width < 10 || ROINow.height < 10) return;
        Scale = 90;
    }
    if(x == -1) x = ROINow.x+ROINow.width/2;
    if(y == -1) y = ROINow.y+ROINow.height/2;
    ROINow.width = ROINow.width*Scale/100+1;
    ROINow.height = ROINow.height*Scale/100+1;
    ROINow.x = x - (x - ROINow.x)*Scale/100;
    ROINow.y = y - (y - ROINow.y)*Scale/100;
    SelectCallbackStep = 0;
    MoveCallbackStep = 0;
    ShowImage();
}

void QFigure::ZoomReset()
{
    if(ImageTower.empty() || ImageTower[0].empty()) return;
    ROINow = cv::Rect(0,0,ImageTower[0].cols,ImageTower[0].rows);
    SelectCallbackStep = 0;
    MoveCallbackStep = 0;
    ShowImage();
}

void QFigure::ZoomMove(cv::Point Old,cv::Point New)
{
    if(ImageTower.empty() || ImageTower[0].empty()) return;
    ROINow.x += Old.x-New.x;
    ROINow.y += Old.y-New.y;
    ShowImage();
}

