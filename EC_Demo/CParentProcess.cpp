#include "CParentProcess.h"
#include <windows.h>
#include "DatabaseManager.h"
#include "MachineManager.h"
#include "QiniOperator.h"
#include "HardwareManager.h"
#include "CFlageManager.h"
#include <QMessageBox>
#include "AlgoManager.h"
#include "Commond_Defined.h"
#include "MyMessageBox.h"
#include "TestBox.h"
#include "CFileOperator.h"
#include "ClientThread.h"
#include "TCPManager.h"
#include "ModbusMoudle.h"

#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8") 
#endif 
CParentProcess::CParentProcess(QObject *parent)
	: QObject(parent)
{
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}


	m_step_index = 0;
	m_Proc_Stat = Process_Status_Idle;
	m_Pre_Proc_Stat = m_Proc_Stat;
	m_bThreadRun = false;
	m_pInfo = NULL;
	m_curStepNmae = "";
	m_Proc_Name = "";
	m_bFinsh = true;
	m_bException = false;
	m_bCountnue = true;
	connect(this, &CParentProcess::StartProcSignal, this, &CParentProcess::StartProc);
}

CParentProcess::~CParentProcess()
{

}

void CParentProcess::StartProc()
{
	m_threadHandle = QtConcurrent::run(this, &CParentProcess::Proc_Wrok_Thread);
}

void CParentProcess::ExecOperatorSlot(OperatorInfo*op)
{
	ProcessState status;
	ExecOperator(op, status);
}

void CParentProcess::ExecStepSlot(StepInfo* step)
{
	ExecStepInfoNotWait(step);
}

void CParentProcess::SetStepIndex(int index)
{
	m_step_index = index;
}

void CParentProcess::Proc_ShowMesBox(QString title, QString contant, int msgType)
{
	msgBox::show(title, contant, msgType);
}

void CParentProcess::ErrorPackage1(CtrlError err, OperatorInfo* op)
{
	if (0 != err)
	{
		QString GetErrStr(err);
		emit SendProcMsg(
			QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
			arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
			.arg(op->oprtType).arg((QString("%1").arg(err, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(err)));
	}
}

QString CParentProcess::GetProcName()
{
	return m_Proc_Name;
}

CParentProcess::ProcessState CParentProcess::GetProcState()
{
	//m_Status_Mutex.lock();
	ProcessState status = m_Proc_Stat;
	//m_Status_Mutex.unlock();
	return status;
}

int CParentProcess::Set_Proc_Status(int status)
{
	m_Status_Mutex.lock();
	switch ((ProcessState)status)
	{
	//case Process_Status_Unknow:
	//	if (m_Proc_Stat == Process_Status_Unknow ||!theMachineManager.isAtuo())
	//	{
	//		m_Proc_Stat = (ProcessState)status;
	//		//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("wait Reset"));
	//		emit SendProMsgWithProfix(QStringList()<<GetProcName(),"wait Reset");
	//		m_Status_Mutex.unlock();
	//		return NoError;
	//	}
	//	break;
	case Process_Status_Idle:
		//if (m_Proc_Stat == Process_Status_Unknow || m_Proc_Stat == Process_Status_Stop || m_Proc_Stat == Process_Status_Idle)
		if (m_Proc_Stat != Process_Status_Work || m_Proc_Stat != Process_Status_Pause || m_Proc_Stat != Process_Status_Continue
			|| !theMachineManager.isAtuo())
		{
			m_Proc_Stat = (ProcessState)status;
			//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("Idle"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Idle");

			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	case Process_Status_Work:
		if (m_Proc_Stat == Process_Status_Idle || !theMachineManager.isAtuo())
		{
			m_Proc_Stat = (ProcessState)status;
			//emit SendProcMsg("Proc Running!");
			//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("Proc Start Running!"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Proc Start Running!");
			if (m_threadHandle.isRunning() == false)
				emit StartProcSignal();
			m_Status_Mutex.unlock();
			return NoError;
		}
		else if (m_Proc_Stat == Process_Status_Pause || !theMachineManager.isAtuo())
		{
			//...
			m_Proc_Stat = (ProcessState)status;
			//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("continue Running!"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "continue Running!");

			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	case Process_Status_Pause:
		if (m_Proc_Stat == Process_Status_Work || !theMachineManager.isAtuo())
		{
			m_Proc_Stat = (ProcessState)status;
			//emit SendProcMsg(QString("%1 get pause cmd ").arg(m_Proc_Name));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), " pause cmd");
			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	case Process_Status_Stop:
		if (m_Proc_Stat == Process_Status_Work || m_Proc_Stat == Process_Status_Pause || !theMachineManager.isAtuo())
		{
			//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("stop"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "stop");
			m_Proc_Stat = (ProcessState)status;
			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	case Process_Status_Start:
		//if (m_Proc_Stat == Process_Status_Idle || m_Proc_Stat == Process_Status_Stop || m_Proc_Stat == Process_Status_End)
		if (m_Proc_Stat == Process_Status_Idle || !theMachineManager.isAtuo())
		{
			m_Proc_Stat = (ProcessState)Process_Status_Work;
			m_bFinsh = false;
			m_bException = false;
			//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("Start Running!"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Start Running!");
			if (m_threadHandle.isRunning() == false)
				emit StartProcSignal();
			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	case Process_Status_Continue:
		if (m_Proc_Stat == Process_Status_Pause || !theMachineManager.isAtuo())
		{
			m_Proc_Stat = Process_Status_Continue;
			QString("%1->%2").arg(m_Proc_Name);
			//emit SendProcMsg(QString("%1-->%2").arg(m_Proc_Name).arg("continue!"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "continue");
			//emit SendProcMsg("Proc continue!");
			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	case Process_Status_End:
		if (m_Proc_Stat == Process_Status_Work || m_Proc_Stat == Process_Status_Pause || !theMachineManager.isAtuo())
		{
			//emit SendProcMsg(QString("%1->%2").arg(m_Proc_Name).arg("Process_Status_End"));
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_End");
			m_Proc_Stat = Process_Status_End;
			m_Status_Mutex.unlock();
			return NoError;
		}
		break;
	default:
		break;
	}
	m_Status_Mutex.unlock();
	return CurrentStateNotAllow;
}

void CParentProcess::ForceQuit()
{
	if (WorkThreadState())
	{
		m_bThreadRun = false;
		m_Proc_Stat = Process_Status_Idle;
		m_Pre_Proc_Stat = m_Proc_Stat;
	}
}

bool CParentProcess::WorkThreadState()
{
	return m_bThreadRun;
}

void CParentProcess::SetAndWaitThreadExit()
{
	if (m_threadHandle.isRunning() == false)
		return;
	if (m_bThreadRun == false) return;
	else
	{
		m_bThreadRun = false;
		while (m_threadHandle.isRunning() == true)
		{
			QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
		}
		//m_threadHandle.cancel();
		//m_threadHandle.waitForFinished();
	}
	m_Proc_Stat = Process_Status_Idle;
	m_Pre_Proc_Stat = m_Proc_Stat;
	ClearInheritProc();
}

int CParentProcess::Proc_Wrok_Thread()
{
	if (m_bThreadRun)
	{
		//emit SendProcMsg(QString("%1-->%2").arg(m_Proc_Name).arg("Proc already Run Cannot clicked Start!"));
		emit SendProMsgWithProfix(QStringList() << GetProcName(), "Proc already Run Cannot clicked Start!");

		return 3;
	}
	else
	{
		emit SignalProcStart(GetProcName());
		m_bThreadRun = true;
	}
	while (true)
	{
		ProcessState currentState =  this->GetProcState();
		switch (currentState)
		{
		//case Process_Status_Unknow:
		//	if (m_Pre_Proc_Stat != m_Proc_Stat)
		//	{
		//		emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_Unkonw");
		//		m_Pre_Proc_Stat = m_Proc_Stat;
		//	}
		//	m_bThreadRun = false;
		//	SpecialProcState(m_Proc_Name, m_Proc_Stat);
		//	emit SignalProcIdle(GetProcName());
		//	ClearInheritProc();
		//	return Process_Status_Unknow;
		//	break;
		case Process_Status_Idle:
			if (m_Pre_Proc_Stat != m_Proc_Stat)
			{
				SpecialProcState(m_Proc_Name, m_Proc_Stat);
				emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_Idle");
				m_Pre_Proc_Stat = m_Proc_Stat;
			}
			m_bThreadRun = false;
			ClearInheritProc();
			emit SignalProcIdle(GetProcName());
			return Process_Status_Idle;
			break;
		case Process_Status_Work:
			if (m_Pre_Proc_Stat != Process_Status_Work)
			{
				SpecialProcState(m_Proc_Name, m_Proc_Stat);
				emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_Busy");
				m_Pre_Proc_Stat = m_Proc_Stat;
			}
			StepManager();
			break;
		case Process_Status_Pause:
		{
			if (m_Pre_Proc_Stat != m_Proc_Stat)
			{
				SpecialProcState(m_Proc_Name, m_Proc_Stat);
				emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_Pause");
				m_Pre_Proc_Stat = m_Proc_Stat;
			}
			Sleep(10);
			break;
		}
		case Process_Status_Continue:
		{
			if (m_Pre_Proc_Stat != m_Proc_Stat)
			{
				SpecialProcState(m_Proc_Name, m_Proc_Stat);
				emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_Continue");
				m_Pre_Proc_Stat = m_Proc_Stat;
				m_Proc_Stat = Process_Status_Work;
			}
			Sleep(10);
			break;
		}
		case Process_Status_Stop:
			m_step_index = 0;
			if (m_Pre_Proc_Stat != Process_Status_Stop)
				emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_Stop Thread Exit");
			m_bThreadRun = false;
			SpecialProcState(m_Proc_Name, Process_Status_Stop);
			m_Pre_Proc_Stat = m_Proc_Stat;
			m_Proc_Stat = Process_Status_Idle;
			m_bThreadRun = false;
			m_Proc_Stat = Process_Status_Idle;
			ClearInheritProc();
			emit SignalProcIdle(GetProcName());
			m_bCountnue = true;
			return Process_Status_Stop;
		case Process_Status_End:
			m_step_index = 0;
			if (m_Pre_Proc_Stat != Process_Status_End)
				emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_End Thread Exit");
			m_Pre_Proc_Stat = m_Proc_Stat;
			m_Proc_Stat = Process_Status_Idle;
			m_bThreadRun = false;
			m_bFinsh = true;
			//特殊流程结束 

			SpecialProcState(m_Proc_Name, m_Proc_Stat);
			m_Proc_Stat = Process_Status_Idle;
			ClearInheritProc();
			emit SignalProcIdle(GetProcName());
			return Process_Status_End;
		case Process_Status_ErrorMoveEnd:
		{
			m_step_index = 0;
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_ErrorMoveEnd Thread Exit", ErrorMsg);
			m_Pre_Proc_Stat = m_Proc_Stat;
			m_bThreadRun = false;
			m_bFinsh = true;
			emit SignalExitExceptionFlag("Process_Status_ErrorMoveEnd", MoveError);
			SpecialProcState(m_Proc_Name, m_Proc_Stat);
			m_Proc_Stat = Process_Status_Idle;
			ClearInheritProc();
			emit SignalProcIdle(GetProcName());
			return Process_Status_ErrorMoveEnd;
		}
		case Process_Status_ErrorInfoEnd:
		{
			m_step_index = 0;
			//if (m_Pre_Proc_Stat != Process_Status_End)
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_ErrorInfoEnd Thread Exit",ErrorMsg);
			m_Pre_Proc_Stat = m_Proc_Stat;
			m_Proc_Stat = Process_Status_Idle;
			m_bThreadRun = false;
			m_bFinsh = true;
			emit SignalExitExceptionFlag("Process_Status_ErrorInfoEnd", InfoError);
			SpecialProcState(m_Proc_Name, m_Proc_Stat);
			m_Proc_Stat = Process_Status_Idle;
			ClearInheritProc();
			emit SignalProcIdle(GetProcName());
			return Process_Status_ErrorInfoEnd;
		}
		case Process_Status_ErrorEnd://未做区分的异常退出
		{
			m_step_index = 0;
			emit SendProMsgWithProfix(QStringList() << GetProcName(), "Process_Status_ErrorEnd Thread Exit", ErrorMsg);
			m_Pre_Proc_Stat = m_Proc_Stat;
			m_bThreadRun = false;
			m_bFinsh = true;
			emit SignalExitExceptionFlag("Process_Status_ErrorEnd", ExecuteError);
			SpecialProcState(m_Proc_Name, m_Proc_Stat);
			m_Proc_Stat = Process_Status_Idle;
			ClearInheritProc();
			m_bCountnue = true;
			emit SignalProcIdle(GetProcName());
			return Process_Status_ErrorInfoEnd;
		}
		default:
			break;
		}
		if (m_Pre_Proc_Stat != m_Proc_Stat)
			SpecialProcState(m_Proc_Name, m_Proc_Stat);

		Sleep(1);
		//强制退出
		if (m_bThreadRun == false)
			break;
	}
	m_bCountnue = true;
	m_bThreadRun = false;
	m_Proc_Stat = Process_Status_Idle;
	ClearInheritProc();
	emit SignalProcIdle(GetProcName());
	return NoError;
}

bool CParentProcess::Find_Proc(QString proc_name)
{
	foreach(ProcessInfo * proc, dataAnaly.m_CurProgram.processInfoList)
	{
		if (proc->aliasName == proc_name)
		{
			m_pInfo = proc;
			m_Proc_Name = proc_name;
			return true;
		}
	}
	return false;
}

void CParentProcess::Set_Proc(ProcessInfo* pInfo)
{
	if (pInfo == NULL)
	{
		//emit SendProcMsg("流程设置失败");
		emit SendProcMsg(QString("%1-->%2").arg(m_Proc_Name).arg("流程设置失败"));
		return;
	}
	m_pInfo = pInfo;
	m_Proc_Name = pInfo->aliasName;
}

bool CParentProcess::Get_Exception_End()
{
	return m_bException;
}

CtrlError CParentProcess::ExecStep(int stepIndex)
{

	emit SignalProCurrentStep(GetProcName(),stepIndex);
	if (m_pInfo == NULL)
	{
		m_msg = "流程信息为空";
		//emit SendProMsgWithProfix(QStringList() << GetProcName(), "Execute Proc is Empty");
		return CtrlError(NotFindStepInfo);
	}
	int stepCount = m_pInfo->stepInfoList.count();
	if (stepIndex > m_pInfo->stepInfoList.count() - 1)
	{
		//流程执行完成
		m_bFinsh = true;
		return CtrlError(NoError);;
	}

	foreach(StepInfo * step, m_pInfo->stepInfoList)
	{
		if (step->index == stepIndex)
		{
			m_curStepNmae = step->aliasName;
			if (step->aliasName == "等待流程结束")
			{
				static bool output = false;
				if (output == false)
				{
					output = true;
					emit SendProMsgWithProfix(QStringList() << "STEP" << step->Identification << QString::number(step->index) << step->aliasName, "Execute Step");

				}
			}
			else
				emit SendProMsgWithProfix(QStringList() << "STEP" << step->Identification << QString::number(step->index) << step->aliasName, "Execute Step");


			//执行步骤里面操作
			return ExecStep(step);
		}
	}



	//emit SendProMsgWithProfix(QStringList() << GetProcName(), "未查寻到相应步骤");
	return CtrlError(NotFindStepInfo);
}

void CParentProcess::StepManager()
{
	//配置文件执行模块
	CtrlError ret = ExecStep(m_step_index);
	if (ret != CtrlError(NoError))
	{
		m_Proc_Stat = Process_Status_ErrorEnd;
		m_bException = true;
		emit SignalExitExceptionFlag(QString("ExecStepError_procName-->%1; stepIndex-->%2;stepName-->%3;").arg(m_Proc_Name).arg(m_step_index).arg(m_curStepNmae));
		//emit SendProMsgWithProfix(QStringList() << GetProcName(), ret);

		// 步骤失败 流程退出
		return;
	}

	//步骤管控模块 当需要发送消息的时候可以进入到此处理
	ret = StepCodeDealwith(m_step_index);
	if (!ret)
	{
		m_Proc_Stat = Process_Status_End;
		m_bException = true;
		emit SignalExitExceptionFlag(QString("StepCodeError_procName-->%1; stepIndex-->%2;stepName-->%3;").arg(m_Proc_Name).arg(m_step_index).arg(m_curStepNmae));
		emit SendProMsgWithProfix(QStringList() << GetProcName(), ret);
		return;
	}
	if (m_bFinsh == true)
	{
		m_step_index = 0;
		m_bException = false;
		m_Proc_Stat = Process_Status_End;
		return;
	}
	m_Proc_Name;
	if(m_bCountnue)
		++m_step_index;
	Sleep(10);
}

void CParentProcess::SpecialProcState(QString procName, ProcessState state)
{
	//emit SignalProcState(procName, state);
	QString startProc = theMachineManager.GetStartProcName();
	QString resetProc = theMachineManager.GetResetProName();
	// 状态发送 只允许 复位流程和 主流程发送 
	if (procName == startProc || procName == resetProc)
	{
		emit SignalProcState(procName, state);
	}
}

bool CParentProcess::StepCodeDealwith(int step)
{
	if (m_pInfo == NULL)
		return false;
	if (m_Proc_Name != m_pInfo->aliasName)
	{
		emit SendProcMsg("流程名称与预设流程名不一致");
		return false;
	}
	if (m_Proc_Name == "整机复位")
		return ResetMatchWorkEndCodeSupport(step);
	else if (m_Proc_Name == "主流程")
	{
	}
	else if (m_Proc_Name == "机械手1工作流程Test")
		return Robot1WorkProcEndCodeSupport(step);
	else if (m_Proc_Name == "机械手2工作流程Test")
		return Robot2WorkProcEndCodeSupport(step);
	else if (m_Proc_Name == "上料皮带线工作流程Test")
	{
	}
	else if (m_Proc_Name == "下料皮带线工作流程Test")
	{
	}
	else if (m_Proc_Name == "放OK料工作流程Test")
		return Robot2WorkProcPutOkEndCodeSupport(step);
	else if (m_Proc_Name == "放NG料工作流程Test")
		return Robot2WorkProcPutNGEndCodeSupport(step);
	else if (m_Proc_Name == "NG下料工作流程Test")
		return NGDownloadEndCodeSupport(step);
	else if (m_Proc_Name == "NG上料工作流程Test")
		return NGUploadEndCodeSupport(step);
	else if (m_Proc_Name == "NG移载工作流程Test")
		return NGMoveEndCodeSupport(step);
	else if (m_Proc_Name == "NG上料复位Test")
	{
	}
	else if (m_Proc_Name == "NG下料复位Test")
	{
	}
	else if (m_Proc_Name == "NG移载复位Test")
	{
	}
	return true;
}


CtrlError CParentProcess::ExecStep(StepInfo * step)
{
	m_startTime = QTime::currentTime();
	if (step->bUse == 0)
		return CtrlError(NoError);
	QList<QFuture<int>> futureList;
	//创建子线程处理Operator 还有数据要进行处理
	foreach(OperatorInfo * op, step->opeartorInfoList)
	{
		QFuture<int> fu = QtConcurrent::run(this, &CParentProcess::ExecOperator, op, m_Proc_Stat);
		futureList.push_back(fu);
	}
	//等待所有操作退出
	while (true)
	{
		QThread::msleep(10);
		int finshedNum = 0;
		foreach(QFuture<int> fu, futureList)
		{
			if (fu.isFinished())
			{
				int eRet = fu.result();
				finshedNum++;
				if (eRet != NoError)
					return CtrlError(eRet);
			}
		}
		//流程结束
		if (finshedNum == futureList.count())
			break;

		if (!m_bThreadRun)
			return ForceExit;
	}

	////等待所有操作退出
	//foreach(QFuture<int> fu, futureList)
	//	fu.waitForFinished();

	////检测操作 是否执行成功
	//foreach(QFuture<int> fu, futureList)
	//{
	//	int bSuccess = fu.result();
	//	if (bSuccess != 0)
	//		return CtrlError(bSuccess);
	//}

	//执行成功返回true;
	// 是否需要进行消息处理
	if (step->CodSupplement == 1)
	{
		if (!EndStepCodeSuportElement(step->index))
			return CtrlError(EndStepCodeSuportError);
	}
	//计时
	m_endTime = QTime::currentTime();
	if (step->aliasName != "等待流程结束")
	{
		theFileManager.WriteTodayFile_CSV(QString("%1,%2,%3,%4")
			.arg(step->Identification).arg(m_Proc_Name).arg(step->aliasName).arg(m_startTime.msecsTo(m_endTime)));
	}

	return  CtrlError(NoError);
}

bool CParentProcess::ExecStepInfoNotWait(StepInfo * step)
{
	QString msg = QString("stepIndex:%1; stepName:%2;stepType:%3;").arg(m_step_index).arg(step->aliasName).arg(step->stepType);

	emit SendProcMsg(QString("%1-->%2").arg(m_Proc_Name).arg(QString("ExecStep index -->%1").arg(msg)));

	if (step->bUse == 0)
		return true;


	//emit SendProcMsg("Step :" + step->aliasName + " " + step->index + " " + step->stepType);
	//步骤中需要添加流程状态
	//需要判断线程是否执行完成

	//static bool ExecOperator(OperatorInfo*op, Process_Status& currentStatus);
	QList<QFuture<int>> futureList;

	//创建子线程处理Operator 还有数据要进行处理
	foreach(OperatorInfo * op, step->opeartorInfoList)
	{
		QFuture<int> fu = QtConcurrent::run(this, &CParentProcess::ExecOperator, op, m_Proc_Stat);
		futureList.push_back(fu);
	}
	return true;
}

int CParentProcess::ExecOperator(OperatorInfo*op, ProcessState& currentStatus)
{
	//QString str = QString("操作标识:%1 ; 操作工站：%2 ；操作名称：%3 ；操作类型：%4")
	//	.arg(op->Identification).arg(op->station).arg(op->aliasName).arg(op->oprtType);
	//emit SendProcMsg(QString("%1-->%2").arg(m_Proc_Name).arg(str));

	QTime opStartTime = QTime::currentTime();

	if (op->oprtType == "waitFlowEnd")
	{
		static bool output = false;
		if (output == false)
		{
			emit SendProMsgWithProfix(QStringList() << "OP" << op->Identification << op->station << op->oprtType, QString("%1%2").arg("执行:").arg(op->aliasName));
			output = true;
		}
	}
	else
		emit SendProMsgWithProfix(QStringList() << "OP" << op->Identification << op->station << op->oprtType, QString("%1%2").arg("执行:").arg(op->aliasName));

	if (op->bUse == 0)
		return NoError;
	//查表操作
	Operator_Type operatorType = InquireOperatorType(op->oprtType);
	if (operatorType == Operator_Type_NotContain)
	{
		//emit SendProcMsg(QString("%1-->%2").arg(op->Identification).arg(QString(CtrlError(Operator_Type_NotContain))),ErrorMsg);
		emit SendProMsgWithProfix(QStringList() << "OP" << op->Identification << op->station << op->oprtType, CtrlError(OperatorTypeNotContain),ErrorMsg);
		return OperatorTypeNotContain;
	}
	QString opExeMsg;
	int IError;
	//执行操作
	try
	{
		IError = PerformOperation(op, operatorType, opExeMsg);
	}
	catch (...)
	{
		emit SendProMsgWithProfix(QStringList() << "OP" << op->Identification << op->station << op->oprtType,"捕捉到未知的异常", ErrorMsg);
		return UndefineError;
	}

	if (NoError != IError)
		emit SendProMsgWithProfix(QStringList() << "OP" << op->Identification << op->station << op->oprtType, CtrlError(IError), ErrorMsg);
	QTime opEndTime = QTime::currentTime();
	if (!op->aliasName.contains("流程结束"))
	{
		theFileManager.WriteTodayFile_CSV(QString("%1,%2,%3,%4")
			.arg(op->Identification).arg(m_Proc_Name).arg(op->aliasName).arg(opStartTime.msecsTo(opEndTime)));
	}
	//emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4-->%5").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(QString(CtrlError(IError))).arg(opExeMsg));
	return 	IError;
}

Operator_Type CParentProcess::InquireOperatorType(QString typeName)
{
	if ("单轴回原" == typeName)
	{
	}
	else if ("工站回原" == typeName)
	{
	}
	else if ("NULL" == typeName)
	{
		return Operator_Type_Empty;
	}
	else if ("单轴运动" == typeName)
	{
		return Operator_Type_Axis_Move;
	}
	else if ("工站运动" == typeName)
	{
		return Operator_Type_Station_Move;
	}
	else if ("OutPutWrite" == typeName)
	{
		return Operator_Type_Write_OutPut;
	}
	//Operator_Type_Station_Check_Pos
	else if ("工站点位检测" == typeName)
	{
		//检查工站是否到达目标位置， 停止退出 ，到位退出，暂停循环检测。
		return Operator_Type_Station_Check_Pos;
	}
	else if ("InputRead" == typeName)
	{
		return Operator_Type_WaitInput;
	}
	else if ("标志位设置" == typeName)
	{
	}
	else if ("标志位检测" == typeName)
	{
	}
	else if ("标志位等待" == typeName)
	{
	}
	else if ("延时操作" == typeName)
	{
	}
	else if ("内部步骤跳转" == typeName)
	{
	}
	else if ("外部流程启动" == typeName)
	{
	}
	else if ("单轴偏移运动" == typeName)
	{
		return Operator_Type_Axis_OffSet_Move;
	}
	else if ("串口信息发送" == typeName)
	{
	}
	else if ("串口信息接收" == typeName)
	{
	}
	else if ("Tcp 信息接收" == typeName)
	{
	}
	else if ("Tcp 发送" == typeName)
	{
	}
	else if ("msgBox" == typeName)
	{
		return Operator_Type_Msg_Box;
	}
	else if ("delay" == typeName)
	{
		return Operator_Type_Delay;
	}
	else if ("startFlow" == typeName)
	{
		return Operator_Type_Start_Flow;
	}
	else if ("TestDebug" == typeName)
	{
		return Operator_Type_TestDebug;
	}
	else if ("ErrorTestDebug" == typeName)
		return Operator_Type_ErrorTestDebug;
	//Operator_Type_ShowMulityTempImgWithResult
	else if ("ShowMulityTempImg" == typeName) return Operator_Type_ShowMulityTempImg;
	//
	else if ("ShowMulityTempImgWithResult" == typeName)
	{
		return Operator_Type_ShowMulityTempImgWithResult;
	}
	else if ("waitFlowStart" == typeName)
	{
		return Operator_Type_Wait_Flow_Start;
	}
	else if ("waitFlowEnd" == typeName)
	{
		return Operator_Type_Wait_Flow_End;
	}
	//Operator_Type_Flow_End
	else if ("FlowEnd" == typeName)
	{
		return Operator_Type_Flow_End;
	}
	else if ("writeFlag" == typeName)
	{
		return Operator_Type_Write_Flag;
	}

	else if ("waitFlag" == typeName)
	{
		return Operator_Type_Wait_Flag;
	}
	else if ("WriteTempFlag" == typeName)	return Operator_Type_WriteTemp_Flag;
	else if ("SelfAddFlag" == typeName)	return Operator_Type_SelfAddFlag;
	else if ("SelfMinuFlag" == typeName)	return Operator_Type_SelfMinuFlag;

	else if ("WaitAndSetFlag" == typeName)
	{
		return Operator_Type_WaitAndSet_Flag;
	}
	else if ("ClearAllFlag" == typeName)
	{
		return Operator_Type_Clear_Flag;
	}
	else if ("FlagJump" == typeName)
	{
		return Operator_Type_Flag_Jump;
	}
	else if ("SwitchFlagJump" == typeName)
	{
		return Operator_Type_SwitchFlagJump;
	}
	else if ("MulityFlagLogicalAndJump" == typeName)
		return Operator_Type_MulityFlagLogicalAndJump;
	else if ("MulityFlagLogicalORJump" == typeName)
		return Operator_Type_MulityFlagLogicalORJump;

	else if ("CreateSN" == typeName)
	{
		return Operator_Type_CreateSN;
	}
	//else if ("DeleteSN" == typeName)
	//{
	//	return Operator_Type_DeleteSN;
	//}
	else if ("GetSN" == typeName)
	{
		return Operator_Type_GetSN;
	}
	else if ("CreateStartTime" == typeName) return Operator_Type_CreateStartTime;
	else if ("ElapsedFromTime" == typeName) return Operator_Type_ElapsedFromTime;
	else if ("ElapsedFromTwoTime" == typeName) return Operator_Type_ElapsedFromTwoTime;
	else if ("jumpIndex" == typeName)		return Operator_Type_Jump;
	else if ("WriteTempString" == typeName)	return Operator_Type_WriteTempString;
	else if ("ReadTempString" == typeName)		return Operator_Type_ReadTempString;
	else if ("外部流程开启" == typeName)
	{
		//
	}
	else if ("等待轴停止" == typeName)
	{
		return Operator_Type_Axis_Wait_Stop;
	}
	else if ("单轴到位检测" == typeName)
	{
		return Operator_Type_Axis_Check_Pos;
	}
	else if ("Robot_GetStatus" == typeName)
	{
		return Operator_Type_Robot_GetStatus;
	}
	else if ("Robot_MovePosition" == typeName) 
	{ 
		return Operator_Type_Robot_MovePosition; 
	}
	else if ("Robot_Reset" == typeName)
	{ 
		return Operator_Type_Robot_Reset;
	}
	else if ("Robot_SetEnable" == typeName)
	{
		return Operator_Type_Robot_SetEnable;
	}
	else if ("Robot_MovePositionWithIMS" == typeName)
	{
		return Operator_Type_Robot_MovePositionWithIMS;
	}
	else if ("Robot_MovePositionWithIMS6" == typeName)
	{
		return Operator_Type_Robot_MovePositionWithIMS6;
	}
	else if ("Robot_WaitArrive" == typeName) 
	{ 
		return Operator_Type_Robot_WaitArrive; 
	}
	else if ("Lighting_SetLight" == typeName) 
	{
		return Operator_Type_Lighting_SetLight;
	}
	else if ("Lighting_SetAllLight" == typeName) 
	{
		return Operator_Type_Lighting_SetAllLight;
	}
	else if ("Lighting_GetLight" == typeName) 
	{
		return Operator_Type_Lighting_GetLight;
	}
	else if ("Lighting_GetAllLight" == typeName)
	{
		return Operator_Type_Lighting_GetAllLight;
	}
	else if ("Lighting_WaitLight" == typeName) 
	{ 
		return Operator_Type_Lighting_WaitLight;
	}
	else if ("Lighting_WaitAllLight" == typeName) 
	{
		return Operator_Type_Lighting_WaitAllLight; 
	}
	else if ("Lighting_SetLightAndWait" == typeName)
	{ 
		return Operator_Type_Lighting_SetLightAndWait;
	}
	else if ("Lighting_SetAllLightAndWait" == typeName) 
	{
		return Operator_Type_Lighting_SetAllLightAndWait;
	}
	else if ("Conveyor_Start" == typeName)
	{ 
		return Operator_Type_Conveyor_Start;
	}
	else if ("Conveyor_Stop" == typeName)
	{ 
		return Operator_Type_Conveyor_Stop; 
	}
	else if ("Conveyor_GetStatus" == typeName)
	{
		return Operator_Type_Conveyor_GetStatus;
	}
	else if ("Conveyor_SetEnable" == typeName) 
	{
		return Operator_Type_Conveyor_SetEnable;
	}
	else if ("Conveyor_SetVel" == typeName)
	{ 
		return Operator_Type_Conveyor_SetVel;
	}
	else if ("Conveyor_ClearErr" == typeName)
	{
		return Operator_Type_Conveyor_ClearErr;
	}
	else if ("Camera_Open" == typeName)
	{
		return Operator_Type_Camera_Open; 
	}
	else if ("Camera_Close" == typeName)
	{
		return Operator_Type_Camera_Close;
	}
	else if ("Camera_GetStatus" == typeName)
	{ 
		return Operator_Type_Camera_GetStatus;
	}
	else if ("Camera_GetImage" == typeName)
	{
		return Operator_Type_Camera_GetImage; 
	}
	else if ("Camera_GetImageAndSave" == typeName)
	{
		return Operator_Type_Camera_GetImageAndSave; 
	}
	else if ("Camera_GetImageSaveAndDisp" == typeName)
	{
		return Operator_Type_Camera_GetImage_Save_Disp;
	}
	else if ("Camera_GetImage2" == typeName)
	{
		return Operator_Type_Camera_GetImage2;
	}
	else if ("Camera_GetImageAndSave2" == typeName)
	{
		return Operator_Type_Camera_GetImageAndSave2;
	}
	else if ("Camera_GetImageAndSaveRecord" == typeName)
	{
		return Operator_Type_Camera_GetImageAndSaveRecord;
	}
	else if ("Camera_GetImageAndSaveRecord2" == typeName)
	{
		return Operator_Type_Camera_GetImageAndSaveRecord2;
	}
	else if ("PanaMotor_MovePos" == typeName)
	{ 
		return Operator_Type_PanaMotor_MovePos;
	}
	else if ("PanaMotor_MoveOffset" == typeName)
	{
		return Operator_Type_PanaMotor_MoveOffset;
	}
	else if ("PanaMotor_ComparePos" == typeName)
	{
		return Operator_Type_PanaMotor_ComparePos;
	}
	else if ("PanaMotor_Stop" == typeName) 
	{ 
		return Operator_Type_PanaMotor_Stop;
	}
	else if ("PanaMotor_GetPos" == typeName) 
	{ 
		return Operator_Type_PanaMotor_GetPos; 
	}
	else if ("PanaMotor_GetStatus" == typeName)
	{ 
		return Operator_Type_PanaMotor_GetStatus;
	}
	else if ("PanaMotor_SetEnable" == typeName) 
	{
		return Operator_Type_PanaMotor_SetEnable; 
	}
	else if ("PanaMotor_SetVel" == typeName)
	{
		return Operator_Type_PanaMotor_SetVel;
	}
	else if ("PanaMotor_WaitArrive" == typeName) 
	{ 
		return Operator_Type_PanaMotor_WaitArrive;
	}
	else if ("PanaMotor_WaitStop" == typeName)
	{
		return Operator_Type_PanaMotor_WaitStop;
	}
	else if ("PanaMotor_Reset" == typeName)
	{ 
		return Operator_Type_PanaMotor_Reset;
	}
	else if ("IOControl_SetOutPut" == typeName) 
	{ 
		return Operator_Type_IOControl_SetOutPut;
	}
	else if ("IOControl_SetOutPuts" == typeName) 
	{
		return Operator_Type_IOControl_SetOutPuts; 
	}
	else if ("IOControl_GetInPut" == typeName) 
	{ 
		return Operator_Type_IOControl_GetInPut;
	}
	else if ("IOControl_GetInPutJump" == typeName)
	{
		return Operator_Type_IOControl_GetInPutJumpStep;
	}
	else if ("IOControl_GetInPutAndSetFlag" == typeName)
	{
		return Operator_Type_IOControl_GetInPutAndSetFlag;
	}
	else if ("IOControl_GetInPuts" == typeName)
	{ 
		return Operator_Type_IOControl_GetInPuts;
	}
	else if ("IOControl_GetOutPut" == typeName) 
	{ 
		return Operator_Type_IOControl_GetOutPut;
	}
	else if ("IOControl_GetOutPuts" == typeName)
	{ 
		return Operator_Type_IOControl_GetOutPuts;
	}
	else if ("IOControl_GetIONames" == typeName)
	{
		return Operator_Type_IOControl_GetIONames;
	}
	else if ("IOControl_WatiInput" == typeName)
	{
		return Operator_Type_IOControl_WatiInput;
	}
	else if ("IOControl_ContinueCheck" == typeName)
	{
		return Operator_Type_IOControl_ContinueCheck;
	}
	else if ("AlgoManager_SendCmd" == typeName) 
	{ 
		return Operator_Type_AlgoManager_SendCmd;
	}
	else if ("AlgoManager_SendCmdPOS" == typeName) 
	{
		return Operator_Type_AlgoManager_SendCmdPOS; 
	}
	else if ("AlgoManager_SendCmdSRC" == typeName) 
	{ 
		return Operator_Type_AlgoManager_SendCmdSRC; 
	}
	else if ("AlgoManager_SendCmdMOON" == typeName)
	{
		return Operator_Type_AlgoManager_SendCmdMOON; 
	}
	else if ("AlgoManager_SendCmdIMS" == typeName) 
	{ 
		return Operator_Type_AlgoManager_SendCmdIMS; 
	}
	else if ("AlgoManager_SendCmdIMSMulity" == typeName)
	{
		return Operator_Type_AlgoManager_SendCmdIMSMulity;
	}
	else if ("AlgoManager_SendCmdIMSMulityADFOnePic" == typeName) return Operator_Type_AlgoManager_SendCmdIMSMulityADFOnePic;
	else if ("AlgoManager_SetBarCode" == typeName) 
	{ 
		return Operator_Type_AlgoManager_SetBarCode;
	}
	else if ("AlgoManager_ChcekFinish" == typeName)
	{ 
		return Operator_Type_AlgoManager_ChcekFinish;
	}
	//Operator_Type_AlgoManager_WaitFinish
	else if ("AlgoManager_WaitFinish" == typeName)
	{
		return Operator_Type_AlgoManager_WaitFinish;
	}
	else if ("AlgoManager_GetResult" == typeName) 
	{ 
		return Operator_Type_AlgoManager_GetResult; 
	}
	else if ("AlgoManager_WaitResult" == typeName)
	{
		return Operator_Type_AlgoManager_WaitResult;
	}
	else if ("AlgoManager_WaitResultSetFlag" == typeName)
	{
		return Operator_Type_AlgoManager_WaitResultSetFlag;
	}
	if("AlgoManager_WaitResultSetFlagADF" == typeName)
		return Operator_Type_AlgoManager_WaitResultSetFlagADF;

	else if ("AlgoManager_WaitResultSetFlagByResultInfoMulity" == typeName)
	{
		return Operator_Type_AlgoManager_WaitResultSetFlagByResultInfoMulity;
	}
	else if ("AlgoManager_GetAllResult" == typeName) 
	{ 
		return Operator_Type_AlgoManager_GetAllResult;
	}
	else if ("AlgoManager_GetAllResultSetFlag" == typeName)
	{
		return Operator_Type_AlgoManager_GetAllResultSetFlag;
	}
	else if ("AlgoManager_Complete" == typeName) 
	{ 
		return Operator_Type_AlgoManager_Complete; 
	}
	else if ("AlgoManager_LoadGolden" == typeName)
	{
		return Operator_Type_AlgoManager_LoadGolden;
	}
	//
	//else if ("EPSONRobot_SendCmd" == typeName)
	//{
	//	return Operator_Type_EPSON_Robot_SendCmd;
	//}
	else if ("EPSONRobot_SendAndRecv" == typeName)
	{
		return Operator_Type_EPSON_Robot_SendCmdAndRecv;
	}
	else if ("EPSONRobot_WaitRobotCmdSetFlag" == typeName)
	{
		return Operator_Type_EPSON_Robot_WaitRobotCmdSetFlag;
	}
	else if ("PLC_ReadPort" == typeName)
	{
		return Operator_Type_PLC_ReadPort;
	}
	else if ("PLC_WritePort" == typeName)
	{
		return Operator_Type_PLC_WritePort;
	}
	else if ("PLC_WaitPort" == typeName)
	{
		return Operator_Type_PLC_WaitPort;
	}
	//ETH 
	else if ("ETHPLC_Connect" == typeName) return 	Operator_Type_ETH_PLC_Connect;//读信号
	else if ("ETHPLC_Disconnect" == typeName) return 	Operator_Type_ETH_PLC_Disconnect;//读信号
	//Operator_Type_ETH_PLC_Disconnect

	else if ("ETHPLC_ReadPortInt" == typeName) return 	Operator_Type_ETH_PLC_ReadPortInt;//读信号
	
	else if ("ETHPLC_WriteTempPosToPLC" == typeName) return 	Operator_Type_ETH_PLC_WriteTempPosToPLC;//读信号
	else if ("ETHPLC_WritePortInt" == typeName) return 	Operator_Type_ETH_PLC_WritePortInt;//写信号
	else if ("ETHPLC_WritePort" == typeName) return 	Operator_Type_ETH_PLC_WritePortInt;//写信号
	else if ("ETHPLC_WaitPortInt" == typeName) return 	Operator_Type_ETH_PLC_WaitPortInt;//等待信号
	else if ("ETHPLC_WaitPort" == typeName) return 	Operator_Type_ETH_PLC_WaitPortInt;//等待信号
	//else if ("ETHPLC_WaitPort" == typeName) return 	Operator_Type_ETH_PLC_WaitPortInt;//等待信号
	else if ("ETHPLC_WaitPortFloat" == typeName) return Operator_Type_ETH_PLC_WaitPortFloat;//等待信号
	else if ("ETHPLC_ReadPortFloat" == typeName) return Operator_Type_ETH_PLC_ReadPortFloat;//读信号
	else if ("ETHPLC_WritePortFloat" == typeName) return 	Operator_Type_ETH_PLC_WritePortFloat;//读信号

	else if ("ETHPLC_WritePortAndWaitTimesInt" == typeName) return 	Operator_Type_ETH_PLC_WritePortAndWaitTimesInt;//读信号
	else if ("ETHPLC_WritePortAndWaitTimesFloat" == typeName) return 	Operator_Type_ETH_PLC_WritePortAndWaitTimesFloat;//读信号



	else if ("ETHPLC_CheckPortSetFlag" == typeName) return 	Operator_Type_ETH_PLC_CheckProtIntSetFlag;//等待信号
	else if ("ETHPLC_CheckPortIntSetFlag" == typeName) return 	Operator_Type_ETH_PLC_CheckProtIntSetFlag;//等待信号
	else if ("ETHPLC_CheckPortFloatSetFlag" == typeName) return 	Operator_Type_ETH_PLC_CheckProtFloatSetFlag;//等待信号

	else if ("ETH_PLC_ReadPortIntWriteToTempDataMulity" == typeName) return 	Operator_Type_ETH_PLC_ReadPortIntWriteToTempDataMulity;//读信号
	else if ("ETH_PLC_ReadPortFloatWriteToTempDataMulity" == typeName) return 	Operator_Type_ETH_PLC_ReadPortFloatWriteToTempDataMulity;//读信号

	else if ("ETHPLC_WriteTempDataMulityInt" == typeName) return 	Operator_Type_ETH_PLC_WriteTempDataMulityInt;//等待信号
	else if ("ETHPLC_WriteTempDataMulityFloat" == typeName) return 	Operator_Type_ETH_PLC_WriteTempDataMulityFloat;//等待信号
	else if ("ETHPLC_WriteDataCheck" == typeName) return 	Operator_Type_ETH_PLC_WriteDataCheck;//等待信号

	else if ("TCP_ClientCreateTcpClientConnectServer" == typeName) return Operator_Type_ClientCreateTcpClientConnectServer;
	else if ("TCP_ClientOperator_Type_WaitClientReConnect" == typeName) return Operator_Type_ClientWaitClientReConnect;
	else if ("TCP_ClientSendDataToServer" == typeName) return Operator_Type_ClientSendDataToServer;
	else if ("TCP_ClientSendTempDataToServer" == typeName) return Operator_Type_ClientSendTempDataToServer;
	else if ("TCP_ClientSendTempDataToServerMulity" == typeName) return Operator_Type_ClientSendTempDataToServerMulity;
	
	else if ("TCP_ADFClientSendTempDataToServerMulity" == typeName) return Operator_Type_ADFClientSendTempDataToServerMulity;
	
	else if ("TCP_ClientSendAndRecv" == typeName) return Operator_Type_ClientSendAndRecv;
	else if ("TCP_ClientWaitOneMsgFromSever" == typeName) return Operator_Type_ClientWaitOneMsgFromSever;
	else if ("TCP_ClientWaitOneMsgFromSeverEqualParam1" == typeName) return Operator_Type_ClientWaitOneMsgFromSeverEqualParam1;
	else if ("TCP_ADFClientWaitOneMsgFromSeverShockPlate" == typeName) return Operator_Type_ADFClientWaitOneMsgFromSeverShockPlate;

	//else if ("TCP_ClientWaitOneMsgSetFlageByContent" == typeName) return Operator_Type_ClientWaitOneMsgSetFlageByContent;
	else if ("TCP_ClientDeleteOnClient" == typeName) return Operator_Type_ClientDeleteOnClient;
	else if ("TCP_ClientDataClear" == typeName) return Operator_Type_ClientDataClear;
	
	//server
	else if ("TCP_ServerCreateTcpServer" == typeName) return Operator_Type_SeverCreateTcpServer;
	else if ("TCP_ServerSendMsgToSkt" == typeName) return Operator_Type_SeverSendMsgToSkt;
	else if ("TCP_ServerRecvMsgFromSkt" == typeName) return Operator_Type_SeverRecvOneMsgFromSkt;
	//count 
	else if ("RefreshCTWithTowTimePoint" == typeName)		return Operator_Type_RefreshCTWithTowTimePoint;
	else if ("AddOneProduct" == typeName)		return Operator_Type_AddOneProduct;
	else if ("AddOneOKProduct" == typeName)	return Operator_Type_AddOneOkProduct;
	else if ("AddOneNGProduct" == typeName)	return Operator_Type_AddOneNGProduct;
	else if ("ClearCount" == typeName)	return Operator_Type_ClearCount;
	else if ("ShowInfo" == typeName)	return Operator_Type_ShowInfo;
	else if ("DeleteDir" == typeName)	return Operator_Type_DeleteDir;
	
	return Operator_Type_NotContain;				//错误操作
}

int CParentProcess::PerformOperation(OperatorInfo*op, Operator_Type type, QString& execMsg)
{
	switch (type)
	{
	case Operator_Type_Empty:
	{
		break;
	}
	case Operator_Type_Robot_GetStatus:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			execMsg = QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet));
			emit SendProcMsg(execMsg,ErrorMsg);
			return  iRet;
		}
		break;
	}
	case Operator_Type_Robot_MovePosition: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Point", tTeachData);
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Robot_Reset: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Robot_SetEnable: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Robot_MovePositionWithIMS:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "MovePositionByPose";
		CtrlError  iRet;
		//点位运动
		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("TimeOut", op->param5);

		QString pointStr;
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.GetTempStringByKey(surface_point_key, pointStr);
		QStringList tempList = pointStr.split(",");
		RobotPoint target_point  =  tTeachData.value<RobotPoint>();
		target_point.Pose[0] = tempList[0].toDouble();
		target_point.Pose[1] = tempList[1].toDouble();

		tTeachData = QVariant::fromValue(target_point);
		ExecuteData.Datas["Point"] = tTeachData;		

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		//WaitArriveByPose
		ExecuteData.OperatorName = "WaitArriveByPose";
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Robot_MovePositionWithIMS6:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "MovePosition";
		CtrlError  iRet;
		//点位运动
		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);

		ExecuteData.Datas.insert("TimeOut", op->param5);
		QString pointStr;
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.GetTempStringByKey(surface_point_key, pointStr);
		QStringList tempList = pointStr.split(",");
		RobotPoint target_point = tTeachData.value<RobotPoint>();
		target_point.Pos[5] += tempList[0].toDouble();


		tTeachData = QVariant::fromValue(target_point);
		ExecuteData.Datas["Point"] = tTeachData;
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		//WaitArriveByPose
		ExecuteData.OperatorName = "WaitArrive";
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Robot_WaitArrive: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());


		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Point", tTeachData);

		//ExecuteData.Datas["Point"] = op->postion;
		ExecuteData.Datas["TimeOut"] = op->param5.toInt();

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_SetLight: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",") ;
		for (int i = 0; i < keys.count();++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_SetAllLight: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Value", tTeachData);

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_GetLight: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_GetAllLight: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		if (!ResultData.Datas.keys().contains("Value"))
		{
			emit SendProcMsg(QString("Identification:%1,%2").arg(op->Identification).arg("返回值不包含键 Value"));
			return false;
		}
		QString msg;
		foreach (QString key ,ResultData.Datas.keys())
		{
			msg = msg + key + ":" + ResultData.Datas[key].toInt() + ";";
		}
		emit SendProcMsg(QString("Identification:%1,%2").arg(op->Identification).arg(msg));
		break;
	}
	case Operator_Type_Lighting_WaitLight:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);

		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_WaitAllLight:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Value", tTeachData);
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString msg = QString("Identification:%1;OpName:%2;OperatorType:%3;ErrorCode:%4;errorMsg:%5;").arg(op->Identification)
				.arg(op->aliasName).arg(op->oprtType).arg((QString("%1")
				.arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet));
			emit SendProcMsg(msg, ErrorMsg);
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_SetLightAndWait:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString msg = QString("Identification:%1;OpName:%2;OperatorType:%3;ErrorCode:%4;errorMsg:%5;").arg(op->Identification)
				.arg(op->aliasName).arg(op->oprtType).arg((QString("%1")
					.arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet));
			emit SendProcMsg(msg, ErrorMsg);
			return  iRet;
		}
		break;
	}
	case Operator_Type_Lighting_SetAllLightAndWait:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet;
		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Value", tTeachData);
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString msg = QString("Identification:%1;OpName:%2;OperatorType:%3;ErrorCode:%4;errorMsg:%5;").arg(op->Identification)
				.arg(op->aliasName).arg(op->oprtType).arg((QString("%1")
					.arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet));
			emit SendProcMsg(msg, ErrorMsg);
			return  iRet;
		}
		if (0 != iRet)
		{
			QString msg = QString("Identification:%1;OpName:%2;OperatorType:%3;ErrorCode:%4;errorMsg:%5;").arg(op->Identification)
				.arg(op->aliasName).arg(op->oprtType).arg((QString("%1")
					.arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet));
			emit SendProcMsg(msg, ErrorMsg);
			return  iRet;
		}
		break;
	}

	case Operator_Type_Conveyor_Start:
	{		
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = op->oprtType.right(op->oprtType.size() - op->oprtType.lastIndexOf("_") - 1);

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		emit SendProcMsg(QString("strat end coveyor %1").arg(ExecuteData.Identfication));
		break;
	}
	case Operator_Type_Conveyor_Stop: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		emit SendProcMsg(QString("stop end coveyor %1").arg(ExecuteData.Identfication));
		break;
	}
	case Operator_Type_Conveyor_GetStatus: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Conveyor_SetEnable: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Conveyor_SetVel: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Conveyor_ClearErr: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_Open: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_Close: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_GetStatus: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_GetImage:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i].toDouble();
		}
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_GetImageAndSave: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetImageAndSave";
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i].toDouble();
		}

		QString dateTime = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss_zzz");
		QFileInfo fileInfo(op->param3);
		if (!fileInfo.exists())
		{
			QDir dir;
			dir.mkdir(op->param3);
			if (!dir.mkdir(op->param3))
			{
				emit SendProcMsg("dir not exist!");
				return FileNotExist;
			}
		}

		QString imgPath = op->param3 + "/" + dateTime + ".bmp";
		ExecuteData.Datas.insert("Path", imgPath);



		CtrlError  iRet;
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}


		break;
	}
	case Operator_Type_Camera_GetImage_Save_Disp:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetImageAndSave";
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i].toDouble();
		}

		QString dateTime = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss_zzz");
		QFileInfo fileInfo(op->param3);
		if (!fileInfo.exists())
		{
			QDir dir;
			dir.mkdir(op->param3);
			if (!dir.mkdir(op->param3))
			{
				emit SendProcMsg("dir not exist!");
				return FileNotExist;
			}
		}

		QString imgPath = op->param3 + "/" + dateTime + ".bmp";
		
		ExecuteData.Datas.insert("Path", imgPath);
		CtrlError  iRet;
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		//CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);


		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		//内容处理
		cv::Mat SourImg,OutImg;
		SourImg = cv::imread(imgPath.toStdString(),1);
		OutImg = SourImg;

		QString imgResPath = op->param3;
		//imgResPath = imgResPath.left(imgResPath.size() - imgResPath.lastIndexOf("/") - 1);
		imgResPath  = imgResPath +"/" + dateTime + "_Res" + ".bmp";
		cv::imwrite(imgResPath.toStdString(), OutImg);
		//图片显示
		QMap<QString, QVariant> dispData;

		Result_Data data;
		data.result = "OK";
		data.reslutImgPath = imgResPath;
		dispData.insert("1", QVariant::fromValue(data));
		//存储图片路径  面号 对应的图片路径
		theFlageManager.SetTempStringByKey(op->param5,imgResPath);
		//存储SN
		QString SN = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss-zzz");
		theFlageManager.SetTempStringByKey(op->param5+"SN", SN);
		emit SignalSendImg(dispData);
		break;
	}
	case Operator_Type_Camera_GetImage2:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Param", tTeachData);

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_GetImageAndSave2:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Param", tTeachData);

		QString dateTime = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss_zzz");
		ExecuteData.Datas.insert("Path", op->param3+"/"+ dateTime + ".bmp");

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_Camera_GetImageAndSaveRecord:
	{
		CtrlError iRet;
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetImageAndSave";

		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i].toDouble();
		}
#pragma region insert path
		QString dateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss-zzz");
		QFileInfo fileInfo(op->param3);
		if (!fileInfo.exists())
		{
			QDir dir;
			dir.mkdir(op->param3);
			if (!dir.mkdir(op->param3))
			{
				emit SendProcMsg("dir not exist!");
				return FileNotExist;
			}
		}
		//获取SN
		QString curDayDir = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss_zzz");
		QString SN;
		//参数五是 记录的 SN key值
		iRet = theFlageManager.GetTempStringByKey(op->param5, SN);
		if (iRet!=0)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName).arg(op->oprtType)
				.arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		//图片路径
		QDir dir;
		dir.mkpath(op->param3 + "/" + SN);
		QString imgPath = op->param3 + "/"+ SN+"/" + dateTime +"_"+ op->param4 +".bmp";
		ExecuteData.Datas.insert("Path", imgPath);
#pragma endregion

#pragma region exec command
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
#pragma endregion
		//存储图片路径  面号 对应的图片路径
		theFlageManager.SetTempStringByKey(op->param4, imgPath);
		break;
	}
	case Operator_Type_Camera_GetImageAndSaveRecord2:
	{
		CtrlError iRet;
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetImageAndSave2";

		QVariant tTeachData;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, tTeachData);
		ExecuteData.Datas.insert("Param", tTeachData);
		QString dateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss-zzz");
		QFileInfo fileInfo(op->param3);
		if (!fileInfo.exists())
		{
			QDir dir;
			dir.mkdir(op->param3);
			if (!dir.mkdir(op->param3))
			{
				emit SendProcMsg("dir not exist!");
				return FileNotExist;
			}
		}

		//获取SN
		QString curDayDir = QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss_zzz");
		QString SN;
		//参数五是 记录的 SN key值
		iRet = theFlageManager.GetTempStringByKey(op->param5, SN);
		if (iRet != 0)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}

		//图片路径
		QDir dir;
		dir.mkpath(op->param3 + "/" + SN);
		QString imgPath = op->param3 + "/" + SN + "/" + dateTime + "_"+op->param4 + ".bmp";
		ExecuteData.Datas.insert("Path", imgPath);
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}


		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		//存储图片路径  面号 对应的图片路径
		theFlageManager.SetTempStringByKey(op->param4, imgPath);
		break;
	}
	case Operator_Type_PanaMotor_MovePos: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		QVariant pos;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, pos);
		ExecuteData.Datas.insert("Pos", pos.toInt());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	//Operator_Type_PanaMotor_MoveOffset
	case Operator_Type_PanaMotor_MoveOffset:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetPos";

		//QVariant pos;
		//TeachManager::GetInstance().GetTeachData(op->station, op->postion, pos);
		//ExecuteData.Datas.insert("Pos", pos.toInt());

		//当前点位获取
		CtrlError  iRet;
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		int cur_pos = ResultData.Datas["Pos"].toInt();

		int offset = op->param1.toInt();
		ExecuteData.Datas.clear();
		ExecuteData.OperatorName = "MovePos";
		//Pos	int
		ExecuteData.Datas.insert("Pos", cur_pos + offset);
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	//Operator_Type_PanaMotor_ComparePos
	case Operator_Type_PanaMotor_ComparePos:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetPos";
		QString flagName = op->postion;
		//当前点位获取
		CtrlError  iRet;
		try
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		}
		catch (...)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		int cur_pos = ResultData.Datas["Pos"].toInt();

		//比较位置获取
		QVariant comPare_pos;
		TeachManager::GetInstance().GetTeachData(op->station, op->param1, comPare_pos);
		//范围
		int range = op->param2.toInt();

		if (qAbs(cur_pos - comPare_pos.toInt())<op->param2.toInt()&&cur_pos<comPare_pos.toInt())
		{
			iRet = theFlageManager.SetFlagValueByName(op->postion, op->param3.toInt());
		}
		else
		{
			iRet = theFlageManager.SetFlagValueByName(op->postion, op->param4.toInt());
		}

		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_Stop: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_GetPos: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_GetStatus: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_SetEnable: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		if (op->param1 == "1")
			ExecuteData.Datas.insert("Enable", true);
		else 
			ExecuteData.Datas.insert("Enable", false);

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_SetVel:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		ExecuteData.Datas.insert("Vel", op->param1.toInt());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_WaitArrive: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//QStringList keys = op->param1.split(",");
		//QStringList values = op->param2.split(",");

		QVariant pos;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, pos);
		ExecuteData.Datas.insert("Pos", pos.toInt());
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_WaitStop:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//QStringList keys = op->param1.split(",");
		//QStringList values = op->param2.split(",");

		QVariant pos;
		TeachManager::GetInstance().GetTeachData(op->station, op->postion, pos);
		ExecuteData.Datas.insert("Pos", pos.toInt());
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_PanaMotor_Reset: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_IOControl_SetOutPut: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());


		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			if (i ==(keys.count()-1))
			{
				if (values[i] == "0")
					ExecuteData.Datas[keys[i]] = false; 
				else
					ExecuteData.Datas[keys[i]] = true;
				continue;
			}
			ExecuteData.Datas[keys[i]] = values[i];
		}

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		emit SendProcMsg(QString("%1;%2").arg(op->Identification).arg(op->oprtType));
		break;
	}
	case Operator_Type_IOControl_SetOutPuts:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_IOControl_GetInPut: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_IOControl_GetInPutJumpStep:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetInPut";
		CtrlError  iRet;
		op->param5.toInt();
		int time_diff;
		if (op->param5 == "" || op->param5 == "0")
			time_diff = 0;
		else
			time_diff = op->param5.toInt();

		QTime s_time = QTime::currentTime();
		ExecuteData.Datas[op->param1] = op->param2.toInt();
		while (true)
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (!ResultData.Datas["Value"].canConvert<bool>())
			{
				emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("数据类型异常"));
				return ParamTypeError;
			}
			else
			{
				bool bValue = ResultData.Datas["Value"].toBool();
				QTime c_time = QTime::currentTime();
				int elapsed = s_time.msecsTo(c_time);
				if (elapsed > time_diff)
				{
					//emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("time out"));
					theFlageManager.SetFlagValueByName(op->postion, 0);
					return NoError;
				}
				if (op->param3 == "0"&&bValue == false)
				{
					theFlageManager.SetFlagValueByName(op->postion, 1);
					return NoError;
				}
				else if (op->param3 != "0"&&bValue == true)
				{
					theFlageManager.SetFlagValueByName(op->postion, 1);
					return NoError;
				}
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
			QThread::msleep(10);
		}
		bool bValue = ResultData.Datas["Value"].toBool();
		if (bValue)
			m_step_index = op->param3.toInt() - 1;
		else
			m_step_index = op->param4.toInt() - 1;
		break;
	}
	case Operator_Type_IOControl_GetInPutAndSetFlag:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetInPut";
		CtrlError  iRet;
		op->param5.toInt();
		int time_diff;
		if (op->param5 == "" || op->param5 == "0")
			time_diff = 0;
		else
			time_diff = op->param5.toInt();

		QTime s_time = QTime::currentTime();
		ExecuteData.Datas[op->param1] = op->param2.toInt();
		while (true)
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (!ResultData.Datas["Value"].canConvert<bool>())
			{
				emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("数据类型异常"));
				return ParamTypeError;
			}
			else
			{
				bool bValue = ResultData.Datas["Value"].toBool();
				QTime c_time = QTime::currentTime();
				int elapsed = s_time.msecsTo(c_time);
				if (elapsed > time_diff)
				{
					//emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("time out"));
					theFlageManager.SetFlagValueByName(op->postion, 0);
					return NoError;
				}
				if (op->param3 == "0"&&bValue == false)
				{
					theFlageManager.SetFlagValueByName(op->postion,1);
					return NoError;
				}
				else if (op->param3 != "0"&&bValue == true)
				{
					theFlageManager.SetFlagValueByName(op->postion, 1);
					return NoError;
				}
			}
			QThread::msleep(10);
		}
		break;
	}
	case Operator_Type_IOControl_GetInPuts:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_IOControl_GetOutPut:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_IOControl_GetOutPuts: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_IOControl_GetIONames:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case  Operator_Type_IOControl_WatiInput:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetInPut";
		op->param5.toInt();
		int time_diff;
		if (op->param5 == "" || op->param5 == "0")
			time_diff = 0;
		else 
			time_diff = op->param5.toInt();

		QTime s_time = QTime::currentTime();
		ExecuteData.Datas[op->param1] = op->param2.toInt();
		while (true)
		{
			
			CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);

			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (!ResultData.Datas["Value"].canConvert<bool>())
			{
				emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("数据类型异常"));
				return false;
			}
			else
			{
				bool bValue = ResultData.Datas["Value"].toBool();
				QTime c_time = QTime::currentTime();
				int elapsed = s_time.msecsTo(c_time);

				if (elapsed > time_diff&& time_diff!=0)
				{
					emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("time out"));
					return TimeOutError;
				}
				if (op->param3=="0"&&bValue==false)
				{
					return NoError;
				}
				else if (op->param3!="0"&&bValue== true)
				{
					return NoError;
				}
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
			QThread::msleep(10);
		}
		break;
	}
	case  Operator_Type_IOControl_ContinueCheck:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetInPut";
		CtrlError  iRet;
		op->param5.toInt();
		int time_diff;
		if (op->param5 == "" || op->param5 == "0")
			time_diff = 0;
		else
			time_diff = op->param5.toInt();

		if (time_diff==0)
		{
			emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("超时时间不允许设置成为 0 "));
			return ParamValueError;
		}

		QTime s_time = QTime::currentTime();
		ExecuteData.Datas[op->param1] = op->param2.toInt();
		while (true)
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (!ResultData.Datas["Value"].canConvert<bool>())
			{
				emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("数据类型异常"));
				return ParamTypeError;
			}
			else
			{
				bool bValue = ResultData.Datas["Value"].toBool();
				QTime c_time = QTime::currentTime();
				int elapsed = s_time.msecsTo(c_time);

				if (elapsed > time_diff&& time_diff != 0)
				{
					return NoError;
				}
				if (op->param3 == "0"&&bValue == false)
				{
					if (op->postion.size()>2)
						msgBox::show("Warning", op->postion, 3);

					if (op->param4.toInt()>0)
					{
						m_step_index = op->param4.toInt() - 1;
						return true;
					}

					return false;
				}
				else if (op->param3 != "0"&&bValue == true)
				{
					if (op->postion.size() > 2)
						msgBox::show("Warning", op->postion, 3);
					if (op->param4.toInt() > 0)
					{
						m_step_index = op->param4.toInt() - 1;
						return NoError;
					}
					return TimeOutError;
				}
			}
			QThread::msleep(10);
		}
		break;
	}
	case Operator_Type_Axis_Check_Pos:
	{
		//单轴到位检测 
		//CStationMotion CurSt;
		//if (!CParentProcess::FindOperatorStatin(op->station, theDbManager.GetStation(), CurSt))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move Not Find Station %1").arg(op->station));
		//	return false;
		//}
		//PointStation targetPos;
		//if (!CParentProcess::FindStaionContainPos(op->postion, CurSt, targetPos))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move not find posName:  %1").arg(op->postion));
		//	return false;
		//}
		////theMachineCtrl.MotionCheckMulityAxisOnTargetPos(CurSt.m_AxisNameList, targetPos.pValueList);
		////根据descption2 序号进行检测 轴是否到位
		//bool ret = theMachineCtrl.MotionCheckAxisOnTargetPos(CurSt.m_AxisNameList.at(op->descption2.toInt()), targetPos.pValueList.at(op->descption2.toInt()));
		//if (!ret)
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Check_Pos not in pos:  %1").arg(op->postion));
		//	return false;
		//}
		break;
	}
	case Operator_Type_Wait_Flag:
	{
		QString flag_name = op->postion;
		int value = op->param1.toInt();
		int timeOut = op->param5.toInt();
		QTime startTime = QTime::currentTime();
		while (true)
		{
			QThread::msleep(10);
			QTime stopTime = QTime::currentTime();
			int elapsed = startTime.msecsTo(stopTime);
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
			if (elapsed > timeOut&&timeOut!=0)
			{
				emit SendProcMsg(QString("tiemOut: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return TimeOutError;
			}
			int realVal = -1;
			CtrlError iRet = theFlageManager.GetFlagValueByName(flag_name, realVal);
			if (0 != iRet)
			{
				emit SendProcMsg(QString("not find flag %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return iRet;
			}
				
			if (value == realVal)
				break;
		}
		break;
	}
	case Operator_Type_Write_Flag:
	{
		QString flag_name = op->postion;
		int value = op->param1.toInt();
		CtrlError iRet = theFlageManager.SetFlagValueByName(flag_name, value);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		break;
	}
	case Operator_Type_WriteTemp_Flag:
	{
		QString flag_name = op->postion;
		//int value = op->param1.toInt();
		QString tempData;
		CtrlError iRet  = theFlageManager.GetTempStringByKey(op->param1,tempData);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		iRet = theFlageManager.SetFlagValueByName(flag_name, tempData.toInt());
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		break;
	}
	case Operator_Type_SelfAddFlag:
	{
		QString flag_name = op->postion;
		CtrlError iRet;
		int value;
		iRet = theFlageManager.GetFlagValueByName(flag_name, value);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		iRet = theFlageManager.SetFlagValueByName(flag_name, ++value);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		break;
	}
	case Operator_Type_SelfMinuFlag:
	{
		QString flag_name = op->postion;
		CtrlError iRet;
		int value;
		iRet = theFlageManager.GetFlagValueByName(flag_name, value);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		iRet = theFlageManager.SetFlagValueByName(flag_name, --value);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		break;
	}

	case Operator_Type_CreateSN:
	{
		QString SN = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss-zzz");
		theFlageManager.SetTempStringByKey(op->param1, SN);
		break;
	}
	//case Operator_Type_DeleteSN:
	//{
	//	theFlageManager.SetTempStringByKey(op->param1, "");
	//	break;
	//}
	case Operator_Type_GetSN:
	{
		QString SN;
		CtrlError  iRet  =  theFlageManager.GetTempStringByKey(op->param1, SN);
		if (iRet !=0)
		{
			emit SendProcMsg(QString("Error: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		theFlageManager.SetTempStringByKey(op->param2, SN);

		break;
	}
	case Operator_Type_CreateStartTime:
	{
		QTime time = QTime::currentTime();
		QString timestr = time.toString(QString::fromLatin1("HH:mm:ss.zzz"));
		theFlageManager.SetTempStringByKey(op->param1, timestr);
		break;
	}
	case Operator_Type_ElapsedFromTime:
	{
		QString lastTimeStr;
		CtrlError iRet = theFlageManager.GetTempStringByKey(op->param2, lastTimeStr);
		if (iRet != NoError)return iRet;
		QTime lastTime = QTime::fromString(lastTimeStr);
		//int diffTime = QTime::currentTime().msecsTo(lastTime);
		int diffTime = lastTime.msecsTo(QTime::currentTime());
		theFlageManager.SetTempStringByKey(op->param1, QString::number(qAbs(diffTime)));

		break;
	}
	case Operator_Type_ElapsedFromTwoTime:
	{
		QString lastTimeStr1, lastTimeStr2;
		CtrlError iRet = theFlageManager.GetTempStringByKey(op->param2, lastTimeStr1);
		if (iRet != NoError)return iRet;
		iRet =  theFlageManager.GetTempStringByKey(op->param3, lastTimeStr2);
		if (iRet != NoError)return iRet;

		QTime lastTime1 = QTime::fromString(lastTimeStr1);
		QTime lastTime2 = QTime::fromString(lastTimeStr2);
		int diffTime = lastTime1.msecsTo(lastTime2);
		theFlageManager.SetTempStringByKey(op->param1, QString::number(qAbs(diffTime) ));
		break;
	}
	case Operator_Type_WriteTempString:
	{
		theFlageManager.SetTempStringByKey(op->param1,op->param2);
		break;
	}
	case  Operator_Type_Flag_Jump:
	{
		QString flag_name = op->postion;
		int p1 = op->param1.toInt();
		int p2 = op->param2.toInt();
		int p3 = op->param3.toInt();
		int p4 = op->param4.toInt();
		int p5 = op->param5.toInt();
		int realVal = -1;
		CtrlError iRet = theFlageManager.GetFlagValueByName(flag_name, realVal);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("not find flag %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		//if (realVal == p1) m_step_index = p1 - 1;
		//else if (realVal == p2) m_step_index = p2 - 1;
		//else if (realVal == p3) m_step_index = p3 - 1;
		//else if (realVal == p4) m_step_index = p4 - 1;
		//else if (realVal == p5) m_step_index = p5 - 1;
		//else return false;

		if (realVal == 0)
		{
			m_step_index = p2 - 1;
		}
		else if (realVal == 1)
		{
			m_step_index = p1 - 1;
		}
		else
		{
			emit SendProcMsg(QString("未知返回 %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return false;
		}
		emit SendProcMsg(QString("操作类型-->%1 标志位名称%2--> 值->%3").arg(op->oprtType).arg(flag_name).arg(realVal));
		break;
	}
	case  Operator_Type_SwitchFlagJump:
	{
		//更具标志位多重跳转
		QString flag_name = op->postion;
		QStringList p1List = op->param1.split(",");//值 
		QStringList p2List = op->param2.split(",");//步骤号
		if (p1List.count() != p2List.count())
			return ParamLengthNotPatch;

		int realVal = -1;
		CtrlError iRet = theFlageManager.GetFlagValueByName(flag_name, realVal);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("not find flag %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
			return iRet;
		}
		bool bSuccesc = false;

		for (int i = 0;i<p1List.size();i++)
		{
			int value1 = p1List.at(i).toInt();
			if (value1 == realVal)
			{
				m_step_index = p2List.at(i).toInt() - 1;
				bSuccesc = true;
				break;
			}
		}
		if (!bSuccesc)
		{
			emit SendProcMsg(QString("未找到对应的标志位值 %1--> %2-->%3 --> %4:%5").arg(m_Proc_Name).arg(op->station).arg(op->oprtType)
				.arg(flag_name).arg(QString::number(realVal)));
			return FlagNameNotFind;
		}
		emit SendProcMsg(QString("操作类型-->%1 标志位名称%2--> 值->%3").arg(op->oprtType).arg(flag_name).arg(realVal));
		break;
	}
	case Operator_Type_MulityFlagLogicalAndJump:
	{
		QStringList flagList = op->param1.split(",");
		int tempValue = 1;
		foreach(QString flagName, flagList)
		{
			int value;
			CtrlError iRet = theFlageManager.GetFlagValueByName(flagName,value);
			if (NoError != iRet)
				return iRet;
			tempValue = tempValue & value;
		}
		if (tempValue == 1)
			m_step_index = op->param2.toInt() - 1;
		else if (tempValue == 0)
			m_step_index = op->param3.toInt() - 1;
		break;
	}
	case Operator_Type_MulityFlagLogicalORJump:
	{
		QStringList flagList = op->param1.split(",");
		int tempValue = 1;
		foreach(QString flagName, flagList)
		{
			int value;
			CtrlError iRet = theFlageManager.GetFlagValueByName(flagName, value);
			if (NoError != iRet)
				return iRet;
			tempValue = tempValue | value;
		}
		if (tempValue == 1)
			m_step_index = op->param2.toInt() - 1;
		else if (tempValue == 0)
			m_step_index = op->param3.toInt() - 1;
		break;
	}
	case Operator_Type_WaitAndSet_Flag:
	{
		QString flag_name = op->postion;
		int p1 = op->param1.toInt();
		int p2 = op->param2.toInt();
		int p3 = op->param3.toInt();
		int p4 = op->param4.toInt();
		int p5 = op->param5.toInt();
		CtrlError iRet = theFlageManager.WaitAndSetFlagValueByName(flag_name,p1,p2);
		return iRet;
		break;
	}
	case Operator_Type_Clear_Flag:
	{
		theFlageManager.ClearAllFlag();
		break;
	}
	case Operator_Type_Station_Check_Pos:
	{
		//CStationMotion CurSt;
		//if (!CParentProcess::FindOperatorStatin(op->station, theDbManager.GetStation(), CurSt))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move Not Find Station %1").arg(op->station));
		//	return false;
		//}
		////
		////theMachineCtrl.MotionMove(op->descption1, targetPos.pValueList.at(op->descption2.toInt()));
		//PointStation targetPos;
		//if (!CParentProcess::FindStaionContainPos(op->postion, CurSt, targetPos))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move not find posName:  %1").arg(op->postion));
		//	return false;
		//}
		//theMachineCtrl.MotionCheckMulityAxisOnTargetPos(CurSt.m_AxisNameList, targetPos.pValueList);
		break;
	}
	case Operator_Type_AlgoManager_SendCmd: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet;
		QString SN;
		iRet = theFlageManager.GetTempStringByKey(op->param3, SN);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("SN",SN);
		//Station	QString	工站名（和初始化对应） 通用 里面写
		//SN	QString	DUT编号
		//Surface	QString	DUT面号
		//Data	QMap<QString, QString>	发送指令的数据内容  ?
		ExecuteData.Datas.insert("Surface", op->param4);

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_SendCmdPOS: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet;
		QString SN;
		iRet = theFlageManager.GetTempStringByKey(op->param3, SN);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("SN", SN);
		ExecuteData.Datas.insert("Surface", op->param4);

		QString ImagePath;
		iRet = theFlageManager.GetTempStringByKey(op->param5, ImagePath);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("ImagePath", ImagePath);

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_SendCmdSRC: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet;
		QString SN;
		iRet = theFlageManager.GetTempStringByKey(op->param3, SN);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("SN", SN);
		//Station	QString	工站名（和初始化对应）
		//SN	QString	DUT编号
		//Surface	QString	DUT面号
		//ImagePath	QString	图片路径
		//Options	QMap<QString, QString>	发送指令的额外内容（可选）
		ExecuteData.Datas.insert("Surface", op->param4);

		QString ImagePath;
		iRet = theFlageManager.GetTempStringByKey(op->param5, ImagePath);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("ImagePath", ImagePath);

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_SendCmdMOON:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		CtrlError  iRet;
		QString SN;
		iRet = theFlageManager.GetTempStringByKey(op->param3, SN);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("SN", SN);
		//Station	QString	工站名（和初始化对应）
		//SN	QString	DUT编号
		//Surface	QString	DUT面号
		//ImagePath	QString	图片路径
		//Options	QMap<QString, QString>	发送指令的额外内容（可选）
		ExecuteData.Datas.insert("Surface", op->param4);

		QString ImagePath;
		iRet = theFlageManager.GetTempStringByKey(op->param4, ImagePath);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas.insert("ImagePath", ImagePath);

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_SendCmdIMS: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//通用键值拼接
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//抓取SN
		QString record_sn;
		theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		//抓取surface
		//QString surface;
		//theFlageManager.GetTempStringByKey(op->param4, surface);
		ExecuteData.Datas["Surface"] = op->param4; 

		QString ImagePathValue;
		QStringList pathList = op->param5.split(",");
		
		for (int i = 0;i<pathList.count();i++)
		{
			QString path;
			theFlageManager.GetTempStringByKey(pathList[i], path);
			if (path.isEmpty())
				continue;
			if (i == pathList.count()-1)
				ImagePathValue = ImagePathValue + path;
			else 
				ImagePathValue = ImagePathValue+ path +",";
		}

		//抓取ImagePath
		//QString imagePath;
		//theFlageManager.GetTempStringByKey(op->param5, imagePath);
		//ExecuteData.Datas["ImagePath"] = imagePath;
		ExecuteData.Datas["ImagePath"] = ImagePathValue;
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_SendCmdIMSMulity:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//通用键值拼接
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}

		//ExecuteData.Datas.insert("Station", op->station);
		//ExecuteData.Datas["Station"] = op->station;
		//抓取SN
		QString record_sn;
		theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		//抓取surface
		//QString surface;
		//theFlageManager.GetTempStringByKey(op->param4, surface);
		ExecuteData.Datas["Surface"] = op->param4.split(",");
		QStringList pathList;
		QStringList nameList = op->param5.split(",");
		
		for (int i = 0; i < nameList.count(); i++)
		{
			QString path;
			theFlageManager.GetTempStringByKey(nameList[i], path);
			pathList.append(path);
		}


		ExecuteData.Datas["ImagePath"] = pathList;
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}

	case Operator_Type_AlgoManager_SendCmdIMSMulityADFOnePic:
	{
		HardwareCommData ExecuteData, ResultData;
		CtrlError iRet;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		//ExecuteData.OperatorName = "SendCmdIMS";
		ExecuteData.OperatorName = "SendCmdFree";
		//通用键值拼接
		QStringList keys = op->param1.split("$");
		QStringList values = op->param2.split("$");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//临时变量添加
		QStringList tempKeyList = op->param3.split("$");
		for (int i = 0; i < tempKeyList.count(); ++i)
		{
			QString value;
			iRet = theFlageManager.GetTempStringByKey(tempKeyList[i], value);
			if (NoError != iRet)
			{
				ErrorPackage1(iRet, op);
				return  iRet;
			}
			//后缀如有SN标志样式变量统一使用SN做key
			if (tempKeyList[i].contains("SN"))
			{
				ExecuteData.Datas["SN"] = value;
			}
			else
				ExecuteData.Datas[tempKeyList[i]] = value;
		}
		//参数4 标志位
		if (!op->param4.isEmpty())
		{
			if (op->param4 == "0")
			{

			}
			else
			{
				int flagValue = 0;
				iRet = theFlageManager.GetFlagValueByName(op->param4, flagValue);
				if (NoError != iRet)
				{
					ErrorPackage1(iRet, op);
					return  iRet;
				}
				ExecuteData.Datas["Index"] = QString::number(flagValue);
			}
		}
		//图片路径
		QString path;
		iRet = theFlageManager.GetTempStringByKey(op->param5, path);
		if (NoError != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas[op->param5] = path;
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_SetBarCode: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet;
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//抓取SN
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}


		//SN
		//BarCode ?

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_ChcekFinish: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet;
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//抓取SN
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas["SN"] = record_sn;


		//
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		int SendCount = ResultData.Datas["Send"].toInt();
		int RecvCount = ResultData.Datas["Recv"].toInt();
		bool bFinsh = ResultData.Datas["isFinish"].toBool();
		if (!bFinsh)
		{
			QString msg = QString("Identification:%1;SendCount:%2;RecvCount:%3; not Finsh").arg(op->Identification).arg(SendCount).arg(RecvCount);
			emit SendProcMsg(msg);
			return UndefineError;
		}
		break;
	}
	case Operator_Type_AlgoManager_WaitFinish:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "ChcekFinish";
		CtrlError  iRet;
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//抓取SN
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas["SN"] = record_sn;
		int time_diff;
		if (op->param5 == "" || op->param5 == "0")
			time_diff = 0;
		else
			time_diff = op->param5.toInt();

		QTime s_time = QTime::currentTime();
		//
		while (true)
		{
			iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			int SendCount = ResultData.Datas["Send"].toInt();
			int RecvCount = ResultData.Datas["Recv"].toInt();
			bool bFinsh = ResultData.Datas["isFinish"].toBool();
			QTime c_time = QTime::currentTime();
			if (s_time.msecsTo(c_time)>time_diff)
			{
				emit SendProcMsg(QString("Indentify:%1;errorMsg:%2").arg(op->Identification).arg("time out"));
				return TimeOutError;
			}
			else
			{
				if (bFinsh)
					return NoError;
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
			//QThread::msleep(10);
			QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
		}
		break;
	}
	case Operator_Type_AlgoManager_WaitResult:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		QString record_sn;
		theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;

		ExecuteData.Datas["Surface"] = op->param4;
		ExecuteData.Datas["TimeOut"] = op->param5.toInt();

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		AlgoResult  result = ResultData.Datas["Result"].value<AlgoResult>();
		if (result.Result != "PASS")
			//if (result.Result == "ERROR")
		{
				emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
				return ResultNotPass;
		}


		//结果存储
		QString surface_result_key = QString("%1_Result").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_result_key, result.Result);

		//记录点位
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);


		//记录结果
		QString surface_ResultPath_key = QString("%1_ResultPath").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_ResultPath_key, result.TargetPoint);
		break;
	}
	case Operator_Type_AlgoManager_WaitResultSetFlag:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "WaitResult";
		CtrlError  iRet;
		//QStringList keys = op->param1.split(",");
		//QStringList values = op->param2.split(",");
		//for (int i = 0; i < keys.count(); ++i)
		//{
		//	ExecuteData.Datas[keys[i]] = values[i];
		//}



		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		ExecuteData.Datas["SN"] = record_sn;
		ExecuteData.Datas["Surface"] = op->param4;
		ExecuteData.Datas["TimeOut"] = op->param5.toInt();

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		AlgoResult  result = ResultData.Datas["Result"].value<AlgoResult>();
		if (result.Result == "ERROR")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
			return false;
		}
		else if (result.Result == "FAILED")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
			iRet = theFlageManager.SetFlagValueByName(op->postion, op->param2.toInt());
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			break;
		}
		else if (result.Result != "PASS")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
			return ResultNotPass;
		}

		iRet = theFlageManager.SetFlagValueByName(op->postion, op->param1.toInt());
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		//结果存储
		QString surface_result_key = QString("%1_Result").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_result_key, result.Result);

		//记录点位
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);


		//记录结果
		QString surface_ResultPath_key = QString("%1_ResultPath").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_ResultPath_key, result.TargetPoint);
		break;

	}
	case Operator_Type_AlgoManager_WaitResultSetFlagADF:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "WaitResult";
		CtrlError  iRet;
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		ExecuteData.Datas["SN"] = record_sn;
		ExecuteData.Datas["Surface"] = op->param4;
		ExecuteData.Datas["TimeOut"] = op->param5.toInt();

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		AlgoResult  result = ResultData.Datas["Result"].value<AlgoResult>();
		if (result.Result == "ERROR")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2; 失败消息:%3").arg(op->Identification).arg(result.Result).arg(result.msg));
			iRet = theFlageManager.SetFlagValueByName(op->postion, op->param2.toInt());
			if (0 != iRet)
			{
				ErrorPackage1(iRet, op);
				return  iRet;
			}
			break;
			//return ResultError;
		}
		else if (result.Result == "FAIL")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
			iRet = theFlageManager.SetFlagValueByName(op->postion, op->param2.toInt());
			if (0 != iRet)
			{
				ErrorPackage1(iRet, op);
				return  iRet;
			}
			break;
		}
		else if (result.Result != "PASS")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
			return ResultNotPass;
		}
		else if (result.Result == "PASS")
		{
			iRet = theFlageManager.SetFlagValueByName(op->postion, op->param1.toInt());
			if (0 != iRet)
			{
				ErrorPackage1(iRet, op);
				return  iRet;
			}
		}
		//iRet = theFlageManager.SetFlagValueByName(op->postion, op->param1.toInt());
		//结果pass
		//iRet = theFlageManager.SetFlagValueByName(op->postion, op->param1.toInt());
		//iRet = theFlageManager.SetFlagValueByName(op->postion, op->param2.toInt()); //结果失败
		//if (0 != iRet)
		//{
		//	QString GetErrStr(iRet);
		//	emit SendProcMsg(
		//		QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
		//		arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
		//		.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
		//	return  iRet;
		//}
		//目标数据
		result.TargetPoint;
		//用SN 当KEY——POS
		QMap<QString, QVariant> dispData;
		QStringList resultPaths = result.ResultPath.split(",");

		//for (int i = 0;i<resultPaths.count();i++)
		//{
		//	Result_Data data;
		//	data.result = result.Result;
		//	data.reslutImgPath = resultPaths[i];
		//	dispData.insert(QString::number(i), QVariant::fromValue(data));
		//}
		//emit SignalSendImg(dispData);

		//结果存储
		QString surface_result_key = QString("%1_Result").arg(op->param4);
		//if (result.Result == "ERROR")
		//	theFlageManager.SetTempStringByKey(surface_result_key, "PASS");
		//else
		theFlageManager.SetTempStringByKey(surface_result_key, result.Result);

		//记录点位
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);

		//if (op->param4 == "P1U1,P1D1")
		//{
		//	theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	if (result.Result == "ERROR")
		//	{
		//		theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	}
		//	else
		//	{
		//		//theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);
		//	}
		//}
		//else if (op->param4 == "P1U1,P2D2")
		//{
		//	theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	if (result.Result == "ERROR")
		//	{
		//		theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	}
		//	else
		//	{
		//		//theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);
		//	}
		//}
		//else if (op->param4 == "P1U1,P3D3")
		//{
		//	theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	if (result.Result == "ERROR")
		//	{
		//		theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	}
		//	else
		//	{
		//		//theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);
		//	}
		//}
		//else if (op->param4 == "P1U1,P4D4")
		//{
		//	theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	if (result.Result == "ERROR")
		//	{
		//		theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//	}
		//	else
		//	{
		//		//theFlageManager.SetTempStringByKey(surface_point_key, "-18.281,305.626,-58.065");
		//		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);
		//	}
		//}

		//记录结果
		QString surface_ResultPath_key = QString("%1_ResultPath").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_ResultPath_key, result.ResultPath);
		//获取图像数据
		break;

	}
	case Operator_Type_AlgoManager_WaitResultSetFlagByResultInfoMulity:
	{
		QStringList resList = op->param1.split(",");
		QStringList valueList = op->param2.split(",");
		if (resList.count() != valueList.count())
			return ParamLengthNotPatch;

		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "WaitResult";
		CtrlError  iRet;
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		ExecuteData.Datas["SN"] = record_sn;
		ExecuteData.Datas["Surface"] = op->param4;
		ExecuteData.Datas["TimeOut"] = op->param5.toInt();

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		AlgoResult  result = ResultData.Datas["Result"].value<AlgoResult>();
		if (!resList.contains(result.Result))
			return ParamValueError;
		for (int i=0;i<resList.count();i++)
		{
			if (result.Result == resList[i])
			{
				iRet = theFlageManager.SetFlagValueByName(op->postion, valueList[i].toInt());
				if (0 != iRet)
				{
					QString GetErrStr(iRet);
					emit SendProcMsg(
						QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
						arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
						.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
					return  ResultFailed;
				}
				emit SendProcMsg(QString("identify:%1;AlgoResult:%2; 失败消息:%3").arg(op->Identification).arg(result.Result).arg(result.msg));
				break;
			}
		}
		//目标数据
		result.TargetPoint;
		//用SN 当KEY——POS
		QMap<QString, QVariant> dispData;
		QStringList resultPaths = result.ResultPath.split(",");

		for (int i = 0; i < resultPaths.count(); i++)
		{
			Result_Data data;
			data.result = result.Result;
			data.reslutImgPath = resultPaths[i];
			dispData.insert(QString::number(i), QVariant::fromValue(data));
		}
		emit SignalSendImg(dispData);

		//结果存储
		QString surface_result_key = QString("%1_Result").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_result_key, result.Result);
		//记录点位
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);
		//记录结果
		QString surface_ResultPath_key = QString("%1_ResultPath").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_ResultPath_key, result.ResultPath);
		//获取图像数据
		break;
	}
	case Operator_Type_AlgoManager_GetResult: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}

		//抓取SN
		QString record_sn;
		theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		////抓取surface
		//QString surface;
		//theFlageManager.GetTempStringByKey(op->param4, surface);
		ExecuteData.Datas["Surface"] = op->param4;
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		//QString Surface;
		//QString Result;
		//QString ResultPath;
		//QString TargetPoint;

		AlgoResult  result = ResultData.Datas["Result"].value<AlgoResult>();
		if (result.Result != "PASS")
		//if (result.Result == "ERROR")
		{
			emit SendProcMsg(QString("identify:%1 ;SmartIMS_GetResult 结果失败:AlgoResult:%2").arg(op->Identification).arg(result.Result));
			return ResultNotPass;
		}


		//结果存储
		QString surface_result_key = QString("%1_Result").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_result_key, result.Result);

		//记录点位
		QString surface_point_key = QString("%1_Point").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_point_key, result.TargetPoint);


		//记录结果
		QString surface_ResultPath_key = QString("%1_ResultPath").arg(op->param4);
		theFlageManager.SetTempStringByKey(surface_ResultPath_key, result.TargetPoint);

		break;
	}
	case Operator_Type_AlgoManager_GetAllResult: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet;
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//抓取SN
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		QMap<QString, QVariant> dispData;
		QMap<QString, AlgoResult> allResult;
		//结果解析
		allResult = ResultData.Datas["Result"].value<QMap<QString, AlgoResult>>();
		foreach(QString keyStr, allResult.keys())
		{
			AlgoResult faceResult = allResult[keyStr];
			Result_Data data;
			data.result = faceResult.Result;
			data.reslutImgPath = faceResult.ResultPath;
			dispData.insert(keyStr, QVariant::fromValue(data));
		}
		emit SignalSendImg(dispData);
		break;
	}
	//
	case Operator_Type_AlgoManager_GetAllResultSetFlag:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "GetAllResult";
		CtrlError  iRet;
		//抓取SN
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		QMap<QString, AlgoResult> allResult;
		//结果解析
		allResult = ResultData.Datas["Results"].value<QMap<QString, AlgoResult>>();
		QMap<QString, QVariant> Datas;
		bool bAllResult = true;
		foreach(QString key,allResult.keys())
		{
			AlgoResult faceResult = allResult[key];
			if (bAllResult)
			{
				if (faceResult.Result != "PASS")
					bAllResult = false;
			}
			Result_Data data;
			data.result = faceResult.Result;
			data.reslutImgPath = faceResult.ResultPath;
			Datas.insert(key, QVariant::fromValue(data));
		}

		if (bAllResult)
			theFlageManager.SetFlagValueByName(op->postion, op->param1.toInt());
		else 
			theFlageManager.SetFlagValueByName(op->postion, op->param2.toInt());
		//页面刷新效果图
		emit SignalSendImg(Datas);
		break;
	}
	case Operator_Type_AlgoManager_Complete: 
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		CtrlError  iRet;
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			
			if (keys[i] == "Force")
			{
				ExecuteData.Datas[keys[i]] = values[i].toInt();
			}
			else
			{
				ExecuteData.Datas[keys[i]] = values[i];
			}
		}

		//抓取SN
		QString record_sn;
		iRet = theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_AlgoManager_LoadGolden:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//通用键值拼接  Station、GoldenPath
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//抓取SN
		QString record_sn;
		theFlageManager.GetTempStringByKey(op->param3, record_sn);
		ExecuteData.Datas["SN"] = record_sn;
		//抓取surface
		//QString surface;
		//theFlageManager.GetTempStringByKey(op->param4, surface);
		ExecuteData.Datas["Surface"] = op->param4;
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}

	case Operator_Type_Axis_OffSet_Move:
	{
		//将描述内容一当做 轴名称使用   将描述二当做 轴里面序号使用		将参数para1 做偏移量
		//等待轴停止
		//QString axisName = "";
		//CStationMotion CurSt;
		//if (!CParentProcess::FindOperatorStatin(op->station, theDbManager.GetStation(), CurSt))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move Not Find Station %1").arg(op->station));
		//	return false;
		//}
		////等待当前工站所有轴停止
		//CParentProcess::WaitStaionStop(CurSt);
		////单轴运动
		//PointStation targetPos;
		//if (!CParentProcess::FindStaionContainPos(op->postion, CurSt, targetPos))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move not find posName:  %1").arg(op->postion));
		//	return false;
		//}
		////将描述内容一当做 轴名称使用   将描述二当做 轴里面序号使用
		////theMachineCtrl.MotionMove(op->descption1, targetPos.pValueList.at(op->descption2.toInt()));
		////int MotionOffsetMove(QString axisName, double offset);
		//theMachineCtrl.MotionOffsetMove(op->descption1, op->param1.toDouble());
		////单轴运动
		break;
	}
	break;
	case Operator_Type_Axis_Wait_Stop:
	{
		//QString axisName = "";
		//CStationMotion CurSt;
		//if (!CParentProcess::FindOperatorStatin(op->station, theDbManager.GetStation(), CurSt))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move Not Find Station %1").arg(op->station));
		//	return false;
		//}
		//theMachineCtrl.WaitMotion(CurSt.m_AxisNameList.at(op->param1.toInt()));
		break;
	}
	case Operator_Type_Axis_Move:
	{
		////等待轴停止
		//QString axisName = "";
		//CStationMotion CurSt;
		//if (!CParentProcess::FindOperatorStatin(op->station, theDbManager.GetStation(), CurSt))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move Not Find Station %1").arg(op->station));
		//	return false;
		//}
		////等待当前工站所有轴停止
		//CParentProcess::WaitStaionStop(CurSt);
		////单轴运动
		//PointStation targetPos;
		//
		//if (!CParentProcess::FindStaionContainPos(op->postion, CurSt, targetPos))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Axis_Move not find posName:  %1").arg(op->postion));
		//	return false;
		//}
		//
		////将描述内容一当做 轴名称使用   将描述二当做 轴里面序号使用
		//theMachineCtrl.MotionMove(op->descption1, targetPos.pValueList.at(op->descption2.toInt()));
		//delete targetPos;
		//获取运动工站，获取轴名称，获取工站点位

		//单轴运动
		break;
	}

	case Operator_Type_WaitInput:
	{
		emit SendProcMsg(QString("Operator_Type_WaitInput %1->%2 wait time %3").arg(m_step_index).arg(op->param1).arg(op->param2));


		//为了防止卡页面读取历史值
		//if (op->param2.toInt()>0)
		//{
		//	QTime time;
		//	time.start();
		//	while (time.elapsed() < op->param2.toInt() * 1000)
		//	{
		//		QThread::msleep(20);
		//		foreach(InputParam inPut, theDbManager.GetInput())
		//		{
		//			if (inPut.name == op->postion)
		//			{
		//				if (op->param1.toInt() * 100 == inPut.value)
		//					return true;
		//			}
		//		}
		//	}
		//	return false;
		//}
		//else
		//{
		//	while (1)
		//	{
		//		QThread::msleep(20);
		//		foreach(InputParam inPut, theDbManager.GetInput())
		//		{
		//			//if (inPut.name == op->postion&&op->param1.toInt() * 100 == inPut.value)
		//			//	break;
		//			if (inPut.name == op->postion)
		//			{
		//				if (op->param1.toInt() * 100 == inPut.value)
		//					return true;
		//			}
		//		}
		//	}
		//}
		break;
	}
	case  Operator_Type_EPSON_Robot_SendCmdAndRecv:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//通用键值拼接  Station、GoldenPath
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		//通用变量 3 ，4 ，5 
		ExecuteData.Datas.insert("Cmd",op->param3);
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		ExecuteData.Datas.insert("MsgType", "clientCmd");

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			//QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		CmdData respond = ResultData.Datas["Respond"].value<CmdData>();
		//QString respond = ResultData.Datas["Respond"].toString();
		if (respond.resInfo !="success")
		{
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg(respond.resInfo));
		}
		qDebug() <<"Operator_Type_EPSON_Robot_SendCmdAndRecv->"<< respond.sData.toString() << endl;
		break;
	}
	case Operator_Type_EPSON_Robot_WaitRobotCmdSetFlag:
	{

		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = "WaitRobotCmd";
		//通用键值拼接  Station、GoldenPath
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			//QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		CmdData cmd = ResultData.Datas["ServerCmd"].value<CmdData>();
		QString cmdStr = cmd.sData.toString();
		//命令处理； 这里按照PLC逻辑是设置相机拍照 通过设置 flagName 来进行步骤跳转
		//通过命令设置标志位
		QString flagName = op->postion;
		QStringList cmdList = op->param1.split(",");//命令
		QStringList flagList = op->param2.split(",");//标志位
		//字符串链表包含 字符？
		cmdList.contains(cmdStr);
		bool find = false;
		for (int i = 0;i<cmdList.size();i++)
		{
			if (cmdList.at(i)==cmdStr)
			{
				theFlageManager.SetFlagValueByName(flagName, flagList.at(i).toInt());
				find = true;
				break;
			}
		}
		if (!find)
		{
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg("接受命令格式错误"));
			return ParamFormatError;
		}
		break;
	}
	case  Operator_Type_PLC_ReadPort:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//通用键值拼接  Station、GoldenPath
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		ExecuteData.Datas.insert("Port", op->param3.toInt());

		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}

		if (!ResultData.Datas.contains("Value"))
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return KeyNotFind;
		}
		break;
	}
	case  Operator_Type_PLC_WritePort:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//op->oprtType.split("_").last();
		//通用键值拼接  Station、GoldenPath
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		ExecuteData.Datas.insert("Port", op->param3.toInt());
		ExecuteData.Datas.insert("Value", op->param4.toInt());
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		qDebug() << ExecuteData.Station;
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case  Operator_Type_PLC_WaitPort:
	{
		HardwareCommData ExecuteData, ResultData;
		ExecuteData.Identfication = op->Identification.toInt();
		ExecuteData.Station = op->station;
		ExecuteData.OperatorName = *(--op->oprtType.split("_").end());
		//通用键值拼接  Station、GoldenPath
		QStringList keys = op->param1.split(",");
		QStringList values = op->param2.split(",");
		for (int i = 0; i < keys.count(); ++i)
		{
			ExecuteData.Datas[keys[i]] = values[i];
		}
		ExecuteData.Datas.insert("Port", op->param3.toInt());
		ExecuteData.Datas.insert("Value", op->param4.toInt());
		ExecuteData.Datas.insert("TimeOut", op->param5.toInt());
		CtrlError  iRet = HardwareManager::GetInstance()->Execute(ExecuteData, ResultData);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}

	case Operator_Type_ETH_PLC_Connect:
	{
		CtrlError  iRet = theModbusMoudle->ConnectETH(op->param1,op->param3.toInt(),op->param2.toInt());
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;	
	}
	case Operator_Type_ETH_PLC_Disconnect:
	{
		CtrlError  iRet = theModbusMoudle->DisconnectETH(op->param1.toInt());
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_ETH_PLC_ReadPortInt:
	{
		int value = 0;
		CtrlError  iRet = theModbusMoudle->ReadDataInt_ETH(REGI_H5U_D, op->param3.toInt(), value);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		theFlageManager.SetTempStringByKey (op->param4, QString::number(value));
		break;
	}
	case Operator_Type_ETH_PLC_ReadPortFloat: 
	{
		float value = 0;
		CtrlError  iRet = theModbusMoudle->ReadDataFloat_ETH(REGI_H5U_D, op->param3.toInt(), value);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		theFlageManager.SetTempStringByKey(op->param4, QString::number(value));
		break;
	}
	case Operator_Type_ETH_PLC_WritePortFloat:
	{
		float value = 0;
		CtrlError  iRet = theModbusMoudle->WriteDataFloat_ETH(REGI_H5U_D, op->param3.toInt(), op->param4.toFloat());
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}

	case Operator_Type_ETH_PLC_ReadPortFloatWriteToTempDataMulity:
	{
		QStringList portStrList = op->param2.split(",");
		QString tempStr;
		for (int i =0;i<portStrList.count();i++)
		{
			int  portIndex = portStrList[i].toInt();
			float value;
			CtrlError iRet = theModbusMoudle->ReadDataFloat_ETH(REGI_H5U_D, portIndex, value);
			if (iRet != NoError)
			{
				ErrorPackage1(iRet, op);
				return iRet;
			}
			if (i == 0)
			{
				tempStr = QString("%1").arg(value);
			}
			else
				tempStr = QString("%1,%2").arg(tempStr).arg(value);
		}
		theFlageManager.SetTempStringByKey(op->param1, tempStr);
		break;
	}
	case Operator_Type_ETH_PLC_ReadPortIntWriteToTempDataMulity:
	{
		QStringList portStrList = op->param2.split(",");
		QString tempStr;
		for (int i = 0;i < portStrList.count();i++)
		{
			int  portIndex = portStrList[i].toInt();
			int value;
			CtrlError iRet = theModbusMoudle->ReadDataInt_ETH(REGI_H5U_D, portIndex, value);
			if (iRet != NoError)
			{
				ErrorPackage1(iRet, op);
				return iRet;
			}
			if (i == 0)
			{
				tempStr = QString("%1").arg(value);
			}
			else
				tempStr = QString("%1,%2").arg(tempStr).arg(value);
		}
		theFlageManager.SetTempStringByKey(op->param1, tempStr);
		break;
	}
	case Operator_Type_ETH_PLC_WriteTempPosToPLC:
	{
		QString tempStr;
		CtrlError iRet = theFlageManager.GetTempStringByKey(op->param1,tempStr);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		QStringList dataList = tempStr.split(",");
		if (dataList.count() != 3)	return ParamLengthNotPatch;

		QStringList portList = op->param2.split(",");
		if (portList.count() != 3)	return ParamLengthNotPatch;
		//数据写入
		for (int i = 0;i<3;i++)
		{
			iRet = theModbusMoudle->WriteDataFloat_ETH(REGI_H5U_D, portList[i].toInt(), dataList[i].toFloat());
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				theFlageManager.SetFlagValueByName(op->postion, op->param4.toInt());
				return NoError;
				//return  iRet;
			}
		}
		theFlageManager.SetFlagValueByName(op->postion, op->param3.toInt());
		break;
	}
	case Operator_Type_ETH_PLC_WritePortInt:
	{
		//qint16 value = 0;
		//value = op->param4.toInt();
		CtrlError  iRet = theModbusMoudle->WriteDataInt_ETH(REGI_H5U_D, op->param3.toInt(), op->param4.toInt());
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_ETH_PLC_WriteDataCheck:
	{
		qint16 value = 0;
		CtrlError  iRet = theModbusMoudle->WriteDataCheckInt_ETH(REGI_H5U_D, op->param3.toInt(), op->param4.toUShort());
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		break;
	}
	case Operator_Type_ETH_PLC_WaitPortInt:
	{
		QTime t;
		t.start();
		while (true)
		{
			//QThread::msleep(10);
			QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
			int value = 0;
			CtrlError  iRet = theModbusMoudle->ReadDataInt_ETH(REGI_H5U_D, op->param3.toInt(), value);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (value == op->param4.toInt())
				return NoError;
			if (t.elapsed()>op->param5.toInt()&& op->param5.toInt()!=0)
				return TimeOutError;
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
		}
		break;
	}
	case Operator_Type_ETH_PLC_WaitPortFloat:
	{
		QTime t;
		t.start();
		while (true)
		{
			QThread::msleep(10);
			float value = 0;
			CtrlError  iRet = theModbusMoudle->ReadDataFloat_ETH(REGI_H5U_D, op->param3.toInt(), value);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (value == op->param4.toFloat())
				return NoError;
			if (t.elapsed() > op->param5.toInt() && op->param5.toInt() != 0)
				return TimeOutError;
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
		}
		break;
	}
	case Operator_Type_ETH_PLC_WritePortAndWaitTimesInt:
	{
		CtrlError  iRet = NoError;
		int times = 0;
		while (true)
		{
			if (times > op->param3.toInt())
				return TimeOutError;
			//写入数值
			iRet = theModbusMoudle->WriteDataInt_ETH(REGI_H5U_D, op->param1.toInt(), op->param2.toInt());
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			//读取数值 超时检测
			QTime t;
			t.start();
			while (true)
			{
				QThread::msleep(10);
				int value = 0;
				iRet = theModbusMoudle->ReadDataInt_ETH(REGI_H5U_D, op->param1.toInt(), value);
				if (0 != iRet)
				{
					QString GetErrStr(iRet);
					emit SendProcMsg(
						QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
						arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
						.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
					return  iRet;
				}
				if (value == op->param2.toInt())
					return NoError;
				if (t.elapsed() > op->param5.toInt() && op->param5.toInt() != 0)//超时跳出
					break;
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
			++times;
		}
		break;
	}
	case Operator_Type_ETH_PLC_WritePortAndWaitTimesFloat:
	{
		CtrlError  iRet = NoError;
		int times = 0;
		while (true)
		{
			if (times > op->param3.toInt())
				return TimeOutError;
			//写入数值
			iRet = theModbusMoudle->WriteDataFloat_ETH(REGI_H5U_D, op->param1.toInt(), op->param2.toInt());
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			//读取数值 超时检测
			QTime t;
			t.start();
			while (true)
			{
				QThread::msleep(10);
				float value = 0;
				iRet = theModbusMoudle->ReadDataFloat_ETH(REGI_H5U_D, op->param1.toInt(), value);
				if (0 != iRet)
				{
					QString GetErrStr(iRet);
					emit SendProcMsg(
						QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
						arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
						.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
					return  iRet;
				}
				if (value == op->param2.toInt())
					return NoError;
				if (t.elapsed() > op->param5.toInt() && op->param5.toInt() != 0)//超时跳出
					break;
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
			++times;
		}
		break;
	}
	case Operator_Type_ETH_PLC_CheckProtIntSetFlag:
	{
		//等待一个端口的值 设置flag
		QTime t;
		t.start();
		while (true)
		{
			QThread::msleep(10);
			int value = 0;
			CtrlError  iRet = theModbusMoudle->ReadDataInt_ETH(REGI_H5U_D, op->param1.toInt(), value);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (value == op->param2.toInt())
			{
				theFlageManager.SetFlagValueByName(op->postion,op->param3.toInt());
				return NoError;
			}
			if (t.elapsed() > op->param5.toInt() && op->param5.toInt() != 0)
			{
				theFlageManager.SetFlagValueByName(op->postion, op->param4.toInt());
				return NoError;
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
		}
		break;
	}
	case Operator_Type_ETH_PLC_CheckProtFloatSetFlag:
	{
		//等待一个端口的值 设置flag
		QTime t;
		t.start();
		while (true)
		{
			QThread::msleep(10);
			float value = 0;
			CtrlError  iRet = theModbusMoudle->ReadDataFloat_ETH(REGI_H5U_D, op->param1.toInt(), value);
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
					.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
			if (qAbs(value - op->param2.toFloat()) <0.001)//浮点数相等比较
			{
				theFlageManager.SetFlagValueByName(op->postion, op->param3.toInt());
				return NoError;
			}
			if (t.elapsed() > op->param5.toInt() && op->param5.toInt() != 0)
			{
				theFlageManager.SetFlagValueByName(op->postion, op->param4.toInt());
				return NoError;
			}
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				return ProcExtendExist;
			}
		}
		break;
	}
	case Operator_Type_ETH_PLC_WriteTempDataMulityInt:
	{
		op->param1;
		QString tempPos;
		CtrlError iRet = theFlageManager.GetTempStringByKey(op->param1, tempPos);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		QStringList portList = op->param2.split(",");
		QStringList posList = tempPos.split(",");
		if (posList.count() != portList.count())
			return ParamLengthNotPatch;
		for (int i = 0;i<portList.count();i++)
		{
			qint16 value = 0;
			CtrlError  iRet = theModbusMoudle->WriteDataInt_ETH(REGI_H5U_D, portList[i].toInt(), posList[i].toInt());
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName).arg(op->oprtType).arg((QString("%1")
						.arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
		}
		break;
	}
	case Operator_Type_ETH_PLC_WriteTempDataMulityFloat:
	{
		op->param1;
		QString tempPos;
		CtrlError iRet = theFlageManager.GetTempStringByKey(op->param1, tempPos);
		if (0 != iRet)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return  iRet;
		}
		QStringList portList = op->param2.split(",");
		QStringList posList = tempPos.split(",");
		if (posList.count() != portList.count())
			return ParamLengthNotPatch;
		for (int i = 0;i < portList.count();i++)
		{
			qint16 value = 0;
			CtrlError  iRet = theModbusMoudle->WriteDataFloat_ETH(REGI_H5U_D, portList[i].toInt(), posList[i].toFloat());
			if (0 != iRet)
			{
				QString GetErrStr(iRet);
				emit SendProcMsg(
					QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
					arg(GetProcName()).arg(m_step_index).arg(op->aliasName).arg(op->oprtType).arg((QString("%1")
						.arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
				return  iRet;
			}
		}
		break;
	}
	case Operator_Type_ClientCreateTcpClientConnectServer: 
	{
		CtrlError iRet = TheTcpManager->Client_CreateOnClientConnectSever(op->postion, op->param1, op->param2.toInt());
		if (0!= iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_CreateTcpClientConnectServer filed"));
			return iRet;
		}
		break;
	}
	case Operator_Type_ClientWaitClientReConnect: {break;}
	case Operator_Type_ClientSendDataToServer:
	{
		CtrlError iRet = TheTcpManager->Transmit_Client_SendDataToSever(op->postion, op->param1 + "\r\n");
		if (0 != iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SendDataToServer filed"));
			return iRet;
		}
		qDebug() << op->param1;
		break;
	}
	case Operator_Type_ClientSendTempDataToServer:
	{
		QString tempStr;
		if (0 != theFlageManager.GetTempStringByKey(op->param1, tempStr))
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientSendTempDataToServerTemp "));
			return TempKeyNotFind;
		}
		CtrlError iRet = TheTcpManager->Transmit_Client_SendDataToSever(op->postion, tempStr + "\r\n");
		if (0 != iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SendDataToServer filed"));
			return iRet;
		}
		qDebug() << op->param1;
		break;
	}
	case Operator_Type_ClientSendTempDataToServerMulity:
	{
		
		QStringList tempStrList = op->param1.split("$");
		int count = tempStrList.count();
		QString sendData;
		sendData += QString::number(count);
		for (int i = 0;i<tempStrList.count();i++)
		{
			QString tempStr;
			if (0 != theFlageManager.GetTempStringByKey(tempStrList[i]+"_Point", tempStr))
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientSendTempDataToServerTemp "));
				return TempKeyNotFind;
			}
			sendData = sendData + ";" + tempStr;
		}


		//if (0 != theFlageManager.GetTempStringByKey(op->param1, tempStr))
		//{
		//	emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientSendTempDataToServerTemp "));
		//	return false;
		//}
		//QStringList dataList = tempStr.split(",");
		////1;2;3;4
		//QString sendData = "1;" + dataList[0] + ";" + dataList[1] + ";" + dataList[3] ;
		if (0 != TheTcpManager->Transmit_Client_SendDataToSever(op->postion, sendData + "\r\n"))
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SendDataToServer filed"));
			return ClientSendDataError;
		}
		qDebug() << op->param1;
		break;
	}
	case Operator_Type_ADFClientSendTempDataToServerMulity:
	{
		QStringList tempStrList = op->param1.split("$");
		int count = tempStrList.count();
		QString sendData;
		int dataNum;
		int resultOK = 1;
		//判断贴拍照结果
		for (int i = 0;i<tempStrList.count();i++)
		{
			QString tempStr;
			if (0 != theFlageManager.GetTempStringByKey(tempStrList[i] + "_Result", tempStr))
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientSendTempDataToServerTemp "));
				return TempKeyNotFind;
			}
			if(tempStr == "PASS")
				resultOK = resultOK & 1;
			else
				resultOK = resultOK & 0;
		}
		if (resultOK != 1)
		{
			if (0 != TheTcpManager->Transmit_Client_SendDataToSever(op->postion,QString("2;0")  + "\r\n"))
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SendDataToServer filed"));
				return ClientSendDataError;
			}
			break;
		}


		//sendData += QString::number(count);
		//所有容器
		QList<QStringList> IMSResList;

		for (int i = 0; i < tempStrList.count(); i++)
		{
			QString tempStr;
			if (0 != theFlageManager.GetTempStringByKey(tempStrList[i] + "_Point", tempStr))
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientSendTempDataToServerTemp "));
				return TempKeyNotFind;
			}
			QStringList tempList =  tempStr.split(";");
			foreach(QString tempParseStr,tempList)
			{
				IMSResList.push_back(tempParseStr.split(";")); 

			}
			//原来的一个$符号只有一个数据情况
			//IMSResList.push_back(tempStr.split(";"));
		}
		
		for (int i = 0;i<IMSResList.count();i++)
		{
			for (int j = 0;j<IMSResList[i].count();j++)
			{
				//if (j == 0)
				//	dataNum += IMSResList[i].first().toInt();
				//else
				//{
					QString tempStr = IMSResList[i].at(j);
					tempStr.replace(",","#");
					sendData = sendData + ";" + tempStr;
				//}
			}
		}



		
		//1是成功标志
		sendData = QString::number(1) + ";" + QString::number(IMSResList.count()) +  sendData;
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(sendData));
		theFlageManager.SetTempStringByKey(op->param4, sendData);

		if (0 != TheTcpManager->Transmit_Client_SendDataToSever(op->postion, sendData + "\r\n"))
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SendDataToServer filed"));
			return ClientSendDataError;
		}
		qDebug() << op->param1;
		break;
	}
	case Operator_Type_ClientSendAndRecv: 
	{
		QString skt = op->postion;
		CtrlError iRet = TheTcpManager->Transmit_Client_SendDataToSever(op->postion, op->param1 + "\r\n");
		if (0 != iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SendDataToServer filed"));
			return ClientSendDataError;
		}
		//接收数据
		int tOut = op->param5.toInt();
		QString recvData;
		QTime t;
		t.start();
		//emit TheTcpManager->emit_TouchReadReady(op->postion);
		while (true)
		{
			if (t.elapsed() > tOut&&tOut != 0)
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("timeOut"));
				return TimeOutError;
			}
			if (0 != TheTcpManager->Client_CatchOneDataFromServerMsg(op->postion, recvData))
			{
				QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
				QThread::msleep(10);
			}
			else break;

			if (theMachineManager.GetExit())
				return ForceExit;

		}
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(recvData));
		//数据处理
		qDebug() << "Operator_Type_WaitOneMsgFromSever" << recvData;
		theFlageManager.SetTempStringByKey(op->param2, recvData);
		break;
	}
	case Operator_Type_ClientWaitOneMsgFromSever: 
	{
		int tOut = op->param5.toInt();
		QString recvData;
		QTime t;
		t.start();
		//emit TheTcpManager->emit_TouchReadReady(op->postion);
		while (true)
		{
			if (t.elapsed()>tOut&&tOut!=0)
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("timeOut"));
				return TimeOutError;
			}
			if (0 != TheTcpManager->Client_CatchOneDataFromServerMsg(op->postion, recvData))
			{
				QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
				QThread::msleep(10);
			}else break;

			if (theMachineManager.GetExit())
				return ForceExit;
				
		}
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(recvData));
		//数据处理
		qDebug() <<"Operator_Type_WaitOneMsgFromSever"<< recvData;

		break;
	}
	case Operator_Type_ClientWaitOneMsgFromSeverEqualParam1:
	{
		int tOut = op->param5.toInt();
		QString recvData;
		QTime t;
		t.start();
		//emit TheTcpManager->emit_TouchReadReady(op->postion);
		while (true)
		{
			if (t.elapsed() > tOut&&tOut != 0)
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("timeOut"));
				return TimeOutError;
			}
			//CtrlError iRet = TheTcpManager->Client_CatchOneDataFromServerMsg(op->postion, recvData);
			CtrlError iRet = TheTcpManager->Client_CatchOneDataFromServerMsg(op->postion, recvData,op->param1.length());
			if (TCPManager::RecvStrBufferEmpty == iRet)
			{
				//QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
				QThread::msleep(10);
			}
			else if (iRet == TCPManager::ClientNotFind)
			{
				return ClientNotFindError;
			}
			else break;
			if (theMachineManager.GetExit())
				return ForceExit;

		}
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(recvData));

		if (recvData != op->param1)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4-->5").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(recvData).arg("recvInfo not equal Param1"));
			//emit SendProMsgWithProfix(QStringList profix, QString msg, int type = 0);
			return DataNotMatch;
		}
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(recvData));
		//数据处理
		//qDebug() << "Operator_Type_WaitOneMsgFromSever" << recvData;
		break;
	}
	case Operator_Type_ADFClientWaitOneMsgFromSeverShockPlate:
	{
		int tOut = op->param5.toInt();
		QString recvData;
		QTime t;
		t.start();
		//emit TheTcpManager->emit_TouchReadReady(op->postion);
		while (true)
		{
			if (t.elapsed() > tOut&&tOut != 0)
			{
				emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("timeOut"));
				return TimeOutError;
			}
			if (0 != TheTcpManager->Client_CatchOneDataFromServerMsg(op->postion, recvData))
			{
				QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
				QThread::msleep(10);
			}
			else break;
			if (theMachineManager.GetExit())
				return ForceExit;
		}
		QString flagName = op->param1;
		QStringList successList = op->param2.split(",");//OK,NG 
		QStringList valueList = op->param3.split(",");
		QString tempKeyName = op->param4;

		QStringList shockDataList = recvData.split(";");
		for (int i =0;i<successList.count();i++)
		{
			if (recvData.contains(successList[i]))
				theFlageManager.SetFlagValueByName(flagName, valueList[i].toInt());
		}
		//if (recvData.contains(successList[0]))
		//{
		//	theFlageManager.SetFlagValueByName(flagName, valueList[0].toInt());
		//}
		//else if(recvData.contains(successList[1]))
		//{
		//	theFlageManager.SetFlagValueByName(flagName, valueList[1].toInt());
		//	
		//}
		//else
		//	return ParamValueError;

		theFlageManager.SetTempStringByKey(tempKeyName, recvData);
		for (int i =0;i<successList.count();++i)
		{
			if (successList[i] == shockDataList[0])
			{
				theFlageManager.SetFlagValueByName(flagName, valueList[i].toInt());
				if (i == 0)//默认设置第一个标识为成功 存放收到的数据
					theFlageManager.SetTempStringByKey(tempKeyName, recvData);
				break;
			}
		}

		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(recvData));
		//数据处理
		qDebug() << "Operator_Type_WaitOneMsgFromSever" << recvData;
		break;
	}
	case Operator_Type_ClientWaitOneMsgSetFlageByContent:
	{

	}
	case Operator_Type_ClientDeleteOnClient: 
	{
		CtrlError iRet = TheTcpManager->DisconnectClient(op->postion);
		if (0 != iRet&&iRet != TCPManager::ClientNotFind)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientDeleteOnClient failed"));
			return iRet;
		}
		break;
	}
	case Operator_Type_ClientDataClear:
	{
		CtrlError iRet = TheTcpManager->ClearBufferByClientName(op->postion);
		if (0 != iRet&&iRet != TCPManager::ClientNotFind)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_ClientDataClear failed"));
			return iRet;
		}
		break;
	}
	case Operator_Type_SeverCreateTcpServer:
	{
		CtrlError iRet = TheTcpManager->CreateServer(op->postion, op->param1.toInt(), op->param2.toInt());
		if (0 != iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SeverCreateTcpServer filed"));
			return iRet;
		}
		break;
	}
	case Operator_Type_SeverSendMsgToSkt:
	{
		CtrlError iRet = TheTcpManager->Server_SendMsgToSkt(op->postion, op->param1);
		if (0 != iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SeverSendMsgToSkt filed"));
			return iRet;
		}
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(op->param1));
		break;
	}
	case Operator_Type_SeverRecvOneMsgFromSkt:
	{
		QString recvStr;
		CtrlError iRet = TheTcpManager->Server_CatchOnMsgFromSkt(op->postion, recvStr, op->param5.toInt());
		if (0 != iRet)
		{
			emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg("Operator_Type_SeverRecvOneMsgFromSkt filed"));
			return iRet;
		}
		emit SendProcMsg(QString("%1--> %2-->%3 --> %4").arg(op->Identification).arg(op->station).arg(op->oprtType).arg(recvStr));
		qDebug() << recvStr;
		break;
	}

	case Operator_Type_Wait_Flow_Start:
	{
		QTime t;
		t.start();
		while (Process_Status_Work != theMachineManager.GetProcStatusByName(op->postion))
		{
			QThread::msleep(10);
			if (theMachineManager.GetExit())
			{
				//emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4").arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1));
				//emit SendProMsgWithProfix(QStringList() << "OP" << op->Identification << op->station << op->oprtType, CtrlError());
				DeleSubProc(op->postion);//停止子流程
				return ProcWaitFlowStartExecuteError;
			}
			if (t.elapsed() > op->param5.toInt())
			{
				return TimeOutError;
			}
		}
		break;
	}

	case Operator_Type_Wait_Flow_End:
	{
		ProcessState status = (ProcessState)theMachineManager.GetProcStatusByName(op->postion);
		if (Process_Status_Idle != theMachineManager.GetProcStatusByName(op->postion))
		{
			ProcessState status = (ProcessState)theMachineManager.GetProcStatusByName(op->postion);
			QThread::msleep(10);
			if (theMachineManager.GetExit())
			{
				emit SendProcMsg(QString("Exit: %1--> %2-->%3 --> %4-->%5").arg(op->Identification).arg(m_Proc_Name).arg(op->station).arg(op->oprtType).arg(op->param1),ErrorMsg);
				//点击停止后在此处强制结束子流程
				theMachineManager.ResetProcByName(op->postion);
				DeleSubProc(op->postion);
				m_bCountnue = true;
				return ProcWaitFlowEndExecuteError;
			}
			m_bCountnue = false;
			return CtrlError(NoError);
		}
		m_bCountnue = true;
		if (MachineManager::NoError != theMachineManager.GetProcExceptionByName(op->postion))
		{
			emit SendProcMsg(QString("%1;%2:异常退出%2").arg(op->Identification).arg(op->postion).arg("Process Exit!"),ErrorMsg);
			theMachineManager.ResetProcByName(op->postion);
			DeleSubProc(op->postion);
			return ProcWaitFlowEndExecuteError;
		}
		emit SendProcMsg(QString("%1->%2").arg(op->postion).arg("Process Exit!"));
		DeleSubProc(op->postion);
		break;
	}
	case Operator_Type_Flow_End:
	{
		Set_Proc_Status(Process_Status_End);
		break;
	}
	case Operator_Type_Start_Flow:
	{
		CtrlError iRet = theMachineManager.StartProcByName(op->postion);
		if (0 == iRet)
			AppendSubProc(op->postion);
		return iRet;
	}
	case Operator_Type_TestDebug:
	{
		emit SendProcMsg(QString("DebugInfo:%1--> %2-->%3 --> %4--> %5--> %6--> %7--> %8--> %9").arg(m_Proc_Name).arg(op->station).arg(op->oprtType)
			.arg(op->postion).arg(op->param1).arg(op->param2).arg(op->param3).arg(op->param4).arg(op->param5));
		break;
	}
	case Operator_Type_ErrorTestDebug:
	{
		emit SendProcMsg(QString("DebugInfo:%1--> %2-->%3 --> %4--> %5--> %6--> %7--> %8--> %9").arg(m_Proc_Name).arg(op->station).arg(op->oprtType)
			.arg(op->postion).arg(op->param1).arg(op->param2).arg(op->param3).arg(op->param4).arg(op->param5));
		return TestDebugError;
	}
	case Operator_Type_ShowMulityTempImg:
	{
		QStringList faceStrList = op->param1.split("$");

		//获取图片路径
		//QStringList prePath;
		//foreach(QString str, faceStrList)
		//{
		//	QString temp;
		//	if (0 != theFlageManager.GetTempStringByKey(str + "_ResultPath", temp))
		//	{
		//		return false;
		//	}
		//	prePath << temp;
		//}
		QStringList prePath;//img prePath
		foreach(QString str, faceStrList)
		{
			QString temp;
			if (0 != theFlageManager.GetTempStringByKey(str , temp))
				return KeyNotFind;
			prePath << temp;
		}


		int count = faceStrList.count();
		QMap<QString, QVariant> dispData;
		//内容解析
		for (int i = 0;i <count;i++)
		{
			Result_Data data;
			data.reslutImgPath = prePath[i];
			data.result = faceStrList[i];
			dispData.insert(QString::number(i), QVariant::fromValue(data));
		}
		emit SignalSendImg(dispData);
		break;
	}
	case Operator_Type_ShowMulityTempImgWithResult:
	{
		QStringList faceStrList = op->param1.split("$");
		QStringList resultStrList = op->param2.split("$");
		//获取图片路径
		//获取结果


		QStringList prePath, reusltList;
		foreach(QString str, faceStrList)
		{
			QString temp;
			if (0 != theFlageManager.GetTempStringByKey(str + "_ResultPath", temp))
			{
				return false;
			}
			prePath << temp;
		}
		foreach(QString str, resultStrList)
		{
			QString temp;
			if (0 != theFlageManager.GetTempStringByKey(str+"_Result", temp))
			{
				return false;
			}
			reusltList << temp;
		}
		//内容长度校验
		if (prePath.count() != resultStrList.count())
		{
			return false;
		}
		int count = prePath.count();
		QMap<QString, QVariant> dispData;
		//内容解析
		for (int i = 0;i < prePath.count();i++)
		{
			QStringList pathList = prePath.at(i).split(",");
			for (int j = 0;j<pathList.count();j++)
			{
				Result_Data data;
				data.reslutImgPath = pathList[j];
				data.result = reusltList[i];
				dispData.insert(QString::number(i*count+j), QVariant::fromValue(data));
			}
		}
		//iRet = theFlageManager.GetTempStringByKey(op->param1, record_sn);
		//QMap<QString, QVariant> dispData;
		//QStringList resultPaths = result.ResultPath.split(",");
		//for (int i = 0; i < resultPaths.count(); i++)
		//{
		//	Result_Data data;
		//	data.result = result.Result;
		//	data.reslutImgPath = resultPaths[i];
		//	dispData.insert(QString::number(i), QVariant::fromValue(data));
		//}
		emit SignalSendImg(dispData);

	}
	case Operator_Type_Write_OutPut:
	{
		//emit SendProcMsg(QString("Operator_Type_Write_OutPut %1->%2").arg(m_step_index).arg(op->param1));
		//foreach(OutputParam outPut, theDbManager.GetOutput())
		//{
		//	if (outPut.name == op->postion)
		//	{
		//		if (op->param1.toInt() ==0)
		//			m_Innctrl->WrtieOutputByName(outPut.name, op->param1.toInt());
		//		else
		//			m_Innctrl->WrtieOutputByName(outPut.name, 100);
		//		//int key = outPut.cardIndex * 1000 + outPut.portIndex;
		//		//QMap<int, int> map;
		//		//int val = op->param1.toInt();
		//		//map[key] = op->param1.toInt();
		//		//m_Innctrl->SetPWM(map);
		//		break;
		//	}
		//}
		break;
	}
	case Operator_Type_Msg_Box:
	{
		//msgBox::show("Warning", op->param1, 3);
		Proc_ShowMesBox("Warning", op->param1, 3);
		break;
	}
	case Operator_Type_RefreshCTWithTowTimePoint:
	{
		QString startTimeStr,endTimeStr;
		CtrlError iRet = theFlageManager.GetTempStringByKey(op->param1, startTimeStr);
		if (iRet != NoError)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		iRet = theFlageManager.GetTempStringByKey(op->param2, endTimeStr);
		if (iRet != NoError)
		{
			QString GetErrStr(iRet);
			emit SendProcMsg(
				QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;ErrorCode:%5;errorMsg:%6").
				arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
				.arg(op->oprtType).arg((QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper()).arg(CtrlError::GetErrStr(iRet)));
			return iRet;
		}
		QTime startTime = QTime::fromString(startTimeStr);
		QTime lastTime = QTime::fromString(endTimeStr);

		int diffTime = lastTime.msecsTo(startTime);
		theFlageManager.SetTempStringByKey(op->param3, QString::number(qAbs(diffTime)));
		theMachineManager.RefrshCTProduct(QString::number(qAbs(diffTime)));
		emit SignalMachineRefresh("");//刷新页面数据
		emit SendProcMsg(
			QString("ProcName:%1;StepIndex:%2;Op_Name:%3;Op_Type:%4;CT:%5;").
			arg(GetProcName()).arg(m_step_index).arg(op->aliasName)
			.arg(op->oprtType).arg(diffTime) );
		break;
	}
	case Operator_Type_AddOneProduct:
	{
		theMachineManager.AddOneProduct();
		emit SignalMachineRefresh("");//刷新页面数据
		break;
	}
	case Operator_Type_AddOneOkProduct:
	{
		theMachineManager.AddOneOKProduct();
		emit SignalMachineRefresh("OK");//刷新页面数据
		break;
	}
	case Operator_Type_AddOneNGProduct:
	{
		theMachineManager.AddOneNGProduct();
		emit SignalMachineRefresh("NG");//刷新页面数据
		break;
	}
	case Operator_Type_ClearCount:
	{
		theMachineManager.ClearCTData();
		emit SignalMachineRefresh("");//刷新页面数据
		break;
	}
	case Operator_Type_ShowInfo:
	{
		emit SignalShowInfo(op->param1,op->param2.toInt());
		break;
	}
	case Operator_Type_DeleteDir:
	{
		CtrlError iRet = theFileManager.DeleteDirectoryFileByRecentHour(op->param1,op->param2.toInt());
		if (iRet!=NoError)
		{
			ErrorPackage1(iRet, op);
			return iRet;
		}
		break;
	}
	case Operator_Type_Delay:
	{
		//op->param1;

		Sleep(op->param5.toInt());
		emit SendProcMsg("Operator_Type_Delay " + op->param1);
		break;
	}
	case Operator_Type_Jump:
	{
		//para1 当做跳转步骤使用
		emit SendProcMsg(QString("Operator_Type_Jump %1->%2").arg(m_step_index).arg(op->param1));
		//由于该步骤结束后会加一所以步骤需要减一 后面会加回来 进入正常
		m_step_index = op->param1.toInt() - 1;
		break;
	}
	case Operator_Type_Station_Move://工站运动...
	{
		//CStationMotion useStation;
		//if (!CParentProcess::FindOperatorStatin(op->station, theDbManager.GetStation(), useStation))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Station_Move not find STATION:  %1").arg(op->station));
		//	return false;
		//}
		//PointStation  targetPos;
		//if (!CParentProcess::FindStaionContainPos(op->postion,useStation, targetPos))
		//{
		//	emit SendProcMsg(QString("Operator_Type_Station_Move not find posName:  %1").arg(op->postion));
		//	return false;
		//}
		//CParentProcess::WaitStaionStop(useStation);
		//
		//CParentProcess::MoveToStiationPos(useStation, &targetPos);

		break;
	}
	case Operator_Type_NotContain:
		return false;
		break;
	default:
		break;
	}
	return NoError;
}

bool CParentProcess::EndStepCodeSuportElement(int stepIndex)
{
	emit SendProcMsg("CodeSupport-->" + m_Proc_Name + "; info stepIndex -->" + QString("%1").arg(stepIndex));
	//根据流程名称处理
	if (m_Proc_Name == "整机复位")
		return ADF01EndCodeSupport(stepIndex);
	else if (m_Proc_Name == "机械手1工作流程Test")
		return ADF05EndCodeSupport(stepIndex);
	else if (m_Proc_Name == "机械手2工作流程Test")
		return ADF06EndCodeSupport(stepIndex);
	else if (m_Proc_Name == "ADF010主流程")
		return ADF10EndCodeSupport(stepIndex);
	else if (m_Proc_Name == "放NG料工作流程Test")
		return ADF17EndCodeSupport(stepIndex);
	else if (m_Proc_Name == "NG下料工作流程Test")
		return ADF18EndCodeSupport(stepIndex);

	//根据xml 项目 名称进行分类处理
	//if ("EC_PROGRESS" == dataAnaly.m_CurProgram.aliasName)
	//{
	//	//根据流程名称处理
	//	if (m_Proc_Name == "整机复位")
	//		return ResetMatchWorkEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "机械手1工作流程Test")
	//		return Robot1WorkProcEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "机械手2工作流程Test")
	//		return Robot2WorkProcEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "放OK料工作流程Test")
	//		return Robot2WorkProcPutOkEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "放NG料工作流程Test")
	//		return Robot2WorkProcPutNGEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "NG下料工作流程Test")
	//		return NGDownloadEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "NG上料工作流程Test")
	//		return NGUploadEndCodeSupport(stepIndex);
	//	else if (m_Proc_Name == "NG移载工作流程Test")
	//		return NGMoveEndCodeSupport(stepIndex);
	//}
	return true;
}

bool CParentProcess::EndStepTest(int stepIndex)
{
	emit SendProcMsg("EndStepTest -->" + m_Proc_Name + "; info stepIndex -->" + QString("%1").arg(stepIndex));



	//根据 步骤号 进行代码补充操作
	switch (stepIndex)
	{
	case 0:

		break;
	case 3:
		//m_step_index = 0;
		break;
	case 39:
	{
		break;
	}
	case 40:
	{

		break;
	}

	default:

		break;
	}
	return true;
}

bool CParentProcess::ResetMatchWorkEndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 15://整机参数初始化
		//theHoldManager.Init(3);
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::RotStationWorkEndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0://整机参数初始化
			//theHoldManager.Init(3);
		break;
	default:
		break;
	}

	return true;
}

bool CParentProcess::CamStationWorkEndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	default:
		break;
	}
	return true;
}

bool CParentProcess::ArmStationWorkEndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	default:
		break;
	}
	return true;
}

bool CParentProcess::Robot1WorkProcEndCodeSupport(int stepIndex)
{
	emit SendProcMsg("EndStepTest -->" + m_Proc_Name + "; info stepIndex -->" + QString("%1").arg(stepIndex));
	//m_startTime = QDateTime::currentDateTime();
	//m_endTime = QDateTime::currentDateTime();
	//uint s_time = m_startTime.toTime_t();
	//uint e_time = m_endTime.toTime_t();
	//uint t_ret = e_time - s_time;
	//GetTickCount();


	//根据 步骤号 进行代码补充操作
	switch (stepIndex)
	{
	case 0:
		theMachineManager.m_Map_StartTime.insert(theMachineManager.TotalCount+1, QTime::currentTime());
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::Robot2WorkProcEndCodeSupport(int stepIndex)
{
	emit SendProcMsg("EndStepTest -->" + m_Proc_Name + "; info stepIndex -->" + QString("%1").arg(stepIndex));
	//m_startTime = QDateTime::currentDateTime();
	//m_endTime = QDateTime::currentDateTime();
	//uint s_time = m_startTime.toTime_t();
	//uint e_time = m_endTime.toTime_t();
	//uint t_ret = e_time - s_time;
	//GetTickCount();


	//根据 步骤号 进行代码补充操作
	switch (stepIndex)
	{
	case 0:

		break;
	case 3:
		//m_step_index = 0;
		break;
	case 39:
	{
		break;
	}
	case 40:
	{
		break;
	}

	default:

		break;
	}
	return true;
}

bool CParentProcess::Robot2WorkProcPutOkEndCodeSupport(int stepIndex)
{
	emit SendProcMsg("Robot2WorkProcPutOkEndCodeSupport -->" + m_Proc_Name + "; info stepIndex -->" + QString("%1").arg(stepIndex));

	//根据 步骤号 进行代码补充操作
	switch (stepIndex)
	{
	case 0:
	{

		break;
	}
	case 22://end
		//m_step_index = 0;
		theMachineManager.m_Map_EndTime.insert(theMachineManager.TotalCount+1, QTime::currentTime());
		theMachineManager.totalTime +=
			 theMachineManager.m_Map_StartTime[theMachineManager.TotalCount + 1]
			.secsTo(theMachineManager.m_Map_EndTime[theMachineManager.TotalCount + 1]);
		
		//计数添加
		theMachineManager.OKCount++;
		theMachineManager.TotalCount++;
		theMachineManager.CycleTime = theMachineManager.totalTime / theMachineManager.TotalCount;
		if (theMachineManager.TotalCount > 0)
			theMachineManager.PASSPercent = (double)theMachineManager.OKCount / (double)theMachineManager.TotalCount;
		theMachineManager.FailurePrecent = 1 - theMachineManager.PASSPercent;
		iniOperator.setValue("count", "TotalCount", theMachineManager.TotalCount);
		iniOperator.setValue("count", "OKCount", theMachineManager.OKCount);
		iniOperator.setValue("count", "NGCount", theMachineManager.NGCount);
		iniOperator.setValue("count", "PASSPercent", theMachineManager.PASSPercent);
		iniOperator.setValue("count", "FailurePrecent", theMachineManager.FailurePrecent);
		iniOperator.setValue("count", "CycleTime", theMachineManager.CycleTime);
		//
		emit SignalMachineRefresh("");//刷新页面数据
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::Robot2WorkProcPutNGEndCodeSupport(int stepIndex)
{
	emit SendProcMsg("Robot2WorkProcPutNGEndCodeSupport -->" + m_Proc_Name + "; info stepIndex -->" + QString("%1").arg(stepIndex));
	//根据 步骤号 进行代码补充操作
	switch (stepIndex)
	{
	case 0:
		{
			break;
		}
	case 23:
		theMachineManager.m_Map_EndTime.insert(theMachineManager.TotalCount + 1, QTime::currentTime());
		theMachineManager.totalTime += 
			theMachineManager.m_Map_StartTime[theMachineManager.TotalCount + 1]
			.secsTo(theMachineManager.m_Map_EndTime[theMachineManager.TotalCount + 1]);

		//计数添加
		theMachineManager.NGCount++;
		theMachineManager.TotalCount++;
		theMachineManager.CycleTime = theMachineManager.totalTime / theMachineManager.TotalCount;
		if (theMachineManager.TotalCount > 0)
			theMachineManager.PASSPercent = (double)theMachineManager.OKCount / (double)theMachineManager.TotalCount;
		theMachineManager.FailurePrecent = 1 - theMachineManager.PASSPercent;
		iniOperator.setValue("count", "TotalCount", theMachineManager.TotalCount);
		iniOperator.setValue("count", "OKCount", theMachineManager.OKCount);
		iniOperator.setValue("count", "NGCount", theMachineManager.NGCount);
		iniOperator.setValue("count", "PASSPercent", theMachineManager.PASSPercent);
		iniOperator.setValue("count", "FailurePrecent", theMachineManager.FailurePrecent);
		iniOperator.setValue("count", "CycleTime", theMachineManager.CycleTime);
		//
		emit SignalMachineRefresh("");//刷新页面数据
		
		break;
	default:

		break;
	}
	return true;
}

bool CParentProcess::NGDownloadEndCodeSupport(int stepIndex)
{
	return true;
}

bool CParentProcess::NGUploadEndCodeSupport(int stepIndex)
{
	return true;
}

bool CParentProcess::NGMoveEndCodeSupport(int stepIndex)
{
	return true;
}

bool CParentProcess::ADF01EndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0:
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::ADF05EndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0:
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::ADF06EndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0:
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::ADF10EndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0:
		break;
	case 1:
		theMachineManager.m_Map_StartTime.insert(theMachineManager.TotalCount + 1, QTime::currentTime());
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::ADF17EndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0:
		break;
	default:
		break;
	}
	return true;
}

bool CParentProcess::ADF18EndCodeSupport(int stepIndex)
{
	switch (stepIndex)
	{
	case 0:
		break;
	default:
		break;
	}
	return true;
}

void CParentProcess::SetFatherProc(QString procName)
{
	QMutexLocker locker(&m_Inherit);
	this->m_FatherProc = procName;
}

void CParentProcess::AppendSubProc(QString procName)
{
	QMutexLocker locker(&m_Inherit);
	this->m_SubProcList.push_back(procName);
}

CtrlError CParentProcess::DeleSubProc(QString procName)
{
	QMutexLocker locker(&m_Inherit);
	if (m_SubProcList.contains(procName))
		return NoError;
	else
		return NotFindProcError;
}

QStringList CParentProcess::GetSubProcList()
{
	QMutexLocker locker(&m_Inherit);
	return m_SubProcList;
}

void CParentProcess::ClearInheritProc()
{
	QMutexLocker locker(&m_Inherit);
	ExitInhreitProc();
	this->m_SubProcList.clear();
	this->m_FatherProc = "";
}

void CParentProcess::ExitInhreitProc()
{
	if (m_SubProcList.count() == 0) return;
	for (int i = 0;i < m_SubProcList.count();i++)
	{
		theMachineManager.ResetProcByName(m_SubProcList[i]);
	}
	this->m_SubProcList.clear();
}

//void CParentProcess::SetProList(QList<CParentProcess*> procList)
//{
//	m_procList = procList;
//}

//bool CParentProcess::FindOperatorStatin(QString station, QList<CStationMotion> stList, CStationMotion& FindSt)
//{
//	foreach(CStationMotion st,stList)
//	{
//		if (station == st.m_StatName )
//		{
//			FindSt = st;
//			return true;
//		}
//	}
//	return false;
//}

//bool CParentProcess::FindStaionContainPos(QString posName, CStationMotion station, PointStation& p_pos)
//{
//	for (int i = 0;i!= station.m_PonitList.count();++i)
//	{
//		PointStation* p = station.m_PonitList.at(i);
//		if (posName == p->PointName)
//		{
//			p_pos = *p;
//			//p_pos.PointIndex = p->PointIndex;
//			//p_pos.PointName = p->PointName;
//			//p_pos.pValueList = p->pValueList;
//			//foreach(double pos, p->pValueList)
//			//{
//			//	p_pos->pValueList.append(pos);
//			//}
//			return true;
//		}
//	}
//	//foreach(PointStation* p,	station.m_PonitList)
//	//{
//	//	if (posName == p->PointName)
//	//	{
//	//		p_pos = p;
//	//		p_pos->PointIndex = p->PointIndex;
//	//		p_pos->PointName = p->PointName;
//	//		foreach(double pos, p->pValueList)
//	//		{
//	//			p_pos->pValueList.append(pos);
//	//		}
//	//		//p_pos->pValueList = p->pValueList;
//	//		return true;
//	//	}
//	//}
//	return false;
//}

//void CParentProcess::WaitStaionStop(CStationMotion station)
//{
//	for (int i = 0; i != station.m_AxisCount; ++i)
//		theMachineCtrl.WaitMotion(station.m_AxisNameList.at(i));
//}
//
//void CParentProcess::MoveToStiationPos(CStationMotion station, PointStation* p_pos)
//{
//	for (int i = 0; i != station.m_AxisCount; ++i)
//	{
//		theMachineCtrl.MotionMove(station.m_AxisNameList.at(i), p_pos->pValueList.at(i));
//	}
//}

