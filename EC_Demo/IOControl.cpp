﻿#include "IOControl.h"
#include <thread>
#include "HardwareManager.h"
#include "Robot.h"
IOControl::IOControl(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
}

IOControl::~IOControl()
{

}

CtrlError IOControl::Init(HardwareInitData InitData)
{
    QMap<int, QString> InPutIOName;
    QMap<int, QString> OutPutIOName;
    QMap<int,QPair<QString, int> > InPutIOData;
    QMap<int,QPair<QString, int> > OutPutIOData;
    QMap<QString, QPair<QString, HardwareModule *> > Modules;
    if(InitData.Keys.size() != 6) return CtrlError(InitDataErr,InitData.Name);
    for(int i=0;i<InitData.InitData.size();i++)
    {
        QString Name;
        QString Type;
        int Index;
        QString Src;
        QString SrcType;
        int ID;
        if(InitData.InitData[0].size() != 6) return CtrlError(InitDataErr,InitData.Name);
        for(int j=0;j<6;j++)
        {
            if(InitData.Keys[j] == "Name" && InitData.InitData[i][j].canConvert<QString>())
                Name = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "Type" && InitData.InitData[i][j].canConvert<QString>())
                Type = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "Index" && InitData.InitData[i][j].canConvert<int>())
                Index = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "Src" && InitData.InitData[i][j].canConvert<QString>())
                Src = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "SrcType" && InitData.InitData[i][j].canConvert<QString>())
                SrcType = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "ID" && InitData.InitData[i][j].canConvert<int>())
                ID = InitData.InitData[i][j].value<int>();
            else
                return CtrlError(InitDataErr,InitData.Name);
        }
        //加入IOData
        if(Type == "InPut")
        {
            InPutIOName.insert(Index,Name);
            InPutIOData.insert(Index,QPair<QString,int>(Src,ID));
        }
        else if(Type == "OutPut")
        {
            OutPutIOName.insert(Index,Name);
            OutPutIOData.insert(Index,QPair<QString,int>(Src,ID));
        }
        else
        {
            return CtrlError(InitDataErr,InitData.Name);
        }
        //加入Modules
        if(!Modules.contains(Src))
        {
            Modules.insert(Src,QPair<QString,HardwareModule *>(SrcType,HardwareManager::GetInstance()->GetModulePtr(Src)));
        }
    }
    return Init(InitData.Name,Modules,InPutIOData,OutPutIOData,InPutIOName,OutPutIOName);
}

CtrlError IOControl::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    CtrlError ret;
    ResultData.Station = ExecuteData.Station;
    ResultData.Identfication = ExecuteData.Identfication;
    ResultData.OperatorName = ExecuteData.OperatorName;

    if(ExecuteData.OperatorName == "SetOutPut")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        if(!ExecuteData.Datas["Value"].canConvert<bool>()) return CtrlError(ExecuteDataNotRight,Name);
        bool Value = ExecuteData.Datas["Value"].value<bool>();
        ret =  SetOutPut(Index,Value);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "SetOutPuts")
    {
        if(!ExecuteData.Datas["Value"].canConvert<QMap<int, bool>>()) return CtrlError(ExecuteDataNotRight,Name);
        QMap<int, bool> Value = ExecuteData.Datas["Value"].value<QMap<int, bool>>();
        ret =  SetOutPuts(Value);
        if(ret != NoError) return ret;
    }

    if(ExecuteData.OperatorName == "GetInPut")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        bool Value;
        ret =  GetInPut(Index,Value);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Value",QVariant::fromValue(Value));
    }
    if(ExecuteData.OperatorName == "GetInPuts")
    {
        QMap<int,bool> Value;
        ret =  GetInPuts(Value);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Value",QVariant::fromValue(Value));
    }
    if(ExecuteData.OperatorName == "GetOutPut")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        bool Value;
        ret =  GetOutPut(Index,Value);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Value",QVariant::fromValue(Value));
    }
    if(ExecuteData.OperatorName == "GetOutPuts")
    {
        QMap<int,bool> Value;
        ret =  GetOutPuts(Value);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Value",QVariant::fromValue(Value));
    }
    if(ExecuteData.OperatorName == "GetIONames")
    {
        QMap<int, QString> Inputs;
        QMap<int, QString> Outputs;
        ret =  GetIONames(Inputs,Outputs);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("InPuts",QVariant::fromValue(Inputs));
        ResultData.Datas.insert("OutPuts",QVariant::fromValue(Outputs));
    }
    return CtrlError(NoError);
}

CtrlError IOControl::Init(QString Name, QMap<QString, QPair<QString, HardwareModule *> > Modules, QMap<int, QPair<QString, int> > InPutIOData, QMap<int, QPair<QString, int> > OutPutIOData, QMap<int, QString> InPutIOName, QMap<int, QString> OutPutIOName)
{
    CtrlError ret;
    this->Name = Name;
    this->Modules = Modules;
    if(InPutIOName.keys() != InPutIOData.keys()) return CtrlError(IONameNotEqualIOData,Name);
    if(OutPutIOName.keys() != OutPutIOData.keys()) return CtrlError(IONameNotEqualIOData,Name);
    this->InPutIOData = InPutIOData;
    this->OutPutIOData = OutPutIOData;
    this->InPutIOName = InPutIOName;
    this->OutPutIOName = OutPutIOName;
    //这里等1s，防止机器人那边没数据影响SetOutPutValue初始化
    QMap<int,bool> Data;
    ret = GetInPuts(Data);
    if(ret != NoError) return ret;
    ret = GetOutPuts(Data);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError IOControl::SetOutPut(int Index, bool Value)
{
    CtrlError ret;
    if(!OutPutIOData.contains(Index)) return CtrlError(IndexOutOfRange,Name);
    QPair<QString,int> OutPutData = OutPutIOData[Index];
    QPair<QString, HardwareModule *> ModuleData = Modules[OutPutData.first];
    if(ModuleData.first == "Robot")
    {
        Robot *tRobot = qobject_cast<Robot*>(ModuleData.second);
        if(tRobot == nullptr) return CtrlError(ModuleTypeIsWrong,Name);
        ret = tRobot->SetOutput(OutPutData.second,Value);
        if(ret != NoError) return ret;
    }
    else
    {
        return CtrlError(ModuleTypeNotAllow,Name);
    }
    return CtrlError(NoError);
}

CtrlError IOControl::SetOutPuts(QMap<int, bool> Value)
{
    CtrlError ret;
    foreach(int Index,Value.keys())
    {
        ret = SetOutPut(Index,Value[Index]);
        if(ret != NoError) return ret;
    }
    return CtrlError(NoError);
}

CtrlError IOControl::GetInPut(int Index, bool &Value)
{
    if(!InPutIOData.contains(Index)) return CtrlError(IndexOutOfRange,Name);
    QPair<QString,int> InPutData = InPutIOData[Index];
    QPair<QString, HardwareModule *> ModuleData = Modules[InPutData.first];
    if(ModuleData.first == "Robot")
    {
        if(Index >= 64) return CtrlError(IndexOutOfRange,Name);
        Robot *tRobot = qobject_cast<Robot*>(ModuleData.second);
        if(tRobot == nullptr) return CtrlError(ModuleTypeIsWrong,Name);
        RobotStatus Status = tRobot->GetStatus();
        Value = Status.digital_ioInput[InPutData.second];
    }
    else
    {
        return CtrlError(ModuleTypeNotAllow,Name);
    }
    return CtrlError(NoError);
}

CtrlError IOControl::GetInPuts(QMap<int, bool> &Value)
{
    CtrlError ret;
    Value.clear();
    foreach (int Index, InPutIOData.keys())
    {
        bool tValue;
        ret = GetInPut(Index, tValue);
        if(ret != NoError) continue;
        Value.insert(Index,tValue);
    }
    return CtrlError(NoError);
}

CtrlError IOControl::GetOutPut(int Index, bool &Value)
{
    if(!OutPutIOData.contains(Index)) return CtrlError(IndexOutOfRange,Name);
    QPair<QString,int> OutPutData = OutPutIOData[Index];
    QPair<QString, HardwareModule *> ModuleData = Modules[OutPutData.first];
    if(ModuleData.first == "Robot")
    {
        if(Index >= 64) return CtrlError(IndexOutOfRange,Name);
        Robot *tRobot = qobject_cast<Robot*>(ModuleData.second);
        if(tRobot == nullptr) return CtrlError(ModuleTypeIsWrong,Name);
        RobotStatus Status = tRobot->GetStatus();
        Value = Status.digital_ioOutput[OutPutData.second];
    }
    else
    {
        return CtrlError(ModuleTypeNotAllow,Name);
    }
    return CtrlError(NoError);
}

CtrlError IOControl::GetOutPuts(QMap<int, bool> &Value)
{
    CtrlError ret;
    Value.clear();
    foreach (int Index, OutPutIOData.keys())
    {
        bool tValue;
        ret = GetOutPut(Index, tValue);
        if(ret != NoError) continue;
        Value.insert(Index,tValue);
    }
    return CtrlError(NoError);
}

CtrlError IOControl::GetIONames(QMap<int, QString> &Inputs, QMap<int, QString> &Outputs)
{
    Inputs = InPutIOName;
    Outputs = OutPutIOName;
    return CtrlError(NoError);
}




























