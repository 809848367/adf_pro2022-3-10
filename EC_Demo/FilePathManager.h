#pragma once
#include <QObject>
#include <QMap>
//程序路径管理和导入
#define theIniManger FilePathManager::Instance()
#define ParamFilePath		"filePath"
#define ParamMachineCfg		"MachineCfg"
#define ParamCount			"count"
class FilePathManager : public QObject
{
	Q_OBJECT
public:
	enum ErrorCode {
		NoError = 0,
		Base = 0x00800000,
		KeyNotFind = Base + 0x001,
		NoSection = Base + 0x002,
	};
	Q_ENUM(ErrorCode)

public:
	FilePathManager(QObject *parent = Q_NULLPTR);
	~FilePathManager();
	static FilePathManager* Instance();
	void ReadInitData();
	int	WriteDataToParamIni(QString section, QString key,QVariant value);
	void WriteToFile();//将数据同步到文件

	int ReadFilePath(QString& value,QString key);
	int ReadMachineCfg(QString& value,QString key);
	int ReadCount(double& value,QString key);

	//数据刷入
private:
	

	QMap<QString, QString> m_pathMap;//filePath
	QMap<QString, QString> m_MachineCfgMap;//MachineCfg
	QMap<QString, double> m_countMap;//count

};
