﻿#include "LogManager.h"
#include <thread>
LogManager::LogManager(QObject *parent) : QObject(parent)
{

}

LogManager &LogManager::GetInstance()
{
    static LogManager Instance;
    return Instance;
}

void LogManager::Init()
{
    LogBuffer.clear();
    LogFilePath = "D:/ADF_Config/LOG";
    QDir Folder,BackupFolder;
    Folder.setPath(LogFilePath);
    BackupFolder.setPath(LogFilePath + "/Backup");
    if(!Folder.exists())
    {
        Folder.mkdir(LogFilePath);
    }
    if(!BackupFolder.exists())
    {
        BackupFolder.mkdir(LogFilePath + "/Backup");
    }
    LogBackup();
    std::thread t(&LogManager::thread_SaveLog,this);
    t.detach();
    WriteLog("RUN","Main","Software Open");
}

void LogManager::WriteLog(QString LogType, QString ModuleName, QString LogStr)
{
    try
    {
		QMutexLocker tempLocker(&Locker);
        //Get Time Data
        QDateTime current_date_time = QDateTime::currentDateTime();
        QString TimeData = current_date_time.toString("yyyy-MM-dd hh:mm:ss.zzz");

        //Send Log Out
        if(LogType == "RUN" || LogType == "ERROR")
        {
            emit SendLog(TimeData,LogType,ModuleName,LogStr);
        }

        //Create Log String And Save
        QString OutStr = TimeData+" ["+LogType+"]["+ModuleName+"] "+LogStr;
        //Locker.lock();
        LogBuffer.push_back(OutStr);
        //Locker.unlock();
    }
    catch(...)
    {
        qDebug()<<"Log Error";
    }
}

void LogManager::thread_SaveLog()
{
    while(1)
    {
        if(LogBuffer.size()>0)
        {
            Locker.lock();
            QList<QString> t = LogBuffer;
            LogBuffer.clear();
            Locker.unlock();
            QFile OutFile;
            OutFile.setFileName(LogFilePath+"/" + "Log" + ".txt");
            OutFile.open(QFile::WriteOnly | QFile::Append);
            for(int i=0;i<t.size();i++)
            {
                qDebug()<< t[i];
                OutFile.write((t[i]+"\n").toLocal8Bit());  //Here Can Only Save English Log
            }
            OutFile.close();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void LogManager::LogBackup()
{
    QDateTime current_date_time = QDateTime::currentDateTime();
    QString TimeData = current_date_time.toString("yyyy-MM-dd_hh-mm-ss");
    QDir LogFolder(LogFilePath);
    QList<QString> NameFilter;
    NameFilter << "*.txt";
    QList<QFileInfo> LogFiles = LogFolder.entryInfoList(NameFilter);
    foreach (QFileInfo LogFile, LogFiles)
    {
        QFile t;
        t.setFileName(LogFile.filePath());
        QString TargetPath = LogFilePath + "/Backup/" + LogFile.fileName().left(LogFile.fileName().size()-LogFile.fileName().lastIndexOf(".")-1) + "_" + TimeData + ".txt";
        qDebug()<<LogFile.filePath();
        qDebug()<<TargetPath;
        if(t.copy(TargetPath))
        {
            t.remove();
        }
    }
}
