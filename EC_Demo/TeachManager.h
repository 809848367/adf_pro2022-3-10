#ifndef TEACHMANAGER_H
#define TEACHMANAGER_H

#include <QObject>
#include <QSharedPointer>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "TeachData.h"
#include "TeachData_Robot.h"
#include "TeachData_Lighting.h"
#include "TeachData_PanaMotor.h"
#include "TeachData_Camera.h"
#include "CtrlError.h"
struct TeachTable
{
    QString Name;
    QString Type;
    QVector<QString> Keys;
    QVector<QVector<QString>> Data;
};

class TeachManager : public QObject
{
    Q_OBJECT
    explicit TeachManager(QObject *parent = nullptr);
public:
    static TeachManager &GetInstance();
    CtrlError InitDB(QString Path);
    CtrlError SetTable(TeachTable Table);
    CtrlError GetTeachData(QString Station,QString Name,QVariant &tTeachData);
    CtrlError GetTeachDataList(QString Station,QVector<QString> &Names);
private:
    QString StationName;
    QMap<QString,QPair<QString,QSharedPointer<TeachData>>> TeachDatas;
    QMutex TeachLock;
    QSqlDatabase Database;
    CtrlError ReadDataBase(QString Table, QVector<QString> &Keys,QVector<QVector<QString>> &Data);
    CtrlError WriteDataBase(QString Table, QVector<QString> Keys,QVector<QVector<QString>> Data);
    CtrlError CreateDataBase(QString Table, QVector<QString> Keys);
signals:
    void UpDateTable(TeachTable Table);
    void RefreshTeachData(QString Station,TeachData_Robot tTeachData);
    void RefreshTeachData(QString Station,TeachData_Lighting tTeachData);
    void RefreshTeachData(QString Station,TeachData_PanaMotor tTeachData);
    void RefreshTeachData(QString Station,TeachData_Camera tTeachData);
public slots:
    void UpDateTableSlot(TeachTable Table);
    void UpDateTeachData(QString Station,TeachData_Robot tTeachData);
    void UpDateTeachData(QString Station,TeachData_Lighting tTeachData);
    void UpDateTeachData(QString Station,TeachData_PanaMotor tTeachData);
    void UpDateTeachData(QString Station,TeachData_Camera tTeachData);
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00500000,
        TeachTableFormatWrong    = Base + 0x001,
        NameIsWrong              = Base + 0x002,
        StationIsWrong           = Base + 0x003,
        OpenDBFail               = Base + 0x004,
        ReadDBError              = Base + 0x005,
        WriteDBError             = Base + 0x006,
    };
    Q_ENUM(ErrorCode)
};

#endif // TEACHMANAGER_H
