﻿#include "Lighting.h"
#include "HardwareManager.h"
#include "SerialPortManager.h"
#include <thread>

Lighting::Lighting(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    threadFlag = 0;
    std::thread t(&Lighting::thread_Comm,this);
    t.detach();
}

Lighting::~Lighting()
{
    threadFlag = 3;
    while(threadFlag == 3) QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

CtrlError Lighting::Init(HardwareInitData InitData)
{
    QMap<int,QPair<QString, int> > LightData;
    SerialPortManager *tSPM;
    if(InitData.Keys.size() != 4) return CtrlError(InitDataErr,InitData.Name);
    for(int i=0;i<InitData.InitData.size();i++)
    {
        QPair<QString, int> tLightData;
        int Index;
        if(InitData.InitData[0].size() != 4) return CtrlError(InitDataErr,InitData.Name);
        for(int j=0;j<4;j++)
        {
            if(InitData.Keys[j] == "Index" && InitData.InitData[i][j].canConvert<int>())
                Index = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "ComID" && InitData.InitData[i][j].canConvert<QString>())
                tLightData.first = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "ID" && InitData.InitData[i][j].canConvert<int>())
                tLightData.second = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "SerialPortManager" && InitData.InitData[i][j].canConvert<QString>())
            {
                if(i == 0) tSPM = qobject_cast<SerialPortManager*>(HardwareManager::GetInstance()->GetModulePtr(InitData.InitData[i][j].value<QString>()));
            }
            else
                return CtrlError(InitDataErr,InitData.Name);
        }
        LightData.insert(Index,tLightData);
    }
    return Init(InitData.Name,tSPM,LightData);
}

CtrlError Lighting::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    CtrlError ret;
    ResultData.Station = ExecuteData.Station;
    ResultData.Identfication = ExecuteData.Identfication;
    ResultData.OperatorName = ExecuteData.OperatorName;
    if(ExecuteData.OperatorName == "SetLight")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        if(!ExecuteData.Datas["Value"].canConvert<uchar>()) return CtrlError(ExecuteDataNotRight,Name);
        uchar Value = ExecuteData.Datas["Value"].value<uchar>();
        ret =  SetLight(Index,Value);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "SetAllLight")
    {
        if(!ExecuteData.Datas["Value"].canConvert<QMap<int,uchar>>()) return CtrlError(ExecuteDataNotRight,Name);
        QMap<int,uchar> Value = ExecuteData.Datas["Value"].value<QMap<int,uchar>>();
        ret =  SetAllLight(Value);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "GetLight")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        uchar Value;
        ret =  GetLight(Index,Value);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Value",QVariant::fromValue(Value));
    }
    if(ExecuteData.OperatorName == "GetAllLight")
    {
        QMap<int,uchar> Value;
        ret =  GetAllLight(Value);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Value",QVariant::fromValue(Value));
    }
    if(ExecuteData.OperatorName == "WaitLight")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        if(!ExecuteData.Datas["Value"].canConvert<uchar>()) return CtrlError(ExecuteDataNotRight,Name);
        uchar Value = ExecuteData.Datas["Value"].value<uchar>();
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret =  WaitLight(Index,Value,TimeOut);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "WaitAllLight")
    {
        if(!ExecuteData.Datas["Value"].canConvert<QMap<int,uchar>>()) return CtrlError(ExecuteDataNotRight,Name);
        QMap<int,uchar> Value = ExecuteData.Datas["Value"].value<QMap<int,uchar>>();
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret =  WaitAllLight(Value,TimeOut);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "SetLightAndWait")
    {
        if(!ExecuteData.Datas["Index"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Index = ExecuteData.Datas["Index"].value<int>();
        if(!ExecuteData.Datas["Value"].canConvert<uchar>()) return CtrlError(ExecuteDataNotRight,Name);
        uchar Value = ExecuteData.Datas["Value"].value<uchar>();
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret =  SetLightAndWait(Index,Value,TimeOut);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "SetAllLightAndWait")
    {
        if(!ExecuteData.Datas["Value"].canConvert<QMap<int,uchar>>()) return CtrlError(ExecuteDataNotRight,Name);
        QMap<int,uchar> Value = ExecuteData.Datas["Value"].value<QMap<int,uchar>>();
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret =  SetAllLightAndWait(Value,TimeOut);
        if(ret != NoError) return ret;
    }
    return CtrlError(NoError);
}

CtrlError Lighting::Init(QString Name, SerialPortManager *SPM, QMap<int,QPair<QString, int> > LightData)
{
    CtrlError ret;
    threadFlag = 2;
    while(threadFlag == 2) QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    if(SPM == nullptr) return CtrlError(SPMisEmpty,Name);
    this->Name = Name;
    this->SPM = SPM;
    this->LightData = LightData;
    QByteArray Res;
    foreach (int Key, LightData.keys())
    {
        ret = SPM->SendAndRecv(LightData[Key].first,QString::asprintf("S%c0000#",'A'+LightData[Key].second).toLocal8Bit(),Res,1);
        if(ret != NoError) return ret;
        SetValue[Key] = 0;
        GetValue[Key] = 0;
    }
    threadFlag = 1;
    return CtrlError(NoError);
}

CtrlError Lighting::SetLight(int Index, uchar Value)
{
    if(!LightData.contains(Index)) return CtrlError(IndexOutOfRange,Name);
    SetLocker.lock();
    SetValue[Index] = Value;
    SetLocker.unlock();
    return CtrlError(NoError);
}

CtrlError Lighting::SetAllLight(QMap<int,uchar> Value)
{
    SetLocker.lock();
    foreach (int Index, SetValue.keys())
    {
        if(Value.contains(Index)) SetValue[Index] = Value[Index];
    }
    SetLocker.unlock();
    return CtrlError(NoError);
}

CtrlError Lighting::GetLight(int Index, uchar &Value)
{
    if(!LightData.contains(Index)) return CtrlError(IndexOutOfRange,Name);
    GetLocker.lock();
    Value = GetValue[Index];
    GetLocker.unlock();
    return CtrlError(NoError);
}

CtrlError Lighting::GetAllLight(QMap<int,uchar> &Value)
{
    GetLocker.lock();
    Value = GetValue;
    GetLocker.unlock();
    return CtrlError(NoError);
}

CtrlError Lighting::WaitLight(int Index, uchar Value, int TimeOut)
{
    CtrlError ret;
    QTime timer;
    timer.start();
    uchar ValueNow;
    do
    {
        ret = GetLight(Index,ValueNow);
        if(ret != NoError) return ret;
        if(TimeOut > 0 && timer.elapsed() >= TimeOut) return CtrlError(WaitLightTimeout,Name);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }while(ValueNow != Value);
    return CtrlError(NoError);
}

CtrlError Lighting::WaitAllLight(QMap<int, uchar> Value, int TimeOut)
{
    CtrlError ret;
    QTime timer;
    timer.start();
    QMap<int, uchar> ValueNow;
    bool isSame = false;
    do
    {
        ret = GetAllLight(ValueNow);
        if(ret != NoError) return ret;
        if(TimeOut > 0 && timer.elapsed() >= TimeOut) return CtrlError(WaitLightTimeout,Name);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        isSame = true;
        foreach (int Index, Value.keys())
        {
            if(Value[Index] != ValueNow[Index]) isSame = false;
        }
    }while(!isSame);
    return CtrlError(NoError);
}

CtrlError Lighting::SetLightAndWait(int Index, uchar Value, int TimeOut)
{
    CtrlError ret;
    ret = SetLight(Index,Value);
    if(ret != NoError) return ret;
    ret = WaitLight(Index,Value,TimeOut);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError Lighting::SetAllLightAndWait(QMap<int, uchar> Value, int TimeOut)
{
    CtrlError ret;
    ret = SetAllLight(Value);
    if(ret != NoError) return ret;
    ret = WaitAllLight(Value,TimeOut);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

void Lighting::thread_Comm()
{
    CtrlError ret;
    QByteArray Res;
    QMap<int,uchar> tSetValue = SetValue;
    QMap<int,uchar> tGetValue = GetValue;
    while(1)
    {
        //无数据发送时，10ms一次，防止卡机
        SetLocker.lock();
        tSetValue = SetValue;
        SetLocker.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
        if(tSetValue == tGetValue) std::this_thread::sleep_for(std::chrono::milliseconds(50));
        if(threadFlag == 3)
        {
            threadFlag = 4;
            return;
        }
        if(threadFlag == 2) threadFlag = 0;
        if(threadFlag == 0) continue;
        //如发现不同，则修改,然后查询

        foreach (int Index, LightData.keys())
        {
            if(tSetValue[Index] != tGetValue[Index])
            {
                Res.clear();
                SPM->SendAndRecv(LightData[Index].first,QString::asprintf("S%c%04d#",'A'+LightData[Index].second,tSetValue[Index]).toLocal8Bit(),Res,1);
                if(ret == NoError && Res.size() == 1 && Res[0] == char('a'+LightData[Index].second))
                {
                    Res.clear();
                    ret = SPM->SendAndRecv(LightData[Index].first,QString::asprintf("S%c#",'A'+LightData[Index].second).toLocal8Bit(),Res,1);
                    if(ret == NoError && Res.size() == 5 && Res[0] == char('a'+LightData[Index].second))
                    {
                        tGetValue[Index] = Res.right(4).toInt();
                    }
                }
            }
        }
        GetLocker.lock();
        GetValue = tGetValue;
        GetLocker.unlock();
    }
}
