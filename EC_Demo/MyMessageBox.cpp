#include "MyMessageBox.h"
#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 
MyMessageBox* MyMessageBox::instance = nullptr;

MyMessageBox::MyMessageBox(QObject *parent)
	: QObject(parent)
{
}


MyMessageBox::MyMessageBox(QString title, QString msg, int type):
	m_title(title), m_content(msg), m_type(type)
{
}

MyMessageBox::~MyMessageBox()
{
}

void MyMessageBox::readyShow()
{
	MyMessageBox* messageBox = MyMessageBox::GetBoxInstance();
	this->moveToThread(qApp->thread());
	QTimer::singleShot(0, messageBox, SLOT(onShow()));
}

void MyMessageBox::onShow()
{
	MyMessageBox* messageBox = MyMessageBox::GetBoxInstance();
	QMessageBox::StandardButton result = QMessageBox::information(nullptr, messageBox->m_title, messageBox->m_content, QMessageBox::Yes | QMessageBox::No);
	//this->deleteLater();
}

void MyMessageBox::showBox()
{
	QEventLoop eventLoop;
	MyMessageBox* messageBox = MyMessageBox::GetBoxInstance();
	connect(messageBox, SIGNAL(destroyed()), &eventLoop, SLOT(quit()));
	messageBox->readyShow();
	eventLoop.exec();
}

void MyMessageBox::DeleteBox()
{
	instance->deleteLater();
}

MyMessageBox* MyMessageBox::GetBoxInstance()
{
	if (instance == nullptr)
	{
		instance = new MyMessageBox;
	}

	return instance;
}

void MyMessageBox::SetDispMsg(QString title, QString content)
{
	MyMessageBox*p = MyMessageBox::GetBoxInstance();
	p->setDispInfo(title, content);

}

void MyMessageBox::setDispInfo(QString title, QString content)
{
	this->m_title = title;
	this->m_content = content;
}
