#include "OneResponeClinet.h"
#include <QTcpServer>
#include "LogManager.h"
#include "CFileOperator.h"
#include "ModbusMoudle.h"


OneResponeClinet::OneResponeClinet(QString IP, quint16 Port):IP(IP),Port(Port)
{
	cmdList.clear();		//���� 
	respondList.clear();	//����
	m_bRun = false;
	theModbusMoudle->ConnectETH(IP, 0, Port);
}


OneResponeClinet::~OneResponeClinet()
{
}

void OneResponeClinet::run()
{
	//CtrlError  iRet = theModbusMoudle->WriteData_ETH(REGI_H5U_D, op->param3.toInt(), op->param4.toUShort());
	m_bRun = true;
	while (true)
	{
		QThread::msleep(100);
		theModbusMoudle->WriteDataInt_ETH(REGI_H5U_D, 10, 1);
		if (!m_bRun)
		{
			theFileManager.WriteTodayFile_TXT("PLC thread exit");
			return;
		}
	}
	qDebug() << "PLC Thread exit";

	//m_socket = new QTcpSocket;
	//if (m_socket->isOpen()) return ;
	//m_socket->connectToHost(QHostAddress(IP), Port);
	//if (!m_socket->waitForConnected())
	//	return ;
	//m_bRun = true;
	//while (true)
	//{
	//	QThread::msleep(100);
	//	if (cmdList.size()>0)
	//	{
	//		qDebug() << "cmdList.size��" << cmdList.size();
	//		PLCData cmd = GetOneCmdDataFromList();
	//		qDebug() << "cmdList.size��" << cmdList.size();
	//		if (cmd.OperatorType == "ReadPort")
	//		{
	//			QByteArray cmdData = CreateReadCmd(cmd.port);
	//			QByteArray resData;
	//			if (CtrlError(NoError) != SendAndRecv(cmdData, resData))
	//			{
	//				cmd.resInfo = "error";
	//				PushOneRespondDataToList(cmd);
	//				return;
	//			}
	//			cmd.value = resData.at(resData.size() - 1);
	//			cmd.resInfo = "success";
	//			PushOneRespondDataToList(cmd);
	//		}
	//		else if (cmd.OperatorType == "WritePort")
	//		{
	//			QByteArray cmdData = CreateWriteCmd(cmd.port,cmd.value);
	//			QByteArray resData;
	//			if (CtrlError(NoError) != SendAndRecv(cmdData, resData))
	//			{
	//				cmd.resInfo = "error";
	//				PushOneRespondDataToList(cmd);
	//				return;
	//			}
	//			cmd.resInfo = "success";
	//			PushOneRespondDataToList(cmd);
	//		}
	//		else if (cmd.OperatorType == "WaitPort")
	//		{
	//			QByteArray cmdData = CreateReadCmd(cmd.port);
	//			qDebug() << "cmd->" << cmdData << endl;
	//			QByteArray resData;
	//			QTime t;
	//			t.start();
	//			do 
	//			{
	//				QThread::msleep(10);
	//				//����ȴ�ʱ��û�з� ����
	//				{
	//					QByteArray cmdData = CreateWriteCmd(10, 1);
	//					QByteArray resData;
	//					if (0 != SendAndRecv(cmdData, resData))
	//					{
	//						qDebug() << "error";
	//						return;
	//					}
	//					qint8 value = resData.at(resData.size() - 1);
	//					if (value != 1)
	//					{
	//						qDebug() << "wart failed";
	//					}
	//				}
	//				if (CtrlError(NoError) != SendAndRecv(cmdData, resData))
	//				{
	//					cmd.resInfo = "error";
	//					PushOneRespondDataToList(cmd);
	//					qDebug() << "wait error";
	//					return;
	//				}
	//				qint8 value = resData.at(resData.size() - 1);
	//				
	//				//qDebug() << "respond->"<< resData << endl;
	//				if (value == cmd.value)
	//				{
	//					cmd.resInfo = "success";
	//					PushOneRespondDataToList(cmd);
	//					break;
	//				}
	//				if (t.elapsed()>cmd.timeOut&&cmd.timeOut!=0)
	//				{
	//					cmd.resInfo = "error";
	//					PushOneRespondDataToList(cmd);
	//					qDebug() << "wait time out";
	//					break;
	//				}
	//				
	//			} while (true);
	//		}
	//	}
	//	else
	//	{
	//		QByteArray cmdData = CreateWriteCmd(10, 1);
	//		QByteArray resData;
	//		if (0 !=SendAndRecv(cmdData, resData))
	//		{
	//			qDebug() <<"error" ;
	//			return;
	//		}
	//		qint8 value = resData.at(resData.size() - 1);
	//		if (value!=1)
	//		{
	//			qDebug() << "wart failed";
	//		}
	//	}
	//	if (!m_bRun) 
	//	{
	//		theFileManager.WriteTodayFile_TXT("PLC thread exit");
	//		return;
	//	}
	//}
	//qDebug() << "PLC Thread exit";

}

bool OneResponeClinet::IsConnectServer()
{
	return m_bRun;
}

QByteArray OneResponeClinet::CreateWriteCmd(qint8 port, qint8 value)
{
	QByteArray cmd;
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x06));
	cmd.append(char(0x01));
	cmd.append(char(0x06));
	cmd.append(char(0x00));
	cmd.append(char(port));
	cmd.append(char(0x00));
	cmd.append(char(value));
	return cmd;
}

QByteArray OneResponeClinet::CreateReadCmd(qint8 port)
{
	QByteArray cmd;
	//head 
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x00));
	cmd.append(char(0x06));
	cmd.append(char(0x01));
	cmd.append(char(0x03));
	cmd.append(char(0x00));
	cmd.append(port);
	cmd.append(char(0x00));
	cmd.append(char(0x01));
	return cmd;
}

CtrlError OneResponeClinet::SendAndRecv(QByteArray cmd, QByteArray& respond)
{
	if (!m_socket->isOpen())
		return CtrlError(ConnectionPLCError);
	m_socket->write(cmd);
	if (!m_socket->waitForBytesWritten())
		return CtrlError(SocketError);
	if (!m_socket->waitForReadyRead())
		return CtrlError(SocketError);
	while (m_socket->bytesAvailable() > 0)
	{
		respond.append(m_socket->readAll());
		if (respond[respond.size() - 1] != '\n')
			continue;
		else break;
	}
	return CtrlError(NoError);
}

PLCData OneResponeClinet::GetOneCmdDataFromList()
{
	m_cmdMutex.lock();//������
	PLCData cmd = cmdList.front();
	cmdList.pop_front();
	m_cmdMutex.unlock();//������
	return cmd;
}

void OneResponeClinet::PushOneCmdDataToList(PLCData data)
{
	m_cmdMutex.lock();//������
	cmdList.push_back(data);
	m_cmdMutex.unlock();//������
}

void OneResponeClinet::PushOneRespondDataToList(PLCData data)
{
	m_resMutex.lock();//��Ӧ��
	respondList.push_back(data);
	m_resMutex.unlock();//��Ӧ��
}

PLCData OneResponeClinet::GetOneRespondDataFromList()
{
	PLCData respond;
	if (respondList.size()==0)
		return respond;
	m_resMutex.lock();//������
	respond = respondList.front();
	respondList.pop_front();
	m_resMutex.unlock();//������
	return respond;
}

bool OneResponeClinet::WaitDataByIdnetiy(int indetify, int tOut)
{
	QTime t;
	t.start();
	while (1)
	{
		QThread::msleep(10);
		foreach(PLCData data, respondList)
		{
			if (data.indetify == indetify)
			{
				GetOneRespondDataFromList();
				return true;
			}
		}
		if (t.elapsed()>tOut&& tOut!=0)
			return false;
	}
	return true;
}
