#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"
#include "CtrlDebugDlg.h"

#define  SUCCESS_RETURN 0
#define  FAILED_RETURN -1
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
	
    MainWindow(QWidget *parent = Q_NULLPTR);

	virtual void closeEvent(QCloseEvent * event);//�����˳�
	
public slots:
	//actiondebugCtrl
void on_Action_DebugCtrl();
void on_Action_Histroy();
void on_Action_Communication();
void on_Action_DevelopmentDocDlg();
void on_Action_TempDataShow();

void tabwidgetChangeIndx(int index);

void refreshTimeLab();
void refreshLoginLab(QString str);//ˢ�µ�¼״̬
void refreshMachineState(QString str);//ˢ���豸״̬
private:
	int  Init();
	void InitWindow();
	void InitConnection();
	void Others();
	void InitStatueBar();

private:
	QLabel* m_loginLab;
	//QLabel* m_stateLab;
	QLabel* m_timeLab ;
	Ui::MainWindowClass ui;
	CtrlDebugDlg* CtrlDlg;
	QTimer*  stateTimer;
};
