﻿#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QMutex>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include "DataAnalysis.h"
#include "StationMotion.h"
#include <QTime>
#include <QDateTime>
#include "opencv2/opencv.hpp"
#include "Public_Test_Func.h"
#include "CtrlError.h"
#include <atomic>
using namespace std;

struct Result_Data
{
	QString result;
	QString reslutImgPath;
};
Q_DECLARE_METATYPE(Result_Data)
#define ErrorMsg		-1
#define NormalMsg		0
#define WarnningMsg		1

class CParentProcess : public QObject
{
	Q_OBJECT
public:
	enum ProcessState {
		//Process_Status_Unknow,		//未复位状态
		Process_Status_Idle,		//空闲状态 已复位状态 等待启动
		Process_Status_Start,		//流程启动 当触发流程启动后 流程设置成为busy 
		Process_Status_Work,		//运行状态
		Process_Status_Pause,		//暂停
		Process_Status_Continue,	//继续
		Process_Status_Stop,		//停止 通过页面或者IO触发的紧急停止 -> Unknow 需要复位
		Process_Status_End,			//正常退出 ->空闲状态
		Process_Status_ErrorMoveEnd,//异常退出 ->未复位状态 运动异常或者其他异常导致必须重新复位
		Process_Status_ErrorInfoEnd,//异常退出 ->未复位状态 消息异常，可以不用复位
		Process_Status_ErrorEnd,	//异常退出 ->未复位状态 消息异常，可以不用复位
		Process_Status_Test,		//测试使用
	};
	Q_ENUM(ProcessState);

	enum ErrorCode
	{
		NoError = 0,
		Base = 0x10100000,
		Error_Type_Common = Base + 0x001,
		KeyNotFind = Base + 0x002,
		CurrentStateNotAllow = Base + 0x003,
		MoveError = Base + 0x004,//运动错误
		InfoError = Base + 0x005,//消息错误
		ExecuteError = Base + 0x006,//执行错误
		NotFindStepInfo = Base + 0x007,//执行内容错误
		EndStepCodeSuportError = Base + 0x008,//代码支持出错
		OperatorTypeNotContain = Base + 0x009,//没有该类型的操作
		BaseExtend1 = Base + 0x00A,
		FileNotExist = BaseExtend1 + 0x001,//文件不存在
		ParamTypeError = BaseExtend1 + 0x002,
		ProcExtendExist = BaseExtend1 + 0x003,
		TimeOutError = BaseExtend1 + 0x004,
		ParamValueError = BaseExtend1 + 0x005,
		FlagNameNotFind = BaseExtend1 + 0x006,
		ResultNotPass = BaseExtend1 + 0x007,
		ResultError = BaseExtend1 + 0x008,
		ResultFailed = BaseExtend1 + 0x009,
		ParamFormatError = BaseExtend1 + 0x00A,
		BaseExtend2 = Base + 0x00A + 0x00A + 0x001,
		TempKeyNotFind = BaseExtend2 + 0x001,
		ClientSendDataError = BaseExtend2 + 0x002,
		DataNotMatch = BaseExtend2 + 0x003,
		ProcWaitFlowEndExecuteError = BaseExtend2 + 0x004,
		ProcWaitFlowStartExecuteError = BaseExtend2 + 0x005,
		NotFindProcError = BaseExtend2 + 0x006,
		ForceExit = BaseExtend2 + 0x007,
		ClientNotFindError = BaseExtend2 + 0x008,
		ParamLengthNotPatch = BaseExtend2 + 0x009,//参数长度不匹配
		
		UndefineError = Base + 0x100,	//未定义的错误类型
		TestDebugError = Base + 0x100 + 0x001, //测试错误
		ParamNotContain = Base + 0x100 + 0x002, //参数没包含
	};
	Q_ENUM(ErrorCode)


	QString			GetProcName();
	ProcessState	GetProcState();
	int 			Set_Proc_Status(int status);

	void ForceQuit();			//强制退出线程 内部使用
	bool WorkThreadState();			
	void SetAndWaitThreadExit();//外部使用

	//int		GetProc_ExitCode();

	int		Proc_Wrok_Thread();
	bool	Find_Proc(QString proc_name);
	void	Set_Proc(ProcessInfo* pInfo);
	bool	Get_Exception_End();

	CtrlError	ExecStep(int stepIndex);//查找相应步骤执行  步骤执行模块  NoError
	void		StepManager();//步骤管控
	//特殊流程 启动 运行 暂停 停止 结束 要刷新主页面状态 
	void		SpecialProcState(QString procName, ProcessState state);

	//////////////////////////////////////////////////////////////////////////
	//步骤内容解析 包含执行 
	CtrlError ExecStep(StepInfo * step);

	bool ExecStepInfoNotWait(StepInfo * step);
	//////////////////////////////////////////////////////////////////////////
	//操作执行线程
	int ExecOperator(OperatorInfo*op, ProcessState& currentStatus);
	Operator_Type InquireOperatorType(QString typeName);
	int PerformOperation(OperatorInfo*op, Operator_Type type,QString& execMsg);

	bool StepCodeDealwith(int step);//执行步骤 所做处理 补充设计不足 
	//根据m_Proc_Name 名称进行代码补充 
	bool EndStepCodeSuportElement(int stepIndex);
	//不同流程的代码补充操作  当创建了新的流程流程中有消息处理  创建新的消息处理函数进行处理
	//流程完成后代码补充区
	bool EndStepTest(int stepIndex);
	//整机复位
	bool ResetMatchWorkEndCodeSupport(int stepIndex);
	//转盘上下料
	bool RotStationWorkEndCodeSupport(int stepIndex);
	//CAM拍照
	bool CamStationWorkEndCodeSupport(int stepIndex);
	//ARM拍照取放
	bool ArmStationWorkEndCodeSupport(int stepIndex);
	//机械手1工作流程Test 代码处理
	bool Robot1WorkProcEndCodeSupport(int stepIndex);
	//机械手2工作流程Test 代码处理
	bool Robot2WorkProcEndCodeSupport(int stepIndex);
	//放OK料工作流程Test
	bool Robot2WorkProcPutOkEndCodeSupport(int stepIndex);
	//放NG料工作流程Test
	bool Robot2WorkProcPutNGEndCodeSupport(int stepIndex);
	//NG下料工作流程Test
	bool NGDownloadEndCodeSupport(int stepIndex);
	//NG上料工作流程Test
	bool NGUploadEndCodeSupport(int stepIndex);
	//NG移载工作流程Test
	bool NGMoveEndCodeSupport(int stepIndex);

	bool ADF01EndCodeSupport(int stepIndex);
	bool ADF05EndCodeSupport(int stepIndex);
	bool ADF06EndCodeSupport(int stepIndex);
	bool ADF10EndCodeSupport(int stepIndex);
	bool ADF17EndCodeSupport(int stepIndex);
	bool ADF18EndCodeSupport(int stepIndex);

public:
	void SetFatherProc(QString procName);
	void AppendSubProc(QString procName);
	CtrlError	DeleSubProc(QString procName);//删除子流程
	QStringList GetSubProcList();
	void ClearInheritProc();
	void ExitInhreitProc();//退出所有子流程
signals:
	//void DispMsg(QString msg, int type = 0)
	void SendProcMsg(QString msg, int type = 0);
	void SendProMsgWithProfix(QStringList profix, QString msg, int type = 0);
	void StartProcSignal();
	//void SignalExitExceptionFlag(bool bException);
	void SignalExitExceptionFlag(QString msg,int ErrorType= Error_Type_Common);
	void SignalSendImg(QMap<QString, QVariant> Datas);
	void SignalMachineRefresh(QString result);//刷新页面数据
	void SignalProcState(QString procName, CParentProcess::ProcessState state);//发送状态信息

	void SignalProcStart(QString procName);
	void SignalProcIdle(QString procName);
	void SignalProCurrentStep(QString procName, int step);
	void SignalShowInfo(QString info,int type);
	//void SignalMachineRefresh()

public:
	CParentProcess(QObject *parent = nullptr);
	~CParentProcess();
	void ErrorPackage1(CtrlError err, OperatorInfo* op);//方便转发

public slots:
	void StartProc();
	void ExecOperatorSlot(OperatorInfo*op);	//执行操作
	void ExecStepSlot(StepInfo* step);		//执行步骤
	void SetStepIndex(int index);
private:
	void Proc_ShowMesBox(QString title,QString contant,int msgType);

protected:
	ProcessState 	m_Proc_Stat;	//当前设备状态
	ProcessState 	m_Pre_Proc_Stat;//当前设备状态
	QString 		m_Proc_Name;	//初始化时赋予对象流程 名称
	bool			m_bCountnue;	//步骤进行运行
	int 			m_step_index;	//运行步骤记录
	QString			m_curStepNmae;
	QMutex 			m_Status_Mutex;	//流程状态锁

	atomic<bool>	m_bThreadRun;	//线程是否在执行
	QFuture<int>	m_threadHandle;	//线程句柄
	ProcessInfo*	m_pInfo;		//流程中执行的内容
	QString			m_msg;			//内部消息
	bool			m_bFinsh;		//流程结束标志
	bool			m_bException;	//异常结束标志

	//父子流程 是动态创建的 
	QStringList		m_SubProcList;//子流程列表
	QString			m_FatherProc;//父流程
	QMutex			m_Inherit;//继承锁

	//用作计算周期耗时
	QTime m_startTime;//开始时间
	QTime m_endTime;//结束时间	
};
