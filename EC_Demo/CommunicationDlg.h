#pragma once

#include <QDialog>
#include "ui_CommunicationDlg.h"
#include "CommunicationPage.h"
#include <QtCore>
#include <QTcpSocket>
#define theCommunicationDlg  CommunicationDlg::GetInstance()

class CommunicationDlg : public QDialog
{
	Q_OBJECT

public:
	CommunicationDlg(QWidget *parent = Q_NULLPTR);
	~CommunicationDlg();
	bool Init();
	void InitWindow();
	void InitConnection();
	static CommunicationDlg* GetInstance();
	static void DispCtrlDlg();
	static void ReleaseCtrlDlg();

signals:





public slots:
//�ⲿ�۽ӿ�
//void Slot_AddOneClient(QString clientName,QTcpSocket* client);
//void Slot_AddOneServer(QString ServerName,int Port);
//void Slot_AddOneSktInServer(QString SeverName,QString clientName);
//
//void Slot_DeleteOneClient(QString clientName, QTcpSocket* client);
//void Slot_DeleteOneServer(QString ServerName, int Port);
//void Slot_DeleteOneSktInServer(QString SeverName, QString clientName);




private:
	Ui::CommunicationDlg ui;
	static CommunicationDlg* Instance;
	CommunicationPage* CPage;
};
