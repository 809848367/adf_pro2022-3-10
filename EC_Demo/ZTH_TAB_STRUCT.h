#pragma once
#ifndef _ZTH_TABLE_STRUCT_H__ 
#define _ZTH_TABLE_STRUCT_H__
#include <QtCore>

#include "TeachManager.h"

//Robot
//IOControl
//SerialPortManager
//Camera
//Lighting
//Conveyor
//PanaMotor
struct S_Total {
	QString Name;
	QString ModuleName;
	QString TableName;
};



struct S_Robot {
	//	192.168.1.200	8055	8056
	QString IP;
	int CmdPort;
	int StatusPort;
};	   
struct S_IOControl {
	QString Name;
	QString Type;
	int Index;
	QString Src;
	QString SrcType;
	int ID;
};	   
struct S_SerialPortManager {
	QString COM;
	int BaudRate;
	int DataBits;
	int FlowControl;
	int Parity;
	int StopBits;
};	   
struct S_Camera {
	QString SN;

};	   
struct S_Lighting {
	int Index;
	QString ComID;
	int ID;
	QString SerialPortManager;
};	   
struct S_Conveyor {
	QString COM;
	int ID;
	int Vel;
	QString SerialPortManager;
};	   
struct S_PanaMotor {
	QString COM;
	int ID;
	int Vel;
	int Acc;
	int Dec;
	int Org;
	int PursePerRound;
	int EncoderPerRound;
	QString SerialPortManager;
};

#endif