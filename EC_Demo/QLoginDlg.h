#pragma once

#include <QDialog>
#include "ui_QLoginDlg.h"
#include "UserManager.h"
class QLoginDlg : public QDialog
{
	Q_OBJECT

public:
	QLoginDlg(QWidget *parent = Q_NULLPTR);
	~QLoginDlg();
signals:
	void loginSuccess(LOGIN_LEVEL level,QString levName);
public slots:
	void on_loginSolt();
	void on_exitSolt();


private:
	Ui::QLoginDlg ui;
};
