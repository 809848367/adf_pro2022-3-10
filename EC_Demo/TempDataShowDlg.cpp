#include "TempDataShowDlg.h"
#include "CFlageManager.h"
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")
#endif
TempDataShowDlg* TempDataShowDlg::instance = Q_NULLPTR;
TempDataShowDlg::TempDataShowDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	Qt::WindowFlags windowFlag = Qt::Dialog;
	windowFlag |= Qt::WindowMinimizeButtonHint;
	windowFlag |= Qt::WindowMaximizeButtonHint;
	windowFlag |= Qt::WindowCloseButtonHint;
	this->setWindowFlags(windowFlag);
	InitTableWidget();
	connect(ui.RefreshBtn, &QPushButton::clicked, this, &TempDataShowDlg::on_RefreshSlot);
	connect(ui.clearDataBtn, &QPushButton::clicked, this, &TempDataShowDlg::on_CleaerDataSlot);
}

TempDataShowDlg::~TempDataShowDlg()
{
}

TempDataShowDlg* TempDataShowDlg::GetInstance()
{
	if (instance == Q_NULLPTR)
	{
		instance = new TempDataShowDlg;
	}
	return instance;
}

void TempDataShowDlg::DispTempDlg()
{
	GetInstance()->show();
}

void TempDataShowDlg::ReleaseTempDlg()
{
	if (instance == Q_NULLPTR)
		return;
	else
	{
		delete instance;
		instance = Q_NULLPTR;
	}
}

void TempDataShowDlg::InitTableWidget()
{
	

	ui.tableWidget->setColumnCount(2);
	ui.tableWidget->horizontalHeader()->setVisible(true);
	ui.tableWidget->horizontalHeader()->setFixedHeight(30);
	//QList<QString> HItems;
	//HItems << "Name" << "Value" << "";
	//ui->TeachData->setHorizontalHeaderLabels(HItems);
	ui.tableWidget->setHorizontalHeaderLabels(QStringList() << "key" << "value");
	ui.tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.tableWidget->verticalHeader()->setVisible(true);
	//ui.tableWidget->verticalHeader()->setFixedWidth(30);
	ui.tableWidget->setColumnWidth(0, 200);
	//ui->TeachData->setColumnWidth(1, 50);
	ui.tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
	ui.tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
	//ui.tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Fixed);

}

void TempDataShowDlg::on_RefreshSlot()
{
	//ui.tableWidget->clear();
	ui.tableWidget->clear();
	InitTableWidget();
	int rowNum = ui.tableWidget->rowCount();
	for (int i = 0; i < rowNum; i++)//清空列表
	{
		ui.tableWidget->removeRow(0);
	}


	QMap<QString,QString> tempData =  theFlageManager.GetTempData();
	foreach(QString key, tempData.keys())
	{
		//ui.tableWidget->insertite
		int rowCount = ui.tableWidget->rowCount();
		ui.tableWidget->insertRow(rowCount);
		ui.tableWidget->setItem(rowCount,0, new QTableWidgetItem(key));
		ui.tableWidget->setItem(rowCount, 1, new QTableWidgetItem(tempData[key]));
	}
}

void TempDataShowDlg::on_CleaerDataSlot()
{
	ui.tableWidget->clear();
	InitTableWidget();
	int rowNum = ui.tableWidget->rowCount();
	for (int i = 0; i < rowNum; i++)//清空列表
	{
		ui.tableWidget->removeRow(0);
	}

}
