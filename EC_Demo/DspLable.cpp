#include "DspLable.h"
#include <QDebug>
#include <QDesktopServices>
#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 
DspLable::DspLable(QWidget *parent)
	: QLabel(parent)
{
	ui.setupUi(this);
	
	this->setScaledContents(true);
	m_filePath = "";
	m_infoLab = new QLabel("EMPTY");
	m_infoLab->setAlignment(Qt::AlignCenter);
	m_infoLab->show();
}

DspLable::~DspLable()
{
}

void DspLable::resizeEvent(QResizeEvent *event)
{
	//当图片大小改变要调用该方法使其铺满...
}

QString DspLable::labelName()
{
	return m_name;
}

void DspLable::mouseDoubleClickEvent(QMouseEvent *event)
{
	systemToolDispImage(m_filePath);
	qDebug() << "DspLable mouseDoubleClickEvent ";
}

void DspLable::setPixmap(const QPixmap &pixmap)
{
	pixmap.scaled(this->size(), Qt::KeepAspectRatio);
	QLabel::setPixmap(pixmap);
}

void DspLable::setImg(QImage *img)
{
	if (img == NULL)
		return;
	this->setPixmap(QPixmap::fromImage(*img));
}

void DspLable::setPicPath(QString picPath)
{
	m_filePath = picPath;
}

void DspLable::setDispInfo(QString info)
{
	m_infoLab->setText(info);
	QPalette pa;
    if (info.contains("PASS"))
	{
		//绿色
		pa.setColor(QPalette::WindowText, Qt::darkGreen);
	}
	else
	{
		//红色
		pa.setColor(QPalette::WindowText, Qt::red);
	}
	m_infoLab->setPalette(pa);
	//m_infoLab->show();
}

QLabel* DspLable::getInfoLab()
{
	return m_infoLab;
}

void DspLable::systemToolDispImage(QString picPath)
{
	//QProcess *proc = new QProcess();
	//QString program = "cmd.exe";
	//QStringList para;
	//para << "/c";
	//para << QString("rundll32") + QString("%Systemroot%\\System32\\shimgvw.dll,ImageView_Fullscreen");
	//para << QString("C://Users//sfai//Desktop//pic//4.jpg");
	////para << QString(" C:\\Users\\Public\\Pictures\\Sample Pictures\\Chrysanthemum.jpg");
	////("C://Users//sfai//Desktop//pic//4.jpg");
	//proc->start(program, para);

	if (picPath.isEmpty())
		return;
	QDesktopServices::openUrl(QUrl::fromLocalFile(picPath));
	//QDesktopServices::openUrl(QUrl::fromLocalFile(QString("C://Users//sfai//Desktop//pic//4.jpg")));
}
