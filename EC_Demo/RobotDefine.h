﻿#ifndef ROBOTDEFINE_H
#define ROBOTDEFINE_H
#include <QtCore>

struct RobotStatus
{
    quint32 Message_Size;       //数据包长度，包含当前字段
    quint64 timestamp;          //时间戳,1970 年1 月1 日至今的毫秒数
    quint8 autorun_cycelMode;   //循环模式，0：单步，1：单循环，2：连续
    double machinePos[8];       //关节角度，单位度
    double machinePose[6];      //直角坐标，前三项单位毫米，后三项单位弧度(已自动转为角度）
    double machineUserPose[6];  //当前用户坐标，前三项单位毫米，后三项单位弧度(已自动转为角度）
    double torque[8];           //关节额定力矩百分比，单位%，
    qint32 robotState;          //机器人状态：0：停止，1：暂停，2：急停，3：运行，4：报警
    qint32 servoReady;          //抱闸状态：0：未打开，1：已打开。
    qint32 can_motor_run;       //同步状态：0：未同步，1：同步
    qint32 motor_speed[8];      //电机速度，单位：转/分
    qint32 robotMode;           //机器人模式：0：示教模式，1：自动模式，2：远程模式
    double analog_ioInput[3];   //模拟量输入口数据，单位V
    double analog_ioOutput[5];  //模拟量输出口数据，单位V
    quint64 digital_ioInputs;   //数字量输入口数据
    bool digital_ioInput[64];   //数字量输入口数据
    quint64 digital_ioOutputs;  //数字量输出口数据
    bool digital_ioOutput[64];  //数字量输出口数据
    quint8 collisionv;          //碰撞报警状态，0：非碰撞报警状态，1：碰撞报警状态
};
Q_DECLARE_METATYPE(RobotStatus)
struct RobotPoint
{
    QVector<double> Pose = QVector<double>(6,0);
    QVector<double> Pos = QVector<double>(8,0);
    double speed = 20;
    int moveType = 0;
    int smooth = 0;
};
Q_DECLARE_METATYPE(RobotPoint)
Q_DECLARE_METATYPE(QVector<RobotPoint>)
#endif // ROBOTDEFINE_H
