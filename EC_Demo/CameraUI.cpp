#include "CameraUI.h"
#include "ui_CameraUI.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
CameraUI::CameraUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CameraUI)
{
    ui->setupUi(this);
    InitTeachDataList();
    connect(&TeachDataBtns,SIGNAL(buttonClicked(int)),this,SLOT(TeachDataBtns_clicked(int)));
    connect(&RefreshTimer,SIGNAL(timeout()),this,SLOT(RefreshTimerSlot()));
    RefreshTimer.start(100);
}

CameraUI::~CameraUI()
{
    delete ui;
}

void CameraUI::SetHardwareManager(HardwareManager *HWM, QString Station)
{
    this->HWM = HWM;
    this->Station = Station;
}

void CameraUI::InitTeachDataList()
{
    ui->TeachData->setColumnCount(3);
    ui->TeachData->horizontalHeader()->setVisible(true);
    ui->TeachData->horizontalHeader()->setFixedHeight(30);
    QList<QString> HItems;
    HItems<<"Name"<<"Value"<<"";
    ui->TeachData->setHorizontalHeaderLabels(HItems);
    ui->TeachData->verticalHeader()->setVisible(true);
    ui->TeachData->verticalHeader()->setFixedWidth(30);
    ui->TeachData->setColumnWidth(0,100);
    ui->TeachData->setColumnWidth(2,50);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    ui->TeachData->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);
}

void CameraUI::ShowTeachData()
{
    ShowTeachDataFlag = true;
    while(TeachDataBtns.buttons().size() > 0) TeachDataBtns.removeButton(TeachDataBtns.buttons()[0]);
    ui->TeachData->clearContents();
    QList<QPair<QString, CameraParam>> Params = TeachData.GetParams();
    ui->TeachData->setRowCount(Params.size()+1);
    for(int i=0;i<Params.size();i++)
    {
        ui->TeachData->setVerticalHeaderItem(i,new QTableWidgetItem(QString::number(i)));
        ui->TeachData->setItem(i,0,new QTableWidgetItem(Params[i].first));
        ui->TeachData->item(i,0)->setFlags(ui->TeachData->item(i,0)->flags() & ~Qt::ItemIsEditable);
        QString tmp = QString("%1,%2,%3").arg(Params[i].second.Exposure).arg(Params[i].second.Gamma).arg(Params[i].second.Gain);
        ui->TeachData->setItem(i,1,new QTableWidgetItem(tmp));
        QPushButton *TeachDataBtn = new QPushButton();
        TeachDataBtn->setText("Now");
        TeachDataBtns.addButton(TeachDataBtn,i);
        ui->TeachData->setCellWidget(i,2,TeachDataBtn);
    }
    ui->TeachData->setVerticalHeaderItem(Params.size(),new QTableWidgetItem());
    for(int j=0;j<ui->TeachData->columnCount();j++)
    {
        ui->TeachData->setItem(Params.size(),j,new QTableWidgetItem());
        ui->TeachData->item(Params.size(),j)->setBackgroundColor(QColor(230,230,230));
        ui->TeachData->item(Params.size(),j)->setFlags(ui->TeachData->item(Params.size(),j)->flags() & ~Qt::ItemIsEditable);
    }
    ShowTeachDataFlag = false;
}



void CameraUI::on_Open_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Open";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void CameraUI::on_Close_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "Close";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void CameraUI::on_GetImage_clicked()
{
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetImage";
    Send.Identfication = 100;
    Send.Datas.insert("Exposure",QVariant::fromValue(ui->Exposure->value()));
    Send.Datas.insert("Gamma",QVariant::fromValue(ui->Gamma->value()));
    Send.Datas.insert("Gain",QVariant::fromValue(ui->Gain->value()));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Image") || !Recv.Datas["Image"].canConvert<cv::Mat>()) return;
    cv::Mat Image = Recv.Datas["Image"].value<cv::Mat>();
    ui->IMShow->SetImage(Image);
    return;
}

void CameraUI::on_SaveImage_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this,"Save Image",QString(),"png(*.png);;bmp(*.bmp)");
    if(filename.isEmpty())return;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetImageAndSave";
    Send.Identfication = 100;
    Send.Datas.insert("Path",QVariant::fromValue(filename));
    Send.Datas.insert("Exposure",QVariant::fromValue(ui->Exposure->value()));
    Send.Datas.insert("Gamma",QVariant::fromValue(ui->Gamma->value()));
    Send.Datas.insert("Gain",QVariant::fromValue(ui->Gain->value()));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void CameraUI::RefreshTimerSlot()
{
    if(!isVisible()) return;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetStatus";
    Send.Identfication = 100;
    ret = HWM->Execute(Send,Recv);
	if (ret != 0) return;
	//QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Status") || !Recv.Datas["Status"].canConvert<int>()) return;
    int Status = Recv.Datas["Status"].value<int>();
    ui->Open->setEnabled(Status == 0);
    ui->Close->setEnabled(Status == 1);
    ui->GetImage->setEnabled(Status == 1);
    ui->SaveImage->setEnabled(Status == 1);
    ui->Exposure->setEnabled(Status == 1);
    ui->Gamma->setEnabled(Status == 1);
    ui->Gain->setEnabled(Status == 1);
    return;
}

void CameraUI::RefreshTeachData(QString Station, TeachData_Camera TeachData)
{
    if(this->Station != Station) return;
    this->TeachData = TeachData;
    ShowTeachData();
}

void CameraUI::TeachDataBtns_clicked(int n)
{
    CameraParam Param;
    Param.Exposure = ui->Exposure->value();
    Param.Gamma = ui->Gamma->value();
    Param.Gain = ui->Gain->value();
    TeachData.SetParam(ui->TeachData->item(n,0)->text(),Param);
    UpDateTeachData(Station,TeachData);
}

void CameraUI::on_Teach_Add_clicked()
{
    QString Name = QInputDialog::getText(this,"Add","Input Add Name");
    if(Name.isEmpty()) return;
    TeachData.SetParam(Name,CameraParam());
    UpDateTeachData(Station,TeachData);
}

void CameraUI::on_Teach_Del_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    if(QMessageBox::warning(this,"Warning",QString("Delete %1 ?").arg(Name),QMessageBox::Ok,QMessageBox::Cancel) != QMessageBox::Ok) return;
    TeachData.RemoveParam(Name);
    UpDateTeachData(Station,TeachData);
}

void CameraUI::on_Teach_Get_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    CameraParam tParam;
    if(TeachData.GetParam(Name,tParam) != 0) return;

    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetImage2";
    Send.Identfication = 100;
    Send.Datas.insert("Param",QVariant::fromValue(tParam));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    if(!Recv.Datas.contains("Image") || !Recv.Datas["Image"].canConvert<cv::Mat>()) return;
    cv::Mat Image = Recv.Datas["Image"].value<cv::Mat>();
    ui->IMShow->SetImage(Image);
    return;
}

void CameraUI::on_Teach_Save_clicked()
{
    int N = ui->TeachData->currentRow();
    if(N<0 || N >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(N,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;
    CameraParam tParam;
    if(TeachData.GetParam(Name,tParam) != 0) return;

    QString filename = QFileDialog::getSaveFileName(this,"Save Image",QString(),"png(*.png);;bmp(*.bmp)");
    if(filename.isEmpty())return;
    CtrlError ret;
    if(HWM == nullptr) return;
    HardwareCommData Send,Recv;
    Send.Station = Station;
    Send.OperatorName = "GetImageAndSave2";
    Send.Identfication = 100;
    Send.Datas.insert("Path",QVariant::fromValue(filename));
    Send.Datas.insert("Param",QVariant::fromValue(tParam));
    ret = HWM->Execute(Send,Recv);
    if(ret != 0) QMessageBox::critical(this,"Error",ret);
    return;
}

void CameraUI::on_TeachData_cellChanged(int row, int column)
{
    if(ShowTeachDataFlag) return;
    if(column == 0) return;
    if(row<0 || row >= ui->TeachData->rowCount()-1) return;
    QTableWidgetItem *item = ui->TeachData->item(row,0);
    if(!item) return;
    QString Name = item->text();
    if(Name.isEmpty()) return;

    CameraParam tParam;
    item = ui->TeachData->item(row,1);
    if(!item) return;
    QList<QString> tmp = item->text().split(",");
    if(tmp.size() != 3) return;
    bool isOK;
    tParam.Exposure = tmp[0].toDouble(&isOK);
    if(!isOK) return;
    tParam.Gamma = tmp[1].toDouble(&isOK);
    if(!isOK) return;
    tParam.Gain = tmp[2].toDouble(&isOK);
    if(!isOK) return;
    TeachData.SetParam(Name,tParam);
    UpDateTeachData(Station,TeachData);
}
