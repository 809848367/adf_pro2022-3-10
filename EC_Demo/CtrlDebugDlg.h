#pragma once

#include <QDialog>
#include "ui_CtrlDebugDlg.h"
#include <QTabWidget>

class CtrlDebugDlg : public QDialog
{
	Q_OBJECT

public:
	CtrlDebugDlg(QWidget *parent = Q_NULLPTR);
	~CtrlDebugDlg();
	bool Init();
	void InitWindow();
	void InitConnection();
	static CtrlDebugDlg* GetInstance();

	static void DispCtrlDlg();
	static void ReleaseCtrlDlg();

private:
	Ui::CtrlDebugDlg ui;
	QTabWidget* mainWidget;
	static CtrlDebugDlg * m_pInstance;
};
