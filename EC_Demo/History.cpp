#include "History.h"

History::History(QObject *parent) : QObject(parent)
{

}

History &History::GetInstance()
{
    static History Instance;
    return Instance;
}

CtrlError History::Init(QString StationName, QString Path)
{
    CtrlError ret;
    this->StationName = StationName;
    if (QSqlDatabase::contains("HistoryDB"))
    {
        Database = QSqlDatabase::database("HistoryDB");
    }
    else
    {
        Database = QSqlDatabase::addDatabase("QSQLITE","HistoryDB");
        Database.setDatabaseName(Path);
        if(!Database.open()) return CtrlError(OpenDBFail,StationName);
    }
    ret = CreateDataBase();
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError History::CreateDataBase()
{
    QString ResultCmd = "CREATE TABLE if Not Exists HistoryResult (         \
            SN      VARCHAR (256)   NOT NULL                                \
                                    PRIMARY KEY                             \
                                    UNIQUE,                                 \
            BarCode VARCHAR (256),                                          \
            Date    DATETIME        NOT NULL,                               \
            Result  VARCHAR (1024),                                         \
            Surface VARCHAR (1024)                                          \
            );";
    QString SurfaceCmd = "CREATE TABLE if Not Exists HistorySurface (       \
            ID      INTEGER         PRIMARY KEY AUTOINCREMENT               \
                                    UNIQUE                                  \
                                    NOT NULL,                               \
            SN      VARCHAR (256)   NOT NULL,                               \
            Surface VARCHAR (256)   NOT NULL,                               \
            Files   VARCHAR (1024),                                         \
            Datas   VARCHAR (1024)                                          \
            );";
    QString FileListCmd = "CREATE TABLE if Not Exists HistoryFileList (     \
            ID      INTEGER         PRIMARY KEY AUTOINCREMENT               \
                                    UNIQUE                                  \
                                    NOT NULL,                               \
            Path    VARCHAR (256)   NOT NULL                                \
            );";
    QSqlQuery Query(Database);
    Query.prepare(ResultCmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(CreateDBError,StationName);
    }
    Query.clear();
    Query.prepare(SurfaceCmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(CreateDBError,StationName);
    }
    Query.clear();
    Query.prepare(FileListCmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(CreateDBError,StationName);
    }
    return CtrlError(NoError);
}

CtrlError History::SetDataInDB(QString SN, QString BarCode, QMap<QString, AlgoCommData> Datas, QMap<QString, QString> FilePaths)
{
    CtrlError ret;
    ret = BEGIN_TRANSACTION();
    if(ret != NoError) return ret;
    QList<QString> Keys;
    QList<QString> Data;
    foreach(QString Key,FilePaths.keys())
    {
        Keys.clear();
        Data.clear();
        Keys<<"Path";
        Data<<FilePaths[Key];
        ret = InsertValue("HistoryFileList",Keys,Data);
        if(ret != NoError)
        {
            CtrlError ret2 = ROLLBACK_TRANSACTION();
            if(ret2 != NoError) return ret2;
            return ret;
        }
        int id;
        ret = GetLastInsertRowid("HistoryFileList",id);
        if(ret != NoError)
        {
            CtrlError ret2 = ROLLBACK_TRANSACTION();
            if(ret2 != NoError) return ret2;
            return ret;
        }
        FilePaths[Key] = QString::number(id);
    }
    QString ResultAll = "PASS";
    QMap<QString,int> SurfaceID;
    foreach(QString Surface,Datas.keys())
    {
        Keys.clear();
        Data.clear();
        Keys.push_back("SN");
        Data.push_back(SN);
        Keys.push_back("Surface");
        Data.push_back(Surface);
        QString Files;
        QString DataStr;
        foreach(QString Key,Datas[Surface].Data.keys())
        {
            QString OneData = Datas[Surface].Data[Key];
            if(OneData.contains("/") && FilePaths.contains(OneData.right(OneData.size()-OneData.lastIndexOf("/")-1)))
            {
                Files.append(QString("%1,%2;").arg(Key).arg(FilePaths[OneData.right(OneData.size()-OneData.lastIndexOf("/")-1)]));
            }
            else
            {
                DataStr.append(QString("%1,%2;").arg(Key).arg(OneData));
            }
        }
        if(!Files.isEmpty()) Files.remove(Files.size()-1,1);
        if(!DataStr.isEmpty()) DataStr.remove(DataStr.size()-1,1);
        Keys.push_back("Files");
        Data.push_back(Files);
        Keys.push_back("Datas");
        Data.push_back(DataStr);
        ret = InsertValue("HistorySurface",Keys,Data);
        if(ret != NoError)
        {
            CtrlError ret2 = ROLLBACK_TRANSACTION();
            if(ret2 != NoError) return ret2;
            return ret;
        }
        int id;
        ret = GetLastInsertRowid("HistorySurface",id);
        if(ret != NoError)
        {
            CtrlError ret2 = ROLLBACK_TRANSACTION();
            if(ret2 != NoError) return ret2;
            return ret;
        }
        SurfaceID.insert(Surface,id);
        if(!Datas[Surface].Data.contains("Result") || Datas[Surface].Data["Result"] != "PASS")
        {
            ResultAll = "FAIL";
        }
    }
    Keys.clear();
    Data.clear();
    Keys.push_back("SN");
    Data.push_back(SN);
    Keys.push_back("BarCode");
    Data.push_back(BarCode);
    Keys.push_back("Result");
    Data.push_back(ResultAll);
    QString SurfaceStr;
    foreach (QString Key, SurfaceID.keys())
    {
        SurfaceStr.append(QString("%1,%2;").arg(Key).arg(SurfaceID[Key]));
    }
    if(SurfaceStr.size() > 0) SurfaceStr.remove(SurfaceStr.size()-1,1);
    Keys.push_back("Surface");
    Data.push_back(SurfaceStr);
    Keys.push_back("Date");
    Data.push_back(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
    ret = InsertValue("HistoryResult",Keys,Data);
    if(ret != NoError)
    {
        CtrlError ret2 = ROLLBACK_TRANSACTION();
        if(ret2 != NoError) return ret2;
        return ret;
    }
    ret = COMMIT();
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError History::SelectBySN(QString SN, int Flag, QVector<HistoryResult> &Results)
{
    QString Cmd = QString("Select SN,BarCode,Date,Result,Surface from HistoryResult where SN like '%1%2%3';").arg((Flag == 0 || Flag ==2)?"%":"").arg(SN).arg((Flag == 0 || Flag ==1)?"%":"");
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(SelectDataError,StationName);
    }
    return GetSelectResult(Query,Results);
}

CtrlError History::SelectByBarCode(QString BarCode, int Flag, QVector<HistoryResult> &Results)
{
    QString Cmd = QString("Select SN,BarCode,Date,Result,Surface from HistoryResult where BarCode like '%1%2%3';").arg((Flag == 0 || Flag ==2)?"%":"").arg(BarCode).arg((Flag == 0 || Flag ==1)?"%":"");
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(SelectDataError,StationName);
    }
    return GetSelectResult(Query,Results);
}

CtrlError History::SelectByDateTime(QDateTime Start, QDateTime Stop, QVector<HistoryResult> &Results)
{
    QString Cmd = QString("Select SN,BarCode,Date,Result,Surface from HistoryResult where Date > '%1' and Date < '%2';").arg(Start.toString("yyyy-MM-dd hh:mm:ss")).arg(Stop.toString("yyyy-MM-dd hh:mm:ss"));
    qDebug()<<Cmd;
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(SelectDataError,StationName);
    }
    return GetSelectResult(Query,Results);
}

CtrlError History::InsertValue(QString Table, QList<QString> Keys, QList<QString> Data)
{
    if(Keys.size() == 0 || Keys.size() != Data.size()) return CtrlError(InsertDataNotRigth,StationName);
    QString Cmd = QString("INSERT INTO %1 (").arg(Table);
    for(int j=0;j<Keys.size();j++)
    {
        Cmd.append(Keys[j]+",");
    }
    Cmd.remove(Cmd.size()-1,1);
    Cmd.append(") VALUES (");
    for(int i=0;i<Data.size();i++)
    {
        Cmd.append("'" + Data[i]+"',");
    }
    Cmd.remove(Cmd.size()-1,1);
    Cmd.append(");");
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(InsertDataError,StationName);
    }
    return CtrlError(NoError);
}

CtrlError History::GetLastInsertRowid(QString Table,int &id)
{
    QString Cmd = QString("SELECT last_insert_rowid() from %1").arg(Table);
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(GetRowidError,StationName);
    }
    Query.next();
    id = Query.value(0).toInt();
    return CtrlError(NoError);

}

CtrlError History::BEGIN_TRANSACTION()
{
    QString Cmd;
    Cmd = "BEGIN TRANSACTION";
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(TRANSACTIONError,StationName);
    }
    return CtrlError(NoError);
}

CtrlError History::ROLLBACK_TRANSACTION()
{
    QString Cmd;
    Cmd = "ROLLBACK TRANSACTION";
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(TRANSACTIONError,StationName);
    }
    return CtrlError(NoError);
}

CtrlError History::COMMIT()
{
    QString Cmd;
    Cmd = "COMMIT";
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(TRANSACTIONError,StationName);
    }
    return CtrlError(NoError);
}

CtrlError History::GetSelectResult(QSqlQuery Query,QVector<HistoryResult> &Results)
{
    CtrlError ret;
    while(Query.next())
    {
        HistoryResult tResult;
        tResult.SN = Query.value(0).toString();
        tResult.BarCode = Query.value(1).toString();
        tResult.DateTime = Query.value(2).toDateTime();
        tResult.Result = Query.value(3).toString();
        QString Tmp;
        Tmp = Query.value(4).toString();
        foreach(QString Data,Tmp.split(";"))
        {
            if(Data.isEmpty() || !Data.contains(",")) continue;
            QString Key = Data.left(Data.indexOf(","));
            int ID = Data.right(Data.size()-Data.lastIndexOf(",")-1).toInt();
            HistorySurface Surface;
            ret = GetSurfaceData(ID,Surface);
            if(ret != NoError) return ret;
            tResult.Surfaces.insert(Key,Surface);
        }
        Results.push_back(tResult);
    }
    return CtrlError(NoError);
}

CtrlError History::GetSurfaceData(int ID,HistorySurface &Surface)
{
    QString Cmd;
    Cmd = QString("Select SN,Surface,Files,Datas from HistorySurface where ID = %1;").arg(ID);
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec() || !Query.next())
    {
        qDebug()<<Query.lastError();
        return CtrlError(SelectDataError,StationName);
    }

    Surface.SN = Query.value(0).toString();
    Surface.Surface = Query.value(1).toString();
    CtrlError ret;
    QString Tmp;
    Tmp = Query.value(2).toString();
    foreach (QString Data,Tmp.split(";"))
    {
        if(Data.isEmpty() || !Data.contains(",")) continue;
        QString Key = Data.left(Data.indexOf(","));
        int ID = Data.right(Data.size()-Data.lastIndexOf(",")-1).toInt();
        QString Path;
        ret = GetFileData(ID,Path);
        if(ret != NoError) return ret;
        Surface.Files.insert(Key,Path);
    }
    Tmp = Query.value(3).toString();
    foreach (QString Data,Tmp.split(";"))
    {
        if(Data.isEmpty() || !Data.contains(",")) continue;
        QString Key = Data.left(Data.indexOf(","));
        QString Value = Data.right(Data.size()-Data.lastIndexOf(",")-1);
        Surface.Datas.insert(Key,Value);
    }
    return CtrlError(NoError);
}

CtrlError History::GetFileData(int ID, QString &Path)
{
    QString Cmd;
    Cmd = QString("Select Path from HistoryFileList where ID = %1;").arg(ID);
    QSqlQuery Query(Database);
    Query.prepare(Cmd);
    if(!Query.exec() || !Query.next())
    {
        qDebug()<<Query.lastError();
        return CtrlError(SelectDataError,StationName);
    }
    Path = Query.value(0).toString();
    return CtrlError(NoError);
}
