#pragma once

#include <QObject>
#include "HardwareModule.h"
#include <QTcpSocket>
#include <QtCore>
#include "OneResponeClinet.h"

class PLC : public HardwareModule
{
	Q_OBJECT
public:
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x00100000,
		ConnectionPLCError = Base + 0x001,
		SignalTypeNotExit = Base + 0x002,
		RecvDataError = Base + 0x003,
		ModuleNameError = Base + 0x004,
		ModuleParaKeyNotContain = Base + 0x005,
		ModuleParaCmdError = Base + 0x006,
		ModuleParaFuncNotContain = Base + 0x007,
		ExecCmdError = Base + 0x008,
		InitDataErr = Base + 0x009,
		ExecuteDataNotRight = Base + 0x00a,
		ClientHasOpen = Base + 0x00b,
		ClientOpenFail = Base + 0x00c,
		ClientWriteError = Base + 0x00d,
		ClientReadTimeOut = Base + 0x00e,
	};
	Q_ENUM(ErrorCode)
public:
	enum PLC_Signal
	{
		PLC_Signal_Soft_Running = 10,
		PLC_Signal_CCD_Camera=12,
		PLC_Signal_CCD_Camera_Complete = 14,
		PLC_Signal_CCD_Algo_Result = 16,
	};
	Q_ENUM(PLC_Signal)
	PLC(QObject *parent = nullptr);
	~PLC();
	CtrlError Init(HardwareInitData InitData) Q_DECL_OVERRIDE;
	CtrlError Execute(HardwareCommData ExecuteData, HardwareCommData& ResultData)Q_DECL_OVERRIDE;
private:
	CtrlError ExecutePLC(PLCData& cmd);// cmd  
	CtrlError Init(QString name, QString ip, int port);
private slots:
	void HeartBeatSlot();
	void stateChanged(QAbstractSocket::SocketState state);
private:
	OneResponeClinet* m_client;

	QTimer HeartBeat;
	QString name;
	QString moduleType = "PLC";
	QMutex TcpMutex;
	const uint timeOut = 1000;
	QThread* m_thread;
	QAtomicInteger<int>  ID = 0;
	QMutex m_SendMutex;
	QMutex m_RecvMutex;
	QMap<int, QByteArray> SendBuffer;
	QMap<int, QByteArray> RecvBuffer;
	bool m_bRun = true;
	QString ip;
	int port;
	QMutex m_Mutex;
};
