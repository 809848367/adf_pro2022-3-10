#ifndef HISTORY_H
#define HISTORY_H

#include <QtCore>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "CtrlError.h"
#include "AlgoCommData.h"
#include "HistoryDefine.h"

class History : public QObject
{
    Q_OBJECT
    explicit History(QObject *parent = nullptr);
public:
    static History &GetInstance();
    CtrlError Init(QString StationName,QString Path);
    CtrlError CreateDataBase();
    CtrlError SetDataInDB(QString SN, QString BarCode,QMap<QString,AlgoCommData> Datas, QMap<QString,QString> FilePaths);
    CtrlError SelectBySN(QString SN, int Flag, QVector<HistoryResult> &Results);
    CtrlError SelectByBarCode(QString BarCode, int Flag, QVector<HistoryResult> &Results);
    CtrlError SelectByDateTime(QDateTime Start, QDateTime Stop, QVector<HistoryResult> &Results);
private:
    QSqlDatabase Database;
    QString StationName;
    CtrlError InsertValue(QString Table, QList<QString> Keys, QList<QString> Data);
    CtrlError GetLastInsertRowid(QString Table,int &id);
    CtrlError BEGIN_TRANSACTION();
    CtrlError ROLLBACK_TRANSACTION();
    CtrlError COMMIT();
    CtrlError GetSelectResult(QSqlQuery Query, QVector<HistoryResult> &Results);
    CtrlError GetSurfaceData(int ID, HistorySurface &Surface);
    CtrlError GetFileData(int ID, QString &Path);
signals:

public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00700000,
        OpenDBFail               = Base + 0x001,
        CreateDBError            = Base + 0x002,
        InsertDataNotRigth       = Base + 0x003,
        InsertDataError          = Base + 0x004,
        GetRowidError            = Base + 0x005,
        WriteDBError             = Base + 0x006,
        TRANSACTIONError         = Base + 0x007,
        SelectDataError          = Base + 0x008,
    };
    Q_ENUM(ErrorCode)
};

#endif // HISTORY_H
