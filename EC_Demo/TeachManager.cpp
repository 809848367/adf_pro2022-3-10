#include "TeachManager.h"

TeachManager::TeachManager(QObject *parent) : QObject(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    StationName = "TeachManager";
    connect(this,SIGNAL(UpDateTable(TeachTable)),this,SLOT(UpDateTableSlot(TeachTable)));
}

TeachManager &TeachManager::GetInstance()
{
    static TeachManager Instance;
    return Instance;
}

CtrlError TeachManager::InitDB(QString Path)
{
    if (QSqlDatabase::contains("TeachDataDB"))
    {
        Database = QSqlDatabase::database("TeachDataDB");
    }
    else
    {
        Database = QSqlDatabase::addDatabase("QSQLITE","TeachDataDB");
        Database.setDatabaseName(Path);
        if(!Database.open()) return CtrlError(OpenDBFail,StationName);
    }
    QVector<QString> SumKeys;
    QVector<QVector<QString>> SumDatas;
    ReadDataBase("Summary", SumKeys, SumDatas);
    for(int i=0;i<SumDatas.size();i++)
    {
        QString Station;
        QString Type;
        QString TableName;
        for(int j=0;j<SumKeys.size();j++)
        {
            if(SumKeys[j] == "Station") Station = SumDatas[i][j];
            if(SumKeys[j] == "Type")    Type = SumDatas[i][j];
            if(SumKeys[j] == "Table")   TableName = SumDatas[i][j];
        }
        TeachTable Table;
        Table.Name = Station;
        Table.Type = Type;
        ReadDataBase(TableName,Table.Keys,Table.Data);
        SetTable(Table);
    }
    return CtrlError(NoError);
}

CtrlError TeachManager::SetTable(TeachTable Table)
{
    QVector<QString> Keys;
    QVector<QVector<QVariant>> Data;
    QSharedPointer<TeachData> Teach;
    if(Table.Type == "Robot")
    {
        for(int i=0;i<Table.Data.size();i++)
        {
            QVector<QVariant> tData;
            for(int j=0;j<Table.Data[i].size();j++)
            {
                bool isOK;
                if(Table.Keys[j] == "Name")
                {
                    if(i == 0) Keys.push_back(Table.Keys[j]);
                    tData.push_back(QVariant::fromValue(Table.Data[i][j]));
                }
                if(Table.Keys[j] == "X" || Table.Keys[j] == "Y" || Table.Keys[j] == "Z" || Table.Keys[j] == "RX" || Table.Keys[j] == "RY" ||
                        Table.Keys[j] == "RZ" || Table.Keys[j] == "speed" || Table.Keys[j] == "moveType" || Table.Keys[j] == "smooth" ||
                        Table.Keys[j] == "Pos1" || Table.Keys[j] == "Pos2" || Table.Keys[j] == "Pos3" || Table.Keys[j] == "Pos4" ||
                        Table.Keys[j] == "Pos5" || Table.Keys[j] == "Pos6" || Table.Keys[j] == "Pos7" || Table.Keys[j] == "Pos8")
                {
                    if(i == 0) Keys.push_back(Table.Keys[j]);
                    tData.push_back(QVariant::fromValue(Table.Data[i][j].toDouble(&isOK)));
                    if(!isOK) return CtrlError(TeachTableFormatWrong,StationName);
                }
            }
            Data.push_back(tData);
        }
        Teach = QSharedPointer<TeachData>(new TeachData_Robot());
    }
    if(Table.Type == "Lighting")
    {
        for(int i=0;i<Table.Data.size();i++)
        {
            QVector<QVariant> tData;
            for(int j=0;j<Table.Data[i].size();j++)
            {
                bool isOK;
                if(Table.Keys[j] == "Name" || Table.Keys[j] == "Value")
                {
                    if(i == 0) Keys.push_back(Table.Keys[j]);
                    tData.push_back(QVariant::fromValue(Table.Data[i][j]));
                }
            }
            Data.push_back(tData);
        }
        Teach = QSharedPointer<TeachData>(new TeachData_Lighting());
    }
    if(Table.Type == "PanaMotor")
    {
        for(int i=0;i<Table.Data.size();i++)
        {
            QVector<QVariant> tData;
            for(int j=0;j<Table.Data[i].size();j++)
            {
                bool isOK;
                if(Table.Keys[j] == "Name")
                {
                    if(i == 0) Keys.push_back(Table.Keys[j]);
                    tData.push_back(QVariant::fromValue(Table.Data[i][j]));
                }
                if(Table.Keys[j] == "Value")
                {
                    if(i == 0) Keys.push_back(Table.Keys[j]);
                    tData.push_back(QVariant::fromValue(Table.Data[i][j].toDouble(&isOK)));
                    if(!isOK) return CtrlError(TeachTableFormatWrong,StationName);
                }
            }
            Data.push_back(tData);
        }
        Teach = QSharedPointer<TeachData>(new TeachData_PanaMotor());
    }
    if(Table.Type == "Camera")
    {
        for(int i=0;i<Table.Data.size();i++)
        {
            QVector<QVariant> tData;
            for(int j=0;j<Table.Data[i].size();j++)
            {
                bool isOK;
                if(Table.Keys[j] == "Name" || Table.Keys[j] == "Value")
                {
                    if(i == 0) Keys.push_back(Table.Keys[j]);
                    tData.push_back(QVariant::fromValue(Table.Data[i][j]));
                }
            }
            Data.push_back(tData);
        }
        Teach = QSharedPointer<TeachData>(new TeachData_Camera());
    }
    if(Teach->FromTable(Keys,Data) != 0) return CtrlError(TeachTableFormatWrong,StationName);
    TeachLock.lock();
    if(TeachDatas.contains(Table.Name)) TeachDatas.remove(Table.Name);
    TeachDatas.insert(Table.Name,QPair<QString,QSharedPointer<TeachData>>(Table.Type,Teach));
    TeachLock.unlock();
    if(Table.Type == "Robot")       emit RefreshTeachData(Table.Name,*qobject_cast<TeachData_Robot*>(Teach));
    if(Table.Type == "Lighting")    emit RefreshTeachData(Table.Name,*qobject_cast<TeachData_Lighting*>(Teach));
    if(Table.Type == "PanaMotor")   emit RefreshTeachData(Table.Name,*qobject_cast<TeachData_PanaMotor*>(Teach));
    if(Table.Type == "Camera")      emit RefreshTeachData(Table.Name,*qobject_cast<TeachData_Camera*>(Teach));
    return CtrlError(NoError);
}

CtrlError TeachManager::GetTeachData(QString Station, QString Name, QVariant &tTeachData)
{
    QSharedPointer<TeachData> Teach;
    QString Type;
    TeachLock.lock();
    if(TeachDatas.contains(Station))
    {
        Type = TeachDatas[Station].first;
        Teach = TeachDatas[Station].second;
    }
    TeachLock.unlock();

    if(Type == "Robot")
    {
        TeachData_Robot *tTeachRobot = qobject_cast<TeachData_Robot *>(Teach.data());
        if(!tTeachRobot) return CtrlError(StationIsWrong,StationName);
        RobotPoint tPoint;
        if(tTeachRobot->GetPoint(Name,tPoint) != -1)
        {
            tTeachData = QVariant::fromValue(tPoint);
            return CtrlError(NoError);
        }
        QVector<RobotPoint> tWay;
        if(tTeachRobot->GetWay(Name,tWay) != -1)
        {
            tTeachData = QVariant::fromValue(tWay);
            return CtrlError(NoError);
        }
        return CtrlError(NameIsWrong,StationName);
    }
    if(Type == "Lighting")
    {
        TeachData_Lighting *tTeachLighting = qobject_cast<TeachData_Lighting *>(Teach.data());
        if(!tTeachLighting) return CtrlError(StationIsWrong,StationName);
        QMap<int, uchar> tLight;
        if(tTeachLighting->GetLight(Name,tLight) != -1)
        {
            tTeachData = QVariant::fromValue(tLight);
            return CtrlError(NoError);
        }
        return CtrlError(NameIsWrong,StationName);
    }
    if(Type == "PanaMotor")
    {
        TeachData_PanaMotor *tTeachPanaMotor = qobject_cast<TeachData_PanaMotor *>(Teach.data());
        if(!tTeachPanaMotor) return CtrlError(StationIsWrong,StationName);
        int tPoint;
        if(tTeachPanaMotor->GetPoint(Name,tPoint) != -1)
        {
            tTeachData = QVariant::fromValue(tPoint);
            return CtrlError(NoError);
        }
        return CtrlError(NameIsWrong,StationName);
    }
    return CtrlError(StationIsWrong,StationName);
}

CtrlError TeachManager::GetTeachDataList(QString Station, QVector<QString> &Names)
{
    QSharedPointer<TeachData> Teach;
    QString Type;
    TeachLock.lock();
    if(TeachDatas.contains(Station))
    {
        Type = TeachDatas[Station].first;
        Teach = TeachDatas[Station].second;
    }
    TeachLock.unlock();
    Names.clear();
    if(Type == "Robot")
    {
        TeachData_Robot *tTeachRobot = qobject_cast<TeachData_Robot *>(Teach.data());
        if(!tTeachRobot) return CtrlError(StationIsWrong,StationName);
        QList<QPair<QString, RobotPoint> > tPoint = tTeachRobot->GetPoints();
        for(int i=0;i<tPoint.size();i++)
        {
            Names.push_back(tPoint[i].first);
        }
        Names.push_back("");
        QList<QPair<QString,QVector<RobotPoint>>> tWay = tTeachRobot->GetWays();
        for(int i=0;i<tWay.size();i++)
        {
            Names.push_back(tWay[i].first);
        }
        return CtrlError(NoError);
    }
    if(Type == "Lighting")
    {
        TeachData_Lighting *tTeachLighting = qobject_cast<TeachData_Lighting *>(Teach.data());
        if(!tTeachLighting) return CtrlError(StationIsWrong,StationName);
        QList<QPair<QString, QMap<int, uchar> > > tLights = tTeachLighting->GetLights();
        for(int i=0;i<tLights.size();i++)
        {
            Names.push_back(tLights[i].first);
        }
        return CtrlError(NoError);
    }
    if(Type == "PanaMotor")
    {
        TeachData_PanaMotor *tTeachPanaMotor = qobject_cast<TeachData_PanaMotor *>(Teach.data());
        if(!tTeachPanaMotor) return CtrlError(StationIsWrong,StationName);
        QList<QPair<QString, int> > tPoints = tTeachPanaMotor->GetPoints();
        for(int i=0;i<tPoints.size();i++)
        {
            Names.push_back(tPoints[i].first);
        }
        return CtrlError(NoError);
    }
    return CtrlError(StationIsWrong,StationName);
}

CtrlError TeachManager::ReadDataBase(QString Table, QVector<QString> &Keys, QVector<QVector<QString> > &Data)
{
    Keys.clear();
    Data.clear();
    QString Cmd;
    QSqlQuery Query(Database);
    Cmd = QString("PRAGMA table_info(%1)").arg(Table);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(ReadDBError,StationName);
    }
    while(Query.next())
    {
        Keys.push_back(Query.value(1).toString());
    }

    Cmd = QString("SELECT * FROM %1").arg(Table);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(ReadDBError,StationName);
    }
    while(Query.next())
    {
        QVector<QString> tData;
        for(int i=0;i<Keys.size();i++)
        {
            tData.push_back(Query.value(i).toString());
        }
        Data.push_back(tData);
    }
    return CtrlError(NoError);
}

CtrlError TeachManager::WriteDataBase(QString Table, QVector<QString> Keys, QVector<QVector<QString> > Data)
{
    QString Cmd;
    QSqlQuery Query(Database);
    Cmd = QString("PRAGMA table_info(%1)").arg(Table);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(WriteDBError,StationName);
    }
    QVector<QString> NowKeys;
    while(Query.next())
    {
        NowKeys.push_back(Query.value(1).toString());
    }
    if(NowKeys != Keys) return CtrlError(WriteDBError,StationName);

    Cmd = "BEGIN TRANSACTION";
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(WriteDBError,StationName);
    }

    Cmd = QString("DELETE from %1;").arg(Table);
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        Cmd = "ROLLBACK TRANSACTION";
        Query.prepare(Cmd);
        if(!Query.exec())
        {
            qDebug()<<Query.lastError();
            return CtrlError(WriteDBError,StationName);
        }
        return CtrlError(WriteDBError,StationName);
    }

    for(int i=0;i<Data.size();i++)
    {
        Cmd = QString("INSERT INTO %1 (").arg(Table);
        for(int j=0;j<Keys.size();j++)
        {
            Cmd.append(Keys[j]+",");
        }
        Cmd.remove(Cmd.size()-1,1);
        Cmd.append(") VALUES (");
        for(int j=0;j<Data[i].size();j++)
        {
            Cmd.append("'" + Data[i][j]+"',");
        }
        Cmd.remove(Cmd.size()-1,1);
        Cmd.append(");");
        Query.prepare(Cmd);
        if(!Query.exec())
        {
            qDebug()<<Query.lastError();
            Cmd = "ROLLBACK TRANSACTION";
            Query.prepare(Cmd);
            if(!Query.exec())
            {
                qDebug()<<Query.lastError();
                return CtrlError(WriteDBError,StationName);
            }
            return CtrlError(WriteDBError,StationName);
        }
    }

    Cmd = "COMMIT";
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(WriteDBError,StationName);
    }
    return CtrlError(NoError);
}

CtrlError TeachManager::CreateDataBase(QString Table, QVector<QString> Keys)
{
    QString Cmd;
    QSqlQuery Query(Database);
    Cmd = QString("Create Table if Not Exists %1 (").arg(Table);
    for(int i=0;i<Keys.size();i++)
    {
        Cmd.append(QString("%1 varchar(30),").arg(Keys[i]));
    }
    Cmd.remove(Cmd.size()-1,1);
    Cmd.append(")");
    Query.prepare(Cmd);
    if(!Query.exec())
    {
        qDebug()<<Query.lastError();
        return CtrlError(WriteDBError,StationName);
    }
    return CtrlError(NoError);
}

void TeachManager::UpDateTableSlot(TeachTable Table)
{
    CtrlError ret =  CreateDataBase(Table.Name,Table.Keys);
    if(ret != NoError) return;
    ret = WriteDataBase(Table.Name,Table.Keys,Table.Data);
    if(ret != NoError) return;
    TeachTable tTable;
    tTable.Name = Table.Name;
    tTable.Type = Table.Type;
    ret = ReadDataBase(tTable.Name,tTable.Keys,tTable.Data);
    if(ret != NoError) return;
    ret = SetTable(tTable);
    if(ret != NoError) return;
}

void TeachManager::UpDateTeachData(QString Station, TeachData_Robot tTeachData)
{
    CtrlError ret;
    QVector<QString> Keys;
    QVector<QVector<QVariant>> Data;
    tTeachData.ToTable(Keys,Data);
    TeachTable Table;
    Table.Name = Station;
    Table.Type = "Robot";
    Table.Keys = Keys;
    Table.Data.clear();
    for(int i=0;i<Data.size();i++)
    {
        QVector<QString> tData;
        for(int j=0;j<Data[i].size();j++)
        {
            if(Data[i][j].typeName() == QString("QString")) tData.push_back(Data[i][j].value<QString>());
            if(Data[i][j].typeName() == QString("double")) tData.push_back(QString::number(Data[i][j].value<double>()));
        }
        Table.Data.push_back(tData);
    }
    emit UpDateTable(Table);
}

void TeachManager::UpDateTeachData(QString Station, TeachData_Lighting tTeachData)
{
    CtrlError ret;
    QVector<QString> Keys;
    QVector<QVector<QVariant>> Data;
    tTeachData.ToTable(Keys,Data);
    TeachTable Table;
    Table.Name = Station;
    Table.Type = "Lighting";
    Table.Keys = Keys;
    Table.Data.clear();
    for(int i=0;i<Data.size();i++)
    {
        QVector<QString> tData;
        for(int j=0;j<Data[i].size();j++)
        {
            if(Data[i][j].typeName() == QString("QString")) tData.push_back(Data[i][j].value<QString>());
            if(Data[i][j].typeName() == QString("double")) tData.push_back(QString::number(Data[i][j].value<double>()));
        }
        Table.Data.push_back(tData);
    }
    emit UpDateTable(Table);
}

void TeachManager::UpDateTeachData(QString Station, TeachData_PanaMotor tTeachData)
{
    CtrlError ret;
    QVector<QString> Keys;
    QVector<QVector<QVariant>> Data;
    tTeachData.ToTable(Keys,Data);
    TeachTable Table;
    Table.Name = Station;
    Table.Type = "PanaMotor";
    Table.Keys = Keys;
    Table.Data.clear();
    for(int i=0;i<Data.size();i++)
    {
        QVector<QString> tData;
        for(int j=0;j<Data[i].size();j++)
        {
            if(Data[i][j].typeName() == QString("QString")) tData.push_back(Data[i][j].value<QString>());
            if(Data[i][j].typeName() == QString("double")) tData.push_back(QString::number(Data[i][j].value<double>()));
        }
        Table.Data.push_back(tData);
    }
    emit UpDateTable(Table);
}

void TeachManager::UpDateTeachData(QString Station, TeachData_Camera tTeachData)
{
    CtrlError ret;
    QVector<QString> Keys;
    QVector<QVector<QVariant>> Data;
    tTeachData.ToTable(Keys,Data);
    TeachTable Table;
    Table.Name = Station;
    Table.Type = "Camera";
    Table.Keys = Keys;
    Table.Data.clear();
    for(int i=0;i<Data.size();i++)
    {
        QVector<QString> tData;
        for(int j=0;j<Data[i].size();j++)
        {
            if(Data[i][j].typeName() == QString("QString")) tData.push_back(Data[i][j].value<QString>());
            if(Data[i][j].typeName() == QString("double")) tData.push_back(QString::number(Data[i][j].value<double>()));
        }
        Table.Data.push_back(tData);
    }
    emit UpDateTable(Table);
}
