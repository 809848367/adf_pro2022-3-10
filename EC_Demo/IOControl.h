﻿#ifndef IOCONTROL_H
#define IOCONTROL_H

#include <QtCore>
#include <atomic>
#include "HardwareModule.h"
class IOControl : public HardwareModule
{
    Q_OBJECT
public:
    explicit IOControl(QObject *parent = nullptr);
    ~IOControl();
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    CtrlError Init(QString Name, QMap<QString,QPair<QString,HardwareModule*>> Modules,
                   QMap<int, QPair<QString, int> > InPutIOData,
                   QMap<int, QPair<QString, int> > OutPutIOData,
                   QMap<int,QString> InPutIOName,
                   QMap<int,QString> OutPutIOName);
    CtrlError SetOutPut(int Index,bool Value);
    CtrlError SetOutPuts(QMap<int, bool> Value);
    CtrlError GetInPut(int Index,bool &Value);
    CtrlError GetInPuts(QMap<int, bool> &Value);
    CtrlError GetOutPut(int Index,bool &Value);
    CtrlError GetOutPuts(QMap<int, bool> &Value);
    CtrlError GetIONames(QMap<int, QString> &Inputs, QMap<int, QString> &Outputs);

private:
    QString Name;
    QMap<QString,QPair<QString,HardwareModule*>> Modules;   //QMap<模块名字,QPair<模块类型，模块地址>>
    QMap<int,QPair<QString,int>> InPutIOData;               //QMap<序号,QPair<模块名字，模块序号>>
    QMap<int,QPair<QString,int>> OutPutIOData;              //QMap<序号,QPair<模块名字，模块序号>>
    QMap<int,QString> InPutIOName;                          //QMap<序号,名字>
    QMap<int,QString> OutPutIOName;                         //QMap<序号,名字>
signals:
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00305000,
        IndexOutOfRange    = Base + 0x001,
        ModuleTypeIsWrong = Base + 0x002,
        ModuleTypeNotAllow = Base + 0x003,
        IONameNotEqualIOData = Base + 0x004,
        InitDataErr = Base + 0x005,
        ExecuteDataNotRight = Base + 0x006,
    };
    Q_ENUM(ErrorCode)
};

#endif // IOCONTROL_H
