#include "SystemConfig.h"
#include "DatabaseManager.h"
#include "CFlageManager.h"
#include "MachineManager.h"
#include "QiniOperator.h"
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")
#endif
SystemConfig::SystemConfig(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	Init();
	InitWindow();
	InitConnection();



	RefreshDspProc(dataAnaly.m_processNameList);
	ui.StartComboBox->setCurrentText(m_StartProc);
	ui.ResetComboBox->setCurrentText(m_ResetProc);
	RefreshTimer.start(100);
}

SystemConfig::~SystemConfig()
{
}

void SystemConfig::Init()
{
	m_bEditTab = false;
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.readStringValue("MachineCfg", "StartProcess", m_StartProc);
	iniOperator.readStringValue("MachineCfg", "RestProcess", m_ResetProc);
}

void SystemConfig::InitWindow()
{
	//ui.tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	//ui.tableView->setSelectionMode(QAbstractItemView::SingleSelection);
	//ui.tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tableView->setAlternatingRowColors(true);
	//ui.tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.tableView->horizontalHeader()->setStyleSheet(
		"QHeaderView::section {" "color: black;padding-left: 4px;border: 1px solid #6c6c6c;}");
	ui.tableView->verticalHeader()->hide();
	m_tableModel = new QStandardItemModel(ui.tableView);
	ui.tableView->setModel(m_tableModel);

	m_tableModel->setHorizontalHeaderLabels(QStringList() << "ID" << "Name" << "Value"<<"Discription");
	//ui.tableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
	//ui.tableView->setColumnWidth(0, 200);
	ui.tableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
	ui.tableView->setColumnWidth(0, 100);
	ui.tableView->setColumnWidth(1, 100);
	ui.tableView->setColumnWidth(2, 100);

	foreach(FlagInfo * info,theDbManager.GetFlagInfo())
	{
		QList<QStandardItem*> rowList;

		rowList << new QStandardItem(QString("%1").arg(info->ID)) << new QStandardItem(QString("%1").arg(info->name))
			<< new QStandardItem(QString("%1").arg(info->value)) << new QStandardItem(QString("%1").arg(info->description));
		m_tableModel->appendRow(rowList);
	}
	ui.tableView->setEnabled(false);
	m_StartProcModule = new QStandardItemModel;
	m_ResetProcModule = new QStandardItemModel;
	ui.ResetComboBox->setModel(m_ResetProcModule);
	ui.StartComboBox->setModel(m_StartProcModule);


}

void SystemConfig::InitConnection()
{
	connect(&RefreshTimer, SIGNAL(timeout()), this, SLOT(RefreshTimerSlot()));
	m_tableModel;
	connect(ui.checkBox_Edit__TableView, &QCheckBox::stateChanged, this, &SystemConfig::Edit_Table_stateChanged);
	connect(m_tableModel,&QStandardItemModel::itemChanged, this, &SystemConfig::itemChanged);

	//void Slot_ComboxStartProc(QString itemStr);
	//void Slot_ComboxResetProc(QString itemStr);

	connect(ui.ResetComboBox, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),this,&SystemConfig::Slot_ComboxResetProc);
	connect(ui.StartComboBox, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged), this, &SystemConfig::Slot_ComboxStartProc);
	connect(this, &SystemConfig::emit_startProc, &theMachineManager, &MachineManager::SetStartProcName);
	connect(this, &SystemConfig::emit_resetProc, &theMachineManager, &MachineManager::SetResetProcName);
}

void SystemConfig::DataRefresh()
{
	for (int i = 0; i != theDbManager.GetFlagInfo().count();i++)
	{
		m_tableModel->setItem(i, 2, new QStandardItem( QString("%1").arg(theDbManager.GetFlagInfo()[i]->value)));
	}


	//foreach(FlagInfo * info, theDbManager.GetFlagInfo())
	//{
	//	QList<QStandardItem*> rowList;
	//	rowList << new QStandardItem(QString(info->ID)) << new QStandardItem(QString(info->name))
	//		<< new QStandardItem(QString(info->value));
	//	m_tableModel->appendRow(rowList);
	//}
}

void SystemConfig::RefreshTimerSlot()
{	
	if (m_bEditTab)
		return;
	DataRefresh();
}

void SystemConfig::itemChanged(QStandardItem *item)
{
	if (!m_bEditTab)
		return;
	QModelIndex index = ui.tableView->currentIndex();
	QModelIndex nameIndex = m_tableModel->index(index.row(), 1);
	QStandardItem* NameItem = m_tableModel->itemFromIndex(nameIndex);
	qDebug() << NameItem->text();
	if (index.column() == 2)//修改值
		theFlageManager.SetFlagValueByName(NameItem->text(), item->text().toInt());
	else if (index.column() == 3) // 修改说明
	{
		QStandardItem* selectItem = m_tableModel->itemFromIndex(index);
		qDebug() << NameItem->text() << ";" << selectItem->text();
		theDbManager.UpdateFlagTableDescription(NameItem->text(), selectItem->text());
	}
}

void SystemConfig::Edit_Table_stateChanged(int state)
{
	if (state == Qt::Checked) // "选中"
	{
		m_bEditTab = true;
		ui.tableView->setEnabled(true);
	}
	else if(state == Qt::Unchecked)// 未选中 - Qt::Unchecked
	{
		m_bEditTab = false;
		ui.tableView->setEnabled(false);
	}
}

void SystemConfig::RefreshDspProc(QList<QString> procNameList)
{
	m_StartProcModule->clear();
	m_ResetProcModule->clear();
	foreach(QString itemstr, procNameList)
	{
		m_StartProcModule->appendRow(new QStandardItem(itemstr));
		m_ResetProcModule->appendRow(new QStandardItem(itemstr));
	}
	//QString startProc, resetProc;
	//iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	//iniOperator.readStringValue("MachineCfg", "StartProcess", startProc);
	//iniOperator.readStringValue("MachineCfg", "RestProcess", resetProc);
	//ui.StartComboBox->setCurrentText(startProc);
	//ui.ResetComboBox->setCurrentText(resetProc);
}

void SystemConfig::Slot_ComboxStartProc(QString itemStr)
{
	qDebug() << itemStr;
	//emit emit_startProc(itemStr);
	theMachineManager.SetStartProcName(itemStr);
}

void SystemConfig::Slot_ComboxResetProc(QString itemStr)
{
	qDebug() << itemStr;
	//emit emit_resetProc(itemStr);
	theMachineManager.SetResetProcName(itemStr);
}
