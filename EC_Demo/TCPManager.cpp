#include "TCPManager.h"
#include "CFileOperator.h"
#include "TestBox.h"

TCPManager::TCPManager(QObject *parent)
	: QObject(parent)
{
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}
	connect(this, &TCPManager::emit_Clinet_SendDataToServer, this, &TCPManager::Client_SendDataToServer);
	connect(this, &TCPManager::emit_Client_CreateOnClientConnectSever, this, &TCPManager::Slot_Client_CreateOnClientConnectSever);
	connect(this, &TCPManager::emit_Client_TouchReadReady, this, &TCPManager::ClientReadReady);
	//connect(this, SIGNAL(emit_SendDataToServer(QString, QString)), this, SLOT(SendDataToServer(QString, QString)), Qt::UniqueConnection);
	connect(this, &TCPManager::emit_CreateServer, this, &TCPManager::Slot_CreateServer);
	connect(this, &TCPManager::emit_Client_DisconnectedClient, this, &TCPManager::Slot_UserDisconnectClient);
}

TCPManager::~TCPManager()
{
}

TCPManager* TCPManager::Instance()
{
	static TCPManager manager;
	return &manager;
}

CtrlError TCPManager::CreateServer(QString severName, int Port, int MaxConnectNum/*=1*/)
{
	emit emit_CreateServer(severName, Port, MaxConnectNum);

	return CtrlError(NoError);
	//QTcpServer* server = new QTcpServer;
	//QHostAddress addr("127.0.0.1");
	//bool bRet = server->listen(addr, Port);
	//if (!bRet)
	//{
	//	QString msg = QString("%1->%2->%3->%4 fail!").arg( this->metaObject()->className()).arg(__FUNCTION__).arg(severName).arg(Port);
	//	theFileManager.WriteTodayFile_TXT(msg);
	//	return CtrlError(InitSeverError);
	//}
	//server->setObjectName(severName);//设置对象名
	//connect(server, &QTcpServer::newConnection, this, &TCPManager::Server_NewConnect, Qt::UniqueConnection);
	//server->setMaxPendingConnections(MaxConnectNum);//设置最大链接数
	//emit emit_ServerSendNewServerName(severName);
	//theFileManager.WriteTodayFile_TXT(QString("%1->%2->%3->%4 success!").arg(this->metaObject()->className()).arg(__FUNCTION__).arg(severName).arg(Port));
	//return CtrlError(NoError);
}

CtrlError TCPManager::Server_CatchOnMsgFromSkt(QString sktName, QString& msg,int tOut)
{
	if (!m_ConnectClient.contains(sktName))
		return CtrlError();
	QTime t;
	t.start();
	while (true)
	{
		QThread::msleep(10);
		QMutexLocker locker(m_ConnectRecvBufferMutex[sktName]);
		if (m_ConnectRecvBuffer[sktName].size() > 0)
		{
			msg = m_ConnectRecvBuffer[sktName].first();
			m_ConnectRecvBuffer[sktName].removeFirst();
			break;
		}
		if (t.elapsed() > tOut&&tOut != 0)
			return CtrlError(TimeOutError);
	}
	return CtrlError(NoError);
}

CtrlError TCPManager::Server_SendMsgToSkt(QString clientName, QString msg)
{
	if (!m_ConnectClient.contains(clientName))
		return CtrlError(NotFindConnectSkt);
	QTcpSocket* skt = m_ConnectClient[clientName];
	skt->write(msg.toUtf8());
	skt->waitForBytesWritten();
	theFileManager.WriteTodayFile_TxtWithModule(QStringList() << ModuleName << clientName << "ServerSend", msg);
	return CtrlError(NoError);
}

CtrlError TCPManager::ClearBufferByClientName(QString sktName)
{
	if (!m_recvBuffer.contains(sktName))
		return NotFindConnectSkt;
	QMutex* mutex = m_recvBufferMutex[sktName];
	QMutexLocker locker(mutex);
	m_recvBuffer[sktName].clear();
	return  CtrlError(NoError);
}

CtrlError TCPManager::Client_CreateOnClientConnectSever(QString clientName, QString IP, int Port)
{
	if (m_client.contains(clientName))
		return CtrlError(ClientHasLive);
	emit emit_Client_CreateOnClientConnectSever(clientName, IP, Port);
	//检查链接是否成功
	QTime t;
	t.start();
	while (true)
	{
		if (m_client.contains(clientName))
		{
			break;
		}
		else if (t.elapsed() > 1000)
		{
			return CtrlError(ConnectionSeverFail);
		}
		QThread::msleep(10);
	}

	return CtrlError(NoError);
}

CtrlError TCPManager::DisconnectClient(QString clientName)
{
	if (!m_client.contains(clientName))
		return CtrlError(ClientNotFind);
	QTcpSocket* client = m_client[clientName];
	if (!client->isOpen())
		return CtrlError(ClientNotOpen);
	emit emit_Client_DisconnectedClient(clientName);
	QMutex* mutex = m_recvBufferMutex[clientName];
	QMutexLocker locker(mutex);
	m_recvBuffer[clientName].clear();
	return CtrlError(NoError);
}

CtrlError TCPManager::Client_CatchOneDataFromServerMsg(QString clientName, QString& dataStr)
{
	if (!m_recvBufferMutex.contains(clientName))
		return CtrlError(ClientNotFind);
	QMutexLocker locker(m_recvBufferMutex[clientName]);

	if (m_recvBuffer[clientName].size() > 0)
	{
		dataStr = m_recvBuffer[clientName].first();
		m_recvBuffer[clientName].removeFirst();
	}
	else return CtrlError(RecvStrBufferEmpty);
	return CtrlError(NoError);
}

CtrlError TCPManager::Client_CatchOneDataFromServerMsg(QString clientName, QString& dataStr, int length)
{
	if (!m_recvBufferMutex.contains(clientName))
		return CtrlError(ClientNotFind);
	QMutexLocker locker(m_recvBufferMutex[clientName]);

	if (m_recvBuffer[clientName].size() > 0)
	{
		QString tempStr = m_recvBuffer[clientName].first();
		if (tempStr.length() == length)
		{
			dataStr = tempStr;
			m_recvBuffer[clientName].removeFirst();
		}
		else
		{
			//m_recvBuffer[clientName].removeFirst();
			dataStr = tempStr.left(length);
			int n = tempStr.length();
			tempStr.right(n- length);
			m_recvBuffer[clientName][0] = tempStr;
		}
	}
	else return CtrlError(RecvStrBufferEmpty);
	return CtrlError(NoError);
}

CtrlError TCPManager::Transmit_Client_SendDataToSever(QString clientName, QString dataStr)
{
	if (!m_client.contains(clientName))
		return CtrlError(ClientNotFind);
	emit emit_Clinet_SendDataToServer(clientName, dataStr);
	return CtrlError(NoError);
}

CtrlError TCPManager::Slot_UserDisconnectClient(QString clientName)
{
	if (!m_client.contains(clientName))
		return CtrlError(ClientNotFind);
	QTcpSocket* client = m_client[clientName];
	if (!client->isOpen())
		return CtrlError(ClientNotOpen);
	client->disconnectFromHost();
	client->deleteLater();
	//emit emit_Client_DisconnectedClient(clientName);
	m_client.remove(clientName);
	m_recvBuffer.remove(clientName);
	m_recvBufferMutex.remove(clientName);
	return CtrlError(NoError);
}

CtrlError TCPManager::Slot_AutoDisconnectClient()
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
	QString clientName = client->objectName();
	//msgBox::show("TCP Warnning", clientName + "ServerDisConnect", 1);
	if (!m_client.contains(clientName))
		return CtrlError(ClientNotFind);

	//QTcpSocket* client = m_client[clientName];
	//if (!client->isOpen())
	//return CtrlError(ClientNotOpen);
	//client->disconnectFromHost();
	client->deleteLater();
	//emit emit_Client_DisconnectedClient(clientName);
	m_client.remove(clientName);
	m_recvBuffer.remove(clientName);
	m_recvBufferMutex.remove(clientName);
	//DeleteOneClient(clientName);
	emit emit_Client_DisconnectedClient(clientName);
	return CtrlError(NoError);
}

void TCPManager::ClientReadReady(QString clientName)
{
	if (!m_client.contains(clientName))
		return ;
	QTcpSocket* client = m_client[clientName];
	emit client->readyRead();
}

CtrlError TCPManager::Slot_CreateServer(QString severName, int Port, int MaxConnectNum)
{
	QTcpServer* server = new QTcpServer;
	QHostAddress addr("127.0.0.1");
	bool bRet = server->listen(addr, Port);
	if (!bRet)
	{
		QString msg = QString("%1->%2->%3->%4 fail!").arg( this->metaObject()->className()).arg(__FUNCTION__).arg(severName).arg(Port);
		theFileManager.WriteTodayFile_TXT(msg);
		return CtrlError(InitSeverError);
	}
	server->setObjectName(severName);//设置对象名
	connect(server, &QTcpServer::newConnection, this, &TCPManager::Server_NewConnect, Qt::UniqueConnection);
	server->setMaxPendingConnections(MaxConnectNum);//设置最大链接数
	emit emit_ServerSendNewServerName(severName);
	theFileManager.WriteTodayFile_TXT(QString("%1->%2->%3->%4 success!").arg(this->metaObject()->className()).arg(__FUNCTION__).arg(severName).arg(Port));
	return CtrlError(NoError);
}

void TCPManager::Sever_InitConnection(QTcpServer* server)
{

}

void TCPManager::Server_NewConnect()
{
	QTcpServer* server = static_cast<QTcpServer*>(sender());
	QTcpSocket *sktConn = server->nextPendingConnection();
	if (!sktConn) return;
	//自动生成 
	static int connNum = 0;
	QString sktObjName = server->objectName()+"_"+QString::number(connNum); //connectTcpName:serverName_0  
	sktConn->setObjectName(sktObjName);
	m_ConnectClient.insert(sktObjName, sktConn);

	m_ConnectRecvBuffer.insert(sktObjName, QList<QString>());
	m_ConnectRecvBufferMutex.insert(sktObjName, new QMutex);
	Server_SktInitConnect(sktConn);
	connNum++;
	emit emit_SeverSendConnectSktName(server->objectName(),sktObjName);//将skt 对象名称 发送出去供消息调用
}

void TCPManager::Server_SktRecv()
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
	QString objName = client->objectName();
	client->waitForReadyRead(100);
	int byteLength = client->bytesAvailable();
	QString recvStr;
	if (byteLength > 0)
	{
		recvStr = QString::fromLocal8Bit(client->readAll());
		QMutexLocker locker(m_ConnectRecvBufferMutex[objName]);
		m_ConnectRecvBuffer[objName].push_back(recvStr);
	}
	theFileManager.WriteTodayFile_TxtWithModule(QStringList() << ModuleName << objName << "ServerRecv", recvStr);
}

void TCPManager::Sever_SktDisconntected()
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
	QString objName = client->objectName();
	QStringList strList = objName.split("_");
	emit emit_SeverSendDisConnectedSktName(strList.at(0),objName);
}

void TCPManager::Server_SktInitConnect(QTcpSocket* skt)
{
	connect(skt, &QTcpSocket::readyRead,this,&TCPManager::Server_SktRecv);
	connect(skt, &QTcpSocket::disconnected,this, &TCPManager::Sever_SktDisconntected);
}

CtrlError TCPManager::Client_SendDataToServer(QString clientName, QString dataStr)
{
	if (!m_client.contains(clientName))
		return CtrlError(ClientNotFind);
	QTcpSocket* client = m_client[clientName];
	if (!client->isOpen())
		return CtrlError(ClientNotOpen);
	client->write(dataStr.toUtf8());
	client->waitForBytesWritten();
	client->flush();
	theFileManager.WriteTodayFile_TxtWithModule(QStringList()<<ModuleName<<clientName<<"clinetSend", dataStr);
	return CtrlError(NoError);
}

CtrlError TCPManager::Slot_Client_CreateOnClientConnectSever(QString clientName, QString IP, int Port)
{
	if (m_client.contains(clientName))
		return CtrlError(ClientHasLive);
	QTcpSocket* client = new QTcpSocket;
	//
	
	client->setObjectName(clientName);//设置对象名称
	client->connectToHost(IP, Port);
	if (!client->waitForConnected(3000)) return CtrlError(ConnectionSeverFail);
	Client_InitConnection(client);
	m_client.insert(clientName, client);
	QList<QString> recvList;
	m_recvBuffer.insert(clientName, recvList);
	QMutex* mutex = new QMutex;
	m_recvBufferMutex.insert(clientName, mutex);
	emit emit_Client_ConnectedClient(clientName);
	//client->moveToThread(qApp->thread());
	return CtrlError(NoError);
}

void TCPManager::Client_InitConnection(QTcpSocket* client)
{
	//connect(client, SIGNAL(readyRead()), this, SLOT(RecvResult()), Qt::UniqueConnection);
	connect(client, &QTcpSocket::readyRead, this, &TCPManager::ClientRecvResult,Qt::UniqueConnection);
	//connect(client, &QTcpSocket::disconnected, [this]() 
	//{
	//	QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
	//	emit emit_Client_DisconnectedClient(client->objectName());
	//});
	connect(client, &QTcpSocket::disconnected, this, &TCPManager::Slot_AutoDisconnectClient, Qt::UniqueConnection);
	
}

void TCPManager::ClientRecvResult()
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
	QString objName = client->objectName();
	//client->waitForReadyRead(100);
	int byteLength = client->bytesAvailable();
	QString recvStr;
	if (byteLength>0)
	{
		recvStr = QString::fromLocal8Bit(client->readAll());
		//不可见字符去除
		recvStr.replace("\r", "");
		recvStr.replace("\n", "");
		QMutex* mutex = m_recvBufferMutex[objName];
		QMutexLocker locker(mutex);
		m_recvBuffer[objName].push_back(recvStr);
	}
	theFileManager.WriteTodayFile_TxtWithModule(QStringList() << ModuleName << objName <<"clinetRecv" , recvStr);
}
