#include "CommunicationPage.h"
#include "MappingManager.h"

CommunicationPage::CommunicationPage(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	Init();
	InitWindow();
	InitConnect();
}

CommunicationPage::~CommunicationPage()
{
}

void CommunicationPage::Init()
{

}

void CommunicationPage::InitWindow()
{
	treeModule = new QStandardItemModel;
	ui.treeView->setModel(treeModule);


}

void CommunicationPage::InitConnect()
{
	connect(ui.refreshBtn, &QPushButton::clicked, this, &CommunicationPage::RefreshSlot);
}

void CommunicationPage::RefreshSlot()
{
	treeModule->clear();
	QMap<QString, QList<QString>> moduleInfo = theMappingManager->GetMapInfo();
	foreach(QString key, moduleInfo.keys())
	{
		QStandardItem* moduleItem = new QStandardItem(key);
		QList<QString> listInfo = moduleInfo[key];
		foreach(QString subItem, listInfo)
		{
			moduleItem->appendRow(new QStandardItem(subItem));
		}

		treeModule->appendRow(moduleItem);
	}
}
