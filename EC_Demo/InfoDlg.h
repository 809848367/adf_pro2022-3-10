#pragma once

#include <QDialog>
#include "ui_InfoDlg.h"
#include <QTabWidget>

class InfoDlg : public QDialog
{
	Q_OBJECT

public:
	InfoDlg(QWidget *parent = Q_NULLPTR);
	~InfoDlg();
	void Init();
	void InitConnection();
	void InitWindow();
	static InfoDlg* GetInstance();
	static void DispInfoDlg();
	static void ReleaseInfoDlg();

public slots:



private:
	Ui::InfoDlg ui;
	QTabWidget* mainWidget;
	static InfoDlg* info_stance;
};
