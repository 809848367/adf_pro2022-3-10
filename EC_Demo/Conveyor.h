﻿#ifndef CONVEYOR_H
#define CONVEYOR_H

#include <QtCore>
#include "HardwareModule.h"
#include "SerialPortManager.h"
#include "CtrlError.h"

class Conveyor : public HardwareModule
{
    Q_OBJECT
public:
    explicit Conveyor(QObject *parent = nullptr);
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    CtrlError Init(QString Name, QString Com, int ID, int Vel, SerialPortManager *SPM);
    CtrlError Start();
    CtrlError Stop();
    CtrlError GetStatus(int &Status); //0:失能,1:使能,2:运行,3:报错
    CtrlError SetEnable(bool Enable);
    CtrlError SetVel(int Vel);
    CtrlError ClearErr();
private:
    SerialPortManager *SPM;
    QString Name;
    QString Com;
    int ID;
    int Vel;
    QTime GetStatustimer;
    int GetStatustStatus;
    bool GetStatusFirst;
    void SetCRC16Modbus(QByteArray &Data);
signals:

public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00303000,
        ClearErrFail    = Base + 0x001,
        StartCmdErr     = Base + 0x002,
        StopCmdErr      = Base + 0x003,
        GetStatusCmdErr = Base + 0x004,
        SetEnableCmdErr = Base + 0x005,
        SetVelCmdErr    = Base + 0x006,
        ClearErrCmdErr  = Base + 0x007,
        InitDataErr     = Base + 0x008,
        SPMisEmpty      = Base + 0x009,
        ExecuteDataNotRight = Base + 0x010,
    };
    Q_ENUM(ErrorCode)
};

#endif // CONVEYOR_H
