/********************************************************************************
** Form generated from reading UI file 'TempDataShowDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEMPDATASHOWDLG_H
#define UI_TEMPDATASHOWDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TempDataShowDlg
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QTableWidget *tableWidget;
    QGroupBox *groupBox_2;
    QPushButton *RefreshBtn;
    QPushButton *clearDataBtn;

    void setupUi(QDialog *TempDataShowDlg)
    {
        if (TempDataShowDlg->objectName().isEmpty())
            TempDataShowDlg->setObjectName(QString::fromUtf8("TempDataShowDlg"));
        TempDataShowDlg->resize(1070, 606);
        verticalLayout = new QVBoxLayout(TempDataShowDlg);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(TempDataShowDlg);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        tableWidget = new QTableWidget(groupBox);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        verticalLayout_2->addWidget(tableWidget);


        horizontalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(TempDataShowDlg);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(130, 0));
        groupBox_2->setMaximumSize(QSize(150, 16777215));
        RefreshBtn = new QPushButton(groupBox_2);
        RefreshBtn->setObjectName(QString::fromUtf8("RefreshBtn"));
        RefreshBtn->setGeometry(QRect(20, 30, 93, 28));
        clearDataBtn = new QPushButton(groupBox_2);
        clearDataBtn->setObjectName(QString::fromUtf8("clearDataBtn"));
        clearDataBtn->setGeometry(QRect(20, 80, 93, 28));

        horizontalLayout->addWidget(groupBox_2);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(TempDataShowDlg);

        QMetaObject::connectSlotsByName(TempDataShowDlg);
    } // setupUi

    void retranslateUi(QDialog *TempDataShowDlg)
    {
        TempDataShowDlg->setWindowTitle(QApplication::translate("TempDataShowDlg", "TempDataShowDlg", nullptr));
        groupBox->setTitle(QApplication::translate("TempDataShowDlg", "\346\225\260\346\215\256", nullptr));
        groupBox_2->setTitle(QApplication::translate("TempDataShowDlg", "\345\212\237\350\203\275", nullptr));
        RefreshBtn->setText(QApplication::translate("TempDataShowDlg", "\345\210\267\346\226\260\346\225\260\346\215\256", nullptr));
        clearDataBtn->setText(QApplication::translate("TempDataShowDlg", "\346\270\205\351\231\244\346\225\260\346\215\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TempDataShowDlg: public Ui_TempDataShowDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEMPDATASHOWDLG_H
