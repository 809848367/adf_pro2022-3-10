/********************************************************************************
** Form generated from reading UI file 'PanaMotorUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PANAMOTORUI_H
#define UI_PANAMOTORUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PanaMotorUI
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *Status;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QLineEdit *Position;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *Reset;
    QPushButton *Enable;
    QPushButton *Disable;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpinBox *PosStep;
    QPushButton *Pos_Add;
    QPushButton *Pos_Del;
    QSpacerItem *horizontalSpacer;
    QLineEdit *MovePosTar;
    QPushButton *MovePos;
    QPushButton *Stop;
    QTableWidget *TeachData;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *TeachAdd;
    QPushButton *TeachDel;
    QPushButton *TeachMove;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PanaMotorUI)
    {
        if (PanaMotorUI->objectName().isEmpty())
            PanaMotorUI->setObjectName(QString::fromUtf8("PanaMotorUI"));
        PanaMotorUI->resize(831, 539);
        verticalLayout = new QVBoxLayout(PanaMotorUI);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(PanaMotorUI);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(70, 30));
        label_2->setMaximumSize(QSize(70, 30));

        horizontalLayout->addWidget(label_2);

        Status = new QLineEdit(PanaMotorUI);
        Status->setObjectName(QString::fromUtf8("Status"));
        Status->setMinimumSize(QSize(0, 30));
        Status->setMaximumSize(QSize(16777215, 30));

        horizontalLayout->addWidget(Status);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label = new QLabel(PanaMotorUI);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(70, 30));
        label->setMaximumSize(QSize(70, 30));

        horizontalLayout_4->addWidget(label);

        Position = new QLineEdit(PanaMotorUI);
        Position->setObjectName(QString::fromUtf8("Position"));
        Position->setMinimumSize(QSize(0, 30));
        Position->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_4->addWidget(Position);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        Reset = new QPushButton(PanaMotorUI);
        Reset->setObjectName(QString::fromUtf8("Reset"));
        Reset->setMinimumSize(QSize(100, 30));
        Reset->setMaximumSize(QSize(100, 30));

        horizontalLayout_2->addWidget(Reset);

        Enable = new QPushButton(PanaMotorUI);
        Enable->setObjectName(QString::fromUtf8("Enable"));
        Enable->setMinimumSize(QSize(100, 30));
        Enable->setMaximumSize(QSize(100, 30));

        horizontalLayout_2->addWidget(Enable);

        Disable = new QPushButton(PanaMotorUI);
        Disable->setObjectName(QString::fromUtf8("Disable"));
        Disable->setMinimumSize(QSize(100, 30));
        Disable->setMaximumSize(QSize(100, 30));

        horizontalLayout_2->addWidget(Disable);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(PanaMotorUI);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(50, 30));
        label_3->setMaximumSize(QSize(50, 30));

        horizontalLayout_3->addWidget(label_3);

        PosStep = new QSpinBox(PanaMotorUI);
        PosStep->setObjectName(QString::fromUtf8("PosStep"));
        PosStep->setMinimumSize(QSize(80, 30));
        PosStep->setMaximumSize(QSize(80, 30));
        PosStep->setMaximum(10000);
        PosStep->setSingleStep(1000);
        PosStep->setValue(1000);

        horizontalLayout_3->addWidget(PosStep);

        Pos_Add = new QPushButton(PanaMotorUI);
        Pos_Add->setObjectName(QString::fromUtf8("Pos_Add"));
        Pos_Add->setMinimumSize(QSize(30, 30));
        Pos_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout_3->addWidget(Pos_Add);

        Pos_Del = new QPushButton(PanaMotorUI);
        Pos_Del->setObjectName(QString::fromUtf8("Pos_Del"));
        Pos_Del->setMinimumSize(QSize(30, 30));
        Pos_Del->setMaximumSize(QSize(30, 30));

        horizontalLayout_3->addWidget(Pos_Del);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        MovePosTar = new QLineEdit(PanaMotorUI);
        MovePosTar->setObjectName(QString::fromUtf8("MovePosTar"));
        MovePosTar->setMinimumSize(QSize(100, 30));
        MovePosTar->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_3->addWidget(MovePosTar);

        MovePos = new QPushButton(PanaMotorUI);
        MovePos->setObjectName(QString::fromUtf8("MovePos"));
        MovePos->setMinimumSize(QSize(100, 30));
        MovePos->setMaximumSize(QSize(100, 30));

        horizontalLayout_3->addWidget(MovePos);

        Stop = new QPushButton(PanaMotorUI);
        Stop->setObjectName(QString::fromUtf8("Stop"));
        Stop->setMinimumSize(QSize(100, 30));
        Stop->setMaximumSize(QSize(100, 30));

        horizontalLayout_3->addWidget(Stop);


        verticalLayout->addLayout(horizontalLayout_3);

        TeachData = new QTableWidget(PanaMotorUI);
        TeachData->setObjectName(QString::fromUtf8("TeachData"));
        TeachData->setMinimumSize(QSize(0, 200));
        TeachData->setMaximumSize(QSize(16777215, 200));

        verticalLayout->addWidget(TeachData);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        TeachAdd = new QPushButton(PanaMotorUI);
        TeachAdd->setObjectName(QString::fromUtf8("TeachAdd"));

        horizontalLayout_5->addWidget(TeachAdd);

        TeachDel = new QPushButton(PanaMotorUI);
        TeachDel->setObjectName(QString::fromUtf8("TeachDel"));

        horizontalLayout_5->addWidget(TeachDel);

        TeachMove = new QPushButton(PanaMotorUI);
        TeachMove->setObjectName(QString::fromUtf8("TeachMove"));

        horizontalLayout_5->addWidget(TeachMove);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 370, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(PanaMotorUI);

        QMetaObject::connectSlotsByName(PanaMotorUI);
    } // setupUi

    void retranslateUi(QWidget *PanaMotorUI)
    {
        PanaMotorUI->setWindowTitle(QApplication::translate("PanaMotorUI", "Form", nullptr));
        label_2->setText(QApplication::translate("PanaMotorUI", "Status", nullptr));
        label->setText(QApplication::translate("PanaMotorUI", "Position", nullptr));
        Reset->setText(QApplication::translate("PanaMotorUI", "Reset", nullptr));
        Enable->setText(QApplication::translate("PanaMotorUI", "Enable", nullptr));
        Disable->setText(QApplication::translate("PanaMotorUI", "Disable", nullptr));
        label_3->setText(QApplication::translate("PanaMotorUI", "Step", nullptr));
        Pos_Add->setText(QApplication::translate("PanaMotorUI", "+", nullptr));
        Pos_Del->setText(QApplication::translate("PanaMotorUI", "-", nullptr));
        MovePos->setText(QApplication::translate("PanaMotorUI", "MovePos", nullptr));
        Stop->setText(QApplication::translate("PanaMotorUI", "Stop", nullptr));
        TeachAdd->setText(QApplication::translate("PanaMotorUI", "Add", nullptr));
        TeachDel->setText(QApplication::translate("PanaMotorUI", "Del", nullptr));
        TeachMove->setText(QApplication::translate("PanaMotorUI", "Move", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PanaMotorUI: public Ui_PanaMotorUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PANAMOTORUI_H
