/********************************************************************************
** Form generated from reading UI file 'LightingUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIGHTINGUI_H
#define UI_LIGHTINGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LightingUI
{
public:
    QHBoxLayout *horizontalLayout;
    QTableWidget *Lighting;
    QVBoxLayout *verticalLayout;
    QTableWidget *TeachData;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *Teach_Add;
    QPushButton *Teach_Del;
    QPushButton *Teach_Set;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *LightingUI)
    {
        if (LightingUI->objectName().isEmpty())
            LightingUI->setObjectName(QString::fromUtf8("LightingUI"));
        LightingUI->resize(978, 446);
        horizontalLayout = new QHBoxLayout(LightingUI);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Lighting = new QTableWidget(LightingUI);
        Lighting->setObjectName(QString::fromUtf8("Lighting"));

        horizontalLayout->addWidget(Lighting);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        TeachData = new QTableWidget(LightingUI);
        TeachData->setObjectName(QString::fromUtf8("TeachData"));

        verticalLayout->addWidget(TeachData);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        Teach_Add = new QPushButton(LightingUI);
        Teach_Add->setObjectName(QString::fromUtf8("Teach_Add"));
        Teach_Add->setMinimumSize(QSize(60, 30));
        Teach_Add->setMaximumSize(QSize(60, 30));

        horizontalLayout_2->addWidget(Teach_Add);

        Teach_Del = new QPushButton(LightingUI);
        Teach_Del->setObjectName(QString::fromUtf8("Teach_Del"));
        Teach_Del->setMinimumSize(QSize(60, 30));
        Teach_Del->setMaximumSize(QSize(60, 30));

        horizontalLayout_2->addWidget(Teach_Del);

        Teach_Set = new QPushButton(LightingUI);
        Teach_Set->setObjectName(QString::fromUtf8("Teach_Set"));
        Teach_Set->setMinimumSize(QSize(60, 30));
        Teach_Set->setMaximumSize(QSize(60, 30));

        horizontalLayout_2->addWidget(Teach_Set);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_2);


        horizontalLayout->addLayout(verticalLayout);

        horizontalLayout->setStretch(0, 3);
        horizontalLayout->setStretch(1, 1);

        retranslateUi(LightingUI);

        QMetaObject::connectSlotsByName(LightingUI);
    } // setupUi

    void retranslateUi(QWidget *LightingUI)
    {
        LightingUI->setWindowTitle(QApplication::translate("LightingUI", "Form", nullptr));
        Teach_Add->setText(QApplication::translate("LightingUI", "Add", nullptr));
        Teach_Del->setText(QApplication::translate("LightingUI", "Del", nullptr));
        Teach_Set->setText(QApplication::translate("LightingUI", "Set", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LightingUI: public Ui_LightingUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIGHTINGUI_H
