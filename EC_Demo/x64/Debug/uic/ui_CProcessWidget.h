/********************************************************************************
** Form generated from reading UI file 'CProcessWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CPROCESSWIDGET_H
#define UI_CPROCESSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CProcessWidget
{
public:
    QHBoxLayout *horizontalLayout_6;
    QSplitter *splitter_2;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QTreeView *treeView;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *Process_Num_lineEdit;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *Process_Name_lineEdit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLineEdit *Step_Identify_lineEdit;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_4;
    QLineEdit *Step_Name_lineEdit;
    QCheckBox *Step_End_CodeSuport_CheckBox;
    QHBoxLayout *horizontalLayout_21;
    QPushButton *EnsureStepBtn;
    QPushButton *RevokeStepBtn;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_15;
    QComboBox *RunProcComboBox;
    QHBoxLayout *horizontalLayout_20;
    QPushButton *ProcExitBtn;
    QLabel *label_16;
    QLabel *StepLabel;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_23;
    QPushButton *Load_Proce_BTN;
    QPushButton *Refresh_BTN;
    QHBoxLayout *horizontalLayout_24;
    QPushButton *Save_As_BTN;
    QPushButton *ReSort_BTN;
    QHBoxLayout *horizontalLayout_25;
    QPushButton *Save_BTN;
    QPushButton *ReStateProcBtn;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_5;
    QLineEdit *Operator_Num_lineEdit;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_6;
    QLineEdit *Operator_Name_lineEdit;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_7;
    QComboBox *Operator_Station_ComboBox;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_8;
    QComboBox *Operator_Type_ComboBox;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_9;
    QLineEdit *Operator_Pos_lineEdit;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_10;
    QLineEdit *Operator_Param_ineEdit_1;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_11;
    QLineEdit *Operator_Param_ineEdit_2;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_13;
    QLineEdit *Operator_Param_ineEdit_3;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_12;
    QLineEdit *Operator_Param_ineEdit_4;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_14;
    QLineEdit *Operator_Param_ineEdit_5;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *Btn_Ensure;
    QPushButton *Btn_Cancle;
    QTextEdit *textEdit;
    QSpacerItem *verticalSpacer_2;
    QTableView *tableView;

    void setupUi(QWidget *CProcessWidget)
    {
        if (CProcessWidget->objectName().isEmpty())
            CProcessWidget->setObjectName(QString::fromUtf8("CProcessWidget"));
        CProcessWidget->resize(995, 745);
        horizontalLayout_6 = new QHBoxLayout(CProcessWidget);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        splitter_2 = new QSplitter(CProcessWidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(splitter_2->sizePolicy().hasHeightForWidth());
        splitter_2->setSizePolicy(sizePolicy);
        splitter_2->setOrientation(Qt::Horizontal);
        widget = new QWidget(splitter_2);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(400, 0));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        treeView = new QTreeView(widget);
        treeView->setObjectName(QString::fromUtf8("treeView"));
        sizePolicy.setHeightForWidth(treeView->sizePolicy().hasHeightForWidth());
        treeView->setSizePolicy(sizePolicy);
        treeView->setMinimumSize(QSize(0, 0));

        verticalLayout_2->addWidget(treeView);

        splitter_2->addWidget(widget);
        widget_2 = new QWidget(splitter_2);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setMinimumSize(QSize(0, 0));
        widget_2->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout = new QVBoxLayout(widget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(widget_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(250, 0));
        groupBox->setMaximumSize(QSize(300, 16777215));
        verticalLayout_6 = new QVBoxLayout(groupBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        Process_Num_lineEdit = new QLineEdit(groupBox);
        Process_Num_lineEdit->setObjectName(QString::fromUtf8("Process_Num_lineEdit"));

        horizontalLayout_2->addWidget(Process_Num_lineEdit);


        verticalLayout_6->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_3->addWidget(label);

        Process_Name_lineEdit = new QLineEdit(groupBox);
        Process_Name_lineEdit->setObjectName(QString::fromUtf8("Process_Name_lineEdit"));

        horizontalLayout_3->addWidget(Process_Name_lineEdit);


        verticalLayout_6->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        Step_Identify_lineEdit = new QLineEdit(groupBox);
        Step_Identify_lineEdit->setObjectName(QString::fromUtf8("Step_Identify_lineEdit"));

        horizontalLayout_4->addWidget(Step_Identify_lineEdit);


        verticalLayout_6->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_5->addWidget(label_4);

        Step_Name_lineEdit = new QLineEdit(groupBox);
        Step_Name_lineEdit->setObjectName(QString::fromUtf8("Step_Name_lineEdit"));

        horizontalLayout_5->addWidget(Step_Name_lineEdit);


        verticalLayout_6->addLayout(horizontalLayout_5);

        Step_End_CodeSuport_CheckBox = new QCheckBox(groupBox);
        Step_End_CodeSuport_CheckBox->setObjectName(QString::fromUtf8("Step_End_CodeSuport_CheckBox"));

        verticalLayout_6->addWidget(Step_End_CodeSuport_CheckBox);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        EnsureStepBtn = new QPushButton(groupBox);
        EnsureStepBtn->setObjectName(QString::fromUtf8("EnsureStepBtn"));

        horizontalLayout_21->addWidget(EnsureStepBtn);

        RevokeStepBtn = new QPushButton(groupBox);
        RevokeStepBtn->setObjectName(QString::fromUtf8("RevokeStepBtn"));

        horizontalLayout_21->addWidget(RevokeStepBtn);


        verticalLayout_6->addLayout(horizontalLayout_21);

        groupBox_4 = new QGroupBox(groupBox);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(0, 150));
        verticalLayout_3 = new QVBoxLayout(groupBox_4);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_15 = new QLabel(groupBox_4);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_19->addWidget(label_15);

        RunProcComboBox = new QComboBox(groupBox_4);
        RunProcComboBox->setObjectName(QString::fromUtf8("RunProcComboBox"));

        horizontalLayout_19->addWidget(RunProcComboBox);

        horizontalLayout_19->setStretch(1, 1);

        verticalLayout_3->addLayout(horizontalLayout_19);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        ProcExitBtn = new QPushButton(groupBox_4);
        ProcExitBtn->setObjectName(QString::fromUtf8("ProcExitBtn"));

        horizontalLayout_20->addWidget(ProcExitBtn);

        label_16 = new QLabel(groupBox_4);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        QFont font;
        font.setPointSize(9);
        label_16->setFont(font);

        horizontalLayout_20->addWidget(label_16);

        StepLabel = new QLabel(groupBox_4);
        StepLabel->setObjectName(QString::fromUtf8("StepLabel"));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setWeight(75);
        StepLabel->setFont(font1);

        horizontalLayout_20->addWidget(StepLabel);


        verticalLayout_3->addLayout(horizontalLayout_20);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_6->addWidget(groupBox_4);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(0, 150));
        verticalLayout_5 = new QVBoxLayout(groupBox_3);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        Load_Proce_BTN = new QPushButton(groupBox_3);
        Load_Proce_BTN->setObjectName(QString::fromUtf8("Load_Proce_BTN"));

        horizontalLayout_23->addWidget(Load_Proce_BTN);

        Refresh_BTN = new QPushButton(groupBox_3);
        Refresh_BTN->setObjectName(QString::fromUtf8("Refresh_BTN"));

        horizontalLayout_23->addWidget(Refresh_BTN);


        verticalLayout_5->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        Save_As_BTN = new QPushButton(groupBox_3);
        Save_As_BTN->setObjectName(QString::fromUtf8("Save_As_BTN"));

        horizontalLayout_24->addWidget(Save_As_BTN);

        ReSort_BTN = new QPushButton(groupBox_3);
        ReSort_BTN->setObjectName(QString::fromUtf8("ReSort_BTN"));

        horizontalLayout_24->addWidget(ReSort_BTN);


        verticalLayout_5->addLayout(horizontalLayout_24);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        Save_BTN = new QPushButton(groupBox_3);
        Save_BTN->setObjectName(QString::fromUtf8("Save_BTN"));

        horizontalLayout_25->addWidget(Save_BTN);

        ReStateProcBtn = new QPushButton(groupBox_3);
        ReStateProcBtn->setObjectName(QString::fromUtf8("ReStateProcBtn"));

        horizontalLayout_25->addWidget(ReStateProcBtn);


        verticalLayout_5->addLayout(horizontalLayout_25);


        verticalLayout_6->addWidget(groupBox_3);


        horizontalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(widget_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(0, 0));
        groupBox_2->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_7 = new QVBoxLayout(groupBox_2);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy2);
        label_5->setMinimumSize(QSize(80, 0));
        label_5->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_8->addWidget(label_5);

        Operator_Num_lineEdit = new QLineEdit(groupBox_2);
        Operator_Num_lineEdit->setObjectName(QString::fromUtf8("Operator_Num_lineEdit"));
        sizePolicy.setHeightForWidth(Operator_Num_lineEdit->sizePolicy().hasHeightForWidth());
        Operator_Num_lineEdit->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(Operator_Num_lineEdit);


        verticalLayout_7->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        sizePolicy2.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy2);
        label_6->setMinimumSize(QSize(80, 0));
        label_6->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_9->addWidget(label_6);

        Operator_Name_lineEdit = new QLineEdit(groupBox_2);
        Operator_Name_lineEdit->setObjectName(QString::fromUtf8("Operator_Name_lineEdit"));
        sizePolicy.setHeightForWidth(Operator_Name_lineEdit->sizePolicy().hasHeightForWidth());
        Operator_Name_lineEdit->setSizePolicy(sizePolicy);

        horizontalLayout_9->addWidget(Operator_Name_lineEdit);


        verticalLayout_7->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        sizePolicy2.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy2);
        label_7->setMinimumSize(QSize(80, 0));
        label_7->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_10->addWidget(label_7);

        Operator_Station_ComboBox = new QComboBox(groupBox_2);
        Operator_Station_ComboBox->setObjectName(QString::fromUtf8("Operator_Station_ComboBox"));
        sizePolicy.setHeightForWidth(Operator_Station_ComboBox->sizePolicy().hasHeightForWidth());
        Operator_Station_ComboBox->setSizePolicy(sizePolicy);

        horizontalLayout_10->addWidget(Operator_Station_ComboBox);


        verticalLayout_7->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        sizePolicy2.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy2);
        label_8->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_11->addWidget(label_8);

        Operator_Type_ComboBox = new QComboBox(groupBox_2);
        Operator_Type_ComboBox->setObjectName(QString::fromUtf8("Operator_Type_ComboBox"));
        sizePolicy.setHeightForWidth(Operator_Type_ComboBox->sizePolicy().hasHeightForWidth());
        Operator_Type_ComboBox->setSizePolicy(sizePolicy);

        horizontalLayout_11->addWidget(Operator_Type_ComboBox);


        verticalLayout_7->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        sizePolicy2.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy2);
        label_9->setMinimumSize(QSize(80, 0));
        label_9->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_12->addWidget(label_9);

        Operator_Pos_lineEdit = new QLineEdit(groupBox_2);
        Operator_Pos_lineEdit->setObjectName(QString::fromUtf8("Operator_Pos_lineEdit"));
        sizePolicy.setHeightForWidth(Operator_Pos_lineEdit->sizePolicy().hasHeightForWidth());
        Operator_Pos_lineEdit->setSizePolicy(sizePolicy);

        horizontalLayout_12->addWidget(Operator_Pos_lineEdit);


        verticalLayout_7->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        sizePolicy2.setHeightForWidth(label_10->sizePolicy().hasHeightForWidth());
        label_10->setSizePolicy(sizePolicy2);
        label_10->setMinimumSize(QSize(80, 0));
        label_10->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_13->addWidget(label_10);

        Operator_Param_ineEdit_1 = new QLineEdit(groupBox_2);
        Operator_Param_ineEdit_1->setObjectName(QString::fromUtf8("Operator_Param_ineEdit_1"));
        sizePolicy.setHeightForWidth(Operator_Param_ineEdit_1->sizePolicy().hasHeightForWidth());
        Operator_Param_ineEdit_1->setSizePolicy(sizePolicy);

        horizontalLayout_13->addWidget(Operator_Param_ineEdit_1);


        verticalLayout_7->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        sizePolicy2.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy2);
        label_11->setMinimumSize(QSize(80, 0));
        label_11->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_14->addWidget(label_11);

        Operator_Param_ineEdit_2 = new QLineEdit(groupBox_2);
        Operator_Param_ineEdit_2->setObjectName(QString::fromUtf8("Operator_Param_ineEdit_2"));
        sizePolicy.setHeightForWidth(Operator_Param_ineEdit_2->sizePolicy().hasHeightForWidth());
        Operator_Param_ineEdit_2->setSizePolicy(sizePolicy);

        horizontalLayout_14->addWidget(Operator_Param_ineEdit_2);


        verticalLayout_7->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        sizePolicy2.setHeightForWidth(label_13->sizePolicy().hasHeightForWidth());
        label_13->setSizePolicy(sizePolicy2);
        label_13->setMinimumSize(QSize(80, 0));
        label_13->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_15->addWidget(label_13);

        Operator_Param_ineEdit_3 = new QLineEdit(groupBox_2);
        Operator_Param_ineEdit_3->setObjectName(QString::fromUtf8("Operator_Param_ineEdit_3"));
        sizePolicy.setHeightForWidth(Operator_Param_ineEdit_3->sizePolicy().hasHeightForWidth());
        Operator_Param_ineEdit_3->setSizePolicy(sizePolicy);

        horizontalLayout_15->addWidget(Operator_Param_ineEdit_3);


        verticalLayout_7->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        sizePolicy2.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy2);
        label_12->setMinimumSize(QSize(80, 0));
        label_12->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_16->addWidget(label_12);

        Operator_Param_ineEdit_4 = new QLineEdit(groupBox_2);
        Operator_Param_ineEdit_4->setObjectName(QString::fromUtf8("Operator_Param_ineEdit_4"));
        sizePolicy.setHeightForWidth(Operator_Param_ineEdit_4->sizePolicy().hasHeightForWidth());
        Operator_Param_ineEdit_4->setSizePolicy(sizePolicy);

        horizontalLayout_16->addWidget(Operator_Param_ineEdit_4);


        verticalLayout_7->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        sizePolicy2.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy2);
        label_14->setMinimumSize(QSize(80, 0));
        label_14->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_17->addWidget(label_14);

        Operator_Param_ineEdit_5 = new QLineEdit(groupBox_2);
        Operator_Param_ineEdit_5->setObjectName(QString::fromUtf8("Operator_Param_ineEdit_5"));
        sizePolicy.setHeightForWidth(Operator_Param_ineEdit_5->sizePolicy().hasHeightForWidth());
        Operator_Param_ineEdit_5->setSizePolicy(sizePolicy);

        horizontalLayout_17->addWidget(Operator_Param_ineEdit_5);


        verticalLayout_7->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        Btn_Ensure = new QPushButton(groupBox_2);
        Btn_Ensure->setObjectName(QString::fromUtf8("Btn_Ensure"));
        sizePolicy.setHeightForWidth(Btn_Ensure->sizePolicy().hasHeightForWidth());
        Btn_Ensure->setSizePolicy(sizePolicy);

        horizontalLayout_18->addWidget(Btn_Ensure);

        Btn_Cancle = new QPushButton(groupBox_2);
        Btn_Cancle->setObjectName(QString::fromUtf8("Btn_Cancle"));
        sizePolicy.setHeightForWidth(Btn_Cancle->sizePolicy().hasHeightForWidth());
        Btn_Cancle->setSizePolicy(sizePolicy);

        horizontalLayout_18->addWidget(Btn_Cancle);


        verticalLayout_7->addLayout(horizontalLayout_18);

        textEdit = new QTextEdit(groupBox_2);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        sizePolicy.setHeightForWidth(textEdit->sizePolicy().hasHeightForWidth());
        textEdit->setSizePolicy(sizePolicy);

        verticalLayout_7->addWidget(textEdit);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_2);


        horizontalLayout->addWidget(groupBox_2);

        horizontalLayout->setStretch(1, 1);

        verticalLayout->addLayout(horizontalLayout);

        tableView = new QTableView(widget_2);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setMinimumSize(QSize(0, 150));

        verticalLayout->addWidget(tableView);

        splitter_2->addWidget(widget_2);

        horizontalLayout_6->addWidget(splitter_2);


        retranslateUi(CProcessWidget);

        QMetaObject::connectSlotsByName(CProcessWidget);
    } // setupUi

    void retranslateUi(QWidget *CProcessWidget)
    {
        CProcessWidget->setWindowTitle(QApplication::translate("CProcessWidget", "CProcessWidget", nullptr));
        groupBox->setTitle(QString());
        label_2->setText(QApplication::translate("CProcessWidget", "\346\265\201\347\250\213\346\240\207\350\257\206:", nullptr));
        label->setText(QApplication::translate("CProcessWidget", "\346\265\201\347\250\213\345\220\215  :", nullptr));
        label_3->setText(QApplication::translate("CProcessWidget", "\346\255\245\351\252\244\346\240\207\350\257\206:", nullptr));
        label_4->setText(QApplication::translate("CProcessWidget", "\346\255\245\351\252\244\345\220\215\347\247\260:", nullptr));
        Step_End_CodeSuport_CheckBox->setText(QApplication::translate("CProcessWidget", "\346\255\245\351\252\244\347\273\223\346\235\237\346\211\247\350\241\214\344\273\243\347\240\201", nullptr));
        EnsureStepBtn->setText(QApplication::translate("CProcessWidget", "\347\241\256\350\256\244", nullptr));
        RevokeStepBtn->setText(QApplication::translate("CProcessWidget", "\345\217\226\346\266\210", nullptr));
        groupBox_4->setTitle(QApplication::translate("CProcessWidget", "CurrentState", nullptr));
        label_15->setText(QApplication::translate("CProcessWidget", "\345\267\245\344\275\234\346\265\201\347\250\213", nullptr));
        ProcExitBtn->setText(QApplication::translate("CProcessWidget", "\351\200\200\345\207\272", nullptr));
        label_16->setText(QApplication::translate("CProcessWidget", "CurrentStep:", nullptr));
        StepLabel->setText(QApplication::translate("CProcessWidget", "0", nullptr));
        groupBox_3->setTitle(QApplication::translate("CProcessWidget", "\347\274\226\350\276\221", nullptr));
        Load_Proce_BTN->setText(QApplication::translate("CProcessWidget", "\345\212\240\350\275\275", nullptr));
        Refresh_BTN->setText(QApplication::translate("CProcessWidget", "\345\210\267\346\226\260", nullptr));
        Save_As_BTN->setText(QApplication::translate("CProcessWidget", "\345\217\246\345\255\230", nullptr));
#ifndef QT_NO_SHORTCUT
        Save_As_BTN->setShortcut(QString());
#endif // QT_NO_SHORTCUT
        ReSort_BTN->setText(QApplication::translate("CProcessWidget", "\346\216\222\345\272\217", nullptr));
        Save_BTN->setText(QApplication::translate("CProcessWidget", "\344\277\235\345\255\230", nullptr));
#ifndef QT_NO_SHORTCUT
        Save_BTN->setShortcut(QApplication::translate("CProcessWidget", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        ReStateProcBtn->setText(QApplication::translate("CProcessWidget", "\346\265\201\347\250\213\345\274\272\345\210\266\351\200\200\345\207\272", nullptr));
        groupBox_2->setTitle(QApplication::translate("CProcessWidget", "\346\223\215\344\275\234", nullptr));
        label_5->setText(QApplication::translate("CProcessWidget", "\346\223\215\344\275\234\346\240\207\350\257\206", nullptr));
        label_6->setText(QApplication::translate("CProcessWidget", "\346\223\215\344\275\234\345\220\215\347\247\260", nullptr));
        label_7->setText(QApplication::translate("CProcessWidget", "\345\267\245\347\253\231\345\220\215\347\247\260", nullptr));
        label_8->setText(QApplication::translate("CProcessWidget", "\346\223\215\344\275\234\347\261\273\345\236\213   ", nullptr));
        label_9->setText(QApplication::translate("CProcessWidget", "\347\202\271\344\275\215\345\210\227\350\241\250", nullptr));
        label_10->setText(QApplication::translate("CProcessWidget", "\345\217\202\346\225\2601", nullptr));
        label_11->setText(QApplication::translate("CProcessWidget", "\345\217\202\346\225\2602", nullptr));
        label_13->setText(QApplication::translate("CProcessWidget", "\345\217\202\346\225\2603", nullptr));
        label_12->setText(QApplication::translate("CProcessWidget", "\345\217\202\346\225\2604", nullptr));
        label_14->setText(QApplication::translate("CProcessWidget", "\345\217\202\346\225\2605", nullptr));
        Btn_Ensure->setText(QApplication::translate("CProcessWidget", "\347\241\256\350\256\244", nullptr));
        Btn_Cancle->setText(QApplication::translate("CProcessWidget", "\345\217\226\346\266\210", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CProcessWidget: public Ui_CProcessWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CPROCESSWIDGET_H
