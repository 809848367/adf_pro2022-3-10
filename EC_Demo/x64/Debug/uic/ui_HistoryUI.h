/********************************************************************************
** Form generated from reading UI file 'HistoryUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HISTORYUI_H
#define UI_HISTORYUI_H

#include <QFigure.h>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HistoryUI
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *SN;
    QPushButton *SNSearch;
    QLabel *label_3;
    QLineEdit *BarCode;
    QPushButton *BarCodeSearch;
    QCheckBox *OnlyFail;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_4;
    QDateTimeEdit *DateTimeStart;
    QLabel *label_5;
    QDateTimeEdit *DateTimeStop;
    QPushButton *DateTimeSearch;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QTreeWidget *DataList;
    QFigure *IMShow;
    QLineEdit *DataShow;

    void setupUi(QWidget *HistoryUI)
    {
        if (HistoryUI->objectName().isEmpty())
            HistoryUI->setObjectName(QString::fromUtf8("HistoryUI"));
        HistoryUI->resize(1030, 598);
        verticalLayout = new QVBoxLayout(HistoryUI);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(HistoryUI);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(70, 30));
        label_2->setMaximumSize(QSize(70, 30));

        horizontalLayout->addWidget(label_2);

        SN = new QLineEdit(HistoryUI);
        SN->setObjectName(QString::fromUtf8("SN"));
        SN->setMinimumSize(QSize(110, 30));
        SN->setMaximumSize(QSize(110, 30));

        horizontalLayout->addWidget(SN);

        SNSearch = new QPushButton(HistoryUI);
        SNSearch->setObjectName(QString::fromUtf8("SNSearch"));
        SNSearch->setMinimumSize(QSize(80, 30));
        SNSearch->setMaximumSize(QSize(80, 30));

        horizontalLayout->addWidget(SNSearch);

        label_3 = new QLabel(HistoryUI);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(70, 30));
        label_3->setMaximumSize(QSize(70, 30));

        horizontalLayout->addWidget(label_3);

        BarCode = new QLineEdit(HistoryUI);
        BarCode->setObjectName(QString::fromUtf8("BarCode"));
        BarCode->setMinimumSize(QSize(110, 30));
        BarCode->setMaximumSize(QSize(110, 30));

        horizontalLayout->addWidget(BarCode);

        BarCodeSearch = new QPushButton(HistoryUI);
        BarCodeSearch->setObjectName(QString::fromUtf8("BarCodeSearch"));
        BarCodeSearch->setMinimumSize(QSize(80, 30));
        BarCodeSearch->setMaximumSize(QSize(80, 30));

        horizontalLayout->addWidget(BarCodeSearch);

        OnlyFail = new QCheckBox(HistoryUI);
        OnlyFail->setObjectName(QString::fromUtf8("OnlyFail"));
        OnlyFail->setMinimumSize(QSize(100, 30));
        OnlyFail->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(OnlyFail);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_4 = new QLabel(HistoryUI);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(70, 30));
        label_4->setMaximumSize(QSize(70, 30));

        horizontalLayout_2->addWidget(label_4);

        DateTimeStart = new QDateTimeEdit(HistoryUI);
        DateTimeStart->setObjectName(QString::fromUtf8("DateTimeStart"));
        DateTimeStart->setMinimumSize(QSize(185, 30));
        DateTimeStart->setMaximumSize(QSize(185, 30));

        horizontalLayout_2->addWidget(DateTimeStart);

        label_5 = new QLabel(HistoryUI);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(10, 30));
        label_5->setMaximumSize(QSize(10, 30));

        horizontalLayout_2->addWidget(label_5);

        DateTimeStop = new QDateTimeEdit(HistoryUI);
        DateTimeStop->setObjectName(QString::fromUtf8("DateTimeStop"));
        DateTimeStop->setMinimumSize(QSize(185, 30));
        DateTimeStop->setMaximumSize(QSize(185, 30));

        horizontalLayout_2->addWidget(DateTimeStop);

        DateTimeSearch = new QPushButton(HistoryUI);
        DateTimeSearch->setObjectName(QString::fromUtf8("DateTimeSearch"));
        DateTimeSearch->setMinimumSize(QSize(80, 30));
        DateTimeSearch->setMaximumSize(QSize(80, 30));

        horizontalLayout_2->addWidget(DateTimeSearch);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(10);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        DataList = new QTreeWidget(HistoryUI);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(1, QString::fromUtf8("2"));
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        DataList->setHeaderItem(__qtreewidgetitem);
        DataList->setObjectName(QString::fromUtf8("DataList"));
        DataList->setMinimumSize(QSize(300, 0));
        DataList->setColumnCount(2);

        horizontalLayout_3->addWidget(DataList);

        IMShow = new QFigure(HistoryUI);
        IMShow->setObjectName(QString::fromUtf8("IMShow"));
        IMShow->setMinimumSize(QSize(400, 300));
        IMShow->setFrameShape(QFrame::Box);

        horizontalLayout_3->addWidget(IMShow);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 3);

        verticalLayout->addLayout(horizontalLayout_3);

        DataShow = new QLineEdit(HistoryUI);
        DataShow->setObjectName(QString::fromUtf8("DataShow"));
        DataShow->setMinimumSize(QSize(0, 30));
        DataShow->setMaximumSize(QSize(16777215, 30));

        verticalLayout->addWidget(DataShow);


        retranslateUi(HistoryUI);

        QMetaObject::connectSlotsByName(HistoryUI);
    } // setupUi

    void retranslateUi(QWidget *HistoryUI)
    {
        HistoryUI->setWindowTitle(QApplication::translate("HistoryUI", "Form", nullptr));
        label_2->setText(QApplication::translate("HistoryUI", "SN", nullptr));
        SNSearch->setText(QApplication::translate("HistoryUI", "Search", nullptr));
        label_3->setText(QApplication::translate("HistoryUI", "BarCode", nullptr));
        BarCodeSearch->setText(QApplication::translate("HistoryUI", "Search", nullptr));
        OnlyFail->setText(QApplication::translate("HistoryUI", "Only Fail", nullptr));
        label_4->setText(QApplication::translate("HistoryUI", "DateTime", nullptr));
        DateTimeStart->setDisplayFormat(QApplication::translate("HistoryUI", "yyyy/MM/dd HH:mm:ss", nullptr));
        label_5->setText(QApplication::translate("HistoryUI", "-", nullptr));
        DateTimeStop->setDisplayFormat(QApplication::translate("HistoryUI", "yyyy/MM/dd HH:mm:ss", nullptr));
        DateTimeSearch->setText(QApplication::translate("HistoryUI", "Search", nullptr));
        IMShow->setText(QApplication::translate("HistoryUI", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HistoryUI: public Ui_HistoryUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HISTORYUI_H
