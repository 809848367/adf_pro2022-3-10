/********************************************************************************
** Form generated from reading UI file 'RobotUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ROBOTUI_H
#define UI_ROBOTUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RobotUI
{
public:
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *StatusGroup;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_7;
    QLineEdit *robotMode;
    QLabel *label_9;
    QLineEdit *robotState;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_10;
    QLineEdit *servoReady;
    QLabel *label_11;
    QLineEdit *can_motor_run;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *ResetBtn;
    QPushButton *EnableBtn;
    QPushButton *DisableBtn;
    QSpacerItem *horizontalSpacer_10;
    QGroupBox *ManualGroup;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_12;
    QDoubleSpinBox *Step;
    QLabel *label_8;
    QDoubleSpinBox *Speed;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label;
    QLineEdit *X_Pos;
    QPushButton *X_Add;
    QPushButton *X_Minus;
    QLineEdit *X_Move;
    QPushButton *X_MoveBtn;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_2;
    QLineEdit *Y_Pos;
    QPushButton *Y_Add;
    QPushButton *Y_Minus;
    QLineEdit *Y_Move;
    QPushButton *Y_MoveBtn;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLineEdit *Z_Pos;
    QPushButton *Z_Add;
    QPushButton *Z_Minus;
    QLineEdit *Z_Move;
    QPushButton *Z_MoveBtn;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QLineEdit *RX_Pos;
    QPushButton *RX_Add;
    QPushButton *RX_Minus;
    QLineEdit *RX_Move;
    QPushButton *RX_MoveBtn;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_5;
    QLineEdit *RY_Pos;
    QPushButton *RY_Add;
    QPushButton *RY_Minus;
    QLineEdit *RY_Move;
    QPushButton *RY_MoveBtn;
    QHBoxLayout *horizontalLayout;
    QLabel *label_6;
    QLineEdit *RZ_Pos;
    QPushButton *RZ_Add;
    QPushButton *RZ_Minus;
    QLineEdit *RZ_Move;
    QPushButton *RZ_MoveBtn;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_13;
    QComboBox *TeachList;
    QRadioButton *TeachPointBtn;
    QRadioButton *TeachWayBtn;
    QPushButton *TeachAdd;
    QPushButton *TeachDel;
    QTableWidget *TeachData;
    QHBoxLayout *horizontalLayout_11;
    QPushButton *TeachAddPoint;
    QPushButton *TeachDelPoint;
    QPushButton *MovewayPoint;

    void setupUi(QWidget *RobotUI)
    {
        if (RobotUI->objectName().isEmpty())
            RobotUI->setObjectName(QString::fromUtf8("RobotUI"));
        RobotUI->resize(1104, 567);
        horizontalLayout_12 = new QHBoxLayout(RobotUI);
        horizontalLayout_12->setSpacing(10);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(10, 10, 10, 10);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(10);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        StatusGroup = new QGroupBox(RobotUI);
        StatusGroup->setObjectName(QString::fromUtf8("StatusGroup"));
        verticalLayout_2 = new QVBoxLayout(StatusGroup);
        verticalLayout_2->setSpacing(10);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, 10, 10, 10);
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(10);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_7 = new QLabel(StatusGroup);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(120, 30));
        label_7->setMaximumSize(QSize(120, 30));

        horizontalLayout_8->addWidget(label_7);

        robotMode = new QLineEdit(StatusGroup);
        robotMode->setObjectName(QString::fromUtf8("robotMode"));
        robotMode->setMinimumSize(QSize(90, 30));
        robotMode->setMaximumSize(QSize(90, 30));
        robotMode->setReadOnly(true);

        horizontalLayout_8->addWidget(robotMode);

        label_9 = new QLabel(StatusGroup);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(120, 30));
        label_9->setMaximumSize(QSize(120, 30));

        horizontalLayout_8->addWidget(label_9);

        robotState = new QLineEdit(StatusGroup);
        robotState->setObjectName(QString::fromUtf8("robotState"));
        robotState->setMinimumSize(QSize(90, 30));
        robotState->setMaximumSize(QSize(90, 30));
        robotState->setReadOnly(true);

        horizontalLayout_8->addWidget(robotState);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(10);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_10 = new QLabel(StatusGroup);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(120, 30));
        label_10->setMaximumSize(QSize(120, 30));

        horizontalLayout_9->addWidget(label_10);

        servoReady = new QLineEdit(StatusGroup);
        servoReady->setObjectName(QString::fromUtf8("servoReady"));
        servoReady->setMinimumSize(QSize(90, 30));
        servoReady->setMaximumSize(QSize(90, 30));
        servoReady->setReadOnly(true);

        horizontalLayout_9->addWidget(servoReady);

        label_11 = new QLabel(StatusGroup);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(120, 30));
        label_11->setMaximumSize(QSize(120, 30));

        horizontalLayout_9->addWidget(label_11);

        can_motor_run = new QLineEdit(StatusGroup);
        can_motor_run->setObjectName(QString::fromUtf8("can_motor_run"));
        can_motor_run->setMinimumSize(QSize(90, 30));
        can_motor_run->setMaximumSize(QSize(90, 30));
        can_motor_run->setReadOnly(true);

        horizontalLayout_9->addWidget(can_motor_run);


        verticalLayout_2->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(10);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        ResetBtn = new QPushButton(StatusGroup);
        ResetBtn->setObjectName(QString::fromUtf8("ResetBtn"));

        horizontalLayout_10->addWidget(ResetBtn);

        EnableBtn = new QPushButton(StatusGroup);
        EnableBtn->setObjectName(QString::fromUtf8("EnableBtn"));

        horizontalLayout_10->addWidget(EnableBtn);

        DisableBtn = new QPushButton(StatusGroup);
        DisableBtn->setObjectName(QString::fromUtf8("DisableBtn"));

        horizontalLayout_10->addWidget(DisableBtn);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_10);


        verticalLayout_2->addLayout(horizontalLayout_10);


        verticalLayout_3->addWidget(StatusGroup);

        ManualGroup = new QGroupBox(RobotUI);
        ManualGroup->setObjectName(QString::fromUtf8("ManualGroup"));
        verticalLayout = new QVBoxLayout(ManualGroup);
        verticalLayout->setSpacing(10);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(10, 10, 10, 10);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(10);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_12 = new QLabel(ManualGroup);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(50, 30));
        label_12->setMaximumSize(QSize(50, 30));

        horizontalLayout_7->addWidget(label_12);

        Step = new QDoubleSpinBox(ManualGroup);
        Step->setObjectName(QString::fromUtf8("Step"));
        Step->setMinimumSize(QSize(160, 30));
        Step->setMaximumSize(QSize(160, 30));
        Step->setDecimals(3);
        Step->setMinimum(0.001000000000000);
        Step->setMaximum(10.000000000000000);
        Step->setValue(1.000000000000000);

        horizontalLayout_7->addWidget(Step);

        label_8 = new QLabel(ManualGroup);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(50, 30));
        label_8->setMaximumSize(QSize(50, 30));

        horizontalLayout_7->addWidget(label_8);

        Speed = new QDoubleSpinBox(ManualGroup);
        Speed->setObjectName(QString::fromUtf8("Speed"));
        Speed->setMinimumSize(QSize(160, 30));
        Speed->setMaximumSize(QSize(160, 30));
        Speed->setMinimum(0.010000000000000);
        Speed->setMaximum(100.000000000000000);
        Speed->setValue(20.000000000000000);

        horizontalLayout_7->addWidget(Speed);

        horizontalLayout_7->setStretch(1, 1);
        horizontalLayout_7->setStretch(3, 1);

        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(10);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label = new QLabel(ManualGroup);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(30, 30));
        label->setMaximumSize(QSize(30, 30));

        horizontalLayout_6->addWidget(label);

        X_Pos = new QLineEdit(ManualGroup);
        X_Pos->setObjectName(QString::fromUtf8("X_Pos"));
        X_Pos->setMinimumSize(QSize(100, 30));
        X_Pos->setMaximumSize(QSize(100, 30));
        X_Pos->setReadOnly(true);

        horizontalLayout_6->addWidget(X_Pos);

        X_Add = new QPushButton(ManualGroup);
        X_Add->setObjectName(QString::fromUtf8("X_Add"));
        X_Add->setMinimumSize(QSize(30, 30));
        X_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout_6->addWidget(X_Add);

        X_Minus = new QPushButton(ManualGroup);
        X_Minus->setObjectName(QString::fromUtf8("X_Minus"));
        X_Minus->setMinimumSize(QSize(30, 30));
        X_Minus->setMaximumSize(QSize(30, 30));

        horizontalLayout_6->addWidget(X_Minus);

        X_Move = new QLineEdit(ManualGroup);
        X_Move->setObjectName(QString::fromUtf8("X_Move"));
        X_Move->setMinimumSize(QSize(100, 30));
        X_Move->setMaximumSize(QSize(100, 30));

        horizontalLayout_6->addWidget(X_Move);

        X_MoveBtn = new QPushButton(ManualGroup);
        X_MoveBtn->setObjectName(QString::fromUtf8("X_MoveBtn"));
        X_MoveBtn->setMinimumSize(QSize(110, 30));
        X_MoveBtn->setMaximumSize(QSize(110, 30));

        horizontalLayout_6->addWidget(X_MoveBtn);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(10);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_2 = new QLabel(ManualGroup);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(30, 30));
        label_2->setMaximumSize(QSize(30, 30));

        horizontalLayout_5->addWidget(label_2);

        Y_Pos = new QLineEdit(ManualGroup);
        Y_Pos->setObjectName(QString::fromUtf8("Y_Pos"));
        Y_Pos->setMinimumSize(QSize(100, 30));
        Y_Pos->setMaximumSize(QSize(100, 30));
        Y_Pos->setReadOnly(true);

        horizontalLayout_5->addWidget(Y_Pos);

        Y_Add = new QPushButton(ManualGroup);
        Y_Add->setObjectName(QString::fromUtf8("Y_Add"));
        Y_Add->setMinimumSize(QSize(30, 30));
        Y_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout_5->addWidget(Y_Add);

        Y_Minus = new QPushButton(ManualGroup);
        Y_Minus->setObjectName(QString::fromUtf8("Y_Minus"));
        Y_Minus->setMinimumSize(QSize(30, 30));
        Y_Minus->setMaximumSize(QSize(30, 30));

        horizontalLayout_5->addWidget(Y_Minus);

        Y_Move = new QLineEdit(ManualGroup);
        Y_Move->setObjectName(QString::fromUtf8("Y_Move"));
        Y_Move->setMinimumSize(QSize(100, 30));
        Y_Move->setMaximumSize(QSize(100, 30));

        horizontalLayout_5->addWidget(Y_Move);

        Y_MoveBtn = new QPushButton(ManualGroup);
        Y_MoveBtn->setObjectName(QString::fromUtf8("Y_MoveBtn"));
        Y_MoveBtn->setMinimumSize(QSize(110, 30));
        Y_MoveBtn->setMaximumSize(QSize(110, 30));

        horizontalLayout_5->addWidget(Y_MoveBtn);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(10);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(ManualGroup);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(30, 30));
        label_3->setMaximumSize(QSize(30, 30));

        horizontalLayout_4->addWidget(label_3);

        Z_Pos = new QLineEdit(ManualGroup);
        Z_Pos->setObjectName(QString::fromUtf8("Z_Pos"));
        Z_Pos->setMinimumSize(QSize(100, 30));
        Z_Pos->setMaximumSize(QSize(100, 30));
        Z_Pos->setReadOnly(true);

        horizontalLayout_4->addWidget(Z_Pos);

        Z_Add = new QPushButton(ManualGroup);
        Z_Add->setObjectName(QString::fromUtf8("Z_Add"));
        Z_Add->setMinimumSize(QSize(30, 30));
        Z_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout_4->addWidget(Z_Add);

        Z_Minus = new QPushButton(ManualGroup);
        Z_Minus->setObjectName(QString::fromUtf8("Z_Minus"));
        Z_Minus->setMinimumSize(QSize(30, 30));
        Z_Minus->setMaximumSize(QSize(30, 30));

        horizontalLayout_4->addWidget(Z_Minus);

        Z_Move = new QLineEdit(ManualGroup);
        Z_Move->setObjectName(QString::fromUtf8("Z_Move"));
        Z_Move->setMinimumSize(QSize(100, 30));
        Z_Move->setMaximumSize(QSize(100, 30));

        horizontalLayout_4->addWidget(Z_Move);

        Z_MoveBtn = new QPushButton(ManualGroup);
        Z_MoveBtn->setObjectName(QString::fromUtf8("Z_MoveBtn"));
        Z_MoveBtn->setMinimumSize(QSize(110, 30));
        Z_MoveBtn->setMaximumSize(QSize(110, 30));

        horizontalLayout_4->addWidget(Z_MoveBtn);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(10);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_4 = new QLabel(ManualGroup);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(30, 30));
        label_4->setMaximumSize(QSize(30, 30));

        horizontalLayout_3->addWidget(label_4);

        RX_Pos = new QLineEdit(ManualGroup);
        RX_Pos->setObjectName(QString::fromUtf8("RX_Pos"));
        RX_Pos->setMinimumSize(QSize(100, 30));
        RX_Pos->setMaximumSize(QSize(100, 30));
        RX_Pos->setReadOnly(true);

        horizontalLayout_3->addWidget(RX_Pos);

        RX_Add = new QPushButton(ManualGroup);
        RX_Add->setObjectName(QString::fromUtf8("RX_Add"));
        RX_Add->setMinimumSize(QSize(30, 30));
        RX_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout_3->addWidget(RX_Add);

        RX_Minus = new QPushButton(ManualGroup);
        RX_Minus->setObjectName(QString::fromUtf8("RX_Minus"));
        RX_Minus->setMinimumSize(QSize(30, 30));
        RX_Minus->setMaximumSize(QSize(30, 30));

        horizontalLayout_3->addWidget(RX_Minus);

        RX_Move = new QLineEdit(ManualGroup);
        RX_Move->setObjectName(QString::fromUtf8("RX_Move"));
        RX_Move->setMinimumSize(QSize(100, 30));
        RX_Move->setMaximumSize(QSize(100, 30));

        horizontalLayout_3->addWidget(RX_Move);

        RX_MoveBtn = new QPushButton(ManualGroup);
        RX_MoveBtn->setObjectName(QString::fromUtf8("RX_MoveBtn"));
        RX_MoveBtn->setMinimumSize(QSize(110, 30));
        RX_MoveBtn->setMaximumSize(QSize(110, 30));

        horizontalLayout_3->addWidget(RX_MoveBtn);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_5 = new QLabel(ManualGroup);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(30, 30));
        label_5->setMaximumSize(QSize(30, 30));

        horizontalLayout_2->addWidget(label_5);

        RY_Pos = new QLineEdit(ManualGroup);
        RY_Pos->setObjectName(QString::fromUtf8("RY_Pos"));
        RY_Pos->setMinimumSize(QSize(100, 30));
        RY_Pos->setMaximumSize(QSize(100, 30));
        RY_Pos->setReadOnly(true);

        horizontalLayout_2->addWidget(RY_Pos);

        RY_Add = new QPushButton(ManualGroup);
        RY_Add->setObjectName(QString::fromUtf8("RY_Add"));
        RY_Add->setMinimumSize(QSize(30, 30));
        RY_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout_2->addWidget(RY_Add);

        RY_Minus = new QPushButton(ManualGroup);
        RY_Minus->setObjectName(QString::fromUtf8("RY_Minus"));
        RY_Minus->setMinimumSize(QSize(30, 30));
        RY_Minus->setMaximumSize(QSize(30, 30));

        horizontalLayout_2->addWidget(RY_Minus);

        RY_Move = new QLineEdit(ManualGroup);
        RY_Move->setObjectName(QString::fromUtf8("RY_Move"));
        RY_Move->setMinimumSize(QSize(100, 30));
        RY_Move->setMaximumSize(QSize(100, 30));

        horizontalLayout_2->addWidget(RY_Move);

        RY_MoveBtn = new QPushButton(ManualGroup);
        RY_MoveBtn->setObjectName(QString::fromUtf8("RY_MoveBtn"));
        RY_MoveBtn->setMinimumSize(QSize(110, 30));
        RY_MoveBtn->setMaximumSize(QSize(110, 30));

        horizontalLayout_2->addWidget(RY_MoveBtn);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_6 = new QLabel(ManualGroup);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(30, 30));
        label_6->setMaximumSize(QSize(30, 30));

        horizontalLayout->addWidget(label_6);

        RZ_Pos = new QLineEdit(ManualGroup);
        RZ_Pos->setObjectName(QString::fromUtf8("RZ_Pos"));
        RZ_Pos->setMinimumSize(QSize(100, 30));
        RZ_Pos->setMaximumSize(QSize(100, 30));
        RZ_Pos->setReadOnly(true);

        horizontalLayout->addWidget(RZ_Pos);

        RZ_Add = new QPushButton(ManualGroup);
        RZ_Add->setObjectName(QString::fromUtf8("RZ_Add"));
        RZ_Add->setMinimumSize(QSize(30, 30));
        RZ_Add->setMaximumSize(QSize(30, 30));

        horizontalLayout->addWidget(RZ_Add);

        RZ_Minus = new QPushButton(ManualGroup);
        RZ_Minus->setObjectName(QString::fromUtf8("RZ_Minus"));
        RZ_Minus->setMinimumSize(QSize(30, 30));
        RZ_Minus->setMaximumSize(QSize(30, 30));

        horizontalLayout->addWidget(RZ_Minus);

        RZ_Move = new QLineEdit(ManualGroup);
        RZ_Move->setObjectName(QString::fromUtf8("RZ_Move"));
        RZ_Move->setMinimumSize(QSize(100, 30));
        RZ_Move->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(RZ_Move);

        RZ_MoveBtn = new QPushButton(ManualGroup);
        RZ_MoveBtn->setObjectName(QString::fromUtf8("RZ_MoveBtn"));
        RZ_MoveBtn->setMinimumSize(QSize(110, 30));
        RZ_MoveBtn->setMaximumSize(QSize(110, 30));

        horizontalLayout->addWidget(RZ_MoveBtn);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_3->addWidget(ManualGroup);

        verticalSpacer = new QSpacerItem(20, 44, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        horizontalLayout_12->addLayout(verticalLayout_3);

        groupBox = new QGroupBox(RobotUI);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(10);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        TeachList = new QComboBox(groupBox);
        TeachList->setObjectName(QString::fromUtf8("TeachList"));
        TeachList->setMinimumSize(QSize(0, 30));
        TeachList->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_13->addWidget(TeachList);

        TeachPointBtn = new QRadioButton(groupBox);
        TeachPointBtn->setObjectName(QString::fromUtf8("TeachPointBtn"));
        TeachPointBtn->setMinimumSize(QSize(70, 30));
        TeachPointBtn->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_13->addWidget(TeachPointBtn);

        TeachWayBtn = new QRadioButton(groupBox);
        TeachWayBtn->setObjectName(QString::fromUtf8("TeachWayBtn"));
        TeachWayBtn->setMinimumSize(QSize(70, 30));
        TeachWayBtn->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_13->addWidget(TeachWayBtn);

        TeachAdd = new QPushButton(groupBox);
        TeachAdd->setObjectName(QString::fromUtf8("TeachAdd"));
        TeachAdd->setMinimumSize(QSize(90, 30));
        TeachAdd->setMaximumSize(QSize(90, 30));

        horizontalLayout_13->addWidget(TeachAdd);

        TeachDel = new QPushButton(groupBox);
        TeachDel->setObjectName(QString::fromUtf8("TeachDel"));
        TeachDel->setMinimumSize(QSize(90, 30));
        TeachDel->setMaximumSize(QSize(90, 30));

        horizontalLayout_13->addWidget(TeachDel);

        horizontalLayout_13->setStretch(0, 1);

        verticalLayout_4->addLayout(horizontalLayout_13);

        TeachData = new QTableWidget(groupBox);
        TeachData->setObjectName(QString::fromUtf8("TeachData"));

        verticalLayout_4->addWidget(TeachData);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        TeachAddPoint = new QPushButton(groupBox);
        TeachAddPoint->setObjectName(QString::fromUtf8("TeachAddPoint"));
        TeachAddPoint->setMinimumSize(QSize(140, 30));
        TeachAddPoint->setMaximumSize(QSize(140, 30));

        horizontalLayout_11->addWidget(TeachAddPoint);

        TeachDelPoint = new QPushButton(groupBox);
        TeachDelPoint->setObjectName(QString::fromUtf8("TeachDelPoint"));
        TeachDelPoint->setMinimumSize(QSize(140, 30));
        TeachDelPoint->setMaximumSize(QSize(140, 30));

        horizontalLayout_11->addWidget(TeachDelPoint);

        MovewayPoint = new QPushButton(groupBox);
        MovewayPoint->setObjectName(QString::fromUtf8("MovewayPoint"));
        MovewayPoint->setMinimumSize(QSize(140, 30));
        MovewayPoint->setMaximumSize(QSize(140, 30));

        horizontalLayout_11->addWidget(MovewayPoint);


        verticalLayout_4->addLayout(horizontalLayout_11);


        horizontalLayout_12->addWidget(groupBox);

        horizontalLayout_12->setStretch(1, 1);

        retranslateUi(RobotUI);

        QMetaObject::connectSlotsByName(RobotUI);
    } // setupUi

    void retranslateUi(QWidget *RobotUI)
    {
        RobotUI->setWindowTitle(QApplication::translate("RobotUI", "Form", nullptr));
        StatusGroup->setTitle(QApplication::translate("RobotUI", "Status", nullptr));
        label_7->setText(QApplication::translate("RobotUI", "robotMode", nullptr));
        label_9->setText(QApplication::translate("RobotUI", "robotState", nullptr));
        label_10->setText(QApplication::translate("RobotUI", "servoReady", nullptr));
        label_11->setText(QApplication::translate("RobotUI", "can_motor_run", nullptr));
        ResetBtn->setText(QApplication::translate("RobotUI", "Reset", nullptr));
        EnableBtn->setText(QApplication::translate("RobotUI", "Enable", nullptr));
        DisableBtn->setText(QApplication::translate("RobotUI", "Disable", nullptr));
        ManualGroup->setTitle(QApplication::translate("RobotUI", "Manual", nullptr));
        label_12->setText(QApplication::translate("RobotUI", "Step", nullptr));
        Step->setSuffix(QApplication::translate("RobotUI", " mm/degree", nullptr));
        label_8->setText(QApplication::translate("RobotUI", "Speed", nullptr));
        Speed->setSuffix(QApplication::translate("RobotUI", " %", nullptr));
        label->setText(QApplication::translate("RobotUI", "X", nullptr));
        X_Add->setText(QApplication::translate("RobotUI", "+", nullptr));
        X_Minus->setText(QApplication::translate("RobotUI", "-", nullptr));
        X_MoveBtn->setText(QApplication::translate("RobotUI", "Move", nullptr));
        label_2->setText(QApplication::translate("RobotUI", "Y", nullptr));
        Y_Add->setText(QApplication::translate("RobotUI", "+", nullptr));
        Y_Minus->setText(QApplication::translate("RobotUI", "-", nullptr));
        Y_MoveBtn->setText(QApplication::translate("RobotUI", "Move", nullptr));
        label_3->setText(QApplication::translate("RobotUI", "Z", nullptr));
        Z_Add->setText(QApplication::translate("RobotUI", "+", nullptr));
        Z_Minus->setText(QApplication::translate("RobotUI", "-", nullptr));
        Z_MoveBtn->setText(QApplication::translate("RobotUI", "Move", nullptr));
        label_4->setText(QApplication::translate("RobotUI", "RX", nullptr));
        RX_Add->setText(QApplication::translate("RobotUI", "+", nullptr));
        RX_Minus->setText(QApplication::translate("RobotUI", "-", nullptr));
        RX_MoveBtn->setText(QApplication::translate("RobotUI", "Move", nullptr));
        label_5->setText(QApplication::translate("RobotUI", "RY", nullptr));
        RY_Add->setText(QApplication::translate("RobotUI", "+", nullptr));
        RY_Minus->setText(QApplication::translate("RobotUI", "-", nullptr));
        RY_MoveBtn->setText(QApplication::translate("RobotUI", "Move", nullptr));
        label_6->setText(QApplication::translate("RobotUI", "RZ", nullptr));
        RZ_Add->setText(QApplication::translate("RobotUI", "+", nullptr));
        RZ_Minus->setText(QApplication::translate("RobotUI", "-", nullptr));
        RZ_MoveBtn->setText(QApplication::translate("RobotUI", "Move", nullptr));
        groupBox->setTitle(QApplication::translate("RobotUI", "Teach", nullptr));
        TeachPointBtn->setText(QApplication::translate("RobotUI", "Point", nullptr));
        TeachWayBtn->setText(QApplication::translate("RobotUI", "Way", nullptr));
        TeachAdd->setText(QApplication::translate("RobotUI", "Add", nullptr));
        TeachDel->setText(QApplication::translate("RobotUI", "Delete", nullptr));
        TeachAddPoint->setText(QApplication::translate("RobotUI", "Add TeachPoint", nullptr));
        TeachDelPoint->setText(QApplication::translate("RobotUI", "Del TeachPoint", nullptr));
        MovewayPoint->setText(QApplication::translate("RobotUI", "Move TeachPoint", nullptr));
    } // retranslateUi

};

namespace Ui {
    class RobotUI: public Ui_RobotUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ROBOTUI_H
