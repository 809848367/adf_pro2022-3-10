/********************************************************************************
** Form generated from reading UI file 'CtrlDebugDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CTRLDEBUGDLG_H
#define UI_CTRLDEBUGDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CtrlDebugDlg
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *CenterWidget;

    void setupUi(QDialog *CtrlDebugDlg)
    {
        if (CtrlDebugDlg->objectName().isEmpty())
            CtrlDebugDlg->setObjectName(QString::fromUtf8("CtrlDebugDlg"));
        CtrlDebugDlg->resize(976, 606);
        verticalLayout = new QVBoxLayout(CtrlDebugDlg);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CenterWidget = new QWidget(CtrlDebugDlg);
        CenterWidget->setObjectName(QString::fromUtf8("CenterWidget"));

        verticalLayout->addWidget(CenterWidget);


        retranslateUi(CtrlDebugDlg);

        QMetaObject::connectSlotsByName(CtrlDebugDlg);
    } // setupUi

    void retranslateUi(QDialog *CtrlDebugDlg)
    {
        CtrlDebugDlg->setWindowTitle(QApplication::translate("CtrlDebugDlg", "CtrlDebugDlg", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CtrlDebugDlg: public Ui_CtrlDebugDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CTRLDEBUGDLG_H
