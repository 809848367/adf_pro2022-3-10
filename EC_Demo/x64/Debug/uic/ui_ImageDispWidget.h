/********************************************************************************
** Form generated from reading UI file 'ImageDispWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEDISPWIDGET_H
#define UI_IMAGEDISPWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ImageDispWidget
{
public:
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *sclWdgtContent;
    QHBoxLayout *horizontalLayout;

    void setupUi(QWidget *ImageDispWidget)
    {
        if (ImageDispWidget->objectName().isEmpty())
            ImageDispWidget->setObjectName(QString::fromUtf8("ImageDispWidget"));
        ImageDispWidget->resize(614, 469);
        gridLayout = new QGridLayout(ImageDispWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        scrollArea = new QScrollArea(ImageDispWidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(false);
        sclWdgtContent = new QWidget();
        sclWdgtContent->setObjectName(QString::fromUtf8("sclWdgtContent"));
        sclWdgtContent->setGeometry(QRect(0, -96, 580, 511));
        horizontalLayout = new QHBoxLayout(sclWdgtContent);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        scrollArea->setWidget(sclWdgtContent);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);


        retranslateUi(ImageDispWidget);

        QMetaObject::connectSlotsByName(ImageDispWidget);
    } // setupUi

    void retranslateUi(QWidget *ImageDispWidget)
    {
        ImageDispWidget->setWindowTitle(QApplication::translate("ImageDispWidget", "ImageDispWidget", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ImageDispWidget: public Ui_ImageDispWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEDISPWIDGET_H
