/********************************************************************************
** Form generated from reading UI file 'WelcomWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WELCOMWIDGET_H
#define UI_WELCOMWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "ImageDispWidget.h"

QT_BEGIN_NAMESPACE

class Ui_WelcomWidget
{
public:
    QVBoxLayout *verticalLayout_7;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QLabel *ResultLab;
    QSpacerItem *horizontalSpacer;
    QToolButton *StartBtn;
    QToolButton *PauseBtn;
    QToolButton *StopBtn;
    QToolButton *ResetBtn;
    QToolButton *LoginBtn;
    QLabel *MachineStatusLab;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_2;
    QLabel *ShowInfoLabel;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_3;
    QFrame *frame_5;
    QVBoxLayout *verticalLayout_8;
    QSplitter *splitter;
    ImageDispWidget *ImagDspWidget;
    QTableView *tableView;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBox_Auto;
    QCheckBox *checkBox_2;
    QCheckBox *check_refresh_io;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QComboBox *DsipNumsComBox;
    QVBoxLayout *verticalLayout_3;
    QPushButton *clearMsgBtn;
    QPushButton *ClearCountBtn;
    QSpacerItem *verticalSpacer;
    QWidget *widget_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLineEdit *Total_lineEdit;
    QLabel *label_4;
    QLineEdit *OK_lineEdit;
    QLabel *label_5;
    QLineEdit *NG_lineEdit;
    QLabel *label_6;
    QLineEdit *PassPrcent_lineEdit;
    QLabel *label_12;
    QLineEdit *CycleTime_lineEdit;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_6;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QWidget *WelcomWidget)
    {
        if (WelcomWidget->objectName().isEmpty())
            WelcomWidget->setObjectName(QString::fromUtf8("WelcomWidget"));
        WelcomWidget->resize(1222, 889);
        verticalLayout_7 = new QVBoxLayout(WelcomWidget);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        frame = new QFrame(WelcomWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setMaximumSize(QSize(16777215, 16777215));
        frame->setAutoFillBackground(false);
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::Box);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        ResultLab = new QLabel(frame);
        ResultLab->setObjectName(QString::fromUtf8("ResultLab"));
        ResultLab->setMinimumSize(QSize(200, 0));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font.setPointSize(49);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        font.setKerning(true);
        ResultLab->setFont(font);
        ResultLab->setStyleSheet(QString::fromUtf8("background-color:rgb(229, 255, 238)"));
        ResultLab->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(ResultLab);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        StartBtn = new QToolButton(frame);
        StartBtn->setObjectName(QString::fromUtf8("StartBtn"));
        sizePolicy.setHeightForWidth(StartBtn->sizePolicy().hasHeightForWidth());
        StartBtn->setSizePolicy(sizePolicy);
        StartBtn->setMinimumSize(QSize(0, 0));
        StartBtn->setMaximumSize(QSize(100, 16777215));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Img/Start_active_main.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        StartBtn->setIcon(icon);
        StartBtn->setIconSize(QSize(50, 50));
        StartBtn->setAutoRepeat(false);

        horizontalLayout->addWidget(StartBtn);

        PauseBtn = new QToolButton(frame);
        PauseBtn->setObjectName(QString::fromUtf8("PauseBtn"));
        sizePolicy.setHeightForWidth(PauseBtn->sizePolicy().hasHeightForWidth());
        PauseBtn->setSizePolicy(sizePolicy);
        PauseBtn->setMinimumSize(QSize(0, 0));
        PauseBtn->setMaximumSize(QSize(100, 16777215));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/Img/Pause_active_main.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        PauseBtn->setIcon(icon1);
        PauseBtn->setIconSize(QSize(50, 50));
        PauseBtn->setAutoRepeat(false);

        horizontalLayout->addWidget(PauseBtn);

        StopBtn = new QToolButton(frame);
        StopBtn->setObjectName(QString::fromUtf8("StopBtn"));
        sizePolicy.setHeightForWidth(StopBtn->sizePolicy().hasHeightForWidth());
        StopBtn->setSizePolicy(sizePolicy);
        StopBtn->setMinimumSize(QSize(0, 0));
        StopBtn->setMaximumSize(QSize(100, 16777215));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/Img/Stop_active_main.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        StopBtn->setIcon(icon2);
        StopBtn->setIconSize(QSize(50, 50));
        StopBtn->setAutoRepeat(false);

        horizontalLayout->addWidget(StopBtn);

        ResetBtn = new QToolButton(frame);
        ResetBtn->setObjectName(QString::fromUtf8("ResetBtn"));
        sizePolicy.setHeightForWidth(ResetBtn->sizePolicy().hasHeightForWidth());
        ResetBtn->setSizePolicy(sizePolicy);
        ResetBtn->setMinimumSize(QSize(0, 0));
        ResetBtn->setMaximumSize(QSize(100, 16777215));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/Img/Reset-Disable.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        ResetBtn->setIcon(icon3);
        ResetBtn->setIconSize(QSize(50, 50));
        ResetBtn->setAutoRepeat(false);

        horizontalLayout->addWidget(ResetBtn);

        LoginBtn = new QToolButton(frame);
        LoginBtn->setObjectName(QString::fromUtf8("LoginBtn"));
        sizePolicy.setHeightForWidth(LoginBtn->sizePolicy().hasHeightForWidth());
        LoginBtn->setSizePolicy(sizePolicy);
        LoginBtn->setMinimumSize(QSize(0, 0));
        LoginBtn->setMaximumSize(QSize(100, 16777215));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/Img/Login_Active.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        LoginBtn->setIcon(icon4);
        LoginBtn->setIconSize(QSize(50, 50));
        LoginBtn->setAutoRepeat(false);

        horizontalLayout->addWidget(LoginBtn);

        MachineStatusLab = new QLabel(frame);
        MachineStatusLab->setObjectName(QString::fromUtf8("MachineStatusLab"));
        sizePolicy.setHeightForWidth(MachineStatusLab->sizePolicy().hasHeightForWidth());
        MachineStatusLab->setSizePolicy(sizePolicy);
        MachineStatusLab->setMaximumSize(QSize(300, 16777215));
        QFont font1;
        font1.setPointSize(28);
        MachineStatusLab->setFont(font1);
        MachineStatusLab->setAutoFillBackground(false);
        MachineStatusLab->setStyleSheet(QString::fromUtf8("background-color:rgb(229, 255, 238)"));
        MachineStatusLab->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(MachineStatusLab);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(100, 0));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/Img/ZDPC11.png")));
        label_2->setScaledContents(true);

        horizontalLayout->addWidget(label_2);


        verticalLayout_7->addWidget(frame);

        ShowInfoLabel = new QLabel(WelcomWidget);
        ShowInfoLabel->setObjectName(QString::fromUtf8("ShowInfoLabel"));
        ShowInfoLabel->setMinimumSize(QSize(0, 50));
        QFont font2;
        font2.setPointSize(15);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        ShowInfoLabel->setFont(font2);
        ShowInfoLabel->setStyleSheet(QString::fromUtf8("background-color:rgb(229, 255, 238)"));
        ShowInfoLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(ShowInfoLabel);

        widget_2 = new QWidget(WelcomWidget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setAutoFillBackground(false);
        widget_2->setStyleSheet(QString::fromUtf8(""));
        horizontalLayout_3 = new QHBoxLayout(widget_2);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        frame_5 = new QFrame(widget_2);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(200);
        sizePolicy1.setVerticalStretch(200);
        sizePolicy1.setHeightForWidth(frame_5->sizePolicy().hasHeightForWidth());
        frame_5->setSizePolicy(sizePolicy1);
        frame_5->setFrameShape(QFrame::Box);
        verticalLayout_8 = new QVBoxLayout(frame_5);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        splitter = new QSplitter(frame_5);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        splitter->setOpaqueResize(true);
        splitter->setHandleWidth(11);
        ImagDspWidget = new ImageDispWidget(splitter);
        ImagDspWidget->setObjectName(QString::fromUtf8("ImagDspWidget"));
        sizePolicy.setHeightForWidth(ImagDspWidget->sizePolicy().hasHeightForWidth());
        ImagDspWidget->setSizePolicy(sizePolicy);
        splitter->addWidget(ImagDspWidget);
        tableView = new QTableView(splitter);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        sizePolicy.setHeightForWidth(tableView->sizePolicy().hasHeightForWidth());
        tableView->setSizePolicy(sizePolicy);
        splitter->addWidget(tableView);

        verticalLayout_8->addWidget(splitter);


        horizontalLayout_3->addWidget(frame_5);

        frame_4 = new QFrame(widget_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(62);
        sizePolicy2.setVerticalStretch(200);
        sizePolicy2.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy2);
        frame_4->setFrameShape(QFrame::Box);
        verticalLayout = new QVBoxLayout(frame_4);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_4 = new QGroupBox(frame_4);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(100);
        sizePolicy3.setVerticalStretch(100);
        sizePolicy3.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy3);
        verticalLayout_5 = new QVBoxLayout(groupBox_4);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        checkBox_Auto = new QCheckBox(groupBox_4);
        checkBox_Auto->setObjectName(QString::fromUtf8("checkBox_Auto"));
        sizePolicy.setHeightForWidth(checkBox_Auto->sizePolicy().hasHeightForWidth());
        checkBox_Auto->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(checkBox_Auto);

        checkBox_2 = new QCheckBox(groupBox_4);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setEnabled(false);
        sizePolicy.setHeightForWidth(checkBox_2->sizePolicy().hasHeightForWidth());
        checkBox_2->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(checkBox_2);

        check_refresh_io = new QCheckBox(groupBox_4);
        check_refresh_io->setObjectName(QString::fromUtf8("check_refresh_io"));
        check_refresh_io->setEnabled(false);
        sizePolicy.setHeightForWidth(check_refresh_io->sizePolicy().hasHeightForWidth());
        check_refresh_io->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(check_refresh_io);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label = new QLabel(groupBox_4);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_4->addWidget(label);

        DsipNumsComBox = new QComboBox(groupBox_4);
        DsipNumsComBox->setObjectName(QString::fromUtf8("DsipNumsComBox"));

        horizontalLayout_4->addWidget(DsipNumsComBox);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_2->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        clearMsgBtn = new QPushButton(groupBox_4);
        clearMsgBtn->setObjectName(QString::fromUtf8("clearMsgBtn"));

        verticalLayout_3->addWidget(clearMsgBtn);

        ClearCountBtn = new QPushButton(groupBox_4);
        ClearCountBtn->setObjectName(QString::fromUtf8("ClearCountBtn"));

        verticalLayout_3->addWidget(ClearCountBtn);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout_3);


        verticalLayout_4->addLayout(horizontalLayout_2);

        widget_3 = new QWidget(groupBox_4);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setMinimumSize(QSize(0, 50));

        verticalLayout_4->addWidget(widget_3);


        verticalLayout_5->addLayout(verticalLayout_4);


        verticalLayout->addWidget(groupBox_4);

        groupBox_2 = new QGroupBox(frame_4);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        sizePolicy3.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy3);
        QFont font3;
        font3.setPointSize(9);
        groupBox_2->setFont(font3);
        groupBox_2->setMouseTracking(true);
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Agency FB"));
        font4.setPointSize(13);
        label_3->setFont(font4);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_3, 0, 0, 1, 1);

        Total_lineEdit = new QLineEdit(groupBox_2);
        Total_lineEdit->setObjectName(QString::fromUtf8("Total_lineEdit"));
        Total_lineEdit->setEnabled(false);
        Total_lineEdit->setMouseTracking(false);

        gridLayout->addWidget(Total_lineEdit, 0, 1, 1, 1);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font4);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_4, 1, 0, 1, 1);

        OK_lineEdit = new QLineEdit(groupBox_2);
        OK_lineEdit->setObjectName(QString::fromUtf8("OK_lineEdit"));
        OK_lineEdit->setEnabled(false);
        OK_lineEdit->setMouseTracking(false);

        gridLayout->addWidget(OK_lineEdit, 1, 1, 1, 1);

        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font4);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_5, 2, 0, 1, 1);

        NG_lineEdit = new QLineEdit(groupBox_2);
        NG_lineEdit->setObjectName(QString::fromUtf8("NG_lineEdit"));
        NG_lineEdit->setEnabled(false);
        NG_lineEdit->setMouseTracking(false);

        gridLayout->addWidget(NG_lineEdit, 2, 1, 1, 1);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font4);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_6, 3, 0, 1, 1);

        PassPrcent_lineEdit = new QLineEdit(groupBox_2);
        PassPrcent_lineEdit->setObjectName(QString::fromUtf8("PassPrcent_lineEdit"));
        PassPrcent_lineEdit->setEnabled(false);
        PassPrcent_lineEdit->setMouseTracking(false);

        gridLayout->addWidget(PassPrcent_lineEdit, 3, 1, 1, 1);

        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setFont(font4);
        label_12->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_12, 4, 0, 1, 1);

        CycleTime_lineEdit = new QLineEdit(groupBox_2);
        CycleTime_lineEdit->setObjectName(QString::fromUtf8("CycleTime_lineEdit"));
        CycleTime_lineEdit->setEnabled(false);
        CycleTime_lineEdit->setMouseTracking(false);

        gridLayout->addWidget(CycleTime_lineEdit, 4, 1, 1, 1);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(frame_4);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(100);
        sizePolicy4.setVerticalStretch(80);
        sizePolicy4.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy4);
        groupBox_3->setFont(font3);
        groupBox_3->setMouseTracking(true);
        verticalLayout_6 = new QVBoxLayout(groupBox_3);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        plainTextEdit = new QPlainTextEdit(groupBox_3);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        verticalLayout_6->addWidget(plainTextEdit);


        verticalLayout->addWidget(groupBox_3);


        horizontalLayout_3->addWidget(frame_4);


        verticalLayout_7->addWidget(widget_2);

        verticalLayout_7->setStretch(2, 1);

        retranslateUi(WelcomWidget);

        QMetaObject::connectSlotsByName(WelcomWidget);
    } // setupUi

    void retranslateUi(QWidget *WelcomWidget)
    {
        WelcomWidget->setWindowTitle(QApplication::translate("WelcomWidget", "WelcomWidget", nullptr));
        ResultLab->setText(QApplication::translate("WelcomWidget", "OK", nullptr));
        StartBtn->setText(QString());
        PauseBtn->setText(QString());
        StopBtn->setText(QString());
        ResetBtn->setText(QString());
        LoginBtn->setText(QString());
        MachineStatusLab->setText(QApplication::translate("WelcomWidget", "\346\234\252\345\233\236\345\216\237", nullptr));
        label_2->setText(QString());
        ShowInfoLabel->setText(QString());
        groupBox_4->setTitle(QApplication::translate("WelcomWidget", "\345\212\237\350\203\275", nullptr));
        checkBox_Auto->setText(QApplication::translate("WelcomWidget", "\350\207\252\345\212\250\346\250\241\345\274\217", nullptr));
        checkBox_2->setText(QApplication::translate("WelcomWidget", "\345\261\217\350\224\275\351\227\250\347\246\201", nullptr));
        check_refresh_io->setText(QApplication::translate("WelcomWidget", "\345\220\257\347\224\250IO", nullptr));
        label->setText(QApplication::translate("WelcomWidget", "\346\257\217\350\241\214\345\233\276\347\211\207", nullptr));
        clearMsgBtn->setText(QApplication::translate("WelcomWidget", "\346\270\205\351\231\244\346\266\210\346\201\257", nullptr));
        ClearCountBtn->setText(QApplication::translate("WelcomWidget", "\346\270\205\351\231\244\350\256\241\346\225\260", nullptr));
        groupBox_2->setTitle(QApplication::translate("WelcomWidget", "Machine Info", nullptr));
        label_3->setText(QApplication::translate("WelcomWidget", "\346\200\273\346\225\260\357\274\232", nullptr));
        label_4->setText(QApplication::translate("WelcomWidget", "\350\211\257\345\223\201\357\274\232", nullptr));
        label_5->setText(QApplication::translate("WelcomWidget", "\344\270\215\350\211\257\345\223\201\357\274\232", nullptr));
        label_6->setText(QApplication::translate("WelcomWidget", "\350\211\257\347\216\207\357\274\232", nullptr));
        PassPrcent_lineEdit->setText(QString());
        label_12->setText(QApplication::translate("WelcomWidget", "\350\200\227\346\227\266\357\274\232", nullptr));
        CycleTime_lineEdit->setText(QString());
        groupBox_3->setTitle(QApplication::translate("WelcomWidget", "\345\274\202\345\270\270\344\277\241\346\201\257", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WelcomWidget: public Ui_WelcomWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WELCOMWIDGET_H
