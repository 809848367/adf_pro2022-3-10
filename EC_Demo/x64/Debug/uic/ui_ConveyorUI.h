/********************************************************************************
** Form generated from reading UI file 'ConveyorUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONVEYORUI_H
#define UI_CONVEYORUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConveyorUI
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *Start;
    QPushButton *Stop;
    QPushButton *Enable;
    QPushButton *Disable;
    QPushButton *ClearError;
    QLineEdit *Status;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *ConveyorUI)
    {
        if (ConveyorUI->objectName().isEmpty())
            ConveyorUI->setObjectName(QString::fromUtf8("ConveyorUI"));
        ConveyorUI->resize(972, 190);
        verticalLayout = new QVBoxLayout(ConveyorUI);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Start = new QPushButton(ConveyorUI);
        Start->setObjectName(QString::fromUtf8("Start"));
        Start->setMinimumSize(QSize(100, 30));
        Start->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(Start);

        Stop = new QPushButton(ConveyorUI);
        Stop->setObjectName(QString::fromUtf8("Stop"));
        Stop->setMinimumSize(QSize(100, 30));
        Stop->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(Stop);

        Enable = new QPushButton(ConveyorUI);
        Enable->setObjectName(QString::fromUtf8("Enable"));
        Enable->setMinimumSize(QSize(100, 30));
        Enable->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(Enable);

        Disable = new QPushButton(ConveyorUI);
        Disable->setObjectName(QString::fromUtf8("Disable"));
        Disable->setMinimumSize(QSize(100, 30));
        Disable->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(Disable);

        ClearError = new QPushButton(ConveyorUI);
        ClearError->setObjectName(QString::fromUtf8("ClearError"));
        ClearError->setMinimumSize(QSize(100, 30));
        ClearError->setMaximumSize(QSize(100, 30));

        horizontalLayout->addWidget(ClearError);

        Status = new QLineEdit(ConveyorUI);
        Status->setObjectName(QString::fromUtf8("Status"));
        Status->setMinimumSize(QSize(0, 30));
        Status->setMaximumSize(QSize(16777215, 30));

        horizontalLayout->addWidget(Status);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 126, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(ConveyorUI);

        QMetaObject::connectSlotsByName(ConveyorUI);
    } // setupUi

    void retranslateUi(QWidget *ConveyorUI)
    {
        ConveyorUI->setWindowTitle(QApplication::translate("ConveyorUI", "Form", nullptr));
        Start->setText(QApplication::translate("ConveyorUI", "Start", nullptr));
        Stop->setText(QApplication::translate("ConveyorUI", "Stop", nullptr));
        Enable->setText(QApplication::translate("ConveyorUI", "Enable", nullptr));
        Disable->setText(QApplication::translate("ConveyorUI", "Disable", nullptr));
        ClearError->setText(QApplication::translate("ConveyorUI", "ClearError", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ConveyorUI: public Ui_ConveyorUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONVEYORUI_H
