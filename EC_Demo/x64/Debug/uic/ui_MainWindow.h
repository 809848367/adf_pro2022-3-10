/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <./CProcessWidget.h>
#include <./SystemConfig.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <WelcomWidget.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QAction *actiondebugCtrl;
    QAction *actionHistroy;
    QAction *actionCommunication;
    QAction *actionDevelopmentLog;
    QAction *actionTempData;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    WelcomWidget *Welcom;
    CProcessWidget *Process;
    SystemConfig *SystemCfg;
    QMenuBar *menuBar;
    QMenu *menulogin;
    QMenu *menuControl;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QString::fromUtf8("MainWindowClass"));
        MainWindowClass->resize(960, 698);
        actiondebugCtrl = new QAction(MainWindowClass);
        actiondebugCtrl->setObjectName(QString::fromUtf8("actiondebugCtrl"));
        actionHistroy = new QAction(MainWindowClass);
        actionHistroy->setObjectName(QString::fromUtf8("actionHistroy"));
        actionCommunication = new QAction(MainWindowClass);
        actionCommunication->setObjectName(QString::fromUtf8("actionCommunication"));
        actionDevelopmentLog = new QAction(MainWindowClass);
        actionDevelopmentLog->setObjectName(QString::fromUtf8("actionDevelopmentLog"));
        actionTempData = new QAction(MainWindowClass);
        actionTempData->setObjectName(QString::fromUtf8("actionTempData"));
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        Welcom = new WelcomWidget();
        Welcom->setObjectName(QString::fromUtf8("Welcom"));
        tabWidget->addTab(Welcom, QString());
        Process = new CProcessWidget();
        Process->setObjectName(QString::fromUtf8("Process"));
        tabWidget->addTab(Process, QString());
        SystemCfg = new SystemConfig();
        SystemCfg->setObjectName(QString::fromUtf8("SystemCfg"));
        tabWidget->addTab(SystemCfg, QString());

        horizontalLayout->addWidget(tabWidget);

        MainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 960, 26));
        menulogin = new QMenu(menuBar);
        menulogin->setObjectName(QString::fromUtf8("menulogin"));
        menuControl = new QMenu(menuBar);
        menuControl->setObjectName(QString::fromUtf8("menuControl"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindowClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindowClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindowClass->setStatusBar(statusBar);

        menuBar->addAction(menulogin->menuAction());
        menuBar->addAction(menuControl->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuControl->addAction(actiondebugCtrl);
        menuControl->addAction(actionHistroy);
        menuControl->addAction(actionCommunication);
        menuControl->addAction(actionTempData);
        menuHelp->addAction(actionDevelopmentLog);

        retranslateUi(MainWindowClass);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "DebugFlow_ADF_Ver_1.1", nullptr));
        actiondebugCtrl->setText(QApplication::translate("MainWindowClass", "debugCtrl", nullptr));
        actionHistroy->setText(QApplication::translate("MainWindowClass", "Histroy", nullptr));
        actionCommunication->setText(QApplication::translate("MainWindowClass", "Communication", nullptr));
        actionDevelopmentLog->setText(QApplication::translate("MainWindowClass", "DevelopmentLog", nullptr));
        actionTempData->setText(QApplication::translate("MainWindowClass", "TempData", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Welcom), QApplication::translate("MainWindowClass", "Welcom", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Process), QApplication::translate("MainWindowClass", "Process", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(SystemCfg), QApplication::translate("MainWindowClass", "SystemCfg", nullptr));
        menulogin->setTitle(QApplication::translate("MainWindowClass", "User", nullptr));
        menuControl->setTitle(QApplication::translate("MainWindowClass", "Control", nullptr));
        menuHelp->setTitle(QApplication::translate("MainWindowClass", "Help", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
