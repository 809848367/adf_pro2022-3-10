/********************************************************************************
** Form generated from reading UI file 'QLoginDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QLOGINDLG_H
#define UI_QLOGINDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QLoginDlg
{
public:
    QGridLayout *gridLayout;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *countEdit;
    QSpacerItem *horizontalSpacer_7;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_5;
    QLineEdit *pwdEdit;
    QSpacerItem *horizontalSpacer_6;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *exit_btn;
    QSpacerItem *horizontalSpacer;
    QPushButton *login_btn;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QDialog *QLoginDlg)
    {
        if (QLoginDlg->objectName().isEmpty())
            QLoginDlg->setObjectName(QString::fromUtf8("QLoginDlg"));
        QLoginDlg->resize(522, 331);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(QLoginDlg->sizePolicy().hasHeightForWidth());
        QLoginDlg->setSizePolicy(sizePolicy);
        QLoginDlg->setMinimumSize(QSize(522, 331));
        QLoginDlg->setMaximumSize(QSize(522, 331));
        gridLayout = new QGridLayout(QLoginDlg);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        widget = new QWidget(QLoginDlg);
        widget->setObjectName(QString::fromUtf8("widget"));
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(500, 309));
        widget->setMaximumSize(QSize(500, 309));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout = new QHBoxLayout(widget_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_8 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_8);

        label = new QLabel(widget_2);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        QFont font;
        font.setFamily(QString::fromUtf8("Adobe \345\256\213\344\275\223 Std L"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(label);

        horizontalSpacer_4 = new QSpacerItem(15, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        countEdit = new QLineEdit(widget_2);
        countEdit->setObjectName(QString::fromUtf8("countEdit"));
        countEdit->setMinimumSize(QSize(0, 40));

        horizontalLayout->addWidget(countEdit);

        horizontalSpacer_7 = new QSpacerItem(30, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);


        verticalLayout->addWidget(widget_2);

        widget_3 = new QWidget(widget);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        horizontalLayout_2 = new QHBoxLayout(widget_3);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_9 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_9);

        label_2 = new QLabel(widget_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer_5 = new QSpacerItem(15, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        pwdEdit = new QLineEdit(widget_3);
        pwdEdit->setObjectName(QString::fromUtf8("pwdEdit"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pwdEdit->sizePolicy().hasHeightForWidth());
        pwdEdit->setSizePolicy(sizePolicy2);
        pwdEdit->setMinimumSize(QSize(40, 40));
        pwdEdit->setEchoMode(QLineEdit::Password);

        horizontalLayout_2->addWidget(pwdEdit);

        horizontalSpacer_6 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        verticalLayout->addWidget(widget_3);

        widget_4 = new QWidget(widget);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        horizontalLayout_3 = new QHBoxLayout(widget_4);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        exit_btn = new QPushButton(widget_4);
        exit_btn->setObjectName(QString::fromUtf8("exit_btn"));

        horizontalLayout_3->addWidget(exit_btn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        login_btn = new QPushButton(widget_4);
        login_btn->setObjectName(QString::fromUtf8("login_btn"));

        horizontalLayout_3->addWidget(login_btn);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout->addWidget(widget_4);


        gridLayout->addWidget(widget, 0, 0, 1, 1);


        retranslateUi(QLoginDlg);
        QObject::connect(exit_btn, SIGNAL(clicked()), QLoginDlg, SLOT(on_exitSolt()));
        QObject::connect(login_btn, SIGNAL(clicked()), QLoginDlg, SLOT(on_loginSolt()));

        QMetaObject::connectSlotsByName(QLoginDlg);
    } // setupUi

    void retranslateUi(QDialog *QLoginDlg)
    {
        QLoginDlg->setWindowTitle(QApplication::translate("QLoginDlg", "QLoginDlg", nullptr));
        label->setText(QApplication::translate("QLoginDlg", "\350\264\246\345\217\267\357\274\232", nullptr));
        label_2->setText(QApplication::translate("QLoginDlg", "\345\257\206\347\240\201\357\274\232", nullptr));
        pwdEdit->setInputMask(QString());
        pwdEdit->setPlaceholderText(QString());
        exit_btn->setText(QApplication::translate("QLoginDlg", "\351\200\200\345\207\272", nullptr));
        login_btn->setText(QApplication::translate("QLoginDlg", "\347\231\273\345\275\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QLoginDlg: public Ui_QLoginDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QLOGINDLG_H
