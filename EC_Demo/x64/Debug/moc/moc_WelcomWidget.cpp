/****************************************************************************
** Meta object code from reading C++ file 'WelcomWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../WelcomWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'WelcomWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_WelcomWidget_t {
    QByteArrayData data[43];
    char stringdata0[539];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WelcomWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WelcomWidget_t qt_meta_stringdata_WelcomWidget = {
    {
QT_MOC_LITERAL(0, 0, 12), // "WelcomWidget"
QT_MOC_LITERAL(1, 13, 9), // "SignalMsg"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 6), // "msgint"
QT_MOC_LITERAL(4, 31, 4), // "type"
QT_MOC_LITERAL(5, 36, 11), // "SignalLogin"
QT_MOC_LITERAL(6, 48, 3), // "str"
QT_MOC_LITERAL(7, 52, 13), // "SignalMachine"
QT_MOC_LITERAL(8, 66, 15), // "RegisterProcMsg"
QT_MOC_LITERAL(9, 82, 7), // "DispMsg"
QT_MOC_LITERAL(10, 90, 3), // "msg"
QT_MOC_LITERAL(11, 94, 11), // "ShowProInfo"
QT_MOC_LITERAL(12, 106, 17), // "DispMsgWithModule"
QT_MOC_LITERAL(13, 124, 10), // "prefixList"
QT_MOC_LITERAL(14, 135, 12), // "StateRefresh"
QT_MOC_LITERAL(15, 148, 8), // "procName"
QT_MOC_LITERAL(16, 157, 28), // "CParentProcess::ProcessState"
QT_MOC_LITERAL(17, 186, 5), // "state"
QT_MOC_LITERAL(18, 192, 11), // "RecvImgInfo"
QT_MOC_LITERAL(19, 204, 5), // "Datas"
QT_MOC_LITERAL(20, 210, 18), // "RefreshMachineInfo"
QT_MOC_LITERAL(21, 229, 6), // "result"
QT_MOC_LITERAL(22, 236, 16), // "RefreshDsipImage"
QT_MOC_LITERAL(23, 253, 16), // "QList<DspLable*>"
QT_MOC_LITERAL(24, 270, 11), // "dispLabList"
QT_MOC_LITERAL(25, 282, 13), // "MachineStatus"
QT_MOC_LITERAL(26, 296, 6), // "status"
QT_MOC_LITERAL(27, 303, 15), // "on_StartBtnSlot"
QT_MOC_LITERAL(28, 319, 15), // "on_PauseBtnSlot"
QT_MOC_LITERAL(29, 335, 14), // "on_StopBtnSlot"
QT_MOC_LITERAL(30, 350, 15), // "on_ResetBtnSlot"
QT_MOC_LITERAL(31, 366, 15), // "on_LoginBtnSlot"
QT_MOC_LITERAL(32, 382, 16), // "Set_ProcListSlot"
QT_MOC_LITERAL(33, 399, 22), // "QList<CParentProcess*>"
QT_MOC_LITERAL(34, 422, 8), // "procList"
QT_MOC_LITERAL(35, 431, 20), // "SetMachineStatusSlot"
QT_MOC_LITERAL(36, 452, 10), // "bException"
QT_MOC_LITERAL(37, 463, 18), // "Slot_AutoCheckSlot"
QT_MOC_LITERAL(38, 482, 11), // "ClearMsgBtn"
QT_MOC_LITERAL(39, 494, 8), // "ResetBtn"
QT_MOC_LITERAL(40, 503, 10), // "ClearCount"
QT_MOC_LITERAL(41, 514, 17), // "on_SelectDispNums"
QT_MOC_LITERAL(42, 532, 6) // "selStr"

    },
    "WelcomWidget\0SignalMsg\0\0msgint\0type\0"
    "SignalLogin\0str\0SignalMachine\0"
    "RegisterProcMsg\0DispMsg\0msg\0ShowProInfo\0"
    "DispMsgWithModule\0prefixList\0StateRefresh\0"
    "procName\0CParentProcess::ProcessState\0"
    "state\0RecvImgInfo\0Datas\0RefreshMachineInfo\0"
    "result\0RefreshDsipImage\0QList<DspLable*>\0"
    "dispLabList\0MachineStatus\0status\0"
    "on_StartBtnSlot\0on_PauseBtnSlot\0"
    "on_StopBtnSlot\0on_ResetBtnSlot\0"
    "on_LoginBtnSlot\0Set_ProcListSlot\0"
    "QList<CParentProcess*>\0procList\0"
    "SetMachineStatusSlot\0bException\0"
    "Slot_AutoCheckSlot\0ClearMsgBtn\0ResetBtn\0"
    "ClearCount\0on_SelectDispNums\0selStr"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WelcomWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  154,    2, 0x06 /* Public */,
       1,    1,  159,    2, 0x26 /* Public | MethodCloned */,
       5,    1,  162,    2, 0x06 /* Public */,
       7,    1,  165,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,  168,    2, 0x0a /* Public */,
       9,    2,  169,    2, 0x0a /* Public */,
       9,    1,  174,    2, 0x2a /* Public | MethodCloned */,
      11,    2,  177,    2, 0x0a /* Public */,
      11,    1,  182,    2, 0x2a /* Public | MethodCloned */,
      12,    3,  185,    2, 0x0a /* Public */,
      12,    2,  192,    2, 0x2a /* Public | MethodCloned */,
      14,    2,  197,    2, 0x0a /* Public */,
      18,    1,  202,    2, 0x0a /* Public */,
      20,    1,  205,    2, 0x0a /* Public */,
      22,    1,  208,    2, 0x0a /* Public */,
      25,    1,  211,    2, 0x0a /* Public */,
      27,    0,  214,    2, 0x0a /* Public */,
      28,    0,  215,    2, 0x0a /* Public */,
      29,    0,  216,    2, 0x0a /* Public */,
      30,    0,  217,    2, 0x0a /* Public */,
      31,    0,  218,    2, 0x0a /* Public */,
      32,    1,  219,    2, 0x0a /* Public */,
      35,    1,  222,    2, 0x0a /* Public */,
      37,    1,  225,    2, 0x0a /* Public */,
      38,    0,  228,    2, 0x0a /* Public */,
      39,    0,  229,    2, 0x0a /* Public */,
      40,    0,  230,    2, 0x0a /* Public */,
      41,    1,  231,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString,    6,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   10,    4,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   10,    4,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString, QMetaType::Int,   13,   10,    4,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString,   13,   10,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 16,   15,   17,
    QMetaType::Void, QMetaType::QVariantMap,   19,
    QMetaType::Void, QMetaType::QString,   21,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 33,   34,
    QMetaType::Void, QMetaType::Bool,   36,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   42,

       0        // eod
};

void WelcomWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<WelcomWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SignalMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->SignalMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->SignalLogin((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->SignalMachine((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->RegisterProcMsg(); break;
        case 5: _t->DispMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->DispMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->ShowProInfo((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->ShowProInfo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->DispMsgWithModule((*reinterpret_cast< QStringList(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 10: _t->DispMsgWithModule((*reinterpret_cast< QStringList(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->StateRefresh((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< CParentProcess::ProcessState(*)>(_a[2]))); break;
        case 12: _t->RecvImgInfo((*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[1]))); break;
        case 13: _t->RefreshMachineInfo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: _t->RefreshDsipImage((*reinterpret_cast< QList<DspLable*>(*)>(_a[1]))); break;
        case 15: _t->MachineStatus((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: _t->on_StartBtnSlot(); break;
        case 17: _t->on_PauseBtnSlot(); break;
        case 18: _t->on_StopBtnSlot(); break;
        case 19: _t->on_ResetBtnSlot(); break;
        case 20: _t->on_LoginBtnSlot(); break;
        case 21: _t->Set_ProcListSlot((*reinterpret_cast< QList<CParentProcess*>(*)>(_a[1]))); break;
        case 22: _t->SetMachineStatusSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->Slot_AutoCheckSlot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->ClearMsgBtn(); break;
        case 25: _t->ResetBtn(); break;
        case 26: _t->ClearCount(); break;
        case 27: _t->on_SelectDispNums((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<DspLable*> >(); break;
            }
            break;
        case 21:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<CParentProcess*> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (WelcomWidget::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&WelcomWidget::SignalMsg)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (WelcomWidget::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&WelcomWidget::SignalLogin)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (WelcomWidget::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&WelcomWidget::SignalMachine)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject WelcomWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_WelcomWidget.data,
    qt_meta_data_WelcomWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WelcomWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WelcomWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WelcomWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int WelcomWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    }
    return _id;
}

// SIGNAL 0
void WelcomWidget::SignalMsg(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 2
void WelcomWidget::SignalLogin(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void WelcomWidget::SignalMachine(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
