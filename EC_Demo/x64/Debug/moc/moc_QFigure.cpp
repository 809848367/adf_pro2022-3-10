/****************************************************************************
** Meta object code from reading C++ file 'QFigure.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../QFigure.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QFigure.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QFigure_t {
    QByteArrayData data[24];
    char stringdata0[182];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QFigure_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QFigure_t qt_meta_stringdata_QFigure = {
    {
QT_MOC_LITERAL(0, 0, 7), // "QFigure"
QT_MOC_LITERAL(1, 8, 8), // "SelectCB"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 9), // "cv::Point"
QT_MOC_LITERAL(4, 28, 3), // "Pt1"
QT_MOC_LITERAL(5, 32, 3), // "Pt2"
QT_MOC_LITERAL(6, 36, 11), // "LBtnClickCB"
QT_MOC_LITERAL(7, 48, 1), // "x"
QT_MOC_LITERAL(8, 50, 1), // "y"
QT_MOC_LITERAL(9, 52, 12), // "LBtnDClickCB"
QT_MOC_LITERAL(10, 65, 11), // "RBtnClickCB"
QT_MOC_LITERAL(11, 77, 12), // "RBtnDClickCB"
QT_MOC_LITERAL(12, 90, 10), // "MouseWheel"
QT_MOC_LITERAL(13, 101, 5), // "value"
QT_MOC_LITERAL(14, 107, 6), // "MoveCB"
QT_MOC_LITERAL(15, 114, 3), // "Old"
QT_MOC_LITERAL(16, 118, 3), // "New"
QT_MOC_LITERAL(17, 122, 8), // "MousePos"
QT_MOC_LITERAL(18, 131, 3), // "Pos"
QT_MOC_LITERAL(19, 135, 2), // "GV"
QT_MOC_LITERAL(20, 138, 14), // "ZoomInBySelect"
QT_MOC_LITERAL(21, 153, 9), // "ZoomInOut"
QT_MOC_LITERAL(22, 163, 9), // "ZoomReset"
QT_MOC_LITERAL(23, 173, 8) // "ZoomMove"

    },
    "QFigure\0SelectCB\0\0cv::Point\0Pt1\0Pt2\0"
    "LBtnClickCB\0x\0y\0LBtnDClickCB\0RBtnClickCB\0"
    "RBtnDClickCB\0MouseWheel\0value\0MoveCB\0"
    "Old\0New\0MousePos\0Pos\0GV\0ZoomInBySelect\0"
    "ZoomInOut\0ZoomReset\0ZoomMove"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QFigure[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   74,    2, 0x06 /* Public */,
       6,    2,   79,    2, 0x06 /* Public */,
       9,    2,   84,    2, 0x06 /* Public */,
      10,    2,   89,    2, 0x06 /* Public */,
      11,    2,   94,    2, 0x06 /* Public */,
      12,    3,   99,    2, 0x06 /* Public */,
      14,    2,  106,    2, 0x06 /* Public */,
      17,    2,  111,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      20,    2,  116,    2, 0x0a /* Public */,
      21,    3,  121,    2, 0x0a /* Public */,
      22,    0,  128,    2, 0x0a /* Public */,
      23,    2,  129,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    7,    8,   13,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,   15,   16,
    QMetaType::Void, 0x80000000 | 3, QMetaType::UChar,   18,   19,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    7,    8,   13,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,   15,   16,

       0        // eod
};

void QFigure::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QFigure *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SelectCB((*reinterpret_cast< cv::Point(*)>(_a[1])),(*reinterpret_cast< cv::Point(*)>(_a[2]))); break;
        case 1: _t->LBtnClickCB((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->LBtnDClickCB((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->RBtnClickCB((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->RBtnDClickCB((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->MouseWheel((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->MoveCB((*reinterpret_cast< cv::Point(*)>(_a[1])),(*reinterpret_cast< cv::Point(*)>(_a[2]))); break;
        case 7: _t->MousePos((*reinterpret_cast< cv::Point(*)>(_a[1])),(*reinterpret_cast< unsigned char(*)>(_a[2]))); break;
        case 8: _t->ZoomInBySelect((*reinterpret_cast< cv::Point(*)>(_a[1])),(*reinterpret_cast< cv::Point(*)>(_a[2]))); break;
        case 9: _t->ZoomInOut((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 10: _t->ZoomReset(); break;
        case 11: _t->ZoomMove((*reinterpret_cast< cv::Point(*)>(_a[1])),(*reinterpret_cast< cv::Point(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QFigure::*)(cv::Point , cv::Point );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::SelectCB)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::LBtnClickCB)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::LBtnDClickCB)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::RBtnClickCB)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::RBtnDClickCB)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(int , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::MouseWheel)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(cv::Point , cv::Point );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::MoveCB)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QFigure::*)(cv::Point , unsigned char );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QFigure::MousePos)) {
                *result = 7;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QFigure::staticMetaObject = { {
    &QLabel::staticMetaObject,
    qt_meta_stringdata_QFigure.data,
    qt_meta_data_QFigure,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QFigure::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QFigure::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QFigure.stringdata0))
        return static_cast<void*>(this);
    return QLabel::qt_metacast(_clname);
}

int QFigure::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLabel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void QFigure::SelectCB(cv::Point _t1, cv::Point _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QFigure::LBtnClickCB(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QFigure::LBtnDClickCB(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QFigure::RBtnClickCB(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QFigure::RBtnDClickCB(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QFigure::MouseWheel(int _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QFigure::MoveCB(cv::Point _t1, cv::Point _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QFigure::MousePos(cv::Point _t1, unsigned char _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
