/****************************************************************************
** Meta object code from reading C++ file 'Conveyor.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Conveyor.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Conveyor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Conveyor_t {
    QByteArrayData data[14];
    char stringdata0[171];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Conveyor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Conveyor_t qt_meta_stringdata_Conveyor = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Conveyor"
QT_MOC_LITERAL(1, 9, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 19, 7), // "NoError"
QT_MOC_LITERAL(3, 27, 4), // "Base"
QT_MOC_LITERAL(4, 32, 12), // "ClearErrFail"
QT_MOC_LITERAL(5, 45, 11), // "StartCmdErr"
QT_MOC_LITERAL(6, 57, 10), // "StopCmdErr"
QT_MOC_LITERAL(7, 68, 15), // "GetStatusCmdErr"
QT_MOC_LITERAL(8, 84, 15), // "SetEnableCmdErr"
QT_MOC_LITERAL(9, 100, 12), // "SetVelCmdErr"
QT_MOC_LITERAL(10, 113, 14), // "ClearErrCmdErr"
QT_MOC_LITERAL(11, 128, 11), // "InitDataErr"
QT_MOC_LITERAL(12, 140, 10), // "SPMisEmpty"
QT_MOC_LITERAL(13, 151, 19) // "ExecuteDataNotRight"

    },
    "Conveyor\0ErrorCode\0NoError\0Base\0"
    "ClearErrFail\0StartCmdErr\0StopCmdErr\0"
    "GetStatusCmdErr\0SetEnableCmdErr\0"
    "SetVelCmdErr\0ClearErrCmdErr\0InitDataErr\0"
    "SPMisEmpty\0ExecuteDataNotRight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Conveyor[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   12,   19,

 // enum data: key, value
       2, uint(Conveyor::NoError),
       3, uint(Conveyor::Base),
       4, uint(Conveyor::ClearErrFail),
       5, uint(Conveyor::StartCmdErr),
       6, uint(Conveyor::StopCmdErr),
       7, uint(Conveyor::GetStatusCmdErr),
       8, uint(Conveyor::SetEnableCmdErr),
       9, uint(Conveyor::SetVelCmdErr),
      10, uint(Conveyor::ClearErrCmdErr),
      11, uint(Conveyor::InitDataErr),
      12, uint(Conveyor::SPMisEmpty),
      13, uint(Conveyor::ExecuteDataNotRight),

       0        // eod
};

void Conveyor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Conveyor::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_Conveyor.data,
    qt_meta_data_Conveyor,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Conveyor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Conveyor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Conveyor.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int Conveyor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
