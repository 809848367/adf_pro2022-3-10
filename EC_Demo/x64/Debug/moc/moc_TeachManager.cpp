/****************************************************************************
** Meta object code from reading C++ file 'TeachManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../TeachManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TeachManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TeachManager_t {
    QByteArrayData data[23];
    char stringdata0[291];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TeachManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TeachManager_t qt_meta_stringdata_TeachManager = {
    {
QT_MOC_LITERAL(0, 0, 12), // "TeachManager"
QT_MOC_LITERAL(1, 13, 11), // "UpDateTable"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 10), // "TeachTable"
QT_MOC_LITERAL(4, 37, 5), // "Table"
QT_MOC_LITERAL(5, 43, 16), // "RefreshTeachData"
QT_MOC_LITERAL(6, 60, 7), // "Station"
QT_MOC_LITERAL(7, 68, 15), // "TeachData_Robot"
QT_MOC_LITERAL(8, 84, 10), // "tTeachData"
QT_MOC_LITERAL(9, 95, 18), // "TeachData_Lighting"
QT_MOC_LITERAL(10, 114, 19), // "TeachData_PanaMotor"
QT_MOC_LITERAL(11, 134, 16), // "TeachData_Camera"
QT_MOC_LITERAL(12, 151, 15), // "UpDateTableSlot"
QT_MOC_LITERAL(13, 167, 15), // "UpDateTeachData"
QT_MOC_LITERAL(14, 183, 9), // "ErrorCode"
QT_MOC_LITERAL(15, 193, 7), // "NoError"
QT_MOC_LITERAL(16, 201, 4), // "Base"
QT_MOC_LITERAL(17, 206, 21), // "TeachTableFormatWrong"
QT_MOC_LITERAL(18, 228, 11), // "NameIsWrong"
QT_MOC_LITERAL(19, 240, 14), // "StationIsWrong"
QT_MOC_LITERAL(20, 255, 10), // "OpenDBFail"
QT_MOC_LITERAL(21, 266, 11), // "ReadDBError"
QT_MOC_LITERAL(22, 278, 12) // "WriteDBError"

    },
    "TeachManager\0UpDateTable\0\0TeachTable\0"
    "Table\0RefreshTeachData\0Station\0"
    "TeachData_Robot\0tTeachData\0"
    "TeachData_Lighting\0TeachData_PanaMotor\0"
    "TeachData_Camera\0UpDateTableSlot\0"
    "UpDateTeachData\0ErrorCode\0NoError\0"
    "Base\0TeachTableFormatWrong\0NameIsWrong\0"
    "StationIsWrong\0OpenDBFail\0ReadDBError\0"
    "WriteDBError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TeachManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       1,  110, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       5,    2,   67,    2, 0x06 /* Public */,
       5,    2,   72,    2, 0x06 /* Public */,
       5,    2,   77,    2, 0x06 /* Public */,
       5,    2,   82,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,   87,    2, 0x0a /* Public */,
      13,    2,   90,    2, 0x0a /* Public */,
      13,    2,   95,    2, 0x0a /* Public */,
      13,    2,  100,    2, 0x0a /* Public */,
      13,    2,  105,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 7,    6,    8,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 9,    6,    8,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 10,    6,    8,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 11,    6,    8,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 7,    6,    8,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 9,    6,    8,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 10,    6,    8,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 11,    6,    8,

 // enums: name, alias, flags, count, data
      14,   14, 0x0,    8,  115,

 // enum data: key, value
      15, uint(TeachManager::NoError),
      16, uint(TeachManager::Base),
      17, uint(TeachManager::TeachTableFormatWrong),
      18, uint(TeachManager::NameIsWrong),
      19, uint(TeachManager::StationIsWrong),
      20, uint(TeachManager::OpenDBFail),
      21, uint(TeachManager::ReadDBError),
      22, uint(TeachManager::WriteDBError),

       0        // eod
};

void TeachManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TeachManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->UpDateTable((*reinterpret_cast< TeachTable(*)>(_a[1]))); break;
        case 1: _t->RefreshTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Robot(*)>(_a[2]))); break;
        case 2: _t->RefreshTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Lighting(*)>(_a[2]))); break;
        case 3: _t->RefreshTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_PanaMotor(*)>(_a[2]))); break;
        case 4: _t->RefreshTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Camera(*)>(_a[2]))); break;
        case 5: _t->UpDateTableSlot((*reinterpret_cast< TeachTable(*)>(_a[1]))); break;
        case 6: _t->UpDateTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Robot(*)>(_a[2]))); break;
        case 7: _t->UpDateTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Lighting(*)>(_a[2]))); break;
        case 8: _t->UpDateTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_PanaMotor(*)>(_a[2]))); break;
        case 9: _t->UpDateTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Camera(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TeachManager::*)(TeachTable );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachManager::UpDateTable)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TeachManager::*)(QString , TeachData_Robot );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachManager::RefreshTeachData)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TeachManager::*)(QString , TeachData_Lighting );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachManager::RefreshTeachData)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (TeachManager::*)(QString , TeachData_PanaMotor );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachManager::RefreshTeachData)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (TeachManager::*)(QString , TeachData_Camera );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TeachManager::RefreshTeachData)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TeachManager::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_TeachManager.data,
    qt_meta_data_TeachManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TeachManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TeachManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TeachManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int TeachManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void TeachManager::UpDateTable(TeachTable _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TeachManager::RefreshTeachData(QString _t1, TeachData_Robot _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TeachManager::RefreshTeachData(QString _t1, TeachData_Lighting _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void TeachManager::RefreshTeachData(QString _t1, TeachData_PanaMotor _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TeachManager::RefreshTeachData(QString _t1, TeachData_Camera _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
