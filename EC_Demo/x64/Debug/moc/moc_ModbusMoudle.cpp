/****************************************************************************
** Meta object code from reading C++ file 'ModbusMoudle.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../ModbusMoudle.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ModbusMoudle.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ModbusMoudle_t {
    QByteArrayData data[8];
    char stringdata0[86];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ModbusMoudle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ModbusMoudle_t qt_meta_stringdata_ModbusMoudle = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ModbusMoudle"
QT_MOC_LITERAL(1, 13, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 23, 7), // "NoError"
QT_MOC_LITERAL(3, 31, 4), // "Base"
QT_MOC_LITERAL(4, 36, 15), // "ConnectionError"
QT_MOC_LITERAL(5, 52, 10), // "WriteError"
QT_MOC_LITERAL(6, 63, 9), // "ReadError"
QT_MOC_LITERAL(7, 73, 12) // "DataNotPatch"

    },
    "ModbusMoudle\0ErrorCode\0NoError\0Base\0"
    "ConnectionError\0WriteError\0ReadError\0"
    "DataNotPatch"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ModbusMoudle[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    6,   19,

 // enum data: key, value
       2, uint(ModbusMoudle::NoError),
       3, uint(ModbusMoudle::Base),
       4, uint(ModbusMoudle::ConnectionError),
       5, uint(ModbusMoudle::WriteError),
       6, uint(ModbusMoudle::ReadError),
       7, uint(ModbusMoudle::DataNotPatch),

       0        // eod
};

void ModbusMoudle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject ModbusMoudle::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_ModbusMoudle.data,
    qt_meta_data_ModbusMoudle,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ModbusMoudle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ModbusMoudle::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ModbusMoudle.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ModbusMoudle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
