/****************************************************************************
** Meta object code from reading C++ file 'IOControl.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../IOControl.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'IOControl.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_IOControl_t {
    QByteArrayData data[10];
    char stringdata0[139];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_IOControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_IOControl_t qt_meta_stringdata_IOControl = {
    {
QT_MOC_LITERAL(0, 0, 9), // "IOControl"
QT_MOC_LITERAL(1, 10, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 20, 7), // "NoError"
QT_MOC_LITERAL(3, 28, 4), // "Base"
QT_MOC_LITERAL(4, 33, 15), // "IndexOutOfRange"
QT_MOC_LITERAL(5, 49, 17), // "ModuleTypeIsWrong"
QT_MOC_LITERAL(6, 67, 18), // "ModuleTypeNotAllow"
QT_MOC_LITERAL(7, 86, 20), // "IONameNotEqualIOData"
QT_MOC_LITERAL(8, 107, 11), // "InitDataErr"
QT_MOC_LITERAL(9, 119, 19) // "ExecuteDataNotRight"

    },
    "IOControl\0ErrorCode\0NoError\0Base\0"
    "IndexOutOfRange\0ModuleTypeIsWrong\0"
    "ModuleTypeNotAllow\0IONameNotEqualIOData\0"
    "InitDataErr\0ExecuteDataNotRight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_IOControl[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    8,   19,

 // enum data: key, value
       2, uint(IOControl::NoError),
       3, uint(IOControl::Base),
       4, uint(IOControl::IndexOutOfRange),
       5, uint(IOControl::ModuleTypeIsWrong),
       6, uint(IOControl::ModuleTypeNotAllow),
       7, uint(IOControl::IONameNotEqualIOData),
       8, uint(IOControl::InitDataErr),
       9, uint(IOControl::ExecuteDataNotRight),

       0        // eod
};

void IOControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject IOControl::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_IOControl.data,
    qt_meta_data_IOControl,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *IOControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *IOControl::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_IOControl.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int IOControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
