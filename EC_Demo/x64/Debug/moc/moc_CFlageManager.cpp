/****************************************************************************
** Meta object code from reading C++ file 'CFlageManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../CFlageManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CFlageManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CFlageManager_t {
    QByteArrayData data[9];
    char stringdata0[153];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CFlageManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CFlageManager_t qt_meta_stringdata_CFlageManager = {
    {
QT_MOC_LITERAL(0, 0, 13), // "CFlageManager"
QT_MOC_LITERAL(1, 14, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 24, 7), // "NoError"
QT_MOC_LITERAL(3, 32, 4), // "Base"
QT_MOC_LITERAL(4, 37, 21), // "ModuleFlage_Not_Found"
QT_MOC_LITERAL(5, 59, 26), // "ModuleFlage_Change_Default"
QT_MOC_LITERAL(6, 86, 22), // "ModuleFlage_Name_Error"
QT_MOC_LITERAL(7, 109, 19), // "ModuleFlage_Commond"
QT_MOC_LITERAL(8, 129, 23) // "ModuleFlage_KeyNotFound"

    },
    "CFlageManager\0ErrorCode\0NoError\0Base\0"
    "ModuleFlage_Not_Found\0ModuleFlage_Change_Default\0"
    "ModuleFlage_Name_Error\0ModuleFlage_Commond\0"
    "ModuleFlage_KeyNotFound"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CFlageManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    7,   19,

 // enum data: key, value
       2, uint(CFlageManager::NoError),
       3, uint(CFlageManager::Base),
       4, uint(CFlageManager::ModuleFlage_Not_Found),
       5, uint(CFlageManager::ModuleFlage_Change_Default),
       6, uint(CFlageManager::ModuleFlage_Name_Error),
       7, uint(CFlageManager::ModuleFlage_Commond),
       8, uint(CFlageManager::ModuleFlage_KeyNotFound),

       0        // eod
};

void CFlageManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject CFlageManager::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_CFlageManager.data,
    qt_meta_data_CFlageManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CFlageManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CFlageManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CFlageManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int CFlageManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
