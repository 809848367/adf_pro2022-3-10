/****************************************************************************
** Meta object code from reading C++ file 'Camera.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Camera.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Camera.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Camera_t {
    QByteArrayData data[15];
    char stringdata0[190];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Camera_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Camera_t qt_meta_stringdata_Camera = {
    {
QT_MOC_LITERAL(0, 0, 6), // "Camera"
QT_MOC_LITERAL(1, 7, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 17, 7), // "NoError"
QT_MOC_LITERAL(3, 25, 4), // "Base"
QT_MOC_LITERAL(4, 30, 14), // "EnumDevicesErr"
QT_MOC_LITERAL(5, 45, 16), // "CameraSNNotExist"
QT_MOC_LITERAL(6, 62, 15), // "CreateHandleErr"
QT_MOC_LITERAL(7, 78, 13), // "CameraOpenErr"
QT_MOC_LITERAL(8, 92, 16), // "StartGrabbingErr"
QT_MOC_LITERAL(9, 109, 10), // "ParaSetErr"
QT_MOC_LITERAL(10, 120, 10), // "ParaGetErr"
QT_MOC_LITERAL(11, 131, 11), // "GetImageErr"
QT_MOC_LITERAL(12, 143, 11), // "InitDataErr"
QT_MOC_LITERAL(13, 155, 19), // "ExecuteDataNotRight"
QT_MOC_LITERAL(14, 175, 14) // "SaveImageError"

    },
    "Camera\0ErrorCode\0NoError\0Base\0"
    "EnumDevicesErr\0CameraSNNotExist\0"
    "CreateHandleErr\0CameraOpenErr\0"
    "StartGrabbingErr\0ParaSetErr\0ParaGetErr\0"
    "GetImageErr\0InitDataErr\0ExecuteDataNotRight\0"
    "SaveImageError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Camera[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   13,   19,

 // enum data: key, value
       2, uint(Camera::NoError),
       3, uint(Camera::Base),
       4, uint(Camera::EnumDevicesErr),
       5, uint(Camera::CameraSNNotExist),
       6, uint(Camera::CreateHandleErr),
       7, uint(Camera::CameraOpenErr),
       8, uint(Camera::StartGrabbingErr),
       9, uint(Camera::ParaSetErr),
      10, uint(Camera::ParaGetErr),
      11, uint(Camera::GetImageErr),
      12, uint(Camera::InitDataErr),
      13, uint(Camera::ExecuteDataNotRight),
      14, uint(Camera::SaveImageError),

       0        // eod
};

void Camera::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Camera::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_Camera.data,
    qt_meta_data_Camera,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Camera::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Camera::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Camera.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int Camera::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
