/****************************************************************************
** Meta object code from reading C++ file 'PLC.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../PLC.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PLC.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PLC_t {
    QByteArrayData data[28];
    char stringdata0[449];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PLC_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PLC_t qt_meta_stringdata_PLC = {
    {
QT_MOC_LITERAL(0, 0, 3), // "PLC"
QT_MOC_LITERAL(1, 4, 13), // "HeartBeatSlot"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 12), // "stateChanged"
QT_MOC_LITERAL(4, 32, 28), // "QAbstractSocket::SocketState"
QT_MOC_LITERAL(5, 61, 5), // "state"
QT_MOC_LITERAL(6, 67, 9), // "ErrorCode"
QT_MOC_LITERAL(7, 77, 7), // "NoError"
QT_MOC_LITERAL(8, 85, 4), // "Base"
QT_MOC_LITERAL(9, 90, 18), // "ConnectionPLCError"
QT_MOC_LITERAL(10, 109, 17), // "SignalTypeNotExit"
QT_MOC_LITERAL(11, 127, 13), // "RecvDataError"
QT_MOC_LITERAL(12, 141, 15), // "ModuleNameError"
QT_MOC_LITERAL(13, 157, 23), // "ModuleParaKeyNotContain"
QT_MOC_LITERAL(14, 181, 18), // "ModuleParaCmdError"
QT_MOC_LITERAL(15, 200, 24), // "ModuleParaFuncNotContain"
QT_MOC_LITERAL(16, 225, 12), // "ExecCmdError"
QT_MOC_LITERAL(17, 238, 11), // "InitDataErr"
QT_MOC_LITERAL(18, 250, 19), // "ExecuteDataNotRight"
QT_MOC_LITERAL(19, 270, 13), // "ClientHasOpen"
QT_MOC_LITERAL(20, 284, 14), // "ClientOpenFail"
QT_MOC_LITERAL(21, 299, 16), // "ClientWriteError"
QT_MOC_LITERAL(22, 316, 17), // "ClientReadTimeOut"
QT_MOC_LITERAL(23, 334, 10), // "PLC_Signal"
QT_MOC_LITERAL(24, 345, 23), // "PLC_Signal_Soft_Running"
QT_MOC_LITERAL(25, 369, 21), // "PLC_Signal_CCD_Camera"
QT_MOC_LITERAL(26, 391, 30), // "PLC_Signal_CCD_Camera_Complete"
QT_MOC_LITERAL(27, 422, 26) // "PLC_Signal_CCD_Algo_Result"

    },
    "PLC\0HeartBeatSlot\0\0stateChanged\0"
    "QAbstractSocket::SocketState\0state\0"
    "ErrorCode\0NoError\0Base\0ConnectionPLCError\0"
    "SignalTypeNotExit\0RecvDataError\0"
    "ModuleNameError\0ModuleParaKeyNotContain\0"
    "ModuleParaCmdError\0ModuleParaFuncNotContain\0"
    "ExecCmdError\0InitDataErr\0ExecuteDataNotRight\0"
    "ClientHasOpen\0ClientOpenFail\0"
    "ClientWriteError\0ClientReadTimeOut\0"
    "PLC_Signal\0PLC_Signal_Soft_Running\0"
    "PLC_Signal_CCD_Camera\0"
    "PLC_Signal_CCD_Camera_Complete\0"
    "PLC_Signal_CCD_Algo_Result"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PLC[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       2,   28, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x08 /* Private */,
       3,    1,   25,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,

 // enums: name, alias, flags, count, data
       6,    6, 0x0,   16,   38,
      23,   23, 0x0,    4,   70,

 // enum data: key, value
       7, uint(PLC::NoError),
       8, uint(PLC::Base),
       9, uint(PLC::ConnectionPLCError),
      10, uint(PLC::SignalTypeNotExit),
      11, uint(PLC::RecvDataError),
      12, uint(PLC::ModuleNameError),
      13, uint(PLC::ModuleParaKeyNotContain),
      14, uint(PLC::ModuleParaCmdError),
      15, uint(PLC::ModuleParaFuncNotContain),
      16, uint(PLC::ExecCmdError),
      17, uint(PLC::InitDataErr),
      18, uint(PLC::ExecuteDataNotRight),
      19, uint(PLC::ClientHasOpen),
      20, uint(PLC::ClientOpenFail),
      21, uint(PLC::ClientWriteError),
      22, uint(PLC::ClientReadTimeOut),
      24, uint(PLC::PLC_Signal_Soft_Running),
      25, uint(PLC::PLC_Signal_CCD_Camera),
      26, uint(PLC::PLC_Signal_CCD_Camera_Complete),
      27, uint(PLC::PLC_Signal_CCD_Algo_Result),

       0        // eod
};

void PLC::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PLC *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->HeartBeatSlot(); break;
        case 1: _t->stateChanged((*reinterpret_cast< QAbstractSocket::SocketState(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketState >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PLC::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_PLC.data,
    qt_meta_data_PLC,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PLC::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PLC::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PLC.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int PLC::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
