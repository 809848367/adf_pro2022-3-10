/****************************************************************************
** Meta object code from reading C++ file 'PanaMotor.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../PanaMotor.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PanaMotor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PanaMotor_t {
    QByteArrayData data[23];
    char stringdata0[302];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PanaMotor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PanaMotor_t qt_meta_stringdata_PanaMotor = {
    {
QT_MOC_LITERAL(0, 0, 9), // "PanaMotor"
QT_MOC_LITERAL(1, 10, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 20, 7), // "NoError"
QT_MOC_LITERAL(3, 28, 4), // "Base"
QT_MOC_LITERAL(4, 33, 12), // "ClearErrFail"
QT_MOC_LITERAL(5, 46, 11), // "StartCmdErr"
QT_MOC_LITERAL(6, 58, 10), // "StopCmdErr"
QT_MOC_LITERAL(7, 69, 15), // "GetStatusCmdErr"
QT_MOC_LITERAL(8, 85, 15), // "SetEnableCmdErr"
QT_MOC_LITERAL(9, 101, 12), // "SetVelCmdErr"
QT_MOC_LITERAL(10, 114, 14), // "ClearErrCmdErr"
QT_MOC_LITERAL(11, 129, 14), // "WaitArrTimeout"
QT_MOC_LITERAL(12, 144, 12), // "GetPosCmdErr"
QT_MOC_LITERAL(13, 157, 14), // "SetBlockCmdErr"
QT_MOC_LITERAL(14, 172, 15), // "GetEnableCmdErr"
QT_MOC_LITERAL(15, 188, 14), // "GetIsErrCmdErr"
QT_MOC_LITERAL(16, 203, 12), // "SetPosCmdErr"
QT_MOC_LITERAL(17, 216, 15), // "MoveBlockCmdErr"
QT_MOC_LITERAL(18, 232, 18), // "GetIsRunningCmdErr"
QT_MOC_LITERAL(19, 251, 11), // "InitDataErr"
QT_MOC_LITERAL(20, 263, 19), // "ExecuteDataNotRight"
QT_MOC_LITERAL(21, 283, 8), // "ResetErr"
QT_MOC_LITERAL(22, 292, 9) // "AxisIsErr"

    },
    "PanaMotor\0ErrorCode\0NoError\0Base\0"
    "ClearErrFail\0StartCmdErr\0StopCmdErr\0"
    "GetStatusCmdErr\0SetEnableCmdErr\0"
    "SetVelCmdErr\0ClearErrCmdErr\0WaitArrTimeout\0"
    "GetPosCmdErr\0SetBlockCmdErr\0GetEnableCmdErr\0"
    "GetIsErrCmdErr\0SetPosCmdErr\0MoveBlockCmdErr\0"
    "GetIsRunningCmdErr\0InitDataErr\0"
    "ExecuteDataNotRight\0ResetErr\0AxisIsErr"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PanaMotor[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   21,   19,

 // enum data: key, value
       2, uint(PanaMotor::NoError),
       3, uint(PanaMotor::Base),
       4, uint(PanaMotor::ClearErrFail),
       5, uint(PanaMotor::StartCmdErr),
       6, uint(PanaMotor::StopCmdErr),
       7, uint(PanaMotor::GetStatusCmdErr),
       8, uint(PanaMotor::SetEnableCmdErr),
       9, uint(PanaMotor::SetVelCmdErr),
      10, uint(PanaMotor::ClearErrCmdErr),
      11, uint(PanaMotor::WaitArrTimeout),
      12, uint(PanaMotor::GetPosCmdErr),
      13, uint(PanaMotor::SetBlockCmdErr),
      14, uint(PanaMotor::GetEnableCmdErr),
      15, uint(PanaMotor::GetIsErrCmdErr),
      16, uint(PanaMotor::SetPosCmdErr),
      17, uint(PanaMotor::MoveBlockCmdErr),
      18, uint(PanaMotor::GetIsRunningCmdErr),
      19, uint(PanaMotor::InitDataErr),
      20, uint(PanaMotor::ExecuteDataNotRight),
      21, uint(PanaMotor::ResetErr),
      22, uint(PanaMotor::AxisIsErr),

       0        // eod
};

void PanaMotor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PanaMotor::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_PanaMotor.data,
    qt_meta_data_PanaMotor,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PanaMotor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PanaMotor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PanaMotor.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int PanaMotor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
