/****************************************************************************
** Meta object code from reading C++ file 'RobotUI.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../RobotUI.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'RobotUI.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_RobotUI_t {
    QByteArrayData data[27];
    char stringdata0[435];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RobotUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RobotUI_t qt_meta_stringdata_RobotUI = {
    {
QT_MOC_LITERAL(0, 0, 7), // "RobotUI"
QT_MOC_LITERAL(1, 8, 15), // "UpDateTeachData"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 7), // "Station"
QT_MOC_LITERAL(4, 33, 15), // "TeachData_Robot"
QT_MOC_LITERAL(5, 49, 9), // "TeachData"
QT_MOC_LITERAL(6, 59, 11), // "RefreshSlot"
QT_MOC_LITERAL(7, 71, 19), // "on_ResetBtn_clicked"
QT_MOC_LITERAL(8, 91, 20), // "on_EnableBtn_clicked"
QT_MOC_LITERAL(9, 112, 21), // "on_DisableBtn_clicked"
QT_MOC_LITERAL(10, 134, 15), // "Pos_Add_clicked"
QT_MOC_LITERAL(11, 150, 1), // "n"
QT_MOC_LITERAL(12, 152, 17), // "Pos_Minus_clicked"
QT_MOC_LITERAL(13, 170, 15), // "MoveBtn_clicked"
QT_MOC_LITERAL(14, 186, 21), // "TeachDataBtns_clicked"
QT_MOC_LITERAL(15, 208, 22), // "TeachDataRadio_clicked"
QT_MOC_LITERAL(16, 231, 19), // "on_TeachAdd_clicked"
QT_MOC_LITERAL(17, 251, 19), // "on_TeachDel_clicked"
QT_MOC_LITERAL(18, 271, 24), // "on_TeachAddPoint_clicked"
QT_MOC_LITERAL(19, 296, 24), // "on_TeachDelPoint_clicked"
QT_MOC_LITERAL(20, 321, 23), // "on_MovewayPoint_clicked"
QT_MOC_LITERAL(21, 345, 24), // "on_TeachData_cellChanged"
QT_MOC_LITERAL(22, 370, 3), // "row"
QT_MOC_LITERAL(23, 374, 6), // "column"
QT_MOC_LITERAL(24, 381, 31), // "on_TeachList_currentTextChanged"
QT_MOC_LITERAL(25, 413, 4), // "arg1"
QT_MOC_LITERAL(26, 418, 16) // "RefreshTeachData"

    },
    "RobotUI\0UpDateTeachData\0\0Station\0"
    "TeachData_Robot\0TeachData\0RefreshSlot\0"
    "on_ResetBtn_clicked\0on_EnableBtn_clicked\0"
    "on_DisableBtn_clicked\0Pos_Add_clicked\0"
    "n\0Pos_Minus_clicked\0MoveBtn_clicked\0"
    "TeachDataBtns_clicked\0TeachDataRadio_clicked\0"
    "on_TeachAdd_clicked\0on_TeachDel_clicked\0"
    "on_TeachAddPoint_clicked\0"
    "on_TeachDelPoint_clicked\0"
    "on_MovewayPoint_clicked\0"
    "on_TeachData_cellChanged\0row\0column\0"
    "on_TeachList_currentTextChanged\0arg1\0"
    "RefreshTeachData"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RobotUI[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  104,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  109,    2, 0x08 /* Private */,
       7,    0,  110,    2, 0x08 /* Private */,
       8,    0,  111,    2, 0x08 /* Private */,
       9,    0,  112,    2, 0x08 /* Private */,
      10,    1,  113,    2, 0x08 /* Private */,
      12,    1,  116,    2, 0x08 /* Private */,
      13,    1,  119,    2, 0x08 /* Private */,
      14,    1,  122,    2, 0x08 /* Private */,
      15,    1,  125,    2, 0x08 /* Private */,
      16,    0,  128,    2, 0x08 /* Private */,
      17,    0,  129,    2, 0x08 /* Private */,
      18,    0,  130,    2, 0x08 /* Private */,
      19,    0,  131,    2, 0x08 /* Private */,
      20,    0,  132,    2, 0x08 /* Private */,
      21,    2,  133,    2, 0x08 /* Private */,
      24,    1,  138,    2, 0x08 /* Private */,
      26,    2,  141,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4,    3,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   22,   23,
    QMetaType::Void, QMetaType::QString,   25,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4,    3,    5,

       0        // eod
};

void RobotUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<RobotUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->UpDateTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Robot(*)>(_a[2]))); break;
        case 1: _t->RefreshSlot(); break;
        case 2: _t->on_ResetBtn_clicked(); break;
        case 3: _t->on_EnableBtn_clicked(); break;
        case 4: _t->on_DisableBtn_clicked(); break;
        case 5: _t->Pos_Add_clicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->Pos_Minus_clicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->MoveBtn_clicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->TeachDataBtns_clicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->TeachDataRadio_clicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_TeachAdd_clicked(); break;
        case 11: _t->on_TeachDel_clicked(); break;
        case 12: _t->on_TeachAddPoint_clicked(); break;
        case 13: _t->on_TeachDelPoint_clicked(); break;
        case 14: _t->on_MovewayPoint_clicked(); break;
        case 15: _t->on_TeachData_cellChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 16: _t->on_TeachList_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->RefreshTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_Robot(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (RobotUI::*)(QString , TeachData_Robot );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&RobotUI::UpDateTeachData)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject RobotUI::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_RobotUI.data,
    qt_meta_data_RobotUI,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *RobotUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RobotUI::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_RobotUI.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int RobotUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void RobotUI::UpDateTeachData(QString _t1, TeachData_Robot _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
