/****************************************************************************
** Meta object code from reading C++ file 'Robot.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Robot.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Robot.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Robot_t {
    QByteArrayData data[22];
    char stringdata0[313];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Robot_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Robot_t qt_meta_stringdata_Robot = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Robot"
QT_MOC_LITERAL(1, 6, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 16, 7), // "NoError"
QT_MOC_LITERAL(3, 24, 4), // "Base"
QT_MOC_LITERAL(4, 29, 19), // "NeedRemoteModeFirst"
QT_MOC_LITERAL(5, 49, 13), // "RobotHasAlarm"
QT_MOC_LITERAL(6, 63, 13), // "ParamNotRight"
QT_MOC_LITERAL(7, 77, 13), // "RecvErrorData"
QT_MOC_LITERAL(8, 91, 14), // "MoveByJointErr"
QT_MOC_LITERAL(9, 106, 13), // "MoveByLineErr"
QT_MOC_LITERAL(10, 120, 13), // "clearAlarmErr"
QT_MOC_LITERAL(11, 134, 7), // "stopErr"
QT_MOC_LITERAL(12, 142, 18), // "syncMotorStatusErr"
QT_MOC_LITERAL(13, 161, 19), // "set_servo_statusErr"
QT_MOC_LITERAL(14, 181, 17), // "clearPathPointErr"
QT_MOC_LITERAL(15, 199, 15), // "addPathPointErr"
QT_MOC_LITERAL(16, 215, 13), // "moveByPathErr"
QT_MOC_LITERAL(17, 229, 16), // "moveTypeNotExist"
QT_MOC_LITERAL(18, 246, 16), // "PoseSizeNotRight"
QT_MOC_LITERAL(19, 263, 17), // "WaitArriveTimeOut"
QT_MOC_LITERAL(20, 281, 11), // "InitDataErr"
QT_MOC_LITERAL(21, 293, 19) // "ExecuteDataNotRight"

    },
    "Robot\0ErrorCode\0NoError\0Base\0"
    "NeedRemoteModeFirst\0RobotHasAlarm\0"
    "ParamNotRight\0RecvErrorData\0MoveByJointErr\0"
    "MoveByLineErr\0clearAlarmErr\0stopErr\0"
    "syncMotorStatusErr\0set_servo_statusErr\0"
    "clearPathPointErr\0addPathPointErr\0"
    "moveByPathErr\0moveTypeNotExist\0"
    "PoseSizeNotRight\0WaitArriveTimeOut\0"
    "InitDataErr\0ExecuteDataNotRight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Robot[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   20,   19,

 // enum data: key, value
       2, uint(Robot::NoError),
       3, uint(Robot::Base),
       4, uint(Robot::NeedRemoteModeFirst),
       5, uint(Robot::RobotHasAlarm),
       6, uint(Robot::ParamNotRight),
       7, uint(Robot::RecvErrorData),
       8, uint(Robot::MoveByJointErr),
       9, uint(Robot::MoveByLineErr),
      10, uint(Robot::clearAlarmErr),
      11, uint(Robot::stopErr),
      12, uint(Robot::syncMotorStatusErr),
      13, uint(Robot::set_servo_statusErr),
      14, uint(Robot::clearPathPointErr),
      15, uint(Robot::addPathPointErr),
      16, uint(Robot::moveByPathErr),
      17, uint(Robot::moveTypeNotExist),
      18, uint(Robot::PoseSizeNotRight),
      19, uint(Robot::WaitArriveTimeOut),
      20, uint(Robot::InitDataErr),
      21, uint(Robot::ExecuteDataNotRight),

       0        // eod
};

void Robot::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Robot::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_Robot.data,
    qt_meta_data_Robot,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Robot::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Robot::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Robot.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int Robot::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
