/****************************************************************************
** Meta object code from reading C++ file 'CParentProcess.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../CParentProcess.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CParentProcess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CParentProcess_t {
    QByteArrayData data[80];
    char stringdata0[1198];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CParentProcess_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CParentProcess_t qt_meta_stringdata_CParentProcess = {
    {
QT_MOC_LITERAL(0, 0, 14), // "CParentProcess"
QT_MOC_LITERAL(1, 15, 11), // "SendProcMsg"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 3), // "msg"
QT_MOC_LITERAL(4, 32, 4), // "type"
QT_MOC_LITERAL(5, 37, 20), // "SendProMsgWithProfix"
QT_MOC_LITERAL(6, 58, 6), // "profix"
QT_MOC_LITERAL(7, 65, 15), // "StartProcSignal"
QT_MOC_LITERAL(8, 81, 23), // "SignalExitExceptionFlag"
QT_MOC_LITERAL(9, 105, 9), // "ErrorType"
QT_MOC_LITERAL(10, 115, 13), // "SignalSendImg"
QT_MOC_LITERAL(11, 129, 5), // "Datas"
QT_MOC_LITERAL(12, 135, 20), // "SignalMachineRefresh"
QT_MOC_LITERAL(13, 156, 6), // "result"
QT_MOC_LITERAL(14, 163, 15), // "SignalProcState"
QT_MOC_LITERAL(15, 179, 8), // "procName"
QT_MOC_LITERAL(16, 188, 28), // "CParentProcess::ProcessState"
QT_MOC_LITERAL(17, 217, 5), // "state"
QT_MOC_LITERAL(18, 223, 15), // "SignalProcStart"
QT_MOC_LITERAL(19, 239, 14), // "SignalProcIdle"
QT_MOC_LITERAL(20, 254, 20), // "SignalProCurrentStep"
QT_MOC_LITERAL(21, 275, 4), // "step"
QT_MOC_LITERAL(22, 280, 14), // "SignalShowInfo"
QT_MOC_LITERAL(23, 295, 4), // "info"
QT_MOC_LITERAL(24, 300, 9), // "StartProc"
QT_MOC_LITERAL(25, 310, 16), // "ExecOperatorSlot"
QT_MOC_LITERAL(26, 327, 13), // "OperatorInfo*"
QT_MOC_LITERAL(27, 341, 2), // "op"
QT_MOC_LITERAL(28, 344, 12), // "ExecStepSlot"
QT_MOC_LITERAL(29, 357, 9), // "StepInfo*"
QT_MOC_LITERAL(30, 367, 12), // "SetStepIndex"
QT_MOC_LITERAL(31, 380, 5), // "index"
QT_MOC_LITERAL(32, 386, 12), // "ProcessState"
QT_MOC_LITERAL(33, 399, 19), // "Process_Status_Idle"
QT_MOC_LITERAL(34, 419, 20), // "Process_Status_Start"
QT_MOC_LITERAL(35, 440, 19), // "Process_Status_Work"
QT_MOC_LITERAL(36, 460, 20), // "Process_Status_Pause"
QT_MOC_LITERAL(37, 481, 23), // "Process_Status_Continue"
QT_MOC_LITERAL(38, 505, 19), // "Process_Status_Stop"
QT_MOC_LITERAL(39, 525, 18), // "Process_Status_End"
QT_MOC_LITERAL(40, 544, 27), // "Process_Status_ErrorMoveEnd"
QT_MOC_LITERAL(41, 572, 27), // "Process_Status_ErrorInfoEnd"
QT_MOC_LITERAL(42, 600, 23), // "Process_Status_ErrorEnd"
QT_MOC_LITERAL(43, 624, 19), // "Process_Status_Test"
QT_MOC_LITERAL(44, 644, 9), // "ErrorCode"
QT_MOC_LITERAL(45, 654, 7), // "NoError"
QT_MOC_LITERAL(46, 662, 4), // "Base"
QT_MOC_LITERAL(47, 667, 17), // "Error_Type_Common"
QT_MOC_LITERAL(48, 685, 10), // "KeyNotFind"
QT_MOC_LITERAL(49, 696, 20), // "CurrentStateNotAllow"
QT_MOC_LITERAL(50, 717, 9), // "MoveError"
QT_MOC_LITERAL(51, 727, 9), // "InfoError"
QT_MOC_LITERAL(52, 737, 12), // "ExecuteError"
QT_MOC_LITERAL(53, 750, 15), // "NotFindStepInfo"
QT_MOC_LITERAL(54, 766, 22), // "EndStepCodeSuportError"
QT_MOC_LITERAL(55, 789, 22), // "OperatorTypeNotContain"
QT_MOC_LITERAL(56, 812, 11), // "BaseExtend1"
QT_MOC_LITERAL(57, 824, 12), // "FileNotExist"
QT_MOC_LITERAL(58, 837, 14), // "ParamTypeError"
QT_MOC_LITERAL(59, 852, 15), // "ProcExtendExist"
QT_MOC_LITERAL(60, 868, 12), // "TimeOutError"
QT_MOC_LITERAL(61, 881, 15), // "ParamValueError"
QT_MOC_LITERAL(62, 897, 15), // "FlagNameNotFind"
QT_MOC_LITERAL(63, 913, 13), // "ResultNotPass"
QT_MOC_LITERAL(64, 927, 11), // "ResultError"
QT_MOC_LITERAL(65, 939, 12), // "ResultFailed"
QT_MOC_LITERAL(66, 952, 16), // "ParamFormatError"
QT_MOC_LITERAL(67, 969, 11), // "BaseExtend2"
QT_MOC_LITERAL(68, 981, 14), // "TempKeyNotFind"
QT_MOC_LITERAL(69, 996, 19), // "ClientSendDataError"
QT_MOC_LITERAL(70, 1016, 12), // "DataNotMatch"
QT_MOC_LITERAL(71, 1029, 27), // "ProcWaitFlowEndExecuteError"
QT_MOC_LITERAL(72, 1057, 29), // "ProcWaitFlowStartExecuteError"
QT_MOC_LITERAL(73, 1087, 16), // "NotFindProcError"
QT_MOC_LITERAL(74, 1104, 9), // "ForceExit"
QT_MOC_LITERAL(75, 1114, 18), // "ClientNotFindError"
QT_MOC_LITERAL(76, 1133, 19), // "ParamLengthNotPatch"
QT_MOC_LITERAL(77, 1153, 13), // "UndefineError"
QT_MOC_LITERAL(78, 1167, 14), // "TestDebugError"
QT_MOC_LITERAL(79, 1182, 15) // "ParamNotContain"

    },
    "CParentProcess\0SendProcMsg\0\0msg\0type\0"
    "SendProMsgWithProfix\0profix\0StartProcSignal\0"
    "SignalExitExceptionFlag\0ErrorType\0"
    "SignalSendImg\0Datas\0SignalMachineRefresh\0"
    "result\0SignalProcState\0procName\0"
    "CParentProcess::ProcessState\0state\0"
    "SignalProcStart\0SignalProcIdle\0"
    "SignalProCurrentStep\0step\0SignalShowInfo\0"
    "info\0StartProc\0ExecOperatorSlot\0"
    "OperatorInfo*\0op\0ExecStepSlot\0StepInfo*\0"
    "SetStepIndex\0index\0ProcessState\0"
    "Process_Status_Idle\0Process_Status_Start\0"
    "Process_Status_Work\0Process_Status_Pause\0"
    "Process_Status_Continue\0Process_Status_Stop\0"
    "Process_Status_End\0Process_Status_ErrorMoveEnd\0"
    "Process_Status_ErrorInfoEnd\0"
    "Process_Status_ErrorEnd\0Process_Status_Test\0"
    "ErrorCode\0NoError\0Base\0Error_Type_Common\0"
    "KeyNotFind\0CurrentStateNotAllow\0"
    "MoveError\0InfoError\0ExecuteError\0"
    "NotFindStepInfo\0EndStepCodeSuportError\0"
    "OperatorTypeNotContain\0BaseExtend1\0"
    "FileNotExist\0ParamTypeError\0ProcExtendExist\0"
    "TimeOutError\0ParamValueError\0"
    "FlagNameNotFind\0ResultNotPass\0ResultError\0"
    "ResultFailed\0ParamFormatError\0BaseExtend2\0"
    "TempKeyNotFind\0ClientSendDataError\0"
    "DataNotMatch\0ProcWaitFlowEndExecuteError\0"
    "ProcWaitFlowStartExecuteError\0"
    "NotFindProcError\0ForceExit\0"
    "ClientNotFindError\0ParamLengthNotPatch\0"
    "UndefineError\0TestDebugError\0"
    "ParamNotContain"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CParentProcess[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       2,  170, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  104,    2, 0x06 /* Public */,
       1,    1,  109,    2, 0x26 /* Public | MethodCloned */,
       5,    3,  112,    2, 0x06 /* Public */,
       5,    2,  119,    2, 0x26 /* Public | MethodCloned */,
       7,    0,  124,    2, 0x06 /* Public */,
       8,    2,  125,    2, 0x06 /* Public */,
       8,    1,  130,    2, 0x26 /* Public | MethodCloned */,
      10,    1,  133,    2, 0x06 /* Public */,
      12,    1,  136,    2, 0x06 /* Public */,
      14,    2,  139,    2, 0x06 /* Public */,
      18,    1,  144,    2, 0x06 /* Public */,
      19,    1,  147,    2, 0x06 /* Public */,
      20,    2,  150,    2, 0x06 /* Public */,
      22,    2,  155,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      24,    0,  160,    2, 0x0a /* Public */,
      25,    1,  161,    2, 0x0a /* Public */,
      28,    1,  164,    2, 0x0a /* Public */,
      30,    1,  167,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString, QMetaType::Int,    6,    3,    4,
    QMetaType::Void, QMetaType::QStringList, QMetaType::QString,    6,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    9,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QVariantMap,   11,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 16,   15,   17,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   15,   21,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   23,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 26,   27,
    QMetaType::Void, 0x80000000 | 29,   21,
    QMetaType::Void, QMetaType::Int,   31,

 // enums: name, alias, flags, count, data
      32,   32, 0x0,   11,  180,
      44,   44, 0x0,   35,  202,

 // enum data: key, value
      33, uint(CParentProcess::Process_Status_Idle),
      34, uint(CParentProcess::Process_Status_Start),
      35, uint(CParentProcess::Process_Status_Work),
      36, uint(CParentProcess::Process_Status_Pause),
      37, uint(CParentProcess::Process_Status_Continue),
      38, uint(CParentProcess::Process_Status_Stop),
      39, uint(CParentProcess::Process_Status_End),
      40, uint(CParentProcess::Process_Status_ErrorMoveEnd),
      41, uint(CParentProcess::Process_Status_ErrorInfoEnd),
      42, uint(CParentProcess::Process_Status_ErrorEnd),
      43, uint(CParentProcess::Process_Status_Test),
      45, uint(CParentProcess::NoError),
      46, uint(CParentProcess::Base),
      47, uint(CParentProcess::Error_Type_Common),
      48, uint(CParentProcess::KeyNotFind),
      49, uint(CParentProcess::CurrentStateNotAllow),
      50, uint(CParentProcess::MoveError),
      51, uint(CParentProcess::InfoError),
      52, uint(CParentProcess::ExecuteError),
      53, uint(CParentProcess::NotFindStepInfo),
      54, uint(CParentProcess::EndStepCodeSuportError),
      55, uint(CParentProcess::OperatorTypeNotContain),
      56, uint(CParentProcess::BaseExtend1),
      57, uint(CParentProcess::FileNotExist),
      58, uint(CParentProcess::ParamTypeError),
      59, uint(CParentProcess::ProcExtendExist),
      60, uint(CParentProcess::TimeOutError),
      61, uint(CParentProcess::ParamValueError),
      62, uint(CParentProcess::FlagNameNotFind),
      63, uint(CParentProcess::ResultNotPass),
      64, uint(CParentProcess::ResultError),
      65, uint(CParentProcess::ResultFailed),
      66, uint(CParentProcess::ParamFormatError),
      67, uint(CParentProcess::BaseExtend2),
      68, uint(CParentProcess::TempKeyNotFind),
      69, uint(CParentProcess::ClientSendDataError),
      70, uint(CParentProcess::DataNotMatch),
      71, uint(CParentProcess::ProcWaitFlowEndExecuteError),
      72, uint(CParentProcess::ProcWaitFlowStartExecuteError),
      73, uint(CParentProcess::NotFindProcError),
      74, uint(CParentProcess::ForceExit),
      75, uint(CParentProcess::ClientNotFindError),
      76, uint(CParentProcess::ParamLengthNotPatch),
      77, uint(CParentProcess::UndefineError),
      78, uint(CParentProcess::TestDebugError),
      79, uint(CParentProcess::ParamNotContain),

       0        // eod
};

void CParentProcess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CParentProcess *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SendProcMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->SendProcMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->SendProMsgWithProfix((*reinterpret_cast< QStringList(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->SendProMsgWithProfix((*reinterpret_cast< QStringList(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->StartProcSignal(); break;
        case 5: _t->SignalExitExceptionFlag((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->SignalExitExceptionFlag((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->SignalSendImg((*reinterpret_cast< QMap<QString,QVariant>(*)>(_a[1]))); break;
        case 8: _t->SignalMachineRefresh((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->SignalProcState((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< CParentProcess::ProcessState(*)>(_a[2]))); break;
        case 10: _t->SignalProcStart((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->SignalProcIdle((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->SignalProCurrentStep((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 13: _t->SignalShowInfo((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 14: _t->StartProc(); break;
        case 15: _t->ExecOperatorSlot((*reinterpret_cast< OperatorInfo*(*)>(_a[1]))); break;
        case 16: _t->ExecStepSlot((*reinterpret_cast< StepInfo*(*)>(_a[1]))); break;
        case 17: _t->SetStepIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CParentProcess::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SendProcMsg)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QStringList , QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SendProMsgWithProfix)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::StartProcSignal)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalExitExceptionFlag)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QMap<QString,QVariant> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalSendImg)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalMachineRefresh)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString , CParentProcess::ProcessState );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalProcState)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalProcStart)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalProcIdle)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalProCurrentStep)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (CParentProcess::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CParentProcess::SignalShowInfo)) {
                *result = 13;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CParentProcess::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_CParentProcess.data,
    qt_meta_data_CParentProcess,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CParentProcess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CParentProcess::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CParentProcess.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int CParentProcess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void CParentProcess::SendProcMsg(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 2
void CParentProcess::SendProMsgWithProfix(QStringList _t1, QString _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 4
void CParentProcess::StartProcSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void CParentProcess::SignalExitExceptionFlag(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 7
void CParentProcess::SignalSendImg(QMap<QString,QVariant> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void CParentProcess::SignalMachineRefresh(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void CParentProcess::SignalProcState(QString _t1, CParentProcess::ProcessState _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void CParentProcess::SignalProcStart(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void CParentProcess::SignalProcIdle(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void CParentProcess::SignalProCurrentStep(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void CParentProcess::SignalShowInfo(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
