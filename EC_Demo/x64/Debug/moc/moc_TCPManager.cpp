/****************************************************************************
** Meta object code from reading C++ file 'TCPManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../TCPManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TCPManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TCPManager_t {
    QByteArrayData data[48];
    char stringdata0[841];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TCPManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TCPManager_t qt_meta_stringdata_TCPManager = {
    {
QT_MOC_LITERAL(0, 0, 10), // "TCPManager"
QT_MOC_LITERAL(1, 11, 17), // "emit_CreateServer"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 9), // "severName"
QT_MOC_LITERAL(4, 40, 4), // "Port"
QT_MOC_LITERAL(5, 45, 13), // "MaxConnectNum"
QT_MOC_LITERAL(6, 59, 28), // "emit_ServerSendNewServerName"
QT_MOC_LITERAL(7, 88, 28), // "emit_SeverSendConnectSktName"
QT_MOC_LITERAL(8, 117, 10), // "serverName"
QT_MOC_LITERAL(9, 128, 10), // "sktObjName"
QT_MOC_LITERAL(10, 139, 33), // "emit_SeverSendDisConnectedSkt..."
QT_MOC_LITERAL(11, 173, 28), // "emit_Clinet_SendDataToServer"
QT_MOC_LITERAL(12, 202, 10), // "clientName"
QT_MOC_LITERAL(13, 213, 7), // "dataStr"
QT_MOC_LITERAL(14, 221, 38), // "emit_Client_CreateOnClientCon..."
QT_MOC_LITERAL(15, 260, 2), // "IP"
QT_MOC_LITERAL(16, 263, 26), // "emit_Client_TouchReadReady"
QT_MOC_LITERAL(17, 290, 27), // "emit_Client_ConnectedClient"
QT_MOC_LITERAL(18, 318, 30), // "emit_Client_DisconnectedClient"
QT_MOC_LITERAL(19, 349, 17), // "Slot_CreateServer"
QT_MOC_LITERAL(20, 367, 9), // "CtrlError"
QT_MOC_LITERAL(21, 377, 17), // "Server_NewConnect"
QT_MOC_LITERAL(22, 395, 14), // "Server_SktRecv"
QT_MOC_LITERAL(23, 410, 22), // "Sever_SktDisconntected"
QT_MOC_LITERAL(24, 433, 23), // "Client_SendDataToServer"
QT_MOC_LITERAL(25, 457, 38), // "Slot_Client_CreateOnClientCon..."
QT_MOC_LITERAL(26, 496, 32), // "Client_CatchOneDataFromServerMsg"
QT_MOC_LITERAL(27, 529, 8), // "QString&"
QT_MOC_LITERAL(28, 538, 6), // "length"
QT_MOC_LITERAL(29, 545, 31), // "Transmit_Client_SendDataToSever"
QT_MOC_LITERAL(30, 577, 25), // "Slot_UserDisconnectClient"
QT_MOC_LITERAL(31, 603, 25), // "Slot_AutoDisconnectClient"
QT_MOC_LITERAL(32, 629, 15), // "ClientReadReady"
QT_MOC_LITERAL(33, 645, 16), // "ClientRecvResult"
QT_MOC_LITERAL(34, 662, 9), // "ErrorCode"
QT_MOC_LITERAL(35, 672, 7), // "NoError"
QT_MOC_LITERAL(36, 680, 4), // "Base"
QT_MOC_LITERAL(37, 685, 19), // "ConnectionSeverFail"
QT_MOC_LITERAL(38, 705, 11), // "SocketError"
QT_MOC_LITERAL(39, 717, 12), // "TimeOutError"
QT_MOC_LITERAL(40, 730, 10), // "NoSeverCmd"
QT_MOC_LITERAL(41, 741, 13), // "ClientHasLive"
QT_MOC_LITERAL(42, 755, 13), // "ClientNotFind"
QT_MOC_LITERAL(43, 769, 13), // "ClientNotOpen"
QT_MOC_LITERAL(44, 783, 18), // "RecvStrBufferEmpty"
QT_MOC_LITERAL(45, 802, 14), // "InitSeverError"
QT_MOC_LITERAL(46, 817, 5), // "Base2"
QT_MOC_LITERAL(47, 823, 17) // "NotFindConnectSkt"

    },
    "TCPManager\0emit_CreateServer\0\0severName\0"
    "Port\0MaxConnectNum\0emit_ServerSendNewServerName\0"
    "emit_SeverSendConnectSktName\0serverName\0"
    "sktObjName\0emit_SeverSendDisConnectedSktName\0"
    "emit_Clinet_SendDataToServer\0clientName\0"
    "dataStr\0emit_Client_CreateOnClientConnectSever\0"
    "IP\0emit_Client_TouchReadReady\0"
    "emit_Client_ConnectedClient\0"
    "emit_Client_DisconnectedClient\0"
    "Slot_CreateServer\0CtrlError\0"
    "Server_NewConnect\0Server_SktRecv\0"
    "Sever_SktDisconntected\0Client_SendDataToServer\0"
    "Slot_Client_CreateOnClientConnectSever\0"
    "Client_CatchOneDataFromServerMsg\0"
    "QString&\0length\0Transmit_Client_SendDataToSever\0"
    "Slot_UserDisconnectClient\0"
    "Slot_AutoDisconnectClient\0ClientReadReady\0"
    "ClientRecvResult\0ErrorCode\0NoError\0"
    "Base\0ConnectionSeverFail\0SocketError\0"
    "TimeOutError\0NoSeverCmd\0ClientHasLive\0"
    "ClientNotFind\0ClientNotOpen\0"
    "RecvStrBufferEmpty\0InitSeverError\0"
    "Base2\0NotFindConnectSkt"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TCPManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       1,  212, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,  124,    2, 0x06 /* Public */,
       6,    1,  131,    2, 0x06 /* Public */,
       7,    2,  134,    2, 0x06 /* Public */,
      10,    2,  139,    2, 0x06 /* Public */,
      11,    2,  144,    2, 0x06 /* Public */,
      14,    3,  149,    2, 0x06 /* Public */,
      16,    1,  156,    2, 0x06 /* Public */,
      17,    1,  159,    2, 0x06 /* Public */,
      18,    1,  162,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    3,  165,    2, 0x0a /* Public */,
      21,    0,  172,    2, 0x08 /* Private */,
      22,    0,  173,    2, 0x08 /* Private */,
      23,    0,  174,    2, 0x08 /* Private */,
      24,    2,  175,    2, 0x0a /* Public */,
      25,    3,  180,    2, 0x0a /* Public */,
      26,    2,  187,    2, 0x0a /* Public */,
      26,    3,  192,    2, 0x0a /* Public */,
      29,    2,  199,    2, 0x0a /* Public */,
      30,    1,  204,    2, 0x0a /* Public */,
      31,    0,  207,    2, 0x0a /* Public */,
      32,    1,  208,    2, 0x0a /* Public */,
      33,    0,  211,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int, QMetaType::Int,    3,    4,    5,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    8,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    8,    9,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,   12,   15,    4,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString,   12,

 // slots: parameters
    0x80000000 | 20, QMetaType::QString, QMetaType::Int, QMetaType::Int,    3,    4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 20, QMetaType::QString, QMetaType::QString,   12,   13,
    0x80000000 | 20, QMetaType::QString, QMetaType::QString, QMetaType::Int,   12,   15,    4,
    0x80000000 | 20, QMetaType::QString, 0x80000000 | 27,   12,   13,
    0x80000000 | 20, QMetaType::QString, 0x80000000 | 27, QMetaType::Int,   12,   13,   28,
    0x80000000 | 20, QMetaType::QString, QMetaType::QString,   12,   13,
    0x80000000 | 20, QMetaType::QString,   12,
    0x80000000 | 20,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,

 // enums: name, alias, flags, count, data
      34,   34, 0x0,   13,  217,

 // enum data: key, value
      35, uint(TCPManager::NoError),
      36, uint(TCPManager::Base),
      37, uint(TCPManager::ConnectionSeverFail),
      38, uint(TCPManager::SocketError),
      39, uint(TCPManager::TimeOutError),
      40, uint(TCPManager::NoSeverCmd),
      41, uint(TCPManager::ClientHasLive),
      42, uint(TCPManager::ClientNotFind),
      43, uint(TCPManager::ClientNotOpen),
      44, uint(TCPManager::RecvStrBufferEmpty),
      45, uint(TCPManager::InitSeverError),
      46, uint(TCPManager::Base2),
      47, uint(TCPManager::NotFindConnectSkt),

       0        // eod
};

void TCPManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TCPManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->emit_CreateServer((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->emit_ServerSendNewServerName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->emit_SeverSendConnectSktName((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->emit_SeverSendDisConnectedSktName((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->emit_Clinet_SendDataToServer((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->emit_Client_CreateOnClientConnectSever((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->emit_Client_TouchReadReady((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->emit_Client_ConnectedClient((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->emit_Client_DisconnectedClient((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: { CtrlError _r = _t->Slot_CreateServer((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 10: _t->Server_NewConnect(); break;
        case 11: _t->Server_SktRecv(); break;
        case 12: _t->Sever_SktDisconntected(); break;
        case 13: { CtrlError _r = _t->Client_SendDataToServer((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 14: { CtrlError _r = _t->Slot_Client_CreateOnClientConnectSever((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 15: { CtrlError _r = _t->Client_CatchOneDataFromServerMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 16: { CtrlError _r = _t->Client_CatchOneDataFromServerMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 17: { CtrlError _r = _t->Transmit_Client_SendDataToSever((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 18: { CtrlError _r = _t->Slot_UserDisconnectClient((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 19: { CtrlError _r = _t->Slot_AutoDisconnectClient();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 20: _t->ClientReadReady((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 21: _t->ClientRecvResult(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TCPManager::*)(QString , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_CreateServer)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_ServerSendNewServerName)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_SeverSendConnectSktName)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_SeverSendDisConnectedSktName)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_Clinet_SendDataToServer)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString , QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_Client_CreateOnClientConnectSever)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_Client_TouchReadReady)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_Client_ConnectedClient)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (TCPManager::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCPManager::emit_Client_DisconnectedClient)) {
                *result = 8;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TCPManager::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_TCPManager.data,
    qt_meta_data_TCPManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TCPManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TCPManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TCPManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int TCPManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void TCPManager::emit_CreateServer(QString _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TCPManager::emit_ServerSendNewServerName(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TCPManager::emit_SeverSendConnectSktName(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void TCPManager::emit_SeverSendDisConnectedSktName(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TCPManager::emit_Clinet_SendDataToServer(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void TCPManager::emit_Client_CreateOnClientConnectSever(QString _t1, QString _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void TCPManager::emit_Client_TouchReadReady(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void TCPManager::emit_Client_ConnectedClient(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void TCPManager::emit_Client_DisconnectedClient(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
