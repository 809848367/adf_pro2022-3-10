/****************************************************************************
** Meta object code from reading C++ file 'History.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../History.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'History.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_History_t {
    QByteArrayData data[12];
    char stringdata0[151];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_History_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_History_t qt_meta_stringdata_History = {
    {
QT_MOC_LITERAL(0, 0, 7), // "History"
QT_MOC_LITERAL(1, 8, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 18, 7), // "NoError"
QT_MOC_LITERAL(3, 26, 4), // "Base"
QT_MOC_LITERAL(4, 31, 10), // "OpenDBFail"
QT_MOC_LITERAL(5, 42, 13), // "CreateDBError"
QT_MOC_LITERAL(6, 56, 18), // "InsertDataNotRigth"
QT_MOC_LITERAL(7, 75, 15), // "InsertDataError"
QT_MOC_LITERAL(8, 91, 13), // "GetRowidError"
QT_MOC_LITERAL(9, 105, 12), // "WriteDBError"
QT_MOC_LITERAL(10, 118, 16), // "TRANSACTIONError"
QT_MOC_LITERAL(11, 135, 15) // "SelectDataError"

    },
    "History\0ErrorCode\0NoError\0Base\0"
    "OpenDBFail\0CreateDBError\0InsertDataNotRigth\0"
    "InsertDataError\0GetRowidError\0"
    "WriteDBError\0TRANSACTIONError\0"
    "SelectDataError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_History[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   10,   19,

 // enum data: key, value
       2, uint(History::NoError),
       3, uint(History::Base),
       4, uint(History::OpenDBFail),
       5, uint(History::CreateDBError),
       6, uint(History::InsertDataNotRigth),
       7, uint(History::InsertDataError),
       8, uint(History::GetRowidError),
       9, uint(History::WriteDBError),
      10, uint(History::TRANSACTIONError),
      11, uint(History::SelectDataError),

       0        // eod
};

void History::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject History::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_History.data,
    qt_meta_data_History,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *History::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *History::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_History.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int History::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
