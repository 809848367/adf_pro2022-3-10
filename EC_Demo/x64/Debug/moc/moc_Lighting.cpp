/****************************************************************************
** Meta object code from reading C++ file 'Lighting.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../Lighting.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Lighting.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Lighting_t {
    QByteArrayData data[11];
    char stringdata0[151];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Lighting_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Lighting_t qt_meta_stringdata_Lighting = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Lighting"
QT_MOC_LITERAL(1, 9, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 19, 7), // "NoError"
QT_MOC_LITERAL(3, 27, 4), // "Base"
QT_MOC_LITERAL(4, 32, 20), // "ChannelsNotEqualData"
QT_MOC_LITERAL(5, 53, 21), // "ChannelsNotEqualValue"
QT_MOC_LITERAL(6, 75, 15), // "IndexOutOfRange"
QT_MOC_LITERAL(7, 91, 11), // "InitDataErr"
QT_MOC_LITERAL(8, 103, 10), // "SPMisEmpty"
QT_MOC_LITERAL(9, 114, 19), // "ExecuteDataNotRight"
QT_MOC_LITERAL(10, 134, 16) // "WaitLightTimeout"

    },
    "Lighting\0ErrorCode\0NoError\0Base\0"
    "ChannelsNotEqualData\0ChannelsNotEqualValue\0"
    "IndexOutOfRange\0InitDataErr\0SPMisEmpty\0"
    "ExecuteDataNotRight\0WaitLightTimeout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Lighting[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    9,   19,

 // enum data: key, value
       2, uint(Lighting::NoError),
       3, uint(Lighting::Base),
       4, uint(Lighting::ChannelsNotEqualData),
       5, uint(Lighting::ChannelsNotEqualValue),
       6, uint(Lighting::IndexOutOfRange),
       7, uint(Lighting::InitDataErr),
       8, uint(Lighting::SPMisEmpty),
       9, uint(Lighting::ExecuteDataNotRight),
      10, uint(Lighting::WaitLightTimeout),

       0        // eod
};

void Lighting::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Lighting::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_Lighting.data,
    qt_meta_data_Lighting,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Lighting::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Lighting::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Lighting.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int Lighting::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
