/********************************************************************************
** Form generated from reading UI file 'IOControlUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IOCONTROLUI_H
#define UI_IOCONTROLUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IOControlUI
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTableWidget *InPutList;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QTableWidget *OutPutList;

    void setupUi(QWidget *IOControlUI)
    {
        if (IOControlUI->objectName().isEmpty())
            IOControlUI->setObjectName(QString::fromUtf8("IOControlUI"));
        IOControlUI->resize(606, 589);
        horizontalLayout = new QHBoxLayout(IOControlUI);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(IOControlUI);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(0, 30));
        label->setMaximumSize(QSize(16777215, 30));

        verticalLayout->addWidget(label);

        InPutList = new QTableWidget(IOControlUI);
        InPutList->setObjectName(QString::fromUtf8("InPutList"));

        verticalLayout->addWidget(InPutList);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(IOControlUI);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(0, 30));
        label_2->setMaximumSize(QSize(16777215, 30));

        verticalLayout_2->addWidget(label_2);

        OutPutList = new QTableWidget(IOControlUI);
        OutPutList->setObjectName(QString::fromUtf8("OutPutList"));

        verticalLayout_2->addWidget(OutPutList);


        horizontalLayout->addLayout(verticalLayout_2);


        retranslateUi(IOControlUI);

        QMetaObject::connectSlotsByName(IOControlUI);
    } // setupUi

    void retranslateUi(QWidget *IOControlUI)
    {
        IOControlUI->setWindowTitle(QApplication::translate("IOControlUI", "Form", nullptr));
        label->setText(QApplication::translate("IOControlUI", "Input", nullptr));
        label_2->setText(QApplication::translate("IOControlUI", "OutPut", nullptr));
    } // retranslateUi

};

namespace Ui {
    class IOControlUI: public Ui_IOControlUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IOCONTROLUI_H
