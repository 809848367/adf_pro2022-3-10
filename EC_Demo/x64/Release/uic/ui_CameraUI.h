/********************************************************************************
** Form generated from reading UI file 'CameraUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERAUI_H
#define UI_CAMERAUI_H

#include <QFigure.h>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CameraUI
{
public:
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QFigure *IMShow;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QDoubleSpinBox *Exposure;
    QSpacerItem *horizontalSpacer;
    QLabel *label_2;
    QDoubleSpinBox *Gamma;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_3;
    QDoubleSpinBox *Gain;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *Open;
    QPushButton *Close;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *GetImage;
    QPushButton *SaveImage;
    QVBoxLayout *verticalLayout;
    QTableWidget *TeachData;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *Teach_Add;
    QPushButton *Teach_Del;
    QPushButton *Teach_Get;
    QPushButton *Teach_Save;
    QSpacerItem *horizontalSpacer_4;

    void setupUi(QWidget *CameraUI)
    {
        if (CameraUI->objectName().isEmpty())
            CameraUI->setObjectName(QString::fromUtf8("CameraUI"));
        CameraUI->resize(875, 617);
        horizontalLayout_4 = new QHBoxLayout(CameraUI);
        horizontalLayout_4->setSpacing(10);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(10, 10, 10, 10);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(10);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        IMShow = new QFigure(CameraUI);
        IMShow->setObjectName(QString::fromUtf8("IMShow"));
        IMShow->setFrameShape(QFrame::Box);

        verticalLayout_2->addWidget(IMShow);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(CameraUI);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(70, 30));
        label->setMaximumSize(QSize(70, 30));

        horizontalLayout->addWidget(label);

        Exposure = new QDoubleSpinBox(CameraUI);
        Exposure->setObjectName(QString::fromUtf8("Exposure"));
        Exposure->setMinimumSize(QSize(100, 30));
        Exposure->setMaximumSize(QSize(16777215, 30));
        Exposure->setMinimum(28.000000000000000);
        Exposure->setMaximum(700000.000000000000000);
        Exposure->setValue(5000.000000000000000);

        horizontalLayout->addWidget(Exposure);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_2 = new QLabel(CameraUI);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(40, 30));
        label_2->setMaximumSize(QSize(40, 30));

        horizontalLayout->addWidget(label_2);

        Gamma = new QDoubleSpinBox(CameraUI);
        Gamma->setObjectName(QString::fromUtf8("Gamma"));
        Gamma->setMinimumSize(QSize(100, 30));
        Gamma->setMaximumSize(QSize(16777215, 30));
        Gamma->setMaximum(4.000000000000000);
        Gamma->setSingleStep(0.100000000000000);
        Gamma->setValue(1.000000000000000);

        horizontalLayout->addWidget(Gamma);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        label_3 = new QLabel(CameraUI);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(40, 30));
        label_3->setMaximumSize(QSize(40, 30));

        horizontalLayout->addWidget(label_3);

        Gain = new QDoubleSpinBox(CameraUI);
        Gain->setObjectName(QString::fromUtf8("Gain"));
        Gain->setMinimumSize(QSize(100, 30));
        Gain->setMaximumSize(QSize(16777215, 30));
        Gain->setMaximum(20.000000000000000);
        Gain->setSingleStep(0.100000000000000);
        Gain->setValue(1.000000000000000);

        horizontalLayout->addWidget(Gain);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        Open = new QPushButton(CameraUI);
        Open->setObjectName(QString::fromUtf8("Open"));
        Open->setMinimumSize(QSize(0, 30));
        Open->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_2->addWidget(Open);

        Close = new QPushButton(CameraUI);
        Close->setObjectName(QString::fromUtf8("Close"));
        Close->setMinimumSize(QSize(0, 30));
        Close->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_2->addWidget(Close);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        GetImage = new QPushButton(CameraUI);
        GetImage->setObjectName(QString::fromUtf8("GetImage"));
        GetImage->setMinimumSize(QSize(0, 30));
        GetImage->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_2->addWidget(GetImage);

        SaveImage = new QPushButton(CameraUI);
        SaveImage->setObjectName(QString::fromUtf8("SaveImage"));
        SaveImage->setMinimumSize(QSize(0, 30));
        SaveImage->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_2->addWidget(SaveImage);


        verticalLayout_2->addLayout(horizontalLayout_2);

        verticalLayout_2->setStretch(0, 1);

        horizontalLayout_4->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        TeachData = new QTableWidget(CameraUI);
        TeachData->setObjectName(QString::fromUtf8("TeachData"));
        TeachData->setMinimumSize(QSize(300, 0));
        TeachData->setMaximumSize(QSize(300, 16777215));

        verticalLayout->addWidget(TeachData);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        Teach_Add = new QPushButton(CameraUI);
        Teach_Add->setObjectName(QString::fromUtf8("Teach_Add"));
        Teach_Add->setMinimumSize(QSize(60, 30));
        Teach_Add->setMaximumSize(QSize(60, 30));

        horizontalLayout_3->addWidget(Teach_Add);

        Teach_Del = new QPushButton(CameraUI);
        Teach_Del->setObjectName(QString::fromUtf8("Teach_Del"));
        Teach_Del->setMinimumSize(QSize(60, 30));
        Teach_Del->setMaximumSize(QSize(60, 30));

        horizontalLayout_3->addWidget(Teach_Del);

        Teach_Get = new QPushButton(CameraUI);
        Teach_Get->setObjectName(QString::fromUtf8("Teach_Get"));
        Teach_Get->setMinimumSize(QSize(60, 30));
        Teach_Get->setMaximumSize(QSize(60, 30));

        horizontalLayout_3->addWidget(Teach_Get);

        Teach_Save = new QPushButton(CameraUI);
        Teach_Save->setObjectName(QString::fromUtf8("Teach_Save"));
        Teach_Save->setMinimumSize(QSize(60, 30));
        Teach_Save->setMaximumSize(QSize(60, 30));

        horizontalLayout_3->addWidget(Teach_Save);

        horizontalSpacer_4 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_3);


        horizontalLayout_4->addLayout(verticalLayout);

        horizontalLayout_4->setStretch(0, 1);

        retranslateUi(CameraUI);

        QMetaObject::connectSlotsByName(CameraUI);
    } // setupUi

    void retranslateUi(QWidget *CameraUI)
    {
        CameraUI->setWindowTitle(QApplication::translate("CameraUI", "Form", nullptr));
        IMShow->setText(QApplication::translate("CameraUI", "TextLabel", nullptr));
        label->setText(QApplication::translate("CameraUI", "Exposure", nullptr));
        label_2->setText(QApplication::translate("CameraUI", "Gamma", nullptr));
        label_3->setText(QApplication::translate("CameraUI", "Gain", nullptr));
        Open->setText(QApplication::translate("CameraUI", "Open", nullptr));
        Close->setText(QApplication::translate("CameraUI", "Close", nullptr));
        GetImage->setText(QApplication::translate("CameraUI", "Get Image", nullptr));
        SaveImage->setText(QApplication::translate("CameraUI", "Save Image", nullptr));
        Teach_Add->setText(QApplication::translate("CameraUI", "Add", nullptr));
        Teach_Del->setText(QApplication::translate("CameraUI", "Del", nullptr));
        Teach_Get->setText(QApplication::translate("CameraUI", "Get", nullptr));
        Teach_Save->setText(QApplication::translate("CameraUI", "Save", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CameraUI: public Ui_CameraUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERAUI_H
