/********************************************************************************
** Form generated from reading UI file 'SystemConfig.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYSTEMCONFIG_H
#define UI_SYSTEMCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SystemConfig
{
public:
    QHBoxLayout *horizontalLayout;
    QSplitter *splitter;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QComboBox *ResetComboBox;
    QLabel *label;
    QLabel *label_2;
    QComboBox *StartComboBox;
    QComboBox *Reset_IO_ComboBox;
    QLabel *label_3;
    QLabel *label_4;
    QComboBox *Start_IO_ComboBox;
    QComboBox *Pause_IO_ComboBox;
    QLabel *label_5;
    QCheckBox *Door_CheckBox;
    QCheckBox *checkBox_Edit__TableView;
    QWidget *widget_2;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QTableView *tableView;

    void setupUi(QWidget *SystemConfig)
    {
        if (SystemConfig->objectName().isEmpty())
            SystemConfig->setObjectName(QString::fromUtf8("SystemConfig"));
        SystemConfig->resize(1061, 709);
        horizontalLayout = new QHBoxLayout(SystemConfig);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        splitter = new QSplitter(SystemConfig);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        widget = new QWidget(splitter);
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(300, 600));
        ResetComboBox = new QComboBox(groupBox);
        ResetComboBox->setObjectName(QString::fromUtf8("ResetComboBox"));
        ResetComboBox->setGeometry(QRect(90, 30, 241, 21));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(22, 30, 60, 16));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(22, 60, 60, 16));
        StartComboBox = new QComboBox(groupBox);
        StartComboBox->setObjectName(QString::fromUtf8("StartComboBox"));
        StartComboBox->setGeometry(QRect(90, 60, 241, 21));
        Reset_IO_ComboBox = new QComboBox(groupBox);
        Reset_IO_ComboBox->setObjectName(QString::fromUtf8("Reset_IO_ComboBox"));
        Reset_IO_ComboBox->setGeometry(QRect(90, 90, 241, 21));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(22, 90, 60, 16));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(22, 120, 60, 16));
        Start_IO_ComboBox = new QComboBox(groupBox);
        Start_IO_ComboBox->setObjectName(QString::fromUtf8("Start_IO_ComboBox"));
        Start_IO_ComboBox->setGeometry(QRect(90, 120, 241, 21));
        Pause_IO_ComboBox = new QComboBox(groupBox);
        Pause_IO_ComboBox->setObjectName(QString::fromUtf8("Pause_IO_ComboBox"));
        Pause_IO_ComboBox->setGeometry(QRect(90, 150, 241, 21));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(22, 150, 60, 16));
        Door_CheckBox = new QCheckBox(groupBox);
        Door_CheckBox->setObjectName(QString::fromUtf8("Door_CheckBox"));
        Door_CheckBox->setGeometry(QRect(20, 200, 111, 31));
        checkBox_Edit__TableView = new QCheckBox(groupBox);
        checkBox_Edit__TableView->setObjectName(QString::fromUtf8("checkBox_Edit__TableView"));
        checkBox_Edit__TableView->setGeometry(QRect(20, 240, 111, 31));

        verticalLayout->addWidget(groupBox);

        splitter->addWidget(widget);
        widget_2 = new QWidget(splitter);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        gridLayout_3 = new QGridLayout(widget_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox_2 = new QGroupBox(widget_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tableView = new QTableView(groupBox_2);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setMinimumSize(QSize(300, 600));

        gridLayout_2->addWidget(tableView, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 0, 0, 1, 1);

        splitter->addWidget(widget_2);

        horizontalLayout->addWidget(splitter);


        retranslateUi(SystemConfig);

        QMetaObject::connectSlotsByName(SystemConfig);
    } // setupUi

    void retranslateUi(QWidget *SystemConfig)
    {
        SystemConfig->setWindowTitle(QApplication::translate("SystemConfig", "SystemConfig", nullptr));
        groupBox->setTitle(QApplication::translate("SystemConfig", "\347\263\273\347\273\237\351\205\215\347\275\256", nullptr));
        label->setText(QApplication::translate("SystemConfig", "\345\244\215\344\275\215\346\265\201\347\250\213", nullptr));
        label_2->setText(QApplication::translate("SystemConfig", "\345\220\257\345\212\250\346\265\201\347\250\213", nullptr));
        label_3->setText(QApplication::translate("SystemConfig", "\345\244\215\344\275\215IO", nullptr));
        label_4->setText(QApplication::translate("SystemConfig", "\345\220\257\345\212\250IO", nullptr));
        label_5->setText(QApplication::translate("SystemConfig", "\346\232\202\345\201\234IO", nullptr));
        Door_CheckBox->setText(QApplication::translate("SystemConfig", "\351\227\250\347\246\201", nullptr));
        checkBox_Edit__TableView->setText(QApplication::translate("SystemConfig", "\347\274\226\350\276\221flag", nullptr));
        groupBox_2->setTitle(QApplication::translate("SystemConfig", "\346\240\207\345\277\227\344\275\215", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SystemConfig: public Ui_SystemConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYSTEMCONFIG_H
