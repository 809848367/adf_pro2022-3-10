/********************************************************************************
** Form generated from reading UI file 'CommunicationDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMMUNICATIONDLG_H
#define UI_COMMUNICATIONDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CommunicationDlg
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *CenterWidget;

    void setupUi(QDialog *CommunicationDlg)
    {
        if (CommunicationDlg->objectName().isEmpty())
            CommunicationDlg->setObjectName(QString::fromUtf8("CommunicationDlg"));
        CommunicationDlg->resize(695, 710);
        verticalLayout = new QVBoxLayout(CommunicationDlg);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CenterWidget = new QWidget(CommunicationDlg);
        CenterWidget->setObjectName(QString::fromUtf8("CenterWidget"));

        verticalLayout->addWidget(CenterWidget);


        retranslateUi(CommunicationDlg);

        QMetaObject::connectSlotsByName(CommunicationDlg);
    } // setupUi

    void retranslateUi(QDialog *CommunicationDlg)
    {
        CommunicationDlg->setWindowTitle(QApplication::translate("CommunicationDlg", "CommunicationDlg", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CommunicationDlg: public Ui_CommunicationDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMMUNICATIONDLG_H
