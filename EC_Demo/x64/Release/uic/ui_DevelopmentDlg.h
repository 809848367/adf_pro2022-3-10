/********************************************************************************
** Form generated from reading UI file 'DevelopmentDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEVELOPMENTDLG_H
#define UI_DEVELOPMENTDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DevelopmentDlg
{
public:
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QDialog *DevelopmentDlg)
    {
        if (DevelopmentDlg->objectName().isEmpty())
            DevelopmentDlg->setObjectName(QString::fromUtf8("DevelopmentDlg"));
        DevelopmentDlg->resize(613, 656);
        verticalLayout = new QVBoxLayout(DevelopmentDlg);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        scrollArea = new QScrollArea(DevelopmentDlg);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 589, 632));
        verticalLayout_3 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(16);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label);

        plainTextEdit = new QPlainTextEdit(scrollAreaWidgetContents);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        verticalLayout_2->addWidget(plainTextEdit);


        verticalLayout_3->addLayout(verticalLayout_2);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);


        retranslateUi(DevelopmentDlg);

        QMetaObject::connectSlotsByName(DevelopmentDlg);
    } // setupUi

    void retranslateUi(QDialog *DevelopmentDlg)
    {
        DevelopmentDlg->setWindowTitle(QApplication::translate("DevelopmentDlg", "DevelopmentDlg", nullptr));
        label->setText(QApplication::translate("DevelopmentDlg", "\345\274\200\345\217\221\350\256\260\345\275\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DevelopmentDlg: public Ui_DevelopmentDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEVELOPMENTDLG_H
