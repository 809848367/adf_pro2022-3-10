/********************************************************************************
** Form generated from reading UI file 'CommunicationPage.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMMUNICATIONPAGE_H
#define UI_COMMUNICATIONPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CommunicationPage
{
public:
    QHBoxLayout *horizontalLayout;
    QSplitter *splitter;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QTreeView *treeView;
    QGroupBox *groupBox_2;
    QPushButton *refreshBtn;

    void setupUi(QWidget *CommunicationPage)
    {
        if (CommunicationPage->objectName().isEmpty())
            CommunicationPage->setObjectName(QString::fromUtf8("CommunicationPage"));
        CommunicationPage->resize(979, 650);
        horizontalLayout = new QHBoxLayout(CommunicationPage);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        splitter = new QSplitter(CommunicationPage);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        groupBox = new QGroupBox(splitter);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        treeView = new QTreeView(groupBox);
        treeView->setObjectName(QString::fromUtf8("treeView"));

        verticalLayout->addWidget(treeView);

        splitter->addWidget(groupBox);
        groupBox_2 = new QGroupBox(splitter);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        refreshBtn = new QPushButton(groupBox_2);
        refreshBtn->setObjectName(QString::fromUtf8("refreshBtn"));
        refreshBtn->setGeometry(QRect(20, 30, 93, 28));
        splitter->addWidget(groupBox_2);

        horizontalLayout->addWidget(splitter);


        retranslateUi(CommunicationPage);

        QMetaObject::connectSlotsByName(CommunicationPage);
    } // setupUi

    void retranslateUi(QWidget *CommunicationPage)
    {
        CommunicationPage->setWindowTitle(QApplication::translate("CommunicationPage", "CommunicationPage", nullptr));
        groupBox->setTitle(QApplication::translate("CommunicationPage", "\351\223\276\346\216\245", nullptr));
        groupBox_2->setTitle(QApplication::translate("CommunicationPage", "\346\223\215\344\275\234", nullptr));
        refreshBtn->setText(QApplication::translate("CommunicationPage", "\345\210\267\346\226\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CommunicationPage: public Ui_CommunicationPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMMUNICATIONPAGE_H
