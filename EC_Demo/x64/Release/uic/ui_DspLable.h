/********************************************************************************
** Form generated from reading UI file 'DspLable.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPLABLE_H
#define UI_DSPLABLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_DspLable
{
public:

    void setupUi(QLabel *DspLable)
    {
        if (DspLable->objectName().isEmpty())
            DspLable->setObjectName(QString::fromUtf8("DspLable"));
        DspLable->resize(400, 300);
        DspLable->setFrameShape(QFrame::Box);

        retranslateUi(DspLable);

        QMetaObject::connectSlotsByName(DspLable);
    } // setupUi

    void retranslateUi(QLabel *DspLable)
    {
        DspLable->setWindowTitle(QApplication::translate("DspLable", "DspLable", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DspLable: public Ui_DspLable {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPLABLE_H
