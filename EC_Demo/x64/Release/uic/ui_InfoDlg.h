/********************************************************************************
** Form generated from reading UI file 'InfoDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFODLG_H
#define UI_INFODLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_InfoDlg
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *CenterWidget;

    void setupUi(QDialog *InfoDlg)
    {
        if (InfoDlg->objectName().isEmpty())
            InfoDlg->setObjectName(QString::fromUtf8("InfoDlg"));
        InfoDlg->resize(694, 505);
        verticalLayout = new QVBoxLayout(InfoDlg);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CenterWidget = new QWidget(InfoDlg);
        CenterWidget->setObjectName(QString::fromUtf8("CenterWidget"));

        verticalLayout->addWidget(CenterWidget);


        retranslateUi(InfoDlg);

        QMetaObject::connectSlotsByName(InfoDlg);
    } // setupUi

    void retranslateUi(QDialog *InfoDlg)
    {
        InfoDlg->setWindowTitle(QApplication::translate("InfoDlg", "InfoDlg", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InfoDlg: public Ui_InfoDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFODLG_H
