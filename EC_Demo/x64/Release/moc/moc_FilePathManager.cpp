/****************************************************************************
** Meta object code from reading C++ file 'FilePathManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FilePathManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FilePathManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FilePathManager_t {
    QByteArrayData data[6];
    char stringdata0[60];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FilePathManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FilePathManager_t qt_meta_stringdata_FilePathManager = {
    {
QT_MOC_LITERAL(0, 0, 15), // "FilePathManager"
QT_MOC_LITERAL(1, 16, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 26, 7), // "NoError"
QT_MOC_LITERAL(3, 34, 4), // "Base"
QT_MOC_LITERAL(4, 39, 10), // "KeyNotFind"
QT_MOC_LITERAL(5, 50, 9) // "NoSection"

    },
    "FilePathManager\0ErrorCode\0NoError\0"
    "Base\0KeyNotFind\0NoSection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FilePathManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    4,   19,

 // enum data: key, value
       2, uint(FilePathManager::NoError),
       3, uint(FilePathManager::Base),
       4, uint(FilePathManager::KeyNotFind),
       5, uint(FilePathManager::NoSection),

       0        // eod
};

void FilePathManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject FilePathManager::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_FilePathManager.data,
    qt_meta_data_FilePathManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FilePathManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FilePathManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FilePathManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int FilePathManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
