/****************************************************************************
** Meta object code from reading C++ file 'SystemConfig.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../SystemConfig.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SystemConfig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SystemConfig_t {
    QByteArrayData data[17];
    char stringdata0[225];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SystemConfig_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SystemConfig_t qt_meta_stringdata_SystemConfig = {
    {
QT_MOC_LITERAL(0, 0, 12), // "SystemConfig"
QT_MOC_LITERAL(1, 13, 14), // "emit_startProc"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "procName"
QT_MOC_LITERAL(4, 38, 14), // "emit_resetProc"
QT_MOC_LITERAL(5, 53, 16), // "RefreshTimerSlot"
QT_MOC_LITERAL(6, 70, 11), // "itemChanged"
QT_MOC_LITERAL(7, 82, 14), // "QStandardItem*"
QT_MOC_LITERAL(8, 97, 4), // "item"
QT_MOC_LITERAL(9, 102, 23), // "Edit_Table_stateChanged"
QT_MOC_LITERAL(10, 126, 5), // "state"
QT_MOC_LITERAL(11, 132, 14), // "RefreshDspProc"
QT_MOC_LITERAL(12, 147, 14), // "QList<QString>"
QT_MOC_LITERAL(13, 162, 12), // "procNameList"
QT_MOC_LITERAL(14, 175, 20), // "Slot_ComboxStartProc"
QT_MOC_LITERAL(15, 196, 7), // "itemStr"
QT_MOC_LITERAL(16, 204, 20) // "Slot_ComboxResetProc"

    },
    "SystemConfig\0emit_startProc\0\0procName\0"
    "emit_resetProc\0RefreshTimerSlot\0"
    "itemChanged\0QStandardItem*\0item\0"
    "Edit_Table_stateChanged\0state\0"
    "RefreshDspProc\0QList<QString>\0"
    "procNameList\0Slot_ComboxStartProc\0"
    "itemStr\0Slot_ComboxResetProc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SystemConfig[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   60,    2, 0x0a /* Public */,
       6,    1,   61,    2, 0x0a /* Public */,
       9,    1,   64,    2, 0x0a /* Public */,
      11,    1,   67,    2, 0x0a /* Public */,
      14,    1,   70,    2, 0x0a /* Public */,
      16,    1,   73,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,

       0        // eod
};

void SystemConfig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SystemConfig *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->emit_startProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->emit_resetProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->RefreshTimerSlot(); break;
        case 3: _t->itemChanged((*reinterpret_cast< QStandardItem*(*)>(_a[1]))); break;
        case 4: _t->Edit_Table_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->RefreshDspProc((*reinterpret_cast< QList<QString>(*)>(_a[1]))); break;
        case 6: _t->Slot_ComboxStartProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->Slot_ComboxResetProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QString> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SystemConfig::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SystemConfig::emit_startProc)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (SystemConfig::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SystemConfig::emit_resetProc)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SystemConfig::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SystemConfig.data,
    qt_meta_data_SystemConfig,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SystemConfig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SystemConfig::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SystemConfig.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SystemConfig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void SystemConfig::emit_startProc(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SystemConfig::emit_resetProc(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
