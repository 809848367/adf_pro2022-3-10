/****************************************************************************
** Meta object code from reading C++ file 'CFileOperator.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../CFileOperator.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CFileOperator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CFileOperator_t {
    QByteArrayData data[9];
    char stringdata0[101];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CFileOperator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CFileOperator_t qt_meta_stringdata_CFileOperator = {
    {
QT_MOC_LITERAL(0, 0, 13), // "CFileOperator"
QT_MOC_LITERAL(1, 14, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 24, 7), // "NoError"
QT_MOC_LITERAL(3, 32, 4), // "Base"
QT_MOC_LITERAL(4, 37, 10), // "DirNotFind"
QT_MOC_LITERAL(5, 48, 11), // "FileNotFind"
QT_MOC_LITERAL(6, 60, 14), // "DeleteDirError"
QT_MOC_LITERAL(7, 75, 15), // "DeleteFileError"
QT_MOC_LITERAL(8, 91, 9) // "PathError"

    },
    "CFileOperator\0ErrorCode\0NoError\0Base\0"
    "DirNotFind\0FileNotFind\0DeleteDirError\0"
    "DeleteFileError\0PathError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CFileOperator[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    7,   19,

 // enum data: key, value
       2, uint(CFileOperator::NoError),
       3, uint(CFileOperator::Base),
       4, uint(CFileOperator::DirNotFind),
       5, uint(CFileOperator::FileNotFind),
       6, uint(CFileOperator::DeleteDirError),
       7, uint(CFileOperator::DeleteFileError),
       8, uint(CFileOperator::PathError),

       0        // eod
};

void CFileOperator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject CFileOperator::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_CFileOperator.data,
    qt_meta_data_CFileOperator,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CFileOperator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CFileOperator::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CFileOperator.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int CFileOperator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
