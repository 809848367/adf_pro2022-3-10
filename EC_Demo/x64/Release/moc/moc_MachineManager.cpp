/****************************************************************************
** Meta object code from reading C++ file 'MachineManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../MachineManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MachineManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MachineManager_t {
    QByteArrayData data[77];
    char stringdata0[1108];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MachineManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MachineManager_t qt_meta_stringdata_MachineManager = {
    {
QT_MOC_LITERAL(0, 0, 14), // "MachineManager"
QT_MOC_LITERAL(1, 15, 11), // "bAutoChange"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 5), // "bAuto"
QT_MOC_LITERAL(4, 34, 18), // "SignalStatus_Check"
QT_MOC_LITERAL(5, 53, 20), // "Signal_Machine_Statu"
QT_MOC_LITERAL(6, 74, 6), // "status"
QT_MOC_LITERAL(7, 81, 16), // "SignalMachineMsg"
QT_MOC_LITERAL(8, 98, 3), // "msg"
QT_MOC_LITERAL(9, 102, 4), // "type"
QT_MOC_LITERAL(10, 107, 21), // "Signal_Send_Proc_List"
QT_MOC_LITERAL(11, 129, 22), // "QList<CParentProcess*>"
QT_MOC_LITERAL(12, 152, 8), // "procList"
QT_MOC_LITERAL(13, 161, 7), // "SetAtuo"
QT_MOC_LITERAL(14, 169, 13), // "SetResetInput"
QT_MOC_LITERAL(15, 183, 5), // "input"
QT_MOC_LITERAL(16, 189, 13), // "SetStartInput"
QT_MOC_LITERAL(17, 203, 13), // "SetPauseInput"
QT_MOC_LITERAL(18, 217, 12), // "SetStopInput"
QT_MOC_LITERAL(19, 230, 16), // "SetMachineStatus"
QT_MOC_LITERAL(20, 247, 13), // "MachineStatus"
QT_MOC_LITERAL(21, 261, 6), // "Status"
QT_MOC_LITERAL(22, 268, 16), // "SetResetProcName"
QT_MOC_LITERAL(23, 285, 8), // "procName"
QT_MOC_LITERAL(24, 294, 16), // "SetStartProcName"
QT_MOC_LITERAL(25, 311, 15), // "StartProcByName"
QT_MOC_LITERAL(26, 327, 9), // "CtrlError"
QT_MOC_LITERAL(27, 337, 11), // "SetProcList"
QT_MOC_LITERAL(28, 349, 19), // "GetProcStatusByName"
QT_MOC_LITERAL(29, 369, 22), // "GetProcExceptionByName"
QT_MOC_LITERAL(30, 392, 12), // "MachineReset"
QT_MOC_LITERAL(31, 405, 11), // "MachineStop"
QT_MOC_LITERAL(32, 417, 16), // "MachineErrorStop"
QT_MOC_LITERAL(33, 434, 12), // "MachinePause"
QT_MOC_LITERAL(34, 447, 15), // "MachineContinue"
QT_MOC_LITERAL(35, 463, 12), // "MachineStart"
QT_MOC_LITERAL(36, 476, 12), // "MachineError"
QT_MOC_LITERAL(37, 489, 9), // "ErrorType"
QT_MOC_LITERAL(38, 499, 7), // "SetExit"
QT_MOC_LITERAL(39, 507, 8), // "exitFlag"
QT_MOC_LITERAL(40, 516, 7), // "GetExit"
QT_MOC_LITERAL(41, 524, 11), // "MachineExit"
QT_MOC_LITERAL(42, 536, 17), // "tRun_Check_Status"
QT_MOC_LITERAL(43, 554, 12), // "ProcessPause"
QT_MOC_LITERAL(44, 567, 11), // "ProcessStop"
QT_MOC_LITERAL(45, 579, 15), // "ProcessContinue"
QT_MOC_LITERAL(46, 595, 11), // "ClearCTData"
QT_MOC_LITERAL(47, 607, 13), // "AddOneProduct"
QT_MOC_LITERAL(48, 621, 15), // "AddOneOKProduct"
QT_MOC_LITERAL(49, 637, 15), // "AddOneNGProduct"
QT_MOC_LITERAL(50, 653, 15), // "RefrshCTProduct"
QT_MOC_LITERAL(51, 669, 5), // "ctStr"
QT_MOC_LITERAL(52, 675, 7), // "m_bAtuo"
QT_MOC_LITERAL(53, 683, 9), // "ErrorCode"
QT_MOC_LITERAL(54, 693, 7), // "NoError"
QT_MOC_LITERAL(55, 701, 4), // "Base"
QT_MOC_LITERAL(56, 706, 16), // "Error_Code_Start"
QT_MOC_LITERAL(57, 723, 9), // "EmptyProc"
QT_MOC_LITERAL(58, 733, 11), // "NotFindProc"
QT_MOC_LITERAL(59, 745, 13), // "PorcNotInIdle"
QT_MOC_LITERAL(60, 759, 13), // "CommondFailed"
QT_MOC_LITERAL(61, 773, 19), // "LoadProcessXMLError"
QT_MOC_LITERAL(62, 793, 17), // "ProcExceptionExit"
QT_MOC_LITERAL(63, 811, 19), // "CheckProcStateError"
QT_MOC_LITERAL(64, 831, 27), // "CurrentStateNotAllowExecute"
QT_MOC_LITERAL(65, 859, 10), // "NoProcWork"
QT_MOC_LITERAL(66, 870, 18), // "MachineStatus_Idle"
QT_MOC_LITERAL(67, 889, 19), // "MachineStatus_Reset"
QT_MOC_LITERAL(68, 909, 18), // "MachineStatus_Work"
QT_MOC_LITERAL(69, 928, 22), // "MachineStatus_Work_End"
QT_MOC_LITERAL(70, 951, 19), // "MachineStatus_Pause"
QT_MOC_LITERAL(71, 971, 22), // "MachineStatus_Continue"
QT_MOC_LITERAL(72, 994, 24), // "MachineStatus_Stop_Going"
QT_MOC_LITERAL(73, 1019, 18), // "MachineStatus_Stop"
QT_MOC_LITERAL(74, 1038, 25), // "MachineStatus_Error_Going"
QT_MOC_LITERAL(75, 1064, 19), // "MachineStatus_Error"
QT_MOC_LITERAL(76, 1084, 23) // "MachineStatus_WaitReset"

    },
    "MachineManager\0bAutoChange\0\0bAuto\0"
    "SignalStatus_Check\0Signal_Machine_Statu\0"
    "status\0SignalMachineMsg\0msg\0type\0"
    "Signal_Send_Proc_List\0QList<CParentProcess*>\0"
    "procList\0SetAtuo\0SetResetInput\0input\0"
    "SetStartInput\0SetPauseInput\0SetStopInput\0"
    "SetMachineStatus\0MachineStatus\0Status\0"
    "SetResetProcName\0procName\0SetStartProcName\0"
    "StartProcByName\0CtrlError\0SetProcList\0"
    "GetProcStatusByName\0GetProcExceptionByName\0"
    "MachineReset\0MachineStop\0MachineErrorStop\0"
    "MachinePause\0MachineContinue\0MachineStart\0"
    "MachineError\0ErrorType\0SetExit\0exitFlag\0"
    "GetExit\0MachineExit\0tRun_Check_Status\0"
    "ProcessPause\0ProcessStop\0ProcessContinue\0"
    "ClearCTData\0AddOneProduct\0AddOneOKProduct\0"
    "AddOneNGProduct\0RefrshCTProduct\0ctStr\0"
    "m_bAtuo\0ErrorCode\0NoError\0Base\0"
    "Error_Code_Start\0EmptyProc\0NotFindProc\0"
    "PorcNotInIdle\0CommondFailed\0"
    "LoadProcessXMLError\0ProcExceptionExit\0"
    "CheckProcStateError\0CurrentStateNotAllowExecute\0"
    "NoProcWork\0MachineStatus_Idle\0"
    "MachineStatus_Reset\0MachineStatus_Work\0"
    "MachineStatus_Work_End\0MachineStatus_Pause\0"
    "MachineStatus_Continue\0MachineStatus_Stop_Going\0"
    "MachineStatus_Stop\0MachineStatus_Error_Going\0"
    "MachineStatus_Error\0MachineStatus_WaitReset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MachineManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      36,   14, // methods
       1,  272, // properties
       2,  276, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  194,    2, 0x06 /* Public */,
       4,    0,  197,    2, 0x06 /* Public */,
       5,    1,  198,    2, 0x06 /* Public */,
       7,    2,  201,    2, 0x06 /* Public */,
      10,    1,  206,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    1,  209,    2, 0x0a /* Public */,
      14,    1,  212,    2, 0x0a /* Public */,
      16,    1,  215,    2, 0x0a /* Public */,
      17,    1,  218,    2, 0x0a /* Public */,
      18,    1,  221,    2, 0x0a /* Public */,
      19,    1,  224,    2, 0x0a /* Public */,
      22,    1,  227,    2, 0x0a /* Public */,
      24,    1,  230,    2, 0x0a /* Public */,
      25,    1,  233,    2, 0x0a /* Public */,
      27,    1,  236,    2, 0x0a /* Public */,
      28,    1,  239,    2, 0x0a /* Public */,
      29,    1,  242,    2, 0x0a /* Public */,
      30,    0,  245,    2, 0x0a /* Public */,
      31,    0,  246,    2, 0x0a /* Public */,
      32,    0,  247,    2, 0x0a /* Public */,
      33,    0,  248,    2, 0x0a /* Public */,
      34,    0,  249,    2, 0x0a /* Public */,
      35,    0,  250,    2, 0x0a /* Public */,
      36,    2,  251,    2, 0x0a /* Public */,
      38,    1,  256,    2, 0x0a /* Public */,
      40,    0,  259,    2, 0x0a /* Public */,
      41,    0,  260,    2, 0x0a /* Public */,
      42,    0,  261,    2, 0x0a /* Public */,
      43,    0,  262,    2, 0x0a /* Public */,
      44,    0,  263,    2, 0x0a /* Public */,
      45,    0,  264,    2, 0x0a /* Public */,
      46,    0,  265,    2, 0x0a /* Public */,
      47,    0,  266,    2, 0x0a /* Public */,
      48,    0,  267,    2, 0x0a /* Public */,
      49,    0,  268,    2, 0x0a /* Public */,
      50,    1,  269,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    8,    9,
    QMetaType::Void, 0x80000000 | 11,   12,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::QString,   23,
    0x80000000 | 26, QMetaType::QString,   23,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Int, QMetaType::QString,   23,
    QMetaType::Int, QMetaType::QString,   23,
    0x80000000 | 26,
    0x80000000 | 26,
    0x80000000 | 26,
    0x80000000 | 26,
    0x80000000 | 26,
    0x80000000 | 26,
    QMetaType::Int, QMetaType::QString, QMetaType::Int,    8,   37,
    QMetaType::Void, QMetaType::Bool,   39,
    QMetaType::Bool,
    QMetaType::Int,
    QMetaType::Void,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   51,

 // properties: name, type, flags
      52, QMetaType::Bool, 0x00495003,

 // properties: notify_signal_id
       0,

 // enums: name, alias, flags, count, data
      53,   53, 0x0,   12,  286,
      20,   20, 0x0,   11,  310,

 // enum data: key, value
      54, uint(MachineManager::NoError),
      55, uint(MachineManager::Base),
      56, uint(MachineManager::Error_Code_Start),
      57, uint(MachineManager::EmptyProc),
      58, uint(MachineManager::NotFindProc),
      59, uint(MachineManager::PorcNotInIdle),
      60, uint(MachineManager::CommondFailed),
      61, uint(MachineManager::LoadProcessXMLError),
      62, uint(MachineManager::ProcExceptionExit),
      63, uint(MachineManager::CheckProcStateError),
      64, uint(MachineManager::CurrentStateNotAllowExecute),
      65, uint(MachineManager::NoProcWork),
      66, uint(MachineManager::MachineStatus_Idle),
      67, uint(MachineManager::MachineStatus_Reset),
      68, uint(MachineManager::MachineStatus_Work),
      69, uint(MachineManager::MachineStatus_Work_End),
      70, uint(MachineManager::MachineStatus_Pause),
      71, uint(MachineManager::MachineStatus_Continue),
      72, uint(MachineManager::MachineStatus_Stop_Going),
      73, uint(MachineManager::MachineStatus_Stop),
      74, uint(MachineManager::MachineStatus_Error_Going),
      75, uint(MachineManager::MachineStatus_Error),
      76, uint(MachineManager::MachineStatus_WaitReset),

       0        // eod
};

void MachineManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MachineManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->bAutoChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->SignalStatus_Check(); break;
        case 2: _t->Signal_Machine_Statu((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->SignalMachineMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->Signal_Send_Proc_List((*reinterpret_cast< QList<CParentProcess*>(*)>(_a[1]))); break;
        case 5: _t->SetAtuo((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->SetResetInput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->SetStartInput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->SetPauseInput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->SetStopInput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->SetMachineStatus((*reinterpret_cast< MachineStatus(*)>(_a[1]))); break;
        case 11: _t->SetResetProcName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->SetStartProcName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: { CtrlError _r = _t->StartProcByName((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->SetProcList((*reinterpret_cast< QList<CParentProcess*>(*)>(_a[1]))); break;
        case 15: { int _r = _t->GetProcStatusByName((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 16: { int _r = _t->GetProcExceptionByName((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 17: { CtrlError _r = _t->MachineReset();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 18: { CtrlError _r = _t->MachineStop();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 19: { CtrlError _r = _t->MachineErrorStop();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 20: { CtrlError _r = _t->MachinePause();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 21: { CtrlError _r = _t->MachineContinue();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 22: { CtrlError _r = _t->MachineStart();
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 23: { int _r = _t->MachineError((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 24: _t->SetExit((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: { bool _r = _t->GetExit();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 26: { int _r = _t->MachineExit();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 27: _t->tRun_Check_Status(); break;
        case 28: { int _r = _t->ProcessPause();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 29: { int _r = _t->ProcessStop();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 30: { int _r = _t->ProcessContinue();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 31: _t->ClearCTData(); break;
        case 32: _t->AddOneProduct(); break;
        case 33: _t->AddOneOKProduct(); break;
        case 34: _t->AddOneNGProduct(); break;
        case 35: _t->RefrshCTProduct((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<CParentProcess*> >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<CParentProcess*> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MachineManager::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MachineManager::bAutoChange)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MachineManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MachineManager::SignalStatus_Check)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (MachineManager::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MachineManager::Signal_Machine_Statu)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (MachineManager::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MachineManager::SignalMachineMsg)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (MachineManager::*)(QList<CParentProcess*> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MachineManager::Signal_Send_Proc_List)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<MachineManager *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isAtuo(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<MachineManager *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->SetAtuo(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject MachineManager::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_MachineManager.data,
    qt_meta_data_MachineManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MachineManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MachineManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MachineManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int MachineManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void MachineManager::bAutoChange(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MachineManager::SignalStatus_Check()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void MachineManager::Signal_Machine_Statu(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MachineManager::SignalMachineMsg(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MachineManager::Signal_Send_Proc_List(QList<CParentProcess*> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
