/****************************************************************************
** Meta object code from reading C++ file 'CProcessWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../CProcessWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CProcessWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CProcessWidget_t {
    QByteArrayData data[60];
    char stringdata0[986];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CProcessWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CProcessWidget_t qt_meta_stringdata_CProcessWidget = {
    {
QT_MOC_LITERAL(0, 0, 14), // "CProcessWidget"
QT_MOC_LITERAL(1, 15, 11), // "SigOperator"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 13), // "OperatorInfo*"
QT_MOC_LITERAL(4, 42, 9), // "execOpera"
QT_MOC_LITERAL(5, 52, 7), // "SigStep"
QT_MOC_LITERAL(6, 60, 9), // "StepInfo*"
QT_MOC_LITERAL(7, 70, 4), // "step"
QT_MOC_LITERAL(8, 75, 19), // "Slot_CurrentRunStep"
QT_MOC_LITERAL(9, 95, 8), // "procName"
QT_MOC_LITERAL(10, 104, 14), // "LoadProcessBtn"
QT_MOC_LITERAL(11, 119, 17), // "RefreshProcessBtn"
QT_MOC_LITERAL(12, 137, 9), // "SaveAsBtn"
QT_MOC_LITERAL(13, 147, 9), // "ResortBtn"
QT_MOC_LITERAL(14, 157, 7), // "SaveBtn"
QT_MOC_LITERAL(15, 165, 24), // "selectionChangedTreeSlot"
QT_MOC_LITERAL(16, 190, 14), // "QItemSelection"
QT_MOC_LITERAL(17, 205, 8), // "selected"
QT_MOC_LITERAL(18, 214, 10), // "deselected"
QT_MOC_LITERAL(19, 225, 25), // "selectionChangedTableSlot"
QT_MOC_LITERAL(20, 251, 24), // "tableviewContextMenuSlot"
QT_MOC_LITERAL(21, 276, 3), // "pos"
QT_MOC_LITERAL(22, 280, 23), // "treeviewContextMenuSlot"
QT_MOC_LITERAL(23, 304, 9), // "ResetProc"
QT_MOC_LITERAL(24, 314, 13), // "AddOneRunProc"
QT_MOC_LITERAL(25, 328, 16), // "DeleteOneRunProc"
QT_MOC_LITERAL(26, 345, 17), // "on_actTreeRunSlot"
QT_MOC_LITERAL(27, 363, 17), // "on_actTreeAddSlot"
QT_MOC_LITERAL(28, 381, 20), // "on_actTreeInsertSlot"
QT_MOC_LITERAL(29, 402, 20), // "on_actTreeDeleteSlot"
QT_MOC_LITERAL(30, 423, 21), // "on_actTreeInvalidSlot"
QT_MOC_LITERAL(31, 445, 20), // "on_actTreeEnableSlot"
QT_MOC_LITERAL(32, 466, 18), // "on_actTreeCopySlot"
QT_MOC_LITERAL(33, 485, 19), // "on_actTreePasteSlot"
QT_MOC_LITERAL(34, 505, 16), // "on_actTabRunSlot"
QT_MOC_LITERAL(35, 522, 16), // "on_actTabAddSlot"
QT_MOC_LITERAL(36, 539, 19), // "on_actTabInsertSlot"
QT_MOC_LITERAL(37, 559, 19), // "on_actTabDeleteSlot"
QT_MOC_LITERAL(38, 579, 20), // "on_actTabInvaildSlot"
QT_MOC_LITERAL(39, 600, 19), // "on_actTabEnableSlot"
QT_MOC_LITERAL(40, 620, 17), // "on_actTabCopySlot"
QT_MOC_LITERAL(41, 638, 18), // "on_actTabPasteSlot"
QT_MOC_LITERAL(42, 657, 17), // "on_actStepRunSlot"
QT_MOC_LITERAL(43, 675, 17), // "on_actStepAddSlot"
QT_MOC_LITERAL(44, 693, 20), // "on_actStepInsertSlot"
QT_MOC_LITERAL(45, 714, 20), // "on_actStepDeleteSlot"
QT_MOC_LITERAL(46, 735, 21), // "on_actStepInvalidSlot"
QT_MOC_LITERAL(47, 757, 20), // "on_actStepEnableSlot"
QT_MOC_LITERAL(48, 778, 18), // "on_actStepCopySlot"
QT_MOC_LITERAL(49, 797, 19), // "on_actStepPasteSlot"
QT_MOC_LITERAL(50, 817, 21), // "on_ensure_change_slot"
QT_MOC_LITERAL(51, 839, 14), // "on_cancle_slot"
QT_MOC_LITERAL(52, 854, 20), // "on_EnsureStepBtnSlot"
QT_MOC_LITERAL(53, 875, 18), // "ChooseIndexChanged"
QT_MOC_LITERAL(54, 894, 7), // "content"
QT_MOC_LITERAL(55, 902, 23), // "OperatorTypeTextChanged"
QT_MOC_LITERAL(56, 926, 6), // "curStr"
QT_MOC_LITERAL(57, 933, 18), // "AbleOperatorWidget"
QT_MOC_LITERAL(58, 952, 21), // "DisableOperatorWidget"
QT_MOC_LITERAL(59, 974, 11) // "ExitRunProc"

    },
    "CProcessWidget\0SigOperator\0\0OperatorInfo*\0"
    "execOpera\0SigStep\0StepInfo*\0step\0"
    "Slot_CurrentRunStep\0procName\0"
    "LoadProcessBtn\0RefreshProcessBtn\0"
    "SaveAsBtn\0ResortBtn\0SaveBtn\0"
    "selectionChangedTreeSlot\0QItemSelection\0"
    "selected\0deselected\0selectionChangedTableSlot\0"
    "tableviewContextMenuSlot\0pos\0"
    "treeviewContextMenuSlot\0ResetProc\0"
    "AddOneRunProc\0DeleteOneRunProc\0"
    "on_actTreeRunSlot\0on_actTreeAddSlot\0"
    "on_actTreeInsertSlot\0on_actTreeDeleteSlot\0"
    "on_actTreeInvalidSlot\0on_actTreeEnableSlot\0"
    "on_actTreeCopySlot\0on_actTreePasteSlot\0"
    "on_actTabRunSlot\0on_actTabAddSlot\0"
    "on_actTabInsertSlot\0on_actTabDeleteSlot\0"
    "on_actTabInvaildSlot\0on_actTabEnableSlot\0"
    "on_actTabCopySlot\0on_actTabPasteSlot\0"
    "on_actStepRunSlot\0on_actStepAddSlot\0"
    "on_actStepInsertSlot\0on_actStepDeleteSlot\0"
    "on_actStepInvalidSlot\0on_actStepEnableSlot\0"
    "on_actStepCopySlot\0on_actStepPasteSlot\0"
    "on_ensure_change_slot\0on_cancle_slot\0"
    "on_EnsureStepBtnSlot\0ChooseIndexChanged\0"
    "content\0OperatorTypeTextChanged\0curStr\0"
    "AbleOperatorWidget\0DisableOperatorWidget\0"
    "ExitRunProc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CProcessWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      47,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  249,    2, 0x06 /* Public */,
       5,    1,  252,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    2,  255,    2, 0x0a /* Public */,
      10,    0,  260,    2, 0x0a /* Public */,
      11,    0,  261,    2, 0x0a /* Public */,
      12,    0,  262,    2, 0x0a /* Public */,
      13,    0,  263,    2, 0x0a /* Public */,
      14,    0,  264,    2, 0x0a /* Public */,
      15,    2,  265,    2, 0x0a /* Public */,
      19,    2,  270,    2, 0x0a /* Public */,
      20,    1,  275,    2, 0x0a /* Public */,
      22,    1,  278,    2, 0x0a /* Public */,
      23,    0,  281,    2, 0x0a /* Public */,
      24,    1,  282,    2, 0x0a /* Public */,
      25,    1,  285,    2, 0x0a /* Public */,
      26,    0,  288,    2, 0x0a /* Public */,
      27,    0,  289,    2, 0x0a /* Public */,
      28,    0,  290,    2, 0x0a /* Public */,
      29,    0,  291,    2, 0x0a /* Public */,
      30,    0,  292,    2, 0x0a /* Public */,
      31,    0,  293,    2, 0x0a /* Public */,
      32,    0,  294,    2, 0x0a /* Public */,
      33,    0,  295,    2, 0x0a /* Public */,
      34,    0,  296,    2, 0x0a /* Public */,
      35,    0,  297,    2, 0x0a /* Public */,
      36,    0,  298,    2, 0x0a /* Public */,
      37,    0,  299,    2, 0x0a /* Public */,
      38,    0,  300,    2, 0x0a /* Public */,
      39,    0,  301,    2, 0x0a /* Public */,
      40,    0,  302,    2, 0x0a /* Public */,
      41,    0,  303,    2, 0x0a /* Public */,
      42,    0,  304,    2, 0x0a /* Public */,
      43,    0,  305,    2, 0x0a /* Public */,
      44,    0,  306,    2, 0x0a /* Public */,
      45,    0,  307,    2, 0x0a /* Public */,
      46,    0,  308,    2, 0x0a /* Public */,
      47,    0,  309,    2, 0x0a /* Public */,
      48,    0,  310,    2, 0x0a /* Public */,
      49,    0,  311,    2, 0x0a /* Public */,
      50,    0,  312,    2, 0x0a /* Public */,
      51,    0,  313,    2, 0x0a /* Public */,
      52,    0,  314,    2, 0x0a /* Public */,
      53,    1,  315,    2, 0x0a /* Public */,
      55,    1,  318,    2, 0x0a /* Public */,
      57,    0,  321,    2, 0x0a /* Public */,
      58,    0,  322,    2, 0x0a /* Public */,
      59,    0,  323,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    9,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 16, 0x80000000 | 16,   17,   18,
    QMetaType::Void, 0x80000000 | 16, 0x80000000 | 16,   17,   18,
    QMetaType::Void, QMetaType::QPoint,   21,
    QMetaType::Void, QMetaType::QPoint,   21,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   54,
    QMetaType::Void, QMetaType::QString,   56,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CProcessWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CProcessWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SigOperator((*reinterpret_cast< OperatorInfo*(*)>(_a[1]))); break;
        case 1: _t->SigStep((*reinterpret_cast< StepInfo*(*)>(_a[1]))); break;
        case 2: _t->Slot_CurrentRunStep((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->LoadProcessBtn(); break;
        case 4: _t->RefreshProcessBtn(); break;
        case 5: _t->SaveAsBtn(); break;
        case 6: _t->ResortBtn(); break;
        case 7: _t->SaveBtn(); break;
        case 8: _t->selectionChangedTreeSlot((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 9: _t->selectionChangedTableSlot((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 10: _t->tableviewContextMenuSlot((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 11: _t->treeviewContextMenuSlot((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 12: _t->ResetProc(); break;
        case 13: _t->AddOneRunProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: _t->DeleteOneRunProc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: _t->on_actTreeRunSlot(); break;
        case 16: _t->on_actTreeAddSlot(); break;
        case 17: _t->on_actTreeInsertSlot(); break;
        case 18: _t->on_actTreeDeleteSlot(); break;
        case 19: _t->on_actTreeInvalidSlot(); break;
        case 20: _t->on_actTreeEnableSlot(); break;
        case 21: _t->on_actTreeCopySlot(); break;
        case 22: _t->on_actTreePasteSlot(); break;
        case 23: _t->on_actTabRunSlot(); break;
        case 24: _t->on_actTabAddSlot(); break;
        case 25: _t->on_actTabInsertSlot(); break;
        case 26: _t->on_actTabDeleteSlot(); break;
        case 27: _t->on_actTabInvaildSlot(); break;
        case 28: _t->on_actTabEnableSlot(); break;
        case 29: _t->on_actTabCopySlot(); break;
        case 30: _t->on_actTabPasteSlot(); break;
        case 31: _t->on_actStepRunSlot(); break;
        case 32: _t->on_actStepAddSlot(); break;
        case 33: _t->on_actStepInsertSlot(); break;
        case 34: _t->on_actStepDeleteSlot(); break;
        case 35: _t->on_actStepInvalidSlot(); break;
        case 36: _t->on_actStepEnableSlot(); break;
        case 37: _t->on_actStepCopySlot(); break;
        case 38: _t->on_actStepPasteSlot(); break;
        case 39: _t->on_ensure_change_slot(); break;
        case 40: _t->on_cancle_slot(); break;
        case 41: _t->on_EnsureStepBtnSlot(); break;
        case 42: _t->ChooseIndexChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 43: _t->OperatorTypeTextChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 44: _t->AbleOperatorWidget(); break;
        case 45: _t->DisableOperatorWidget(); break;
        case 46: _t->ExitRunProc(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelection >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelection >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CProcessWidget::*)(OperatorInfo * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CProcessWidget::SigOperator)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CProcessWidget::*)(StepInfo * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CProcessWidget::SigStep)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CProcessWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_CProcessWidget.data,
    qt_meta_data_CProcessWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CProcessWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CProcessWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CProcessWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int CProcessWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 47)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 47;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 47)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 47;
    }
    return _id;
}

// SIGNAL 0
void CProcessWidget::SigOperator(OperatorInfo * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CProcessWidget::SigStep(StepInfo * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
