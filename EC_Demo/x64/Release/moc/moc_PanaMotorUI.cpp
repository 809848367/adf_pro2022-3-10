/****************************************************************************
** Meta object code from reading C++ file 'PanaMotorUI.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../PanaMotorUI.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PanaMotorUI.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PanaMotorUI_t {
    QByteArrayData data[23];
    char stringdata0[349];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PanaMotorUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PanaMotorUI_t qt_meta_stringdata_PanaMotorUI = {
    {
QT_MOC_LITERAL(0, 0, 11), // "PanaMotorUI"
QT_MOC_LITERAL(1, 12, 15), // "UpDateTeachData"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 7), // "Station"
QT_MOC_LITERAL(4, 37, 19), // "TeachData_PanaMotor"
QT_MOC_LITERAL(5, 57, 9), // "TeachData"
QT_MOC_LITERAL(6, 67, 16), // "RefreshTimerSlot"
QT_MOC_LITERAL(7, 84, 17), // "on_Enable_clicked"
QT_MOC_LITERAL(8, 102, 18), // "on_Disable_clicked"
QT_MOC_LITERAL(9, 121, 18), // "on_MovePos_clicked"
QT_MOC_LITERAL(10, 140, 16), // "on_Reset_clicked"
QT_MOC_LITERAL(11, 157, 15), // "on_Stop_clicked"
QT_MOC_LITERAL(12, 173, 18), // "on_Pos_Del_clicked"
QT_MOC_LITERAL(13, 192, 18), // "on_Pos_Add_clicked"
QT_MOC_LITERAL(14, 211, 16), // "RefreshTeachData"
QT_MOC_LITERAL(15, 228, 21), // "TeachDataBtns_clicked"
QT_MOC_LITERAL(16, 250, 1), // "n"
QT_MOC_LITERAL(17, 252, 19), // "on_TeachAdd_clicked"
QT_MOC_LITERAL(18, 272, 19), // "on_TeachDel_clicked"
QT_MOC_LITERAL(19, 292, 20), // "on_TeachMove_clicked"
QT_MOC_LITERAL(20, 313, 24), // "on_TeachData_cellChanged"
QT_MOC_LITERAL(21, 338, 3), // "row"
QT_MOC_LITERAL(22, 342, 6) // "column"

    },
    "PanaMotorUI\0UpDateTeachData\0\0Station\0"
    "TeachData_PanaMotor\0TeachData\0"
    "RefreshTimerSlot\0on_Enable_clicked\0"
    "on_Disable_clicked\0on_MovePos_clicked\0"
    "on_Reset_clicked\0on_Stop_clicked\0"
    "on_Pos_Del_clicked\0on_Pos_Add_clicked\0"
    "RefreshTeachData\0TeachDataBtns_clicked\0"
    "n\0on_TeachAdd_clicked\0on_TeachDel_clicked\0"
    "on_TeachMove_clicked\0on_TeachData_cellChanged\0"
    "row\0column"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PanaMotorUI[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   89,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   94,    2, 0x08 /* Private */,
       7,    0,   95,    2, 0x08 /* Private */,
       8,    0,   96,    2, 0x08 /* Private */,
       9,    0,   97,    2, 0x08 /* Private */,
      10,    0,   98,    2, 0x08 /* Private */,
      11,    0,   99,    2, 0x08 /* Private */,
      12,    0,  100,    2, 0x08 /* Private */,
      13,    0,  101,    2, 0x08 /* Private */,
      14,    2,  102,    2, 0x08 /* Private */,
      15,    1,  107,    2, 0x08 /* Private */,
      17,    0,  110,    2, 0x08 /* Private */,
      18,    0,  111,    2, 0x08 /* Private */,
      19,    0,  112,    2, 0x08 /* Private */,
      20,    2,  113,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4,    3,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4,    3,    5,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   21,   22,

       0        // eod
};

void PanaMotorUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PanaMotorUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->UpDateTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_PanaMotor(*)>(_a[2]))); break;
        case 1: _t->RefreshTimerSlot(); break;
        case 2: _t->on_Enable_clicked(); break;
        case 3: _t->on_Disable_clicked(); break;
        case 4: _t->on_MovePos_clicked(); break;
        case 5: _t->on_Reset_clicked(); break;
        case 6: _t->on_Stop_clicked(); break;
        case 7: _t->on_Pos_Del_clicked(); break;
        case 8: _t->on_Pos_Add_clicked(); break;
        case 9: _t->RefreshTeachData((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< TeachData_PanaMotor(*)>(_a[2]))); break;
        case 10: _t->TeachDataBtns_clicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_TeachAdd_clicked(); break;
        case 12: _t->on_TeachDel_clicked(); break;
        case 13: _t->on_TeachMove_clicked(); break;
        case 14: _t->on_TeachData_cellChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PanaMotorUI::*)(QString , TeachData_PanaMotor );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PanaMotorUI::UpDateTeachData)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PanaMotorUI::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PanaMotorUI.data,
    qt_meta_data_PanaMotorUI,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PanaMotorUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PanaMotorUI::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PanaMotorUI.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PanaMotorUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void PanaMotorUI::UpDateTeachData(QString _t1, TeachData_PanaMotor _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
