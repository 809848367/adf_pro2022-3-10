/****************************************************************************
** Meta object code from reading C++ file 'AlgoCommData.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../AlgoCommData.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AlgoCommData.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AlgoCommData_t {
    QByteArrayData data[5];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AlgoCommData_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AlgoCommData_t qt_meta_stringdata_AlgoCommData = {
    {
QT_MOC_LITERAL(0, 0, 12), // "AlgoCommData"
QT_MOC_LITERAL(1, 13, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 23, 7), // "NoError"
QT_MOC_LITERAL(3, 31, 4), // "Base"
QT_MOC_LITERAL(4, 36, 17) // "CommandTransError"

    },
    "AlgoCommData\0ErrorCode\0NoError\0Base\0"
    "CommandTransError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AlgoCommData[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    3,   19,

 // enum data: key, value
       2, uint(AlgoCommData::NoError),
       3, uint(AlgoCommData::Base),
       4, uint(AlgoCommData::CommandTransError),

       0        // eod
};

QT_INIT_METAOBJECT const QMetaObject AlgoCommData::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_AlgoCommData.data,
    qt_meta_data_AlgoCommData,
    nullptr,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
