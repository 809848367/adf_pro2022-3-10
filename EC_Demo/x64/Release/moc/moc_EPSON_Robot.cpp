/****************************************************************************
** Meta object code from reading C++ file 'EPSON_Robot.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../EPSON_Robot.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'EPSON_Robot.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_EPSON_Robot_t {
    QByteArrayData data[19];
    char stringdata0[286];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_EPSON_Robot_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_EPSON_Robot_t qt_meta_stringdata_EPSON_Robot = {
    {
QT_MOC_LITERAL(0, 0, 11), // "EPSON_Robot"
QT_MOC_LITERAL(1, 12, 9), // "ErrorCode"
QT_MOC_LITERAL(2, 22, 7), // "NoError"
QT_MOC_LITERAL(3, 30, 4), // "Base"
QT_MOC_LITERAL(4, 35, 20), // "ConnectionEPSONError"
QT_MOC_LITERAL(5, 56, 16), // "SendCmdDataError"
QT_MOC_LITERAL(6, 73, 8), // "ResError"
QT_MOC_LITERAL(7, 82, 10), // "ResTimeOut"
QT_MOC_LITERAL(8, 93, 23), // "ModuleParaKeyNotContain"
QT_MOC_LITERAL(9, 117, 18), // "ModuleParaCmdError"
QT_MOC_LITERAL(10, 136, 24), // "ModuleParaFuncNotContain"
QT_MOC_LITERAL(11, 161, 12), // "ExecCmdError"
QT_MOC_LITERAL(12, 174, 11), // "InitDataErr"
QT_MOC_LITERAL(13, 186, 19), // "ExecuteDataNotRight"
QT_MOC_LITERAL(14, 206, 13), // "ClientHasOpen"
QT_MOC_LITERAL(15, 220, 14), // "ClientOpenFail"
QT_MOC_LITERAL(16, 235, 16), // "ClientWriteError"
QT_MOC_LITERAL(17, 252, 17), // "ClientReadTimeOut"
QT_MOC_LITERAL(18, 270, 15) // "ModuleNameError"

    },
    "EPSON_Robot\0ErrorCode\0NoError\0Base\0"
    "ConnectionEPSONError\0SendCmdDataError\0"
    "ResError\0ResTimeOut\0ModuleParaKeyNotContain\0"
    "ModuleParaCmdError\0ModuleParaFuncNotContain\0"
    "ExecCmdError\0InitDataErr\0ExecuteDataNotRight\0"
    "ClientHasOpen\0ClientOpenFail\0"
    "ClientWriteError\0ClientReadTimeOut\0"
    "ModuleNameError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_EPSON_Robot[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   17,   19,

 // enum data: key, value
       2, uint(EPSON_Robot::NoError),
       3, uint(EPSON_Robot::Base),
       4, uint(EPSON_Robot::ConnectionEPSONError),
       5, uint(EPSON_Robot::SendCmdDataError),
       6, uint(EPSON_Robot::ResError),
       7, uint(EPSON_Robot::ResTimeOut),
       8, uint(EPSON_Robot::ModuleParaKeyNotContain),
       9, uint(EPSON_Robot::ModuleParaCmdError),
      10, uint(EPSON_Robot::ModuleParaFuncNotContain),
      11, uint(EPSON_Robot::ExecCmdError),
      12, uint(EPSON_Robot::InitDataErr),
      13, uint(EPSON_Robot::ExecuteDataNotRight),
      14, uint(EPSON_Robot::ClientHasOpen),
      15, uint(EPSON_Robot::ClientOpenFail),
      16, uint(EPSON_Robot::ClientWriteError),
      17, uint(EPSON_Robot::ClientReadTimeOut),
      18, uint(EPSON_Robot::ModuleNameError),

       0        // eod
};

void EPSON_Robot::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject EPSON_Robot::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_EPSON_Robot.data,
    qt_meta_data_EPSON_Robot,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *EPSON_Robot::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *EPSON_Robot::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_EPSON_Robot.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int EPSON_Robot::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
