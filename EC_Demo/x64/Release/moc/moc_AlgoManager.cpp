/****************************************************************************
** Meta object code from reading C++ file 'AlgoManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../AlgoManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AlgoManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AlgoManager_t {
    QByteArrayData data[24];
    char stringdata0[294];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AlgoManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AlgoManager_t qt_meta_stringdata_AlgoManager = {
    {
QT_MOC_LITERAL(0, 0, 11), // "AlgoManager"
QT_MOC_LITERAL(1, 12, 11), // "SetDataInDB"
QT_MOC_LITERAL(2, 24, 9), // "CtrlError"
QT_MOC_LITERAL(3, 34, 0), // ""
QT_MOC_LITERAL(4, 35, 2), // "SN"
QT_MOC_LITERAL(5, 38, 26), // "QMap<QString,AlgoCommData>"
QT_MOC_LITERAL(6, 65, 5), // "Datas"
QT_MOC_LITERAL(7, 71, 21), // "QMap<QString,QString>"
QT_MOC_LITERAL(8, 93, 9), // "FilePaths"
QT_MOC_LITERAL(9, 103, 10), // "RecvResult"
QT_MOC_LITERAL(10, 114, 4), // "Name"
QT_MOC_LITERAL(11, 119, 9), // "ResultStr"
QT_MOC_LITERAL(12, 129, 9), // "ErrorCode"
QT_MOC_LITERAL(13, 139, 7), // "NoError"
QT_MOC_LITERAL(14, 147, 4), // "Base"
QT_MOC_LITERAL(15, 152, 15), // "StationNotExist"
QT_MOC_LITERAL(16, 168, 11), // "SNNotFinish"
QT_MOC_LITERAL(17, 180, 10), // "SNNotExist"
QT_MOC_LITERAL(18, 191, 19), // "ExecuteDataNotRight"
QT_MOC_LITERAL(19, 211, 11), // "InitDataErr"
QT_MOC_LITERAL(20, 223, 14), // "ResultNotExist"
QT_MOC_LITERAL(21, 238, 17), // "WaitResultTimeout"
QT_MOC_LITERAL(22, 256, 17), // "WaitFinishTimeout"
QT_MOC_LITERAL(23, 274, 19) // "IMSMulityMatchError"

    },
    "AlgoManager\0SetDataInDB\0CtrlError\0\0"
    "SN\0QMap<QString,AlgoCommData>\0Datas\0"
    "QMap<QString,QString>\0FilePaths\0"
    "RecvResult\0Name\0ResultStr\0ErrorCode\0"
    "NoError\0Base\0StationNotExist\0SNNotFinish\0"
    "SNNotExist\0ExecuteDataNotRight\0"
    "InitDataErr\0ResultNotExist\0WaitResultTimeout\0"
    "WaitFinishTimeout\0IMSMulityMatchError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AlgoManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       1,   36, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   24,    3, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    2,   31,    3, 0x08 /* Private */,

 // signals: parameters
    0x80000000 | 2, QMetaType::QString, 0x80000000 | 5, 0x80000000 | 7,    4,    6,    8,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   10,   11,

 // enums: name, alias, flags, count, data
      12,   12, 0x0,   11,   41,

 // enum data: key, value
      13, uint(AlgoManager::NoError),
      14, uint(AlgoManager::Base),
      15, uint(AlgoManager::StationNotExist),
      16, uint(AlgoManager::SNNotFinish),
      17, uint(AlgoManager::SNNotExist),
      18, uint(AlgoManager::ExecuteDataNotRight),
      19, uint(AlgoManager::InitDataErr),
      20, uint(AlgoManager::ResultNotExist),
      21, uint(AlgoManager::WaitResultTimeout),
      22, uint(AlgoManager::WaitFinishTimeout),
      23, uint(AlgoManager::IMSMulityMatchError),

       0        // eod
};

void AlgoManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<AlgoManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { CtrlError _r = _t->SetDataInDB((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QMap<QString,AlgoCommData>(*)>(_a[2])),(*reinterpret_cast< QMap<QString,QString>(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< CtrlError*>(_a[0]) = std::move(_r); }  break;
        case 1: _t->RecvResult((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = CtrlError (AlgoManager::*)(QString , QMap<QString,AlgoCommData> , QMap<QString,QString> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AlgoManager::SetDataInDB)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AlgoManager::staticMetaObject = { {
    &HardwareModule::staticMetaObject,
    qt_meta_stringdata_AlgoManager.data,
    qt_meta_data_AlgoManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *AlgoManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AlgoManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AlgoManager.stringdata0))
        return static_cast<void*>(this);
    return HardwareModule::qt_metacast(_clname);
}

int AlgoManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HardwareModule::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
CtrlError AlgoManager::SetDataInDB(QString _t1, QMap<QString,AlgoCommData> _t2, QMap<QString,QString> _t3)
{
    CtrlError _t0{};
    void *_a[] = { const_cast<void*>(reinterpret_cast<const void*>(&_t0)), const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
    return _t0;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
