/****************************************************************************
** Meta object code from reading C++ file 'AlgoClient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../AlgoClient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AlgoClient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AlgoClient_t {
    QByteArrayData data[14];
    char stringdata0[147];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AlgoClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AlgoClient_t qt_meta_stringdata_AlgoClient = {
    {
QT_MOC_LITERAL(0, 0, 10), // "AlgoClient"
QT_MOC_LITERAL(1, 11, 10), // "RecvResult"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 4), // "Name"
QT_MOC_LITERAL(4, 28, 4), // "Data"
QT_MOC_LITERAL(5, 33, 13), // "HeartBeatSlot"
QT_MOC_LITERAL(6, 47, 9), // "ErrorCode"
QT_MOC_LITERAL(7, 57, 7), // "NoError"
QT_MOC_LITERAL(8, 65, 4), // "Base"
QT_MOC_LITERAL(9, 70, 13), // "ClientHasOpen"
QT_MOC_LITERAL(10, 84, 14), // "ClientOpenFail"
QT_MOC_LITERAL(11, 99, 16), // "ClientSetParaErr"
QT_MOC_LITERAL(12, 116, 13), // "ClientNotOpen"
QT_MOC_LITERAL(13, 130, 16) // "RecvDataNotExist"

    },
    "AlgoClient\0RecvResult\0\0Name\0Data\0"
    "HeartBeatSlot\0ErrorCode\0NoError\0Base\0"
    "ClientHasOpen\0ClientOpenFail\0"
    "ClientSetParaErr\0ClientNotOpen\0"
    "RecvDataNotExist"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AlgoClient[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       1,   36, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       5,    0,   35,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // enums: name, alias, flags, count, data
       6,    6, 0x0,    7,   41,

 // enum data: key, value
       7, uint(AlgoClient::NoError),
       8, uint(AlgoClient::Base),
       9, uint(AlgoClient::ClientHasOpen),
      10, uint(AlgoClient::ClientOpenFail),
      11, uint(AlgoClient::ClientSetParaErr),
      12, uint(AlgoClient::ClientNotOpen),
      13, uint(AlgoClient::RecvDataNotExist),

       0        // eod
};

void AlgoClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<AlgoClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->RecvResult((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->RecvResult(); break;
        case 2: _t->HeartBeatSlot(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (AlgoClient::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AlgoClient::RecvResult)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AlgoClient::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_AlgoClient.data,
    qt_meta_data_AlgoClient,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *AlgoClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AlgoClient::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AlgoClient.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int AlgoClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void AlgoClient::RecvResult(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
