﻿#ifndef SERIALPORT_H
#define SERIALPORT_H
#include <QtCore>
#include <QSerialPort>
#include <QMutex>
#include <atomic>
#include "LogManager.h"
#include "CtrlError.h"

class SerialPort : public QObject
{
    Q_OBJECT
public:
    struct SerialPortConfig
    {
        QString COM;
        int BaudRate = QSerialPort::Baud115200;
        int DataBits = QSerialPort::Data8;
        int FlowControl = QSerialPort::NoFlowControl;
        int Parity = QSerialPort::NoParity;
        int StopBits = QSerialPort::OneStop;
        QString Type;
    };

    explicit SerialPort(QObject *parent = nullptr);
    ~SerialPort();
    CtrlError Open(SerialPortConfig Config);
    CtrlError Stop();
    CtrlError SendMsg(QByteArray Msg, QString& ID, int Priority = 1);
private:
    void thread_Send();
    std::atomic<int> threadFlag;                //0未运行，1运行中，2准备停止，3准备关闭线程，4已关闭线程（关闭线程后不可开启）
    QSerialPort *Serial;
    QList<QPair<int,QByteArray>> SendList1;     //发送清单，高优先级，用于运动控制
    QList<QPair<int,QByteArray>> SendList2;     //发送清单，低优先级，用于状态查询
    QMutex Send1;                               //发送清单1的线程锁
    QMutex Send2;                               //发送清单2的线程锁
    SerialPortConfig Config;                    //当前Config
    int tID;
    QMutex tIDLock;
    const int timeout = 1000;                   //发送后接受超时时间
signals:
    void RecvMsg(QString ID,QByteArray Msg);
    void SendAndRecvMsg(QByteArray Msg,int ID);
private slots:
    void SendAndRecvMsgSlot(QByteArray Msg,int ID);
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00301000,
        SerialPortHasOpen       = Base + 0x001,
        SerialPortOpenFail      = Base + 0x002,
        SerialPortSetParaErr    = Base + 0x003,
        SerialPortNotOpen       = Base + 0x004,
        PriorityError           = Base + 0x005,
        TempIsTooBig            = Base + 0x006,
    };
    Q_ENUM(ErrorCode)
};

#endif // SERIALPORT_H
