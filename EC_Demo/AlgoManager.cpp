#include "AlgoManager.h"
#include "AlgoCommData.h"
#include "History.h"

AlgoManager::AlgoManager(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
}

AlgoManager::~AlgoManager()
{
    foreach(QString Key,Clients.keys())
    {
        Clients[Key]->Close();
        Clients[Key]->deleteLater();
    }
}

CtrlError AlgoManager::Init(HardwareInitData InitData)
{
    QMap<QString, QPair<QString, quint16> > Client;

    if(InitData.Keys.size() != 3) return CtrlError(InitDataErr,InitData.Name);
    for(int i=0;i<InitData.InitData.size();i++)
    {
        if(InitData.InitData[0].size() != 3) return CtrlError(InitDataErr,InitData.Name);
        QPair<QString, quint16> tClient;
        QString Station;
        for(int j=0;j<3;j++)
        {
            if(InitData.Keys[j] == "Station" && InitData.InitData[i][j].canConvert<QString>())
                Station = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "IP" && InitData.InitData[i][j].canConvert<QString>())
                tClient.first = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "Port" && InitData.InitData[i][j].canConvert<quint16>())
                tClient.second= InitData.InitData[i][j].value<quint16>();
            else
                return CtrlError(InitDataErr,InitData.Name);
        }
        Client.insert(Station,tClient);
    }
    return Init(InitData.Name,Client);
}

CtrlError AlgoManager::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    CtrlError ret;
    ResultData.Station = ExecuteData.Station;
    ResultData.Identfication = ExecuteData.Identfication;
    ResultData.OperatorName = ExecuteData.OperatorName;
	if (ExecuteData.OperatorName == "SendCmdFree")
	{
		QMap<QString, QString> Data;
		foreach(QString key, ExecuteData.Datas.keys())
		{
			Data.insert(key, ExecuteData.Datas[key].value<QString>());
		}
		Data.insert("Key", ExecuteData.Datas["SN"].value<QString>() + "&" + ExecuteData.Datas["Surface"].value<QString>());
		ret = SendCmdFree(Data);
		if (ret != NoError) return ret;
	}
	else if (ExecuteData.OperatorName == "SendCmd")
    {
        if(!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Station = ExecuteData.Datas["Station"].value<QString>();
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["Data"].canConvert<QMap<QString,QString>>()) return CtrlError(ExecuteDataNotRight,Name);
        QMap<QString,QString> Data = ExecuteData.Datas["Data"].value<QMap<QString,QString>>();
        ret = SendCmd(Station,SN,Surface,Data);
        if(ret != NoError) return ret;
    }
	else if(ExecuteData.OperatorName == "LoadGolden")
    {
        if(!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Station = ExecuteData.Datas["Station"].value<QString>();
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["GoldenPath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString GoldenPath = ExecuteData.Datas["GoldenPath"].value<QString>();
        QMap<QString,QString> Options;
        if(ExecuteData.Datas.contains("Options"))
        {
            if(!ExecuteData.Datas["Options"].canConvert<QMap<QString,QString>>()) return CtrlError(ExecuteDataNotRight,Name);
            Options = ExecuteData.Datas["Options"].value<QMap<QString,QString>>();
        }
        ret = LoadGolden(Station,SN,Surface,GoldenPath,Options);
        if(ret != NoError) return ret;
    }
	else if(ExecuteData.OperatorName == "SendCmdPOS")
    {
        if(!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Station = ExecuteData.Datas["Station"].value<QString>();
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["ImagePath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString ImagePath = ExecuteData.Datas["ImagePath"].value<QString>();
        QMap<QString,QString> Options;
        if(ExecuteData.Datas.contains("Options"))
        {
            if(!ExecuteData.Datas["Options"].canConvert<QMap<QString,QString>>()) return CtrlError(ExecuteDataNotRight,Name);
            Options = ExecuteData.Datas["Options"].value<QMap<QString,QString>>();
        }
        ret = SendCmdPOS(Station,SN,Surface,ImagePath,Options);
        if(ret != NoError) return ret;
    }
	else if(ExecuteData.OperatorName == "SendCmdSRC")
    {
        if(!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Station = ExecuteData.Datas["Station"].value<QString>();
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["ImagePath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString ImagePath = ExecuteData.Datas["ImagePath"].value<QString>();
        QMap<QString,QString> Options;
        if(ExecuteData.Datas.contains("Options"))
        {
            if(!ExecuteData.Datas["Options"].canConvert<QMap<QString,QString>>()) return CtrlError(ExecuteDataNotRight,Name);
            Options = ExecuteData.Datas["Options"].value<QMap<QString,QString>>();
        }
        ret = SendCmdSRC(Station,SN,Surface,ImagePath,Options);
        if(ret != NoError) return ret;
    }
	else if(ExecuteData.OperatorName == "SendCmdMOON")
    {
        if(!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Station = ExecuteData.Datas["Station"].value<QString>();
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["ImagePath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString ImagePath = ExecuteData.Datas["ImagePath"].value<QString>();
        QMap<QString,QString> Options;
        if(ExecuteData.Datas.contains("Options"))
        {
            if(!ExecuteData.Datas["Options"].canConvert<QMap<QString,QString>>()) return CtrlError(ExecuteDataNotRight,Name);
            Options = ExecuteData.Datas["Options"].value<QMap<QString,QString>>();
        }
        ret = SendCmdMOON(Station,SN,Surface,ImagePath,Options);
        if(ret != NoError) return ret;
    }
	else if(ExecuteData.OperatorName == "SendCmdIMS")
    {
        if(!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Station = ExecuteData.Datas["Station"].value<QString>();
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["ImagePath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString ImagePath = ExecuteData.Datas["ImagePath"].value<QString>();
        if(!ExecuteData.Datas["ConfigPath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString ConfigPath = ExecuteData.Datas["ConfigPath"].value<QString>();
        QMap<QString,QString> Options;
        if(ExecuteData.Datas.contains("Options"))
        {
            if(!ExecuteData.Datas["Options"].canConvert<QMap<QString,QString>>()) return CtrlError(ExecuteDataNotRight,Name);
            Options = ExecuteData.Datas["Options"].value<QMap<QString,QString>>();
        }
        ret = SendCmdIMS(Station,SN,Surface,ImagePath,ConfigPath,Options);
        if(ret != NoError) return ret;
    }
	else if (ExecuteData.OperatorName == "SendCmdIMSMulity")
	{
		if (!ExecuteData.Datas["Station"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight, Name);
		QString Station = ExecuteData.Datas["Station"].value<QString>();
		if (!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight, Name);
		QString SN = ExecuteData.Datas["SN"].value<QString>();
		if (!ExecuteData.Datas["Surface"].canConvert<QStringList>()) return CtrlError(ExecuteDataNotRight, Name);
		QStringList Surface = ExecuteData.Datas["Surface"].value<QStringList>();
		if (!ExecuteData.Datas["ImagePath"].canConvert<QStringList>()) return CtrlError(ExecuteDataNotRight, Name);
		QStringList ImagePath = ExecuteData.Datas["ImagePath"].value<QStringList>();
		if (!ExecuteData.Datas["ConfigPath"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight, Name);
		QString ConfigPath = ExecuteData.Datas["ConfigPath"].value<QString>();
		QMap<QString, QString> Options;
		if (ExecuteData.Datas.contains("Options"))
		{
			if (!ExecuteData.Datas["Options"].canConvert<QMap<QString, QString>>()) return CtrlError(ExecuteDataNotRight, Name);
			Options = ExecuteData.Datas["Options"].value<QMap<QString, QString>>();
		}
		ret = SendCmdIMS_Mulity(Station, SN, Surface, ImagePath, ConfigPath, Options);
		if (ret != NoError) return ret;
	}
	else if(ExecuteData.OperatorName == "SetBarCode")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["BarCode"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString BarCode = ExecuteData.Datas["BarCode"].value<QString>();
        ret = SetBarCode(SN,BarCode);
        if(ret != NoError) return ret;
    }
	else if (ExecuteData.OperatorName == "ChcekFinish")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        int Send,Recv;
        bool isFinish;
        ret = ChcekFinish(SN,Send,Recv,isFinish);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Send",QVariant::fromValue(Send));
        ResultData.Datas.insert("Recv",QVariant::fromValue(Recv));
        ResultData.Datas.insert("isFinish",QVariant::fromValue(isFinish));
    }
    else if(ExecuteData.OperatorName == "WaitFinish")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        ret = WaitFinish(SN,TimeOut);
        if(ret != NoError) return ret;
    }
    else if(ExecuteData.OperatorName == "GetResult")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        AlgoResult Result;
        ret = GetResult(SN,Surface,Result);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Result",QVariant::fromValue(Result));
    }
	else if(ExecuteData.OperatorName == "WaitResult")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["Surface"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Surface = ExecuteData.Datas["Surface"].value<QString>();
        if(!ExecuteData.Datas["TimeOut"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int TimeOut = ExecuteData.Datas["TimeOut"].value<int>();
        AlgoResult Result;
        ret = WaitResult(SN,Surface,TimeOut,Result);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Result",QVariant::fromValue(Result));
    }
    else if(ExecuteData.OperatorName == "GetAllResult")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        QMap<QString,AlgoResult> Results;
        ret = GetAllResult(SN,Results);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Results",QVariant::fromValue(Results));
    }
	else if(ExecuteData.OperatorName == "Complete")
    {
        if(!ExecuteData.Datas["SN"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString SN = ExecuteData.Datas["SN"].value<QString>();
        if(!ExecuteData.Datas["NewFolder"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString NewFolder = ExecuteData.Datas["NewFolder"].value<QString>();
        int Force = 0;
        if(ExecuteData.Datas.contains("Force"))
        {
            if(!ExecuteData.Datas["Force"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
            Force = ExecuteData.Datas["Force"].value<int>();
        }
        ret = Complete(SN,NewFolder,Force);
        if(ret != NoError) return ret;
    }
    return CtrlError(NoError);
}

CtrlError AlgoManager::Init(QString Name, QMap<QString, QPair<QString, quint16> > Client)
{
    CtrlError ret;
    foreach (QString Station, Client.keys())
    {
        AlgoClient *tClient = new AlgoClient();
        ret = tClient->Open(Name+"_"+Station,Client[Station].first,Client[Station].second);
        if(ret != NoError) return ret;
        Clients.insert(Station,tClient);
        connect(tClient,SIGNAL(RecvResult(QString,QString)),this,SLOT(RecvResult(QString,QString)));
    }
    return CtrlError(NoError);
}

CtrlError AlgoManager::SendCmd(QString Station, QString SN,QString Surface, QMap<QString, QString> Data)
{
    CtrlError ret;
    AlgoCommData Cmd;
    Cmd.Data = Data;
    if(!Clients.contains(Station)) return CtrlError(StationNotExist,Name);
    Datalock.lockForWrite();
    if(!SendSum.contains(SN)) SendSum[SN] = QSet<QString>();
    SendSum[SN].insert(Surface);
    ret = Clients[Station]->Send(Cmd.GetData());
    Datalock.unlock();
    return ret;
}

CtrlError AlgoManager::SendCmdFree(QMap<QString, QString> Data)
{
	CtrlError ret;
	AlgoCommData Cmd;
	Cmd.Data = Data;
	Datalock.lockForWrite();
	if (!Data.keys().contains("Station"))return CtrlError(StationNotExist, Name);
	ret = Clients[Data["Station"]]->Send(Cmd.GetData());
	Datalock.unlock();
	return ret;
}

CtrlError AlgoManager::SendCmdMulityFace(QString Station, QString SN, QStringList Surface, QMap<QString, QString> Data)
{
	CtrlError ret;
	AlgoCommData Cmd;
	Cmd.Data = Data;
	if (!Clients.contains(Station)) return CtrlError(StationNotExist, Name);
	Datalock.lockForWrite();
	if (!SendSum.contains(SN)) SendSum[SN] = QSet<QString>();
	foreach(QString face,Surface)
		SendSum[SN].insert(face);
	ret = Clients[Station]->Send(Cmd.GetData());
	Datalock.unlock();
	return ret;
}

CtrlError AlgoManager::LoadGolden(QString Station, QString SN, QString Surface, QString GoldenPath, QMap<QString, QString> Options)
{
    QMap<QString, QString> Data = Options;
    Data.insert("Module","SmartAlgoC");
    Data.insert("Command","LoadGolden");
    Data.insert("GoldenPath",GoldenPath);
    return SendCmd(Station,SN,Surface,Data);
}

CtrlError AlgoManager::SendCmdPOS(QString Station, QString SN, QString Surface, QString ImagePath, QMap<QString, QString> Options)
{
    QMap<QString, QString> Data = Options;
    Data.insert("Module","SmartAlgoC");
    Data.insert("Command","CalcImage");
    Data.insert("Type","POS");
    Data.insert("SerialNum",SN);
    Data.insert("FaceID",Surface);
    Data.insert("DutPath",ImagePath);
    return SendCmd(Station,SN,Surface,Data);
}

CtrlError AlgoManager::SendCmdSRC(QString Station, QString SN, QString Surface, QString ImagePath, QMap<QString, QString> Options)
{
    QMap<QString, QString> Data = Options;
    Data.insert("Module","SmartAlgoC");
    Data.insert("Command","CalcImage");
    Data.insert("Type","SRCNOPOS");
    Data.insert("SerialNum",SN);
    Data.insert("FaceID",Surface);
    Data.insert("DutPath",ImagePath);
    return SendCmd(Station,SN,Surface,Data);
}

CtrlError AlgoManager::SendCmdMOON(QString Station, QString SN, QString Surface, QString ImagePath, QMap<QString, QString> Options)
{
    QMap<QString, QString> Data = Options;
    Data.insert("Module","SmartAlgoC");
    Data.insert("Command","CalcImage");
    Data.insert("Type","MOON");
    Data.insert("SerialNum",SN);
    Data.insert("FaceID",Surface);
    Data.insert("DutPath",ImagePath);
    return SendCmd(Station,SN,Surface,Data);
}

CtrlError AlgoManager::SendCmdIMS(QString Station, QString SN, QString Surface, QString ImagePath, QString ConfigPath, QMap<QString,QString> Options)
{
    QMap<QString, QString> Data = Options;
    Data.insert("Key",SN + "&" + Surface);
    Data.insert("ConfigPath", ConfigPath);
    Data.insert("ImagePath",ImagePath);
    return SendCmd(Station,SN,Surface,Data);
}

CtrlError AlgoManager::SendCmdIMS_Mulity(QString Station, QString SN, QStringList Surface, QStringList ImagePath, QString ConfigPath, QMap<QString, QString> Options /*= QMap<QString, QString>()*/)
{
	QMap<QString, QString> Data = Options;
	//Data.insert("Key", SN + "&" + Surface);
	QString surfaceStr;
	for (int i =0;i<Surface.count();i++)
	{
		if (i == Surface.count()-1)
			surfaceStr = surfaceStr + Surface[i];
		else
			surfaceStr = surfaceStr + Surface[i] + ",";
	}
	Data.insert("Key", SN + "&" + surfaceStr);
	if (Surface.count() != ImagePath.count())
		return CtrlError(IMSMulityMatchError);
	for (int i = 0; i < Surface.count(); i++)
	{
		Data.insert(Surface[i], ImagePath[i]);
	}

	Data.insert("ConfigPath", ConfigPath);
	//Data.insert("ImagePath", ImagePath);
	return SendCmdMulityFace(Station, SN, Surface, Data);
}

CtrlError AlgoManager::SetBarCode(QString SN, QString BarCode)
{
    QWriteLocker tLocker(&Datalock);
    BarCodes.insert(SN,BarCode);
    return CtrlError(NoError);
}

CtrlError AlgoManager::ChcekFinish(QString SN, int &Send, int &Recv, bool &isFinish)
{
    QReadLocker tLocker(&Datalock);
    Send = SendSum[SN].size();
    Recv = RecvSum[SN].size();
    isFinish = Send <= Recv;
    return CtrlError(NoError);
}

CtrlError AlgoManager::WaitFinish(QString SN, int TimeOut)
{
    CtrlError ret;
    QTime timer;
    timer.start();
    bool isFinish;
    int Send,Recv;
    while(1)
    {
        ret = ChcekFinish(SN,Send,Recv,isFinish);
        if(ret != NoError) return ret;
        if(TimeOut > 0 && timer.elapsed() >= TimeOut) return CtrlError(WaitFinishTimeout,Name);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        if(isFinish) break;
    }
    return CtrlError(NoError);
}

CtrlError AlgoManager::GetResult(QString SN, QString Surface, AlgoResult &Result)
{
    QReadLocker tLocker(&Datalock);
    if(RecvRes.contains(SN) && RecvRes[SN].contains(Surface))
    {
        Result = RecvRes[SN][Surface];
    }
    else
    {
        return CtrlError(ResultNotExist,Name);
    }
    return CtrlError(NoError);
}

CtrlError AlgoManager::WaitResult(QString SN, QString Surface, int TimeOut, AlgoResult &Result)
{
    QTime timer;
    timer.start();
    while(1)
    {
        Datalock.lockForRead();
        if(RecvRes.contains(SN) && RecvRes[SN].contains(Surface))
        {
           Result = RecvRes[SN][Surface];
		   RecvRes[SN].remove(Surface);
		   //RecvRes.remove(SN);
           Datalock.unlock();
           return CtrlError(NoError);
        }
        Datalock.unlock();
        if(TimeOut > 0 && timer.elapsed() >= TimeOut) return CtrlError(WaitResultTimeout,Name);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

CtrlError AlgoManager::GetAllResult(QString SN, QMap<QString, AlgoResult> &Results)
{
    if(!RecvRes.contains(SN)) return CtrlError(SNNotExist,Name);
    Results = RecvRes[SN];
    return CtrlError(NoError);
}

CtrlError AlgoManager::Complete(QString SN, QString NewFolder, int Force)
{
    CtrlError ret;
    if(Force == 0)
    {
        int Send,Recv;
        bool isFinish;
        ret = ChcekFinish(SN,Send,Recv,isFinish);
        if(ret != NoError) return ret;
        if(!isFinish) return CtrlError(SNNotFinish,Name);
    }
    QWriteLocker tLocker(&Datalock);
    QString Path = NewFolder + "/" + SN;
    if(BarCodes.contains(SN)) Path.append("_"+BarCodes[SN]);
    QDir tFolder(Path);
    if(!tFolder.exists())
    {
        tFolder.mkpath(Path);
    }
    QMap<QString,QString> NewFilePaths;
    if(RecvPath.contains(SN))
    {
        foreach (QString OldPath, RecvPath[SN].values())
        {
            QString NewPath = Path + "/" + OldPath.right(OldPath.size() - OldPath.lastIndexOf("/"));
            if(QFile::copy(OldPath,NewPath))
            {
                QFile::remove(OldPath);
                NewFilePaths.insert(OldPath.right(OldPath.size() - OldPath.lastIndexOf("/")-1),NewPath);
            }
        }
    }
    //emit SetDataInDB(SN,BarCodes[SN],DataBaseData[SN],NewFilePaths);
	History::GetInstance().SetDataInDB(SN, BarCodes[SN], DataBaseData[SN], NewFilePaths);
    SendSum.remove(SN);
    RecvSum.remove(SN);
    RecvRes.remove(SN);
    RecvPath.remove(SN);
    BarCodes.remove(SN);
    DataBaseData.remove(SN);
    return CtrlError(NoError);
}

void AlgoManager::RecvResult(QString Name, QString ResultStr)
{
    QWriteLocker tLocker(&Datalock);
    AlgoCommData Result;
    CtrlError ret = Result.SetData(ResultStr);
    if(ret != NoError) return;
    QString SN;
    QString Surface;

    if(Result.Data.contains("Key"))
    {
        SN = Result.Data["Key"].left(Result.Data["Key"].indexOf("&"));
        Surface = Result.Data["Key"].right(Result.Data["Key"].size() - Result.Data["Key"].indexOf("&") - 1);
    }
    else if(Result.Data.contains("SerialNum")&&Result.Data.contains("FaceID"))
    {
        SN = Result.Data["SerialNum"];
        Surface = Result.Data["FaceID"];
    }
    else return;

    AlgoResult Res;
    Res.Surface = Surface;
    Res.Result = "ERROR";
	Res.msg = ResultStr;

	Res.ResultPath = Result.Data["ResultPath"];
	//if (Result.Data.contains("ImagePath"))
	//{
	//	Res.ResultPath = Result.Data["ResultPath"];
	//}

    if(Result.Data.contains("Error") && (Result.Data["Error"] == "NoError" || Result.Data["Error"] == "Null"))
    {
        if(Result.Data.contains("Result"))
        {
            Res.Result = Result.Data["Result"];
        }
        if(Result.Data.contains("ResultPath"))
        {
            Res.ResultPath = Result.Data["ResultPath"];
        }
		if (Result.Data.contains("ImagePath"))
		{
			Res.ResultPath = Result.Data["ImagePath"];
		}
        if(Result.Data.contains("TargetPoint"))
        {
            Res.TargetPoint = Result.Data["TargetPoint"];
        }
		if (Result.Data.contains("TargetPose"))
		{
			Res.TargetPoint = Result.Data["TargetPose"];
		}
		//P1U1, P1D2
		if (Result.Data.contains("P1U1"))
		{
			Res.FacePath.push_back(Result.Data["P1U1"]);
		}
		if (Result.Data.contains("P1D2"))
		{
			Res.FacePath.push_back(Result.Data["P1D2"]);
		}
		if (Result.Data.contains("P2U1"))
		{
			Res.FacePath.push_back(Result.Data["P2U1"]);
		}
		if (Result.Data.contains("P2D2"))
		{
			Res.FacePath.push_back(Result.Data["P2D2"]);
		}
		if (Result.Data.contains("P3U1"))
		{
			Res.FacePath.push_back(Result.Data["P3U1"]);
		}
		if (Result.Data.contains("P3D2"))
		{
			Res.FacePath.push_back(Result.Data["P3D2"]);
		}
		if (Result.Data.contains("P4U1"))
		{
			Res.FacePath.push_back(Result.Data["P4U1"]);
		}
		if (Result.Data.contains("P4D2"))
		{
			Res.FacePath.push_back(Result.Data["P4D2"]);
		}
		if (Result.Data.contains("P5U1"))
		{
			Res.FacePath.push_back(Result.Data["P5U1"]);
		}
		if (Result.Data.contains("P5D2"))
		{
			Res.FacePath.push_back(Result.Data["P5D2"]);
		}

    }
//    else
//    {
//        if(Result.Data.contains("Result"))
//    }
    if(!RecvRes.contains(SN)) RecvRes[SN] = QMap<QString,AlgoResult>();
    RecvRes[SN].insert(Surface,Res);

    if(!RecvPath.contains(SN)) RecvPath[SN] = QSet<QString>();
    foreach(QString Key,Result.Data.keys())
    {
        QFileInfo tFile(Result.Data[Key]);
        if(tFile.exists()) RecvPath[SN].insert(Result.Data[Key]);
    }

    if(!RecvSum.contains(SN)) RecvSum[SN] = QSet<QString>();
    RecvSum[SN].insert(Surface);

    if(!DataBaseData.contains(SN)) DataBaseData[SN] = QMap<QString,AlgoCommData>();
    DataBaseData[SN].insert(Surface,Result);
}
