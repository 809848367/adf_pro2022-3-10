#include "TeachData_Lighting.h"

TeachData_Lighting::TeachData_Lighting(QObject *parent) : TeachData(parent)
{
    qRegisterMetaType <TeachData_Lighting>("TeachData_Lighting");
}

TeachData_Lighting::TeachData_Lighting(const TeachData_Lighting &TeachData)
{
    this->Lights = TeachData.Lights;
}

int TeachData_Lighting::FromTable(QVector<QString> Keys, QVector<QVector<QVariant> > Data)
{
    if(Keys.size() != 2) return -1;
    for(int i=0;i<Data.size();i++)
    {
        if(Data[i].size() != 2) return -1;
        QMap<int,uchar> tLight;
        QString tLightName;
        for(int j=0;j<Keys.size();j++)
        {
            QList<QString> tmp;
            if(Keys[j] == "Name"     && Data[i][j].typeName() == QString("QString")) tLightName = Data[i][j].value<QString>();
            if(Keys[j] == "Value"    && Data[i][j].typeName() == QString("QString"))
            {
                tmp = Data[i][j].value<QString>().split(";");
                foreach(QString KeyValue,tmp)
                {
                    if(!KeyValue.contains(",")) continue;
                    bool isOK;
                    int Index = KeyValue.left(KeyValue.indexOf(",")).toInt(&isOK);
                    if(!isOK) continue;
                    uchar Value = KeyValue.right(KeyValue.size()-KeyValue.lastIndexOf(",")-1).toUInt(&isOK);
                    if(!isOK) continue;
                    tLight.insert(Index,Value);
                }
            }
        }
        SetLight(tLightName,tLight);
    }
    return 0;
}

void TeachData_Lighting::ToTable(QVector<QString> &Keys, QVector<QVector<QVariant> > &Data)
{
    Keys.clear();
    Data.clear();
    Keys<<"Name"<<"Value";
    for(int i=0;i<Lights.size();i++)
    {
        QVector<QVariant> tData;
        tData.push_back(QVariant::fromValue(Lights[i].first));
        QString tmp;
        foreach(int Key,Lights[i].second.keys())
        {
            tmp.append(QString::number(Key)+","+QString::number(Lights[i].second[Key])+";");
        }
        if(!tmp.isEmpty()) tmp.remove(tmp.size()-1,1);
        tData.push_back(QVariant::fromValue(tmp));
        Data.push_back(tData);
    }
    return;
}

int TeachData_Lighting::GetLight(QString Name, QMap<int, uchar> &Light)
{
    int Index = 0;
    for(;Index < Lights.size();Index++)
    {
        if(Lights[Index].first == Name)
        {
            Light = Lights[Index].second;
            return 0;
        }
    }
    return -1;
}

QList<QPair<QString, QMap<int, uchar> > > TeachData_Lighting::GetLights()
{
    return Lights;
}

void TeachData_Lighting::SetLight(QString Name, QMap<int, uchar> Light)
{
    int Index = 0;
    for(;Index < Lights.size();Index++)
    {
        if(Lights[Index].first == Name) break;
    }
    if(Index < Lights.size() && Lights[Index].first == Name)
    {
        Lights[Index].second = Light;
    }
    else
    {
        Lights.push_back(QPair<QString,QMap<int, uchar>>(Name,Light));
    }
    return;
}

void TeachData_Lighting::RemoveLight(QString Name)
{
    int Index = 0;
    for(;Index < Lights.size();Index++)
    {
        if(Lights[Index].first == Name) break;
    }
    if(Index < Lights.size() && Lights[Index].first == Name)
    {
        Lights.removeAt(Index);
    }
    return;
}

TeachData_Lighting &TeachData_Lighting::operator =(const TeachData_Lighting &TeachData)
{
    this->Lights = TeachData.Lights;
    return *this;
}
