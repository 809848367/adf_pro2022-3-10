#include "ModbusMoudle.h"

ModbusMoudle::ModbusMoudle(QObject *parent)
	: QObject(parent)
{
	RegisterCtrlError();
}

ModbusMoudle::~ModbusMoudle()
{
}

CtrlError ModbusMoudle::ConnectETH(QString ipAdress, int nNetId, int nIpPort /*= 502*/)
{
	int iRet = Init_ETH_String(const_cast<char*> (ipAdress.toStdString().c_str()), nNetId, nIpPort);
	if (iRet) return CtrlError(NoError);
	else return ConnectionError;
}


CtrlError ModbusMoudle::DisconnectETH(int nNetId)
{
	Exit_ETH(nNetId);
	return NoError;
}

CtrlError ModbusMoudle::WriteDataInt_ETH(SoftElemType eType, int nStartAddr, int nValue)
{
	QMutexLocker locker(&m_mutex);
	//BYTE dataBuf[256];
	//BYTE *pData = dataBuf;
	//ZeroMemory(pData, 256);
	//IntValueToPByte(nValue, pData);
	//int iRet = H5u_Write_Device_Block(eType, nStartAddr, 2, pData, 0);
	long L_Val = nValue;
	int iRet = H5u_Write_Soft_Elem_Int32(eType, nStartAddr, 2, &L_Val, 0);

	if (iRet==0) return WriteError;
	return NoError;
}

CtrlError ModbusMoudle::WriteDataFloat_ETH(SoftElemType eType, int nStartAddr, float nValue)
{
	QMutexLocker locker(&m_mutex);
	//BYTE dataBuf[256];
	//BYTE *pData = dataBuf;
	//ZeroMemory(pData, 256);
	//FloatValueToPByte(nValue, pData);
	//int iRet = H5u_Write_Device_Block(eType, nStartAddr, 2, pData, 0);
	int iRet = H5u_Write_Soft_Elem_Float(eType, nStartAddr, 2, &nValue, 0);

	if (iRet) return NoError;
	else  return WriteError;
}

CtrlError ModbusMoudle::ReadDataInt_ETH(SoftElemType eType, int nStartAddr, int& value)
{
	QMutexLocker locker(&m_mutex);
	//BYTE dataBuf[256];
	//BYTE *pData = dataBuf;
	//ZeroMemory(pData, 256);
	//int nNetId = 0;
	//int nRet = H5u_Read_Device_Block(eType, nStartAddr, 2, pData, nNetId);
	//value = PByteConvertToInt(pData);
	long L_Val = 0;
	int nRet = H5u_Read_Soft_Elem_Int32(eType, nStartAddr, 2, &L_Val, 0);
	value = L_Val;

	if (nRet) return NoError;
	else return ReadError;
}

CtrlError ModbusMoudle::ReadDataFloat_ETH(SoftElemType eType, int nStartAddr, float& value)
{
	QMutexLocker locker(&m_mutex);
	//byte m_DataBuf[256];
	//BYTE* pData = m_DataBuf;
	//ZeroMemory(pData, 256);
	//int nRet = 0;
	//int nNetId = 0;
	//nRet = H5u_Read_Device_Block(eType, nStartAddr, 2, pData, nNetId);
	//value = PByteConvertToFloat(pData);

	int nRet = H5u_Read_Soft_Elem_Float(eType, nStartAddr, 2, &value, 0);

	if (nRet)
		return NoError;
	else
		return ReadError;
}

CtrlError ModbusMoudle::WriteDataCheckInt_ETH(SoftElemType eType, int nStartAddr, qint16 nValue)
{
	QMutexLocker locker(&m_mutex);
	//BYTE dataBuf[256];
	//BYTE *pData = dataBuf;
	//ZeroMemory(pData, 256);
	//IntValueToPByte(nValue, pData);
	//int iRet = H5u_Write_Device_Block(eType, nStartAddr, 2, pData, 0);
	long WL_Val = nValue;
	int iRet = H5u_Write_Soft_Elem_Int32(eType, nStartAddr, 2, &WL_Val, 0);

	//读取数据
	//ZeroMemory(pData, 256);
	//int nRet = 0;
	//int nNetId = 0;
	//int iRead = 0;
	//nRet = H5u_Read_Device_Block(eType, nStartAddr, 2, pData, nNetId);
	//iRead = PByteConvertToInt(pData);
	long RL_Val = -1;
	int nRet = H5u_Read_Soft_Elem_Int32(eType, nStartAddr, 2, &RL_Val, 0);

	if (nRet == 0)
		return ReadError;
	if (WL_Val != RL_Val)
		return DataNotPatch;
	return NoError;
}

CtrlError ModbusMoudle::WriteDataCheckFloat_ETH(SoftElemType eType, int nStartAddr, float nValue)
{
	QMutexLocker locker(&m_mutex);
	//BYTE dataBuf[256];
	//BYTE *pData = dataBuf;
	//ZeroMemory(pData, 256);
	////写入数据
	//FloatValueToPByte(nValue, pData);
	//int iRet = H5u_Write_Device_Block(eType, nStartAddr, 2, pData, 0);
	int iRet = H5u_Write_Soft_Elem_Float(eType, nStartAddr, 2, &nValue, 0);

	//读取数据
	//ZeroMemory(pData, 256);
	//int nRet = 0;
	//int nNetId = 0;
	//float fRead = 0;
	//nRet = H5u_Read_Device_Block(eType, nStartAddr, 2, pData, nNetId);
	//fRead = PByteConvertToFloat(pData);
	float fRead = 0;
	H5u_Read_Soft_Elem_Float(eType, nStartAddr, 2, &fRead, 0);

	if (nValue != fRead)
		return WriteError;
	return NoError;
}

ModbusMoudle* ModbusMoudle::Instance()
{
	static ModbusMoudle d;
	return &d;
}

void ModbusMoudle::RegisterCtrlError()
{
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0;i < ErrorCodeEnum.keyCount();i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}
}
// nDataType  0:INT ,1:FLOAT
int ModbusMoudle::PByteConvertToInt(BYTE* pData)
{
	int iRet = 0;
	iRet = *((WORD*)pData);
	return iRet;
}

float ModbusMoudle::PByteConvertToFloat(BYTE* pData)
{
	float fRet = 0;
	fRet = *((float*)pData);
	return fRet;
}

void ModbusMoudle::IntValueToPByte(int value, BYTE* pData)
{
	//*pData = (BYTE)value;
	//WORD DValue = value;
	*((WORD*)pData) = (WORD)value;
}

void ModbusMoudle::FloatValueToPByte(float value, BYTE* pData)
{
	*((float*)pData) = (float)value;
}





