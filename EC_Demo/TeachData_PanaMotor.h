#ifndef TEACHDATA_PANAMOTOR_H
#define TEACHDATA_PANAMOTOR_H
#include "TeachData.h"

class TeachData_PanaMotor:public TeachData
{
    Q_OBJECT
public:
	virtual int FromTable(QVector<QString> Keys, QVector<QVector<QVariant>> Data)override;
	virtual void ToTable(QVector<QString> &Keys, QVector<QVector<QVariant>> &Data)override;
public:
    explicit TeachData_PanaMotor(QObject *parent = nullptr);
    TeachData_PanaMotor(const TeachData_PanaMotor &TeachData);

    int GetPoint(QString Name,int &Point);
    QList<QPair<QString,int>> GetPoints();
    void SetPoint(QString Name,int Point);
    void RemovePoint(QString Name);
    TeachData_PanaMotor &operator =(const TeachData_PanaMotor &TeachData);
private:
    QList<QPair<QString,int>> Points;
};

#endif // TEACHDATA_PANAMOTOR_H
