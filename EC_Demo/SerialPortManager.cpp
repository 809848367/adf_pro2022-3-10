﻿#include "SerialPortManager.h"

SerialPortManager::SerialPortManager(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
}

SerialPortManager::~SerialPortManager()
{
    foreach (QString Key, SerialList.keys())
    {
        SerialList[Key]->Stop();
        SerialList[Key]->deleteLater();
    }
}

CtrlError SerialPortManager::Init(HardwareInitData InitData)
{
    QList<SerialPort::SerialPortConfig> Configs;

    if(InitData.Keys.size() != 7) return CtrlError(InitDataErr,InitData.Name);
    for(int i=0;i<InitData.InitData.size();i++)
    {
        if(InitData.InitData[0].size() != 7) return CtrlError(InitDataErr,InitData.Name);
        SerialPort::SerialPortConfig tConfig;
        for(int j=0;j<7;j++)
        {
            if(InitData.Keys[j] == "COM" && InitData.InitData[i][j].canConvert<QString>())
                tConfig.COM = InitData.InitData[i][j].value<QString>();
            else if(InitData.Keys[j] == "BaudRate" && InitData.InitData[i][j].canConvert<int>())
                tConfig.BaudRate = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "DataBits" && InitData.InitData[i][j].canConvert<int>())
                tConfig.DataBits = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "FlowControl" && InitData.InitData[i][j].canConvert<int>())
                tConfig.FlowControl = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "Parity" && InitData.InitData[i][j].canConvert<int>())
                tConfig.Parity = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "StopBits" && InitData.InitData[i][j].canConvert<int>())
                tConfig.StopBits = InitData.InitData[i][j].value<int>();
            else if(InitData.Keys[j] == "Type" && InitData.InitData[i][j].canConvert<QString>())
                tConfig.Type = InitData.InitData[i][j].value<QString>();
            else
                return CtrlError(InitDataErr,InitData.Name);
        }
        Configs.push_back(tConfig);
    }


    return Init(InitData.Name,Configs);
}

CtrlError SerialPortManager::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    return CtrlError(ExecuteNotAllowed,Name);
}

CtrlError SerialPortManager::Init(QString Name,QList<SerialPort::SerialPortConfig> Configs)
{
    CtrlError ret;
    this->Name = Name;
    foreach (SerialPort::SerialPortConfig Config, Configs)
    {
        SerialPort *tSerial = new SerialPort;
        ret = tSerial->Open(Config);
        if(ret != NoError) return ret;
        SerialList.insert(Config.COM,tSerial);
        connect(tSerial,SIGNAL(RecvMsg(QString,QByteArray)),this,SLOT(RecvMsg(QString,QByteArray)));
    }
    return CtrlError(NoError);
}

CtrlError SerialPortManager::SendAndRecv(QString Com, QByteArray Cmd, QByteArray &Res, int Priority)
{
    CtrlError ret;
    QString ID;
    ret = Send(Com,Cmd,ID,Priority);
    if(ret != NoError) return ret;
    ret = WaitforRecv(ID,Res);
    if(ret != NoError) return ret;
    return CtrlError(NoError);
}

CtrlError SerialPortManager::Send(QString Com, QByteArray Cmd, QString &ID, int Priority)
{
    CtrlError ret;
    if(!SerialList.contains(Com)) return CtrlError(SerialNotExist,Name);
    ret = SerialList[Com]->SendMsg(Cmd,ID,Priority);
    if(ret != NoError) return ret;
    if(RecvData.contains(ID))
    {
        RecvDataLock.lock();
        RecvData.remove(ID);
        RecvDataLock.unlock();
    }
    return CtrlError(NoError);
}

CtrlError SerialPortManager::Recv(QString ID, QByteArray &Res)
{
    if(!RecvData.contains(ID)) return CtrlError(RecvDataNotExist,Name);
    Res = RecvData[ID];
    RecvDataLock.lock();
    RecvData.remove(ID);
    RecvDataLock.unlock();
    return CtrlError(NoError);
}

CtrlError SerialPortManager::WaitforRecv(QString ID, QByteArray &Res)
{
    Res = "";
    while(1)
    {
        RecvDataLock.lock();
        if(RecvData.contains(ID))
        {
            Res = RecvData[ID];
            RecvData.remove(ID);
            RecvDataLock.unlock();
            return CtrlError(NoError);
        }
        RecvDataLock.unlock();
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}

void SerialPortManager::RecvMsg(QString ID, QByteArray Msg)
{
    RecvDataLock.lock();
    RecvData.insert(ID,Msg);
    RecvDataLock.unlock();
}
