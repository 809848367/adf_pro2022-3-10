#ifndef TEACHDATA_LIGHTING_H
#define TEACHDATA_LIGHTING_H
#include "TeachData.h"

class TeachData_Lighting:public TeachData
{
    Q_OBJECT
public:
	virtual int FromTable(QVector<QString> Keys, QVector<QVector<QVariant>> Data)override;
	virtual void ToTable(QVector<QString> &Keys, QVector<QVector<QVariant>> &Data)override;
public:
    explicit TeachData_Lighting(QObject *parent = nullptr);
    TeachData_Lighting(const TeachData_Lighting &TeachData);
    int GetLight(QString Name,QMap<int, uchar> &Light);
    QList<QPair<QString,QMap<int, uchar>>> GetLights();
    void SetLight(QString Name,QMap<int, uchar> Light);
    void RemoveLight(QString Name);
    TeachData_Lighting &operator =(const TeachData_Lighting &TeachData);
private:
    QList<QPair<QString,QMap<int, uchar>>> Lights;
};

#endif // TEACHDATA_LIGHTING_H
