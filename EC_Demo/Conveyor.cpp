﻿#include "Conveyor.h"
#include "HardwareManager.h"
#include "SerialPortManager.h"
Conveyor::Conveyor(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    GetStatusFirst = true;
}

CtrlError Conveyor::Init(HardwareInitData InitData)
{
    if(InitData.Keys.size() != 4) return CtrlError(InitDataErr,InitData.Name);
    if(InitData.InitData.size() != 1) return CtrlError(InitDataErr,InitData.Name);
    if(InitData.InitData[0].size() != 4) return CtrlError(InitDataErr,InitData.Name);
    QString COM;
    int ID;
    int Vel;
    SerialPortManager *tSPM;
    for(int j=0;j<4;j++)
    {
        if(InitData.Keys[j] == "COM" && InitData.InitData[0][j].canConvert<QString>())
            COM = InitData.InitData[0][j].value<QString>();
        else if(InitData.Keys[j] == "ID" && InitData.InitData[0][j].canConvert<int>())
            ID = InitData.InitData[0][j].value<int>();
        else if(InitData.Keys[j] == "Vel" && InitData.InitData[0][j].canConvert<int>())
            Vel = InitData.InitData[0][j].value<int>();
        else if(InitData.Keys[j] == "SerialPortManager" && InitData.InitData[0][j].canConvert<QString>())
        {
            tSPM = qobject_cast<SerialPortManager*>(HardwareManager::GetInstance()->GetModulePtr(InitData.InitData[0][j].value<QString>()));
        }
        else
            return CtrlError(InitDataErr,InitData.Name);
    }
    return Init(InitData.Name,COM,ID,Vel,tSPM);
}

CtrlError Conveyor::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    CtrlError ret;
    ResultData.Station = ExecuteData.Station;
    ResultData.Identfication = ExecuteData.Identfication;
    ResultData.OperatorName = ExecuteData.OperatorName;
    if(ExecuteData.OperatorName == "Start")
    {
        ret =  Start();
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "Stop")
    {
        ret =  Stop();
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "GetStatus")
    {
        int Status;
        ret =  GetStatus(Status);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Status",QVariant::fromValue(Status));
    }
    if(ExecuteData.OperatorName == "SetEnable")
    {
        if(!ExecuteData.Datas["Enable"].canConvert<bool>()) return CtrlError(ExecuteDataNotRight,Name);
        bool Enable = ExecuteData.Datas["Enable"].value<bool>();
        ret =  SetEnable(Enable);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "SetVel")
    {
        if(!ExecuteData.Datas["Vel"].canConvert<int>()) return CtrlError(ExecuteDataNotRight,Name);
        int Vel = ExecuteData.Datas["Vel"].value<int>();
        ret =  SetVel(Vel);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "ClearErr")
    {
        ret =  ClearErr();
        if(ret != NoError) return ret;
    }
    return CtrlError(NoError);
}

CtrlError Conveyor::Init(QString Name, QString Com, int ID, int Vel, SerialPortManager *SPM)
{
    this->Name = Name;
    this->Com = Com;
    this->ID = ID;
    this->Vel = Vel;
    this->SPM = SPM;
    int Status;
    return GetStatus(Status);
}

//启动可以慢点，所以先查询状态/开启使能/清除错误，并且设置速度，再启动
CtrlError Conveyor::Start()
{
    CtrlError ret;
    int RetryTimes = 0;
    while(1)
    {
        int Status;
        ret = GetStatus(Status);
        if(ret != NoError) return ret;
        if(Status == 1 || Status == 2) break;
        if(Status == 0)
        {
            ret = SetEnable(true);
            if(ret != NoError) return ret;
        }
        if(Status == 3)
        {
            ret = ClearErr();
            if(ret != NoError) return ret;
        }
        if(++RetryTimes > 5) return CtrlError(ClearErrFail,Name);
    }
    ret = SetVel(Vel);
    if(ret != NoError) return ret;
    QByteArray StartCmd,Recv;
    StartCmd.push_back(char(ID));
    StartCmd.push_back(char(0x06));
    StartCmd.push_back(char(0x00));
    StartCmd.push_back(char(0x7C));
    StartCmd.push_back(char(0x00));
    StartCmd.push_back(char(0x96));
    SetCRC16Modbus(StartCmd);
    ret = SPM->SendAndRecv(Com,StartCmd,Recv,1);
    if(ret != NoError) return ret;
    if(Recv.size() < 2 || (int)Recv[1] == 0x86) return CtrlError(StartCmdErr,Name);
    return CtrlError(NoError);
}

CtrlError Conveyor::Stop()
{
    CtrlError ret;
    QByteArray StopCmd,Recv;
    StopCmd.push_back(char(ID));
    StopCmd.push_back(char(0x06));
    StopCmd.push_back(char(0x00));
    StopCmd.push_back(char(0x7C));
    StopCmd.push_back(char(0x00));
    StopCmd.push_back(char(0xD8));
    SetCRC16Modbus(StopCmd);
    ret = SPM->SendAndRecv(Com,StopCmd,Recv,1);
    if(ret != NoError) return ret;
    if(Recv.size() < 2 || (int)Recv[1] == 0x86) return CtrlError(StopCmdErr,Name);
    return CtrlError(NoError);
}

CtrlError Conveyor::GetStatus(int &Status)
{
    if(GetStatusFirst || GetStatustimer.elapsed() > 200)
    {
        GetStatustimer.start();
        CtrlError ret;
        QByteArray StatusCmd,Recv;
        StatusCmd.push_back(char(ID));
        StatusCmd.push_back(char(0x03));
        StatusCmd.push_back(char(0x00));
        StatusCmd.push_back(char(0x01));
        StatusCmd.push_back(char(0x00));
        StatusCmd.push_back(char(0x02));
        SetCRC16Modbus(StatusCmd);
        ret = SPM->SendAndRecv(Com,StatusCmd,Recv,1);
        if(ret != NoError) return ret;
        if(Recv.size() < 5 || (int)Recv[1] == 0x83) return CtrlError(GetStatusCmdErr,Name);
        int StatusByte = (int)Recv[3]*256+(int)Recv[4];
        //出错和报警
        if(StatusByte & 0x0004 || StatusByte & 0x0200) GetStatustStatus = 3;
        //运动和点动
        else if(StatusByte & 0x0010 || StatusByte & 0x0020) GetStatustStatus = 2;
        //使能
        else if(StatusByte & 0x0001) GetStatustStatus = 1;
        else GetStatustStatus = 0;
    }
    Status = GetStatustStatus;
    return CtrlError(NoError);
}

CtrlError Conveyor::SetEnable(bool Enable)
{
    CtrlError ret;
    QByteArray EnableCmd,Recv;
    EnableCmd.push_back(char(ID));
    EnableCmd.push_back(char(0x06));
    EnableCmd.push_back(char(0x00));
    EnableCmd.push_back(char(0x7C));
    EnableCmd.push_back(char(0x00));
    if(Enable) EnableCmd.push_back(char(0x9F));
    else EnableCmd.push_back(char(0x9E));
    SetCRC16Modbus(EnableCmd);
    ret = SPM->SendAndRecv(Com,EnableCmd,Recv,1);
    if(ret != NoError) return ret;
    if(Recv.size() < 2 || (int)Recv[1] == 0x86) return CtrlError(SetEnableCmdErr,Name);
    return CtrlError(NoError);
}

CtrlError Conveyor::SetVel(int Vel)
{
    CtrlError ret;
    QByteArray VelCmd,Recv;
    VelCmd.push_back(char(ID));
    VelCmd.push_back(char(0x10));
    VelCmd.push_back(char(0x00));
    VelCmd.push_back(char(0x2E));
    VelCmd.push_back(char(0x00));
    VelCmd.push_back(char(0x03));
    VelCmd.push_back(char(0x06));
    VelCmd.push_back(char(0x02));
    VelCmd.push_back(char(0x58));
    VelCmd.push_back(char(0x02));
    VelCmd.push_back(char(0x58));
    VelCmd.push_back(char(Vel/256));
    VelCmd.push_back(char(Vel%256));
    SetCRC16Modbus(VelCmd);
    ret = SPM->SendAndRecv(Com,VelCmd,Recv,1);
    if(ret != NoError) return ret;
    if(Recv.size() < 2 || (int)Recv[1] == 0x90) return CtrlError(SetVelCmdErr,Name);
    return CtrlError(NoError);
}

CtrlError Conveyor::ClearErr()
{
    CtrlError ret;
    QByteArray ClearErrCmd,Recv;
    ClearErrCmd.push_back(char(ID));
    ClearErrCmd.push_back(char(0x06));
    ClearErrCmd.push_back(char(0x00));
    ClearErrCmd.push_back(char(0x7C));
    ClearErrCmd.push_back(char(0x00));
    ClearErrCmd.push_back(char(0xBA));
    SetCRC16Modbus(ClearErrCmd);
    ret = SPM->SendAndRecv(Com,ClearErrCmd,Recv,1);
    if(ret != NoError) return ret;
    if(Recv.size() < 2 || (int)Recv[1] == 0x86) return CtrlError(ClearErrCmdErr,Name);
    return CtrlError(NoError);
}

void Conveyor::SetCRC16Modbus(QByteArray &Data)
{
    static const quint16 crc16Table[] =
    {
        0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
        0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
        0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
        0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
        0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
        0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
        0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
        0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
        0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
        0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
        0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
        0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
        0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
        0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
        0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
        0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
        0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
        0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
        0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
        0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
        0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
        0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
        0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
        0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
        0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
        0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
        0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
        0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
        0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
        0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
        0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
        0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
    };

    quint8 buf;
    quint16 crc16 = 0xFFFF;

    for(int i=0;i<Data.size();i++)
    {
        buf = Data[i] ^ crc16;
        crc16 >>= 8;
        crc16 ^= crc16Table[buf];
    }
    Data.push_back(char(crc16%256));
    Data.push_back(char(crc16/256));
    return;
}
