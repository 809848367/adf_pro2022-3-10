#include "MainWindow.h"
#include <QtCore>
#include <QtWidgets/QApplication>
#include "QiniOperator.h"
#include "MachineManager.h"
#include "DatabaseManager.h"
#include "HardwareManager.h"
#include <QMessageBox>
#include "CFlageManager.h"
#include "CFileOperator.h"
#include "LogManager.h"
#include "FilePathManager.h"
#include <QSplashScreen>
#include <QDateTime>
//崩溃生成DUMP
#include <windows.h>
#include <DbgHelp.h>
#pragma comment(lib,"Dbghelp.lib")



#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 

#define  SUCCESS_RETURN 0
#define  FAILED_RETURN -1
int Init();
int InitModule();
int InitModuleDataByTableName(HardwareInitData& tData);

/**
* @brief 保证程序单开
* @param key 提供唯一性的字符串，类似于ID
* @param tipsText 检测到多开时的提示信息
* @note 本实现基于共享内存
*/
void runAsSingleton(QString key, QString tipsText);

long  __stdcall CrashInfocallback(_EXCEPTION_POINTERS *pexcp);

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	//多开防护
	runAsSingleton("DebugFlow App", "");
	//生成dump
	::SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)CrashInfocallback);

	//  启动画面
	QPixmap pixmap(":/Img/ZDPC11.png");   //设置启动画面
	QSplashScreen splash(pixmap);
	splash.show();   //显示此启动图像
	a.processEvents();   //使得程序在显示启动画面的同时还能够响应其他事件


	a.setWindowIcon(QIcon(":/Img/21.ico"));
	LogManager::GetInstance().Init();
	if (SUCCESS_RETURN != Init())
		return FAILED_RETURN;
	if (SUCCESS_RETURN != InitModule())
		return FAILED_RETURN;

	MainWindow w;
	w.show();

	splash.finish(&w);  //程序启动画面完成
	return a.exec();
}

int Init()
{
	//加载ini
	theIniManger->ReadInitData();

	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	if (0 != theMachineManager.LoadProcessXML())
		return FAILED_RETURN;
	if (!theMachineManager.Init())
		return FAILED_RETURN;

#pragma region Read DataBase
	if (!theDbManager.InitDataBase())
		return FAILED_RETURN;
	if (!theDbManager.ReadAllTables())
		return FAILED_RETURN;
#pragma endregion 
	theFlageManager.InitFlag();
	theFileManager.Init();
	return SUCCESS_RETURN;
}

int InitModule()
{
	//if (!theDbManager.ReadAllTables())
	//	return FAILED_RETURN;


	HardwareInitData robot1, robot2;
	QList<HardwareInitData> initDataList;
	QList<S_Total> stotal = theDbManager.GetTotalInfo();
	for (int i = 0; i < stotal.size(); i++)
	{
		HardwareInitData tData;
		tData.Name = stotal[i].Name;
		tData.Type = stotal[i].ModuleName;
		CtrlError iRet = -1;
		iRet = InitModuleDataByTableName(tData);
		if (iRet != SUCCESS_RETURN)
			return FAILED_RETURN;

		iRet = HardwareManager::GetInstance()->InitModule(tData);
		if (iRet != 0)
		{

			QString msg = QString("%1模块初始化失败。\nerrorCode:0x%2;\nerror Msg:%3;")
				.arg(stotal[i].Name)
				.arg( (QString("%1").arg(iRet, 8, 16, QLatin1Char('0'))).toUpper())
				.arg(CtrlError::GetErrStr(iRet));
			QMessageBox::warning(NULL, "提示", msg, QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
			return FAILED_RETURN;
		}
	}
	return SUCCESS_RETURN;
}
int InitModuleDataByTableName(HardwareInitData& tData)
{
	if (tData.Name == "Robot1")
	{
		tData.Keys = theDbManager.m_Title_Robot1;
		for (int i = 0; i != theDbManager.m_Robot1.count();i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Robot1[i].IP);
			row.push_back(theDbManager.m_Robot1[i].CmdPort);
			row.push_back(theDbManager.m_Robot1[i].StatusPort);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Robot2")
	{
		tData.Keys = theDbManager.m_Title_Robot2;
		for (int i = 0; i != theDbManager.m_Robot2.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Robot2[i].IP);
			row.push_back(theDbManager.m_Robot2[i].CmdPort);
			row.push_back(theDbManager.m_Robot2[i].StatusPort);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "SerialPortManager")
	{
		//tData.Keys = theDbManager.m_Title_SerialPortManager;
		//for (int i = 0; i != theDbManager.m_SerialPortManager.count(); i++)
		//{
		//	QVector<QVariant> row;
		//	row.push_back(theDbManager.m_SerialPortManager[i].COM);
		//	row.push_back(theDbManager.m_SerialPortManager[i].BaudRate);
		//	row.push_back(theDbManager.m_SerialPortManager[i].DataBits);
		//	row.push_back(theDbManager.m_SerialPortManager[i].FlowControl);
		//	row.push_back(theDbManager.m_SerialPortManager[i].Parity);
		//	row.push_back(theDbManager.m_SerialPortManager[i].StopBits);
		//	tData.InitData.push_back(row);
		//}

		if (!theDbManager.SearchValueByTableQVariant(tData.Name, tData.Keys, tData.InitData))
			return FAILED_RETURN;
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Lighting1")
	{
		tData.Keys = theDbManager.m_Title_Lighting1;
		for (int i = 0; i != theDbManager.m_Lighting1.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Lighting1[i].Index);
			row.push_back(theDbManager.m_Lighting1[i].ComID);
			row.push_back(theDbManager.m_Lighting1[i].ID);
			row.push_back(theDbManager.m_Lighting1[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Camera1")
	{
		tData.Keys = theDbManager.m_Title_Camera1;
		for (int i = 0; i != theDbManager.m_Camera1.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Camera1[i].SN);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Lighting2")
	{
		tData.Keys = theDbManager.m_Title_Lighting2;
		for (int i = 0; i != theDbManager.m_Lighting2.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Lighting2[i].Index);
			row.push_back(theDbManager.m_Lighting2[i].ComID);
			row.push_back(theDbManager.m_Lighting2[i].ID);
			row.push_back(theDbManager.m_Lighting2[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Camera2")
	{
		tData.Keys = theDbManager.m_Title_Camera2;
		for (int i = 0; i != theDbManager.m_Camera2.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Camera2[i].SN);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "LightingUp")
	{
		tData.Keys = theDbManager.m_Title_LightingUp;
		for (int i = 0; i != theDbManager.m_LightingUp.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_LightingUp[i].Index);
			row.push_back(theDbManager.m_LightingUp[i].ComID);
			row.push_back(theDbManager.m_LightingUp[i].ID);
			row.push_back(theDbManager.m_LightingUp[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "CameraUp")
	{
		tData.Keys = theDbManager.m_Title_CameraUp;
		for (int i = 0; i != theDbManager.m_CameraUp.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_CameraUp[i].SN);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "CameraDown")
	{
		tData.Keys = theDbManager.m_Title_CameraDown;
		for (int i = 0; i != theDbManager.m_CameraDown.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_CameraDown[i].SN);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "LightingDown")
	{
		tData.Keys = theDbManager.m_Title_LightingDown;
		for (int i = 0; i != theDbManager.m_LightingDown.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_LightingDown[i].Index);
			row.push_back(theDbManager.m_LightingDown[i].ComID);
			row.push_back(theDbManager.m_LightingDown[i].ID);
			row.push_back(theDbManager.m_LightingDown[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "LightingNG")
	{
		tData.Keys = theDbManager.m_Title_LightingNG;
		for (int i = 0; i != theDbManager.m_LightingNG.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_LightingNG[i].Index);
			row.push_back(theDbManager.m_LightingNG[i].ComID);
			row.push_back(theDbManager.m_LightingNG[i].ID);
			row.push_back(theDbManager.m_LightingNG[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "CameraNG")
	{
		tData.Keys = theDbManager.m_Title_CameraNG;
		for (int i = 0; i != theDbManager.m_CameraNG.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_CameraNG[i].SN);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Conveyor1")
	{
		tData.Keys = theDbManager.m_Title_Conveyor1;
		for (int i = 0; i != theDbManager.m_Conveyor1.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Conveyor1[i].COM);
			row.push_back(theDbManager.m_Conveyor1[i].ID);
			row.push_back(theDbManager.m_Conveyor1[i].Vel);
			row.push_back(theDbManager.m_Conveyor1[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "Conveyor2")
	{
		tData.Keys = theDbManager.m_Title_Conveyor2;
		for (int i = 0; i != theDbManager.m_Conveyor2.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_Conveyor2[i].COM);
			row.push_back(theDbManager.m_Conveyor2[i].ID);
			row.push_back(theDbManager.m_Conveyor2[i].Vel);
			row.push_back(theDbManager.m_Conveyor2[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "PanaMotor1")
	{
		tData.Keys = theDbManager.m_Title_PanaMotor1;
		for (int i = 0; i != theDbManager.m_PanaMotor1.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_PanaMotor1[i].COM);
			row.push_back(theDbManager.m_PanaMotor1[i].ID);
			row.push_back(theDbManager.m_PanaMotor1[i].Vel);
			row.push_back(theDbManager.m_PanaMotor1[i].Acc);
			row.push_back(theDbManager.m_PanaMotor1[i].Dec);
			row.push_back(theDbManager.m_PanaMotor1[i].Org);
			row.push_back(theDbManager.m_PanaMotor1[i].PursePerRound);
			row.push_back(theDbManager.m_PanaMotor1[i].EncoderPerRound);
			row.push_back(theDbManager.m_PanaMotor1[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "PanaMotor2")
	{
		tData.Keys = theDbManager.m_Title_PanaMotor2;
		for (int i = 0; i != theDbManager.m_PanaMotor2.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_PanaMotor2[i].COM);
			row.push_back(theDbManager.m_PanaMotor2[i].ID);
			row.push_back(theDbManager.m_PanaMotor2[i].Vel);
			row.push_back(theDbManager.m_PanaMotor2[i].Acc);
			row.push_back(theDbManager.m_PanaMotor2[i].Dec);
			row.push_back(theDbManager.m_PanaMotor2[i].Org);
			row.push_back(theDbManager.m_PanaMotor2[i].PursePerRound);
			row.push_back(theDbManager.m_PanaMotor2[i].EncoderPerRound);
			row.push_back(theDbManager.m_PanaMotor2[i].SerialPortManager);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	else if (tData.Name == "IOControl")
	{
		tData.Keys = theDbManager.m_Title_IOControl;
		for (int i = 0; i != theDbManager.m_IOControl.count(); i++)
		{
			QVector<QVariant> row;
			row.push_back(theDbManager.m_IOControl[i].Name);
			row.push_back(theDbManager.m_IOControl[i].Type);
			row.push_back(theDbManager.m_IOControl[i].Index);
			row.push_back(theDbManager.m_IOControl[i].Src);
			row.push_back(theDbManager.m_IOControl[i].SrcType);
			row.push_back(theDbManager.m_IOControl[i].ID);
			tData.InitData.push_back(row);
		}
		return SUCCESS_RETURN;
	}
	//else if (tData.Name == "AlgoManager")
	else
	{
		if (!theDbManager.SearchValueByTableQVariant(tData.Name, tData.Keys, tData.InitData))
			return FAILED_RETURN;
		return SUCCESS_RETURN;
	}

	return FAILED_RETURN;
}

/**
* @brief 保证程序单开
* @param key 提供唯一性的字符串，类似于ID
* @param tipsText 检测到多开时的提示信息
* @note 本实现基于共享内存
*/
void runAsSingleton(QString key, QString tipsText)
{
	static QSharedMemory shm;
	if (key.isEmpty())
		key = QFileInfo(qApp->applicationFilePath()).fileName();
	shm.setKey(key);
	if (!shm.attach()) {
		shm.create(1);
		return;
	}
	QMessageBox::warning(nullptr, "警告", tipsText.isEmpty() ? "检测到程序已运行，即将退出！" : tipsText);
	exit(0);
}



long  __stdcall CrashInfocallback(_EXCEPTION_POINTERS *pexcp)
{
	QDateTime date;
	QString q_str = date.currentDateTime().toString("yyyy_MM_dd hh-mm-ss")+".DMP";
	//QString 转 LPCWSTR
	const QChar* szLogin = q_str.unicode();
	LPCWSTR c_str = (LPCWSTR)szLogin;

	//创建 Dump 文件
	HANDLE hDumpFile = ::CreateFile(
		//L"MEMORY.DMP",
		c_str,
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);
	if (hDumpFile != INVALID_HANDLE_VALUE)
	{
		//Dump信息
		MINIDUMP_EXCEPTION_INFORMATION dumpInfo;
		dumpInfo.ExceptionPointers = pexcp;
		dumpInfo.ThreadId = GetCurrentThreadId();
		dumpInfo.ClientPointers = TRUE;
		//写入Dump文件内容
		::MiniDumpWriteDump(
			GetCurrentProcess(),
			GetCurrentProcessId(),
			hDumpFile,
			MiniDumpNormal,
			&dumpInfo,
			NULL,
			NULL
		);
	}
	return 0;
}
