﻿#ifndef HARDWAREMODULE_H
#define HARDWAREMODULE_H

#include <QtCore>
#include "CtrlError.h"
struct HardwareInitData
{
    QString Name;                           //模块名称
    QString Type;                           //模块类型
    QVector<QString> Keys;                  //参数表键值
    QVector<QVector<QVariant>> InitData;    //参数表内容（先行后列）
};
struct HardwareCommData
{
    QString Station;
	int Identfication;
    QString OperatorName;
    QMap<QString,QVariant> Datas;
};

class HardwareModule : public QObject
{
    Q_OBJECT
public:
    explicit HardwareModule(QObject *parent = nullptr);
    virtual CtrlError Init(HardwareInitData InitData) = 0;
    virtual CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData) = 0;
signals:

};

#endif // HARDWAREMODULE_H
