#ifndef TEACHDATA_ROBOT_H
#define TEACHDATA_ROBOT_H

#include "TeachData.h"
#include "RobotDefine.h"
class TeachData_Robot: public TeachData
{
    Q_OBJECT
public:
	virtual int FromTable(QVector<QString> Keys, QVector<QVector<QVariant>> Data)override;
	virtual void ToTable(QVector<QString> &Keys, QVector<QVector<QVariant>> &Data)override;
public:
    explicit TeachData_Robot(QObject *parent = nullptr);
    TeachData_Robot(const TeachData_Robot &TeachData);

    int GetPoint(QString Name,RobotPoint &Point);
    QList<QPair<QString,RobotPoint>> GetPoints();
    int GetWay(QString Name,QVector<RobotPoint> &Way);
    QList<QPair<QString,QVector<RobotPoint>>> GetWays();
    //UI设参数用
    void SetPoint(QString Name,RobotPoint Point);
    void RemovePoint(QString Name);
    void SetWay(QString Name,QVector<RobotPoint> Way);
    void RemoveWay(QString Name);
    TeachData_Robot &operator =(const TeachData_Robot &TeachData);
private:
    QList<QPair<QString,RobotPoint>> Points;
    QList<QPair<QString,QVector<RobotPoint>>> Ways;
};


#endif // TEACHDATA_ROBOT_H
