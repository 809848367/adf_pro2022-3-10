﻿#ifndef ROBOT_H
#define ROBOT_H

#include <QtCore>
#include "HardwareModule.h"
#include "RobotCommandClient.h"
#include "RobotStatusClient.h"
#include "CtrlError.h"
class Robot : public HardwareModule
{
    Q_OBJECT
public:

    explicit Robot(QObject *parent = nullptr);
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    CtrlError Init(QString Name,QString IP,quint16 CmdPort,quint16 StatusPort);

    //Command
    RobotStatus GetStatus();
    CtrlError Reset();
    CtrlError SetEnable(bool Enable);
    CtrlError MovePosition(RobotPoint Point);
    CtrlError MovePositionByPose(RobotPoint Point);
    CtrlError MoveWay(QVector<RobotPoint> Points);
    CtrlError WaitArrive(RobotPoint Point, int TimeOut);
    CtrlError WaitArriveByPose(RobotPoint Point, int TimeOut);
    CtrlError SetOutput(int Index, bool out);

private:
    //API
    CtrlError inverseKinematic(QVector<double> Pose, QVector<double> Reference, QVector<double> &Pos);   //逆解函数
    CtrlError moveByJoint(QVector<double> Pos,double Speed);
    CtrlError moveByLine(QVector<double> Pos,double Speed);
    CtrlError clearAlarm();
    CtrlError stop();
    CtrlError syncMotorStatus();
    CtrlError set_servo_status(bool Enable);
    CtrlError clearPathPoint();
    CtrlError addPathPoint(QVector<double> Pos, double speed, int moveType, int smooth);
    CtrlError moveByPath();
    CtrlError setOutput(int Index, bool out);
    //Data
    QString Name;
    RobotCommandClient CmdClient;
    RobotStatusClient  StatusClient;
signals:
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00200000,
        NeedRemoteModeFirst     = Base + 0x001,
        RobotHasAlarm           = Base + 0x002,
        ParamNotRight           = Base + 0x003,
        RecvErrorData           = Base + 0x004,
        MoveByJointErr          = Base + 0x005,
        MoveByLineErr           = Base + 0x006,
        clearAlarmErr           = Base + 0x007,
        stopErr                 = Base + 0x008,
        syncMotorStatusErr      = Base + 0x009,
        set_servo_statusErr     = Base + 0x00a,
        clearPathPointErr       = Base + 0x00b,
        addPathPointErr         = Base + 0x00c,
        moveByPathErr           = Base + 0x00d,
        moveTypeNotExist        = Base + 0x00e,
        PoseSizeNotRight        = Base + 0x00f,
        WaitArriveTimeOut       = Base + 0x010,
        InitDataErr             = Base + 0x011,
        ExecuteDataNotRight     = Base + 0x012,
    };
    Q_ENUM(ErrorCode)
};

#endif // ROBOT_H
