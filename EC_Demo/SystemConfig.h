#pragma once

#include <QWidget>
#include "ui_SystemConfig.h"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTimer>

class SystemConfig : public QWidget
{
	Q_OBJECT

public:
	SystemConfig(QWidget *parent = Q_NULLPTR);
	~SystemConfig();
	void Init();
	void InitWindow();
	void InitConnection();
	void DataRefresh();

signals:
	void emit_startProc(QString procName);
	void emit_resetProc(QString procName);
public slots:
void RefreshTimerSlot();
void itemChanged(QStandardItem *item);
void Edit_Table_stateChanged(int state);
void RefreshDspProc(QList<QString> procNameList);

void Slot_ComboxStartProc(QString itemStr);
void Slot_ComboxResetProc(QString itemStr);

private:
	Ui::SystemConfig ui;
	QStandardItemModel* m_tableModel;
	QStandardItemModel* m_StartProcModule;
	QStandardItemModel* m_ResetProcModule;
	QTimer RefreshTimer;
	QString m_ResetProc;
	QString m_StartProc;

	bool m_bEditTab;
};
