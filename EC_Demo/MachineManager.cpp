#include "MachineManager.h"
#include "QiniOperator.h"
#include <QMutexLocker>
#include "DataAnalysis.h"

#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")
#endif
MachineManager::MachineManager(QObject *parent)
	: QObject(parent)
{
	m_MainProc = "";//启动按钮流程
	m_ResetProc = "";//复位流程
	m_RestInput = "";//复位IO
	m_StartInput = "";//启动IO
	m_PauseInput = "";//暂停IO
	m_StopInput = "";//停止IO
	m_bAtuo = false;//自动标志
	m_machineStatus = MachineStatus_WaitReset;
	////////////////////////////////
	TotalCount = 0;//总数
	OKCount = 0;//OK 品
	NGCount = 0;//NG 品
	PASSPercent = 0;// 良率
	FailurePrecent = 0;//不良率
	CycleTime = 0;//循环耗时 s

}
MachineManager theMachineManager;

MachineManager::~MachineManager()
{
}

int MachineManager::LoadProcessXML()
{
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	QString processPath;
	iniOperator.readStringValue("filePath", "ProcessPath", processPath);

	dataAnaly.clear();
	bool bRet = dataAnaly.AnalysisProgramXml(processPath);
	if (!bRet)
		return LoadProcessXMLError;
	return NoError;
}

int MachineManager::ReloadProcessXML(QString ProFile)
{
	dataAnaly.clear();
	bool bRet = dataAnaly.AnalysisProgramXml(ProFile);
	if (!bRet)
		return LoadProcessXMLError;
	foreach(CParentProcess *proc_obj, m_procList)
	{
		disconnect(proc_obj, &CParentProcess::SignalExitExceptionFlag, this, &MachineManager::MachineError);
		delete proc_obj;
		proc_obj = nullptr;
	}
	m_procList.clear();
	QStringList procNameList;
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		CParentProcess* proc_obj = new CParentProcess;
		proc_obj->Set_Proc(proc);
		m_procList.push_back(proc_obj);
		connect(proc_obj, &CParentProcess::SignalExitExceptionFlag, this, &MachineManager::MachineError);
	}

	return NoError;
}

bool MachineManager::Init()
{
	//
	static bool isFirst = true;
	if (isFirst)
	{
		QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
		for (int i = 0; i < ErrorCodeEnum.keyCount(); i++)
		{
			CtrlError::RegisterErr(ErrorCodeEnum.value(i), ErrorCodeEnum.key(i));
		}
		isFirst = false;
	}

	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.readStringValue("MachineCfg", "StartProcess", m_MainProc);
	iniOperator.readStringValue("MachineCfg", "RestProcess", m_ResetProc);

	iniOperator.readIntValue("count", "TotalCount", TotalCount);
	iniOperator.readIntValue("count", "OKCount", OKCount);
	iniOperator.readIntValue("count", "NGCount", NGCount);
	iniOperator.readDoubleValue("count", "PASSPercent", PASSPercent);
	iniOperator.readDoubleValue("count", "FailurePrecent", FailurePrecent);
	iniOperator.readDoubleValue("count", "CycleTime", CycleTime);

	//流程加载
	QStringList procNameList;
	foreach(ProcessInfo *proc, dataAnaly.m_CurProgram.processInfoList)
	{
		CParentProcess* proc_obj = new CParentProcess;
		proc_obj->Set_Proc(proc);
		m_procList.push_back(proc_obj);
		connect(proc_obj, &CParentProcess::SignalExitExceptionFlag,this, &MachineManager::MachineError);
	}

	m_bExit = false;
	//状态检测刷新
	//m_status_thread = new QThread;
	//connect(this, &MachineManager::SignalStatus_Check, this, &MachineManager::tRun_Check_Status);
	//this->moveToThread(m_status_thread);
	//m_status_thread->start();
	//emit SignalStatus_Check();

	return true;
}

void MachineManager::DeInit()
{
	MachineExit();
	//if (m_status_thread->isRunning())
	//{
	//	m_bExit = true;
	//	while (!m_bExit)	QThread::msleep(10);
	//	m_status_thread->quit();
	//	m_status_thread->wait();
	//	SignalMachineMsg("Machine exit",0);
	//}


}

void MachineManager::MsgRegister()
{
	emit Signal_Send_Proc_List(m_procList);
}

bool MachineManager::isAtuo()
{
	QMutexLocker locker(&m_autoMutex);
	return m_bAtuo;
}

void MachineManager::SetAtuo(bool bAuto)
{
	QMutexLocker locker(&m_autoMutex);
	m_bAtuo = bAuto;
	emit bAutoChange(m_bAtuo);
}

MachineManager::MachineStatus MachineManager::GetMachineStatus()
{
	//QMutexLocker locker(&m_stateMutex);
	return m_machineStatus;
}

QString MachineManager::GetResetInput()
{
	return m_RestInput;
}

QString MachineManager::GetStartInput()
{
	return m_StartInput;
}

QString MachineManager::GetPauseInput()
{
	return m_PauseInput;
}

QString MachineManager::GetStopInput()
{
	return m_StopInput;
}

QList<CParentProcess*> MachineManager::GetProcList()
{
	return m_procList;
}

void MachineManager::ResetCurrentState()
{
	//m_procList
	foreach(CParentProcess* proc,m_procList)
	{
		proc->SetAndWaitThreadExit();
	}
}

CtrlError MachineManager::ResetProcByName(QString procName)
{
	//m_procList
	foreach(CParentProcess* proc, m_procList)
	{
		if (proc->GetProcName() == procName)
		{
			proc->SetAndWaitThreadExit();
			proc->SetStepIndex(0);
			return NoError;
		}
	}
	return NotFindProc;
}

void MachineManager::SetResetInput(QString input)
{
	m_RestInput = input;
}

void MachineManager::SetStartInput(QString input)
{
	m_StartInput = input;
}

void MachineManager::SetPauseInput(QString input)
{
	m_PauseInput = input;
}

void MachineManager::SetStopInput(QString input)
{
	m_StopInput = input;
}

void MachineManager::SetMachineStatus(MachineStatus Status)
{
	QMutexLocker locker(&m_stateMutex);
	//switch (Status)
	//{
	//case MachineManager::MachineStatus_Idle:
	//	//m_bAtuo = false;//手动
	//	break;
	//case MachineManager::MachineStatus_Reset:
	//{
	//	if (m_machineStatus == MachineStatus_Work ||
	//		m_machineStatus == MachineStatus_Pause)
	//		return;
	//	MachineReset();
	//}
	//break;
	//case MachineManager::MachineStatus_Work:
	//{
	//	m_bAtuo = true;//自动运行
	//	if (m_machineStatus != MachineStatus_Idle)
	//		return;
	//	MachineStrat();
	//}
	//break;
	//case MachineManager::MachineStatus_Pause:
	//{
	//	m_bAtuo = true;//自动运行
	//	if (m_machineStatus != MachineStatus_Work)
	//		return;
	//	MachinePause();
	//}
	//break;
	//case MachineManager::MachineStatus_Continue:
	//{
	//	m_bAtuo = true;//自动运行
	//	if (m_machineStatus != MachineStatus_Pause)
	//		return;
	//	MachineContinue();
	//}
	//break;
	//case MachineManager::MachineStatus_Stop:
	//{
	//	m_bAtuo = true;//自动运行
	//	if (m_machineStatus != MachineStatus_Work&& m_machineStatus != MachineStatus_Pause)
	//		return;
	//	MachineStop();
	//	m_machineStatus = MachineStatus_Stop_Going;
	//	return;
	//}
	//break;
	//case MachineManager::MachineStatus_Error:
	//	MachineStop();
	//	m_machineStatus = MachineStatus_Stop_Going;
	//	return;
	//	break;
	//case MachineManager::MachineStatus_WaitReset:
	//	//m_bAtuo = false;
	//	break;
	//default:
	//	break;
	//}
	m_machineStatus = Status;
}

void MachineManager::SetResetProcName(QString procName)
{
	m_ResetProc = procName;
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("MachineCfg", "RestProcess", procName);
}

void MachineManager::SetStartProcName(QString procName)
{
	m_MainProc = procName;
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("MachineCfg", "StartProcess", procName);
}

void MachineManager::SetProcList(QList<CParentProcess*> procList)
{
	m_procList = procList;
	emit Signal_Send_Proc_List(m_procList);
}

int MachineManager::GetProcStatusByName(QString procName)
{
	if (m_procList.count() == 0)
		return EmptyProc;
	foreach(CParentProcess * proc, m_procList)
	{
		if (proc->GetProcName() == procName)
		{
			return proc->GetProcState();
		}
	}
	return NotFindProc;
}

int MachineManager::GetProcExceptionByName(QString procName)
{
	if (m_procList.count() == 0)
		return EmptyProc;
	foreach(CParentProcess * proc, m_procList)
	{
		if (proc->GetProcName() == procName)
		{
			if (proc->Get_Exception_End())
				return ProcExceptionExit;
			else 
				return NoError;
		}
	}
	return NotFindProc;
}

CtrlError MachineManager::MachineReset()
{
	if (m_procList.size() == 0)	return EmptyProc;
	if (MachineStatus_Work == GetMachineStatus()|| MachineStatus_Pause == GetMachineStatus())
		return CurrentStateNotAllowExecute;
	if (m_ResetProc.isEmpty())	return EmptyProc;
	QMap<QString, QString> procState;
	if (NoError != CheckAllProcessIsWork(procState))
	{
		SignalMachineMsg("Execute CheckAllProcessIsWork failed. ", ErrorMsg);
		return CheckProcStateError;
	}
	if (procState.count() > 0)
	{
		foreach(QString procName, procState.keys())
		{
			QString msg = procName + "state:" + procState[procName];
			qDebug() << msg;
			SignalMachineMsg(msg, ErrorMsg);
		}
		return CtrlError(CurrentStateNotAllowExecute);
	}

	bool bFind = false;
	for (int i = 0; i < m_procList.size(); i++)
	{
		if (m_procList.at(i)->GetProcName() == m_ResetProc)
		{
			bFind = true;
			CParentProcess::ProcessState state = m_procList.at(i)->GetProcState();
			if (CParentProcess::Process_Status_Work == state|| CParentProcess::Process_Status_Pause == state)
			{
				emit SignalMachineMsg(CtrlError(PorcNotInIdle),ErrorMsg) ;
				return PorcNotInIdle;
			}
				
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Start);
			//break;
		}
	}
	if (!bFind) return NotFindProc;

	m_machineStatus = MachineStatus_Reset;
	m_bExit = false;
	//foreach(CParentProcess * proc, m_procList)
	//{
	//	proc->Set_Proc_Status(CParentProcess::Process_Status_Idle);
	//}
	return NoError;
}

CtrlError MachineManager::MachineStop()
{
	if (m_procList.size() == 0)
		return EmptyProc;

	if (MachineStatus_Work != GetMachineStatus() && MachineStatus_Pause != GetMachineStatus())
		return CurrentStateNotAllowExecute;

	QMap<QString, QString> procState;
	if (NoError != CheckAllProcessIsWork(procState))
	{
		SignalMachineMsg("Execute CheckAllProcessIsWork failed. ", ErrorMsg);
		return CheckProcStateError;
	}
	if (procState.count() == 0)	return CtrlError(NoProcWork);

	for (int i = 0; i < m_procList.size(); i++)
	{
		CParentProcess::ProcessState state = m_procList.at(i)->GetProcState();
		if (state == CParentProcess::Process_Status_Work || state == CParentProcess::Process_Status_Pause)
		{
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Stop);
		}
		//if (m_procList.at(i)->GetProcName() != m_MainProc && m_procList.at(i)->GetProcName() != m_ResetProc)
		//{
		//	if (m_procList.at(i)->GetProcState() == CParentProcess::Process_Status_Idle)
		//		continue;
		//	m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Stop);
		//}
	}
	
	return NoError;
}

CtrlError MachineManager::MachineErrorStop()
{
	if (m_procList.size() == 0)
		return CommondFailed;
	if (MachineStatus_Work != GetMachineStatus() && MachineStatus_Pause != GetMachineStatus())
		return CurrentStateNotAllowExecute;
	for (int i = 0; i < m_procList.size(); i++)
	{
		CParentProcess::ProcessState state = m_procList.at(i)->GetProcState();
		if (state == CParentProcess::Process_Status_Work || state == CParentProcess::Process_Status_Pause)
		{
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_ErrorEnd);
		}
		//m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Stop);
	}
	return NoError;
}

CtrlError MachineManager::MachinePause()
{
	if (m_procList.size() == 0)
		return EmptyProc;
	if (MachineStatus_Work != GetMachineStatus())
		return CurrentStateNotAllowExecute;
	for (int i = 0; i < m_procList.size(); i++)
	{
		CParentProcess::ProcessState state = m_procList.at(i)->GetProcState();
		if (state == CParentProcess::Process_Status_Work)
		{
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Pause);
		}
		//if (m_procList.at(i)->GetProcName() != m_MainProc&& m_procList.at(i)->GetProcName() != m_ResetProc)
		//	m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Pause);
	}
	//m_machineStatus = MachineStatus_Pause;
	return NoError;
}

CtrlError MachineManager::MachineContinue()
{
	if (m_procList.size() == 0)
		return EmptyProc;
	if (MachineStatus_Pause != GetMachineStatus())
	{
		return CurrentStateNotAllowExecute;
	}
	for (int i = 0; i < m_procList.size(); i++)
	{
		CParentProcess::ProcessState state = m_procList.at(i)->GetProcState();
		if (state == CParentProcess::Process_Status_Pause)
		{
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Continue);
		}
		//if (m_procList.at(i)->GetProcName() != m_MainProc || m_procList.at(i)->GetProcName() != m_ResetProc)
		//	m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Continue);
	}
	//m_machineStatus = MachineStatus_Work;
	return NoError;
}

CtrlError MachineManager::MachineStart()
{
	if (m_procList.size() == 0)	return CommondFailed;
		
	if (MachineStatus_Idle != GetMachineStatus())	return CtrlError(CurrentStateNotAllowExecute);

	QMap<QString, QString> procState;
	if (0!= CheckAllProcessIsWork(procState))	return CtrlError(CheckProcStateError);

	if (procState.count()>0)
	{
		foreach(QString procName, procState.keys())
		{
			QString msg = procName + "state:" + procState[procName];
			qDebug() << msg;
			SignalMachineMsg(msg,ErrorMsg);
		}
		return CtrlError(CurrentStateNotAllowExecute);
	}
	bool bFind = false;
	for (int i = 0; i < m_procList.size(); i++)
	{
		if (m_procList.at(i)->GetProcName() == m_MainProc)
		{
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Start);
			bFind = true;
			break;
		}
	}
	if (!bFind)	return CtrlError(NotFindProc);
	m_machineStatus = MachineStatus_Work;
	return CtrlError(NoError);
}

int MachineManager::MachineError(QString msg, int ErrorType)
{
	MachineErrorStop();
	m_machineStatus = MachineStatus_Error_Going;
	return NoError;
}

void MachineManager::SetExit(bool exitFlag)
{
	m_bExit = exitFlag;
}

bool MachineManager::GetExit()
{
	return m_bExit;
}

int MachineManager::MachineExit()
{
	for (int i = 0; i < m_procList.size(); i++)
	{
		m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Stop);
	}
	m_bExit = true;
	return NoError;
}

void MachineManager::tRun_Check_Status()
{
	while (!m_bExit)
	{
		QThread::msleep(100);
		if (m_procList.size() == 0)
		{
			m_machineStatus = MachineStatus_WaitReset;
			continue;
		}
		switch (m_machineStatus)
		{
		case MachineManager::MachineStatus_Idle:
			emit Signal_Machine_Statu("空闲");
			break;
		case MachineManager::MachineStatus_Reset:
			bool bRet;
			foreach(CParentProcess* proc, m_procList)
			{
				if (proc->GetProcName() == m_ResetProc)
				{
					while (proc->GetProcState() == CParentProcess::Process_Status_Work)
					{
						emit Signal_Machine_Statu("复位中...");
						QThread::msleep(10);
					}
					bRet = proc->Get_Exception_End();
				}
			}
			if (!bRet)
			{
				m_machineStatus = MachineStatus_Idle;
				emit Signal_Machine_Statu("空闲");
			}
			else
			{
				emit Signal_Machine_Statu("待复位");
			}
			break;
		case MachineManager::MachineStatus_Work:
			emit Signal_Machine_Statu("工作");
			foreach(CParentProcess* proc, m_procList)
			{
				if (proc->GetProcName() == m_MainProc)
				{
					if (proc->GetProcState() == CParentProcess::Process_Status_End)
					{
						m_machineStatus = MachineStatus_Idle;
					}
				}
			}
			break;
		case MachineManager::MachineStatus_Work_End:
			m_machineStatus = MachineStatus_Idle;
			break;
		case MachineManager::MachineStatus_Pause:
			emit Signal_Machine_Statu("暂停");
			break;
		case MachineManager::MachineStatus_Continue:
			emit Signal_Machine_Statu("工作");
			m_machineStatus = MachineStatus_Work;
			break;
		case MachineManager::MachineStatus_Stop_Going:
			for (int i = 0; i < m_procList.size(); i++)
			{
				if (m_procList.at(i)->GetProcName() == m_MainProc)
				{
					while (m_procList.at(i)->GetProcState() != CParentProcess::Process_Status_Idle)
						QThread::msleep(100);
				}
			}
			emit Signal_Machine_Statu("停止");
			m_machineStatus = MachineStatus_Stop;
			break;
		case MachineStatus_Error_Going:
			emit Signal_Machine_Statu("故障停止中...");
			for (int i = 0; i < m_procList.size(); i++)
			{
				while (m_procList.at(i)->GetProcState() != CParentProcess::Process_Status_Idle)
					QThread::msleep(100);
			}
			
			m_machineStatus = MachineStatus_Error;
			break;
		case MachineManager::MachineStatus_Error:
			emit Signal_Machine_Statu("故障停止");
			break;
		case MachineManager::MachineStatus_WaitReset:
			emit Signal_Machine_Statu("待复位");
			break;
		default:
			break;
		}

	}
	m_bExit = false;
}



void MachineManager::ClearCTData()
{
	QMutexLocker locker(&m_mutexCount);
	theMachineManager.TotalCount = 0;//总数
	theMachineManager.OKCount = 0;//OK 品
	theMachineManager.NGCount = 0;//NG 品
	theMachineManager.PASSPercent = 0;// 良率
	theMachineManager.FailurePrecent = 0;//不良率
	theMachineManager.CycleTime = 0;//循环耗时 s
	theMachineManager.totalTime = 0;
	//theMachineManager.RefreshCT = 1;
	m_Map_StartTime.clear();
	m_Map_EndTime.clear();

	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("count", "TotalCount", TotalCount);
	iniOperator.setValue("count", "OKCount", OKCount);
	iniOperator.setValue("count", "NGCount", NGCount);
	iniOperator.setValue("count", "PASSPercent", PASSPercent);
	iniOperator.setValue("count", "FailurePrecent", FailurePrecent);
	iniOperator.setValue("count", "CycleTime", CycleTime);
}

void MachineManager::AddOneProduct()
{
	QMutexLocker locker(&m_mutexCount);
	TotalCount++;

	if (TotalCount == 0)
	{
		PASSPercent    = 0;
		FailurePrecent = 0;
		OKCount = 0;
		NGCount = 0;
		CycleTime = 0;
	}
	else
	{
		PASSPercent = static_cast<double>(OKCount)/ static_cast<double>(TotalCount);
		FailurePrecent = static_cast<double>(OKCount) / static_cast<double>(TotalCount);
	}
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("count", "TotalCount", TotalCount);
	iniOperator.setValue("count", "OKCount", OKCount);
	iniOperator.setValue("count", "NGCount", NGCount);
	iniOperator.setValue("count", "PASSPercent", PASSPercent);
	iniOperator.setValue("count", "FailurePrecent", FailurePrecent);
	iniOperator.setValue("count", "CycleTime", CycleTime);
}

void MachineManager::AddOneOKProduct()
{
	QMutexLocker locker(&m_mutexCount);
	TotalCount++;
	OKCount++;

	if (TotalCount == 0)
	{
		PASSPercent = 0;
		FailurePrecent = 0;
		OKCount = 0;
		NGCount = 0;
		CycleTime = 0;
	}
	else
	{
		PASSPercent = static_cast<double>(OKCount) / static_cast<double>(TotalCount);
		FailurePrecent = static_cast<double>(OKCount) / static_cast<double>(TotalCount);
	}
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("count", "TotalCount", TotalCount);
	iniOperator.setValue("count", "OKCount", OKCount);
	iniOperator.setValue("count", "NGCount", NGCount);
	iniOperator.setValue("count", "PASSPercent", PASSPercent);
	iniOperator.setValue("count", "FailurePrecent", FailurePrecent);
	iniOperator.setValue("count", "CycleTime", CycleTime);
}

void MachineManager::AddOneNGProduct()
{
	QMutexLocker locker(&m_mutexCount);
	TotalCount++;
	NGCount++;

	if (TotalCount == 0)
	{
		PASSPercent = 0;
		FailurePrecent = 0;
		OKCount = 0;
		NGCount = 0;
		CycleTime = 0;
	}
	else
	{
		PASSPercent = static_cast<double>(OKCount) / static_cast<double>(TotalCount);
		FailurePrecent = static_cast<double>(OKCount) / static_cast<double>(TotalCount);
	}
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("count", "TotalCount", TotalCount);
	iniOperator.setValue("count", "OKCount", OKCount);
	iniOperator.setValue("count", "NGCount", NGCount);
	iniOperator.setValue("count", "PASSPercent", PASSPercent);
	iniOperator.setValue("count", "FailurePrecent", FailurePrecent);
	iniOperator.setValue("count", "CycleTime", CycleTime);
}

void MachineManager::RefrshCTProduct(QString ctStr)
{
	QMutexLocker locker(&m_mutexCount);
	CycleTime = ctStr.toDouble();
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.setValue("count", "CycleTime", CycleTime);
}

CtrlError MachineManager::CheckAllProcessIsIdle(QMap<QString, QString>& procState)
{
	if (m_procList.size() == 0)
		return CtrlError(NoError);
	procState.clear();
	QMetaEnum metaEnum = QMetaEnum::fromType<CParentProcess::ProcessState>();
	for (int i = 0; i < m_procList.size(); i++)
	{
		CParentProcess::ProcessState state =  m_procList.at(i)->GetProcState();
		if (m_procList.at(i)->GetProcState() != CParentProcess::Process_Status_Idle)
		{
			procState.insert(m_procList[i]->GetProcName(),QString(metaEnum.valueToKey(state)));
		}
	}
	return CtrlError(NoError);
}

CtrlError MachineManager::CheckAllProcessIsWork(QMap<QString, QString>& procState)
{
	if (m_procList.size() == 0)
		return CtrlError(NoError);
	procState.clear();
	QMetaEnum metaEnum = QMetaEnum::fromType<CParentProcess::ProcessState>();
	for (int i = 0; i < m_procList.size(); i++)
	{
		CParentProcess::ProcessState state = m_procList.at(i)->GetProcState();
		if (state == CParentProcess::Process_Status_Work || state == CParentProcess::Process_Status_Pause)
		{
			procState.insert(m_procList[i]->GetProcName(), QString(metaEnum.valueToKey(state)));
		}
	}
	return CtrlError(NoError);
}

int MachineManager::ProcessPause()
{
	if (m_machineStatus != MachineStatus_Work)
		return CommondFailed;

	if (m_procList.size() == 0)
		return EmptyProc;
	for (int i = 0; i < m_procList.size(); i++)
	{
		if (CParentProcess::Process_Status_Work == m_procList.at(i)->GetProcState())
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Pause);
	}
	m_machineStatus = MachineStatus_Pause;
	return NoError;
}

int MachineManager::ProcessStop()
{
	if (m_machineStatus != MachineStatus_Work)
		return CommondFailed;

	if (m_procList.size() == 0)
		return EmptyProc;
	for (int i = 0; i < m_procList.size(); i++)
	{
		if (CParentProcess::Process_Status_Work == m_procList.at(i)->GetProcState())
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Stop);
	}
	m_machineStatus = MachineStatus_Pause;
	return NoError;
}

int MachineManager::ProcessContinue()
{
	if (m_machineStatus != MachineStatus_Pause)
		return CommondFailed;

	if (m_procList.size() == 0)
		return EmptyProc;
	for (int i = 0; i < m_procList.size(); i++)
	{
		if (CParentProcess::Process_Status_Work == m_procList.at(i)->GetProcState())
			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Continue);
	}
	m_machineStatus = MachineStatus_Work;
	return NoError;
}

CtrlError MachineManager::StartProcByName(QString procName)
{
	if (m_procList.size() == 0)
		return EmptyProc;
	for (int i = 0; i < m_procList.size(); i++)
	{
		if (m_procList.at(i)->GetProcName() == procName)
		{
			if (CParentProcess::Process_Status_Idle != m_procList.at(i)->GetProcState())
				return CtrlError(PorcNotInIdle) ;

			m_procList.at(i)->Set_Proc_Status(CParentProcess::Process_Status_Start);
			break;
		}
	}
	return CtrlError(NoError);
}
