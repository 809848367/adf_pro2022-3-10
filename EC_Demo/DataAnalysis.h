#pragma once
#include <QString>
#include <QList>
#include <QFile>
#include <QtXml/QtXml>
#include <QStyleFactory>
#include <QStringList>
#include <QtCore>
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 

//************************流程使用区*************************
//操作类型表
enum Operator_Type
{
	Operator_Type_Empty = 0,		//空操作
	Operator_Type_Axis_Move,		//工站中单轴操作
	Operator_Type_Axis_OffSet_Move,	//单轴偏移量运动
	Operator_Type_Axis_Wait_Stop,	//等待轴停止
	Operator_Type_Axis_Check_Pos,	//单轴检测点位是否到位 

	Operator_Type_Station_Move,		//工站运动
	Operator_Type_Station_Wait_Stop,//等待轴停止
	Operator_Type_Station_Check_Pos,//工站检测到位

	Operator_Type_Virtual_Station,	//虚拟工站
	Operator_Type_WaitInput,		//Input		信号等待
	Operator_Type_Write_OutPut,		//Output	写入


	//虚拟工站
	Operator_Type_Start_Flow,			//流程开启
	Operator_Type_Wait_Flow_Start,		//等待流程开启
	Operator_Type_Wait_Flow_End,		//等待流程结束 并且获取结束状态
	Operator_Type_Flow_End,				//流程结束 自动退出
	Operator_Type_Write_Flag,			//标志位写入
	Operator_Type_Wait_Flag,			//等待标志位 
	Operator_Type_WriteTemp_Flag,		//将整数类型写入flag

	Operator_Type_SelfAddFlag,			//标志位自加1
	Operator_Type_SelfMinuFlag,			//标志位自减1

	Operator_Type_Flag_Jump,			//根据标志位内容跳转
	Operator_Type_SwitchFlagJump,		//根据标志位多重跳转
	Operator_Type_MulityFlagLogicalAndJump,	//多个标志位逻辑与 为真跳转 参数2 为假跳转 参数3
	Operator_Type_MulityFlagLogicalORJump,	//多个标志位逻辑或 为真跳转 参数2 为假跳转 参数3
	Operator_Type_WaitAndSet_Flag,		//等待标志位的值然后写入 
	//Operator_Type_WaitFlagSetOtherFlag,	//根据Flag值设置新Flag值
	Operator_Type_Clear_Flag,			//标志位清除重置
	Operator_Type_TestDebug,			//输出一段内容用做测试流程
	Operator_Type_ErrorTestDebug,			//输出一段内容用做测试流程 触发错误
	Operator_Type_ShowMulityTempImg,		//显示图片多张图片不带结果
	Operator_Type_ShowMulityTempImgWithResult,	//显示多张图片,带结果

	Operator_Type_CreateSN,				//生成一个SN号
	//Operator_Type_DeleteSN,			//删除一个SN号
	Operator_Type_GetSN,				//获取SN
	Operator_Type_CreateStartTime,		//创建一个时间节点
	Operator_Type_ElapsedFromTwoTime,	//计算两个时间节点 存储到临时变量中
	Operator_Type_ElapsedFromTime,		//计算当前到上一个时间节点过去时间 存储到临时变量中

	Operator_Type_Msg_Box,				//弹框卡顿 
	Operator_Type_Delay,				//延时
	Operator_Type_Jump,					//步骤跳转
	Operator_Type_WriteTempString,		//写入临时内容 map
	Operator_Type_ReadTempString,		//读取临时内容 map
	Operator_Type_NotContain,			//错误操作

	Operator_Type_RefreshCTWithTowTimePoint,//使用两个时间点刷新CT
	Operator_Type_AddOneProduct,		//添加一个物料
	Operator_Type_AddOneOkProduct,	//添加一个OK物料
	Operator_Type_AddOneNGProduct,	//添加一个NG物料
	Operator_Type_ClearCount,			//重置计数
	Operator_Type_ShowInfo,			//消息显示
	Operator_Type_DeleteDir,		//删除文件夹下文件


	//Robot operator
	Operator_Type_Robot_GetStatus,
	Operator_Type_Robot_MovePosition,
	Operator_Type_Robot_ComparePositionSetFlag,//工站  点位比较功能 当前点位和某个点位(param1)比较 range(param2),在范围 param3 flag设置值 ，不在范围param4 flag 设置值 ，POS flag名称 
	Operator_Type_Robot_Reset,
	Operator_Type_Robot_SetEnable,
	Operator_Type_Robot_MovePositionWithIMS,
	Operator_Type_Robot_MovePositionWithIMS6,
	Operator_Type_Robot_WaitArrive,

	//Lighting operator
	Operator_Type_Lighting_SetLight,
	Operator_Type_Lighting_SetAllLight,
	Operator_Type_Lighting_GetLight,
	Operator_Type_Lighting_GetAllLight,
	Operator_Type_Lighting_WaitLight,
	Operator_Type_Lighting_WaitAllLight,
	Operator_Type_Lighting_SetLightAndWait,
	Operator_Type_Lighting_SetAllLightAndWait,

	//Conveyor	operator
	Operator_Type_Conveyor_Start,
	Operator_Type_Conveyor_Stop,
	Operator_Type_Conveyor_GetStatus,
	Operator_Type_Conveyor_SetEnable,
	Operator_Type_Conveyor_SetVel,
	Operator_Type_Conveyor_ClearErr,
	//Camera	operator
	Operator_Type_Camera_Open,
	Operator_Type_Camera_Close,
	Operator_Type_Camera_GetStatus,
	Operator_Type_Camera_GetImage,
	Operator_Type_Camera_GetImageAndSave,
	Operator_Type_Camera_GetImage_Save_Disp,//拍照存图，显示
	Operator_Type_Camera_GetImage2,
	Operator_Type_Camera_GetImageAndSave2,
	Operator_Type_Camera_GetImageAndSaveRecord,	//拍照存图 保留图片  key,面号 路径  提供数据内部存储
	Operator_Type_Camera_GetImageAndSaveRecord2,//拍照存图 保留路径

	//PanaMotor	operator
	Operator_Type_PanaMotor_MovePos,
	Operator_Type_PanaMotor_MoveOffset,
	Operator_Type_PanaMotor_ComparePos,//工站  点位比较功能 当前点位和某个点位(param1)比较 range(param2),在范围 param3 flag设置值 ，不在范围param4 flag 设置值 ，POS flag名称 
	Operator_Type_PanaMotor_Stop,
	Operator_Type_PanaMotor_GetPos,
	Operator_Type_PanaMotor_GetStatus,
	Operator_Type_PanaMotor_SetEnable,
	Operator_Type_PanaMotor_SetVel,//设置电机速度
	Operator_Type_PanaMotor_WaitArrive,
	Operator_Type_PanaMotor_WaitStop,//等待轴停止
	Operator_Type_PanaMotor_Reset,

	//IOControl	operator
	Operator_Type_IOControl_SetOutPut,
	Operator_Type_IOControl_SetOutPuts,
	Operator_Type_IOControl_GetInPut,
	Operator_Type_IOControl_GetInPutAndSetFlag,//读取flag一段时间 设置标志位
	Operator_Type_IOControl_GetInPutJumpStep,//根据读取Input 步骤跳转
	Operator_Type_IOControl_GetInPuts,
	Operator_Type_IOControl_GetOutPut,
	Operator_Type_IOControl_GetOutPuts,
	Operator_Type_IOControl_GetIONames,
	Operator_Type_IOControl_WatiInput,//等待一个信号 超时退出
	Operator_Type_IOControl_ContinueCheck,//持续监测一个信号，如果与设定值一致则报错


	//AlgoManager
	Operator_Type_AlgoManager_SendCmd,
	Operator_Type_AlgoManager_SendCmdPOS,
	Operator_Type_AlgoManager_SendCmdSRC,
	Operator_Type_AlgoManager_SendCmdMOON,
	Operator_Type_AlgoManager_SendCmdIMS,
	Operator_Type_AlgoManager_SendCmdIMSMulityADFOnePic,//兼容ADF18 数据
	Operator_Type_AlgoManager_SendCmdIMSMulity,//extend SendIMS
	Operator_Type_AlgoManager_SetBarCode,
	Operator_Type_AlgoManager_ChcekFinish,
	Operator_Type_AlgoManager_WaitFinish,
	Operator_Type_AlgoManager_WaitResult,//等待IMS 结果
	Operator_Type_AlgoManager_WaitResultSetFlag,//等待IMS结果 设置Flag
	Operator_Type_AlgoManager_WaitResultSetFlagADF,//等待IMS结果 设置Flag forADF
	Operator_Type_AlgoManager_WaitResultSetFlagByResultInfoMulity,//等待IMS结果 根据返回值的结果设置标志位

	Operator_Type_AlgoManager_GetResult,
	Operator_Type_AlgoManager_GetAllResult,//获取所有面的结果
	Operator_Type_AlgoManager_GetAllResultSetFlag,//获取所有面的结果
	Operator_Type_AlgoManager_Complete,//图片转移
	Operator_Type_AlgoManager_LoadGolden,//加载golden

	//EPSON_Robot
	//Operator_Type_EPSON_Robot_SendCmd,//发送命令
	Operator_Type_EPSON_Robot_SendCmdAndRecv,
	Operator_Type_EPSON_Robot_WaitRobotCmdSetFlag,//通过robot 命令进行触发操作
	//PLC
	Operator_Type_PLC_ReadPort,//读信号
	Operator_Type_PLC_WritePort,//写信号
	Operator_Type_PLC_WaitPort,//等待信号
	//ETH 
	Operator_Type_ETH_PLC_Connect,//读信号
	Operator_Type_ETH_PLC_Disconnect,//读信号
	Operator_Type_ETH_PLC_ReadPortInt,//读信号
	Operator_Type_ETH_PLC_WritePortInt,//写信号
	Operator_Type_ETH_PLC_ReadPortFloat,//读信号
	Operator_Type_ETH_PLC_WritePortFloat,//写信号

	Operator_Type_ETH_PLC_WaitPortInt,//等待信号
	Operator_Type_ETH_PLC_WaitPortFloat,//等待信号
	Operator_Type_ETH_PLC_WritePortAndWaitTimesInt,//写入信号并且等待信号，失败次数设置·
	Operator_Type_ETH_PLC_WritePortAndWaitTimesFloat,//写入信号并且等待信号，失败次数设置·

	Operator_Type_ETH_PLC_CheckProtIntSetFlag,//检测信号设置flag
	Operator_Type_ETH_PLC_CheckProtFloatSetFlag,//检测信号设置flag
	Operator_Type_ETH_PLC_WriteTempDataMulityInt,//将点位写入PLC
	Operator_Type_ETH_PLC_WriteTempDataMulityFloat,//将点位写入PLC
	Operator_Type_ETH_PLC_WriteDataCheck,//将点位写入PLC
	Operator_Type_ETH_PLC_ReadPortIntWriteToTempDataMulity,//读取多个端口号值写入一个临时变量用，分割  Int 类型
	Operator_Type_ETH_PLC_ReadPortFloatWriteToTempDataMulity,//读取多个端口号值写入一个临时变量用，分割  float 类型
	Operator_Type_ETH_PLC_WriteTempPosToPLC,//将点位写入PLC 


	//TCP 
	Operator_Type_ClientCreateTcpClientConnectServer,//创建一个TCP 客户端链接服务器
	Operator_Type_ClientWaitClientReConnect,//等待TCP重连链接
	Operator_Type_ClientSendDataToServer,//发送一个消息到服务器
	Operator_Type_ClientSendTempDataToServer,//发送一个消息到服务器
	Operator_Type_ClientSendTempDataToServerMulity,//发送一个消息到服务器
	Operator_Type_ADFClientSendTempDataToServerMulity,//发送一个消息到服务器
	

	Operator_Type_ClientSendAndRecv,//发送消息并且等待返回
	Operator_Type_ClientWaitOneMsgFromSever,//等待一条服务器消息
	Operator_Type_ClientWaitOneMsgFromSeverEqualParam1,//等待一条服务器消息参数相同
	Operator_Type_ADFClientWaitOneMsgFromSeverShockPlate,//ADF震动盘自定义消息处理

	Operator_Type_ClientWaitOneMsgSetFlageByContent,//等待一条服务器更具内容设置标志位
	Operator_Type_ClientDeleteOnClient,//删除一个客户端
	Operator_Type_ClientDataClear,//删除一个链接的数据
	
	Operator_Type_SeverCreateTcpServer,//创建一个TCP 服务器
	Operator_Type_SeverSendMsgToSkt,//向一个链接发送消息
	Operator_Type_SeverRecvOneMsgFromSkt,//向一个链接取一条消息
};



//<operator index = "1" station = "中转工站" Identification = "000001" Name = "中转轴回原" type = "AxisHome" 
//	postion = "0" description1 = "1" description2 = "2" param1 = "1" param2 = "1" param3 = "1" belong = "0000">
//	< / operator>

//对应结构体中 operator 操作
struct OperatorInfo
{
	QString labStr;		//标签名称	nodeName
	QString station;	//工站名称	attribute::station
	QString aliasName;	//别名		attribute::Name
	QString oprtType;	//操作类型	attribute::type
	QString postion;		//点位索引	attribute::postion

	QString  param1;	//参数1 供相应操作处理	attribute::param1
	QString  param2;	//参数2 供相应操作处理	attribute::param2
	QString  param3;	//参数3 供相应操作处理	attribute::param3
	QString  param4;	//参数4	供相应操作处理	attribute::param4
	QString  param5;	//参数5	供相应操作处理	attribute::param5
						//QString opeatorNum;
	int index;			//attribute::index
	QString Identification;	//唯一标识 号 流程号 + 步骤号 + 操作号 即标识 attribute::Identification
	QString belongStep;		//attribute::belong
	int bUse;
	OperatorInfo()
	{
		labStr = "operator";
		station = "虚拟工站";
		aliasName = "0";
		oprtType = "0";
		postion = "0";
		param4 = "0";
		param5 = "0";
		param1 = "0";
		param2 = "0";
		param3 = "0";
		//opeatorNum = "";
		index = 0;
		Identification = "000000";//六位
		belongStep = "0000";//Belong step indentify
		bUse = 1;
	}

};
//old xml step content 
//<step index = "1" stepNum = "01" type = "Normal" stepName = "工站回原" stepIndex = "0001" Identification = "0001" 
//	belong = "00" CodSupplement = "0">
//new ...
//<step index = "1"  type = "Normal" stepName = "工站回原"  Identification = "0001" belong = "00" CodSupplement = "0">

//step 结构 对应xml 中step 标签
struct StepInfo
{
	QString labStr;			//标签名称		nodeName		default::step
	QString stepType;		//步骤类型		default::Normal	attribute::type
	QString aliasName;		//attribute::stepName
							//QString stepNum;		//attribute::stepNum //throw away
	int index;				//attribute::index
	QString Identification;	//attribute::Identification
	int CodSupplement;		//是否需要代码处理 //attribute::CodSupplement  0:Not need Code support  1:need
	QString belongProc;		//attribute::belong
	int bUse;

	QList<OperatorInfo*> opeartorInfoList;
	StepInfo()
	{
		labStr = "step";
		stepType = "Normal";
		aliasName = "";
		//stepNum = "";
		index = 0;
		opeartorInfoList.clear();
		Identification = "0000";//四位
		CodSupplement = 0;
		belongProc = "00";//两位
		bUse = 1;
	}
};
//<Process index = "0" Identification = "00" Alias = "复位" StartInput = "复位" PauseInput = "暂停" StopInput = "停止">

struct ProcessInfo
{
	QString labStr;						//标签名称		nodeName		default::Process
										//QString processNum;				//attribute::stepName
	QString aliasName;					//attribute::Alias
	int index;							//attribute::index
	QString Identification;				//attribute::Identification
	QString startInput;					//attribute::StartInput
	QString pauseInput;					//attribute::PauseInput
	QString stopInput;					//attribute::StopInput
	QList<StepInfo*> stepInfoList;
	int bUse;
	ProcessInfo()
	{
		labStr = "process";
		//processNum = "";
		aliasName = "";
		stepInfoList.clear();
		index = 0;
		Identification = "00";//二位
		startInput = "";
		pauseInput = "";
		stopInput = "";
		bUse = 1;
	}
};
//<program name = "运动流程" Identification = "202006011">
struct ProgramInfo
{
	QString labStr;				//标签名称		nodeName		default::program
	QString aliasName;			//attribute::name
	QString Identification;		//attribute::Identification
	QList<ProcessInfo*> processInfoList;
	ProgramInfo() {
		labStr = "program";
		aliasName = "运动流程";
		Identification = "202006011";
		processInfoList.clear();
	}
};

enum Device_Status
{
	Device_Status_Running = 0,		//运行 工作态
	Device_Status_Pause,			//暂停
	Device_Status_Wait_Reset,		//待复位
	Device_Status_Reset_Compelete,	//已复位
	Device_Status_Idle,				//空闲状态
	Device_Status_Err				//故障

};

//XML 
class CDataAnalysis
{
public:
	CDataAnalysis();
	~CDataAnalysis();
	bool AnalysisProgramXml(QString path);
	void clear();

	void SaveProgramInfo(ProgramInfo Program, QString SavePath);
	void SaveXMLFile(QDomDocument& doc, QString savePath);
	void DataSort();
	QStringList GetOperatorTypeList();
	QStringList GetProcessNameList();



public:
	ProgramInfo m_CurProgram;
	Device_Status m_deviceStatus;
	QStringList m_operType;
	QStringList m_processNameList;
};
extern CDataAnalysis dataAnaly;

