#include "CFileOperator.h"
#include "FilePathManager.h"

CFileOperator::CFileOperator(QObject *parent)
	: QObject(parent)
{
}

CFileOperator::~CFileOperator()
{
}

bool CFileOperator::Init()
{
	//开启当天Log文件
	//QString logDir = qApp->applicationDirPath() + "/log/";
	QString logDirTopDir;
	theIniManger->ReadFilePath(logDirTopDir,"LogDir");
	QString logDir;
	logDir = logDirTopDir + "/RunLog/";

	QDir dir;
	if (!dir.exists(logDir))
		dir.mkpath(logDir);
	QString logPath = logDir + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".txt";
	m_txtPath = logPath;
	if (!m_txtfile.isOpen())
	{
		m_txtfile.setFileName(logPath);
		if (!m_txtfile.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Text))
			return false;
	}
	//开启CSV ?
	QString csvDir = logDirTopDir + "/RunCSV/";

	if (!dir.exists(csvDir))
		dir.mkpath(csvDir);
	QString csvPath = csvDir + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".csv";
	m_csvPath = csvPath;
	if (!m_csvFile.isOpen())
	{
		m_csvFile.setFileName(m_csvPath);
		if (!m_csvFile.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Truncate))
			return false;
	}
	return true;
}

bool CFileOperator::close()
{
	//关闭log
	if (m_csvFile.isOpen())
		m_csvFile.close();
	if (m_txtfile.isOpen())
		m_txtfile.close();
	return true;
}

CFileOperator theFileManager;

//bool CFileOperator::WriteFile_CSV(QString msg, QString path)
//{
//	return true;
//}
//
//bool CFileOperator::WriteFileWithTitle_CSV(QString dataStr, QString title, QString path)
//{
//	return true;
//}

bool CFileOperator::WriteTodayFile_CSV(QString msg)
{
	QMutexLocker locker(&m_csvMutex);
	QTextStream	csvStream(&m_csvFile);
	csvStream << msg << "\n";
	qDebug() << msg<< m_csvPath;
	return true;
}

//bool CFileOperator::WriteFile_TXT(QString msg, QString path)
//{
//	return true;
//}

bool CFileOperator::WriteTodayFile_TXT(QString msg)
{
	QMutexLocker locker(&m_txtMutex);
	//if (dirPath.isEmpty())
	//	return false;
	//QString logPath =dirPath+  QDateTime::currentDateTime().toString("yyyy-MM-dd")+".txt";
	//qDebug() << logPath;
	////检测 dir exit?
	//QDir dir;
	//if (!dir.exists(dirPath))
	//	dir.mkpath(dirPath);
	////写入文件
	//QFile file;
	//if (logPath != m_txtPath)
	//{
	//	m_txtPath = logPath;
	//	if (m_txtfile.isOpen())
	//		m_txtfile.close();
	//	m_txtfile.setFileName(logPath);
	//	if(!m_txtfile.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Text))
	//		return false;
	//}
	//else {
	//	if (!m_txtfile.isOpen())
	//	{
	//		m_txtfile.setFileName(logPath);
	//		if (!m_txtfile.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Text))
	//			return false;
	//	}
	//}
	QString dateTime =  QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");

	QTextStream	logStream(&m_txtfile);
	logStream << dateTime<<":" << msg<<"\n" ;
	return true;
}

bool CFileOperator::WriteTodayFile_TxtWithModule(QStringList ModuleName, QString msg)
{
	QMutexLocker locker(&m_txtMutex);

	QString temp;
	foreach(QString str, ModuleName)
		temp += QString("[%1]").arg(str);
	QString dateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
	QTextStream	logStream(&m_txtfile);
	logStream << dateTime << ":"<<temp << msg << "\n";

	return true;
}

bool CFileOperator::DeleteDirectoryFileWithTime(QString dirPath, int time /*= 0*/)
{
	QDir dir(dirPath);
	dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
	dir.setSorting(QDir::Size | QDir::Reversed);

	QFileInfoList list_sub = GetFileList(dirPath);
	QDateTime curTime = QDateTime::currentDateTime();
	int ndaysec = 24 * 60 * 60;
	uint etime = curTime.toTime_t();

	for (int i = 0; i < list_sub.size(); ++i) {
		QString filepath;
		QString full_path;
		QFileInfo fileInfo = list_sub.at(i);
		filepath.append(fileInfo.path());

		filepath += "/" + fileInfo.fileName();
		if (!filepath.isEmpty()) {
			QFile file(filepath);
			if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
				return false;
			}
			QFileInfo info(filepath);
			if (info.exists()) {

				QString file_size = tr("%1").arg(info.size());
				QDateTime dt = info.created();
				uint stime = dt.toTime_t();
				int day = (etime - stime) / (ndaysec)+((etime - stime) % (ndaysec)+(ndaysec - 1)) / (ndaysec)-1;

				if (day >= 30)
					QFile::remove(filepath);

				//QString create_time = tr("%1").arg(dt.toString("yyyy-MM-dd hh:mm:ss"));
				//dt = info.lastModified();
				//QString modify_time = tr("%1").arg(dt.toString("yyyy-MM-dd hh:mm:ss"));
				//QString file_id = tr("%1").arg(i);
				//qDebug() << "文件名:" << info.fileName() << " 版本:" << file_ver << " 大小(Byte) :" << file_size << " 创建时间 : " << create_time << " 创建时间 : " << modify_time;
			}
		}
	}
	return true;
}

bool CFileOperator::SetTxtDirPath(QString dirPath)
{
	QMutexLocker locker(&m_txtMutex);
	QString logPath = dirPath + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".txt";
	if (m_txtPath != logPath)
	{
		m_txtPath = logPath;
		QDir dir;
		if (!dir.exists(dirPath))
			dir.mkpath(dirPath);

		if (m_txtfile.isOpen())
			m_txtfile.close();
		m_txtfile.setFileName(logPath);
		if (!m_txtfile.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Text))
			return false;
	}
	return true;
}

bool CFileOperator::SetCsvDirPath(QString dirPath)
{
	QMutexLocker locker(&m_csvMutex);
	QString csvPath = dirPath + QDateTime::currentDateTime().toString("yyyy-MM-dd") + ".csv";
	if (m_csvPath != csvPath)
	{
		m_csvPath = csvPath;
		QDir dir;
		if (!dir.exists(dirPath))
			dir.mkpath(dirPath);

		if (m_txtfile.isOpen())
			m_txtfile.close();
		m_txtfile.setFileName(csvPath);
		if (!m_txtfile.open(QIODevice::WriteOnly | QIODevice::Append | QFile::Truncate))
			return false;
	}
	return true;
}

QFileInfoList CFileOperator::GetFileList(QString& path)
{
	QDir dir(path);
	QFileInfoList file_list = dir.entryInfoList(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
	QFileInfoList folder_list = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);

	for (int i = 0; i != folder_list.size(); i++) {
		QString name = folder_list.at(i).absoluteFilePath();
		QFileInfoList child_file_list = GetFileList(name);
		file_list.append(child_file_list);
	}

	return file_list;
}

CtrlError CFileOperator::DeleteDir(QString dirPath)
{
	if (dirPath.isEmpty())
		return PathError;
	QDir dir(dirPath);
	if (!dir.exists())
		return PathError;
	if (!dir.removeRecursively())
		return DeleteDirError;
	return NoError;
}
//删除文件不删除文件夹
//CtrlError CFileOperator::DeleteDirectoryFileByRecentHour(QString dirPath, int hours)
//{
//	if (dirPath.isEmpty())
//		return PathError;
//	QDir dir(dirPath);
//	if (!dir.exists())
//		return PathError;
//	if (hours == 0)
//	{
//		if (!dir.removeRecursively())
//		{
//			return DeleteDirError;
//		}
//		dir.mkdir(dirPath);
//		return NoError;
//	}
//	//筛选 文件和文件夹
//	dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
//	dir.setSorting(QDir::DirsLast);//文件夹排在后面
//	QFileInfoList infoList = dir.entryInfoList();
//	if (infoList.count() <= 0)
//	{
//		//\转 / （反斜杠转斜杠）
//		dirPath = QDir::QDir::fromNativeSeparators(dirPath);
//		if (1 == dirPath.count("/"))
//			return NoError;
//		dir.removeRecursively();
//		return NoError;
//	}
//
//	QDateTime curTime = QDateTime::currentDateTime();
//	foreach(QFileInfo info, infoList)
//	{
//		if (info.fileName() == "." | info.fileName() == "..")
//			continue;
//		QString fileName = info.fileName();
//		if (info.isDir())
//		{
//			QDir subDir;
//			QString subPath = info.absoluteFilePath();
//			//QString path = info.path() + "/" + fileName;
//			subDir.setPath(info.absoluteFilePath());
//			QFileInfoList sublist = subDir.entryInfoList();
//			if (sublist.count() <= 0)
//			{
//				QFile::remove(info.path());
//				continue;
//			}
//		
//			//文件夹递归删除
//			DeleteDirectoryFileByRecentHour(info.absoluteFilePath(), hours);
//			continue;
//		}
//		//QDateTime dt = info.created();
//		QDateTime dt = info.lastModified();
//		qint64 secDiffTime = dt.secsTo(curTime);//文件创建时间到现在时间
//		if (secDiffTime < 0)
//			continue;
//		else if (secDiffTime/(60*60)>hours)
//		{
//			QFile::remove(info.absoluteFilePath());
//		}
//	}
//	return NoError;
//}

//QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
CtrlError CFileOperator::DeleteDirectoryFileByRecentHour(QString dirPath, int hours)
{
	if (dirPath.isEmpty())
		return PathError;
	QDir dir(dirPath);
	if (!dir.exists())
		return PathError;
	if (hours == 0)
	{
		if (!dir.removeRecursively())
		{
			return DeleteDirError;
		}
		dir.mkdir(dirPath);
		return NoError;
	}
	static bool bEnterTopDir = true;
	//筛选 文件和文件夹
	dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
	dir.setSorting(QDir::DirsLast);//文件夹排在后面
	QFileInfoList infoList = dir.entryInfoList();
	if (infoList.count() <= 0)
	{
		//\转 / （反斜杠转斜杠）
		dirPath = QDir::QDir::fromNativeSeparators(dirPath);
		if (1 == dirPath.count("/"))
			return NoError;
		dir.removeRecursively();
		return NoError;
	}

	QDateTime curTime = QDateTime::currentDateTime();
	foreach(QFileInfo info, infoList)
	{
		if (info.fileName() == "." | info.fileName() == "..")
			continue;
		QString fileName = info.fileName();
		if (info.isDir())
		{
			QDir subDir;
			QString subPath = info.absoluteFilePath();
			//QString path = info.path() + "/" + fileName;
			subDir.setPath(info.absoluteFilePath());
			QFileInfoList sublist = subDir.entryInfoList();
			if (sublist.count() <= 0)
			{
				QFile::remove(info.path());
				continue;
			}
			//文件夹递归删除
			DeleteDirectoryFileByRecentHour(info.absoluteFilePath(), hours);
			continue;
		}
		//QDateTime dt = info.created();
		QDateTime dt = info.lastModified();
		qint64 secDiffTime = dt.secsTo(curTime);//文件创建时间到现在时间
		if (secDiffTime < 0)
			continue;
		else if (secDiffTime / (60 * 60) > hours)
		{
			QFile::remove(info.absoluteFilePath());
		}
	}
	return NoError;
}