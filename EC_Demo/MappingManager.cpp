#include "MappingManager.h"

MappingManager::MappingManager(QObject *parent)
	: QObject(parent)
{
}

MappingManager::~MappingManager()
{
}

QMap<QString,QList<QString>> MappingManager::GetMapInfo()
{
	return mdouleInfo;
}

void MappingManager::AddOneClient(QString clientName)
{
	//client
	mdouleInfo["Client"].push_back(clientName);
}

void MappingManager::DeleteOneClient(QString clinetName)
{
	QList<QString> clientList = mdouleInfo["Client"];
	foreach(QString client, clientList)
	{
		if (client == clinetName)
		{
			mdouleInfo["Client"].removeOne(clinetName);
			break;
		}
	}
}

void MappingManager::AddOneServer(QString serverName)
{
	QList<QString> clientList;
	if (!mdouleInfo.contains(serverName))
		mdouleInfo.insert(serverName, clientList);
}

MappingManager* MappingManager::Instance()
{
	static MappingManager theObj;
	return &theObj;
}

void MappingManager::AddOneSkt(QString serverName, QString clientName)
{
	if (!mdouleInfo.contains(serverName))return;
	else
	{
		if (!mdouleInfo[serverName].contains(clientName))
			mdouleInfo[serverName].push_back(clientName);
	}
}
void MappingManager::DeleteOneSkt(QString serverName, QString clientName)
{
	if (!mdouleInfo.contains(serverName))return;
	else
	{
		if (mdouleInfo[serverName].contains(clientName))
			mdouleInfo[serverName].removeOne(clientName);
	}
}