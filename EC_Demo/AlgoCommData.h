﻿#ifndef ALGOCOMMDATA_H
#define ALGOCOMMDATA_H
#include <QtCore>
#include "CtrlError.h"
class AlgoCommData
{
    Q_GADGET
public:
    AlgoCommData();
    ~AlgoCommData();

    CtrlError SetData(QString tData);    //Trans from String
    QString GetData();              //Trans to String
    int RecvNum;
    QMap<QString,QString> Data;
    enum ErrorCode
    {
        NoError             = 0,
        Base                = 0x00602000,
        CommandTransError   = Base+0x001,
    };
    Q_ENUM(ErrorCode)
};

#endif // ALGOCOMMDATA_H
