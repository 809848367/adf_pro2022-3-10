#pragma once

#include <QObject>
#include <QtCore>
#include <QTcpSocket>
#include <QTcpServer>
#include "CtrlError.h"
#define TheTcpManager  TCPManager::Instance()
class TCPManager : public QObject
{
	Q_OBJECT
public: 
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x00203000,
		ConnectionSeverFail = Base + 0x001,
		SocketError = Base + 0x002,
		TimeOutError = Base + 0x003,
		NoSeverCmd = Base + 0x004,
		ClientHasLive = Base + 0x005,
		ClientNotFind = Base + 0x006,
		ClientNotOpen = Base + 0x007,
		RecvStrBufferEmpty = Base + 0x008,
		InitSeverError = Base + 0x009,
		Base2 = 0x00203010,
		NotFindConnectSkt = Base2 +0x001,
	};
	Q_ENUM(ErrorCode)

public:
	TCPManager(QObject *parent = Q_NULLPTR);
	~TCPManager();
	static TCPManager* Instance();

//Server func region 
//description:
public:
	CtrlError CreateServer(QString severName,int Port,int MaxConnectNum=1);
	void CloseSever(QString serverName);
	CtrlError Server_CatchOnMsgFromSkt(QString sktName,QString& msg,int tOut);
	CtrlError Server_SendMsgToSkt(QString clientName, QString msg);
	CtrlError ClearBufferByClientName(QString sktName);

signals:
	void emit_CreateServer(QString severName, int Port, int MaxConnectNum);
	void emit_ServerSendNewServerName(QString severName);
	void emit_SeverSendConnectSktName(QString serverName, QString sktObjName);
	void emit_SeverSendDisConnectedSktName(QString serverName, QString sktObjName);
public slots:
	CtrlError Slot_CreateServer(QString severName, int Port, int MaxConnectNum);
private:
	void Sever_InitConnection(QTcpServer* server);
	void Server_SktInitConnect(QTcpSocket* skt);//链接槽函数创建
private slots:
void Server_NewConnect();//链接触发
void Server_SktRecv();
void Sever_SktDisconntected();
//Client func region
public:
	CtrlError Client_CreateOnClientConnectSever(QString clientName, QString IP, int Port);
	CtrlError DisconnectClient(QString clientName);
public slots: //消息发送必须使用信号槽
	CtrlError Client_SendDataToServer(QString clientName, QString dataStr);
	CtrlError Slot_Client_CreateOnClientConnectSever(QString clientName,QString IP,int Port);
	CtrlError Client_CatchOneDataFromServerMsg(QString clientName,QString& dataStr);
	CtrlError Client_CatchOneDataFromServerMsg(QString clientName, QString& dataStr,int length);//获取数据根据长度读取 防止粘包数据异常

	CtrlError Transmit_Client_SendDataToSever(QString clientName, QString dataStr);//转发消息 
	CtrlError Slot_UserDisconnectClient(QString clientName);//手动断链
	CtrlError Slot_AutoDisconnectClient();//服务器端断连 

	void ClientReadReady(QString clientName);
signals:
	void emit_Clinet_SendDataToServer(QString clientName, QString dataStr);
	void emit_Client_CreateOnClientConnectSever(QString clientName, QString IP, int Port);
	void emit_Client_TouchReadReady(QString clientName);		//手动触发一次读取
	void emit_Client_ConnectedClient(QString clientName);		//发送连接信息
	void emit_Client_DisconnectedClient(QString clientName);	//发送断开信息
private:
	void Client_InitConnection(QTcpSocket* client);
private slots:
void ClientRecvResult();
private:
	//client 
	QMap<QString, QTcpSocket*> m_client;
	QMap<QString, QList<QString>> m_recvBuffer;
	QMap<QString, QMutex*> m_recvBufferMutex;

	//server 面向链接
	QMap<QString, QTcpSocket*> m_ConnectClient;//<socketName,QTcpSocket*>
	QMap<QString, QList<QString>> m_ConnectRecvBuffer;//<socketName,buffer>
	QMap<QString, QMutex*> m_ConnectRecvBufferMutex; //<socketName,QMutex>

	QString ModuleName =  "TCPManager";
};








