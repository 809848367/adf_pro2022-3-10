﻿#ifndef IOCONTROLUI_H
#define IOCONTROLUI_H

#include <QtCore>
#include <QWidget>
#include <QButtonGroup>
#include <QTableWidget>
#include "HardwareManager.h"

namespace Ui {
class IOControlUI;
}

class IOControlUI : public QWidget
{
    Q_OBJECT

public:
    explicit IOControlUI(QWidget *parent = nullptr);
    ~IOControlUI();
    void SetHardwareManager(HardwareManager* HWM,QString Station);

private:
    Ui::IOControlUI *ui;
    HardwareManager* HWM;
    QString Station;
    QButtonGroup InputBtns,OutPutBtns;
    QTimer RefreshTimer;
    void InitBtnsList(QTableWidget *BtnsList);
    void ShowData(QTableWidget *BtnsList, QMap<int, QString> IOs, QMap<int, bool> IOValue, QButtonGroup &Btns);
private slots:
    void OutPutBtnsClicker(int n);
    void RefreshTimerSlot();
};

#endif // IOCONTROLUI_H
