#pragma once

#include <QObject>
#include <QtCore>
#include <wtypes.h>
//#include "../ModbusApi/Include/Include.h"
#include "../ModbusApi/Include/Hc_Modbus_Api.h"
#include "CtrlError.h"
//#include "../ModbusApi/Include/ModbusTcpAPI.h"
//#include "../ModbusApi/Include/MontionCtrl.h"
#define  theModbusMoudle ModbusMoudle::Instance()

#define ETH_INT_VALUE		0
#define ETH_FLOAT_VALUE		1



class ModbusMoudle : public QObject
{
	Q_OBJECT
public:
	enum ErrorCode
	{
		NoError = 0x00,
		Base = 0x00605000,
		ConnectionError = Base + 0x001,
		WriteError = Base + 0x002,
		ReadError = Base + 0x003,
		DataNotPatch = Base + 0x004,
	};
	Q_ENUM(ErrorCode)
public:
	ModbusMoudle(QObject *parent = Q_NULLPTR);
	~ModbusMoudle();
	//inovance �㴨 H5U  ��д������ַ  
	CtrlError ConnectETH(QString ipAdress, int nNetId = 0, int nIpPort = 502);
	CtrlError DisconnectETH(int nNetId = 0);
	CtrlError WriteDataInt_ETH(SoftElemType eType, int nStartAddr, int nValue); //
	CtrlError WriteDataFloat_ETH(SoftElemType eType, int nStartAddr, float nValue); //
	CtrlError ReadDataInt_ETH(SoftElemType eType, int nStartAddr, int& value);
	CtrlError ReadDataFloat_ETH(SoftElemType eType, int nStartAddr, float& value);
	CtrlError WriteDataCheckInt_ETH(SoftElemType eType, int nStartAddr, qint16 nValue);
	CtrlError WriteDataCheckFloat_ETH(SoftElemType eType, int nStartAddr, float nValue);
	static ModbusMoudle* Instance();
private:
	int PByteConvertToInt(BYTE* pData);
	float PByteConvertToFloat(BYTE* pData);

	void IntValueToPByte(int value, BYTE* pData);
	void FloatValueToPByte(float value,BYTE* pData );

	void RegisterCtrlError();
	QMutex m_mutex;
	
};
