#pragma once

#include <QThread>
#include <QtCore>
#include <QTcpSocket>
#include <atomic>
#include "CtrlError.h"
//通用 clientSoketThread 
struct CmdData //发接模式 通过标识 判断 ，从服务器发送过来的命令, 通过 msgType检查 
{
	int indetify;//标识 
	QString OperatorType;//Func Name
	QVariant sData;//数据 
	int timeOut; //超时 0不限时间
	QString resInfo;//success or fail timeOut disconnect
	QString msgType; //clientCmd,serverRespond,serverCmd、clientRespond 
	CmdData() // 默认构造心跳
	{
		indetify = 0;
		OperatorType = "";
		resInfo = "";
		timeOut = 0;
	}
};
Q_DECLARE_METATYPE(CmdData)
class ClientThread : public QThread
{
	Q_OBJECT
public:
	enum ErrorCode
	{
		NoError = 0,
		Base = 0x00103000,
		ConnectionPLCError = Base + 0x001,
		SocketError = Base + 0x002,
		TimeOutError = Base + 0x003,
		NoSeverCmd = Base + 0x004,
	};
	Q_ENUM(ErrorCode)

public:
	ClientThread(QString ip, qint16 port,QObject *parent = nullptr);
	//ClientThread(QString ip,qint16 port);
	~ClientThread();
	void run() override;
	CtrlError SendCmdWaitRespond(CmdData send, CmdData& respond);
	bool IsConnectServer();
	CtrlError CatchOneServerCmd(CmdData& serverCmd);
private:
	void PushOneCmdDataToList(CmdData data);
	void PushOneRespondDataToList(CmdData data);
	CmdData GetOneRespondDataFromList();
	CmdData GetOneCmdDataFromList();

	CtrlError WaitDataByIdnetiy(int indetify, int tOut,CmdData& respond);
	CtrlError SendAndRecv(QByteArray cmd, QByteArray& respond,int timeOut);
	CtrlError Recv(QByteArray& recvData);
	//

	void PushOneServerCmd(CmdData& serverCmd);

private:
	QTcpSocket* m_socket;
	QString IP;
	qint16 Port;
	QList<CmdData> cmdList;		//发送 
	QList<CmdData> respondList;	//反馈
	QList<CmdData> serverCmdList;
	bool m_bRun;
	QMutex m_cmdMutex;//命令锁
	QMutex m_resMutex;//回应锁
	QMutex m_serverCmdMutex;
};
