#include "TeachData_Camera.h"

TeachData_Camera::TeachData_Camera(QObject *parent) : TeachData(parent)
{
    qRegisterMetaType <TeachData_Camera>("TeachData_Camera");
}

TeachData_Camera::TeachData_Camera(const TeachData_Camera &TeachData)
{
    this->Params = TeachData.Params;
}

int TeachData_Camera::FromTable(QVector<QString> Keys, QVector<QVector<QVariant> > Data)
{
    if(Keys.size() != 2) return -1;
    for(int i=0;i<Data.size();i++)
    {
        if(Data[i].size() != 2) return -1;
        CameraParam tParam;
        QString tParamName;
        for(int j=0;j<Keys.size();j++)
        {
            QList<QString> tmp;
            if(Keys[j] == "Name"     && Data[i][j].typeName() == QString("QString")) tParamName = Data[i][j].value<QString>();
            if(Keys[j] == "Value"    && Data[i][j].typeName() == QString("QString"))
            {
                tmp = Data[i][j].value<QString>().split(",");
                if(tmp.size() != 3) return -1;
                bool isOK;
                tParam.Exposure = tmp[0].toDouble(&isOK);
                if(!isOK) return -1;
                tParam.Gamma = tmp[1].toDouble(&isOK);
                if(!isOK) return -1;
                tParam.Gain = tmp[2].toDouble(&isOK);
                if(!isOK) return -1;
            }
        }
        SetParam(tParamName,tParam);
    }
    return 0;
}

void TeachData_Camera::ToTable(QVector<QString> &Keys, QVector<QVector<QVariant> > &Data)
{
    Keys.clear();
    Data.clear();
    Keys<<"Name"<<"Value";
    for(int i=0;i<Params.size();i++)
    {
        QVector<QVariant> tData;
        tData.push_back(QVariant::fromValue(Params[i].first));
        QString tmp = QString("%1,%2,%3").arg(Params[i].second.Exposure).arg(Params[i].second.Gamma).arg(Params[i].second.Gain);
        tData.push_back(QVariant::fromValue(tmp));
        Data.push_back(tData);
    }
    return;
}

int TeachData_Camera::GetParam(QString Name, CameraParam &Param)
{
    int Index = 0;
    for(;Index < Params.size();Index++)
    {
        if(Params[Index].first == Name)
        {
            Param = Params[Index].second;
            return 0;
        }
    }
    return -1;
}

QList<QPair<QString, CameraParam> > TeachData_Camera::GetParams()
{
    return Params;
}

void TeachData_Camera::SetParam(QString Name, CameraParam Param)
{
    int Index = 0;
    for(;Index < Params.size();Index++)
    {
        if(Params[Index].first == Name) break;
    }
    if(Index < Params.size() && Params[Index].first == Name)
    {
        Params[Index].second = Param;
    }
    else
    {
        Params.push_back(QPair<QString,CameraParam>(Name,Param));
    }
    return;
}

void TeachData_Camera::RemoveParam(QString Name)
{
    int Index = 0;
    for(;Index < Params.size();Index++)
    {
        if(Params[Index].first == Name) break;
    }
    if(Index < Params.size() && Params[Index].first == Name)
    {
        Params.removeAt(Index);
    }
    return;
}

TeachData_Camera &TeachData_Camera::operator =(const TeachData_Camera &TeachData)
{
    this->Params = TeachData.Params;
    return *this;
}

