﻿#include "Camera.h"
#include "MvCameraControl.h"
Camera::Camera(QObject *parent) : HardwareModule(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    CameraHandle = nullptr;
}

Camera::~Camera()
{
    int Status;
    GetStatus(Status);
    if(Status == 1) Close();
}

CtrlError Camera::Init(HardwareInitData InitData)
{
    if(InitData.Keys.size() != 1) return CtrlError(InitDataErr,InitData.Name);
    if(InitData.InitData.size() != 1) return CtrlError(InitDataErr,InitData.Name);
    if(InitData.InitData[0].size() != 1) return CtrlError(InitDataErr,InitData.Name);
    QString SN;
    for(int j=0;j<1;j++)
    {
        if(InitData.Keys[j] == "SN" && InitData.InitData[0][j].canConvert<QString>())
            SN = InitData.InitData[0][j].value<QString>();
        else
            return CtrlError(InitDataErr,InitData.Name);
    }
    return Init(InitData.Name,SN);
}

CtrlError Camera::Execute(HardwareCommData ExecuteData, HardwareCommData &ResultData)
{
    CtrlError ret;
    ResultData.Station = ExecuteData.Station;
    ResultData.Identfication = ExecuteData.Identfication;
    ResultData.OperatorName = ExecuteData.OperatorName;
    if(ExecuteData.OperatorName == "Open")
    {
        ret =  Open();
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "Close")
    {
        ret =  Close();
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "GetStatus")
    {
        int Status;
        ret =  GetStatus(Status);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Status",QVariant::fromValue(Status));
    }
    if(ExecuteData.OperatorName == "GetImageAndSave")
    {
        if(!ExecuteData.Datas["Path"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Path = ExecuteData.Datas["Path"].value<QString>();
        if(!ExecuteData.Datas["Exposure"].canConvert<double>()) return CtrlError(ExecuteDataNotRight,Name);
        double Exposure = ExecuteData.Datas["Exposure"].value<double>();
        if(!ExecuteData.Datas["Gamma"].canConvert<double>()) return CtrlError(ExecuteDataNotRight,Name);
        double Gamma = ExecuteData.Datas["Gamma"].value<double>();
        if(!ExecuteData.Datas["Gain"].canConvert<double>()) return CtrlError(ExecuteDataNotRight,Name);
        double Gain = ExecuteData.Datas["Gain"].value<double>();
        ret =  GetImageAndSave(Path,Exposure,Gamma,Gain);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "GetImage")
    {
        if(!ExecuteData.Datas["Exposure"].canConvert<double>()) return CtrlError(ExecuteDataNotRight,Name);
        double Exposure = ExecuteData.Datas["Exposure"].value<double>();
        if(!ExecuteData.Datas["Gamma"].canConvert<double>()) return CtrlError(ExecuteDataNotRight,Name);
        double Gamma = ExecuteData.Datas["Gamma"].value<double>();
        if(!ExecuteData.Datas["Gain"].canConvert<double>()) return CtrlError(ExecuteDataNotRight,Name);
        double Gain = ExecuteData.Datas["Gain"].value<double>();
        cv::Mat Image;
        ret = GetImage(Image,Exposure,Gamma,Gain);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Image",QVariant::fromValue(Image));
    }
    if(ExecuteData.OperatorName == "GetImageAndSave2")
    {
        if(!ExecuteData.Datas["Path"].canConvert<QString>()) return CtrlError(ExecuteDataNotRight,Name);
        QString Path = ExecuteData.Datas["Path"].value<QString>();
        if(!ExecuteData.Datas["Param"].canConvert<CameraParam>()) return CtrlError(ExecuteDataNotRight,Name);
        CameraParam Param = ExecuteData.Datas["Param"].value<CameraParam>();
        ret =  GetImageAndSave(Path,Param.Exposure,Param.Gamma,Param.Gain);
        if(ret != NoError) return ret;
    }
    if(ExecuteData.OperatorName == "GetImage2")
    {
        if(!ExecuteData.Datas["Param"].canConvert<CameraParam>()) return CtrlError(ExecuteDataNotRight,Name);
        CameraParam Param = ExecuteData.Datas["Param"].value<CameraParam>();
        cv::Mat Image;
        ret = GetImage(Image,Param.Exposure,Param.Gamma,Param.Gain);
        if(ret != NoError) return ret;
        ResultData.Datas.insert("Image",QVariant::fromValue(Image));
    }
    return CtrlError(NoError);
}

CtrlError Camera::Init(QString Name, QString SN)
{
    this->Name = Name;
    this->SN = SN;
    return Open();
}

CtrlError Camera::Open()
{
	return  COGNEX_CAM_Open();

    int ret;
    //枚举设备只需要进行一次，防止内存重复刷新
    static bool isEnumDevices = false;
    static MV_CC_DEVICE_INFO_LIST DeviceList;
    if(!isEnumDevices)
    {
        ret = MV_CC_EnumDevices(MV_GIGE_DEVICE|MV_USB_DEVICE,&DeviceList);
        if(ret != MV_OK) return CtrlError(EnumDevicesErr,Name);
        isEnumDevices = true;
    }
    //查找当前设备
    bool isFind = false;
    MV_CC_DEVICE_INFO *NowDevice;
    for(int i=0;i<DeviceList.nDeviceNum;i++)
    {
        if(DeviceList.pDeviceInfo[i]->nTLayerType == MV_GIGE_DEVICE)
        {
            if(QString((char*)DeviceList.pDeviceInfo[i]->SpecialInfo.stGigEInfo.chSerialNumber) == SN)
            {
                NowDevice = DeviceList.pDeviceInfo[i];
                isFind = true;
                break;
            }
        }
        if(DeviceList.pDeviceInfo[i]->nTLayerType == MV_USB_DEVICE)
        {
            if(QString((char*)DeviceList.pDeviceInfo[i]->SpecialInfo.stUsb3VInfo.chSerialNumber) == SN)
            {
                NowDevice = DeviceList.pDeviceInfo[i];
                isFind = true;
                break;
            }
        }
    }
    if(!isFind) return CtrlError(CameraSNNotExist,Name);
    //创建句柄
    ret = MV_CC_CreateHandle(&CameraHandle,NowDevice);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(CreateHandleErr,Name);
    }
    //开启设备
    ret = MV_CC_OpenDevice(CameraHandle);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(CameraOpenErr,Name);
    }
    //开启取流
    ret = MV_CC_StartGrabbing(CameraHandle);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(StartGrabbingErr,Name);
    }

    //触发模式开启
    ret = MV_CC_SetEnumValue(CameraHandle, "TriggerMode", MV_TRIGGER_MODE_ON);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(ParaSetErr,Name);
    }


    //软件触发
    ret = MV_CC_SetEnumValue(CameraHandle, "TriggerSource", MV_TRIGGER_SOURCE_SOFTWARE);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(ParaSetErr,Name);
    }
    ret = MV_CC_SetEnumValue(CameraHandle, "ExposureMode", MV_EXPOSURE_MODE_TIMED);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(ParaSetErr,Name);
    }
    ret = MV_CC_SetEnumValue(CameraHandle, "GainAuto", MV_GAIN_MODE_OFF);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(ParaSetErr,Name);
    }

    /*ret = MV_CC_SetBoolValue(CameraHandle, "GammaEnable", true);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(ParaSetErr,Name);
    }*/
    return CtrlError(NoError);
}

CtrlError Camera::Close()
{
    //这里因为无法抓取相机状态，直接全部结束，所以有可能会在未开启相机/未开启取流的情况下执行此函数，所以不能读取函数执行状态
    if(CameraHandle != nullptr)
    {
        MV_CC_StopGrabbing(CameraHandle);
        MV_CC_CloseDevice(CameraHandle);
        MV_CC_DestroyHandle(CameraHandle);
    }
    CameraHandle = nullptr;
    return CtrlError(NoError);
}

CtrlError Camera::GetStatus(int &Status)
{
    if(CameraHandle == nullptr) Status = 0;
    else
    {
        if(MV_CC_IsDeviceConnected(CameraHandle)) Status = 1;
        else Status = 0;
    }
    return CtrlError(NoError);
}

CtrlError Camera::GetImageAndSave(QString Path, double Exposure, double Gamma, double Gain)
{
    CtrlError ret;
    cv::Mat Image;
    ret = GetImage(Image,Exposure,Gamma,Gain);
    if(ret != NoError) return ret;
    bool bRet = cv::imwrite(Path.toLocal8Bit().toStdString(),Image);
	if (!bRet)
	{
		return CtrlError(SaveImageError);
	}
    return CtrlError(NoError);
}

CtrlError Camera::GetImage(cv::Mat &Image, double Exposure, double Gamma, double Gain)
{
	QMutexLocker locker(&m_catchImgMutex);
	return COGNEX_CAM_GetImage(Image, Exposure, Gamma, Gain);


    int ret;
    ret = MV_CC_SetFloatValue(CameraHandle, "ExposureTime", Exposure);
    if(ret != MV_OK) return CtrlError(ParaSetErr,Name);
    //ret = MV_CC_SetFloatValue(CameraHandle, "Gamma", Gamma);
    //if(ret != MV_OK) return CtrlError(ParaSetErr,Name);
    ret = MV_CC_SetFloatValue(CameraHandle, "Gain", Gain);
    if(ret != MV_OK) return CtrlError(ParaSetErr,Name);
    ret = MV_CC_ClearImageBuffer(CameraHandle);
    if(ret != MV_OK) return CtrlError(ParaSetErr,Name);

    MVCC_INTVALUE nDataSize;
    ret = MV_CC_GetIntValue(CameraHandle,"PayloadSize", &nDataSize);
    if(ret != MV_OK)
    {
        Close();
        return CtrlError(ParaGetErr,Name);
    }

    unsigned char* pGrabBuf = (unsigned char*)malloc(nDataSize.nCurValue);
    MV_FRAME_OUT_INFO_EX ImgInfo;
    int losePacktetTimes = 0;
    while(1)
    {
        //ret = MV_CC_SetCommandValue(CameraHandle,"TriggerSoftware");
        //if(ret != MV_OK)
        //{
        //    Close();
        //    return CtrlError(ParaGetErr,Name);
        //}
        ret = MV_CC_GetOneFrameTimeout(CameraHandle, pGrabBuf, nDataSize.nCurValue, &ImgInfo, 1000);
        if( ImgInfo.nLostPacket == 0)
            break;
		if (ret != MV_OK)
			++losePacktetTimes;
        if(++losePacktetTimes > 3) return CtrlError(GetImageErr,Name);
    }
    Image = cv::Mat(ImgInfo.nHeight,ImgInfo.nWidth,CV_8UC1);
    memcpy(Image.data,pGrabBuf,ImgInfo.nHeight*ImgInfo.nWidth*sizeof(uchar));
    free(pGrabBuf);
	cv::cvtColor(Image, Image, cv::COLOR_BayerGR2RGB);
    return CtrlError(NoError);
}


CtrlError Camera::COGNEX_CAM_GetImage(cv::Mat &Image, double Exposure, double Gamma, int Gain)
{
	int ret;
	ret = MV_CC_SetFloatValue(CameraHandle, "ExposureTimeAbs", Exposure);
	if (ret != MV_OK) return CtrlError(ParaSetErr, Name);
	ret = MV_CC_SetFloatValue(CameraHandle, "Gamma", Gamma);
	//if (ret != MV_OK) return CtrlError(ParaSetErr, Name);
	//ret = MV_CC_SetIntValBue(CameraHandle, "GainRaw", Gain);
	if (ret != MV_OK) return CtrlError(ParaSetErr, Name);
	ret = MV_CC_ClearImageBuffer(CameraHandle);
	if (ret != MV_OK) return CtrlError(ParaSetErr, Name);

	MVCC_INTVALUE nDataSize;
	ret = MV_CC_GetIntValue(CameraHandle, "PayloadSize", &nDataSize);
	if (ret != MV_OK)
	{
		Close();
		return CtrlError(ParaGetErr, Name);
	}
	unsigned char* pGrabBuf = (unsigned char*)malloc(nDataSize.nCurValue);
	MV_FRAME_OUT_INFO_EX ImgInfo;
	int losePacktetTimes = 0;
	while (1)
	{
		//ret = MV_CC_SetCommandValue(CameraHandle, "TriggerSoftware");
		//if (ret != MV_OK)
		//{
		//	Close();
		//	return CtrlError(ParaGetErr, Name);
		//}
		//采图
		//MV_FRAME_OUT stImageInfo = { 0 };
		//ret = MV_CC_GetImageBuffer(CameraHandle, &stImageInfo, 1000);
		//if (ret != MV_OK)
		//{
		//	return CtrlError(GetImageErr, Name);
		//}

		ret = MV_CC_GetOneFrameTimeout(CameraHandle, pGrabBuf, nDataSize.nCurValue, &ImgInfo, 1000);
		if (ImgInfo.nLostPacket == 0)
			break;
		if (ret == MV_OK)
			break;
		if (++losePacktetTimes > 3) return CtrlError(GetImageErr, Name);
	}
	Image = cv::Mat(ImgInfo.nHeight, ImgInfo.nWidth, CV_8UC1);
	if (Image.cols == 0 || Image.rows == 0)
		return CtrlError(GetImageErr, Name);
	memcpy(Image.data, pGrabBuf, ImgInfo.nHeight*ImgInfo.nWidth * sizeof(uchar));
	free(pGrabBuf);

	return CtrlError(NoError);
}



CtrlError Camera::COGNEX_CAM_Open()
{
	int ret;
	//枚举设备只需要进行一次，防止内存重复刷新
	static bool isEnumDevices = false;
	static MV_CC_DEVICE_INFO_LIST DeviceList;
	if (!isEnumDevices)
	{
		ret = MV_CC_EnumDevices(MV_GIGE_DEVICE | MV_USB_DEVICE, &DeviceList);
		if (ret != MV_OK) return CtrlError(EnumDevicesErr, Name);
		isEnumDevices = true;
	}
	//查找当前设备
	bool isFind = false;
	MV_CC_DEVICE_INFO *NowDevice;
	for (int i = 0; i < DeviceList.nDeviceNum; i++)
	{
		if (DeviceList.pDeviceInfo[i]->nTLayerType == MV_GIGE_DEVICE)
		{
			if (QString((char*)DeviceList.pDeviceInfo[i]->SpecialInfo.stGigEInfo.chSerialNumber) == SN)
			{
				NowDevice = DeviceList.pDeviceInfo[i];
				isFind = true;
				//创建句柄
				ret = MV_CC_CreateHandle(&CameraHandle, NowDevice);
				if (ret != MV_OK)
				{
					return CtrlError(CreateHandleErr, Name);
				}

				ret = MV_CC_OpenDevice(CameraHandle);
				if (ret != MV_OK)
				{
					return CtrlError(CameraOpenErr, Name);
				}

				unsigned int nPacketSize = ret;
				ret = MV_CC_GetOptimalPacketSize(CameraHandle);
				nPacketSize = (unsigned int)ret;
				//设置包大小
				ret = MV_CC_SetIntValueEx(CameraHandle, "GevSCPSPacketSize", nPacketSize);
				if (ret != MV_OK)
				{
					return  CtrlError(ParaSetErr, Name);
				}
				break;
			}
		}
		if (DeviceList.pDeviceInfo[i]->nTLayerType == MV_USB_DEVICE)
		{
			if (QString((char*)DeviceList.pDeviceInfo[i]->SpecialInfo.stUsb3VInfo.chSerialNumber) == SN)
			{
				NowDevice = DeviceList.pDeviceInfo[i];
				isFind = true;
				break;
			}
		}
	}
	if (!isFind) return CtrlError(CameraSNNotExist, Name);

	//开启设备

	//开启取流
	ret = MV_CC_StartGrabbing(CameraHandle);
	if (ret != MV_OK)
	{
		Close();
		return CtrlError(StartGrabbingErr, Name);
	}
	//触发模式开启
	ret = MV_CC_SetEnumValue(CameraHandle, "TriggerMode", 0);   //OFF
	if (ret != MV_OK)
	{
		Close();
		return CtrlError(ParaSetErr, Name);
	}

	//软件触发
	//ret = MV_CC_SetEnumValue(CameraHandle, "TriggerSource", 0); //Software
	//if (ret != MV_OK)
	//{
	//	Close();
	//	return CtrlError(ParaSetErr, Name);
	//}

	ret = MV_CC_SetEnumValue(CameraHandle, "ExposureMode", 1);  //Timed
	if (ret != MV_OK)
	{
		Close();
		return CtrlError(ParaSetErr, Name);
	}

	ret = MV_CC_SetEnumValue(CameraHandle, "GainAuto", 0);      //OFF
	if (ret != MV_OK)
	{
		Close();
		return CtrlError(ParaSetErr, Name);
	}

	ret = MV_CC_SetBoolValue(CameraHandle, "GammaEnable", true);    //ON
	if (ret != MV_OK)
	{
		Close();
		return CtrlError(ParaSetErr, Name);
	}
	return CtrlError(NoError);

}

















