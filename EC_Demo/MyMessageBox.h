#pragma once

#include <QObject>
#include <QtCore>
#include <QEventLoop>
#include <QMessageBox>
#include <QTimer>
#include <QApplication>

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8") 
#endif 

class MyMessageBox : public QObject
{
	Q_OBJECT

public:
	MyMessageBox(QObject *parent = nullptr);
	MyMessageBox(QString title, QString msg, int type);
	~MyMessageBox();
	

public:
	void onShow();
	static void showBox();
	static void DeleteBox();
	static MyMessageBox* GetBoxInstance();
	static void SetDispMsg(QString title,QString content);
	void setDispInfo(QString title, QString content);
	
private:
	void readyShow();

	QString m_title;
	QString m_content;
	QString m_type;
	static MyMessageBox* instance;
};
