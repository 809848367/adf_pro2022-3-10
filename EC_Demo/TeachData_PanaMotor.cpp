#include "TeachData_PanaMotor.h"

TeachData_PanaMotor::TeachData_PanaMotor(QObject *parent) : TeachData(parent)
{
    qRegisterMetaType <TeachData_PanaMotor>("TeachData_PanaMotor");
}

TeachData_PanaMotor::TeachData_PanaMotor(const TeachData_PanaMotor &TeachData)
{
    this->Points = TeachData.Points;
}

int TeachData_PanaMotor::FromTable(QVector<QString> Keys, QVector<QVector<QVariant> > Data)
{
    if(Keys.size() != 2) return -1;
    for(int i=0;i<Data.size();i++)
    {
        if(Data[i].size() != 2) return -1;
        int tPoint;
        QString tPointName;
        for(int j=0;j<Keys.size();j++)
        {
            if(Keys[j] == "Name"     && Data[i][j].typeName() == QString("QString")) tPointName      = Data[i][j].value<QString>();
            if(Keys[j] == "Value"    && Data[i][j].typeName() == QString("double"))  tPoint          = Data[i][j].value<double>();
        }
        SetPoint(tPointName,tPoint);
    }
    return 0;
}

void TeachData_PanaMotor::ToTable(QVector<QString> &Keys, QVector<QVector<QVariant> > &Data)
{
    Keys.clear();
    Data.clear();
    Keys<<"Name"<<"Value";
    for(int i=0;i<Points.size();i++)
    {
        QVector<QVariant> tData;
        tData.push_back(QVariant::fromValue(Points[i].first));
        tData.push_back(QVariant::fromValue((double)Points[i].second));
        Data.push_back(tData);
    }
    return;
}

void TeachData_PanaMotor::SetPoint(QString Name, int Point)
{
    int Index = 0;
    for(;Index < Points.size();Index++)
    {
        if(Points[Index].first == Name) break;
    }
    if(Index < Points.size() && Points[Index].first == Name)
    {
        Points[Index].second = Point;
    }
    else
    {
        Points.push_back(QPair<QString,int>(Name,Point));
    }
    return;
}

void TeachData_PanaMotor::RemovePoint(QString Name)
{
    int Index = 0;
    for(;Index < Points.size();Index++)
    {
        if(Points[Index].first == Name) break;
    }
    if(Index < Points.size() && Points[Index].first == Name)
    {
        Points.removeAt(Index);
    }
    return;
}

int TeachData_PanaMotor::GetPoint(QString Name, int &Point)
{
    int Index = 0;
    for(;Index < Points.size();Index++)
    {
        if(Points[Index].first == Name)
        {
            Point = Points[Index].second;
            return 0;
        }
    }
    return -1;
}

QList<QPair<QString, int> > TeachData_PanaMotor::GetPoints()
{
    return Points;
}

TeachData_PanaMotor &TeachData_PanaMotor::operator =(const TeachData_PanaMotor &TeachData)
{
    this->Points = TeachData.Points;
    return *this;
}
