﻿#include "CtrlError.h"
QMap<int,QString> CtrlError::ErrorList = QMap<int,QString>();
CtrlError::CtrlError()
{
    this->ErrCode = 0;
    this->Key = QString();
}

CtrlError::CtrlError(int ErrCode)
{
    this->ErrCode = ErrCode;
    this->Key = QString();
}

CtrlError::CtrlError(int ErrCode, QString Key)
{
    this->ErrCode = ErrCode;
    this->Key = Key;
}

CtrlError &CtrlError::SetKey(QString Key)
{
    this->Key = Key;
    return *this;
}

CtrlError::operator QString()
{
    QString ErrStr = "";
    if(!Key.isEmpty()) ErrStr.append(Key + ": ");
    ErrStr.append(QString("(0x%1) ").arg(uint(ErrCode),8,16,QLatin1Char('0'))+GetErrStr(ErrCode));
    return ErrStr;
}

CtrlError::operator int()
{
    return this->ErrCode;
}

void CtrlError::RegisterErr(int ErrCode, QString ErrStr)
{
    ErrorList.insert(ErrCode,ErrStr);
}

QString CtrlError::GetErrStr(int ErrCode)
{
    return ErrorList[ErrCode];
}


