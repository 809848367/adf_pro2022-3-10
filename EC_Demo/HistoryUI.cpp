#include "HistoryUI.h"
#include "ui_HistoryUI.h"

HistoryUI::HistoryUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HistoryUI)
{
    ui->setupUi(this);
    ui->DateTimeStart->setDateTime(QDateTime::currentDateTime().addDays(-1));
    ui->DateTimeStart->setCalendarPopup(true);
    ui->DateTimeStop->setDateTime(QDateTime::currentDateTime());
    ui->DateTimeStop->setCalendarPopup(true);
    InitDataList();
    ShowData(QVector<HistoryResult>());
}

HistoryUI::~HistoryUI()
{
    delete ui;
}

void HistoryUI::on_SNSearch_clicked()
{
    QVector<HistoryResult> Results;
    History::GetInstance().SelectBySN(ui->SN->text(),0,Results);
    ShowData(Results);
}

void HistoryUI::ShowData(QVector<HistoryResult> Results)
{
    ShowDataFlag = true;
    ui->DataList->clear();
    QColor PASSColor = QColor(100,255,100);
    QColor FAILColor = QColor(255,100,100);
    foreach(HistoryResult Result,Results)
    {
        QTreeWidgetItem *SNItem = new QTreeWidgetItem();
        SNItem->setText(0,Result.SN);
        SNItem->setText(1,Result.BarCode);
        if(Result.Result == "PASS" && ui->OnlyFail->isChecked()) continue;
        if(Result.Result == "PASS")
        {
            SNItem->setBackgroundColor(1,PASSColor);
        }
        else
        {
            SNItem->setBackgroundColor(1,FAILColor);
        }
        foreach(QString Surface, Result.Surfaces.keys())
        {
            QTreeWidgetItem *SurfaceItem = new QTreeWidgetItem();
            SurfaceItem->setText(0,Surface);
            if(Result.Surfaces[Surface].Datas.contains("Result"))
            {
                SurfaceItem->setText(1,Result.Surfaces[Surface].Datas["Result"]);
                if(Result.Surfaces[Surface].Datas["Result"] == "PASS" && ui->OnlyFail->isChecked()) continue;
                if(Result.Surfaces[Surface].Datas["Result"] == "PASS")
                {
                    SurfaceItem->setBackgroundColor(1,PASSColor);
                }
                else
                {
                    SurfaceItem->setBackgroundColor(1,FAILColor);
                }

                QTreeWidgetItem *DataItem = new QTreeWidgetItem();
                DataItem->setText(0,"Result");
                DataItem->setText(1,Result.Surfaces[Surface].Datas["Result"]);
                SurfaceItem->addChild(DataItem);
            }
            if(Result.Surfaces[Surface].Files.contains("ResultPath"))
            {
                QTreeWidgetItem *DataItem = new QTreeWidgetItem();
                DataItem->setText(0,"ResultPath");
                DataItem->setText(1,Result.Surfaces[Surface].Files["ResultPath"]);
                SurfaceItem->addChild(DataItem);
            }
            foreach (QString DetailKey, Result.Surfaces[Surface].Datas.keys())
            {
                if(DetailKey == "Result") continue;
                QTreeWidgetItem *DataItem = new QTreeWidgetItem();
                DataItem->setText(0,DetailKey);
                DataItem->setText(1,Result.Surfaces[Surface].Datas[DetailKey]);
                SurfaceItem->addChild(DataItem);
            }
            foreach (QString DetailKey, Result.Surfaces[Surface].Files.keys())
            {
                if(DetailKey == "ResultPath") continue;
                QTreeWidgetItem *DataItem = new QTreeWidgetItem();
                DataItem->setText(0,DetailKey);
                DataItem->setText(1,Result.Surfaces[Surface].Files[DetailKey]);
                SurfaceItem->addChild(DataItem);
            }
            SNItem->addChild(SurfaceItem);
        }
        ui->DataList->addTopLevelItem(SNItem);
    }
    ShowDataFlag = false;
}

void HistoryUI::InitDataList()
{
    ui->DataList->setColumnCount(2);
    ui->DataList->header()->setVisible(false);
    ui->DataList->header()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->DataList->header()->setSectionResizeMode(1,QHeaderView::Stretch);
}

void HistoryUI::on_BarCodeSearch_clicked()
{
    QVector<HistoryResult> Results;
    History::GetInstance().SelectByBarCode(ui->BarCode->text(),0,Results);
    ShowData(Results);
}

void HistoryUI::on_DateTimeSearch_clicked()
{
    QVector<HistoryResult> Results;
    History::GetInstance().SelectByDateTime(ui->DateTimeStart->dateTime(),ui->DateTimeStop->dateTime(),Results);
    ShowData(Results);
}

void HistoryUI::on_DataList_itemClicked(QTreeWidgetItem *item, int column)
{
    if(ShowDataFlag) return;
    ui->DataShow->setText(QString());
    ui->IMShow->SetImage(cv::Mat());
    if(item)
    {
        QString Key = item->text(0);
        QString Value = item->text(1);
        ui->DataShow->setText(Key + " , " + Value);
        if(Value.contains("."))
        {
            QFileInfo tFile(Value);
            if(tFile.exists() && (tFile.suffix().toLower() == "bmp" || tFile.suffix().toLower() == "png"))
            {
                 cv::Mat Image = cv::imread(Value.toLocal8Bit().toStdString(),-1);
                 ui->IMShow->SetImage(Image);
            }
        }
    }
}
