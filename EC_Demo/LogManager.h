﻿#ifndef LOGMANAGER_H
#define LOGMANAGER_H

#include <QObject>
#include <QDateTime>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <QMutex>

//LogModule
class LogManager : public QObject
{
    Q_OBJECT
    explicit LogManager(QObject *parent = nullptr);
public:
    static LogManager &GetInstance();
    void Init();
    void WriteLog(QString LogType,QString ModuleName,QString LogStr);
private:
    QString LogFilePath;
    QMutex Locker;
    void LogBackup();
    void thread_SaveLog();
    QList<QString> LogBuffer;
signals:
    //Send Log When RecvLog
    void SendLog(QString TimeData,QString LogType,QString ModuleName,QString LogStr);
public slots:

};

#endif // LOGMANAGER_H
