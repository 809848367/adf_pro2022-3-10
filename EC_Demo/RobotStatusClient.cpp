﻿#include "RobotStatusClient.h"
#include <thread>
#include <QTcpServer>

RobotStatusClient::RobotStatusClient(QObject *parent) : QObject(parent)
{
    static bool isFirst = true;
    if(isFirst)
    {
        QMetaEnum ErrorCodeEnum = QMetaEnum::fromType<ErrorCode>();
        for(int i=0;i<ErrorCodeEnum.keyCount();i++)
        {
            CtrlError::RegisterErr(ErrorCodeEnum.value(i),ErrorCodeEnum.key(i));
        }
        isFirst = false;
    }
    Socket = nullptr;
}

RobotStatusClient::~RobotStatusClient()
{

}

CtrlError RobotStatusClient::Open(QString Name, QString IP, quint16 Port)
{
    this->Name = Name;
    this->IP = IP;
    this->Port = Port;
    if(Socket == nullptr) Socket = new QTcpSocket();
    if(Socket->isOpen()) return CtrlError(ClientHasOpen,Name);
    connect(Socket,SIGNAL(readyRead()),this,SLOT(RecvStatus()),Qt::UniqueConnection);
    Socket->setReadBufferSize(366*1250);
    Socket->connectToHost(QHostAddress(IP),Port);
    if(!Socket->waitForConnected(3000)) return CtrlError(ClientOpenFail,Name);
    return CtrlError(NoError);
}

CtrlError RobotStatusClient::Stop()
{
    if(!Socket->isOpen()) return CtrlError(ClientNotOpen,Name);
    disconnect(Socket,SIGNAL(readyRead()),this,SLOT(RecvStatus()));
    Socket->disconnectFromHost();
    Socket->close();
    Socket = nullptr;
    return CtrlError(NoError);
}

RobotStatus RobotStatusClient::GetStatus()
{
    return Status;
}

void RobotStatusClient::RecvStatus()
{
    QByteArray Data;
    Data.append(Socket->readAll());
    while(Data.size() >= 366)
    {
        RobotStatus tStatus;
        GetParam(Data,tStatus.Message_Size);
        GetParam(Data,tStatus.timestamp);
        GetParam(Data,tStatus.autorun_cycelMode);
        for(int i=0;i<8;i++) GetParam(Data,tStatus.machinePos[i]);
        for(int i=0;i<6;i++) GetParam(Data,tStatus.machinePose[i]);
        for(int i=0;i<6;i++) GetParam(Data,tStatus.machineUserPose[i]);
        for(int i=3;i<6;i++)
        {
            tStatus.machinePose[i] = tStatus.machinePose[i]*180/M_PI;           //转化成角度，方便显示
            tStatus.machineUserPose[i] = tStatus.machineUserPose[i]*180/M_PI;   //转化成角度，方便显示
        }
        for(int i=0;i<8;i++) GetParam(Data,tStatus.torque[i]);
        GetParam(Data,tStatus.robotState);
        GetParam(Data,tStatus.servoReady);
        GetParam(Data,tStatus.can_motor_run);
        for(int i=0;i<8;i++) GetParam(Data,tStatus.motor_speed[i]);
        GetParam(Data,tStatus.robotMode);
        for(int i=0;i<3;i++) GetParam(Data,tStatus.analog_ioInput[i]);
        for(int i=0;i<5;i++) GetParam(Data,tStatus.analog_ioOutput[i]);
        GetParam(Data,tStatus.digital_ioInputs);
        GetParam(Data,tStatus.digital_ioOutputs);
        quint64 n = 1;
        for(int i=0;i<64;i++)
        {
            tStatus.digital_ioInput[i] = tStatus.digital_ioInputs&n;
            tStatus.digital_ioOutput[i] = tStatus.digital_ioOutputs&n;
            n <<=1;
        }
        GetParam(Data,tStatus.collisionv);
        if(tStatus.Message_Size == 366)
            Status = tStatus;
    }
}

template<typename T>
void RobotStatusClient::GetParam(QByteArray &Data, T &Param)
{
    QByteArray tData;
    for(int i=0;i<sizeof(T);i++)
    {
        tData.insert(0,Data[0]);
        Data.remove(0,1);
    }
    memcpy(&Param,tData.data(),sizeof(T));
}
