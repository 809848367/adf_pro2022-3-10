#include "MainWindow.h"
#include "MachineManager.h"
#include "QiniOperator.h"
#include "DatabaseManager.h"
#include <QMessageBox>
#include "TeachManager.h"
#include "InfoDlg.h"
#include "History.h"
#include "CommunicationDlg.h"
#include "TCPManager.h"
#include "MappingManager.h"
#include "DevelopmentDlg.h"
#include "TempDataShowDlg.h"
#include "ModbusMoudle.h"
#include "UserManager.h"

#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
	if (SUCCESS_RETURN != Init())
	{
		QMessageBox::warning(NULL, "提示", "程序初始化失败", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
		//exit(0);
		qApp->quit();
		return;
	}
	InitWindow();
	InitConnection();
	theMachineManager.MsgRegister();
	Others();
	//int n, m;
	//m = 69, n = 0;
	//m = m / n;
}

int MainWindow::Init()
{
	CtrlDebugDlg::GetInstance();
	//qRegisterMetaType <TeachData>("TeachData");
	QString teachDB;
	iniOperator.setPath(FILE_NAEE_PATHMANAGER);
	iniOperator.readStringValue("filePath", "teachDB", teachDB);
	if (0 != TeachManager::GetInstance().InitDB(teachDB))
		return -1;
	QString histroyDB;
	iniOperator.readStringValue("filePath", "histroyDB", histroyDB);
	History::GetInstance().Init("histroyDB", histroyDB);
	return 0;
}

void MainWindow::InitWindow()
{
	CtrlDlg = new CtrlDebugDlg;
	setWindowTitle("DebugFlow_ADF_Ver_1.4.2");
	InitStatueBar();
}

void MainWindow::InitConnection()
{
	connect(&theMachineManager,&MachineManager::Signal_Send_Proc_List,ui.Welcom,&WelcomWidget::Set_ProcListSlot);
	//on_Action_DebugCtrl() connect(m_actTreeRun, &QAction::triggered, this, &MainWindow::on_actTreeRunSlot);
	connect(ui.actiondebugCtrl, &QAction::triggered, this, &MainWindow::on_Action_DebugCtrl);
	connect(ui.actionHistroy, &QAction::triggered, this, &MainWindow::on_Action_Histroy);
	connect(ui.actionCommunication, &QAction::triggered, this, &MainWindow::on_Action_Communication);
	connect(ui.actionDevelopmentLog, &QAction::triggered, this, &MainWindow::on_Action_DevelopmentDocDlg);
	connect(ui.actionTempData, &QAction::triggered, this, &MainWindow::on_Action_TempDataShow);
	//actionTempData
	//TeachManager* = &(TeachManager::GetInstance());
	connect(&theDbManager, &CDatabaseManager::SendTableDataSignal, &TeachManager::GetInstance(), &TeachManager::SetTable);
	connect(&TeachManager::GetInstance(), &TeachManager::UpDateTable, &theDbManager, &CDatabaseManager::RefreshTeachData);

	//TCPManager connect  MappingManger
	connect(TheTcpManager, &TCPManager::emit_Client_ConnectedClient, theMappingManager, &MappingManager::AddOneClient);
	connect(TheTcpManager, &TCPManager::emit_Client_DisconnectedClient, theMappingManager, &MappingManager::DeleteOneClient);

	connect(TheTcpManager, &TCPManager::emit_ServerSendNewServerName, theMappingManager, &MappingManager::AddOneServer);
	connect(TheTcpManager, &TCPManager::emit_SeverSendConnectSktName, theMappingManager, &MappingManager::AddOneSkt);
	connect(TheTcpManager, &TCPManager::emit_SeverSendDisConnectedSktName, theMappingManager, &MappingManager::DeleteOneSkt);
	//actionCommunication  on_Action_Communication

	connect(ui.Welcom, &WelcomWidget::SignalLogin, this, &MainWindow::refreshLoginLab);
	connect(ui.Welcom, &WelcomWidget::SignalMachine, this, &MainWindow::refreshMachineState);
	ui.tabWidget->setCurrentIndex(0);
	connect(ui.tabWidget, &QTabWidget::currentChanged, this, &MainWindow::tabwidgetChangeIndx);


}

void MainWindow::closeEvent(QCloseEvent * event)
{
	CtrlDebugDlg::ReleaseCtrlDlg();
	InfoDlg::ReleaseInfoDlg();
	theMachineManager.DeInit();
}

void MainWindow::Others()
{
	//刷新其他内容
	//ui.SystemCfg->RefreshDspProc(dataAnaly.m_processNameList);

	//ModbusMoudle* test = new ModbusMoudle;
	//if (test->ConnectETH("192.168.1.88"))
	//	qDebug() << __FUNCTION__ << "success";
	//else 
	//	qDebug() << __FUNCTION__ << "fail";

	//for (int i = 0; i < 100; i++)
	//{
	//	test->WriteData_ETH(REGI_H5U_D, 600 + i, i);
	//}
	//QList<qint16> values;
	//for (int i = 0; i<100; i++)
	//{
	//	qint16 value = 0;
	//	test->ReadData_ETH(REGI_H5U_D, 600 + i, value);
	//	values.push_back(value);
	//}
}

void MainWindow::InitStatueBar()
{
	m_loginLab = new QLabel("未登陆");
	//m_stateLab = new QLabel("待复位");
	m_timeLab = new QLabel(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	ui.statusBar->addWidget(m_loginLab);//永久信息窗口 - 不会被一般消息覆盖;
	m_loginLab->setAlignment(Qt::AlignHCenter);

	//ui.statusBar->addPermanentWidget(m_stateLab);//永久信息窗口 - 不会被一般消息覆盖;
	ui.statusBar->addPermanentWidget(m_timeLab);//永久信息窗口 - 不会被一般消息覆盖;
	ui.statusBar->setSizeGripEnabled(false);//去掉状态栏右下角的三角
	stateTimer = new QTimer;
	stateTimer->setInterval(10);

	connect(stateTimer, &QTimer::timeout, this, &MainWindow::refreshTimeLab);//主线程中执行心跳操作
	stateTimer->start();
}

void MainWindow::on_Action_DebugCtrl()
{
	CtrlDebugDlg::DispCtrlDlg();
}

void MainWindow::on_Action_Histroy() 
{
	InfoDlg::DispInfoDlg();
}

void MainWindow::on_Action_Communication()
{
	CommunicationDlg::DispCtrlDlg();
}

void MainWindow::on_Action_DevelopmentDocDlg()
{
	DevelopmentDlg dlg;
	dlg.exec();
}

void MainWindow::on_Action_TempDataShow()
{
	//TempDataShowDlg* tempDlg = TempDataShowDlg::GetInstance();
	TempDataShowDlg::DispTempDlg();
}

void MainWindow::tabwidgetChangeIndx(int index)
{
	if (index > 0 && theUserManager.GetCurrentUserLevel() == LOGIN_LEVEL_NULL)
	{
		ui.tabWidget->setCurrentIndex(0);
		QMessageBox::information(this, "提示", "请先登陆账户", QMessageBox::Yes, QMessageBox::No);
	}
}

void MainWindow::refreshTimeLab()
{
	//m_timeLab = new QLabel(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	m_timeLab->setText(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
}

void MainWindow::refreshLoginLab(QString str)
{
	m_loginLab->setText(str);
}

void MainWindow::refreshMachineState(QString str)
{
	//m_stateLab->setText(str);
}
