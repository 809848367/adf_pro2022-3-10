﻿#ifndef LIGHTING_H
#define LIGHTING_H

#include <QtCore>
#include <atomic>
#include "HardwareModule.h"
#include "SerialPortManager.h"
#include "CtrlError.h"

class Lighting : public HardwareModule
{
    Q_OBJECT
public:
    explicit Lighting(QObject *parent = nullptr);
    ~Lighting();
    //接口
    CtrlError Init(HardwareInitData InitData);
    CtrlError Execute(HardwareCommData ExecuteData,HardwareCommData& ResultData);

public:
    CtrlError Init(QString Name, SerialPortManager *SPM, QMap<int, QPair<QString, int> > LightData);
    CtrlError SetLight(int Index,uchar Value);
    CtrlError SetAllLight(QMap<int, uchar> Value);
    CtrlError GetLight(int Index,uchar &Value);
    CtrlError GetAllLight(QMap<int, uchar> &Value);
    CtrlError WaitLight(int Index,uchar Value, int TimeOut = 3000);
    CtrlError WaitAllLight(QMap<int, uchar> Value, int TimeOut = 3000);
    CtrlError SetLightAndWait(int Index,uchar Value, int TimeOut = 3000);
    CtrlError SetAllLightAndWait(QMap<int, uchar> Value, int TimeOut = 3000);
private:
    QString Name;
    SerialPortManager *SPM;
    QMap<int,QPair<QString,int>> LightData;
    QMap<int,uchar> SetValue;
    QMutex SetLocker;
    QMap<int,uchar> GetValue;
    QMutex GetLocker;
    void thread_Comm();
    std::atomic<int> threadFlag;
signals:
public:
    enum ErrorCode
    {
        NoError = 0,
        Base = 0x00302000,
        ChannelsNotEqualData    = Base + 0x001,
        ChannelsNotEqualValue   = Base + 0x002,
        IndexOutOfRange         = Base + 0x003,
        InitDataErr             = Base + 0x004,
        SPMisEmpty              = Base + 0x005,
        ExecuteDataNotRight     = Base + 0x006,
        WaitLightTimeout        = Base + 0x007,
    };
    Q_ENUM(ErrorCode)
};

#endif // LIGHTING_H
