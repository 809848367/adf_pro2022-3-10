﻿#ifndef LIGHTINGUI_H
#define LIGHTINGUI_H

#include <QWidget>
#include <QtCore>
#include "HardwareManager.h"
#include <QSignalMapper>
#include <QButtonGroup>
#include "TeachData_Lighting.h"
namespace Ui {
class LightingUI;
}

class LightingUI : public QWidget
{
    Q_OBJECT

public:
    explicit LightingUI(QWidget *parent = nullptr);
    ~LightingUI();
    void SetHardwareManager(HardwareManager* HWM,QString Station);
private:
    Ui::LightingUI *ui;
    HardwareManager* HWM;
    QString Station;
    QSignalMapper LightValueMapper;
    QTimer RefreshTimer;
    void InitLightingList();
    void ShowData();

    QButtonGroup TeachDataBtns;
    void InitTeachDataList();
    TeachData_Lighting TeachData;
    void ShowTeachData();
    bool ShowTeachDataFlag;
signals:
    void UpDateTeachData(QString Station,TeachData_Lighting TeachData);
private slots:
    void LightValueChanged(int n);
    void RefreshTimerSlot();

    void RefreshTeachData(QString Station,TeachData_Lighting TeachData);
    void TeachDataBtns_clicked(int n);
    void on_Teach_Add_clicked();
    void on_Teach_Del_clicked();
    void on_Teach_Set_clicked();
    void on_TeachData_cellChanged(int row, int column);
};

#endif // LIGHTINGUI_H
