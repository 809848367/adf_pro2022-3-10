#include "DatabaseManager.h"
#include "QiniOperator.h"
#include <QMessageBox>
#include <QCoreApplication>
#include <QSqlRecord>
#if _MSC_VER >= 1600 
#pragma execution_character_set("utf-8")  
#endif 

CDatabaseManager theDbManager;
CDatabaseManager::CDatabaseManager(QObject *parent)
	: QObject(parent)
{
	m_UserInfoList.clear();
	m_FlagInfoList.clear();
	m_Robot1.clear();
	m_Robot2.clear();
	m_SerialPortManager.clear();
	m_Lighting1.clear();
	m_Camera1.clear();
	m_Lighting2.clear();
	m_Camera2.clear();
	m_LightingUp.clear();
	m_CameraUp.clear();
	m_LightingDown.clear();
	m_LightingNG.clear();
	m_CameraNG.clear();
	m_Conveyor1.clear();
	m_Conveyor2.clear();
	m_PanaMotor1.clear();
	m_PanaMotor2.clear();
	m_IOControl.clear();
}


CDatabaseManager::~CDatabaseManager()
{

}

bool CDatabaseManager::InitDataBase()
{
	db = QSqlDatabase::addDatabase("QSQLITE");
	QString dataPath;
	iniOperator.readStringValue("filePath", "DataBasePath", dataPath);
	if (dataPath.isEmpty())
		return false;
	db.setDatabaseName(dataPath);
	return true;
}

bool CDatabaseManager::ReadUserInfo(QList<UserInfo>& UserInfoList, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	UserInfoList.clear();
	fliedNames.clear();
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	//查询数据
	sql_query.exec("select * from UserInfo");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			UserInfo userInfo;
			userInfo.ID = sql_query.value(0).toInt();
			userInfo.user_Name = sql_query.value(1).toString();
			userInfo.psw = sql_query.value(2).toString();
			userInfo.level = sql_query.value(3).toInt();
			//safeMove.belong = sql_query.value(4).toInt();
			UserInfoList.push_back(userInfo);
		}
	}

	db.close();
	return true;
}

bool CDatabaseManager::ReadFlagInfo(QList<FlagInfo*>& FlagInfoList, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	FlagInfoList.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	//查询数据
	sql_query.exec("select * from Flag");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			
			FlagInfo* flagInfo = new FlagInfo;
			flagInfo->ID = sql_query.value(0).toInt();
			flagInfo->name = sql_query.value(1).toString();
			flagInfo->value = sql_query.value(2).toInt();
			flagInfo->description = sql_query.value(3).toString();
			FlagInfoList.push_back(flagInfo);
		}
	}
	db.close();
	return true;
}

bool CDatabaseManager::ReadTotalTable(QList< S_Total >& totalTable, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	totalTable.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	//查询数据
	sql_query.exec("select * from TotalTable");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Total data;
			data.Name = sql_query.value(0).toString();
			data.ModuleName = sql_query.value(1).toString();
			data.TableName = sql_query.value(2).toString();

			totalTable.push_back(data);
		}
	}
	db.close();
	return true;
}

bool CDatabaseManager::ReadAllTables()
{
	if (!ReadTotalTable(m_Total, m_Title_Total))return false;
	if (!ReadRobot1(m_Robot1, m_Title_Robot1))return false;
	if (!ReadRobot2(m_Robot2, m_Title_Robot2))return false;
	if (!ReadSerialPortManager(m_SerialPortManager, m_Title_SerialPortManager))return false;
	if (!ReadLighting1(m_Lighting1, m_Title_Lighting1))return false;
	if (!ReadCamera1(m_Camera1, m_Title_Camera1))return false;
	if (!ReadLighting2(m_Lighting2, m_Title_Lighting2))return false;
	if (!ReadCamera2(m_Camera2, m_Title_Camera2))return false;
	if (!ReadLightingUp(m_LightingUp,m_Title_LightingUp))return false;
	if (!ReadCameraUp(m_CameraUp,m_Title_CameraUp))return false;
	if (!ReadCameraDown(m_CameraDown, m_Title_CameraDown))return false;
	if (!ReadLightingDown(m_LightingDown,m_Title_LightingDown))return false;
	if (!ReadLightingNG(m_LightingNG,m_Title_LightingNG))return false;
	if (!ReadCameraNG(m_CameraNG,m_Title_CameraNG))return false;
	if (!ReadConveyor1(m_Conveyor1,m_Title_Conveyor1))return false;
	if (!ReadConveyor2(m_Conveyor2,m_Title_Conveyor2))return false;
	if (!ReadPanaMotor1(m_PanaMotor1,m_Title_PanaMotor1))return false;
	if (!ReadPanaMotor2(m_PanaMotor2, m_Title_PanaMotor2))return false;
	if (!ReadIOControl(m_IOControl, m_Title_IOControl))return false;
	if (!ReadUserInfo(m_UserInfoList, m_Title_UserInfoList))return false;
	if (!ReadFlagInfo(m_FlagInfoList, m_Title_FlagInfoList))return false;
	if (!ReadTotalTable(m_Total, m_Title_Total))return false;
	return true;
}



QList<S_Total> CDatabaseManager::GetTotalInfo()
{
	return m_Total;
}

QList<UserInfo> CDatabaseManager::GetUserInfo()
{
	return m_UserInfoList;
}

QList<FlagInfo*> CDatabaseManager::GetFlagInfo()
{
	return m_FlagInfoList;
}

bool CDatabaseManager::UpdatePosTab(CStationMotion station, PointStation * Point)
{
	QMutexLocker loker(&m_mutex);
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	QString sqlStr = QString("UPDATE %1 SET ").arg(station.m_PosTableName);
	sqlStr += QString(" PosName=?, ");
	for (int i = 0; i != station.m_AxisCount; ++i)
	{
		if (i == station.m_AxisCount - 1)
			sqlStr += QString(" %1=? ").arg(station.m_AxisNameList.at(i));
		else
			sqlStr += QString(" %1=? ,").arg(station.m_AxisNameList.at(i));
	}
	sqlStr += QString(" WHERE ID = ? ");
	bool b = sql_query.prepare(sqlStr);
	qDebug() << sql_query.lastError();
	sql_query.addBindValue(Point->PointName);
	for (int i = 0; i != station.m_AxisCount; ++i)
	{
		sql_query.addBindValue(Point->pValueList.at(i));
	}
	sql_query.addBindValue(Point->PointIndex);
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
		db.close();
		return false;
	}
	db.close();
	return true;
}

bool CDatabaseManager::ResetPosTab(CStationMotion station, PointStation * Point)
{
	QMutexLocker loker(&m_mutex);
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	QString sqlStr = QString("UPDATE %1 SET ").arg(station.m_PosTableName);
	sqlStr += QString(" PosName=?, ");
	for (int i = 0; i != station.m_AxisCount; ++i)
	{
		if (i == station.m_AxisCount - 1)
			sqlStr += QString(" %1=? ").arg(station.m_AxisNameList.at(i));
		else
			sqlStr += QString(" %1=? ,").arg(station.m_AxisNameList.at(i));
	}
	sqlStr += QString(" WHERE ID = ? ");
	bool b = sql_query.prepare(sqlStr);
	qDebug() << sql_query.lastError();
	sql_query.addBindValue("");
	for (int i = 0; i != station.m_AxisCount; ++i)
	{
		sql_query.addBindValue("");
	}
	sql_query.addBindValue(Point->PointIndex);
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
		db.close();
		return false;
	}
	db.close();
	return true;
}

bool CDatabaseManager::UpdateFlagTableDescription(QString flagName, QString description)
{
	QMutexLocker loker(&m_mutex);
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	QString sqlStr = QString("UPDATE Flag SET Description ='%1' WHERE Name = '%2'").arg(description).arg(flagName);
	qDebug() << sqlStr;
	sql_query.prepare(sqlStr);
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
		db.close();
		return false;
	}
	db.close();
	return true;
}

bool CDatabaseManager::AddValueByTable(TeachTable teachData)
{
	QMutexLocker loker(&m_mutex);
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	//查询数据
	//QString sqlStr = QString("select * from %1").arg(tabName);
	QString sqlStr, tempStr;
	if (teachData.Name.contains("Robot"))
		tempStr = tempStr+ "insert into " + "Robot_Pos";


	QString fieldStr, values;
	fieldStr.append("(");
	values.append("values(");
	//QString tempStr = "insert into" + teachData.Name+"(";
	for (int i = 0; i < teachData.Keys.count();++i)
	{
		if (i==1)
		{
			fieldStr.append("Model,");
			values.append(":Model,");

			fieldStr.append(teachData.Keys[i] + ",");
			values.append(":" + teachData.Keys[i] + ",");
		}
		else if (i== teachData.Keys.count()-1)
		{
			fieldStr.append(teachData.Keys[i]);
			values.append(":" + teachData.Keys[i]);
		}
		else
		{
			fieldStr.append(teachData.Keys[i]+",");
			values.append(":"+ teachData.Keys[i] + ",");
		}

	}
	fieldStr.append(")");
	values.append(")");
	sqlStr = tempStr + " " + fieldStr+" " + values;
	sql_query.prepare(sqlStr);


	//for (int i = 0; i < teachData.Keys.count(); ++i)
	//{
	//	QVariantList col_list;
	//	for (int j = 0; j < teachData.Data.count();j++)
	//	{
	//		col_list.append(teachData.Data[j][i]);//取每列
	//	}
	//	QString keystr = teachData.Keys[i];
	//	sql_query.bindValue(QString(":%1").arg(teachData.Keys[i]), col_list);
	//}
	//QVariantList col_list;
	//for (int j = 0; j < teachData.Data.count(); j++)
	//{
	//	col_list.append(teachData.Name);//取每列
	//}
	//sql_query.bindValue(":Model", col_list);


	//for (int i = 0; i < teachData.Data.count(); i++)
	//{
	//	QVariantList col_list;
	//	for (int j = 0; j < teachData.Keys.count(); ++j)
	//	{
	//		col_list.append(teachData.Data[i][j]);//取每列
	//	}
	//	sql_query.bindValue(QString(":%1").arg(teachData.Keys[i]), col_list);
	//}


	//sql_query.bindValue(":Model", teachData.Name);
	//for (int i = 0; i < teachData.Keys.count(); ++i)
	//{
	//	//sql_query.bindValue(QString(":%1").arg(teachData.Keys[i]), teachData.Data[teachData.Data.count()-1][i]);
	//	sql_query.bindValue(QString(":%1").arg(teachData.Keys[i]),QVariant("ttttt"));
	//}



	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}

	return true;
}

bool CDatabaseManager::DeleteValueByTableAndFiled(TeachTable teachData)
{

	return true;
}

bool CDatabaseManager::UpdateValueByTableAndFiled(TeachTable teachData)
{

	return true;
}

bool CDatabaseManager::SearchValueByTableString(QString tabName, QVector<QString>&fileds, QVector< QVector<QString>>& values)
{
	QMutexLocker loker(&m_mutex);
	fileds.clear();
	values.clear();
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	QString sqlStr = QString("select * from %1").arg(tabName);
	sql_query.exec(sqlStr);
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fileds << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			QVector<QString> rowData;
			for (int i = 0; i < sqlRecord.count(); i++)
			{
				rowData.push_back(sql_query.value(i).toString());
			}
			values.push_back(rowData);
		}
	}
	return true;
}





bool CDatabaseManager::SearchValueByTableQVariant(QString tabName, QVector<QString>&fileds, QVector< QVector<QVariant>>& values)
{
	QMutexLocker loker(&m_mutex);
	fileds.clear();
	values.clear();
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	QString sqlStr = QString("select * from %1").arg(tabName);
	sql_query.exec(sqlStr);
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fileds << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			QVector<QVariant> rowData;
			for (int i = 0; i < sqlRecord.count(); i++)
			{
				rowData.push_back(sql_query.value(i));
			}
			values.push_back(rowData);
		}
	}
	return true;
}

bool CDatabaseManager::RefreshTeachDataByTabName(QString tableName, QString modelName,QString modelType)
{
	TeachTable data;
	QVector<QVector<QString>> values;

	//查询表数据
	//if (!SearchValueByTable(tableName, data.Keys, data.Data))
	//	return false;
	if (!SearchValueByTableString(tableName, data.Keys, values))
		return false;
	data.Name = modelName;
	data.Type = modelType;
	foreach(QVector<QString>rowData ,values)
	{
		if (rowData[1] == modelName)
			data.Data.append(rowData);
	}
	
	
	emit SendTableDataSignal(data);
	return true;
}

void CDatabaseManager::RefreshTeachData(TeachTable tTeachData)
{
	AddValueByTable(tTeachData);
}

void CDatabaseManager::FillSafeMove(QList<SafeMove>& m_SafeMoveList, QList<SafePos> m_SafePosList)
{
	//foreach(SafeMove& safeMove, m_SafeMoveList)
	//{
	//}
	for (int i = 0; i != m_SafeMoveList.count(); ++i)
	{
		int ID = m_SafeMoveList[i].ID;
		foreach(SafePos pos, m_SafePosList)
		{
			if (pos.belong == ID)
				m_SafeMoveList[i].posList.push_back(pos);
		}
		m_SafeMoveList[i].SortPosList();
	}

	foreach(SafeMove s, m_SafeMoveList)
	{
		qDebug() << "-------------safemove:" << s.name << "---------------";
		foreach(SafePos pos, s.posList)
			qDebug() << "优先级:" << pos.priority << ";名称:" << pos.posName << endl;
	}
}

SafeMove::SafeMove()
{
	name = "";
	posList.clear();
	ID = 0;
}

//SortDis(Dis, PtList, 0, Dis.size() - 1);
//void SortDis(std::vector<double> &Dis, QVector<cv::Point2f> &PtList, int l, int r)
//{
//	//快排标准算法，将Dis由小到大排序
//	if (l >= r) return;
//	size_t i = l, j = r;
//	double tDis = Dis[l];
//	cv::Point tPt = PtList[l];
//
//	while (i < j)
//	{
//		while (i < j && Dis[j] >= tDis) j--;
//		if (i < j)
//		{
//			Dis[i] = Dis[j];
//			PtList[i] = PtList[j];
//			i++;
//		}
//		while (i < j && Dis[i] <= tDis) i++;
//		if (i < j)
//		{
//			Dis[j] = Dis[i];
//			PtList[j] = PtList[i];
//			j--;
//		}
//	}
//	Dis[i] = tDis;
//	PtList[i] = tPt;
//	SortDis(Dis, PtList, l, i - 1);
//	SortDis(Dis, PtList, j + 1, r);
//}





void SafeMove::SortPosList()
{
	for (int i = 0; i < posList.count(); ++i)
	{
		for (int j = i + 1; j < posList.count(); j++)
		{
			SafePos pos = posList.at(i);
			SafePos pos1 = posList.at(j);
			if (pos.priority > pos1.priority)
			{
				posList[i] = pos1;
				posList[j] = pos;
				//冒泡排序交换
				//posList[j] = pos;
				//posList[i] = pos1;
				//pos = pos1;
			}
		}
	}
}

void SafeMove::quickSort()
{
	//	//快排标准算法，将Dis由小到大排序
	//	if (posList.count() < 2)
	//		return;
	//	int l = 0, r = posList.count();
	//	if (l >= r) return;
	//	size_t i = l, j = r;
	//	double tDis = Dis[l];
	//	cv::Point tPt = PtList[l];
	//
	//	while (i < j)
	//	{
	//		while (i < j && Dis[j] >= tDis) j--;
	//		if (i < j)
	//		{
	//			Dis[i] = Dis[j];
	//			PtList[i] = PtList[j];
	//			i++;
	//		}
	//		while (i < j && Dis[i] <= tDis) i++;
	//		if (i < j)
	//		{
	//			Dis[j] = Dis[i];
	//			PtList[j] = PtList[i];
	//			j--;
	//		}
	//	}
	//	Dis[i] = tDis;
	//	PtList[i] = tPt;
	//	SortDis(Dis, PtList, l, i - 1);
	//	SortDis(Dis, PtList, j + 1, r);
}

bool CDatabaseManager::ReadRobot1(QList< S_Robot >& Robot1, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Robot1.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	//查询数据
	sql_query.exec("select * from Robot1");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Robot robot;
			//QString IP;
			//int CmdPort;
			//int StatusPort;

			robot.IP = sql_query.value(0).toString();
			robot.CmdPort = sql_query.value(1).toInt();
			robot.StatusPort = sql_query.value(2).toInt();
			Robot1.push_back(robot);
		}
	}
	db.close();
	return true;
}
bool CDatabaseManager::ReadRobot2(QList< S_Robot >& Robot2, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Robot2.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Robot2");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Robot robot;
			//QString IP;
			//int CmdPort;
			//int StatusPort;

			robot.IP = sql_query.value(0).toString();
			robot.CmdPort = sql_query.value(1).toInt();
			robot.StatusPort = sql_query.value(2).toInt();
			Robot2.push_back(robot);
		}
	}
	return true;
}
bool CDatabaseManager::ReadSerialPortManager(QList< S_SerialPortManager >& SerialPortManager, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	SerialPortManager.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from SerialPortManager");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_SerialPortManager SPM;
			//int ID;
			//QString COM;
			//int BaudRate;
			//int DataBits;
			//int FlowControl;
			//int Parity;
			//int StopBits;


			SPM.COM = sql_query.value(0).toString();
			SPM.BaudRate = sql_query.value(1).toInt();
			SPM.DataBits = sql_query.value(2).toInt();
			SPM.FlowControl = sql_query.value(3).toInt();
			SPM.Parity = sql_query.value(4).toInt();
			SPM.StopBits = sql_query.value(5).toInt();

			SerialPortManager.push_back(SPM);
		}
	}
	return true;
}
bool CDatabaseManager::ReadLighting1(QList< S_Lighting >& Lighting1, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Lighting1.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Lighting1");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Lighting light;
			//int Index;
			//QString ComID;
			//int ID;
			//QString SerialPortManager;

			light.Index = sql_query.value(0).toInt();
			light.ComID = sql_query.value(1).toString();
			light.ID = sql_query.value(2).toInt();
			light.SerialPortManager = sql_query.value(3).toString();

			Lighting1.push_back(light);
		}
	}


	return true;
}
bool CDatabaseManager::ReadCamera1(QList< S_Camera >& Camera1, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Camera1.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Camera1");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Camera camera;
			//QString SN;

			camera.SN = sql_query.value(0).toString();


			Camera1.push_back(camera);
		}
	}

	return true;
}
bool CDatabaseManager::ReadLighting2(QList< S_Lighting >& Lighting2, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Lighting2.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Lighting2");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Lighting light;
			//int Index;
			//QString ComID;
			//int ID;
			//QString SerialPortManager;

			light.Index = sql_query.value(0).toInt();
			light.ComID = sql_query.value(1).toString();
			light.ID = sql_query.value(2).toInt();
			light.SerialPortManager = sql_query.value(3).toString();

			Lighting2.push_back(light);
		}
	}
	return true;
}
bool CDatabaseManager::ReadCamera2(QList< S_Camera >& Camera2, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Camera2.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Camera2");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Camera camera;
			//QString SN;

			camera.SN = sql_query.value(0).toString();


			Camera2.push_back(camera);
		}
	}

	return true;
}
bool CDatabaseManager::ReadLightingUp(QList< S_Lighting > &LightingUp, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	LightingUp.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from LightingUp");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Lighting light;
			//int Index;
			//QString ComID;
			//int ID;
			//QString SerialPortManager;

			light.Index = sql_query.value(0).toInt();
			light.ComID = sql_query.value(1).toString();
			light.ID = sql_query.value(2).toInt();
			light.SerialPortManager = sql_query.value(3).toString();

			LightingUp.push_back(light);
		}
	}
	return true;
}
bool CDatabaseManager::ReadCameraUp(QList< S_Camera >& CameraUp, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	CameraUp.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from CameraUp");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Camera camera;
			//QString SN;

			camera.SN = sql_query.value(0).toString();


			CameraUp.push_back(camera);
		}
	}

	return true;
}

bool CDatabaseManager::ReadCameraDown(QList< S_Camera > &CameraDown, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	CameraDown.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from CameraDown");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Camera camera;
			//QString SN;

			camera.SN = sql_query.value(0).toString();


			CameraDown.push_back(camera);
		}
	}

	return true;
}

bool CDatabaseManager::ReadLightingDown(QList< S_Lighting >& LightingDown, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	LightingDown.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from LightingDown");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Lighting light;
			//int Index;
			//QString ComID;
			//int ID;
			//QString SerialPortManager;

			light.Index = sql_query.value(0).toInt();
			light.ComID = sql_query.value(1).toString();
			light.ID = sql_query.value(2).toInt();
			light.SerialPortManager = sql_query.value(3).toString();

			LightingDown.push_back(light);
		}
	}
	return true;
}
bool CDatabaseManager::ReadLightingNG(QList< S_Lighting >& LightingNG, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	LightingNG.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from LightingNG");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Lighting light;
			//int Index;
			//QString ComID;
			//int ID;
			//QString SerialPortManager;

			light.Index = sql_query.value(0).toInt();
			light.ComID = sql_query.value(1).toString();
			light.ID = sql_query.value(2).toInt();
			light.SerialPortManager = sql_query.value(3).toString();

			LightingNG.push_back(light);
		}
	}
	return true;
}
bool CDatabaseManager::ReadCameraNG(QList< S_Camera >& CameraNG, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	CameraNG.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from CameraNG");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Camera camera;
			//QString SN;

			camera.SN = sql_query.value(0).toString();
			CameraNG.push_back(camera);
		}
	}

	return true;
}
bool CDatabaseManager::ReadConveyor1(QList< S_Conveyor > &Conveyor1, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Conveyor1.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Conveyor1");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Conveyor conveyor;

			//QString COM;
			//int ID;
			//int Vel;
			//QString SerialPortManager;
			conveyor.COM = sql_query.value(0).toString();
			conveyor.ID = sql_query.value(1).toInt();
			conveyor.Vel = sql_query.value(2).toInt();
			conveyor.SerialPortManager = sql_query.value(3).toString();
			Conveyor1.push_back(conveyor);
		}
	}
	return true;
}
bool CDatabaseManager::ReadConveyor2(QList< S_Conveyor >& Conveyor2, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	Conveyor2.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from Conveyor2");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_Conveyor conveyor;

			//QString COM;
			//int ID;
			//int Vel;
			//QString SerialPortManager;
			conveyor.COM = sql_query.value(0).toString();
			conveyor.ID = sql_query.value(1).toInt();
			conveyor.Vel = sql_query.value(2).toInt();
			conveyor.SerialPortManager = sql_query.value(3).toString();
			Conveyor2.push_back(conveyor);
		}
	}
	return true;
}
bool CDatabaseManager::ReadPanaMotor1(QList< S_PanaMotor >& PanaMotor1, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	PanaMotor1.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from PanaMotor1");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_PanaMotor panamotor;

			//QString COM;
			//int ID;
			//int Vel;
			//int Acc;
			//int Dec;
			//int Org;
			//int PursePerRound;
			//int EncoderPerRound;
			//QString SerialPortManager;

			panamotor.COM = sql_query.value(0).toString();
			panamotor.ID = sql_query.value(1).toInt();
			panamotor.Vel = sql_query.value(2).toInt();
			panamotor.Acc = sql_query.value(3).toInt();
			panamotor.Dec = sql_query.value(4).toInt();
			panamotor.Org = sql_query.value(5).toInt();
			panamotor.PursePerRound = sql_query.value(6).toInt();
			panamotor.EncoderPerRound = sql_query.value(7).toInt();
			panamotor.SerialPortManager = sql_query.value(8).toString();

			PanaMotor1.push_back(panamotor);
		}
	}
	return true;
}
bool CDatabaseManager::ReadPanaMotor2(QList< S_PanaMotor >& PanaMotor2, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	PanaMotor2.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from PanaMotor2");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_PanaMotor panamotor;

			//QString COM;
			//int ID;
			//int Vel;
			//int Acc;
			//int Dec;
			//int Org;
			//int PursePerRound;
			//int EncoderPerRound;
			//QString SerialPortManager;

			panamotor.COM = sql_query.value(0).toString();
			panamotor.ID = sql_query.value(1).toInt();
			panamotor.Vel = sql_query.value(2).toInt();
			panamotor.Acc = sql_query.value(3).toInt();
			panamotor.Dec = sql_query.value(4).toInt();
			panamotor.Org = sql_query.value(5).toInt();
			panamotor.PursePerRound = sql_query.value(6).toInt();
			panamotor.EncoderPerRound = sql_query.value(7).toInt();
			panamotor.SerialPortManager = sql_query.value(8).toString();

			PanaMotor2.push_back(panamotor);
		}
	}
	return true;
}
bool CDatabaseManager::ReadIOControl(QList< S_IOControl >& IOControl, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	IOControl.clear();
	QSqlQuery sql_query;
	fliedNames.clear();
	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";
	//查询数据
	sql_query.exec("select * from IOControl");
	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count(); i++)
			fliedNames << sqlRecord.fieldName(i);
		while (sql_query.next())
		{
			S_IOControl control;
			//QString Name;
			//QString Type;
			//int Index;
			//QString Src;
			//QString SrcType;
			//int ID;

			control.Name = sql_query.value(0).toString();
			control.Type = sql_query.value(1).toString();
			control.Index = sql_query.value(2).toInt();
			control.Src = sql_query.value(3).toString();
			control.SrcType = sql_query.value(4).toString();
			control.ID = sql_query.value(5).toInt();
			IOControl.push_back(control);
		}
	}
	return true;
}

QList<S_Robot> CDatabaseManager::GetRobot1()
{
	return m_Robot1;
}
QList<S_Robot> CDatabaseManager::GetRobot2()
{
	return m_Robot2;
}
QList<S_SerialPortManager> CDatabaseManager::GetSerialPortManager()
{
	return m_SerialPortManager;
}
QList<S_Lighting>CDatabaseManager::GetLighting1()
{
	return m_Lighting1;
}
QList<S_Camera>	CDatabaseManager::GetCamera1()
{
	return m_Camera1;
}
QList<S_Lighting>CDatabaseManager::GetLighting2()
{
	return m_Lighting2;
}
QList<S_Camera>CDatabaseManager::GetCamera2()
{
	return m_Camera2;
}
QList<S_Lighting>CDatabaseManager::GetLightingUp()
{
	return m_LightingUp;
}
QList<S_Camera>	CDatabaseManager::GetCameraUp()
{
	return m_CameraUp;
}
QList<S_Lighting>CDatabaseManager::GetLightingDown()
{
	return m_LightingDown;
}
QList<S_Lighting>CDatabaseManager::GetLightingNG()
{
	return m_LightingNG;
}
QList<S_Camera>	CDatabaseManager::GetCameraNG()
{
	return m_CameraNG;
}
QList<S_Conveyor>CDatabaseManager::GetConveyor1()
{
	return m_Conveyor1;
}
QList<S_Conveyor>CDatabaseManager::GetConveyor2()
{
	return m_Conveyor2;
}
QList<S_PanaMotor>CDatabaseManager::GetPanaMotor1()
{
	return m_PanaMotor1;
}
QList<S_PanaMotor>	CDatabaseManager::GetPanaMotor2()
{
	return m_PanaMotor2;
}
QList<S_IOControl>	CDatabaseManager::GetIOControl()
{
	return m_IOControl;
}

int CDatabaseManager::GetTableFiledNamesByTableName(QString tableName, QVector<QString>& fliedNames)
{
	QMutexLocker loker(&m_mutex);
	fliedNames.clear();
	QSqlQuery sql_query;

	if (!db.open())
	{
		qDebug() << "Error: Failed to connect database." << db.lastError();
		return false;
	}
	else
		qDebug() << "Succeed to connect database.";

	

	//查询数据
	QString sqlStr = QString("select * from %1").arg(tableName);
	sql_query.exec(sqlStr);

	if (!sql_query.exec())
	{
		qDebug() << sql_query.lastError();
	}
	else
	{
		QSqlRecord sqlRecord(sql_query.record());
		for (int i = 0; i < sqlRecord.count();i++)
			fliedNames << sqlRecord.fieldName(i);
	}
	return true;


	return -1;
}

















