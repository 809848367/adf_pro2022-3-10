﻿#ifndef ROBOTUI_H
#define ROBOTUI_H

#include <QWidget>
#include <QtCore>
#include <QButtonGroup>
#include "HardwareManager.h"
#include "TeachData_Robot.h"
namespace Ui {
class RobotUI;
}

class RobotUI : public QWidget
{
    Q_OBJECT

public:
    explicit RobotUI(QWidget *parent = nullptr);
    ~RobotUI();
    void SetHardwareManager(HardwareManager* HWM,QString Station);
private:
    Ui::RobotUI *ui;
    HardwareManager* HWM;
    QString Station;
    QTimer RefreshTimer;
    bool isReseting;
    QButtonGroup Pos_Add_Group,Pos_Minus_Group,MoveBtn_Group;

    QButtonGroup TeachDataBtns;
    QButtonGroup TeachDataRadio;
    void InitTeachDataList();
    TeachData_Robot TeachData;
    void ShowTeachData(QString Name = "");
    bool ShowTeachDataFlag;
signals:
    void UpDateTeachData(QString Station,TeachData_Robot TeachData);
private slots:
    void RefreshSlot();
    void on_ResetBtn_clicked();
    void on_EnableBtn_clicked();
    void on_DisableBtn_clicked();
    void Pos_Add_clicked(int n);
    void Pos_Minus_clicked(int n);
    void MoveBtn_clicked(int n);
    void TeachDataBtns_clicked(int n);
    void TeachDataRadio_clicked(int n);

    void on_TeachAdd_clicked();
    void on_TeachDel_clicked();
    void on_TeachAddPoint_clicked();
    void on_TeachDelPoint_clicked();
    void on_MovewayPoint_clicked();
    void on_TeachData_cellChanged(int row, int column);
    void on_TeachList_currentTextChanged(const QString &arg1);
    void RefreshTeachData(QString Station,TeachData_Robot TeachData);
};

#endif // ROBOTUI_H
