#ifndef TEACHDATA_CAMERA_H
#define TEACHDATA_CAMERA_H
#include "TeachData.h"
#include "CameraDefine.h"
class TeachData_Camera : public TeachData
{
    Q_OBJECT
public:
    virtual int FromTable(QVector<QString> Keys, QVector<QVector<QVariant>> Data)override;
    virtual void ToTable(QVector<QString> &Keys, QVector<QVector<QVariant>> &Data)override;
public:
    explicit TeachData_Camera(QObject *parent = nullptr);
    TeachData_Camera(const TeachData_Camera &TeachData);
    int GetParam(QString Name,CameraParam &Param);
    QList<QPair<QString,CameraParam>> GetParams();
    void SetParam(QString Name,CameraParam Param);
    void RemoveParam(QString Name);
    TeachData_Camera &operator =(const TeachData_Camera &TeachData);
private:
    QList<QPair<QString,CameraParam>> Params;

};

#endif // TEACHDATA_CAMERA_H
